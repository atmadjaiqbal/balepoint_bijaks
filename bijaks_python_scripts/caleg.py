# -*- coding: utf-8 -*-
import os
import sys
import time
import redis
import ConfigParser
import simplejson as json

import logging

from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb

pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Caleg(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass


    def detail(self):
        pipe = r_master.pipeline(transaction=False)
        province = self.db.getProvince()
        for row, val in enumerate(province):
            #if row == 1:
            #    break
            dapil = self.db.getDapil(val['province_id'])
            for index, value in enumerate(dapil):
                partai = self.db.getPartai(val['province_id'])
                for i, v in enumerate(partai):
                    caleg = self.db.getCaleg(val['province_id'], value['dapil_id'], v['partai_id'])
                    #print caleg
                    for ind, clg in enumerate(caleg):
                        scandal_count = self.db.getCalegScandal(clg['caleg_id'])
                        news_count = self.db.getCalegNews(clg['caleg_id'])

                        clg['scandal_count'] = scandal_count[0]['co']
                        clg['news_count'] = news_count[0]['co']

                    json_dumps = json.dumps(caleg)
                    pipe.set('caleg:%s:%s:%s' % (val['province_id'], value['dapil_id'], v['partai_id']), json_dumps)

        pipe.execute()