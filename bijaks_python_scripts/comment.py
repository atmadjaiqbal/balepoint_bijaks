# -*- coding: utf-8 -*-
import os
import sys
import time
import redis
import ConfigParser
import simplejson as json

import logging

from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb
pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Comment(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass

    def get_comment_list(self):
        all_content = self.db.getCommentList()
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('comment:latest:list')
        for index, val in enumerate(all_content):
            pipe.rpush('comment:latest:list', val['comment_id'])
        pipe.execute()

    def get_comment(self, comment_id=0):
        comment_content = self.db.getComment(comment_id)
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('comment:latest:detail')
        for index, val in enumerate(comment_content):
            json_comment = json.dumps(val)
            pipe.set('comment:latest:detail:%s' % val['comment_id'], json_comment)
        pipe.execute()

    def doRun(self):
        comment_content = self.timeline_db.getlogchanged('tipe = \'comment\'')
        #print all_content
        for index, val in enumerate(comment_content):
            self.get_comment_list()
            self.get_comment()
            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))
        #self.get_scandal_list()

      
