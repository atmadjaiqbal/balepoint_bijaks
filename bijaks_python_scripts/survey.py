# -*- coding: utf-8 -*-
import os
import sys
import threading
import time
import redis
import pickle
import ConfigParser
import Queue
import simplejson as json


from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb

pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

#url = 'http://news.bijaks.net/mobile/get_post/?dev=1&id='
url = 'http://news.bijaks.net/mobile/get_post/?id='

queue = Queue.Queue()
out_queue = Queue.Queue()


r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Survey(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass

    def survey_last_activity(self):
        last_list = self.db.getsurveylastactivity()
        pipe = r_master.pipeline(transaction=False)

        ps = json.dumps(last_list)
        pipe.set('survey:last:activity', ps)
        pipe.execute()


    def survey(self, where='1 = 1'):
        pipe = r_master.pipeline(transaction=False)

        surveys = self.db.getsurvey(where)
        for i, val in enumerate(surveys):
            question = self.db.getsurveyquestion(val['survey_id'])
            for m, row in enumerate(question):
                option = self.db.getsurveyquestionoption(row['question_id'])
                for k, ans in enumerate(option):
                    answer = self.db.getsurveyquestionanswer(row['question_id'], ans['question_option_id'])
                    ans['answer'] = answer
                    ans['score'] = len(answer)

                row['option'] = option

            val['question'] = question
            valu = json.dumps(val)
            pipe.set('survey:detail:%s' % val['survey_id'], valu)

        pipe.execute()


    def survey_list(self):
        pipe = r_master.pipeline(transaction=False)
        survey = self.db.getsurvey('s.publish = 1')
        pipe.delete('survey:list:all')
        for i, val in enumerate(survey):
            pipe.rpush('survey:list:all', val['survey_id'])

        pipe.execute()


    def get_survey_list_hot(self):
        pipe = r_master.pipeline(transaction=False)
        survey = self.db.getsurvey('s.begin_date <= now() and s.expired_date >= now() and s.publish = 1 and s.hotlist = 1')
        pipe.delete('survey:list:hot')
        for i, val in enumerate(survey):
            pipe.rpush('survey:list:hot', val['survey_id'])

        pipe.execute()

    def doRun(self):
        all_content = self.timeline_db.getlogchanged('tipe = \'survey\'')
        for index, val in enumerate(all_content):
            self.survey('s.survey_id = %s ' % val['id'])
            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))

        self.survey_list()

