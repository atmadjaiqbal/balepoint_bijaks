__author__ = 'alam'
from sqlalchemy import create_engine, exc

f = '%Y-%m-%d %H:%M:%S'

class TimelineDb(object):
    """docstring for ActiveDb"""
    def __init__(self, Config):
        self.myskuel = Config.get('app:main', 'sqlalchemy.url')
        self.engine = create_engine(self.myskuel, pool_size=20, max_overflow=0)
        pass

    def getnewslistheadline(self, limit, offset=0):
        query = '''
            SELECT tt.content_id, tt.news_id
            FROM tcontent_text tt
            JOIN tcontent t ON t.`content_id`=tt.`content_id`
            WHERE tt.type_news = 3 ORDER BY tt.type_news_date DESC LIMIT %s OFFSET %s
        ''' % (limit, offset)

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getscandallisthome(self, where='headline = 1', order='head_sortnumber', limit=20):
        query = '''
            SELECT scandal_id
            FROM tscandal WHERE %s ORDER BY %s ASC LIMIT %s;
        ''' % (where, order, limit)

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getsuksesilisthome(self, where='hotlist = 1', order='hot_date', limit=10):
        query = '''
            SELECT id_race
            FROM trace WHERE %s ORDER BY %s DESC LIMIT %s;
        ''' % (where, order, limit)

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getprofilelistheadline(self, where=1, limit=30):
        query = '''
            SELECT page_id
            FROM tobject_page WHERE publish = %s
            ORDER BY publish_date DESC LIMIT %s;
        ''' % (where, limit)

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getprofilelisthome(self, limit=50):
        query = '''
            SELECT page_id FROM tusr_hotlist ORDER BY id_tusr_hotlist ASC LIMIT %s
        ''' % (limit)

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getinstitusilisthome(self, limit=50):
        query = '''
            SELECT page_id FROM tusr_institusi ORDER BY id_institusi ASC LIMIT %s
        ''' % (limit)

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getscandallist(self):
        query = '''
            SELECT * FROM
            (
                SELECT id, MAX(tanggal) AS tanggal FROM
                (
                    SELECT * FROM
                    (
                    SELECT * FROM
                    (
                        SELECT scandal_id AS id, MAX(`date`) AS tanggal
                        FROM tscandal
                        WHERE main_scandal = 0 AND publish = 1 AND scandal_id !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.headline = 1 ORDER BY tt.head_date DESC LIMIT 1
                        )AND scandal_id !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.`hotlist` = 1 ORDER BY tt.`hot_date` DESC LIMIT 1
                        )
                        GROUP BY scandal_id
                    )AS scandal ORDER BY tanggal DESC
                    )AS s UNION ALL
                    SELECT * FROM
                    (
                    SELECT * FROM
                    (
                        SELECT t.main_scandal AS id, MAX(t.date) AS tanggal
                        FROM tscandal t
                        LEFT JOIN tscandal ts ON ts.scandal_id = t.main_scandal
                        WHERE t.main_scandal != 0 AND ts.publish = 1 AND t.main_scandal !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.headline = 1 ORDER BY tt.head_date DESC LIMIT 1
                        )AND t.main_scandal !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.`hotlist` = 1 ORDER BY tt.`hot_date` DESC LIMIT 1
                        )
                        GROUP BY t.main_scandal
                    )AS kronologi ORDER BY tanggal DESC
                    )AS k
                )AS ld GROUP BY id
            )AS mk ORDER BY tanggal DESC
        '''

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getsuksesilist(self):
        query = '''
            SELECT id_race FROM trace WHERE publish = 1 ORDER BY tgl_pelaksanaan DESC, id_race DESC;
        '''

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def get_home_total(self):
        query = '''
            SELECT * FROM
            (
                SELECT 'like' AS tipe, SUM(total) as total FROM
                (
                    SELECT * FROM
                    (
                        SELECT COUNT(*) AS total FROM tcontent_like
                    )AS content_like UNION ALL
                    SELECT * FROM
                    (
                        SELECT COUNT(*) AS total  FROM page_like
                    )AS page_like

                ) AS lik UNION ALL

                SELECT 'comment' AS tipe, SUM(total) as total FROM
                (
                    SELECT * FROM
                    (
                        SELECT COUNT(*) AS total FROM tcontent_comment
                    )AS content_comment UNION ALL
                    SELECT * FROM
                    (
                        SELECT COUNT(*) AS total FROM page_comment
                    )AS page_comment

                ) AS commen UNION ALL

                SELECT 'user' AS tipe, total FROM
                (
                    SELECT COUNT(*) AS total FROM tusr_account
                    WHERE account_type = 0
                )AS us UNION ALL

                SELECT 'skandal' AS tipe, total FROM
                (
                    SELECT COUNT(*) AS total FROM tscandal WHERE `tscandal`.`main_scandal` = 0
                )AS skandal UNION ALL

                SELECT 'suksesi' AS tipe, total FROM
                (
                    SELECT COUNT(*) AS total FROM trace
                )AS suksesi UNION ALL

                SELECT 'tokoh' AS tipe, total FROM
                (
                    SELECT COUNT(*) AS total FROM v_usr_politik
                )AS tokoh UNION ALL
                SELECT 'news' AS tipe, total FROM
                (
	             SELECT COUNT(*) AS total FROM tcontent WHERE content_group_type = 12
                )AS news UNION ALL
                SELECT 'surpey' AS tipe, total FROM
                (
                     SELECT COUNT(*) AS total FROM question
                ) AS surpey

            )AS home_total;
        '''

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getcontentscore(self, content_id):
        query = '''
            SELECT * FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, content_id, COUNT(tc.`content_id`) AS total
                    FROM tcontent_comment tc WHERE `status` = 0 AND content_id = '%s'
                )AS `comment` UNION ALL
                SELECT * FROM
                (
                    SELECT 'like' AS tipe, content_id, COUNT(`content_id`) AS total
                    FROM tcontent_like tl WHERE content_id = '%s'
                )AS `like` UNION ALL
                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, content_id, COUNT(`content_id`) AS total
                    FROM tcontent_dislike tl WHERE content_id = '%s'
                )AS `dislike`
            )AS last_count
        ''' % (content_id, content_id, content_id)

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getlogchanged(self, where=0):
        wh = '1 = 1'
        if where != 0:
            wh = where

        query = '''
            SELECT * FROM
            (
                SELECT * FROM tb_changed_redis WHERE %s and status = 1 ORDER BY insert_date DESC
            )AS ch GROUP BY id;
        ''' % wh

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def updatelogchanged(self, id=0, tgl=''):

        query = '''
            update tb_changed_redis
            set status = 2
            where id = '%s' AND insert_date <= '%s';
        ''' % (id, tgl)

        dic = []
        con = self.engine.connect()
        try:
            con.execute(query)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getStructureNode(self, parent=0):
        wh = 'parent_node IS NULL'
        if parent != 0:
            wh = 'parent_node = %s' % parent

        #`status` = 0 AND

        query = '''
            SELECT id_structure_node, structure_name, page_id
            FROM structure_node
            WHERE %s ORDER BY level_node ASC;
        ''' % wh

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def setStructureCaleg(self, dt=dict()):
        query = '''
            INSERT INTO structure_caleg
            set partai = '%s',
             partai_id = '%s',
             province = '%s',
             province_id = '',
             dapil = '%s',
             caleg = '%s',
             caleg_id = '%s'
        ''' % (dt['partai'],dt['partai_id'], dt['province'], dt['dapil'], dt['caleg'], dt['caleg_id'])

        dic = []
        con = self.engine.connect()
        try:
            con.execute(query)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

    def truncateStructureCaleg(self):
        query = '''
            TRUNCATE structure_caleg;
        '''

        dic = []
        con = self.engine.connect()
        try:
            con.execute(query)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

    def updateStructureCalegDapil(self):
        query = '''
            UPDATE structure_caleg sc SET dapil_id = (SELECT mp.dapil_id FROM master_dapil mp WHERE mp.province_id = sc.province_id AND mp.`dapil`=sc.`dapil`)
        '''

        dic = []
        con = self.engine.connect()
        try:
            con.execute(query)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

    def updateStructureCalegProvince(self):
        query = '''
            UPDATE structure_caleg sc SET province_id = (SELECT mp.province_id FROM master_province mp WHERE mp.province = sc.province)
        '''

        dic = []
        con = self.engine.connect()
        try:
            con.execute(query)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

    def getscandallistbycategory(self,category_id=1):
        #offset = (page-1)*limit
        query = '''
            SELECT * FROM
            (
                SELECT id, MAX(tanggal) AS tanggal FROM
                (
                    SELECT * FROM
                    (
                    SELECT * FROM
                    (
                        SELECT scandal_id AS id, MAX(`date`) AS tanggal
                        FROM tscandal
                        WHERE main_scandal = 0 AND publish = 1 AND scandal_id !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.headline = 1 ORDER BY tt.head_date DESC LIMIT 1
                        )AND scandal_id !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.`hotlist` = 1 ORDER BY tt.`hot_date` DESC LIMIT 1
                        )
                        GROUP BY scandal_id
                    )AS scandal ORDER BY tanggal DESC
                    )AS s UNION ALL
                    SELECT * FROM
                    (
                    SELECT * FROM
                    (
                        SELECT t.main_scandal AS id, MAX(t.date) AS tanggal
                        FROM tscandal t
                        LEFT JOIN tscandal ts ON ts.scandal_id = t.main_scandal
                        WHERE t.main_scandal != 0 AND ts.publish = 1 AND t.main_scandal !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.headline = 1 ORDER BY tt.head_date DESC LIMIT 1
                        )AND t.main_scandal !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.`hotlist` = 1 ORDER BY tt.`hot_date` DESC LIMIT 1
                        )
                        GROUP BY t.main_scandal
                    )AS kronologi ORDER BY tanggal DESC
                    )AS k
                )AS ld GROUP BY id
            )AS mk 


            INNER JOIN tscandal_category_detail tsd ON(tsd.`scandal_id` = mk.id)
            WHERE tsd.`scandal_category_id`= %s


            ORDER BY tanggal DESC limit 53
        ''' % (category_id)

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getcategorylist(self):
        query = '''
        SELECT scandal_category_id FROM tscandal_category ORDER BY sort_number ASC
        '''
        dic = []
        con = self.engine.connect();
        try:
            result = con.execute(query)
            for index, row in enumerate(result):
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)
        except exc.DBAPIError, e:
            pass
        finally:
            con.close()
        return dic

    def getheadtopten(self):
        query = '''
            SELECT id_topten_category FROM master_topten_category ORDER BY set_headline ASC
        '''

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)
            for index, row in enumerate(result):
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass

        finally:
            con.close()

        return dic

    def getheadtoptendetail(self, id_topten=0):
        wh = ''
        if(id_topten != 0):
            wh = 'WHERE id_topten_category = %s' % id_topten

        query = '''
            SELECT * FROM master_topten_category %s
            ORDER BY set_headline ASC
        ''' % wh

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)
            for index, row in enumerate(result):
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass

        finally:
            con.close()

        return dic

    def getcontentevent(self):
        query = '''
            SELECT event_id FROM tcontent_event ORDER BY no_urut ASC LIMIT 2
        '''

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)
            for index, row in enumerate(result):
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass

        finally:
            con.close()

        return dic

    def getcontenteventdetail(self, id_event=0):
        wh = ''
        if(id_event != 0):
            wh = 'and event_id = %s' % id_event

        query = '''
            SELECT * FROM tcontent_event WHERE 1 = 1 %s ORDER BY no_urut ASC LIMIT 2
        ''' % wh

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)
            for index, row in enumerate(result):
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass

        finally:
            con.close()

        return dic
