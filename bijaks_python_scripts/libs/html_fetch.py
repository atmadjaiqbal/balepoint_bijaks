# -*- coding: utf-8 -*-
import urllib
import urllib2
import threading

import os
import sys

import datetime
import time
threadLock = threading.Lock()

class HtmlFetch(threading.Thread):

    def __init__(self, queue, out_queue):
        threading.Thread.__init__(self)
        self.queue = queue
        self.out_queue = out_queue

    def run(self):
        threadLock.acquire()
        while True:
            try:
                qu = self.queue.get()

                #pid = qu['pid']
                tipe = qu['tipe']
                host = qu['host']


                request = urllib2.Request(host)
                if tipe == 1:
                    values = qu['values']
                    data = urllib.urlencode(values)
                    headers = qu['headers']
                    request = urllib2.Request(host, data, headers)

                response = urllib2.urlopen(request)
                html = response.read()

                self.out_queue.put(html)

            except Exception, e:
                #self.queue.task_done()
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                #print 'error message : %s %s %s' % (exc_obj, fname, exc_tb.tb_lineno)
                self.out_queue.put('1')
                continue

        self.queue.task_done()
        threadLock.release()