import sys
import os
#import MySQLdb
import datetime
from sqlalchemy import create_engine, exc

f = '%Y-%m-%d %H:%M:%S'

class ActiveDb(object):
    """docstring for ActiveDb"""
    def __init__(self, Config):
        #self.database = MySQLdb.connect(HOST, self.user, self.password, self.database)
        self.host = str(Config.get('mysql', 'host'))
        self.user = str(Config.get('mysql', 'user'))
        self.password = str(Config.get('mysql', 'password'))
        self.port = int(Config.get('mysql', 'port'))
        self.database = str(Config.get('mysql', 'database'))
        self.myskuel = Config.get('app:main', 'sqlalchemy.url')
        self.base_url = Config.get('root:web', 'base.url')
        self.engine = create_engine(self.myskuel, pool_size=20, max_overflow=0)
        pass

    def getnewslist(self, id=0, id_type=0):
        wh = '1 = 1'

        if id != 0:
            if id_type == 0:
                wh = 'tt.news_id = %s' % id
            else:
                wh = 'topic_id = %s' %id

        #query = '''
        #    SELECT tt.content_id, tt.news_id, tt.type_news, tt.type_news_date, t.entry_date, tht.topic_id
        #    FROM tcontent_text tt
        #    JOIN tcontent t ON t.`content_id` = tt.`content_id`
        #    JOIN tcontent_has_topic tht ON tht.`content_id` = t.`content_id`
        #    WHERE %s and tt.news_id IS NOT NULL AND tt.news_id != ' ' AND tt.news_id != 0
        #    ORDER BY t.`entry_date` DESC;
        #
        #    ''' % wh
        query = '''
            SELECT * FROM
            (
                SELECT tt.content_id, tt.news_id, tt.type_news, tt.type_news_date, t.entry_date, tht.topic_id
                FROM tcontent_text tt
                JOIN tcontent t ON t.`content_id` = tt.`content_id`
                JOIN tcontent_has_topic tht ON tht.`content_id` = t.`content_id`
                WHERE %s and tt.news_id IS NOT NULL AND tt.news_id != ' ' AND tt.news_id != 0
                ORDER BY t.entry_date DESC LIMIT 500 OFFSET 0
            ) AS ta GROUP BY `news_id`;
            ''' % (wh)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
                #print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getnewsprofile(self, contentid):
        query = '''
            SELECT tht.status, tht.score, tp.`page_id`, tp.`page_name`,  ta.`attachment_title` FROM tcontent_has_politic tht
            JOIN tobject_page tp ON tp.`page_id` = tht.page_id
            JOIN tcontent_attachment ta ON ta.`content_id` = tp.profile_content_id WHERE tht.`content_id` = '%s'
            ''' % contentid

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getcontentlastactivity(self, id=0):
        where = '1=1'
        if id != 0:
            where = "tm.content_id = '%s'" % id

        query = '''
            SELECT tm.*, ta.`display_name`, tp.`page_id`, tp.`profile_content_id`, tac.attachment_title FROM
            (
            	SELECT * FROM
            	(
            		SELECT 'comment' AS tipe, account_id, text_comment AS teks, content_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM tcontent_comment tc
            				ORDER BY entry_date DESC
            			) AS last_comment GROUP BY content_id UNION ALL
            		SELECT 'like' AS tipe, account_id, 'like' AS teks, content_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM tcontent_like
            				ORDER BY entry_date DESC
            			)AS last_like GROUP BY content_id UNION ALL
            		SELECT 'dislike' AS tipe, account_id, 'dislike' AS teks, content_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM tcontent_dislike
            				ORDER BY entry_date DESC
            			)AS last_dislike GROUP BY content_id
            	)AS act ORDER BY entry_date DESC
            ) AS tm
            JOIN `tusr_account` ta ON ta.`account_id`=tm.`account_id`
            JOIN  `tobject_page` tp ON tp.page_id = ta.`user_id`
            LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
            WHERE %s AND tm.content_id IS NOT NULL AND tm.content_id != ' ' AND tm.content_id != '0' GROUP BY tm.content_id;
        ''' % where
        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getprofilelastactivity(self, limit=0):
        lm = ''
        if(limit != 0):
            lm = 'limit 1'
        query = '''
            SELECT tm.*, top.`page_name` AS title, ta.`display_name`, ta.`display_name` AS page_name, tp.`page_id`, tp.`profile_content_id`, tac.attachment_title FROM
            (
            	SELECT * FROM
            	(
            		SELECT 'comment' AS tipe, account_id, comment_text AS teks, page_id AS profile_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM page_comment tc
            				WHERE tc.`status` = 0
            				ORDER BY entry_date DESC %s
            			) AS last_comment GROUP BY page_id UNION ALL
            		SELECT 'like' AS tipe, account_id, 'like' AS teks, page_id AS profile_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM page_like
            				ORDER BY entry_date DESC %s
            			)AS last_like GROUP BY page_id UNION ALL
            		SELECT 'dislike' AS tipe, account_id, 'dislike' AS teks, page_id AS profile_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM page_dislike
            				ORDER BY entry_date DESC %s
            			)AS last_dislike GROUP BY page_id
            	)AS act ORDER BY entry_date DESC
            ) AS tm
            LEFT JOIN tobject_page top ON top.`page_id`=tm.profile_id
            LEFT JOIN `tusr_account` ta ON ta.`account_id`=tm.`account_id`
            LEFT JOIN  `tobject_page` tp ON tp.page_id = ta.`user_id`
            LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
            GROUP BY tm.profile_id;
        ''' % (lm, lm, lm)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic



    def gettopiclastactivity(self, topic_id):
        query = '''
            SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT 'comment' AS tipe, account_id, content_id, text_comment AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tc.status = 0 AND tt.`news_id` IS NOT NULL AND %s
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT 'like' AS tipe, account_id, content_id, 'like' AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tt.`news_id` IS NOT NULL AND %s
                    ORDER BY tc.entry_date DESC LIMIT 1
                )AS last_topic_like GROUP BY content_id UNION ALL

                SELECT 'dislike' AS tipe, account_id, content_id, 'dislike' AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tt.`news_id` IS NOT NULL and %s
                    ORDER BY tc.entry_date DESC LIMIT 1
                )AS last_topic_dislike  GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`;

        ''' % (topic_id, topic_id, topic_id)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getbijaklastactivity(self):
        query = '''
            SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT 'comment' AS tipe, account_id, content_id, text_comment AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tc.status = 0 AND tt.`news_id` IS NOT NULL
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT 'like' AS tipe, account_id, content_id, 'like' AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tt.`news_id` IS NOT NULL
                    ORDER BY tc.entry_date DESC LIMIT 1
                )AS last_topic_like GROUP BY content_id UNION ALL

                SELECT 'dislike' AS tipe, account_id, content_id, 'dislike' AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tt.`news_id` IS NOT NULL
                    ORDER BY tc.entry_date DESC LIMIT 1
                )AS last_topic_dislike  GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`;
        '''

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getskandallastactivity(self):
        query = '''
            SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, tc.`account_id`, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE tc.status = 0 AND t.`location` IS NOT NULL AND t.`content_group_type` = 50
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'like' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 50
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_like GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 50
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_dislike GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`;
        '''

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)            

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getsuksesilastactivity(self):
        query = '''
            SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, tc.`account_id`, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE tc.status = 0 AND t.`location` IS NOT NULL AND t.`content_group_type` = 56
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'like' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 56
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_like GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 56
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_dislike GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`;
        '''

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getsurveylastactivity(self):
        query = '''
            SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, tc.`account_id`, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE tc.status = 0 AND t.`location` IS NOT NULL AND t.`content_group_type` = 54
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'like' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 54
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_like GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 54
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_dislike GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`;
        '''

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getcontent(self, where=0):
        wh = '1 = 1'
        if where != 0:
            wh = "content_id = '%s'" % where
        query = '''
            SELECT * FROM tcontent where %s and  status = 0 ORDER BY entry_date DESC
            ''' % wh
        dic = []
        con = self.engine.connect()
        try:
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #exc_type, exc_obj, exc_tb = sys.exc_info()
            #fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            #print 'error message : %s %s %s' % (exc_obj, fname, exc_tb.tb_lineno)
            #print '\n', query
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getprofile(self, where=0):
        wh = '1 = 1'
        if where != 0:
            wh = "vup.`page_id` = '%s'" % where
        query = '''
            SELECT vup.page_id, vup.page_name, vup.profile_content_id, vup.entry_date, vup.entry_by, vup.last_update_date, vup.website,
            vup.about, vup.personal_message, vup.posisi, vup.prestasi, vup.prestasi_lain, vup.partai_id, tac.account_id,
            tac.`email` ,tac.`gender`, tac.`birthday`, tac.`phone`, tac.alias,
            ta.`attachment_title`, tp.`page_name` AS partai_name, tac2.alias as partai_alias, ta_partai.`attachment_title` AS attachment_title_partai,
            CONCAT('{0}/public/upload/image/politisi/', vup.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.partai_id, '/icon/', ta_partai.attachment_title) AS icon_partai_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.partai_id, '/large/', ta_partai.attachment_title) AS large_partai_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.partai_id, '/thumb/', ta_partai.attachment_title) AS thumb_partai_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.partai_id, '/badge/', ta_partai.attachment_title) AS badge_partai_url
            FROM v_usr_politik vup
            LEFT JOIN tusr_account tac ON tac.`user_id` = vup.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = vup.`profile_content_id`
            LEFT JOIN tobject_page tp ON tp.`page_id` = vup.`partai_id`
            LEFT JOIN tusr_account tac2 ON tac2.`user_id` = tp.`page_id`
            LEFT JOIN tcontent_attachment ta_partai ON ta_partai.`content_id` = tp.`profile_content_id`
            where %s
        ''' % wh

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getcontentalbum(self, page_id):
        query = '''
            SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date`
            FROM tcontent tc
            WHERE content_group_type = 40 AND content_type = 'GROUP'
            AND tc.`status` = 0 AND tc.page_id = '%s'
            ORDER BY tc.`entry_date` DESC;
        ''' % page_id

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getcontentfotoalbum(self, content_id):
        query = '''
            SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date` , ta.`attachment_title`, ta.hotlist AS 'hotlist', ta.headline AS 'headline',
            ta.`order` AS 'set_order', ta.`is_profile` AS 'is_profile',
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/headline/', ta.attachment_title) AS headline_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/hotlist/', ta.attachment_title) AS hotlist_url
            FROM tcontent tc
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tc.`content_id`
            WHERE tc.content_group_type = 40 AND tc.content_type = 'IMAGE'
            AND tc.`status` = 0 AND tc.group_content_id = '%s'
            ORDER BY ta.`order` ASC;
        ''' % content_id

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getcontentfoto(self, page_id, content_type):
        query = '''
            SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date` , ta.`attachment_title`, ta.hotlist AS 'hotlist', ta.headline AS 'headline',
            ta.`order` AS 'set_order', ta.`is_profile` AS 'is_profile',
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/headline/', ta.attachment_title) AS headline_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/hotlist/', ta.attachment_title) AS hotlist_url
            FROM tcontent tc
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tc.`content_id`
            WHERE tc.content_group_type = %s AND tc.content_type = 'IMAGE'
            AND tc.`status` = 0 AND tc.page_id = '%s'
            ORDER BY ta.`order` ASC;
        ''' % (content_type, page_id)

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getcontentfotoheadline(self, page_id):
        query = '''
            SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date` , ta.`attachment_title`, ta.hotlist AS 'hotlist', ta.headline AS 'headline',
            ta.`order` AS 'set_order', ta.`is_profile` AS 'is_profile',
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/headline/', ta.attachment_title) AS headline_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/hotlist/', ta.attachment_title) AS hotlist_url
            FROM tcontent tc
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tc.`content_id`
            WHERE tc.content_type = 'IMAGE'
            AND tc.`status` = 0 AND ta.headline = 1 AND tc.page_id = '%s'
            ORDER BY ta.`order` ASC;
        ''' % (page_id)

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getcontentfotohotlist(self, page_id):
        query = '''
            SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date` , ta.`attachment_title`, ta.hotlist AS 'hotlist', ta.headline AS 'headline',
            ta.`order` AS 'set_order', ta.`is_profile` AS 'is_profile',
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/headline/', ta.attachment_title) AS headline_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/hotlist/', ta.attachment_title) AS hotlist_url
            FROM tcontent tc
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tc.`content_id`
            WHERE tc.content_type = 'IMAGE'
            AND tc.`status` = 0 AND ta.hotlist = 1 AND tc.page_id = '%s'
            ORDER BY ta.`order` ASC;
        ''' % (page_id)

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getskandalbyplayer(self, page_id):
        query = '''
            SELECT tp.date, tp.`title` AS title_player, tp.`description` AS player_desc, tp.`short_desc`, tp.`sumber_data`, tp.`nomor_urut`, tp.`pengaruh`,
            ts.`scandal_id`, ts.`title` AS scandal_title, ts.`description` AS scandal_desc
            FROM tscandal_player tp
            LEFT JOIN tscandal ts ON ts.`scandal_id`=tp.`scandal_id`
            WHERE tp.page_id  = '%s'
            ORDER BY tp.date desc;
        ''' % page_id

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            
            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getcontentcomment(self, content_id):
        query = '''
            SELECT tc.*, tp.`page_id`, tp.`page_name`, tat.`attachment_title`
            FROM tcontent_comment tc
            LEFT JOIN tusr_account ta ON ta.`account_id`=tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`
            where tc.content_id = '%s' and tc.status = 0
            ORDER BY tc.entry_date desc
        ''' % str(content_id)

        dic = list()

        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)

                #if row['comment_id'] == '4c98a18c807da0b3aeed397eec1fff172cdc0824':
                #    print row['text_comment'].decode('ascii','ignore')
                #    print str(row['text_comment'])
                dic.append(di)
            con.close()
        except exc.DBAPIError, e:
            con.close()
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    result.close()

        return dic

    def getprofilecomment(self, page_id):

        query = '''
            SELECT pc.page_comment_id, pc.`account_id`, pc.`comment_text` as text_comment, pc.`page_id` AS profile_id, pc.entry_date,  pc.`status` , tp.`page_id`, tp.`page_name`, tat.`attachment_title`
            FROM page_comment pc
            LEFT JOIN tusr_account ta ON ta.`account_id` = pc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id` = ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id` = tp.`profile_content_id`
            WHERE pc.page_id = '%s' and pc.status = 0
            ORDER BY pc.entry_date DESC;
        ''' % str(page_id)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)
            con.close()
        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getskandal(self, scandal_id=0, main_scandal=0):
        wh = '1 = 1'
        if(scandal_id != 0):
            wh = 'ts.scandal_id = %s' % scandal_id
        query = '''
            SELECT ts.*, tc.content_id
            FROM tscandal ts
            LEFT JOIN tcontent tc ON tc.`location` = ts.`scandal_id` AND tc.content_group_type = 50
            WHERE %s and ts.main_scandal = %s
            ORDER BY `date` DESC
        ''' % (wh, main_scandal)

        dic = []
        try:
            con = self.engine.connect()
            
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)
            con.close()
        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getskandalphoto(self, scandal_id):
        query = '''
            SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/skandal/', ta.attachment_title) AS original_url,
            CONCAT('{0}/public/upload/image/skandal/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url
            FROM tscandal_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            where tp.scandal_id = %s ORDER BY ta.`order` ASC
        ''' % scandal_id

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)
            
            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)
            con.close()
        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getskandalphotoheadline(self, scandal_id):
        query = '''
            SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/skandal/', ta.attachment_title) AS original_url,
            CONCAT('{0}/public/upload/image/skandal/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url
            FROM tscandal_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            where tp.scandal_id = %s AND ta.headline = 1
        ''' % scandal_id

        query = query.format(self.base_url)
        
        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)
            con.close()
        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getskandalphotohotlist(self, scandal_id):
        query = '''
            SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/skandal/', ta.attachment_title) AS original_url,
            CONCAT('{0}/public/upload/image/skandal/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url
            FROM tscandal_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            where tp.scandal_id = %s AND ta.hotlist = 1
        ''' % scandal_id

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)
           
            #print query
            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()
        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getskandalplayer(self, scandal_id):
        query = '''
            SELECT sp.date, sp.title, sp.`description`, sp.`short_desc`, sp.`sumber_data`, sp.`main_player`, sp.`scandal_id`, sp.`nomor_urut`,
            sp.`pengaruh`, tp.page_id, tp.`page_name`,
            CONCAT('{0}/politisi/index/', tp.page_id) AS profile_url,
            ta.`attachment_title`,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS profile_icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/thumb/', ta.attachment_title) AS profile_thumb_url
            FROM tscandal_player sp
            LEFT JOIN tobject_page tp ON tp.`page_id`=sp.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            where sp.scandal_id = %s
            ORDER BY nomor_urut ASC
        ''' % scandal_id

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            #print query
            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()
        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getskandalrelations(self, scandal_id):
        query = '''
            SELECT tm.*,
            tp.`page_name` AS page_name_source, ta.`attachment_title` AS attachment_title_source,
            CONCAT('{0}/politisi/index/', tp.page_id) AS profile_source_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/thumb/', ta.attachment_title) AS profile_thumb_url_source,
            tp2.`page_name` AS page_name_target, ta2.`attachment_title` AS attachment_title_target,
            CONCAT('{0}/politisi/index/', tp2.page_id) AS profile_target_url,
            CONCAT('{0}/public/upload/image/politisi/', tp2.`page_id`, '/thumb/', ta2.attachment_title) AS profile_thumb_url_target
            FROM trelations_map tm
            LEFT JOIN tobject_page tp ON tp.`page_id` = tm.`source_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            LEFT JOIN tobject_page tp2 ON tp2.`page_id`=tm.`target_id`
            LEFT JOIN tcontent_attachment ta2 ON ta2.`content_id`=tp2.`profile_content_id`
            WHERE scandal_id = %s ORDER BY tanggal DESC;
        ''' % scandal_id

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)
            
            #print query
            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()
        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesi(self, id_race=0):
        wh = '1 = 1'
        if(id_race != 0):
            wh = 't.id_race = %s' % id_race
        query = '''
           SELECT t.* , td.`dname`, tkd.`kdname`, tc.`content_id`, tss.status_suksesi as 'status_akhir_suksesi'
            FROM trace t
            LEFT JOIN trace_daerah td ON td.`id_daerah`=t.`id_daerah`
            LEFT JOIN trace_kepala_daerah tkd ON tkd.`id_kepala_daerah` = t.`id_kepala_daerah`
            LEFT JOIN tcontent tc ON tc.`location` = t.`id_race` AND tc.`content_group_type` = 56
            LEFT JOIN trace_status_suksesi tss ON t.idtrace_status_suksesi= tss.idtrace_status_suksesi
            where %s
        ''' % wh

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()
        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesistatus(self, id_suksesi):
        query = '''
            SELECT * FROM trace_status WHERE id_race = %s ORDER BY `status` ASC;
        ''' % id_suksesi

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesikandidat(self, status):
        query = '''
            SELECT tk.*, tp.`page_name`, ta.`attachment_title`,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/badge/', ta.attachment_title) AS profile_badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS profile_icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/large/', ta.attachment_title) AS profile_large_url,
            tp2.`page_name` AS page_name_pasangan, ta2.`attachment_title` AS attachment_title_pasangan,
            CONCAT('{0}/public/upload/image/politisi/', tp2.`page_id`, '/badge/', ta2.attachment_title) AS profile_badge_pasangan_url,
            CONCAT('{0}/public/upload/image/politisi/', tp2.`page_id`, '/icon/', ta2.attachment_title) AS profile_icon_pasangan_url,
            CONCAT('{0}/public/upload/image/politisi/', tp2.`page_id`, '/large/', ta2.attachment_title) AS profile_large_pasangan_url
            FROM trace_kandidat tk
            LEFT JOIN tobject_page tp ON tp.`page_id`=tk.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            LEFT JOIN tobject_page tp2 ON tp2.`page_id`=tk.`page_id_pasangan`
            LEFT JOIN tcontent_attachment ta2 ON ta2.`content_id`=tp2.`profile_content_id`
            WHERE tk.`id_trace_status` = %s
            ORDER BY tk.`nomor_urut` ASC
        ''' % status

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesikandidatpendukung(self, id_kandidat):
        query = '''
            SELECT tk.*, tp.`page_name` AS partai_name, ta.`attachment_title`,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/badge/', ta.attachment_title) AS partai_badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS partai_icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/large/', ta.attachment_title) AS partai_large_url
            FROM trace_kandidat_pendukung tk
            LEFT JOIN tobject_page tp ON tp.`page_id`=tk.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            WHERE idtrace_kandidat = %s
        ''' % id_kandidat

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesilembaga(self, status):
        query = '''
            SELECT tk.*, tp.`page_name` AS lembaga_name, tu.`alias` AS 'lembaga_alias', ta.`attachment_title`,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/badge/', ta.attachment_title) AS partai_badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS partai_icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/large/', ta.attachment_title) AS partai_large_url,
	(select update_date from trace_score where id_trace_lembaga = tk.id_trace_lembaga limit 1) as score_update_date
FROM trace_lembaga tk
            LEFT JOIN tobject_page tp ON tp.`page_id`=tk.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            LEFT JOIN tusr_account tu ON tp.`page_id` = tu.`user_id`
            WHERE id_trace_status = %s order by score_update_date desc
        ''' % status

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesiphoto(self, suksesi_id):
        query = '''
            SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/suksesi/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/portrait/', ta.attachment_title) AS thumb_portrait_url
            FROM trace_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            WHERE tp.id_race = %s ORDER BY ta.order ASC;
        ''' % suksesi_id

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic


    def getsuksesiphotoheadline(self, suksesi_id):
        query = '''
            SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/suksesi/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/portrait/', ta.attachment_title) AS thumb_portrait_url
            FROM trace_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            WHERE tp.id_race = %s AND ta.headline = 1;
        ''' % suksesi_id

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesiphotohotlist(self, suksesi_id):
        query = '''
            SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/suksesi/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/portrait/', ta.attachment_title) AS thumb_portrait_url
            FROM trace_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            WHERE tp.id_race = %s AND ta.hotlist = 1;
        ''' % suksesi_id

        query = query.format(self.base_url)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesisengketa(self, suksesi_id):
        query = '''
            SELECT * FROM trace_sengketa
            WHERE id_race = %s;
        ''' % suksesi_id

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic

    def getsuksesiscore(self, lebaga_id, kandidat_id):
        query = '''
            SELECT * FROM trace_score
            WHERE id_trace_lembaga = %s and idtrace_kandidat = %s
        ''' % (lebaga_id, kandidat_id)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

            con.close()

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        #finally:
        #    con.close()

        return dic


    def getprofiledetail(self):
        query = '''
            SELECT `page_id`, `page_name`, `profile_content_id`, `entry_date`, `entry_by`,
              `last_update_date`, `user_page`, `website`, `about`, `personal_message`,
              `posisi`, `prestasi`, `prestasi_lain`, `contact_info`, `rating`, `publish`,
              `partai_id`, `node_size`, `node_color`
             FROM v_usr_politik;
        '''

        dic = []
        con = self.engine.connect()
        try:

            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getsurvey(self, where='1 = 1'):

        query = '''
            SELECT s.*, t.`content_id`
            FROM survey s
            LEFT JOIN tcontent t ON t.`location` = s.`survey_id` AND t.`content_group_type` = 54
            where %s
            ORDER BY s.begin_date DESC;
        ''' % where

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getsurveyquestion(self, survey_id):
        query = '''
            SELECT * FROM question WHERE survey_id = %s
            ORDER BY question_id ASC
        ''' % survey_id

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getsurveyquestionoption(self, question_option_id):
        query = '''
            SELECT * FROM question_option WHERE question_id	= %s ORDER BY question_option_id ASC;
        ''' % question_option_id

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getsurveyquestionanswer(self, question_id, question_option_id):
        query = '''
            SELECT * FROM question_answer WHERE question_id = %s AND question_option_id = %s ORDER BY answer_date DESC;
        ''' % (question_id, question_option_id)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getnewslisttopic(self, topic_id=0):
        wh = '1=1'
        if topic_id != 0:
            wh = 'topic_id = %s' % topic_id
        query = '''
            SELECT tt.content_id, tt.news_id, t.entry_date, tht.topic_id
            FROM tcontent_text tt
            JOIN tcontent t ON t.`content_id` = tt.`content_id`
            JOIN tcontent_has_topic tht ON tht.`content_id` = t.`content_id`
            WHERE %s
            ORDER BY t.`entry_date` DESC limit 1000 offset 0;
        ''' % wh

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()

        return dic


    def getProvince(self):
        query = '''
            select * from master_province
            '''

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getDapil(self, province_id):
        query = '''
            select * from master_dapil where province_id = %s
            ''' % province_id

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getPartai(self, province_id):
        query = '''
            SELECT p.partai, p.partai_id, tp.`profile_content_id`, tpa.alias,ta.`attachment_title` FROM structure_caleg AS p
LEFT JOIN tobject_page tp ON tp.`page_id` = p.`partai_id`
LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tp.`profile_content_id`
LEFT JOIN tusr_account tpa on tpa.user_id = tp.page_id
WHERE province_id = %s group by partai_id
            ''' % province_id

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic


    def getCaleg(self, province_id, dapil_id, partai_id):
        query = '''
            SELECT p.partai, p.partai_id, p.caleg, p.caleg_id, tp.`profile_content_id`, ta.`attachment_title`, left(tp.about, 200) as about,
tp2.`profile_content_id` AS partai_profile_content_id, ta2.`attachment_title` AS partai_attachment_title
FROM structure_caleg AS p
LEFT JOIN tobject_page tp ON tp.`page_id` = p.`caleg_id`
LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tp.`profile_content_id`
LEFT JOIN tobject_page tp2 ON tp2.`page_id` = p.`partai_id`
LEFT JOIN tcontent_attachment ta2 ON ta2.`content_id` = tp2.`profile_content_id`
 WHERE province_id = %s AND dapil_id = %s AND p.partai_id = '%s';
            ''' % (province_id, dapil_id, partai_id)


        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getCalegScandal(self, partai_id):
        query = '''
            select count(*) co from tscandal_player where page_id = '%s';
            ''' % (partai_id)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic

    def getCalegNews(self, partai_id):
        query = '''
            select count(*) co from tcontent_has_politic where page_id = '%s';
            ''' % (partai_id)

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
            #    print "Connection was invalidated!"

        finally:
            con.close()


        return dic


    def getBlogType(self, typeblog):
        if typeblog == 'latest':
            ordby = ' ORDER BY tc.entry_date desc '

        if typeblog == 'favorit':
            ordby = ' ORDER BY count_like desc '

        query = '''
           SELECT tc.content_id FROM tcontent tc LEFT JOIN
				tcontent_blog tb on tc.content_id = tb.content_id
				LEFT join tobject_page tp on tp.page_id = tc.page_id
				LEFT JOIN tcontent_attachment ta ON ta.content_id = tp.profile_content_id
				WHERE tc.content_group_type='11' and tc.status ='0' %s LIMIT 1000 offset 0;
		   ''' % ordby
        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
                #print "Connection was invalidated!"

        finally:
            con.close()

        return dic


    def getBlog(self, content_id=0):
        wh = ''
        if(content_id != 0):
            wh = 'and tc.content_id = %s' % content_id

        query = '''
           SELECT tc.content_id, tc.account_id, tc.page_id, tp.page_name, ta.attachment_title page_attachment, title,
				tc.entry_date,
				(
					SELECT attachment_title from tcontent_attachment where content_id =
					(SELECT content_id FROM tcontent WHERE group_content_id = tc.content_id ORDER BY entry_date DESC LIMIT 1 )
					Limit 1
				) attachment_title, tc.count_like,
				tb.content_blog FROM
				tcontent tc LEFT JOIN
				tcontent_blog tb on tc.content_id = tb.content_id
				LEFT join tobject_page tp on tp.page_id = tc.page_id
				LEFT JOIN tcontent_attachment ta ON ta.content_id = tp.profile_content_id
				WHERE tc.content_group_type='11' and tc.status ='0'  %s;
		   ''' % wh

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
                #print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getCommentList(self):
        query = '''
           SELECT tcc.comment_id
                  FROM tcontent_comment tcc
                  LEFT JOIN tusr_account tua ON tcc.account_id = tua.account_id
                  LEFT JOIN tcontent tc ON tc.content_id = tcc.content_id
                  LEFT JOIN tcontent_text tct ON tc.content_id = tct.content_id
                  LEFT JOIN tm_content_group_type tgt ON tc.content_group_type = tgt.content_group_type
                  LEFT JOIN tobject_page tp ON tp.page_id = tua.user_id
                  LEFT JOIN tcontent_attachment tca ON tca.content_id = tp.profile_content_id
                  WHERE tcc.status ='0'
                  AND tgt.content_group_name IN ('STATUS', 'BLOG', 'NEWS', 'SCANDAL','SURVEY', 'RACE')
                  ORDER BY tcc.entry_date DESC LIMIT 0, 500;
		   '''
        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
                #print "Connection was invalidated!"

        finally:
            con.close()

        return dic

    def getComment(self, content_id=0):
        wh = ''
        if(content_id != 0):
            wh = 'and tcc.comment_id = %s' % content_id

        query = '''
           SELECT tcc.comment_id, tcc.account_id, tua.user_id, tcc.content_id, tcc.entry_date, tp.page_id, tp.page_name, tca.attachment_title,
                  IFNULL(tua.account_type, 1) account_type, tc.title, tc.content_group_type, tgt.content_group_name,
                  tc.location, tct.news_id, tct.content_url, LEFT(tcc.text_comment, 80) text_comment
           FROM tcontent_comment tcc
           LEFT JOIN tusr_account tua ON tcc.account_id = tua.account_id
           LEFT JOIN tcontent tc ON tc.content_id = tcc.content_id
           LEFT JOIN tcontent_text tct ON tc.content_id = tct.content_id
           LEFT JOIN tm_content_group_type tgt ON tc.content_group_type = tgt.content_group_type
           LEFT JOIN tobject_page tp ON tp.page_id = tua.user_id
           LEFT JOIN tcontent_attachment tca ON tca.content_id = tp.profile_content_id
           WHERE tcc.status ='0' AND tgt.content_group_name IN ('STATUS', 'BLOG', 'NEWS', 'SCANDAL','SURVEY', 'RACE')
           %s;
		   ''' % wh

        dic = []
        try:
            con = self.engine.connect()
            result = con.execute(query)

            for index, row in enumerate(result):
                #print index
                di = dict()
                for column, val in row.items():
                    vals = str(val).decode('ascii','ignore')
                    di[column] = str(vals)
                dic.append(di)

        except exc.DBAPIError, e:
            pass
            #if e.connection_invalidated:
                #print "Connection was invalidated!"

        finally:
            con.close()

        return dic
