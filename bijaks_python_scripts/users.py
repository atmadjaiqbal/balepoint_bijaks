__author__ = 'alam'
import os
import sys
import threading
import time
import redis
import ConfigParser
import Queue
import simplejson as json

from content import Content
from profile import Profile

from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb
from libs.komunitas_db import KomunitasDb

pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

queue = Queue.Queue()
out_queue = Queue.Queue()

r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Users(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        self.komunitas_db = KomunitasDb(Config)
        self.content = Content()
        self.profile = Profile()
        pass

    def user_wall_list(self, account_id=0):
        users = self.komunitas_db.get_users(account_id)
        pipe = r_master.pipeline(transaction=False)
        for row, user in enumerate(users):
            # get last key wall by user
            last_key = r_slave.get('wall:user:key:%s' % user['account_id'])
            if(last_key == None):
                last_key = 0

            rkey = int(last_key) + 1

            last_list = self.komunitas_db.get_wall_list(user['account_id'], user['user_id'], 1000)
            #print last_list
            for ls, content in enumerate(last_list):

                if int(content['content_group_type']) == 40:
                    content['photo'] = self.komunitas_db.get_foto_album(content['content_id'])

                pipe.rpush('wall:user:list:%s:%s' % (rkey, user['account_id']), content['content_id'])
                content_json = json.dumps(content)
                pipe.set('wall:user:detail:%s' % content['content_id'], content_json)

            r_master.incr('wall:user:key:%s' % user['account_id'])
            #pipe.delete('wall:user:list:%s:%s' % (last_key, user['account_id']))

        pipe.execute()

    def user_wall_relist(self, account_id=0):
        users = self.komunitas_db.get_friend_users(account_id)
        #print users
        pipe = r_master.pipeline(transaction=False)
        for row, user in enumerate(users):
            # get last key wall by user
            last_key = r_slave.get('wall:user:key:%s' % user['friend_account_id'])
            if(last_key == None):
                last_key = 0

            rkey = int(last_key) + 1

            last_list = self.komunitas_db.get_wall_list(user['account_id'], user['user_id'], 1000)
            #print rkey
            for ls, content in enumerate(last_list):
                #print content['content_id']
                pipe.rpush('wall:user:list:%s:%s' % (rkey, user['friend_account_id']), content['content_id'])

            r_master.incr('wall:user:key:%s' % user['friend_account_id'])
            pipe.delete('wall:user:list:%s:%s' % (last_key, user['friend_account_id']))

        pipe.execute()

    def user_wall_reset(self, account_id=0):
        users = self.komunitas_db.get_friend_users(account_id)
        #print users
        pipe = r_master.pipeline(transaction=False)
        for row, user in enumerate(users):

            last_list = self.komunitas_db.get_wall_list(user['friend_account_id'], user['page_id'], 1000)
            #print rkey
            for ls, content in enumerate(last_list):

                if int(content['content_group_type']) == 40:
                    content['photo'] = self.komunitas_db.get_foto_album(content['content_id'])

                content_json = json.dumps(content)
                pipe.set('wall:user:detail:%s' % content['content_id'], content_json)


        pipe.execute()

    def user_content_comment_reset(self, account_id=0):
        comment = self.komunitas_db.get_content_is_comment(account_id)
        for row, val in enumerate(comment):
            self.content.content_comment(val['content_id'])

    def user_page_comment_reset(self, account_id=0):
        comment = self.komunitas_db.get_page_is_comment(account_id)
        for row, val in enumerate(comment):
            self.profile.profile_comment_reset(val['page_id'])

    def doRun(self):
        all_content = self.timeline_db.getlogchanged('tipe = \'wall\'')
        for index, val in enumerate(all_content):
            if(int(val['post_type']) == 3):
                self.user_wall_relist(val['id'])
            elif(int(val['post_type']) == 1):
                self.user_wall_reset(val['id'])
                self.user_content_comment_reset(val['id'])
                self.user_page_comment_reset(val['id'])

            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))
