# -*- coding: utf-8 -*-
import os
import sys
import time
import redis
import ConfigParser
import Queue
import simplejson as json

from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb

pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

#url = 'http://news.bijaks.net/mobile/get_post/?dev=1&id='
url = 'http://news.bijaks.net/mobile/get_post/?id='

queue = Queue.Queue()
out_queue = Queue.Queue()


r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Suksesi(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass


    def suksesi_last_activity(self):

        last_list = self.db.getsuksesilastactivity()
        pipe = r_master.pipeline(transaction=False)

        ps = json.dumps(last_list)
        pipe.set('suksesi:last:activity', ps)
        pipe.execute()


    def suksesi_detail(self, suksesi_id=0):
        pipe = r_master.pipeline(transaction=False)
        suksesi = self.db.getsuksesi(suksesi_id)
        for index, val in enumerate(suksesi):
            #if index == 2:
            #    break

            sengketa = self.db.getsuksesisengketa(val['id_race'])
            val['sengketa'] = sengketa
            photos = self.db.getsuksesiphoto(val['id_race'])
            val['photos'] = photos
            #suksesi[index]['photos'] = photos
            status = self.db.getsuksesistatus(val['id_race'])
            #suksesi[index]['status'] = status
            val['status'] = list()
            for i, row in enumerate(status):
                val['status'].append(row)
                lembaga = self.db.getsuksesilembaga(row['id_trace_status'])
                kandidat = self.db.getsuksesikandidat(row['id_trace_status'])
                val['status'][i]['lembaga'] = list()
                val['status'][i]['kandidat'] = kandidat
                for k, l in enumerate(lembaga):
                    val['status'][i]['lembaga'].append(l)
                    val['status'][i]['lembaga'][k]['kandidat'] = list()
                    for m, kdt in enumerate(kandidat):
                        kandidat_pendukung = self.db.getsuksesikandidatpendukung(kdt['idtrace_kandidat'])
                        score = self.db.getsuksesiscore(l['id_trace_lembaga'], kdt['idtrace_kandidat'])
                        vn = dict()
                        vn['kandidat'] = kdt
                        vn['kandidat_pendukung'] = kandidat_pendukung
                        vn['score'] = score[0] if len(score) > 0 else dict()
                        val['status'][i]['lembaga'][k]['kandidat'].append(vn)

                #status[i]['lembaga'] = lembaga
                #status[i]['kandidat'] = kandidat

                #status[i]['score'] = list()
                #for k, m in enumerate(lembaga):
                #    status[i]['score'].append( m )
                #    status[i]['score'][k]['kandidat'] = list()
                #    for l, o in enumerate(kandidat):
                #        status[i]['score'][k]['kandidat'].append(o)
                #        status[i]['score'][k]['kandidat'][l]['score'] = dict()
                #        score = self.db.getsuksesiscore(m['id_trace_lembaga'], o['idtrace_kandidat'])
                #        status[i]['score'][k]['kandidat'][l]['score'] = score

                #for n, didat in enumerate(kandidat):
                #    kandidat_pendukung = self.db.getsuksesikandidatpendukung(didat['idtrace_kandidat'])
                #    val['status'][i]['kandidat_pendukung'] = kandidat_pendukung


            suk = json.dumps(val)
            pipe.set('suksesi:detail:%s' % val['id_race'], suk)


        pipe.execute()


    def get_suksesi_list_headline(self):
        all_content = self.timeline_db.getsuksesilisthome(where='headline = 1', order='head_date')
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('suksesi:list:headline')
        for index, val in enumerate(all_content):
            pipe.rpush('suksesi:list:headline', val['id_race'])

        pipe.execute()

    def get_suksesi_list_hot(self):
        all_content = self.timeline_db.getsuksesilisthome(where='hotlist = 1', order='hot_date')
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('suksesi:list:hot')
        for index, val in enumerate(all_content):
            pipe.rpush('suksesi:list:hot', val['id_race'])

        pipe.execute()


    def get_suksesi_list(self):
        all_content = self.timeline_db.getsuksesilist()
        pipe = r_master.pipeline(transaction=False)

        pipe.delete('suksesi:list:all')
        for index, val in enumerate(all_content):
            pipe.rpush('suksesi:list:all', val['id_race'])

        pipe.execute()

    def doRun(self):
        all_content = self.timeline_db.getlogchanged('tipe = \'suksesi\'')
        #print all_content
        for index, val in enumerate(all_content):
            self.suksesi_detail(val['id'])
            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))

        self.get_suksesi_list()


