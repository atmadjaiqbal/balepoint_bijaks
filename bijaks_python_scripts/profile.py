# -*- coding: utf-8 -*-
import os
import sys
import threading
import time
import redis
import pickle
import ConfigParser
import Queue
import simplejson as json


from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb

pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

#url = 'http://news.bijaks.net/mobile/get_post/?dev=1&id='
url = 'http://news.bijaks.net/mobile/get_post/?id='

queue = Queue.Queue()
out_queue = Queue.Queue()


r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Profile(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass


    def profile_last_activity(self):
        last_list = self.db.getprofilelastactivity()
        pipe = r_master.pipeline(transaction=False)
        for index, val in enumerate(last_list):
            #print index, val['profile_id']
            ps = json.dumps(val)
            #print index, ps
            pipe.set('profile:last:activity:%s' % str(val['profile_id']), ps)

        pipe.execute()

    def profile_last_activity_header(self):
        last_list = self.db.getprofilelastactivity(1)
        #print 'ok %s' % last_list
        pipe = r_master.pipeline(transaction=False)
        #pipe.delete('profile:last:activity')
        #for index, val in enumerate(last_list):
            #print index, val['profile_id']
        ps = json.dumps(last_list)
            #print index, ps
        pipe.set('profile:last:activity', ps)

        pipe.execute()

    def profile_comment(self):
        all_content = self.db.getprofile()
        pipe = r_master.pipeline(transaction=False)
        for index, val in enumerate(all_content):
            page_id = val['page_id'].replace("'", "\\'")
            pipe.delete('profile:comment:list:%s' % page_id)
            comment = self.db.getprofilecomment(page_id)
            for rw, vl in enumerate(comment):
                comm = json.dumps(vl)
                pipe.rpush('profile:comment:list:%s' % page_id, vl['page_comment_id'])
                pipe.set('profile:comment:detail:%s' % vl['page_comment_id'], comm)

            #ps = json.dumps(comment)
            #pipe.set('profile:comments:%s' % page_id, ps)

        pipe.execute()

    def profile_comment_reset(self, page_id):
        all_content = self.db.getprofile()
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('profile:comment:list:%s' % page_id)
        comment = self.db.getprofilecomment(page_id)
        for rw, vl in enumerate(comment):
            comm = json.dumps(vl)
            pipe.rpush('profile:comment:list:%s' % page_id, vl['page_comment_id'])
            pipe.set('profile:comment:detail:%s' % vl['page_comment_id'], comm)

            #ps = json.dumps(comment)
            #pipe.set('profile:comments:%s' % page_id, ps)

        pipe.execute()

    def profile(self, where = 0):
        pipe = r_master.pipeline(transaction=False)
        profile = self.db.getprofile(where)
        #print len(profile)
        for i, val in enumerate(profile):
            #if i == 100:
            #    break

            page_id = val['page_id'].replace("'", "\\'")
            skandal = self.db.getskandalbyplayer(page_id)
            val['scandal'] = skandal

            wall_photo = self.db.getcontentfoto(page_id, 20)
            profile_photo = self.db.getcontentfoto(page_id, 22)
            scandal_photo = self.db.getcontentfoto(page_id, 60)
            race_photo = self.db.getcontentfoto(page_id, 61)
            headline_photo = self.db.getcontentfotoheadline(page_id)
            hotlist_photo = self.db.getcontentfotohotlist(page_id)

            val['wall_photo'] = wall_photo
            val['profile_photo'] = profile_photo
            val['scandal_photo'] = scandal_photo
            val['race_photo'] = race_photo
            val['headline_photo'] = headline_photo
            val['hotlist_photo'] = hotlist_photo

            content_album = self.db.getcontentalbum(page_id)
            for m, alb in enumerate(content_album):
                content_id = alb['content_id'].replace("'", "\\'")
                album_item = self.db.getcontentfotoalbum(content_id)
                alb['photo'] = album_item

            val['album_photo'] = content_album
            value = json.dumps(val)
            pipe.set('profile:detail:%s' % page_id, value)
            value_skandal = json.dumps(skandal)
            pipe.set('profile:skandal:%s' % page_id, value_skandal)

        pipe.execute()

    def wall_photo(self, page_id):
        wall_photo = self.db.getcontentfoto(page_id, 20)
        if len(wall_photo) > 0:
            pipe = r_master.pipeline(transaction=False)
            pipe.set('profile:photo:wall:%s' % page_id, json.dumps(wall_photo))
            pipe.execute()

    def profile_photo(self, page_id):
        profile_photo = self.db.getcontentfoto(page_id, 22)
        if len(profile_photo) > 0:
            pipe = r_master.pipeline(transaction=False)
            pipe.set('profile:photo:profile:%s' % page_id, json.dumps(profile_photo))
            pipe.execute()

    def scandal_photo(self, page_id):
        scandal_photo = self.db.getcontentfoto(page_id, 60)
        if len(scandal_photo) > 0:
            pipe = r_master.pipeline(transaction=False)
            pipe.set('profile:photo:scandal:%s' % page_id, json.dumps(scandal_photo))
            pipe.execute()

    def race_photo(self, page_id):
        race_photo = self.db.getcontentfoto(page_id, 61)
        if len(race_photo) > 0:
            pipe = r_master.pipeline(transaction=False)
            pipe.set('profile:photo:race:%s' % page_id, json.dumps(race_photo))
            pipe.execute()

    def headline_photo(self, page_id):
        headline_photo = self.db.getcontentfoto(page_id, 70)
        if len(headline_photo) > 0:
            pipe = r_master.pipeline(transaction=False)
            pipe.set('profile:photo:headline:%s' % page_id, json.dumps(headline_photo))
            pipe.execute()

    def hotlist_photo(self, page_id):
        hotlist_photo = self.db.getcontentfoto(page_id, 71)
        if len(hotlist_photo) > 0:
            pipe = r_master.pipeline(transaction=False)
            pipe.set('profile:photo:hot:%s' % page_id, json.dumps(hotlist_photo))
            pipe.execute()


    def get_profile_list_hot(self):
        all_content = self.timeline_db.getprofilelisthome(limit=50)
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('profile:list:hit')
        for index, val in enumerate(all_content):
            pipe.rpush('profile:list:hit', val['page_id'])
        pipe.execute()

    def get_institusi_list_hot(self):
        all_content = self.timeline_db.getinstitusilisthome(limit=50)
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('institusi:list:hot')
        for index, val in enumerate(all_content):
            pipe.rpush('institusi:list:hot', val['page_id'])
        pipe.execute()

    def get_profile_list_headline(self):
        #print 'please wait...'
        all_content = self.timeline_db.getprofilelistheadline(where=2)
        pipe = r_master.pipeline(transaction=False)
        starttime=time.clock()
        pipe.delete('profile:list:headline')
        for index, val in enumerate(all_content):
            pipe.rpush('profile:list:headline', val['page_id'])

        pipe.execute()
        #stoptime=time.clock()
        #print "success, execution time %.3f seconds" % (stoptime-starttime)

    def doRun(self):
        all_content = self.timeline_db.getlogchanged('tipe = \'profile\'')
        for index, val in enumerate(all_content):
            #print val
            if (int(val['post_type']) == 3):
                pipe = r_master.pipeline(transaction=False)
                pipe.delete('profile:detail:%s' % val['id'])
                pipe.delete('profile:skandal:%s' % val['id'])
                pipe.delete('profile:photo:wall:%s' % val['id'])
                pipe.delete('profile:photo:scandal:%s' % val['id'])
                pipe.delete('profile:photo:hot:%s' % val['id'])
                pipe.delete('profile:photo:headline:%s' % val['id'])
                pipe.delete('profile:photo:profile:%s' % val['id'])
                pipe.delete('profile:photo:race:%s' % val['id'])
                pipe.execute()
            else:
                self.profile(val['id'])
                self.profile_photo(val['id'])
                self.wall_photo(val['id'])
                self.scandal_photo(val['id'])
                self.hotlist_photo(val['id'])
                self.race_photo(val['id'])
                self.headline_photo(val['id'])

            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))

