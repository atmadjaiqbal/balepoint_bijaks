# -*- coding: utf-8 -*-
import os
import sys
import time
import redis
import ConfigParser
import simplejson as json

import logging

from scandal import Scandal
from news import News
from profile import Profile
from suksesi import Suksesi
from survey import Survey
from content import Content
from struktur import Struktur
from users import Users
from caleg import Caleg
from blog import Blog
from comment import Comment

from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb

pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Populate(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass


    def doRun(self):

        #exec_what = r_slave.get('key:exec')

        if(r_slave.get('key:exec:user') == 'true'):
            user = Users()
            user.user_wall_list()

            r_master.set('key:exec:user', 'false')

        if(r_slave.get('key:exec:news') == 'true'):
            news = News()
            #news.news(0)
            #news.news(1, 1)
            #news.news(3, 1)
            #news.news(7, 1)
            #news.news(9, 1)
            #news.news(4, 1)
            #news.news(10, 1)
            #news.news(12, 1)
            #news.news(17, 1)
            #news.news(18, 1)
            #news.news(19, 1)
            #news.news(8, 1)
            news.news(1150, 1)
            #news.news(1151, 1)
            #news.news(1152, 1)
            news.news(347, 1)       
            #news.news(1221, 1)
            #news.news(1222, 1)
            #news.news(1223, 1)

            #news.new_list()
            #news.get_news_list_headline()
            #news.topic_last_activity()

            r_master.set('key:exec:news', 'false')

            #news.new_list()
            #news.get_news_list_headline()
            #news.topic_last_activity()

        if(r_slave.get('key:exec:profile') == 'true'):
            profile = Profile()
            #profile.profile()
            #profile.profile_comment()
            #profile.profile_last_activity_header()
            profile.get_profile_list_hot()
            profile.get_institusi_list_hot()
            #profile.get_profile_list_headline()

            r_master.set('key:exec:profile', 'false')

        if(r_slave.get('key:exec:scandal') == 'true'):
            scandal = Scandal()
            scandal.scandal_detail()
            scandal.get_scandal_list()
            scandal.get_scandal_list_headline()
            scandal.get_scandal_list_hot()
            scandal.scandal_last_activity()

            r_master.set('key:exec:scandal', 'false')

        if(r_slave.get('key:exec:suksesi') == 'true'):
            suksesi = Suksesi()
            suksesi.suksesi_detail()
            suksesi.get_suksesi_list()
            suksesi.get_suksesi_list_headline()
            suksesi.get_suksesi_list_hot()
            suksesi.suksesi_last_activity()

            r_master.set('key:exec:suksesi', 'false')

        if(r_slave.get('key:exec:survey') == 'true'):
            survey = Survey()
            survey.survey()
            survey.survey_list()
            survey.get_survey_list_hot()
            survey.survey_last_activity()

            r_master.set('key:exec:survey', 'false')

        if(r_slave.get('key:exec:content') == 'true'):
            content = Content()
            #content.home_total()
            #content.content_last_activity()
            content.content_comment()
            content.content_score()

            r_master.set('key:exec:content', 'false')

        if(r_slave.get('key:exec:struktur') == 'true'):
            struktur = Struktur()
            #struktur.structure_group(26176)
            struktur.caleg_row()

            r_master.set('key:exec:struktur', 'false')

        if(r_slave.get('key:exec:caleg') == 'true'):
            caleg = Caleg()
            caleg.detail()

            r_master.set('key:exec:caleg', 'false')

        if(r_slave.get('key:exec:blog') == 'true'):
            blog = Blog()
            blog.get_blog_list_latest()
            blog.get_blog_latest()
            blog.get_topten_list()
            blog.get_topten_detail()
            blog.get_event_list()
            blog.get_event_detail()            
          
            r_master.set('key:exec:blog', 'false')

        if(r_slave.get('key:exec:comment') == 'true'):
            comment = Comment()
            comment.get_comment_list()
            comment.get_comment()

            r_master.set('key:exec:comment', 'false')


