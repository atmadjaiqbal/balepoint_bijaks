import logging
import time
from croniter import croniter
from datetime import datetime

#third party libs

class Demon():

    def __init__(self, obj, pid_name, logger, cron, secc):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pid_name = pid_name
        self.pidfile_path =  '/var/run/bijaks/%s.pid' %pid_name
        self.pidfile_timeout = 5
        self.logger = logger
        self.obj = obj
        self.dt = time.localtime()
        self.base = datetime.now()
        self.cron = cron
        self.cron_sced = croniter(cron, self.base)
        self.next_cron = self.cron_sced.get_next(datetime)
        self.sec = secc

    def run(self):
        while True:
            try:
                if(self.sec == 0):
                    time.sleep(1)
                    self.dt = time.localtime()
                    strtime = time.strftime("%Y-%m-%d %H:%M:%S", self.dt)
                    #self.logger.info("%s loop" % self.pid_name)
                    if strtime == str(self.next_cron):
                        #self.logger.info("Start execute %s" % (self.pid_name))
                        start_time=time.clock()
                        self.obj.doRun()
                        stop_time=time.clock()
                        msg = "success, execution time %.3f seconds" % (stop_time-start_time)
                        #self.logger.info("%s %s" % (self.pid_name, msg))
                        self.base = datetime.now()
                        self.cron_sced = croniter(self.cron, self.base)
                        self.next_cron = self.cron_sced.get_next(datetime)
                else:
                    time.sleep(self.sec)
                    #self.logger.info("Start execute %s" % (self.pid_name))
                    start_time=time.clock()
                    self.obj.doRun()
                    stop_time=time.clock()
                    msg = "success, execution time %.3f seconds" % (stop_time-start_time)
                    self.logger.info("%s %s" % (self.pid_name, msg))


            except Exception, e:
                self.logger.error( 'error : %s , %s' % (self.pid_name, e.message))
                continue


