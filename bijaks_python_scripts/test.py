#!/usr/local/bin/python 
# Author  : Fajri Abdillah a.k.a clasense4
# Twitter : @clasense4
# mail    : clasense4@gmail.com

import demon from Demon 
import croniter
import datetime
import time
 
now = datetime.datetime.now()
now2 = time.mktime(now.timetuple())
sched = '*/20 * * * *' # every 20 minutes
cron = croniter.croniter(sched,now)
 
print "Next Cron job : %s (Seconds) | Now : %s " % ( cron.get_next() - time.mktime(now.timetuple()), datetime.datetime.now()) #to next 20 minutes
#print "Next Cron job : %s (Minutes) | Now : %s " % ( datetime.timedelta(seconds=cron.get_next() - time.mktime(now.timetuple())), datetime.datetime.now())
