__author__ = 'alam'
# -*- coding: utf-8 -*-
import os
import sys
import time
import redis
import ConfigParser
import simplejson as json

import logging

from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb
pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Blog(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass

    def get_blog_list_latest(self):
        all_content = self.db.getBlogType('latest')
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('blog:latest:list')
        for index, val in enumerate(all_content):
            pipe.rpush('blog:latest:list', val['content_id'])
        pipe.execute()

    def get_blog_latest(self, content_id=0):
        blog_content = self.db.getBlog(content_id)
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('blog:latest:detail')
        for index, val in enumerate(blog_content):
            json_blog = json.dumps(val)
            pipe.set('blog:latest:detail:%s' % val['content_id'], json_blog)
        pipe.execute()

    def get_topten_list(self):
        all_content = self.timeline_db.getheadtopten()
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('topten:list')
        for index, val in enumerate(all_content):
            pipe.rpush('topten:list', val['id_topten_category'])
        pipe.execute()

    def get_topten_detail(self, topten_id=0):
        all_content = self.timeline_db.getheadtoptendetail(topten_id)
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('topten:detail')
        for index, val in enumerate(all_content):
            json_topten = json.dumps(val)
            pipe.set('topten:detail:%s' % val['id_topten_category'], json_topten)
        pipe.execute()

    def get_event_list(self):
        all_content = self.timeline_db.getcontentevent()
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('event:list')
        for index, val in enumerate(all_content):
            pipe.rpush('event:list', val['event_id'])
        pipe.execute()

    def get_event_detail(self, event_id=0):
        all_content = self.timeline_db.getcontenteventdetail(event_id)
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('event:detail')
        for index, val in enumerate(all_content):
            json_event = json.dumps(val)
            pipe.set('event:detail:%s' % val['event_id'], json_event)
        pipe.execute()

    def doRun(self):
        blog_content = self.timeline_db.getlogchanged('tipe = \'blog\'')
        for index, val in enumerate(blog_content):
            self.get_blog_list_latest()
            self.get_blog_latest()
            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))

        ticker_content = self.timeline_db.getlogchanged('tipe = \'ticker\'')
        for index, val in enumerate(ticker_content):
            self.get_topten_list()
            self.get_topten_detail()
            self.get_event_list()
            self.get_event_detail()
            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))

      
