# -*- coding: utf-8 -*-
import logging
import os
import sys

pth = os.path.dirname(os.path.abspath(__file__))

from demon import Demon

from daemon import runner

from scandal import Scandal
from struktur import Struktur
from suksesi import Suksesi
from survey import Survey
from blog import Blog
from comment import Comment
from cron.home import Home
from cron.populate import Populate as Popul
from populate import Populate
from cron.activity import Activity
from profile import Profile
from news import News
from content import Content
from users import Users

from caleg import Caleg

logger = logging.getLogger("DaemonLog")
handler = logging.FileHandler("/var/log/bijaks/bijaks.log")

def set_logger():
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

def scandal_daemon():
    set_logger()
    scandal = Scandal()
    app = Demon(scandal, 'scandal', logger, '* * * * *', 10)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def suksesi_daemon():
    set_logger()
    suksesi = Suksesi()
    app = Demon(suksesi, 'suksesi', logger, '* * * * *', 15)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def survey_daemon():
    set_logger()
    survey = Survey()
    app = Demon(survey, 'survey', logger, '* * * * *', 15)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def profile_daemon():
    set_logger()
    profile = Profile()
    app = Demon(profile, 'profile', logger, '*/5 * * * *', 1)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def news_daemon():
    set_logger()
    news = News()
    app = Demon(news, 'news', logger, '* * * * *', 5)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def content_daemon():
    set_logger()
    content = Content()
    app = Demon(content, 'content', logger, '*/1 * * * *', 1)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def home_daemon():
    set_logger()
    home = Home()
    app = Demon(home, 'home', logger, '*/5 * * * *', 1)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def activity_daemon():
    set_logger()
    activity = Activity()
    app = Demon(activity, 'activity', logger, '*/5 * * * *', 1)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def user_daemon():
    set_logger()
    user = Users()
    app = Demon(user, 'user', logger, '* * * * *', 1)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def populate_daemon():
    set_logger()
    populate = Populate()
    app = Demon(populate, 'populate', logger, '* * * * *', 1)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def struktur_daemon():
    set_logger()
    struktur = Struktur()
    app = Demon(struktur, 'struktur', logger, '0 */1 * * *', 0)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def blog_daemon():
    set_logger()
    blogs = Blog()
    app = Demon(blogs, 'blogs', logger, '*/15 * * * *', 0)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()

def comment_daemon():
    set_logger()
    comment = Comment()
    app = Demon(comment, 'comment', logger, '*/15 * * * *', 0)
    daemon_runner = runner.DaemonRunner(app)

    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()
	

if __name__ == '__main__':
#    sched.add_interval_job(job_function, minutes=1)

    if len(sys.argv) == 3:
        argp = sys.argv[2]
        print argp
        if argp == 'scandal':
            scandal_daemon()
        elif argp == 'home':
            home_daemon()
        elif argp == 'populate':
            populate_daemon()
        elif argp == 'suksesi':
            suksesi_daemon()
        elif argp == 'activity':
            activity_daemon()
        elif argp == 'survey':
            survey_daemon()
        elif argp == 'news':
            news_daemon()
        elif argp == 'struktur':
            struktur_daemon()
        elif argp == 'content':
            content_daemon()
        elif argp == 'user':
            user_daemon()
        elif argp == 'profile':
            profile_daemon()
        elif argp == 'blogs':
            blog_daemon()
        elif argp == 'comment':
        	  comment_daemon()    
    else:
        #populate = Popul()
        #populate.doRun()

        news = News()
        #news.new_list()
        news.doRun()

        #caleg = Caleg()
        #caleg.detail()

        #scandal = Scandal()
        #scandal.doRun()

        #profile = Profile()
        #profile.doRun()
        #profile.profile()
        #profile.get_profile_list_hot()

        #news = News()
        #news.doRun()
        #news.news()

        #structure = Struktur()
        #structure.structure_group()
