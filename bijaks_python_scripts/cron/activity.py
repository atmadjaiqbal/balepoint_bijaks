__author__ = 'alam'
from scandal import Scandal
from content import Content
from news import News
from profile import Profile
from suksesi import Suksesi
from survey import Survey

class Activity(object):
    def __init__(self):
        pass

    def doRun(self):
        scandal = Scandal()
        scandal.scandal_last_activity()

        content = Content()
        content.content_last_activity()

        news = News()
        news.topic_last_activity()

        profile = Profile()
        profile.profile_last_activity()

        suksesi = Suksesi()
        suksesi.suksesi_last_activity()

        survey = Survey()
        survey.survey_last_activity()