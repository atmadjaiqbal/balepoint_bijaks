__author__ = 'alam'
from scandal import Scandal
from suksesi import Suksesi
from news import News
from profile import Profile
from survey import Survey
from content import Content

class Home(object):
    def __init__(self):
        pass

    def doRun(self):
        scancal = Scandal()
        scancal.get_scandal_list_headline()
        scancal.get_scandal_list_hot()

        suksesi = Suksesi()
        suksesi.get_suksesi_list_headline()
        suksesi.get_suksesi_list_hot()

        news = News()
        news.get_news_list_headline()

        profile = Profile()
        profile.get_profile_list_headline()
        profile.get_profile_list_hot()

        survey = Survey()
        survey.get_survey_list_hot()

        content = Content()
        content.home_last_activity()
        content.home_total()