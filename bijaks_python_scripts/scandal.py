# -*- coding: utf-8 -*-
import os
import sys
import time
import redis
import ConfigParser
import simplejson as json

import logging

from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb

pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class Scandal(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass


    def scandal_detail(self, scandal_id=0, main_scandal_id=0):
        pipe = r_master.pipeline(transaction=False)
        skandal = self.db.getskandal(scandal_id, main_scandal_id)
        print skandal
        for index, val in enumerate(skandal):
            #if index == 1:
            #    break

            kronologi = self.db.getskandal(0, val['scandal_id'])
            json_kronologi = json.dumps(kronologi)
            pipe.set('scandal:kronologi:%s' % val['scandal_id'], json_kronologi)

            photos = self.db.getskandalphoto(val['scandal_id'])
            json_photo = json.dumps(photos)
            pipe.set('scandal:photo:%s' % val['scandal_id'], json_photo)

            players = self.db.getskandalplayer(val['scandal_id'])
            json_players = json.dumps(players)
            pipe.set('scandal:players:%s' % val['scandal_id'], json_players)

            relations = self.db.getskandalrelations(val['scandal_id'])
            json_relations = json.dumps(relations)
            pipe.set('scandal:relations:%s' % val['scandal_id'], json_relations)

            sk = json.dumps(val)
            pipe.set('scandal:min:%s' % val['scandal_id'], sk)

        pipe.execute()

    def scandal_min(self, scandal_id=0, main_scandal_id=0):
        pipe = r_master.pipeline(transaction=False)
        skandal = self.db.getskandal(scandal_id, main_scandal_id)

        for index, val in enumerate(skandal):
            #if index == 1:
            #    break
            json_val = json.dumps(val)
            pipe.set('scandal:min:%s' % val['scandal_id'], json_val)

        pipe.execute()


    def scandal_kronologi(self, id=0):
        pipe = r_master.pipeline(transaction=False)
        kronologi = self.db.getskandal(0, id)

        json_kronologi = json.dumps(kronologi)
        pipe.set('scandal:kronologi:%s' % id, json_kronologi)

        pipe.execute()


    def scandal_players(self, id=0):
        pipe = r_master.pipeline(transaction=False)
        players = self.db.getskandalplayer(id)

        json_players = json.dumps(players)
        pipe.set('scandal:players:%s' % id, json_players)

        pipe.execute()


    def scandal_relations(self, id=0):
        pipe = r_master.pipeline(transaction=False)
        relations = self.db.getskandalrelations(id)

        json_relations = json.dumps(relations)
        pipe.set('scandal:relations:%s' % id, json_relations)

        pipe.execute()


    def scandal_photo(self, id=0):

        pipe = r_master.pipeline(transaction=False)
        photo = self.db.getskandalphoto(id)

        json_photo = json.dumps(photo)
        pipe.set('scandal:photo:%s' % id, json_photo)

        pipe.execute()


    def scandal_last_activity(self):

        last_list = self.db.getskandallastactivity()
        pipe = r_master.pipeline(transaction=False)
        ps = json.dumps(last_list)
        pipe.watch('scandal:last:activity')
        pipe.set('scandal:last:activity', ps)
        pipe.execute()


    def get_scandal_list_hot(self):
        all_content = self.timeline_db.getscandallisthome(where='hotlist = 1', order='hot_sortnumber')
        pipe = r_master.pipeline(transaction=False)
        pipe.watch('scandal:list:hot')
        pipe.delete('scandal:list:hot')
        for index, val in enumerate(all_content):
            pipe.rpush('scandal:list:hot', val['scandal_id'])

        pipe.execute()


    def get_scandal_list(self):
        all_content = self.timeline_db.getscandallist()
        # get headline
        head_id = r_slave.lrange('scandal:list:headline', 0, 0)
        # get hot
        hot_id = r_slave.lrange('scandal:list:hot', 0, 0)

        pipe = r_master.pipeline(transaction=False)
        pipe.delete('scandal:list:all')
        #pipe.rpush('scandal:list:all', head_id[0])
        #if(head_id != hot_id):
        #    pipe.rpush('scandal:list:all', hot_id)
        for index, val in enumerate(all_content):
            pipe.rpush('scandal:list:all', val['id'])

        pipe.execute()


    def get_scandal_list_headline(self):
        all_content = self.timeline_db.getscandallisthome(where='headline = 1', order='head_sortnumber')
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('scandal:list:headline')
        for index, val in enumerate(all_content):
            pipe.rpush('scandal:list:headline', val['scandal_id'])

        pipe.execute()


    def get_scandal_list_by_category(self):
        all_category = self.timeline_db.getcategorylist()
        pipe2 = r_master.pipeline(transaction=False)
        pipe2.delete('category:list')
        for index, val in enumerate(all_category):
            category_id = val['scandal_category_id']
            all_content = self.timeline_db.getscandallistbycategory(category_id)
	    pipe2.rpush('category:list', category_id)
            keyname = 'scandal:list:'+str(category_id)+':category'
            pipe = r_master.pipeline(transaction=False)
            pipe.delete(keyname)
            for index, val in enumerate(all_content):
                pipe.rpush(keyname, val['id'])
            pipe.execute()
        pipe2.execute()
            
    def doRun(self):
        all_content = self.timeline_db.getlogchanged('tipe = \'scandal\'')
        #print all_content
        for index, val in enumerate(all_content):
            self.scandal_min(val['id'])
            self.scandal_photo(val['id'])
            self.scandal_kronologi(val['id'])
            self.get_scandal_list()
            self.scandal_players(val['id'])
            self.scandal_relations(val['id'])
            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))

        self.get_scandal_list()
        self.get_scandal_list_by_category()
