__author__ = 'alam'

import os
import redis
import ConfigParser
import simplejson as json
from libs.timeline_db import TimelineDb
from libs.active_db import ActiveDb


pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))


class Content(object):
    def __init__(self):
        self.db = TimelineDb(Config)
        self.active_db = ActiveDb(Config)

    def content_score(self, id=0):
        pipe = r_master.pipeline(transaction=False)
        content = self.active_db.getcontent(id)
        for i, val in enumerate(content):
            #if i == 100:
            #    break
            content_id = val['content_id'].replace("'", "\\'")
            score = self.db.getcontentscore(content_id)
            pal = json.dumps(score)
            pipe.set('content:last:score:%s' % str(val['content_id']), pal )

        pipe.execute()


    def home_last_activity(self):
        last_list = self.active_db.getbijaklastactivity()
        pipe = r_master.pipeline(transaction=False)
        ps = json.dumps(last_list)
        pipe.set('bijak:last:activity', ps)
        pipe.execute()

    def content_last_activity(self, id=0):
        last_list = self.active_db.getcontentlastactivity(id)
        pipe = r_master.pipeline(transaction=False)
        for index, val in enumerate(last_list):
            ps = json.dumps(val)
            pipe.set('content:last:activity:%s' % str(val['content_id']), ps)

        pipe.execute()

    def content_comment(self, id=0):
        all_content = self.active_db.getcontent(id)
        print all_content
        pipe = r_master.pipeline(transaction=False)
        for index, val in enumerate(all_content):
            if index == 10:
                break
            content_id = val['content_id'].replace("'", "\\'")

            last_key = r_slave.get('comment:key:%s' % content_id)
            if(last_key == None):
                last_key = 0
            rkey = int(last_key) + 1

            comment = self.active_db.getcontentcomment(content_id)
            for row, cmnt in enumerate(comment):
                comm = json.dumps(cmnt)
                pipe.rpush('comment:list:%s:%s' % (rkey,content_id), cmnt['comment_id'])
                pipe.set('comment:detail:%s' % cmnt['comment_id'], comm)

            pipe.delete('comment:list:%s:%s' % (last_key,content_id))
            pipe.set('comment:key:%s' % content_id, rkey)

        pipe.execute()

    def home_total(self):
        home_total = self.db.get_home_total()
        ps = json.dumps(home_total)
        r_master.set('home:total', ps)


    def doRun(self):
        all_content = self.db.getlogchanged('tipe = \'content\'')
        print all_content
        for index, val in enumerate(all_content):
            #self.content_score(val['id'])
            #self.content_comment(val['id'])
            self.content_last_activity(val['id'])
            self.db.updatelogchanged(val['id'], str(val['insert_date']))

