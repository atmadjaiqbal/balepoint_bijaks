__author__ = 'alam'
# -*- coding: utf-8 -*-
import os
import sys
import time
import redis
import ConfigParser
import simplejson as json

import logging

from libs.active_db import ActiveDb
from libs.timeline_db import TimelineDb
pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

row_province = []
row_dapil = []
list_caleg = list()


class Struktur(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass

    def structure_group(self, parent=0):
        structure = self.timeline_db.getStructureNode(parent)
        pipe = r_master.pipeline(transaction=False)
        sk = json.dumps(structure)
        pipe.set('structure:group:%s' % parent, sk)
        pipe.execute()
        self.__recurse_structure(structure)


    def __recurse_structure(self, parent_list=dict()):

        for row, val in enumerate(parent_list):
            #print val['id_structure_node']
            structure = self.timeline_db.getStructureNode(val['id_structure_node'])
            if len(structure) > 0:
                pipe = r_master.pipeline(transaction=False)
                sk = json.dumps(structure)
                pipe.set('structure:group:%s' % val['id_structure_node'], sk)
                pipe.execute()
                self.__recurse_structure(structure)

    def __recurse_structure_index(self, parent_list=dict(), prt=dict(), index=0):
        for row, val in enumerate(parent_list):
            #row_caleg.append(val['structure_name'])
            # provinsi

            if(index == 0):
                row_province.append(val['structure_name'])

            structure = self.timeline_db.getStructureNode(val['id_structure_node'])
            if len(structure) > 0:
                if index == 1:
                    row_dapil.append(val['structure_name'])
                indexs = index + 1
                self.__recurse_structure_index(structure, prt, indexs)
            else:
                if(index == 1):
                    idk = len(row_province) - 1
                    list_caleg.append(dict({'partai':prt['partai'], 'partai_id':prt['partai_id'], 'province':row_province[-1], 'dapil':'', 'caleg':val['structure_name'], 'caleg_id':val['page_id']}))
                else:
                    idk = len(row_province) - 1
                    list_caleg.append(dict({'partai':prt['partai'], 'partai_id':prt['partai_id'], 'province':row_province[-1], 'dapil':row_dapil[-1], 'caleg':val['structure_name'], 'caleg_id':val['page_id']}))

    #26181, 26179,  26204, 26205, 26206, 26207, 26208, 26209, 26210, 26211, 26212, 26213
    def caleg_row(self):
        prt = ({
                0 : ({'parent':26181, 'partai':'Partai Nasdem', 'partai_id':'nasdem5119b72a0ea62'}),
                1 : ({'parent':26179, 'partai':'Partai Kebangkitan Bangsa', 'partai_id':'partaikebangkitanbangsa5119b257621a4'}),
                2 : ({'parent':26204, 'partai':'Partai Keadilan Sejahtera', 'partai_id':'partaikeadilansejahtera5119b06f84fef'}),
                3 : ({'parent':26205, 'partai':'Partai Demokrasi Indonesia Perjuangan', 'partai_id':'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'}),
                4 : ({'parent':26206, 'partai':'Partai Golongan Karya', 'partai_id':'partaigolongankarya5119aaf1dadef'}),
                5 : ({'parent':26207, 'partai':'Partai Gerakan Indonesia Raya', 'partai_id':'partaigerakanindonesiarayagerindra5119a4028d14c'}),
                6 : ({'parent':26208, 'partai':'Partai Demokrat', 'partai_id':'partaidemokrat5119a5b44c7e4'}),
                7 : ({'parent':26209, 'partai':'Partai Amanat Nasional', 'partai_id':'partaiamanatnasional5119b55ab5fab'}),
                8 : ({'parent':26210, 'partai':'Partai Persatuan Pembangunan', 'partai_id':'partaipersatuanpembangunan5189ad769b227'}),
                9 : ({'parent':26211, 'partai':'Partai Hati Nurani Rakyat', 'partai_id':'partaihatinuranirakyathanura5119a1cb0fdc1'}),
                10 : ({'parent':26212, 'partai':'Partai Bulan Bintang', 'partai_id':'partaibulanbintang52155330a9408'}),
                11 : ({'parent':26213, 'partai':'Partai Keadilan dan Persatuan Indonesia', 'partai_id':'partaikeadialndnpersatuanindonesia5119bd7483d2a'})
               })

        self.timeline_db.truncateStructureCaleg()
        for row, value in enumerate(prt):
            vals = prt[value]
            #print vals['parent']
            structure = self.timeline_db.getStructureNode(vals['parent'])
            self.__recurse_structure_index(structure, vals, 0)
            for row, val in enumerate(list_caleg):
                #if row == 1:
                #    break
                vs = dict(val)
                #print vs
                self.timeline_db.setStructureCaleg(vs)

            del list_caleg[0:len(list_caleg)]

        self.timeline_db.updateStructureCalegProvince()
        self.timeline_db.updateStructureCalegDapil()


    def doRun(self):
        self.caleg_row()