# -*- coding: utf-8 -*-
import os
import sys
import time
import redis
import ConfigParser
import Queue
import simplejson as json
import uuid
import urllib
import urllib2

from libs.active_db import ActiveDb
from libs.html_fetch import HtmlFetch
from libs.timeline_db import TimelineDb

pth = os.path.dirname(os.path.abspath(__file__))
Config = ConfigParser.ConfigParser()
Config.read('%s/bijaks.ini' % pth)

#url = 'http://10.136.22.17/mobile/get_post/?dev=1&id=' 

url = 'http://news.bijaks.net/mobile/get_post/?dev=1&id='
#url = 'http://news.bijaks.net/mobile/get_post/?id='

queue = Queue.Queue()
out_queue = Queue.Queue()


r_master = redis.StrictRedis(host=str(Config.get('redis_master', 'host')), port=int(Config.get('redis_master', 'port')), db=int(Config.get('redis_master', 'db')), password=str(Config.get('redis_master', 'auth')))
r_slave = redis.StrictRedis(host=str(Config.get('redis_slave', 'host')), port=int(Config.get('redis_slave', 'port')), db=int(Config.get('redis_slave', 'db')), password=str(Config.get('redis_slave', 'auth')))

class News(object):

    def __init__(self):
        self.db = ActiveDb(Config)
        self.timeline_db = TimelineDb(Config)
        pass

    def news(self, id=0, id_type=0):
        news_list = self.db.getnewslist(id, id_type)
        pipe = r_master.pipeline(transaction=False)

        posts = []

        for i in range(20):
            fetch = HtmlFetch(queue, out_queue)
            fetch.setDaemon(True)
            fetch.start()


        for index, val in enumerate(news_list):
            #f index == 100:
            #    break

            news_uri = url + str(val['news_id'])
            que_data = {'tipe':0, 'host':news_uri }

            queue.put(que_data)


        for index, val in enumerate(news_list):
            #if index == 100:
            #    break

            news_uri = url + str(val['news_id'])
            print '%s fetch uri %s' %(str(index), news_uri)

            conentid = val['content_id']
            set_html = out_queue.get()
            out_queue.task_done()
            news_profile = self.db.getnewsprofile(conentid)
            if str(set_html) != '1':
                news_json = json.loads(set_html)
               #print news_json['status']
                if news_json['status'] == 'ok':

                    post_detail = news_json['post']
                    attach = post_detail['attachments'][0] if len(post_detail['attachments'])  > 0 else '';
                    post = dict({
                            'caption':attach['caption'] if 'caption' in attach else '',
                            'image_full':attach['images']['full']['url'].replace('192.168.52.6', 'news.bijaks.net') if 'images' in attach else '',
                            'image_thumbnail':attach['images']['thumbnail']['url'].replace('192.168.52.6', 'news.bijaks.net') if 'images' in attach else '',
                            'image_medium':attach['images']['medium']['url'].replace('192.168.52.6', 'news.bijaks.net') if 'images' in attach else '',
                            'image_large':attach['images']['large']['url'].replace('192.168.52.6', 'news.bijaks.net') if 'images' in attach else '',
                            'categories': post_detail['categories'],
                            'content': post_detail['content'],
                            'excerpt': post_detail['excerpt'],
                            'title': post_detail['title_plain'],
                            'slug': post_detail['slug'],
                            'id': post_detail['id'],
                            'date': post_detail['date'],
                            'modified_date': post_detail['modified'],

                            'author': post_detail['author'],

                            'content_id': val['content_id'],
                            'type_news': val['type_news'],
                            'type_news_date': val['type_news_date'],
                            'news_profile': news_profile
                        })

                    posts.append(post)

        #pipe.delete('news:0')
        #pipe.delete('news:3')
        #pipe.delete('news:7')
        #pipe.delete('news:9')
        #pipe.delete('news:4')
        #pipe.delete('news:10')
        #pipe.delete('news:12')
        #pipe.delete('news:17')
        #pipe.delete('news:18')
        #pipe.delete('news:19')
        #pipe.delete('news:8')
        for index, post in enumerate(posts):
            #print index, post['title'], '\n'\
            #for rw, cat in enumerate(post['categories']):
            #    #print cat['id']
            #    pipe.rpush('news:%s' % str(cat['id']), str(post['id']))

            dt = {
                    'caption':post['caption'].encode('ascii', 'ignore'),
                    'image_full':post['image_full'].encode('ascii', 'ignore'),
                    'image_thumbnail':post['image_thumbnail'].encode('ascii', 'ignore'),
                    'image_medium':post['image_medium'].encode('ascii', 'ignore'),
                    'image_large':post['image_large'].encode('ascii', 'ignore'),
                    'categories': convert(post['categories']),
                    'content': post['content'].encode('ascii', 'ignore'),
                    'excerpt': post['excerpt'].encode('ascii', 'ignore'),
                    'title': post['title'].encode('ascii', 'ignore'),
                    'slug': post['slug'].encode('ascii', 'ignore'),
                    'id': str(post['id']).encode('ascii', 'ignore'),
                    'date': post['date'].encode('ascii', 'ignore'),
                    'modified_date': post['modified_date'].encode('ascii', 'ignore'),
                    'author': { str(key): str(value)  for key, value in post['author'].iteritems() },
                    'content_id': post['content_id'].encode('ascii', 'ignore'),
                    'type_news': str(post['type_news']).encode('ascii', 'ignore'),
                    'type_news_date': str(post['type_news_date']).encode('ascii', 'ignore'),
                    'news_profile': convert(post['news_profile'])
                }
            #str(post['author']).encode('ascii', 'ignore'),
            ps = json.dumps(dt, ensure_ascii=False)

            pipe.set('news:detail:%s' % post['id'], ps)

            #pipe.rpush('news:0', post['id'])


        pipe.execute()

    def new_list(self, topic_id=0):
        topic_key = [0, 1, 3, 7, 9, 4, 10, 12, 17, 18, 19, 8, 1150, 1151, 1152, 347, 1221, 1222, 1223]

        #print news_list
        last_key = r_slave.get('news:key')
        if(last_key == None):
            last_key = 0
        rkey = int(last_key) + 1

        for topik in topic_key:
            news_list = self.db.getnewslisttopic(topik)

            pipe = r_master.pipeline(transaction=False)

            for row, val in enumerate(news_list):
                cint = r_slave.get('news:detail:%s' % val['news_id'])
                #print "push ", val['news_id'],
                if cint == None:
                    #print 'continue ', val['news_id']
                    continue
                pipe.rpush('news:topic:list:%s:%s' % (rkey, topik), val['news_id'])
                #pipe.rpush('news:topic:list:%s:%s' % (rkey, 0), val['news_id'])

            #pipe.lpush('news:list:rkey', rkey)

            #pipe.set('news:key', rkey)
            pipe.execute()

        r_master.set('news:key', rkey)

        r_master.delete('news:topic:list:%s:%s' % (last_key, 0))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 3))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 7))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 9))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 4))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 10))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 12))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 17))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 18))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 19))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 8))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 1150))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 1151))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 1152))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 347))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 1221))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 1222))
        r_master.delete('news:topic:list:%s:%s' % (last_key, 1223))

    def news_min(self, id=0):
        news_list = self.db.getnewslist(id)
        pipe = r_master.pipeline(transaction=False)

        posts = []

        for index, val in enumerate(news_list):
            news_uri = url + str(val['news_id'])
            request = urllib2.Request(news_uri)

            response = urllib2.urlopen(request)
            set_html = response.read()

            conentid = val['content_id']

            news_profile = self.db.getnewsprofile(conentid)
            if str(set_html) != '1':
                news_json = json.loads(set_html)
                #print news_json['status']
                if news_json['status'] == 'ok':

                    post_detail = news_json['post']
                    attach = post_detail['attachments'][0] if len(post_detail['attachments'])  > 0 else '';
                    post = dict({
                            'caption':attach['caption'] if 'caption' in attach else '',
                            'image_full':attach['images']['full']['url'].replace('192.168.52.6', 'news.bijaks.net') if 'images' in attach else '',
                            'image_thumbnail':attach['images']['thumbnail']['url'].replace('192.168.52.6', 'news.bijaks.net') if 'images' in attach else '',
                            'image_medium':attach['images']['medium']['url'].replace('192.168.52.6', 'news.bijaks.net') if 'images' in attach else '',
                            'image_large':attach['images']['large']['url'].replace('192.168.52.6', 'news.bijaks.net') if 'images' in attach else '',
                            'categories': post_detail['categories'],
                            'content': post_detail['content'],
                            'excerpt': post_detail['excerpt'],
                            'title': post_detail['title_plain'],
                            'slug': post_detail['slug'],
                            'id': post_detail['id'],
                            'date': post_detail['date'],
                            'modified_date': post_detail['modified'],

                            'author': post_detail['author'],

                            'content_id': val['content_id'],
                            'type_news': val['type_news'],
                            'type_news_date': val['type_news_date'],
                            'news_profile': news_profile
                        })

                    posts.append(post)


        for index, post in enumerate(posts):
            dt = {
                    'caption':post['caption'].encode('ascii', 'ignore'),
                    'image_full':post['image_full'].encode('ascii', 'ignore'),
                    'image_thumbnail':post['image_thumbnail'].encode('ascii', 'ignore'),
                    'image_medium':post['image_medium'].encode('ascii', 'ignore'),
                    'image_large':post['image_large'].encode('ascii', 'ignore'),
                    'categories': convert(post['categories']),
                    'content': post['content'],
                    'excerpt': post['excerpt'].encode('ascii', 'ignore'),
                    'title': post['title'].encode('ascii', 'ignore'),
                    'slug': post['slug'].encode('ascii', 'ignore'),
                    'id': str(post['id']).encode('ascii', 'ignore'),
                    'date': post['date'].encode('ascii', 'ignore'),
                    'modified_date': post['modified_date'].encode('ascii', 'ignore'),
                    'author': { str(key): str(value)  for key, value in post['author'].iteritems() },
                    'content_id': post['content_id'].encode('ascii', 'ignore'),
                    'type_news': str(post['type_news']).encode('ascii', 'ignore'),
                    'type_news_date': str(post['type_news_date']).encode('ascii', 'ignore'),
                    'news_profile': convert(post['news_profile'])
                }
            #str(post['author']).encode('ascii', 'ignore'),
            ps = json.dumps(dt, ensure_ascii=False)

            pipe.set('news:detail:%s' % post['id'], ps)

            #pipe.rpush('news:0', post['id'])


        pipe.execute()




    def get_news_list(self, category_id):
        #get last keys
        last_key = r_slave.get('news:key')
        list = r_slave.lrange('news:topic:list:%s:%s' % (last_key, category_id), 0, -1)
        for row, val in enumerate(list):
            self.news_min(val)


    def topic_last_activity(self):
        topic_list = [0, 1, 3, 4, 7, 8, 9, 10, 12, 17, 18, 19, 1150, 1151, 1152, 347, 1221, 1222, 1223]
        pipe = r_master.pipeline(transaction=False)
        for i in topic_list:
            wh = 'topic_id = %s' % i
            if i == 0:
                wh = '1 = 1'
            all_activity = self.db.gettopiclastactivity(wh)
            jso = json.dumps(all_activity)
            pipe.set('topic:last:activity:%s' % i, jso)

        pipe.execute()


    def get_news_list_headline(self):
        all_content = self.timeline_db.getnewslistheadline(200)
        pipe = r_master.pipeline(transaction=False)
        pipe.delete('news:list:headline')
        for index, val in enumerate(all_content):
            cint = r_slave.get('news:detail:%s' % val['news_id'])
            if cint == None:
                continue
            pipe.rpush('news:list:headline', val['news_id'])

        pipe.execute()


    def doRun(self):
        all_content = self.timeline_db.getlogchanged('tipe = \'news\'')
        topic_list = list()
        for index, val in enumerate(all_content):
            if(int(val['post_type']) == 1):
                self.news_min(val['id'])
                a_inst = r_slave.get('news:detail:%s' % val['id'])
                if(a_inst == None):
                    self.news_min(val['id'])

                #topic_list.append(index)
                #self.new_list()
            elif(int(val['post_type']) == 2):
                self.news_min(val['id'])
                a_inst = r_slave.get('news:detail:%s' % val['id'])
                if(a_inst == None):
                    self.news_min(val['id'])
                #self.new_list()
                #topic_list.append(index)
            #else:
                #pass
                #topic_list.append(index)
                #pipe = r_master.pipeline(transaction=False)
                #r_master.delete('news:detail:%s' % val['id'])
                #pipe.execute()
                #self.new_list()

            cint = r_slave.get('news:detail:%s' % val['id'])
            if cint == None:
                continue

            self.timeline_db.updatelogchanged(val['id'], str(val['insert_date']))

        #if len(topic_list) > 0:
        if len(all_content) > 0:
            self.new_list()

        #for val in topic_list:
        #    print val
        #    self.new_list(val)

def convert(input):
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input
