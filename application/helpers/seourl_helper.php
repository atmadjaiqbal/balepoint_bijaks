<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('urlpageid'))
{
	function urlpageid($str) {
		$trans = array(
				' ' => '',
				'-' => '',
				'&' => '',
				',' => '',
				'%' => '',
				'+' => '',
				'/' => '',
				'?' => '',
				'(' => '',
				')' => '',
				'\'' => '',
				'"' => '',
				'.' => '',
				'~' => '',
				'`' => '',
				'!' => '',
				'@' => '',
				'#' => '',
				'$' => '',
				'^' => '',
				'*' => '',
				'_' => '',
				'=' => '',
				'\\' => '',
				'|' => '',
				'{' => '',
				'}' => '',
				'[' => '',
				']' => '',
				':' => '',
				';' => '',
				'<' => '',
				'>' => '',
		);
		return trim(strtr(strtolower($str),$trans));
	}
}

if (!function_exists('urltitle'))
{	
	function urltitle($str) {
		$trans = array(
			' ' => '_',
			'-' => '_dash_',
			'&' => '_and_',
			',' => '_comma_',
			'%' => '_prcnt_',
			'+' => '_plus_',
			'/' => '_slash_',
			'?' => '_question_',
			'(' => '_opt_',
			')' => '_cpt_',
		);
		return trim(substr(strtr(strtolower($str),$trans), 0, 70));
	}
}

if (!function_exists('rev_urltitle'))
{	
	function rev_urltitle($str)
	{
		$trans = array(
			'-'				=> ' ',
			'_dash_'		=> '-',
			'_and_'			=> '&',
			'_comma_'		=> ',',
			'_prcnt_'		=> '%',
			'_plus_'		=> '+',
			'_slash_'		=> '/',
			'_question_'	=> '?',
			'_opt_'			=> '(',
			'_cpt_'			=> ')',
			'_'				=> ' ',
		);
		return trim(strtr($str,$trans));
	}
}

if (!function_exists('full_urltitle'))
{	
	function full_urltitle($str) {
		$trans = array(
			' ' => '_',
			'-' => '_dash_',
			'&' => '_and_',
			',' => '_comma_',
			'%' => '_prcnt_',
			'+' => '_plus_',
			'/' => '_slash_',
			'?' => '_question_',
			'(' => '_opt_',
			')' => '_cpt_',
		);
		return trim(strtr($str,$trans));
	}
}

?>
