<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Extends date helper to return current time in MySQL datetime format
 * By Dave Rogers
 * Shinytype.com
 *
 * @access  public
 * @param   string (optional)
 * @return  string
 */
if (!function_exists('mysql_datetime'))
{
	function mysql_datetime($date=null) {
	    if(!$date) $date = time();         // use now() instead of time() to adhere to user setting

	    if(is_numeric($date) && strlen($date)==10) {
	        return mdate("%Y-%m-%d %H:%i:%s", $date);
	   }   else    {
	        // try to use now()
	        return mdate("%Y-%m-%d %H:%i:%s", now());
		}
	}
}


// --------------------------------------------------------------------

/**
 * Take a MySQL datetime var and turn it into PHP's Unix Epoch time
 *
 * @access  public
 * @param   string
 * @return  int
 */
if (!function_exists('datetime_to_unix'))
{
	function datetime_to_unix($date) {
	   if(!$date) {
	        return false;
	   }   else    {
	        return date('U', strtotime($date));
		}
	}
}

if (!function_exists('time_passed'))
{

	function time_passed($timestamp)
	{
	     $timestamp =  datetime_to_unix($timestamp);
	     $diff = time() - (int)$timestamp;

	     if ($diff == 0)
	          return 'baru saja';

	     $intervals = array
	     (
	//         1                   => array('year',    31556926),
	//         $diff < 31556926    => array('month',   2628000),
	//         $diff < 2629744     => array('week',    604800),
	//         $diff < 604800      => array('day',     86400),
	//         $diff < 86400       => array('hour',    3600),
	//         $diff < 3600        => array('minute',  60),
	//         $diff < 60          => array('second',  1)
	         1                   => array('tahun',    31556926),
	         $diff < 31556926    => array('bulan',   2628000),
	         $diff < 2629744     => array('minggu',    604800),
	         $diff < 604800      => array('hari',     86400),
	         $diff < 86400       => array('jam',    3600),
	         $diff < 3600        => array('menit',  60),
	         $diff < 60          => array('detik',  1)
	     );

	      $value = floor($diff/$intervals[1][1]);
	      return $value.' '.$intervals[1][0].($value > 1 ? '' : '').' lalu';
	}
}