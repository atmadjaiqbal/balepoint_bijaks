<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('large_url'))
{
	 function create_dir($dir = '')
	 {

		  if (!is_dir($dir)) {

				$old = umask(0);

				@mkdir($dir, 0777, true);

				umask($old);
		  }

		  return $dir;

	 }
}

if ( ! function_exists('large_url'))
{

	function large_url($fname, $prefix = '', $migration = true)
	{
		if($prefix == '') {
			$real_path = 'public/upload/image/large/';
		} else {
			$real_path = 'public/upload/image/'.$prefix.'/large/';
		}

		if( file_exists($real_path.$fname)  && ! empty($fname) ) {
			return base_url() . $real_path.$fname;
		} else {
			if($migration == true) {
				$old_path = 'public/upload/image/large/';

				$prefix_explode = explode('/',$prefix);

				if(file_exists($old_path.$fname)  && ! empty($fname)) {
					 create_dir(FCPATH.$real_path);
					 @copy(FCPATH.$old_path.$fname, FCPATH.$real_path.$fname);
				} else {
					if(count($prefix_explode) > 1) {
						$old_prefix_path = 'public/upload/image/'.$prefix_explode[1].'/large/';

						if(file_exists($old_prefix_path.$fname)) {
							create_dir(FCPATH.$real_path);
							@copy(FCPATH.$old_prefix_path.$fname, FCPATH.$real_path.$fname);
						}
					}
				}
			}

			if( file_exists($real_path.$fname)  && ! empty($fname) ) {
				return base_url() . $real_path.$fname;
			} else {
				if($prefix == '') {
					return base_url() . 'assets/images/large/noimage.jpg';
				} else {
					$prefix_explode = explode('/',$prefix);
					if (!empty($prefix_explode)) {
						if (strtolower($prefix_explode[0]) == 'user') {
							return base_url() . 'assets/images/large/no-image-user.png';
						}
						if (strtolower($prefix_explode[0]) == 'politisi') {
							return base_url() . 'assets/images/large/no-image-politisi.png';
						}
					}
				}
			}
		}
	}
}

if ( ! function_exists('badge_url'))
{
	function badge_url($fname, $prefix = '', $migration = true)
	{
		if($prefix == '') {
			$real_path = 'public/upload/image/badge/';
		} else {
			$real_path = 'public/upload/image/'.$prefix.'/badge/';
		}

		if( file_exists($real_path.$fname)  && ! empty($fname) ) {
			return base_url() . $real_path.$fname;
		} else {
			if($migration == true)
			{
				$old_path = 'public/upload/image/badge/';

				 $prefix_explode = explode('/',$prefix);

				if(file_exists($old_path.$fname)  && ! empty($fname))
				{
					 create_dir(FCPATH.$real_path);
					 @copy(FCPATH.$old_path.$fname, FCPATH.$real_path.$fname);
				} else {
					if(count($prefix_explode) > 1)
					{
						$old_prefix_path = 'public/upload/image/'.$prefix_explode[1].'/badge/';

						if(file_exists($old_prefix_path.$fname))
						{
							create_dir(FCPATH.$real_path);
							@copy(FCPATH.$old_prefix_path.$fname, FCPATH.$real_path.$fname);
						}
					}

				}
			}

			if( file_exists($real_path.$fname)  && ! empty($fname) ) {
				return base_url() . $real_path.$fname;
			} else {
				if($prefix == '') {
					return base_url() . 'assets/images/badge/noimage.jpg';
				} else {
					$prefix_explode = explode('/',$prefix);
					if (!empty($prefix_explode)) {
						if (strtolower($prefix_explode[0]) == 'user') {
							return base_url() . 'assets/images/badge/no-image-user.png';
						}
						if (strtolower($prefix_explode[0]) == 'politisi') {
							return base_url() . 'assets/images/badge/no-image-politisi.png';
						}
					}
				}
			}
		}
	}
}

if ( ! function_exists('thumb_url'))
{
	function thumb_url($fname, $prefix = '', $migration = true)
	{
		  if($prefix == '') 		  {
				$real_path = 'public/upload/image/thumb/';
		  } else {
				$real_path = 'public/upload/image/'.$prefix.'/thumb/';
		  }

		if( file_exists($real_path.strval($fname)) && ! empty($fname) ) {
			return base_url() . $real_path.strval($fname);
		} else {
			if($migration == true) {
				$old_path = 'public/upload/image/thumb/';

				$prefix_explode = explode('/',$prefix);

				if(file_exists($old_path.$fname)  && ! empty($fname)) {
					create_dir(FCPATH.$real_path);
					@copy(FCPATH.$old_path.$fname, FCPATH.$real_path.$fname);
				} else {
					if(count($prefix_explode) > 1) {
						$old_prefix_path = 'public/upload/image/'.$prefix_explode[1].'/thumb/';

						if(file_exists($old_prefix_path.$fname)) {
						 create_dir(FCPATH.$real_path);
						 @copy(FCPATH.$old_prefix_path.$fname, FCPATH.$real_path.$fname);
						}
					}
				}
			}

			if( file_exists($real_path.$fname)  && ! empty($fname) ) {
				return base_url() . $real_path.$fname;
			} else {
				if($prefix == '') {
					return base_url() . 'assets/images/thumb/noimage.jpg';
				} else {
					$prefix_explode = explode('/',$prefix);
					if (!empty($prefix_explode)) {
						if (strtolower($prefix_explode[0]) == 'user') {
							return base_url() . 'assets/images/thumb/no-image-user.png';
						}
						if (strtolower($prefix_explode[0]) == 'politisi') {
							return base_url() . 'assets/images/thumb/no-image-politisi.png';
						}
					}
				}
			}
		}
	}
}

if ( ! function_exists('thumb_port_url'))
{
	function thumb_port_url($fname, $prefix = '', $migration = true)
	{
		  if($prefix == '')
		  {
				$real_path = 'public/upload/image/thumb/portrait/';
				$crop_path = 'thumb/portrait';

		  } else {
				$real_path = 'public/upload/image/'.$prefix.'/thumb/portrait/';
				$crop_path = $prefix.'/thumb/portrait';
		  }
		if( file_exists($real_path.strval($fname)) && ! empty($fname) ) {
			return base_url() . $real_path.strval($fname);
		} else {

			if($migration == true) {
				$old_path = 'public/upload/image/thumb/portrait/';
				$prefix_explode = explode('/',$prefix);

				if(file_exists($old_path.$fname)  && ! empty($fname)) {
					create_dir(FCPATH.$real_path);
					@copy(FCPATH.$old_path.$fname, FCPATH.$real_path.$fname);
				} else {

					if(count($prefix_explode) > 1) {
						$old_prefix_path = 'public/upload/image/'.$prefix_explode[1].'/thumb/portrait/';

						if(file_exists($old_prefix_path.$fname)) {
							create_dir(FCPATH.$real_path);
							@copy(FCPATH.$old_prefix_path.$fname, FCPATH.$real_path.$fname);
						}
					}
				}
			}

			if( file_exists($real_path.strval($fname)) && ! empty($fname) ) {
				return base_url() . $real_path.strval($fname);
			} else {
				$cro = image_crop('320', '240', $crop_path, $fname);
				if($cro){
					if( file_exists($real_path.strval($fname))) {
						return base_url() . $real_path.strval($fname);
					} else {
						if($prefix == '') {
							return base_url() . 'assets/images/thumb/portrait/noimage.jpg';
						} else {
							$prefix_explode = explode('/',$prefix);
							if (!empty($prefix_explode)) {
								if (strtolower($prefix_explode[0]) == 'user') {
									return base_url() . 'assets/images/thumb/portrait/no-image-user.png';
								}
								if (strtolower($prefix_explode[0]) == 'politisi') {
									return base_url() . 'assets/images/thumb/portrait/no-image-politisi.png';
								}
							}
						}
					}
				}else{
					return thumb_url($fname, $prefix);
				}
			}
		}
	}
}


if ( ! function_exists('thumb_land_url'))
{
	function thumb_land_url($fname, $prefix = '', $migration = true)
	{
		  if($prefix == '')
		  {
				$real_path = 'public/upload/image/thumb/landscape/';
				$crop_path = 'thumb/landscape';

		  } else {
				$real_path = 'public/upload/image/'.$prefix.'/thumb/landscape/';
				$crop_path = $prefix.'/thumb/landscape';
		  }

		if( file_exists($real_path.strval($fname)) && ! empty($fname) ) {
			return base_url() . $real_path.strval($fname);
		} else {

			if($migration == true)
			{
				$old_path = 'public/upload/image/thumb/landscape/';
				$prefix_explode = explode('/',$prefix);

				if(file_exists($old_path.$fname)  && ! empty($fname)) {
					create_dir(FCPATH.$real_path);
					@copy(FCPATH.$old_path.$fname, FCPATH.$real_path.$fname);
				} else {

					if(count($prefix_explode) > 1)  {
							$old_prefix_path = 'public/upload/image/'.$prefix_explode[1].'/thumb/landscape/';

							if(file_exists($old_prefix_path.$fname)) {
								create_dir(FCPATH.$real_path);
								@copy(FCPATH.$old_prefix_path.$fname, FCPATH.$real_path.$fname);
							}
					 }

				}
			}

			if( file_exists($real_path.strval($fname)) && ! empty($fname) ) {
				return base_url() . $real_path.strval($fname);
			} else {

				$cro = image_crop('180', '320', $crop_path, $fname);

				if($cro){
					if( file_exists($real_path.strval($fname))) {
						return base_url() . $real_path.strval($fname);
					} else {
						if($prefix == '') {
							return base_url() . 'assets/images/thumb/landscape/noimage.jpg';
						} else {
							$prefix_explode = explode('/',$prefix);
							if (!empty($prefix_explode)) {
								if (strtolower($prefix_explode[0]) == 'user') {
									return base_url() . 'assets/images/thumb/landscape/no-image-user.png';
								}
								if (strtolower($prefix_explode[0]) == 'politisi') {
									return base_url() . 'assets/images/thumb/landscape/no-image-politisi.png';
								}
							}
						}
					}
				}else{
					return thumb_url($fname,$prefix);
				}

			}
		}
	}
}

if ( ! function_exists('icon_url'))
{

	function icon_url($fname, $prefix = '', $migration = true)
	{
		  if($prefix == '') {
				$real_path = 'public/upload/image/icon/';
		  } else {
				$real_path = 'public/upload/image/'.$prefix.'/icon/';
		  }

		if( file_exists($real_path.$fname)  && ! empty($fname) ) {
			// return base_url() . $real_path.$fname;
            return base_url() . $real_path.$fname;
		} else {

			  if($migration == true) {
					$old_path = 'public/upload/image/icon/';

					$prefix_explode = explode('/',$prefix);

					if(file_exists($old_path.$fname)  && ! empty($fname)) {
						 create_dir(FCPATH.$real_path);
						 @copy(FCPATH.$old_path.$fname, FCPATH.$real_path.$fname);
					} else {

						if(count($prefix_explode) > 1) {
							$old_prefix_path = 'public/upload/image/'.$prefix_explode[1].'/icon/';

							if(file_exists($old_prefix_path.$fname))
							{
								create_dir(FCPATH.$real_path);
								@copy(FCPATH.$old_prefix_path.$fname, FCPATH.$real_path.$fname);
							}
						}
					}
			  }

			if( file_exists($real_path.$fname)  && ! empty($fname) ) {
				return base_url() . $real_path.$fname;
			} else {
				if($prefix == '') {
					return base_url() . 'assets/images/icon/noimage.jpg';
				} else {
					$prefix_explode = explode('/',$prefix);
					if (!empty($prefix_explode)) {
						if (strtolower($prefix_explode[0]) == 'user') {
							return base_url() . 'assets/images/icon/no-image-user.png';
						}
						if (strtolower($prefix_explode[0]) == 'politisi') {
							return base_url() . 'assets/images/icon/no-image-politisi.png';
						}
					}
				}
			}
		}
	}
}


if ( ! function_exists('image_crop'))
{
	function image_crop($height, $width, $path, $file_name)
	{
		$tr = false;
		if( file_exists('public/upload/image/'.$file_name)  && ! empty($file_name) )
		{
			$CI =& get_instance();
			$CI->load->library('image_lib');

			$config['image_library'] = 'gd2';
			$config['source_image'] = './public/upload/image/'.$file_name;
			$config['new_image'] = "./public/upload/image/".$path."/".$file_name;

			$original_size = getimagesize('public/upload/image/'.$file_name);
			$image_height = $original_size[1];
			$image_width = 	$original_size[0];

			$config['width'] = $width;
			$config['height'] = $height;
			if($height > $image_height && $width > $image_width)
			{
				$path_new = "./public/upload/image/".$path."/";
				if(!file_exists($path_new)) {
					create_dir($path_new);
				}
				copy($config['source_image'],$config['new_image']);
				$tr = true;
			}
			else
			{
				if($image_width/$image_height > $width/$height)
				{
					$config['master_dim'] = 'height';
					$config['x_axis'] = (($image_width*($height/$image_height)) - $width) / 2;
					$config['y_axis'] = 0;
				}
				else
				{
					$config['master_dim'] = 'width';
					$config['x_axis'] = 0;
					$config['y_axis'] = (($image_height*($width/$image_width))  - $height) / 2;
				}

				$config['maintain_ratio'] = TRUE;
				$CI->image_lib->initialize($config);
				$CI->image_lib->resize();
				$config['source_image'] = $config['new_image'];
				$config['maintain_ratio'] = FALSE;
				$CI->image_lib->initialize($config);
				$CI->image_lib->crop();
				$tr = true;
			}
		}
		return $tr;
	}
}


if ( ! function_exists('photo_name'))
{
	function photo_name($page_id, $type, $prefix = '')
	{
		$CI =& get_instance();
		$CI->load->model('Sentimen_model');

		if(!empty($page_id) && !is_null($page_id)){
			$qr = $CI->Sentimen_model->getPhotoName($page_id);
			//var_dump($qr);
			//return var_dump($qr);

			  if($prefix != '')
			  {
					$prefix = $prefix.'/'.$page_id;
			  } else {
					$prefix = $page_id;

			  }

			if( !is_null($qr) && !empty($qr)){
				if($qr->num_rows() > 0){
					$fname = $qr->row()->attachment_title;
					$rs = icon_url($fname);
					switch($type)
					{
						case("large"):
							$rs = large_url($fname, $prefix);
							break;
						case("badge"):
							$rs = badge_url($fname, $prefix);
							break;
						case("thumb"):
							$rs = thumb_url($fname, $prefix);
							break;
						case("icon"):
							$rs = icon_url($fname, $prefix);
							break;
					}
					return $rs;
				}else{
					return icon_url(''); //base_url() . 'public/image/noimage.jpg';
				}
			}else{
				return icon_url(''); //base_url() . 'public/image/noimage.jpg';
			}
		}else{
			return icon_url(''); //base_url() . 'public/image/noimage.jpg';
		}
	}
}

if ( ! function_exists('content_photo'))
{
	function content_photo($content_id, $type, $group, $prefix = '')
	{
		$CI =& get_instance();
		$CI->load->model('Sentimen_model');

		if(!empty($content_id) && !is_null($content_id)){
			$qr = $CI->Sentimen_model->getPhotoContent($content_id, $group);
			//var_dump($qr);
			//return var_dump($qr);
			if(!is_null($qr) && !empty($qr)){
				if($qr->num_rows() > 0){
					$fname = $qr->row()->attachment_title;
					$rs = icon_url($fname);
					switch($type)
					{
						case("large"):
							$rs = large_url($fname, $prefix);
							break;
						case("badge"):
							$rs = badge_url($fname, $prefix);
							break;
						case("thumb"):
							$rs = thumb_url($fname, $prefix);
							break;
						case("icon"):
							$rs = icon_url($fname, $prefix);
							break;
					}
					return $rs;
				}
			}else{
				return icon_url(''); //base_url() . 'public/image/noimage.jpg';
			}
		}else{
			return icon_url(''); //base_url() . 'public/image/noimage.jpg';
		}
	}
}