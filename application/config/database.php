<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'iqbal';
$db['default']['password'] = 'labqi'; //'bijaks';
$db['default']['database'] = 'politic';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = FALSE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

$active_group = 'master';
$active_record = TRUE;

$db['master']['hostname'] = 'localhost';
$db['master']['username'] = 'iqbal';
$db['master']['password'] = 'labqi'; //'bijaks';
$db['master']['database'] = 'politic';
$db['master']['dbdriver'] = 'mysql';
$db['master']['dbprefix'] = '';
$db['master']['pconnect'] = TRUE;
$db['master']['db_debug'] = FALSE;
$db['master']['cache_on'] = FALSE;
$db['master']['cachedir'] = '';
$db['master']['char_set'] = 'utf8';
$db['master']['dbcollat'] = 'utf8_general_ci';
$db['master']['swap_pre'] = '';
$db['master']['autoinit'] = TRUE;
$db['master']['stricton'] = FALSE;

$active_group = 'slave';
$active_record = TRUE;

$db['slave']['hostname'] = 'localhost';
$db['slave']['username'] = 'iqbal';
$db['slave']['password'] = 'labqi'; //'bijaks';
$db['slave']['database'] = 'politic';
$db['slave']['dbdriver'] = 'mysql';
$db['slave']['dbprefix'] = '';
$db['slave']['pconnect'] = TRUE;
$db['slave']['db_debug'] = FALSE;
$db['slave']['cache_on'] = FALSE;
$db['slave']['cachedir'] = '';
$db['slave']['char_set'] = 'utf8';
$db['slave']['dbcollat'] = 'utf8_general_ci';
$db['slave']['swap_pre'] = '';
$db['slave']['autoinit'] = TRUE;
$db['slave']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */
