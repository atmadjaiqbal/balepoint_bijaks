<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Slave Database location
*/

$active_group = 'slave';
$active_record = TRUE;

$db['slave']['hostname'] = '10.131.11.115:3306'; //small server
$db['slave']['username'] = 'politic';
$db['slave']['password'] = 'newpr0d_p0litic_d4t4b4s3';
$db['slave']['database'] = 'politic';
$db['slave']['dbdriver'] = 'mysql';
$db['slave']['dbprefix'] = '';
$db['slave']['pconnect'] = TRUE;
$db['slave']['db_debug'] = TRUE;
$db['slave']['cache_on'] = FALSE;
$db['slave']['cachedir'] = '';
$db['slave']['char_set'] = 'utf8';
$db['slave']['dbcollat'] = 'utf8_general_ci';
$db['slave']['swap_pre'] = '';
$db['slave']['autoinit'] = TRUE;
$db['slave']['stricton'] = FALSE;


/*
Master Database location
*/

$active_group = 'master';
$active_record = TRUE;

$db['master']['hostname'] = '10.131.11.115:3306'; //small server
$db['master']['username'] = 'politic';
$db['master']['password'] = 'newpr0d_p0litic_d4t4b4s3';
$db['master']['database'] = 'politic';
$db['master']['dbdriver'] = 'mysql';
$db['master']['dbprefix'] = '';
$db['master']['pconnect'] = TRUE;
$db['master']['db_debug'] = TRUE;
$db['master']['cache_on'] = FALSE;
$db['master']['cachedir'] = '';
$db['master']['char_set'] = 'utf8';
$db['master']['dbcollat'] = 'utf8_general_ci';
$db['master']['swap_pre'] = '';
$db['master']['autoinit'] = TRUE;
$db['master']['stricton'] = FALSE;

/*
Slave Database location
*/

$active_group = 'select';
$active_record = TRUE;

$db['select']['hostname'] = '10.131.11.115:3306'; //small server
$db['select']['username'] = 'politic';
$db['select']['password'] = 'newpr0d_p0litic_d4t4b4s3';
$db['select']['database'] = 'politic';
$db['select']['dbdriver'] = 'mysql';
$db['select']['dbprefix'] = '';
$db['select']['pconnect'] = TRUE;
$db['select']['db_debug'] = TRUE;
$db['select']['cache_on'] = FALSE;
$db['select']['cachedir'] = '';
$db['select']['char_set'] = 'utf8';
$db['select']['dbcollat'] = 'utf8_general_ci';
$db['select']['swap_pre'] = '';
$db['select']['autoinit'] = TRUE;
$db['select']['stricton'] = FALSE;

/*
Slave Database location
*/

$active_group = 'update';
$active_record = TRUE;

$db['update']['hostname'] = '10.131.11.115:3306'; //small server
$db['update']['username'] = 'politic';
$db['update']['password'] = 'newpr0d_p0litic_d4t4b4s3';
$db['update']['database'] = 'politic';
$db['update']['dbdriver'] = 'mysql';
$db['update']['dbprefix'] = '';
$db['update']['pconnect'] = TRUE;
$db['update']['db_debug'] = TRUE;
$db['update']['cache_on'] = FALSE;
$db['update']['cachedir'] = '';
$db['update']['char_set'] = 'utf8';
$db['update']['dbcollat'] = 'utf8_general_ci';
$db['update']['swap_pre'] = '';
$db['update']['autoinit'] = TRUE;
$db['update']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */
