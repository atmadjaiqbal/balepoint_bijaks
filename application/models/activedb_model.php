<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activedb_model extends CI_Model{
//    var $DB_Master;
    public function __construct()
    {
            parent::__construct();
//            $this->load->database('slave');
//            $this->DB_Master = $this->load->database('master', true);
    }
    
    public function getnewslist($this,$id=0, $id_type=0){
        if($id != 0){
            if($id_type == 0){
                $wh = "tt.news_id = '".$id."'";
            }else{
                $wh = "topic_id = '".$id."'";
            }
        }
        
        $sql = " SELECT * FROM
            (
                SELECT tt.content_id, tt.news_id, tt.type_news, tt.type_news_date, t.entry_date, tht.topic_id
                FROM tcontent_text tt
                JOIN tcontent t ON t.`content_id` = tt.`content_id`
                JOIN tcontent_has_topic tht ON tht.`content_id` = t.`content_id`
                WHERE ".$wh." and tt.news_id IS NOT NULL AND tt.news_id != ' ' AND tt.news_id != 0
                ORDER BY t.entry_date DESC LIMIT 1000 OFFSET 0
            ) AS ta GROUP BY `news_id`";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
        
    public function getnewsprofile($this, $contentid){
        $sql = "SELECT tht.status, tht.score, tp.`page_id`, tp.`page_name`,  ta.`attachment_title` FROM tcontent_has_politic tht
            JOIN tobject_page tp ON tp.`page_id` = tht.page_id
            JOIN tcontent_attachment ta ON ta.`content_id` = tp.profile_content_id WHERE tht.`content_id` = '".$contentid."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontentlastactivity($this, $id=0){
        $where = $id != 0 ? "tm.content_id = '".$id."'" : '1=1';
        $sql = "SELECT tm.*, ta.`display_name`, tp.`page_id`, tp.`profile_content_id`, tac.attachment_title FROM
            (
            	SELECT * FROM
            	(
            		SELECT 'comment' AS tipe, account_id, text_comment AS teks, content_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM tcontent_comment tc
            				ORDER BY entry_date DESC
            			) AS last_comment GROUP BY content_id UNION ALL
            		SELECT 'like' AS tipe, account_id, 'like' AS teks, content_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM tcontent_like
            				ORDER BY entry_date DESC
            			)AS last_like GROUP BY content_id UNION ALL
            		SELECT 'dislike' AS tipe, account_id, 'dislike' AS teks, content_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM tcontent_dislike
            				ORDER BY entry_date DESC
            			)AS last_dislike GROUP BY content_id
            	)AS act ORDER BY entry_date DESC
            ) AS tm
            JOIN `tusr_account` ta ON ta.`account_id`=tm.`account_id`
            JOIN  `tobject_page` tp ON tp.page_id = ta.`user_id`
            LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
            WHERE ".$where." AND tm.content_id IS NOT NULL AND tm.content_id != ' ' AND tm.content_id != '0' GROUP BY tm.content_id";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getprofilelastactivity($this, $limit=0){
        $lm = $limit != 0 ? "limit ".$limit : 'limit = 1';
        $sql = "SELECT tm.*, top.`page_name` AS title, ta.`display_name`, ta.`display_name` AS page_name, tp.`page_id`, tp.`profile_content_id`, tac.attachment_title FROM
            (
            	SELECT * FROM
            	(
            		SELECT 'comment' AS tipe, account_id, comment_text AS teks, page_id AS profile_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM page_comment tc
            				WHERE tc.`status` = 0
            				ORDER BY entry_date DESC ".$lm."
            			) AS last_comment GROUP BY page_id UNION ALL
            		SELECT 'like' AS tipe, account_id, 'like' AS teks, page_id AS profile_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM page_like
            				ORDER BY entry_date DESC ".$lm."
            			)AS last_like GROUP BY page_id UNION ALL
            		SELECT 'dislike' AS tipe, account_id, 'dislike' AS teks, page_id AS profile_id, entry_date, COUNT(*) total FROM
            			(
            				SELECT * FROM page_dislike
            				ORDER BY entry_date DESC ".$lm."
            			)AS last_dislike GROUP BY page_id
            	)AS act ORDER BY entry_date DESC
            ) AS tm
            LEFT JOIN tobject_page top ON top.`page_id`=tm.profile_id
            LEFT JOIN `tusr_account` ta ON ta.`account_id`=tm.`account_id`
            LEFT JOIN  `tobject_page` tp ON tp.page_id = ta.`user_id`
            LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
            GROUP BY tm.profile_id";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
        
    }
    
    public function gettopiclastactivity($this, $topic_id=0){
        $tid = $topic_id != 0 ? "tht.`topic_id` = '".$topic_id."'" : '1';
        $sql = "SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT 'comment' AS tipe, account_id, content_id, text_comment AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tc.status = 0 AND tt.`news_id` IS NOT NULL AND ".$tid."
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT 'like' AS tipe, account_id, content_id, 'like' AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tt.`news_id` IS NOT NULL AND ".$tid."
                    ORDER BY tc.entry_date DESC LIMIT 1
                )AS last_topic_like GROUP BY content_id UNION ALL

                SELECT 'dislike' AS tipe, account_id, content_id, 'dislike' AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tt.`news_id` IS NOT NULL and ".$tid."
                    ORDER BY tc.entry_date DESC LIMIT 1
                )AS last_topic_dislike  GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getbijaklastactivity($this){
        $sql = "SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT 'comment' AS tipe, account_id, content_id, text_comment AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tc.status = 0 AND tt.`news_id` IS NOT NULL
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT 'like' AS tipe, account_id, content_id, 'like' AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tt.`news_id` IS NOT NULL
                    ORDER BY tc.entry_date DESC LIMIT 1
                )AS last_topic_like GROUP BY content_id UNION ALL

                SELECT 'dislike' AS tipe, account_id, content_id, 'dislike' AS teks, entry_date, title, news_id, content_url,
                topic_id FROM
                (
                    SELECT tc.*, t.`title`, tt.`news_id`, tt.`content_url`, tht.`topic_id`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    LEFT JOIN tcontent_text tt ON tt.content_id = tc.`content_id`
                    LEFT JOIN tcontent_has_topic tht ON tht.`content_id`=tt.`content_id`
                    WHERE tt.`news_id` IS NOT NULL
                    ORDER BY tc.entry_date DESC LIMIT 1
                )AS last_topic_dislike  GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getskandallastactivity($this){
        $sql = "SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, tc.`account_id`, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE tc.status = 0 AND t.`location` IS NOT NULL AND t.`content_group_type` = 50
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'like' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 50
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_like GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 50
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_dislike GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesilastactivity($this){
        $sql = "SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, tc.`account_id`, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE tc.status = 0 AND t.`location` IS NOT NULL AND t.`content_group_type` = 56
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'like' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 56
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_like GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 56
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_dislike GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsurveylastactivity($this){
        $sql = "SELECT tc.*, tp.`page_id`, tp.`page_name`, tp.`profile_content_id`, tat.`attachment_title` FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, tc.`account_id`, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_comment tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE tc.status = 0 AND t.`location` IS NOT NULL AND t.`content_group_type` = 54
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_topic_comment GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'like' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_like tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 54
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_like GROUP BY content_id UNION ALL

                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, tc.account_id, tc.`content_id`, t.`title`, t.`location`
                    FROM tcontent_dislike tc
                    LEFT JOIN tcontent t ON t.`content_id` = tc.`content_id`
                    WHERE t.`location` IS NOT NULL AND t.`content_group_type` = 54
                    ORDER BY tc.entry_date  DESC LIMIT 1
                )AS last_scandal_dislike GROUP BY content_id
            ) AS tc
            LEFT JOIN tusr_account ta ON ta.`account_id` =tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontent($this, $where=0){
        $wh = $where != 0 ? "content_id = '".$where."'" : '1=1';
        $sql = "SELECT * FROM tcontent where ".$wh." and  status = 0 ORDER BY entry_date DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getprofile($this, $where=0){
        $wh = $where != 0 ? "vup.`page_id` = '".$where."'" : '1=1';
        $sql = "SELECT vup.page_id, vup.page_name, vup.profile_content_id, vup.entry_date, vup.entry_by, vup.last_update_date, vup.website,
            vup.about, vup.personal_message, vup.posisi, vup.prestasi, vup.prestasi_lain, vup.partai_id, tac.account_id,
            tac.`email` ,tac.`gender`, tac.`birthday`, tac.`phone`, tac.alias,
            ta.`attachment_title`, tp.`page_name` AS partai_name, tac2.alias as partai_alias, ta_partai.`attachment_title` AS attachment_title_partai,
            CONCAT('{0}/public/upload/image/politisi/', vup.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.partai_id, '/icon/', ta_partai.attachment_title) AS icon_partai_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.partai_id, '/large/', ta_partai.attachment_title) AS large_partai_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.partai_id, '/thumb/', ta_partai.attachment_title) AS thumb_partai_url,
            CONCAT('{0}/public/upload/image/politisi/', vup.partai_id, '/badge/', ta_partai.attachment_title) AS badge_partai_url
            FROM v_usr_politik vup
            LEFT JOIN tusr_account tac ON tac.`user_id` = vup.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = vup.`profile_content_id`
            LEFT JOIN tobject_page tp ON tp.`page_id` = vup.`partai_id`
            LEFT JOIN tusr_account tac2 ON tac2.`user_id` = tp.`page_id`
            LEFT JOIN tcontent_attachment ta_partai ON ta_partai.`content_id` = tp.`profile_content_id`
            where '".$wh."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontentalbum($this, $page_id){
        $sql = "SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date`
            FROM tcontent tc
            WHERE content_group_type = 40 AND content_type = 'GROUP'
            AND tc.`status` = 0 AND tc.page_id = '".$page_id."'
            ORDER BY tc.`entry_date` DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontentfotoalbum($this, $content_id){
        $sql = "SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date` , ta.`attachment_title`, ta.hotlist AS 'hotlist', ta.headline AS 'headline',
            ta.`order` AS 'set_order', ta.`is_profile` AS 'is_profile',
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/headline/', ta.attachment_title) AS headline_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/hotlist/', ta.attachment_title) AS hotlist_url
            FROM tcontent tc
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tc.`content_id`
            WHERE tc.content_group_type = 40 AND tc.content_type = 'IMAGE'
            AND tc.`status` = 0 AND tc.group_content_id = '".$content_id."'
            ORDER BY ta.`order` ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontentfoto($this, $page_id=0, $content_type='IMAGE'){
        $sql = "SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date` , ta.`attachment_title`, ta.hotlist AS 'hotlist', ta.headline AS 'headline',
            ta.`order` AS 'set_order', ta.`is_profile` AS 'is_profile',
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/headline/', ta.attachment_title) AS headline_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/hotlist/', ta.attachment_title) AS hotlist_url
            FROM tcontent tc
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tc.`content_id`
            WHERE tc.content_group_type = ".$content_type." AND tc.content_type = 'IMAGE'
            AND tc.`status` = 0 AND tc.page_id = '".$page_id."'
            ORDER BY ta.`order` ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontentfotoheadline($this, $page_id=0){
        $sql = "SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date` , ta.`attachment_title`, ta.hotlist AS 'hotlist', ta.headline AS 'headline',
            ta.`order` AS 'set_order', ta.`is_profile` AS 'is_profile',
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/headline/', ta.attachment_title) AS headline_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/hotlist/', ta.attachment_title) AS hotlist_url
            FROM tcontent tc
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tc.`content_id`
            WHERE tc.content_type = 'IMAGE'
            AND tc.`status` = 0 AND ta.headline = 1 AND tc.page_id = '".$page_id."'
            ORDER BY ta.`order` ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontentfotohotlist($this, $page_id=0){
        $sql = "SELECT tc.content_id, tc.`account_id`, tc.`content_group_type`, tc.`group_content_id`, tc.`page_id`, tc.`title`, tc.`description`,
            tc.`content_type`, tc.`last_update_date`, tc.`entry_date` , ta.`attachment_title`, ta.hotlist AS 'hotlist', ta.headline AS 'headline',
            ta.`order` AS 'set_order', ta.`is_profile` AS 'is_profile',
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/icon/', ta.attachment_title) AS icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/badge/', ta.attachment_title) AS badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/headline/', ta.attachment_title) AS headline_url,
            CONCAT('{0}/public/upload/image/politisi/', tc.`page_id`, '/hotlist/', ta.attachment_title) AS hotlist_url
            FROM tcontent tc
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tc.`content_id`
            WHERE tc.content_type = 'IMAGE'
            AND tc.`status` = 0 AND ta.hotlist = 1 AND tc.page_id = '".$page_id."'
            ORDER BY ta.`order` ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getskandalbyplayer($this, $page_id=0){
        $sql = "SELECT tp.date, tp.`title` AS title_player, tp.`description` AS player_desc, tp.`short_desc`, tp.`sumber_data`, tp.`nomor_urut`, tp.`pengaruh`,
            ts.`scandal_id`, ts.`title` AS scandal_title, ts.`description` AS scandal_desc
            FROM tscandal_player tp
            LEFT JOIN tscandal ts ON ts.`scandal_id`=tp.`scandal_id`
            WHERE tp.page_id  = '".$page_id."'
            ORDER BY tp.date desc";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontentcomment($this, $content_id=0){
        $sql = "SELECT tc.*, tp.`page_id`, tp.`page_name`, tat.`attachment_title`
            FROM tcontent_comment tc
            LEFT JOIN tusr_account ta ON ta.`account_id`=tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`
            where tc.content_id = '".$content_id."' and tc.status = 0
            ORDER BY tc.entry_date desc";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getprofilecomment($this, $page_id=0){
        $sql = "SELECT pc.page_comment_id, pc.`account_id`, pc.`comment_text` as text_comment, pc.`page_id` AS profile_id, pc.entry_date,  pc.`status` , tp.`page_id`, tp.`page_name`, tat.`attachment_title`
            FROM page_comment pc
            LEFT JOIN tusr_account ta ON ta.`account_id` = pc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id` = ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id` = tp.`profile_content_id`
            WHERE pc.page_id = '".$page_id."' and pc.status = 0
            ORDER BY pc.entry_date DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getskandal($this, $scandal_id=0, $main_scandal=0){
        $wh = $scandal_id != 0 ? 'ts.scandal_id = "'.$scandal_id.'"' : '1=1';
        $sql = "SELECT ts.*, tc.content_id
            FROM tscandal ts
            LEFT JOIN tcontent tc ON tc.`location` = ts.`scandal_id` AND tc.content_group_type = 50
            WHERE ".$wh." and ts.main_scandal = '".$main_scandal."'
            ORDER BY `date` DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getskandalphoto($this, $scandal_id=0){
        $sql = "SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/skandal/', ta.attachment_title) AS original_url,
            CONCAT('{0}/public/upload/image/skandal/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url
            FROM tscandal_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            where tp.scandal_id = ".$scandal_id." ORDER BY ta.`order` ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getskandalphotoheadline($this, $scandal_id=0){
        $sql = "SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/skandal/', ta.attachment_title) AS original_url,
            CONCAT('{0}/public/upload/image/skandal/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url
            FROM tscandal_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            where tp.scandal_id = '".$scandal_id."' AND ta.headline = 1";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getskandalphotohotlist($this, $scandal_id=0){
        $sql = "SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/skandal/', ta.attachment_title) AS original_url,
            CONCAT('{0}/public/upload/image/skandal/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url
            FROM tscandal_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            where tp.scandal_id = '".$scandal_id."' AND ta.hotlist = 1";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getskandalplayer($this, $scandal_id=0){
        $sql = "SELECT sp.date, sp.title, sp.`description`, sp.`short_desc`, sp.`sumber_data`, sp.`main_player`, sp.`scandal_id`, sp.`nomor_urut`,
            sp.`pengaruh`, tp.page_id, tp.`page_name`,
            CONCAT('{0}/politisi/index/', tp.page_id) AS profile_url,
            ta.`attachment_title`,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS profile_icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/thumb/', ta.attachment_title) AS profile_thumb_url
            FROM tscandal_player sp
            LEFT JOIN tobject_page tp ON tp.`page_id`=sp.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            where sp.scandal_id = '".$scandal_id."'
            ORDER BY nomor_urut ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getskandalrelations($this, $scandal_id=0){
        $sql = "SELECT tm.*,
            tp.`page_name` AS page_name_source, ta.`attachment_title` AS attachment_title_source,
            CONCAT('{0}/politisi/index/', tp.page_id) AS profile_source_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/thumb/', ta.attachment_title) AS profile_thumb_url_source,
            tp2.`page_name` AS page_name_target, ta2.`attachment_title` AS attachment_title_target,
            CONCAT('{0}/politisi/index/', tp2.page_id) AS profile_target_url,
            CONCAT('{0}/public/upload/image/politisi/', tp2.`page_id`, '/thumb/', ta2.attachment_title) AS profile_thumb_url_target
            FROM trelations_map tm
            LEFT JOIN tobject_page tp ON tp.`page_id` = tm.`source_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            LEFT JOIN tobject_page tp2 ON tp2.`page_id`=tm.`target_id`
            LEFT JOIN tcontent_attachment ta2 ON ta2.`content_id`=tp2.`profile_content_id`
            WHERE scandal_id = '".$scandal_id."' ORDER BY tanggal DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesi($this, $id_race=0){
        $wh = $id_race != 0 ? 't.id_race = "'.$id_race.'"' : '1=1'; 
        $sql = "SELECT t.* , td.`dname`, tkd.`kdname`, tc.`content_id`, tss.status_suksesi as 'status_akhir_suksesi'
            FROM trace t
            LEFT JOIN trace_daerah td ON td.`id_daerah`=t.`id_daerah`
            LEFT JOIN trace_kepala_daerah tkd ON tkd.`id_kepala_daerah` = t.`id_kepala_daerah`
            LEFT JOIN tcontent tc ON tc.`location` = t.`id_race` AND tc.`content_group_type` = 56
            LEFT JOIN trace_status_suksesi tss ON t.idtrace_status_suksesi= tss.idtrace_status_suksesi
            where '".$wh."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesistatus($this, $id_suksesi=0){
        $sql = "SELECT * FROM trace_status WHERE id_race = '".$id_suksesi."' ORDER BY `status` ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesikandidat($this, $status){
        $sql = "SELECT tk.*, tp.`page_name`, ta.`attachment_title`,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/badge/', ta.attachment_title) AS profile_badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS profile_icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/large/', ta.attachment_title) AS profile_large_url,
            tp2.`page_name` AS page_name_pasangan, ta2.`attachment_title` AS attachment_title_pasangan,
            CONCAT('{0}/public/upload/image/politisi/', tp2.`page_id`, '/badge/', ta2.attachment_title) AS profile_badge_pasangan_url,
            CONCAT('{0}/public/upload/image/politisi/', tp2.`page_id`, '/icon/', ta2.attachment_title) AS profile_icon_pasangan_url,
            CONCAT('{0}/public/upload/image/politisi/', tp2.`page_id`, '/large/', ta2.attachment_title) AS profile_large_pasangan_url
            FROM trace_kandidat tk
            LEFT JOIN tobject_page tp ON tp.`page_id`=tk.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            LEFT JOIN tobject_page tp2 ON tp2.`page_id`=tk.`page_id_pasangan`
            LEFT JOIN tcontent_attachment ta2 ON ta2.`content_id`=tp2.`profile_content_id`
            WHERE tk.`id_trace_status` = '".$status."'
            ORDER BY tk.`nomor_urut` ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
        
    }
    
    public function getsuksesikandidatpendukung($this, $id_kandidat){
        $sql = "SELECT tk.*, tp.`page_name` AS partai_name, ta.`attachment_title`,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/badge/', ta.attachment_title) AS partai_badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS partai_icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/large/', ta.attachment_title) AS partai_large_url
            FROM trace_kandidat_pendukung tk
            LEFT JOIN tobject_page tp ON tp.`page_id`=tk.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            WHERE idtrace_kandidat = '".$id_kandidat."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesilembaga($this, $status){
        $sql = "SELECT tk.*, tp.`page_name` AS lembaga_name, tu.`alias` AS 'lembaga_alias', ta.`attachment_title`,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/badge/', ta.attachment_title) AS partai_badge_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS partai_icon_url,
            CONCAT('{0}/public/upload/image/politisi/', tp.`page_id`, '/large/', ta.attachment_title) AS partai_large_url,
	(select update_date from trace_score where id_trace_lembaga = tk.id_trace_lembaga limit 1) as score_update_date
FROM trace_lembaga tk
            LEFT JOIN tobject_page tp ON tp.`page_id`=tk.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            LEFT JOIN tusr_account tu ON tp.`page_id` = tu.`user_id`
            WHERE id_trace_status = '".$status."' order by score_update_date desc";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesiphoto($this, $suksesi_id){
        $sql = "SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/suksesi/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/portrait/', ta.attachment_title) AS thumb_portrait_url
            FROM trace_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            WHERE tp.id_race = '".$suksesi_id."' ORDER BY ta.order ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesiphotoheadline($this, $suksesi_id){
        $sql = "SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/suksesi/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/portrait/', ta.attachment_title) AS thumb_portrait_url
            FROM trace_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            WHERE tp.id_race = '".$suksesi_id."' AND ta.headline = 1";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesiphotohotlist($this, $suksesi_id){
        $sql = "SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/suksesi/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url,
            CONCAT('{0}/public/upload/image/suksesi/thumb/portrait/', ta.attachment_title) AS thumb_portrait_url
            FROM trace_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            WHERE tp.id_race = '".$suksesi_id."' AND ta.hotlist = 1";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesisengketa($this, $suksesi_id){
        $sql = "SELECT * FROM trace_sengketa WHERE id_race = '".$suksesi_id."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesiscore($this, $lebaga_id, $kandidat_id){
        $sql = "SELECT * FROM trace_score
            WHERE id_trace_lembaga = '".$lebaga_id."' and idtrace_kandidat = '".$kandidat_id."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getprofiledetail($this){
        $sql = "SELECT `page_id`, `page_name`, `profile_content_id`, `entry_date`, `entry_by`,
              `last_update_date`, `user_page`, `website`, `about`, `personal_message`,
              `posisi`, `prestasi`, `prestasi_lain`, `contact_info`, `rating`, `publish`,
              `partai_id`, `node_size`, `node_color`
             FROM v_usr_politik";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsurvey($this, $where='1 = 1'){
        $sql = "SELECT s.*, t.`content_id`
            FROM survey s
            LEFT JOIN tcontent t ON t.`location` = s.`survey_id` AND t.`content_group_type` = 54
            where ".$where."
            ORDER BY s.begin_date DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsurveyquestion($this, $survey_id){
        $sql = "SELECT * FROM question WHERE survey_id = '".$survey_id."'
            ORDER BY question_id ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsurveyquestionoption($this, $question_option_id){
        $sql = "SELECT * FROM question_option WHERE 
            question_id	= '".$question_option_id."' 
                ORDER BY question_option_id ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsurveyquestionanswer($this, $question_id, $question_option_id){
        $sql = "SELECT * FROM question_answer WHERE question_id = '".$question_id."' 
            AND question_option_id = '".$question_option_id."' ORDER BY answer_date DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getnewslisttopic($this, $topic_id=0){
        $wh = $topic_id != 0 ? 'topic_id = "'.$topic_id.'"' : '1=1';
        $sql = "SELECT tt.content_id, tt.news_id, t.entry_date, tht.topic_id
            FROM tcontent_text tt
            JOIN tcontent t ON t.`content_id` = tt.`content_id`
            JOIN tcontent_has_topic tht ON tht.`content_id` = t.`content_id`
            WHERE ".$wh."
            ORDER BY t.`entry_date` DESC limit 1000 offset 0";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getProvince($this){
        $sql = "select * from master_province";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getDapil($this, $province_id){
        $sql = "select * from master_dapil where province_id = '".$province_id."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getPartai($this, $province_id){
        $sql = "SELECT p.partai, p.partai_id, tp.`profile_content_id`, tpa.alias,ta.`attachment_title` FROM structure_caleg AS p
LEFT JOIN tobject_page tp ON tp.`page_id` = p.`partai_id`
LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tp.`profile_content_id`
LEFT JOIN tusr_account tpa on tpa.user_id = tp.page_id
WHERE province_id = '".$province_id."' group by partai_id";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getCaleg($this, $province_id, $dapil_id, $partai_id){
        $sql = "SELECT p.partai, p.partai_id, p.caleg, p.caleg_id, tp.`profile_content_id`, ta.`attachment_title`, left(tp.about, 200) as about,
tp2.`profile_content_id` AS partai_profile_content_id, ta2.`attachment_title` AS partai_attachment_title
FROM structure_caleg AS p
LEFT JOIN tobject_page tp ON tp.`page_id` = p.`caleg_id`
LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tp.`profile_content_id`
LEFT JOIN tobject_page tp2 ON tp2.`page_id` = p.`partai_id`
LEFT JOIN tcontent_attachment ta2 ON ta2.`content_id` = tp2.`profile_content_id`
 WHERE province_id = '".$province_id."' AND dapil_id = '".$dapil_id."' AND p.partai_id = '".$partai_id."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getCalegScandal($this, $partai_id){
        $sql = "select count(*) co from tscandal_player where page_id = '".$partai_id."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getCalegNews($this, $partai_id){
        $sql = "select count(*) co from tcontent_has_politic where page_id = '".$partai_id."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
}

