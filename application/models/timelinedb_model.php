<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timelinedb_model extends CI_Model{
//    var $DB_Master;
    public function __construct()
    {
            parent::__construct();
//            $this->load->database('slave');
//            $this->DB_Master = $this->load->database('master', true);
    }
    
    public function getnewslistheadline($this, $limit=0, $offset=0){
        $sql = "SELECT tt.content_id, tt.news_id
            FROM tcontent_text tt
            JOIN tcontent t ON t.`content_id`=tt.`content_id`
            WHERE tt.type_news = 3 ORDER BY tt.type_news_date DESC LIMIT ".$limit." OFFSET ".$offset;
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getscandallisthome($this, $where='headline = 1', $order='head_date', $limit=20){
        $sql = "SELECT scandal_id FROM tscandal WHERE ".$where." ORDER BY ".$order." ASC LIMIT ".$limit;
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesilisthome($this, $where='hotlist = 1', $order='hot_date', $limit=10){
        $sql= " SELECT id_race FROM trace WHERE ".$where." ORDER BY ".$order." DESC LIMIT ".$limit;
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getprofilelisthome($this, $where=1, $limit=30){
        $sql = "SELECT page_id
            FROM tobject_page WHERE publish = '".$where."'
            ORDER BY publish_date DESC LIMIT ".$limit;
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getscandallist($this){
        $sql = "SELECT * FROM
            (
                SELECT id, MAX(tanggal) AS tanggal FROM
                (
                    SELECT * FROM
                    (
                    SELECT * FROM
                    (
                        SELECT scandal_id AS id, MAX(`date`) AS tanggal
                        FROM tscandal
                        WHERE main_scandal = 0 AND publish = 1 AND scandal_id !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.headline = 1 ORDER BY tt.head_date DESC LIMIT 1
                        )AND scandal_id !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.`hotlist` = 1 ORDER BY tt.`hot_date` DESC LIMIT 1
                        )
                        GROUP BY scandal_id
                    )AS scandal ORDER BY tanggal DESC
                    )AS s UNION ALL
                    SELECT * FROM
                    (
                    SELECT * FROM
                    (
                        SELECT t.main_scandal AS id, MAX(t.date) AS tanggal
                        FROM tscandal t
                        LEFT JOIN tscandal ts ON ts.scandal_id = t.main_scandal
                        WHERE t.main_scandal != 0 AND ts.publish = 1 AND t.main_scandal !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.headline = 1 ORDER BY tt.head_date DESC LIMIT 1
                        )AND t.main_scandal !=
                        (
                        SELECT tt.scandal_id
                        FROM tscandal tt WHERE tt.`hotlist` = 1 ORDER BY tt.`hot_date` DESC LIMIT 1
                        )
                        GROUP BY t.main_scandal
                    )AS kronologi ORDER BY tanggal DESC
                    )AS k
                )AS ld GROUP BY id
            )AS mk ORDER BY tanggal DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getsuksesilist($this){
        $sql = "SELECT id_race FROM trace WHERE publish = 1 ORDER BY tgl_pelaksanaan DESC, id_race DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function get_home_total($this){
        $sql = "SELECT * FROM
            (
                SELECT 'like' AS tipe, SUM(total) as total FROM
                (
                    SELECT * FROM
                    (
                        SELECT COUNT(*) AS total FROM tcontent_like
                    )AS content_like UNION ALL
                    SELECT * FROM
                    (
                        SELECT COUNT(*) AS total  FROM page_like
                    )AS page_like

                ) AS lik UNION ALL

                SELECT 'comment' AS tipe, SUM(total) as total FROM
                (
                    SELECT * FROM
                    (
                        SELECT COUNT(*) AS total FROM tcontent_comment
                    )AS content_comment UNION ALL
                    SELECT * FROM
                    (
                        SELECT COUNT(*) AS total FROM page_comment
                    )AS page_comment

                ) AS commen UNION ALL

                SELECT 'user' AS tipe, total FROM
                (
                    SELECT COUNT(*) AS total FROM tusr_account
                    WHERE account_type = 0
                )AS us UNION ALL

                SELECT 'skandal' AS tipe, total FROM
                (
                    SELECT COUNT(*) AS total FROM tscandal WHERE `tscandal`.`main_scandal` = 0
                )AS skandal UNION ALL

                SELECT 'suksesi' AS tipe, total FROM
                (
                    SELECT COUNT(*) AS total FROM trace
                )AS suksesi UNION ALL

                SELECT 'tokoh' AS tipe, total FROM
                (
                    SELECT COUNT(*) AS total FROM v_usr_politik
                )AS tokoh UNION ALL
                SELECT 'news' AS tipe, total FROM
                (
	             SELECT COUNT(*) AS total FROM tcontent WHERE content_group_type = 12
                )AS news UNION ALL
                SELECT 'surpey' AS tipe, total FROM
                (
                     SELECT COUNT(*) AS total FROM question
                ) AS surpey

            )AS home_total";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getcontentscore($this, $content_id){
        $sql = "SELECT * FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, content_id, COUNT(tc.`content_id`) AS total
                    FROM tcontent_comment tc WHERE `status` = 0 AND content_id = '%s'
                )AS `comment` UNION ALL
                SELECT * FROM
                (
                    SELECT 'like' AS tipe, content_id, COUNT(`content_id`) AS total
                    FROM tcontent_like tl WHERE content_id = '%s'
                )AS `like` UNION ALL
                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, content_id, COUNT(`content_id`) AS total
                    FROM tcontent_dislike tl WHERE content_id = '".$content_id."'
                )AS `dislike`
            )AS last_count";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getlogchanged($this, $where=0){
        $wh = $where != 0 ? $where : '1=1';
        $sql = " SELECT * FROM
            (
                SELECT * FROM tb_changed_redis WHERE ".$wh." and status = 1 ORDER BY insert_date DESC
            )AS ch GROUP BY id";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function updatelogchanged($this, $id=0, $tgl=''){
        $sql= "update tb_changed_redis
            set status = 2
            where id = '".$id."' AND insert_date <= '".$tgl."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function getStructureNode($this, $parent=0){
        $wh = $parent != 0 ? 'parent_node = "'.$parent.'"' : 'parent_node IS NULL';
        $sql = "SELECT id_structure_node, structure_name, page_id
            FROM structure_node
            WHERE ".$wh." ORDER BY level_node ASC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function truncateStructureCaleg($this){
        $sql = "TRUNCATE structure_caleg";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function updateStructureCalegDapil($this){
        $sql = "UPDATE structure_caleg sc SET dapil_id = (SELECT mp.dapil_id 
            FROM master_dapil mp WHERE mp.province_id = sc.province_id AND 
            mp.`dapil`=sc.`dapil`)";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function updateStructureCalegProvince($this){
        $sql = "UPDATE structure_caleg sc SET province_id = 
            (SELECT mp.province_id FROM master_province mp WHERE 
            mp.province = sc.province)";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function setStructureCaleg($this, $dt){
        $sql = "INSERT INTO structure_caleg
            set partai = '".$dt['partai']."',
             partai_id = '".$dt['partai_id']."',
             province = '".$dt['province']."',
             province_id = '',
             dapil = '".$dt['dapil']."',
             caleg = '".$dt['caleg']."',
             caleg_id = '".$dt['caleg_id']."'";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
}