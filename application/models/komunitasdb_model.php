<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Komunitasdb_model extends CI_Model{
//    var $DB_Master;
    public function __construct()
    {
            parent::__construct();
//            $this->load->database('slave');
//            $this->DB_Master = $this->load->database('master', true);
    }
    
    public function get_users($this, $account_id=0){
        $wh = $account_id != 0 ? "ta.account_id = '".$account_id."'" : '1=1';
        $sql = "SELECT ta.`account_id`, ta.`user_id`, ta.`display_name`, ta.`registration_date`, ta.`last_login_date`, ta.`email`, ta.`fname`, ta.`lname`,
            ta.`gender`, ta.`birthday`, ta.`current_city`, tp.`profile_content_id`, tp.`about`, tp.`personal_message`, tat.`attachment_title`
            FROM tusr_account ta
            LEFT JOIN tobject_page	tp ON tp.`page_id` = ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`
            WHERE ".$wh." and ta.`account_type` = 0";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function get_foto_album($this, $content_id=0){
        $sql = "SELECT tc.* , tat.`attachment_title`
            FROM tcontent tc
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tc.`content_id`
            WHERE group_content_id = '".$content_id."' ORDER BY entry_date DESC";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function get_wall_list($this, $account_id, $page_id, $limit=0){
        $lm = $limit != 0 ? 'LIMIT '.$limit.' OFFSET 0' : '';
        $sql = "SELECT st.*,
            tat.attachment_title, c_blog.content_blog,
            ta.`user_id` AS from_page_id, ta.account_type as from_account_type, tp.page_name AS from_page_name, tc.attachment_title AS from_attachment_title,
            tp2.page_id AS to_page_id, ta2.account_type as to_account_type, tp2.page_name AS to_page_name, tc2.attachment_title AS to_attachment_title
            FROM
            (
                SELECT * FROM
                (
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND (t.account_id = '".$account_id."' OR t.page_id = '".$page_id."')
                        AND t.`status` = 0
                    )AS own_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND t.account_id IN
                        (
                            SELECT friend_account_id FROM tusr_friend WHERE `account_id` = '".$account_id."' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`friend_account_id`
                            WHERE tf.`account_id` = '".$account_id."'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND t.account_id IN
                        (
                            SELECT account_id
                            FROM tusr_friend
                            WHERE `friend_account_id` = '".$account_id."' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
                            WHERE tf.`friend_account_id` = '".$account_id."'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status_2 UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND (t.account_id = '".$account_id."' OR t.page_id = '".$page_id."')
                        AND t.`status` = 0
                    )AS own_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND t.account_id IN
                        (
                            SELECT friend_account_id FROM tusr_friend WHERE `account_id` = '".$account_id."' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`friend_account_id`
                            WHERE tf.`account_id` = '".$account_id."'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND t.account_id IN
                        (
                            SELECT account_id
                            FROM tusr_friend
                            WHERE `friend_account_id` = '".$account_id."' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
                            WHERE tf.`friend_account_id` = '".$account_id."'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status_ph
                )AS stat GROUP BY content_id
            )AS st
            LEFT JOIN tcontent_attachment tat ON tat.content_id = st.content_id
            LEFT JOIN tcontent_blog c_blog ON c_blog.content_id = st.content_id
            LEFT JOIN tusr_account ta ON ta.`account_id` = st.account_id
            LEFT JOIN tobject_page tp ON tp.page_id = ta.user_id
            LEFT JOIN tcontent_attachment tc ON tc.content_id = tp.profile_content_id
            LEFT JOIN tobject_page tp2 ON tp2.page_id = st.page_id
            LEFT JOIN tusr_account ta2 ON ta2.`user_id` = tp2.page_id
            LEFT JOIN tcontent_attachment tc2 ON tc2.content_id = tp2.profile_content_id
            ORDER BY entry_date DESC ".$lm;
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function get_wall_list_content_id($this, $account_id, $page_id, $limit=0){
        $lm = $limit != 0 ? 'LIMIT '.$limit.' OFFSET 0' : '';
        $sql = "SELECT st.*
            FROM
            (
                SELECT * FROM
                (
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.entry_date FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND (t.account_id = '".$account_id."' OR t.page_id = '".$page_id."')
                        AND t.`status` = 0
                    )AS own_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.entry_date FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND t.account_id IN
                        (
                            SELECT friend_account_id FROM tusr_friend WHERE `account_id` = '".$account_id."' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`friend_account_id`
                            WHERE tf.`account_id` = '".$account_id."'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.entry_date FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND t.account_id IN
                        (
                            SELECT account_id
                            FROM tusr_friend
                            WHERE `friend_account_id` = '".$account_id."' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
                            WHERE tf.`friend_account_id` = '".$account_id."'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status_2 UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.entry_date FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND (t.account_id = '".$account_id."' OR t.page_id = '".$page_id."')
                        AND t.`status` = 0
                    )AS own_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.entry_date FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND t.account_id IN
                        (
                            SELECT friend_account_id FROM tusr_friend WHERE `account_id` = '".$account_id."' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`friend_account_id`
                            WHERE tf.`account_id` = '".$account_id."'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.entry_date FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND t.account_id IN
                        (
                            SELECT account_id
                            FROM tusr_friend
                            WHERE `friend_account_id` = '".$account_id."' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
                            WHERE tf.`friend_account_id` = '".$account_id."'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status_ph
                )AS stat GROUP BY content_id
            )AS st
            ORDER BY entry_date DESC ".$lm;
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function get_friend_users($this, $account_id=0){
        $sql = "SELECT grp.*, ta.user_id AS page_id, ta.gender, ta.display_name AS
            page_name, tp.about, tat.attachment_title FROM
            (
                SELECT * FROM
                (
                    SELECT friend_account_id  FROM tusr_friend WHERE
                    account_id = '".$account_id."'
                )AS fr UNION ALL
                SELECT * FROM
                (
                    SELECT account_id AS friend_account_id FROM tusr_friend WHERE
                    friend_account_id = '".$account_id."'
                )AS fr2
            ) AS grp
            LEFT JOIN tusr_account ta ON ta.`account_id` = grp.friend_account_id
            LEFT JOIN tobject_page tp ON tp.`page_id` = ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`
            GROUP BY friend_account_id";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function get_content_is_comment($this, $account_id){
        $sql = "SELECT content_id FROM tcontent_comment WHERE account_id = '".$account_id."' GROUP BY content_id";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
    
    public function get_page_is_comment($this, $account_id){
        $sql = "SELECT page_id FROM page_comment WHERE account_id = '".$account_id."' GROUP BY page_id";
        $result = $this->db->query($sql);
        return json_encode($result->result_array());
    }
}