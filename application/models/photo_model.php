<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photo_model extends CI_Model{

	var $DB_Master;

	public function __construct()
	{
		parent::__construct();
		$this->load->database('slave');
		//$this->DB_Master = $this->load->database('master', true);
	}
    
    /*
    public function __call($method, $args) {
        
        $haystack = $method;
        $needle = '_cache_';
        $needle_plus = '_cache';
        
        $rstrstr = substr($haystack, 0,strpos($haystack, $needle));
        
        $pos = strpos($haystack, $needle);
        
        if (is_int($pos)) {
            
            $fstrstr = substr($haystack, $pos + strlen($needle));
            
        } else {
            
            $fstrstr = $pos;
        }
        
        $params_call = explode('_',$fstrstr);
        
        if($rstrstr != '' && method_exists(__CLASS__, $rstrstr) == true)
        {
        
            $method_reflect = new ReflectionMethod($this, $rstrstr);
            
            if(!$method_reflect->isPublic())
            {
                return false;
            }
            
            
            $ttl = (int)$params_call[0];
            
            if(is_null($ttl) || $ttl === false || $ttl === 0)
            {
                $ttl = 600;
            }
            
            $cache_config = array(
                'container' => 'File',
                'ttl' => $ttl, // Cache will expired in seconds
                'File' => array(
                    'store' => APPPATH.'cache'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.strtolower(__CLASS__),
                    'auto_clean'      => false,
                    'auto_clean_life' => 900,
                    'auto_clean_all'  => false,
                    'max_estimate_process' =>  0, // Cache process about 0 second
                    'debug'  => false
                )
            );
            
            $lib_name = 'smartcache_photo_'.$ttl;
    
    		$this->load->library('Smartcache',$cache_config,$lib_name);
            
            
            $args_implode = @implode(',',$args);

	        if($args_implode == 'Array')
	        {
	            $args_implode = md5(serialize($args));
	        }
            
            if(isset($params_call[2]))
            {
                
            
                if(strtolower(@$params_call[2]) == 'key')
                {
                    $key_string = __CLASS__.'::'.$rstrstr.'::'.str_replace("_{$params_call[2]}", "", $method).'::'.$args_implode;
                    
                    
                    $key = $this->{$lib_name}->generatekey($key_string);
                    
                    return $key;
                }
                
                if(strtolower(@$params_call[2]) == 'delete')
                {
                     $this->{$lib_name}->delete(@$args[0]);
                     
                     return true;
                }
            }

            $key_string = __CLASS__.'::'.$rstrstr.'::'.$method.'::'.$args_implode;
    
            $key = $this->{$lib_name}->generatekey($key_string);

            if (($data = $this->{$lib_name}->fetch($key)) === false){
                
                $data = call_user_func_array(array(__CLASS__,$rstrstr), $args);
                    
                if(strtolower(@$params_call[1]) == 'array')
                {
                    $data = $data->result_array();
                    
                } else if(strtolower(@$params_call[1]) == 'row')
                {
                    $data = $data->row();
                    
                }
    
                $this->{$lib_name}->store($key, $data);
            }
    
            return $data;
            
        } else {
            
            return false;
        }
    }
	*/	
	public function getContentAttachment($id) {
		$this->db->where('content_id', $id);
		$query = $this->db->get('tcontent_attachment');
		return $query;
	}

	public function getProfilePicActive($account_id) {
		$sql = 'SELECT user_id FROM tusr_account
				WHERE account_id = ? LIMIT 1';
		$query = $this->db->query($sql, array($account_id));

		$page_id = '';
		if($query->num_rows() > 0) $page_id = $query->row()->user_id;

		$query = $this->db->query('SELECT `tcontent_attachment`.* FROM `tobject_page` LEFT JOIN `tcontent` ON (`tcontent`.`content_id` = `tobject_page`.`profile_content_id`) LEFT JOIN `tcontent_attachment` ON (`tcontent_attachment`.`content_id` = `tcontent`.`content_id`) WHERE `tobject_page`.`page_id` = ? AND `content_group_type` = 22', array($page_id));
		$rs = $query->result_array();
		if(count($rs) > 0) {
			return $rs[0];
		} else {
			return false;
		}
	}

}