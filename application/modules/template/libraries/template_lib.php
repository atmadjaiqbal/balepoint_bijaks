<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template_lib {

	protected  $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('cache');
	}
	
	public function getSidebarKomunitasPolitisi()
	{
		$this->CI->load->model('Politik_model');
		$cm 					= $this->CI->load->module('ajax');
		$data['sidebar'] 	= array();

		$data['sidebar'][0]['title'] = 'Hot Profile';
		$data['sidebar'][0]['list'] 	= $this->CI->Politik_model->getHotPolitisi(5, 1); 
		if (!empty($data['sidebar'][0]['list'] )) {
			foreach ($data['sidebar'][0]['list'] as $k => $v) {
				$data['sidebar'][0]['list'][$k]['count_page'] = $cm->count_page($v['page_id'], 2);	
			}
		}
		

		$data['sidebar'][1]['title'] = 'Politisi Favorit';
		$data['sidebar'][1]['list'] 	= $this->CI->Politik_model->getFavoritPolitisi(5, 1); 
		if (!empty($data['sidebar'][1]['list'])) {
			foreach ($data['sidebar'][1]['list'] as $k => $v) {
				$data['sidebar'][1]['list'][$k]['count_page'] = $cm->count_page($v['page_id'], 2);	
			}
		}


		$data['sidebar'][2]['title'] = 'Politisi Tidak Favorit';
		$data['sidebar'][2]['list'] 	= $this->CI->Politik_model->getUnFavoritPolitisi(5, 1); 
		if (!empty($data['sidebar'][2]['list'] )) {
			foreach ($data['sidebar'][2]['list']  as $k => $v) {
				$data['sidebar'][2]['list'] [$k]['count_page'] = $cm->count_page($v['page_id'], 2);	
			}
		}


		$data['sidebar'][3]['title'] = 'Politisi dibicarakan';
		$data['sidebar'][3]['list'] 	= $this->CI->Politik_model->getCommentPolitisi(5, 1); 
		if (!empty($data['sidebar'][3]['list'] )) {
			foreach ($data['sidebar'][3]['list']  as $k => $v) {
				$data['sidebar'][3]['list'] [$k]['count_page'] = $cm->count_page($v['page_id'], 2);	
			}
		}

	   return $this->CI->load->view('template/sidebar/komunitas_politisi', $data, TRUE);
	}

}

/* End of file file.php */
