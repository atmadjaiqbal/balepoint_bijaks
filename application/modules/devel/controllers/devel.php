<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Devel extends Application {


    public function __construct()
    {
        parent::__construct();

        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);

        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->model('content_model');
        $this->load->library('politik/politik_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();

        $qs = $_GET['pwd'];
        if($qs != 'bijaks321'){
            show_404();
        }
        // echo '<pre>';
        // var_dump($qs);
        // echo '</pre>';

    }

    public function komublog()
    {
        $this->load->library('core/cache_lib');
        $this->load->helper('imagefull');
        $data				 = $this->data;
        $data['scripts']     = array('bijaks.home.js', 'bijaks.js', 'highcharts.js', 'bijaks.survey.js');

        $html['html']['content']  = $this->load->view('komublog', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = '';
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);

    }

    public function pilpres()
    {
        $this->load->library('core/cache_lib');
        $this->load->helper('imagefull');
        $data                = $this->data;
        $data['scripts']     = array('bijaks.home.js', 'bijaks.js', 'highcharts.js', 'bijaks.survey.js');

        $arrSuksesi = array();
        $resultSuksesi = $this->redis_slave->lrange("suksesi:list:hot", 0, 0);
        if(count($resultSuksesi))
        {
            foreach($resultSuksesi as $key => $value)
            {
                $rowSuksesi = $this->redis_slave->get('suksesi:detail:'.$value);
                $arrsuksesi = @json_decode($rowSuksesi, true);
                $rsSuksesi[$key] = $arrsuksesi;
            }
        }

        //** More suksesi slider **//
        $arrMore = array();
        $resultMore = $this->redis_slave->lrange("suksesi:list:all", 2, 32);
        if(count($resultMore))
        {
            foreach($resultMore as $key => $value)
            {
                $rowMore = $this->redis_slave->get('suksesi:detail:'.$value);
                $arrMore = @json_decode($rowMore, true);
                $rsMore[$key] = $arrMore;
            }
            array_multisort($rsMore, SORT_DESC, $resultMore);
        }
        $data['suksesi'] = $rsSuksesi;
        $data['moresuksesi'] = $rsMore;

        // echo '<pre>';
        // var_dump($data['suksesi']);
        // echo "</pre>";

        // $this->output->set_content_type('application/json');
        // $this->output->set_output(json_encode($data['suksesi']));


        // exit();

        // return $this->load->view('home/suksesi', $data, true);

        $html['html']['content']  = $this->load->view('pilpres', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = '';
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }


    public function BlogLatest()
    {

        $data['scripts'] = array('bijaks.js');
        $topic_id = 'blog';
        $page = $this->uri->segment(3); $limit = 200;
        //if($page < 30)
        //{
        //$offset = ($limit*intval($page)) - $limit;
        $offset = 0;
        $data['blogs'] = $this->content_model->getBlogList('latest', $offset, $limit);
        $data['section']    = 'opini';
        return $this->load->view('devel/blog_latest', $data);
        //}
    }


}
