<style type="text/css">
	.span12 {
		margin-left: 0px;
		width: 100%;
	}
	.span3 {
		margin-left: 15px;
	}
	.owl-next img {
		margin-left: 68px;
		margin-top: -55px;
	}
	.owl-prev img {
		margin-left: -2px;
		margin-top: -55px;
	}
	.owl-stage-outer {
		padding-left: 5px;
	}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.carousel.css')?>" />
<script type="text/javascript" language="javascript" src="<?php echo base_url('assets/js/owl.carousel.min.js')?>" ></script>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
			<div class="span12 text-center"><h4>SIMAK PERISTIWA LAIN :</h4></div>
			<div class="clear"></div>
			<div id="heading_carousel">
			    <div id="peristiwa_carousel" class="owl-carousel">
					<?php 
					if(!empty($peristiwaIndex)){
						foreach($peristiwaIndex as $pi) { ?>	
			            <div style="margin-bottom: 5px;height: 140px;border: 2px solid lightgray;width: 220px;padding: 5px 5px;">
			            	<a href="<?php echo $pi['url']; ?>">
								<img src="<?php echo $pi['thumbUrl']?>" style="max-height: 100px;"/>
								<div style="width: 220px;">
									<?php echo strtoupper($pi['title']);?>
								</div>
							</a>
						</div> <?php
			        	}
			        }
			        ?>
			    </div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    $("#peristiwa_carousel").owlCarousel({

        autoPlay: 1000, //Set AutoPlay to 3 seconds
        navigation: true,
        items : 4,
        itemsDesktop : [1199,5],
        itemsDesktopSmall : [979,5],
        navigationText: [
            "<i >XXXXXXXXXXXXXX</i>",
            "<i >YYYYYYYYYYYYY</i>"
        ],

    });

    $(document).ready(function(){
        $("#peristiwa_carousel .owl-controls .owl-nav .owl-prev").html('<img src="<?php echo base_url('assets/images/left_transparent_control.png');?>"/>')
        $("#peristiwa_carousel .owl-controls .owl-nav .owl-next").html('<img src="<?php echo base_url('assets/images/right_transparent_control.png');?>"/>')


    })
</script>