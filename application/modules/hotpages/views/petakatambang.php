<?php
$data = [
    'block1'=>[
        'title'=>'Tragedi Tambang di Selok Awar-Awar',
        'narasi'=>'
        <img src="http://www.bangsaonline.com/images/uploads/berita/700/8d9037c33d1ec23b32c2cb2d149b554d.jpg" class="pic">
        <p>Aksi  penolakan penambangan pasir ilegal di Selok Awar-Awar, Kabupaten Lumajang, Jatim 
        berujung pada tewasnya seorang aktivis lingkungan dan mencederai seorang aktivis lainnya. 
        Dua aktivitis tersebut yakni Salim Kancil dan Tosan tergabung dalam Forum Komunikasi Masyarakat 
        Peduli Desa Selok Awar-Awar. Mereka dikenal cukup vokal dalam menolak aktivitas pertambangan 
        pasir yang merusak lingkungan.</p>
        <p>Pengeroyokan sadis yang berujung maut tersebut dilakukan oleh oknum Pemerintah Desa. Kepala Desa 
        Selok Awar-Awar, beserta puluhan anak buahnya menganiaya secara sadis dan brutal dua aktivis tersebut 
        hingga tewas. Aksi tersebut, dilakukan sebagai upaya untuk mengamankan bisnis tambang ilegal. Kuat 
        dugaan, banyak pihak yang mendapat untung dari tambang pasir ilegal tersebut. Tidak tanggung-tanggung, 
        oknum-oknum  pemerintah dari tingkat Desa, Kecamatan bahkan sampai pada oknum  DPRD ikut menikmati hasil 
        tambang pasir besi tersebut. Tidak hanya itu,  Kepolisian Lumajang  dan oknum TNI juga diduga menerima 
        suap untuk membekingi aktifitas penambangan ilegal di wilayah itu.</p>
        <img src="https://aws-dist.brta.in/2015-09/original_700/0_0_1000_660_17ff74b91e8d1936c6a90d058f8c6f8c957cee32.jpg" class="pic2">
        <p>Kasus penganiayaan serta pembunuhan sadis yang dialami dua aktivis lingkungan yang berjuang mempertahankan 
        tanah dan lingkungannya dari aktivitas pertambangan liar mendapat banyak kecaman. Kutukan keras juga 
        dilayangkan karena tragedi berdarah tersebut terjadi karena pihak kepolisian abai sehingga jatuhnya korban jiwa.</p>'

    ],

    'block2'=>[
        'title'=>'Menolak Tambang Nyawapun Tumbang',
        'narasi'=>'
        <img src="http://img.hello-pet.com/assets/uploads/2015/09/Salim-Kancil1.jpg" class="pic2">
        
        <p>"Tragedi berdarah di Lumajang, Jawa Timur dipicu oleh penolakan masyarakat terhadap aktivitas 
        pertambangan yang  merusak lingkungan dan lahan pertanian milik warga. Eksploitasi pasir emas di 
        pantai tersebut berdampak buruk pada lahan pertanian sehingga warga menjadi resah.</p>  
        <p>Diketahui, bahwa tambang pasir tersebut merupakan pertambangan ilegal. Selama ini pengelolaan 
        tambang dikendalikan oleh  aparat dari  desa dan kecamatan. Selain itu, banyak "orang kuat" membekingi 
        aktivitas pertambangan tersebut sehingga mampu beroperasi cukup lama. Kalangan TNI, Polisi dan Anggota 
        DPRD diduga ikut menikmati suap tambang ilegal tersebut.</p>
        <p>Beberapa kali warga melakukan protes namun upaya tersebut selalu gagal. Bahkan intensitas penggerukan 
        pasir semakin meningkat dengan skala kerusakan lingkungan  yang ditimbulkan semakin luas.</p>
        <img src="http://media.viva.co.id/thumbs2/2015/10/04/340504_foto-tosan_663_382.jpg" class="pic">
        <p>Para petani  kerap mendapat ancaman dan intimidasi dari anak buah kepala desa jika melakukan aksi 
        penolakan terhadap aktivitas penambangan pasir. Tewasnya Salim Kancil memperlihatkan betapa warga tidak 
        memiliki daya untuk menghentikan aktivitas pertambangan liar tersebut. Sebelum tewas, Salim Kancil disiksa 
        secara brutal dan keji oleh puluhan preman suruhan Kades Selok Awar-Awar."</p>'
    ],

    'block4'=>[
        'title'=>'QUOTE PEMBANTAI',
        'narasi'=>'
        <img src="http://assets2.jpnn.com/picture/normal/20150930_093908/093908_226420_salim_kades.jpg" class="pic">
         
        <p>"Itu saya lakukan untuk mengantisipasi adanya aksi demo, memang Salim cs berencana melakukan demo antitambang). 
        Saat itu dihadiri babinsa, babinkamtibmas, dan orang-orang yang terlibat pengeroyokan. Tapi, saya tak pernah 
        merencanakan untuk membunuh. Murni untuk kerja bakti.</p>(Hariyono, Kades Selok Awar-awar)'
    ],

    'block3'=>[
        'title'=>'Potensi Konflik di Tambang Pasir',
        'narasi'=>'
        <img src="http://fajar.co.id/wp-content/uploads/2015/10/Tambang-Pasir-Lumajang.jpg" class="picprofil">
        
        <p class="rightcol">Maraknya lokasi tambang pasir yang berjejeran di sepanjang pantai selatan, khususnya di Kabupaten 
        Lumajang berpotensi memicu konflik antara warga pemilik lahan dan pengelola tambang. Khusus untuk 
        penambangan batu dan pasir, kabupaten itu menyatakan memiliki bahan galian pasir dan batu bangunan seluas 
        82,50 hektare. Areal pasir dan batu yang dieksploitasi baru 15 hektare dengan volume 239.065 m³ atau hanya 
        4% dari kapasitas yang tersedia.</p>
        <p class="rightcol">Fakta tersebut menegaskan bahwa ruang hidup dan lingkungan masyarakat semakin tergerus oleh aktivitas 
        tambang ilegal. Sehingga potensi konflik akibat adanya aktivitas tambang liar yang merusak lahan pertanian 
        milik warga akan meningkat.</p>  
        <img src="http://cdn.tmpo.co/data/2011/02/14/id_64643/64643_620.jpg" class="picprofil">
        <p class="rightcol">Kasus di Selok Awar-Awar hanya salah satu kasus yang mencuat ke publik. Sebelumnya di  desa Wotgalih, 
        Kecamatan Yosowilangun menimbulkan konflik. Kasus serupa juga terjadi di desa Pandanarum dan Pandanwangi, 
        kecamatan Tempeh.</p>'
    ],

    'block8'=>[
        'title'=>'Otak Pembunuhan dan Peran Tim 12',
        'narasi'=>'
        <img src="http://pojokpitu.com/yp-gbr-news/15298_29-09-2015_21-05-43_.jpg" class="pic">
        <p>Desa Selok Awar-awar merupakan desa tambang pasir yang menyimpan banyak kekayaan alam. Tidak mengherankan 
        jika banyak oknum yang tergiur untuk mengoptimalkan potensi alam tersebut dengan cara membuka lahan tambang. 
        Hariyono, Kades Selok Awar-Awar salah satu pengelola tambang pasir yang ada di Kab. Lumajang.</p>   
        <p>Pria berkumis itu kemudian menjalankan bisnis ladang tambangnya dengan tangan besi.  Untuk menjaga 
        pungutan tetap langgeng, Hariyono menggunakan perdes sebagai dalih untuk mengekploitasi pasir di wilayahnya. 
        Dan ada dugaan bahwa ia melakukan penambangan pasir tanpa izin karena dekat  aparatur pemerintahan daerah Kab Lumajang, 
        bahkan oknum kepolisian dari Polres dan Polsek. Selain itu,  Ia juga menggunakan kelompok preman yang dinamai Tim 12.</p>   
        <p>Tim yang terdiri dari para preman itu ditugasi untuk menjaga portal-portal tempat truk keluar masuk mengangkut pasir 
        dari Watu Pecak. Ada tiga portal yang dijaga oleh tim 12. Dua di antaranya berada di luar Desa Selok Awar-awar. 
        Tarif yang dikenakan pun naik dari Rp 15 ribu menjadi Rp 35 ribu. Apabila dalam sehari saja ada 300 truk yang 
        melintasi portal, setidaknya Hariyono bisa mengeruk Rp 10 juta. Tidak sembarang orang bisa melewati portal yang dijaga 
        ketat tersebut, bahkan petugas Perum Perhutani.</p>
        <img src="http://www.jpnn.com/picture/normal/20151002_073546/073546_861616_Pembunuh_Tim_12_Gun_JP.jpg" class="pic2">

        <p>Usaha tambang pasir semakin lancar dengan keberadaan Desir sebagai pemimpin tim 12. Desir merupakan ketua LSM 
        di Desa tersebut. Dia punya wewenang kerjasama dengan Perhutani untuk mengelola hutan. Tim 12 pimpinan Desir inilah yang 
        kerap meneror dan mengintimidasi warga yang berani menolak aktivitas pertambangan di wilayah tersebut. Atas intruksi 
        Sang Kades, Tim 12 tersebut membunuh Salim kancil di depan banyak orang dan menganiaya Tosan sampai kritis.</p>'
    ],

    'block9'=>[
        'title'=>'Potensi Alam Terbaik se Indonesia',
        'narasi'=>'
          <img src="http://energitoday.com/uploads//2013/02/GUNUNG-SEMERU.jpg" class="pic2">
          
          <p>Daerah Lumajang, terkenal sebagai penghasil pasir terbaik di Indonesia yang disebabkan 
          pula keberadaan Gunung Semeru yang terletak di Kabupaten Lumajang. Adanya gunung tertinggi 
          di Pulau Jawa  itu telah membawa berkah dengan berlimpahnya bahan galian golongan C khususnya 
          jenis pasir, batu, coral dan sirtu yang tak pernah habis dan berhenti mengalir.</p>
          <p>Potensi bahan Galian golongan C jumlahnya akan bertambah terus sesuai dengan kegiatan rutin 
          Gunung Semeru yang mengeluarkan material kurang lebih 1 (satu) juta M3/tahun. Bukan saja 
          kuantitasnya yang sangat besar, namun kualitasnya juga sangat baik dan terbaik di Jawa Timur. 
          Berbagai penelitian menyimpulkan, unggulnya kualitas pasir Gunung Semeru karena kandungan tanah 
          (lumpur) sedikit, butiran pasirnya standar serta warna dan daya rekatnya yang baik.</p>
          <p>Lokasi  penambangan pasir dan batu cukup banyak, di antaranya di sepanjang Sungai/Kali 
          Rejali, Kali Regoyo, dan Kali Glidig. Tepatnya berada di Kecamatan Candipuro, Pasirian, Tempursari 
          dan Pronojiwo. Areal bahan tambang/galian pasir dan batu bangunan 82,50 ha dengan volume 5.976.625 m. 
          Areal pasir dan batu yang di eksploitasi baru 15 ha dengan volume 239.065 m atau hanya 4% dari 
          kapasitas yang tersedia.</p>
          <img src="http://lumajangsatu.com/foto_berita/60pasir-lumajang.jpg" class="pic">
          <p>Khusus mengenai areal tambang pasir besi yang dimiliki Lumajang mencapai 2.650 ha. Lokasinya 
          memanjang dalam satu deret di sepanjang pantai selatan. Tepatnya, di pantai selatan Kecamatan 
          Yosowilangun, Kecamatan Kunir, Kecamatan Tempeh dan Kecamatan Pasirian. Di samping pasir besi 
          Kabupaten Lumajang tersedia potensi tambang emas. Setidaknya ada 2 Desa yang dinyatakan memiliki 
          kandungan emas. Yaitu Desa Bulurejo, dan Desa Oro-Oro Ombo.  Keduanya berada di Kecamatan Tempursari, 
          60 Km dari jantung kota Lumajang.</p>
          <p>Potensi mineral pasir besi di wilayah itu sangat besar, Hasil survey geologi, potensi pasir 
          besi mencapai luasan sekitar 12.500 hektare.</p>'
    ],

    //<div ><img src="http://bharatanews.com/foto_berita/46UANG.jpg" class="pic2"></div>
    'block10'=>[
        'title'=>'Aliran Dana Gratifikasi Tambang ',
        'narasi'=>'
          
          <p>Hasil tambang pasir disinyalir mengalir kebeberapa aparat pemerintah dan oknum polisi serta TNI.</p>   
          <p>
          <div style="float: left;width: 17px;">1.</div>
          <div style="float: left;width: 590px;">Kepala Bagian Ekonomi Pemkab Lumajang, Ir. Ninis Ridyawati ikut diperiksa atas dugaan 
          penerimaan gratifikasi tambang illegal</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 17px;">2.</div>
          <div style="float: left;width: 339;">Anggota DPRD Lumajang, Sugianto dikabarkan menerima dana senilai Rp. 4 Juta</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 17px;">3.</div>
          <div style="float: left;width: 339;">Kapolsek Pasirian, AKP Sudarminto dikabarkan menerima dana senilai Rp. 6 Juta</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 17px;">4.</div>
          <div style="float: left;width: 339;">Kanit Reskrim Pasirian, Ipda Samsul Hadi dikabarkan menerima dana senilai Rp. 1,5 Juta</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 17px;">5.</div>
          <div style="float: left;width: 339;">Babinkamtibnas, Aipda Sigit Purnomo dikabarkan menerima dana senilai Rp. 3 Juta</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 17px;">6.</div>
          <div style="float: left;width: 339;">Danramil juga dikabarkan ikut menerima gratifikasi senilai Rp. 1 Juta</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 17px;">7.</div>
          <div style="float: left;width: 339;">Babinsa Selok Awar-awar menerima dana senilai Rp. 500 Ribu</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 17px;">8.</div>
          <div style="float: left;width: 339;">Camat Pasirian menerima dana rutin sebesar Rp. 1 Juta/Bulan</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 17px;">9.</div>
          <div style="float: left;width: 339;">Dua Pejabat Perhutani yang dikabarkan juga menerima dana rutin sebesar Rp. 500 Ribu/Bulan</div>
          <div style="clear: both;"></div>

          <div style="float: left;width: 20px;">10.</div>
          <div style="float: left;width: 393px;">Oknum Wartawan dan LSM diantaranya Beni, Basori, Karminto, Wanto dan Karyaman yang dikabarkan 
          juga ikut menerima gratifikasi dari keuntungan yang diperoleh melalui aktifitas tambang illegal</div>
          <div style="float: left;width: 200px;"><img src="http://bharatanews.com/foto_berita/46UANG.jpg" style="width: 50%;"></div>
          <div style="clear: both;"></div>
          </p>
          
          '
    ],

    'block11'=>[
        'title'=>'Bupati Terindikasi Turut Andil',
        'narasi'=>'
                  
            <p>Dinas Energi dan Sumber Daya Mineral (ESDM) Jawa Timur, mengendus izin tak wajar yang diduga dilakukan 
            pejabat Lumajang. Bahkan hasil penyelidikan Dinas ESDM mengerucut, bahwa Bupati Lumajang diduga memberi 
            izin kepada kepala Desa Selok Awar-Awar, Hariono melakukan aktivitas penambangan di area milik PT 
            Indo Modern Maining Sejahtera (IMMS).</p>
            <img src="http://lumajangsatu.com/foto_berita/2wabup-bupati%281%29.jpg" class="pic">

            <p>ESDM Jawa Timur menjelaskan, perusahaan tambang ini memiliki izin operasi yang dikeluarkan oleh Bupati 
            Lumajang Asa&#39;at Malik sejak 2012 hingga 2022. Namun sejak Januari 2014, sudah tidak beroperasi lagi 
            lantaran ada larangan ekspor pasir besi dalam bentuk mentah.</p>
            
            <p>Dari sini kemudian sebagian kelompok warga melakukan penambangan di lahan PT IMMS, tanpa sepengetahuan 
            perusahaan. Aktivitas ilegal ini berjalan sekian lama lantaran warga merasa mendapat perlindungan dari 
            pejabat setempat. Dalam tata ruang izin pertambangan, alur perizinan sebenarnya memang cukup sampai kabupaten, 
            dalam hal ini bupati atau wali kota. Tapi sejak Tahun 2015, ada undang-undang yang mengharuskan perizinan 
            aktivitas penambangan harus sampai ke tingkat provinsi.</p>
            <img src="http://jabar.pojoksatu.id/wp-content/uploads/2015/04/Tambang-Pasir.png" class="pic2">
            <p>Bagi perusahaan yang sudah memiliki kontrak sebelum 2015, maka menggunakan aturan izin di tingkat kabupaten 
            atau kota sampai masa akhir kontrak. Sementara itu sejak Januari 2015, PT IMMS yang mengajukan izin belum diproses. 
            Seluruh izin di Lumajang masih mengikuti kabupaten, ada 60 perizinan yang dikeluarkan bupati setempat.</p>'
    ],


    // 'institusiPendukung' => [
    //     ['link'=>'','image'=>'','title'=>''],
    // ],

    'tersangka' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/haryono561d263041a43','image'=>'http://assets2.jpnn.com/picture/normal/20150930_093908/093908_226420_salim_kades.jpg','title'=>'Kepala Desa Selok Awar-Awar'],
        ['link'=>'#','image'=>'http://cdn.img.print.kompas.com/getattachment/662172c1-62d1-42fe-81e8-64b332292ad2/249816?maxsidesize=315','title'=>'Oknum Polisi Polres Lumajang'],
        ['link'=>'#','image'=>'http://www.jpnn.com/picture/normal/20151002_073546/073546_861616_Pembunuh_Tim_12_Gun_JP.jpg','title'=>'Tim 12 Hariyono'],
    ],

    'institusiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/wahanalingkunganhidupindonesiawlhi5370cfc3cd6a3','image'=>'http://jowonews.com/wp-content/uploads/2014/11/201401044400.jpg','title'=>'Walhi'],
        ['link'=>'http://www.bijaks.net/aktor/profile/komisiiiidprri54d30f40415f0','image'=>'http://terkininews.com/files/DPR_RI_371677936.png&size=article_large','title'=>'Komisi III DPR'],
        ['link'=>'http://www.bijaks.net/aktor/profile/kompolnas530b08ed8a7cd','image'=>'http://cdn-2.tstatic.net/wartakota/foto/bank/images/20140603kompolnas-wajar-ada-kombes-kawal-capres.jpg','title'=>'Kompolnas'],
        ['link'=>'http://change.org','image'=>'http://www.nptechforgood.com/wp-content/uploads/2009/09/change.org_logo.png','title'=>'Change.org'],
        ['link'=>'#','image'=>'https://pbs.twimg.com/profile_images/1245332931/LOGO_GARUDA_400x400.jpg','title'=>'Komnas HAM'],
        ['link'=>'#','image'=>'http://www.jatam.org/wp-content/uploads/2015/02/logo-jatam01.jpg','title'=>'Jaringan Advokasi Tambang'],

    ],

    // 'partaiPendukung' => [
    //     ['link'=>'http://www.bijaks.net/aktor/profile/partaiamanatnasional5119b55ab5fab','image'=>'http://www.pemiluindonesia.com/wp-content/uploads/2013/04/PAN.jpg','title'=>'PAN'],
    // ],

    'partaiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/partaidemokrat5119a5b44c7e4','image'=>'http://4.bp.blogspot.com/-7VkISlK9gQE/Tu6xnxV646I/AAAAAAAAAQY/2pPGaGPwpo8/s1600/demokrat-2512.gif','title'=>'Demokrat'],
        ['link'=>'http://www.bijaks.net/politik','image'=>'http://dprd-tegalkab.go.id/wp-content/uploads/2015/04/logo_master.jpg','title'=>'PKB'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaidemokrasiindonesiaperjuangan5119ac6bba0dd','image'=>'http://1.bp.blogspot.com/-iHAUrzTJ82g/Uw79n0kJn-I/AAAAAAAAAlE/quCk5_1OEOY/s1600/Partai+Demokrasi+Indonesia+Perjuangan+%2528Logo%2529.jpeg','title'=>'PDI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/nasdem5119b72a0ea62','image'=>'http://www.kindo.hk/blog/images/2014-05/logo-partai-nasdem.jpg','title'=>'Nasdem'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaihatinuranirakyathanura5119a1cb0fdc1','image'=>'https://upload.wikimedia.org/wikipedia/id/c/ce/HANURA.jpg','title'=>'Hanura'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaipersatuanpembangunan5189ad769b227','image'=>'http://www.medanbagus.com/images/berita/normal/341256_10291211042013_PPP.jpg','title'=>'PPP'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaigerakanindonesiarayagerindra5119a4028d14c','image'=>'https://pbs.twimg.com/profile_images/1936137252/GERINDRA1.jpg','title'=>'Gerindra'],
    ],

    // 'quotePendukung'=>[
    //   ['from'=>'Ir. H. Jokowi','jabatan'=>'Presiden RI','img'=>'http://rmol.co/images/berita/normal/471192_04185709072015_jokowi_smile_1','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'"TNI juga bergerak, pemerintah daerah juga harus bergerak. Semua bergerak untuk memadamkan api dan membebaskan asap dengan target operasi yang jelas,"'],
    // ],

    'quotePenentang'=>[
        ['from'=>'Zulkifli Hasan','jabatan'=>'Ketua MPR RI','img'=>'http://img.lensaindonesia.com/uploads/1/2015/08/30540-main-zulkifli-hasan-ant-ketum-pan-ingatkan-jokowi-indonesia-di-ambang-krisis.gif','url'=>'http://www.bijaks.net/aktor/profile/zulkiflihasan50ef907d0661c','content'=>'"Memang biadab, usut tuntas dan tegakkan aturan agar ada yang terjerat jangan hilang begitu saja, apalagi kan ini pembela tambang, ada yang menambang tetapi merusak lingkungan"'],
        ['from'=>'Marwan Djafar','jabatan'=>'Menteri Desa dan Daerah Tertinggal','img'=>'http://www.rmol.co/images/berita/normal/847705_07334801092015_Marwan_Jafar.jpg','url'=>'http://www.bijaks.net/aktor/profile/hmarwanjafarsesh50f8f52b84acd','content'=>'"Jangan sampai ada pengelolaan SDA seperti pertambangan yang hanya menguntungkan kepala desa saja,"'],
        ['from'=>'Saifullah Yusuf','jabatan'=>'Wakil Gubernur Jawa Timur','img'=>'http://korannusantara.com/wp-content/uploads/2012/11/Saifullah-Yusuf.jpg','url'=>'http://www.bijaks.net/aktor/profile/drshsaifullahyusuf51402dc53a94e','content'=>'"Saya minta penyelidikannya dilakukan secara terbuka dan tidak ada yang kebal hukum,"'],
        ['from'=>'Akbar Faisal','jabatan'=>'Politisi Partai NasDem','img'=>'http://fajar.co.id/wp-content/uploads/2015/02/Akbar-Faisal1.jpg','url'=>'http://www.bijaks.net/aktor/profile/drsakbarfaizalmsi50f778b33337e','content'=>'"Kepada bupatinya, (Komisi III DPR) bertanya soal pembiaran. Kenapa lahan ini notabenenya sudah dianggap penambangan liar, tapi kok dibiarkan (beroperasi)"'],
        ['from'=>'Risa Mariska','jabatan'=>'Anggota Komisi III','img'=>'http://risamariska.com/wp-content/uploads/2014/02/risa-kpu-02-risamariska.com_.jpg','url'=>'http://www.bijaks.net/aktor/profile/risamariska526dcab9026d2','content'=>'"Saya sudah meminta Divkum, Propam dan Provost, turun menyelidiki kasus tambang pasir illegal,"'],
        ['from'=>'Benny Kabur Harman','jabatan'=>'Wakil Ketua Komisi III DPR','img'=>'http://sp.beritasatu.com/media/images/original/20141029174602970.jpg','url'=>'http://www.bijaks.net/aktor/profile/drbennykaburharmansh50f8dd6613925','content'=>'"Pembiaran ini terjadi karena diduga terdapat beberapa pihak yang terlibat dalam kasus tersebut"'],
        ['from'=>'Badrodin Haiti','jabatan'=>'Kapolri','img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150218_015120_harianterbit_badrodin.jpg','url'=>'http://www.bijaks.net/aktor/profile/badrodinhaiti531d430cb8e05','content'=>'"Apakah informasi yang selama ini berkembang negatif itu betul atau tidak, ada pembiaran, lambat dalam penanganan, itu nanti akan dijawab dari hasil penyelidikan Propam"'],
        ['from'=>'Ony Mahardika','jabatan'=>'Direktur WALHI JATIM','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2015--10--73182-ony-walhi-desak-pemprov-tinjau-rtrw-penataan-galian-tambang-jawa-timur.jpg','url'=>'http://www.bijaks.net/aktor/profile/onymahardika52cf55f1640c3','content'=>'"Jika ditangani sedari awal pasti tidak ada korban, soalnya penambangan pasir liar di Desa Selok Awar-awar, Kecamatan Pasirian, Kabupaten Lumajang sudah terjadi sejak Januari 2015 silam"'],
        ['from'=>'Natalius Pigay','jabatan'=>'Komisioner Komnas HAM','img'=>'http://rmolsumsel.com/images/berita/normal/485608_02143625102014_Natalius_Pigai.jpg','url'=>'http://www.bijaks.net/aktor/profile/nataliuspigai51a5607bce402','content'=>'"Jadi, pihak kepolisian bisa mengembangkan, apakah ada keterlibatan aparat pemerintah kabupaten atau ada keterlibatan korporasi"'],
        ['from'=>'Arsul Sani','jabatan'=>'Anggota Komisi III DPR RI','img'=>'http://www.rmol.co/images/berita/thumb/thumb_652735_04355920072015_arsul_sani.JPG','url'=>'http://www.bijaks.net/aktor/profile/arsulsani52c4f8ac84d4b','content'=>'"Kami minta Polda menelusurinya sesuai dengan penyidikan dalam kasus TPPU (Tindak Pidana Pencucian Uang) karena pembiaran yang terjadi itu menimbulkan dugaan adanya aliran uang"'],
        ['from'=>'Yuni Satia Rahayu','jabatan'=>'Mantan Wakil Bupati Sleman dan Politisi PDIP','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH250jEwZYUex_BpmbZf4y8ZI74yQFMSvsAxZfCeGo6Y-MI4z2','url'=>'http://www.bijaks.net/aktor/profile/yunisatiarahayu561d34aed7054','content'=>'"Kalau boleh kritis, negara harus hadir agar masyarakat jangan saling melukai. Kasus Salim Kancil jangan sampai terjadi di Sleman,"'],
    ],

    'video'=>[
        ['id'=>'Ajm7JyCuD_Y'],
        ['id'=>'-4kQd0hpGlk'],
        ['id'=>'zz7dxrjxeM4'],
        ['id'=>'M699qB9g9zI'],
        ['id'=>'GVbKlYzRvZY'],
        ['id'=>'uaPTeTk8KuU'],
        ['id'=>'PSuZDa5-zqo'],
        //['id'=>'bAmci0OlmRY'],
        ['id'=>'_9MZZ7has5Y'],
        ['id'=>'VoJ9qI_ZwVE'],
        ['id'=>'ZxKnY2J0tPI'],
        ['id'=>'9zrQtuenWX8'],
        ['id'=>'6wGA-N00hRs'],
        ['id'=>'g-uXRH35FUk'],
        ['id'=>'Ti_7VWsmgVY'],
        ['id'=>'jly6Q7MtosU'],
        ['id'=>'IuTMSSvzI3U'],
        ['id'=>'BzlvJxCB96o'],
        ['id'=>'USiCd4bf1dY'],
        ['id'=>'mlURHlyx0qs'],
        ['id'=>'SDJsC5OFY8c'],
        ['id'=>'s0OaFSnIlEM'],
    ],

    'foto'=>[
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/10/12/607894/670x335/duit-tambang-berdarah-lumajang-dimakan-aparat-dan-pejabat.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2011/07/03/id_82329/82329_620.jpg'],
        ['img'=>'http://media.galamedianews.com/news/151002211732-tragedi-tambang-berdarah-hamida-bupati-lumajang-harus-diperiksa.jpg'],
        ['img'=>'http://www.topik9.com/wp-content/uploads/2015/10/penambang-pasir.jpg'],
        ['img'=>'http://images1.rri.co.id/thumbs/berita_203683_800x600_korban_new.jpg'],
        ['img'=>'http://www.suarasurabaya.net/_watermark/createimage_small.php?d=kk&c=berita&b=201510&a=160099&angka=10'],
        ['img'=>'http://www.jemberpost.com/wp-content/uploads/2015/09/339229_aksi-solidaritas-terhadap-salim-kancil_663_382.jpg'],
        ['img'=>'http://www.dpp.pkb.or.id/unlocked/styles/slide-show-dpp-pkb-headline/public/unlocked/940dab37768af683ee55b3578b86d1aa960675b681ab89972ac5bba7865b6bd6271c99a6abd3f0cb8ef8c9463dd4c409beritaplusfoto/Tambang%20Pasir%20Lumajang.jpg?itok=rpvsdsjR'],
        ['img'=>'http://www.suarasurabaya.net/_watermark/createimage_small.php?d=kk&c=berita&b=201510&a=160453&angka=10'],
        ['img'=>'http://www.suarasurabaya.net/_watermark/createimage_medium.php?d=kk&c=berita&b=201510&a=160160&angka=2'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/10/02/9b1bc7c9-1663-4018-84d0-5228d6eb304d_169.jpg?w=780&q=90'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/10/05/997dc2c7-d255-49cb-b8a3-07e8ce5c1955_169.jpg?w=300&q='],
        ['img'=>'http://www.suarasurabaya.net/_watermark/createimage_small.php?d=&c=potret&b=201510&a=16067&angka=3'],
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/09/30/602287/670x335/beri-izin-tambang-akankah-bupati-lumajang-terseret-kasus-salim.jpg'],
        ['img'=>'http://lumajangsatu.com/foto_berita/20walhi-jatim-selok-awar-awar.jpg'],
        ['img'=>'http://lacak.id/wp-content/uploads/2015/10/7a9e025b-30ef-41cb-afab-0831c9191095_169.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2012/05/29/id_122319/122319_620.jpg'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/09/27/3d3bf59b-177d-4106-9bdf-5d979b1b84f2_169.jpg?w=780&q=90'],
        ['img'=>'http://www.jurnal3.com/wp-content/uploads/2015/10/lumajang-kasus-600x370.jpg'],
        ['img'=>'https://ytimg.googleusercontent.com/vi/79NzPnMIrvM/mqdefault.jpg'],
        ['img'=>'http://surabayapost.net/foto_berita/12tambang.jpg'],
        ['img'=>'http://ytimg.googleusercontent.com/vi/9MeaBCxNEkk/mqdefault.jpg'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/09/27/ce36f5a8-41e7-4a0c-b4ce-d86ebb58fc01_169.jpg?w=780&q=90'],
        ['img'=>'http://halloapakabar.com/wp-content/uploads/2015/10/kasus-salim-kancil.jpg'],
        ['img'=>'https://images.detik.com/community/media/visual/2015/09/30/c52c3098-3eeb-4238-99d9-e29cf7a62c19_169.jpg?w=620&mark=undefined&image_body_visual_id=143137'],
        ['img'=>'http://lumajangsatu.com/foto_berita/62Bupati-Lumajang-Jengguk-Tosan.jpg'],
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/10/13/608369/670x335/komnas-ham-sebut-izin-tambang-ilegal-modus-cari-modal-menang-pilkada.jpg'],
        ['img'=>'https://singindo.files.wordpress.com/2015/10/wpid-screenshot_2015-10-06-20-37-03.png'],
        ['img'=>'http://berita.beritajatim.com/brt604406527.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/09/28/339303_demo-usut-pembunuhan-petani-lumajang_663_498.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/1017785/big/097903900_1444632865-20151012-_Kasus_Salim_Kancil-Sidang_Etik.jpg'],
        ['img'=>'http://geotimes.co.id/wp-content/uploads/2015/10/Aksi-Dukungan-Salim-Kancil-300915-Prad-1.jpg'],

    ],

    'BERITA'=>[
        ['img'=>'http://news.bijaks.net/uploads/2015/05/Agus-Hermanto.jpg','shortText'=>'DPR Minta Tutup Kampus Bodong','link'=>'http://www.bijaks.net/news/article/0-139354/dpr-minta-tutup-kampus-bodong'],


    ],

    'kronologi'=>[
        'list'=>[
            ['date'=>'Januari 2015','content'=>'Masyarakat Desa Selok Awar-Awar melakukan aksi penolakan tambang pasir berupa 
            penyampaian pernyataan sikap dari Forum Komunikasi Masyarakat Peduli Desa Selok Awar-Awar, Lumajang di mana Tosan 
            dan Salim Kancil menjadi bagian dan anggota dari Forum'],
            ['date'=>'Juni 2015','content'=>'Forum warga menyurati Bupati Lumajang untuk meminta audiensi tentang penolakan tambang pasir. Surat tersebut tidak direspons oleh Bupati Lumajang'],
            ['date'=>'9 September 2015','content'=>'Forum warga melakukan aksi damai penghentian aktivitas penambangan pasir dan truk muatan pasir di Balai Desa Selok Awar-Awar'],
            ['date'=>'10 September 2015','content'=>'Muncul ancaman pembunuhan yang diduga dilakukan oleh sekelompok preman yang dibentuk oleh Kepala Desa Selok Awar-Awar kepada Tosan. Kelompok preman tersebut diketuai oleh Desir'],
            ['date'=>'11 September 2015','content'=>'Forum melaporkan tindak pidana pengancaman ke Polres Lumajang yang diterima langsung oleh Kasat Reskrim Lumajang, Heri. Saat itu Kasat menjamin akan merespons pengaduan tersebut'],
            ['date'=>'19 September 2015','content'=>'Forum menerima surat pemberitahuan dari Polres Lumajang terkait nama-nama penyidik Polres yang menangani kasus pengancaman tersebut'],
            ['date'=>'21 September 2015','content'=>'Forum mengirim surat pengaduan terkait penambangan ilegal yang dilakukan oleh oknum aparat Desa Selok Awar-Awar di daerah hutan lindung Perhutani'],
            ['date'=>'25 September 2015','content'=>'Forum mengadakan koordinasi dan konsolidasi dengan masyarakat luas tentang rencana aksi penolakan tambang pasir dikarenakan aktivitas penambangan tetap berlangsung. Aksi ini rencananya digelar 26 September 2015 pukul 07.30 WIB.'],
            ['date'=>'26 September 2015','content'=>'Sekitar pukul 08.00 WIB, terjadi penjemputan paksa dan penganiayaan terhadap dua orang anggota forum yaitu Tosan dan Salim Kancil'],
            ['date'=>'26 September 2015','content'=>'Masih dihari yang sama, Salim Kancil ditemukan tewas dengan kondisi yang sangat memprihatinkan. Menurut beberapa saksi, Salim Kancil disiksa secara keji dan sadis sampai ia meregang nyawa. Sementara Tosan mengalami luka yang cukup serius dan dibawa ke Rumah Sakit'],
        ]
    ],

]


?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .num_point{
      float: left;
      width: 20px;
    };

    .content_point{
      float: left;
      padding-left: 40px;
    }

    .rightcol{margin-left: 20px;margin-right: 20px;font-size: 12px;}
    .clearspace{clear:both;margin-bottom: 1em;}
    .allpage {width: 100%;height: auto;margin-top: 12px;}
    .col_top {background: url('<?php echo base_url(); ?>assets/images/hotpages/tambang/salim-kancil.jpg') no-repeat transparent;height: 1352px;margin-bottom: -615px;}
    .col_kiri {width: 64%;height: auto;background: transparent;float: left;padding-right: 2%;}
    .col_kiri p, .col_kiri li, .font_kecil {font-size: 13px;}
    .col_kanan {width: 34%;height: auto;background: transparent;float: left;}
    .col_kiri2 {width: 34%;height: auto;background: transparent;float: left;padding-right: 2%;}
    .col_kanan2 {width: 64%;height: auto;background: transparent;float: left;}
    .col_kiri50 {width: 20%;height: auto;/*background-color: red;*/float: left;padding-right: 1%;}
    .col_kanan50 {width: 78%;height: auto;/* background-color: green; */float: right;padding-left: 1%;border-left: 1px dotted black;display: inblock-table;}
    .col_full {width: 100%;/*background-color: lightgray;*/}
    .boxprofile {box-shadow: -5px 5px 10px gray;border-radius: 10px 10px 10px 10px;width: 290px;margin: 0 auto;padding-bottom: 10px;background: rgba(0, 0, 0, 0.7);}
    .block_red {background-color: #761908;border-radius: 10px 10px 0 0;padding-top: 5px;padding-bottom: 5px;}
    .picprofil {width: 80%;height: auto;margin: 0 auto;display: inherit;}
    .pic {float: left;margin-right: 10px;max-width: 200px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 200px;margin-top: 5px;}
    .pic3 {margin-right: 10px;max-width: 50px;}
    .garis {border-top: 1px dotted black;}
    .boxgray {width: 99%;border: 5px solid lightgray;box-shadow: -5px 5px 10px gray;}
    .boxgray_red {width: 96%;border: 9px solid #a60008;border-radius: 8px;box-shadow: -5px 5px 10px gray;}
    .boxgray_green {width: 96%;border: 9px solid #00a651;border-radius: 8px;box-shadow: -5px 5px 10px gray;}
    .penyokong {width: 100%;height: auto;display: inblock-block;margin-left: 20px;}
    .boxpenyokong {width: 126px;height: auto;}
    .foto {width: 100px;height: 100px;border: 3px solid lightgray;border-radius: 8px;box-shadow: 5px 5px 10px gray;padding: 10px 10px;}
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 113px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100%;padding: 0px !important;font-size: 8px;height: auto;block-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 110px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi2 img {width: 98px; height: 70px;  padding: 0px !important;}
    li.organisasi2 p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;block-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 121px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;block-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
    li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;block-height: 12px;text-align: center;margin-top: 5px;}

    li.dukung {float: left; padding:3px;vertical-align: top;width: 177px;height:115px;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
    li.dukung img {width: auto; height: 75px;padding: 0px !important;margin: 0 auto;display: block;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;block-height: 12px;}

    .black {color: black;}
    .white {color: white;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    p, li {text-align: justify;font-size: 14px;}
    .clear {clear: both;}
    .qpenentang {float: left;width: 30%;height: auto;background-color: red;display: inblock-block;border-bottom: 1px solid black;}
    .parodi {width: 107px;height: 87px;float: left;margin-right: 10px;margin-bottom: 10px;}
    .uprow {margin-left: 20px;margin-top:10px;border-color: #761908 transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {width: 260px;height: auto;margin-left: 20px;margin-top:0px;margin-bottom: 10px;background-color: #424040;background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px #761908;border-right:solid 2px #761908;border-bottom:solid 2px #761908;border-radius: 0 0 5px 5px;color: #ffffff;z-index: 100;}
    .kronologi-title {font-size:14px;font-weight:bold;background-color: #761908;color: white;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {block-height: 15px;font-size:12px;}

    .ketua {background-color: yellow;width: 30%;display: block;float: left;margin-right: 3%;box-shadow: -3px 3px 10px gray;border-radius: 8px;margin-bottom: 15px;}
    .ketua img {width: 100%;height: 200px;}
    .kritik {font-size: 18px;font-weight: bold;color: black;}
    .isi-col50 {float: left;margin: 10px 16px;width: 41%;height: auto;display: inblock-block;}
    .boxdotted {border-radius: 10px;border: 2px dotted #29166f;width: 293px;height: 345px;float: left;margin-bottom: 10px;padding-top: 5px;margin-left: 10px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;float: left;border: 1px solid black;}
    #bulet {background-color: #ffff00;text-align: center;width: 50px;height: 25px;border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: black;padding: 6px 10px;margin-left: -10px;margin-right: 10px;}
    .gallery {list-style: none outside none;padding-left: 0;margin-left: 0px;}
    .gallery li {display: block;float: left;height: 80px;margin-bottom: 7px;margin-right: 0px;width: 120px;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 115px;}

</style>
<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.thumb2').click(function(){
            var target = $(this).data('homeid');
            var src = $(this).attr('src');
            $('#'+target).attr('src',src);
        });
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<br/>
<div class="container">
<div class="sub-header-container">
<div class="allpage">
<div class="col_top"></div>
<div class="col_kiri">
    <h4 style="width: 335px;margin-bottom: 25px;margin-top:43px;">
        <a id="analisa" style="color: black;font-size: 30px;block-height: 30px;"><?php echo $data['block1']['title'];?></a>
    </h4>
    <p><?php echo $data['block1']['narasi'];?></p>
    <div class="clearspace"></div>
    <div class="garis"></div>
    
    <h4 class="list"><a id="prokontra" style="color: black;"><?php echo $data['block2']['title'];?></a></h4>
    <p><?php echo $data['block2']['narasi'];?></p>
    <div class="clearspace"></div>
    <div class="garis"></div>

    <h4 class="list"><a id="prokontra" style="color: black;"><?php echo $data['block8']['title'];?></a></h4>
    <p class="font_kecil"><?php echo $data['block8']['narasi'];?></p>
    <div class="clearspace"></div>
    <div class="garis"></div>

    <h4 class="list"><a id="prokontra" style="color: black;"><?php echo $data['block10']['title'];?></a></h4>
    <p class="font_kecil"><?php echo $data['block10']['narasi'];?></p>
    <div class="clearspace"></div>
    <div class="garis"></div>

    <h4 class="list"><a id="prokontra" style="color: black;"><?php echo $data['block11']['title'];?></a></h4>
    <p class="font_kecil"><?php echo $data['block11']['narasi'];?></p>
    <div class="clearspace"></div>
    <div class="garis"></div>

    <h4 class="list"><a id="prokontra" style="color: black;"><?php echo $data['block9']['title'];?></a></h4>
    <p class="font_kecil"><?php echo $data['block9']['narasi'];?></p>
    <div class="clearspace"></div>
    <div class="garis"></div>


</div>

<div class="col_kanan">


    <div class="boxprofile white">
        <h4 class="block_red text-center"><a id="profil" style="color: white;"><?php echo $data['block3']['title'];?></a></h4>
        <?php echo $data['block3']['narasi'];?>
    </div>
    <div class="clear"></div>
    <br/>

    <h4 class="list" style="margin-left: 20px;"><a id="kronologi" style="color: black;">KRONOLOGI</a></h4>
    <?php 
    foreach ($data['kronologi']['list'] as $key => $value) {
    ?>
    <div class="uprow"></div>
    <div class="kronologi">
        <div class="kronologi-title"><?php echo $value['date'];?></div>
        <div class="kronologi-info">
            <p><?php echo $value['content'];?></p>
        </div>
    </div>
    <div class="clear"></div>
    <?php
    }
    ?>
    <?php /*
    <div class="boxprofile white">
        <h4 class="block_red text-center"><a id="profil" style="color: white;"><?php echo $data['block5']['title'];?></a></h4>
        <?php echo $data['block5']['narasi'];?>
    </div>
    <div class="clear"></div>
    <br/>
    */?>

    <h4 class="list"><a id="pendukung" style="color: black;">BERITA TERKAIT</a></h4>
    <div style="width: 306px;height:auto;">
        <div style="padding-left:0px;background-color: #E5E5E5;">
            <div id="tambang_container" data-tipe="1" data-page='1' style="height: auto;margin-bottom: 9px !important;"></div>
            <div class="row-fluid" style="margin-bottom: 2px;">
                <div class="text-left">
                    <a id="tambang_loadmore" data-tipe="1" class="btn btn-mini" >7 Berikutnya</a>
                </div>
            </div>

        </div>
    </div>

</div>
<div class="clear"></div>

<?php if(!empty($data['tersangka'])){ ?>
<div class="col_full">
    <?php 
      echo '<h4 class="list"><a id="pendukung" style="color: black;">KELOMPOK TERSANGKA</a></h4>';
      echo '<div>';
      echo '<ul style="margin-left: 0px;">';
      foreach($data['tersangka'] as $vip){
    ?>
        <li class="organisasi">
            <a href="<?php echo $vip['link'];?>">
            <img src="<?php echo $vip['image'];?>" data-toggle="tooltip" data-original-title="<?php echo $vip['title'];?>"/>
            <p><?php echo $vip['title'];?></p>
            </a>
        </li>
  <?php
      }
      echo '</ul>';
      echo '</div>';  
    ?>
   <div class="clear"></div> 
</div>
<div class="garis"></div>
<?php };?>

<div class="col_full">
    <div class="garis"></div>

    <?php 
    if(!empty($data['institusiPendukung'])){
      echo '<h4 class="list"><a id="pendukung" style="color: black;">INSTITUSI PENDUKUNG</a></h4>';
      echo '<div>';
      echo '<ul style="margin-left: 0px;">';
      foreach($data['institusiPendukung'] as $vip){
    ?>
        <li class="organisasi">
            <a href="<?php echo $vip['link'];?>">
            <img src="<?php echo $vip['image'];?>" data-toggle="tooltip" data-original-title="<?php echo $vip['title'];?>"/>
            <p><?php echo $vip['title'];?></p>
            </a>
        </li>
  <?php
      }
      echo '</ul>';
      echo '</div>';  
    };?>
   <div class="clear"></div> 

    <?php 
    if(!empty($data['institusiPenentang'])){
      echo '<h4 class="list"><a id="penentang" style="color: black;">INSTITUSI PENENTANG</a></h4>';
      echo '<div>';
              
      echo '<ul style="margin-left: 0px;">';
      foreach($data['institusiPenentang'] as $vip){
    ?>
        <li class="organisasi">
            <a href="<?php echo $vip['link'];?>">
            <img src="<?php echo $vip['image'];?>" data-toggle="tooltip" data-original-title="<?php echo $vip['title'];?>"/>
            <p><?php echo $vip['title'];?></p>
            </a>
        </li>
  <?php
      }
      echo '</ul>';
      echo '</div>';
    };?>
</div>
<div class="clear"></div>
<br>
<div class="garis"></div>


<div class="col_full">
    <div class="garis"></div>

    <?php 
    if(!empty($data['partaiPendukung'])){
      echo '<h4 class="list"><a id="pendukung" style="color: black;">PARTAI PENDUKUNG</a></h4>';
      echo '<div>';
      echo '<ul style="margin-left: 0px;">';
      foreach($data['partaiPendukung'] as $vip){
    ?>
        <li class="organisasi">
            <a href="<?php echo $vip['link'];?>">
            <img src="<?php echo $vip['image'];?>" data-toggle="tooltip" data-original-title="<?php echo $vip['title'];?>"/>
            <p><?php echo $vip['title'];?></p>
            </a>
        </li>
  <?php
      }
      echo '</ul>';
      echo '</div>';  
    };?>
   <div class="clear"></div> 

    <?php 
    if(!empty($data['partaiPenentang'])){
      echo '<h4 class="list"><a id="penentang" style="color: black;">PARTAI PENENTANG</a></h4>';
      echo '<div>';
              
      echo '<ul style="margin-left: 0px;">';
      foreach($data['partaiPenentang'] as $vip){
    ?>
        <li class="organisasi">
            <a href="<?php echo $vip['link'];?>">
            <img src="<?php echo $vip['image'];?>" data-toggle="tooltip" data-original-title="<?php echo $vip['title'];?>"/>
            <p><?php echo $vip['title'];?></p>
            </a>
        </li>
  <?php
      }
      echo '</ul>';
      echo '</div>';
    };?>
</div>
<div class="clear"></div>
<br>
<div class="garis"></div>

<?php if(!empty($data['quotePendukung'])){?>
<div class="col_full">
    <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
    <div class="clear"></div>
    <ul style="margin-left: 0px;margin-top: 10px;">
        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
            <?php
            for($i=0;$i<=5;$i++){
              if(!empty($data['quotePendukung'][$i])){

                ?>
                <div style="width: 100%;height: auto;display: inblock-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['quotePendukung'][$i]['url'];?>" ><img src="<?php echo $data['quotePendukung'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 77%;margin-right: 10px;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['quotePendukung'][$i]['from']; ?> | <?php echo $data['quotePendukung'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;block-height: 14px;margin-top: -5px;"><?php echo $data['quotePendukung'][$i]['content']; ?></p>
                    </div>
                </div>
                <?php };
                }; 
                ?>
        </div>
        <div style="float: left;width: 47%;margin-left: 20px;">
            <?php
            for($i=6;$i<=11;$i++){
              if(!empty($data['quotePendukung'][$i])){
                ?>
                <div style="width: 100%;height: auto;display: inblock-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['quotePendukung'][$i]['url'];?>" ><img src="<?php echo $data['quotePendukung'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 78%;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['quotePendukung'][$i]['from']; ?> | <?php echo $data['quotePendukung'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;block-height: 14px;margin-top: -5px;"><?php echo $data['quotePendukung'][$i]['content']; ?></p>
                    </div>
                </div>
            <?php };
            }; ?>
        </div>
    </ul>
</div>
<div class="clear"/><br>
<div class="garis"></div>
<?php };?>

<?php if(!empty($data['quotePenentang'])){?>
<div class="col_full">
    <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENENTANG</a></h4>
    <div class="clear"></div>
    <ul style="margin-left: 0px;margin-top: 10px;">
        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
            <?php
            for($i=0;$i<=5;$i++){
              if(!empty($data['quotePenentang'][$i])){
                ?>
                <div style="width: 100%;height: auto;display: inline-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['quotePenentang'][$i]['url'];?>" ><img src="<?php echo $data['quotePenentang'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 77%;margin-right: 10px;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['quotePenentang'][$i]['from']; ?> | <?php echo $data['quotePenentang'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;"><?php echo $data['quotePenentang'][$i]['content']; ?></p>
                    </div>
                </div>
            <?php };
            }; ?>
        </div>
        <div style="float: left;width: 47%;margin-left: 20px;">
            <?php
            for($i=6;$i<=16;$i++){
              if(!empty($data['quotePenentang'][$i])){
                ?>
                <div style="width: 100%;height: auto;display: inline-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['quotePenentang'][$i]['url'];?>" ><img src="<?php echo $data['quotePenentang'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 78%;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['quotePenentang'][$i]['from']; ?> | <?php echo $data['quotePenentang'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;"><?php echo $data['quotePenentang'][$i]['content']; ?></p>
                    </div>
                </div>
            <?php };
            }; ?>
        </div>
    </ul>
</div>
<div class="clear"></div>
<br>
<div class="garis"></div>

<?php };?>

    <h4 class="list"><a id="prokontra" style="color: black;"><?php echo $data['block4']['title'];?></a></h4>
    <p class="font_kecil"><?php echo $data['block4']['narasi'];?></p>
    <div class="clearspace"></div>
    <div class="garis"></div>

<h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
<div class="col_full" style="display: inblock-block;">
    <ul id="light-gallery" class="gallery" style="margin-left: 0px;margin-top: 0px;">
        <?php
        foreach ($data['foto'] as $key => $val) { ?>

            <li data-src="<?php echo $val['img'];?>" style="overflow: hidden;">
                <a href="#">
                    <img src="<?php echo $val['img'];?>" />
                </a>
            </li>
        <?php
        }
        ?>
    </ul>
</div>

<div class="clear"></div>
<h4 class="list" style="margin-top: 10px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>
<div class="col_full">
    <div class="boxgray" style="height: 330px;">
        <ul style="margin-left: 15px;margin-top: 10px;">
            <?php
            if(!empty($data['video'])){
              foreach ($data['video'] as $key => $val) { ?>
                  <li class="video">
                      <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                          <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                      </a>
                  </li>
                  <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                              </div>
                              <div class="modal-body">
                                  <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                              </div>
                          </div>
                      </div>
                  </div>
              <?php
              };
            };
            ?>
        </ul>
    </div>
</div>
<div class="clear"></div>
<br/><br/>

</div>
</div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>