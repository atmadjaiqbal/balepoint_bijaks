<?php 
$data = [
    'NARASI'=>[
        'title'=>'RUSUH KAMPUNG PULO',
        'narasi'=>'<img src="http://cdn.tmpo.co/data/2015/08/18/id_428624/428624_620.jpg" class="pic">Kamis 20/08/2015 pemrov DKI Jakarta melakukan pembongkaran bangunan di bantaran kali Ciliwung. Upaya Pemrov DKI Jakarta untuk merevitalisasi kali dengan menertibkan rumah dan bangunan milik warga mendapat respon keras dari warga. Warga kampung pulo menolak penggusuran, akhirnya bentrokanpun terjadi antara warga dan aparat. Bahkan Gubernur Ahok menurunkan ratusan polisi untuk mengamankan jalannya proses penggusuran tersebut. 
                    </p><p>Tindakan Gubernur Ahok dianggap terlalu berlebihan dan refresif. Beberapa kalangan menganggap, termasuk dari Komnas HAM menilai cara Pemrov DKI Jakarta menggusur warga melanggar HAM dan tidak sesuai prosedur. Polemik penggusuran tanah di Kampung Pulo menjadi kontroversi.
                    </p><p><img src="http://cdn-2.tstatic.net/tribunnews/foto/bank/images/penggusuran-kampung-pulo_20150820_215926.jpg" class="pic2">Gubernur Ahok berdalih bahwa penggusuran dilakukan untuk menanggulangi banjir yang kerap datang setiap tahun. Selain itu, penggusuran dilakukan karena tanah tersebut tanah Negara dan warga Kampung Pulo akan diberikan fasilitas rumah susun sebagai bentuk ganti rugi. 
                    </p><p>Namun, status kepemilikan tanah di Kampung Pulo menjadi perdebatan terkait ganti rugi yang akan diberikan oleh Pemerintah. Pemerintah DKI Jakarta berpegang pada ketentuan Undang-Undang Nomor 6 Tahun 1960 tentang Peraturan Dasar Pokok-pokok Agraria: bukti kepemilikan adalah sertifikat. Namun warga Kampung Pulo di Jakarta Timur itu merasa hal ini cukup ditunjukkan dengan surat perjanjian jual-beli dan tanda bayar Pajak Bumi dan Bangunan.'
    ],
    'ANALISA'=>[
        'narasi'=>'<img src="http://cdn1-a.production.liputan6.static6.com/medias/940788/big/024778900_1438225857-20150729-Stok-Photo-Ahok4.jpg" class="pic">Kampung Pulo. Kini Kampung Pulo rata dengan tanah akibat digusur oleh Pemrov DKI Jakarta. 
                    </p><p class="font_kecil">Alasan Gubernur Ahok yang meratakan bangunan-bangunan di sekitar kali Ciliwung untuk revitalisasi. 
                    </p><p class="font_kecil">Banjir yang kerap menggenangi Jakarta bersumber di Kampung Pulo. Kawasan tersebut sangat kumuh akibat tumpukan sampah dari warga dan banyaknya bangunan liar yang menghampat aliran sungai.',
        'list'=>[
            ['isi'=>'Menjadi sumber Banjir di Jakarta'],
            ['isi'=>'Tanah Negara dan resapan air.'],
            ['isi'=>'Sebelum keputusan penggusuran diambil, Gubernur Ahok telah berusaha memasyarakatkan proyek normalisasi itu: apa saja yang akan dilakukan dan apa akibatnya bagi sebagian warga di sekitar bantaran Sungai Ciliwung. Sosialisasi sudah dilaksanakan sejak tahun lalu.'],
            ['isi'=>'Pilhan untu menertibkan warga Kampung Pulo sudah tepat karena alasan kemanusiaan. Agar warga tidak lagi menderita ketika banjir datang.'],
            ['isi'=>'Langkah penggusuran diambil sesuai prosedur, yakni dengan melakukan negosiasi terlebih dahulu.'],
            ['isi'=>'Penggusuran tidak dilakukan secara mendadak. Sebelumnya sudah ada peringatan dari Pemrov DKI agar mengosongkan kawasan Kampung Pulo.'],
            ['isi'=>'Pemrov DKI berdalih bahwa daerah tempat tinggal warga merupakan tanah milik Negara. Bukan milik warga Kami.'],
            ['isi'=>'Setiap warga akan menerima penggantian ganti rugi sesuai dengan peraturan Pergub No 19 tahun 2004.'],
            ['isi'=>'Pemrov DKI akan menyediakan rumah susun untuk warga sebagai bentuk ganti rugi.']
        ]                    
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="http://cdn-2.tstatic.net/tribunnews/foto/bank/images/ormas_20150822_175105.jpg" class="pic2">Penggusuran yang dilakukan Pemprov DKI terhadap warga Kampung Pulo,Jatinegara, Jakarta Timur mendapat reaksi yang beragam dari masyarakat. Banyak dari masyarakat mengkritik kebijakan Pemrov DKI tersebut dengan mempertanyakan konsistensi Pemerintah DKI dalam menanggulangi masalah banjir di Jakarta. 
                    </p><p class="font_kecil">Warga Kampung Pulo juga merasa heran dengan klaim Pemrov DKI yang mengakui tanah yang mereka tempati sebagai tanah Negara, padahal warga memiliki sertifkat tanah yang sah. Selain Itu, penanganan bentrokan yang terjadi saat proses penggusura juga mendapat kritikan keras. Pelibatan polisi dan tentara dalam menghalau aksi massa dianggap cara yang sangat refresif. 
                    </p><p class="font_kecil"><img src="http://cdn.klimg.com/merdeka.com/i/w/news/2015/08/25/585860/670x335/video-perdebatan-sengit-ahok-dan-warga-kp-pulo-sebelum-digusur.png" class="pic">Seharusnya, Pemrov DKI melakukan pendekatan yang persuasif agar tidak terjadi korban jiwa. Di lain pihak, Gubernur Ahok sendiri mengaku bahwa relokasi ini masih menyisakan masalah, karena ada warga yang menolak dipindahkan, dan menuntut ganti rugi uang sekaligus rusun. Sedianya, warga akan dipindahkan ke rumah susun sewa Jatinegara Barat.Menurutnya tuntutan ganti rugi uang itu tidak relevan, karena status lahan yang merupakan milik negara. Ia pun menyinggung soal sumber anggaran, bila Pemprov DKI Jakarta harus memenuhi tuntutan ganti rugi uang.'
    ],
    'KRONOLOGI'=>[
        'list'=>[
            ['date'=>'8 Juni 2015','content'=>'pemerintah menyedorkan 786 pasal dalam Rancangan Undang Undang (RUU) Kitab Undang Undang Hukum Pidana (KUHP) ke DPR RI, Salah satu pasal adalah soal pasal penghinaan presiden dan wakil presiden RI.'],
            ['date'=>'3 Agustus 2015','content'=>'secara azas hukum yang berlaku segala Undang-Undang atau pasal yang telah dibatalkan oleh MK itu sudah tak bisa dibahas atau dihidupkan kembali dalam UU, ungkap Aziz Syamsuddin (ketua komisi III DPR RI)'],
            ['date'=>'8 Agustus 2015','content'=>'pasal penghinaan presiden bisa dibatalkan MK. Peluang pasal penghinaan Presiden dimuat dalam Kitab Undang-Undang Hukum Pidana (KUHP) sangat kecil. Demikian disampaikan Mantan Ketua Mahkamah Konstitusi Mahfud MD.'],
            ['date'=>'09 Agustus 2015','content'=>'SBY mengatakan pasal penghinaan presiden bersifat ngaret. SBY mengakui bahwa pasal pencemaran nama baik memang bersifat ngaret. Artinya, kata SBY, memang ada unsur subyektivitas.'],
            ['date'=>'10 Agustus 2015','content'=>'Ketua Mahkamah Konstitusi (MK) Arief Hidayat, mengatakan bahwa putusan MK itu final dan mengikat. Artinya, pasal tersebut takkan pernah lagi diterima sebagai delik aduan.']
        ]
    ],
    'INDIKASI'=>[
        'narasi'=>'<img src="http://usimages.detik.com/customthumb/2015/08/21/10/103225_ekokampungpulo.jpg?w=780&q=90" class="pic">Gubernur DKI Jakarta Basuki `Ahok` Tjahaja Purnama dilaporkan ke Komnas HAM terkait kekerasan yang terjadi saat penggusuran warga Kampung Pulo, Jakarta Timur. Gubernur Ahok dilaporkan oleh warga Kampung Pulo yang diwakili oleh Forum Warga Kota Jakarta (Fakta).
                    </p><p class="font_kecil"><img src="https://img.okezone.com/content/2015/08/20/338/1199829/oknum-satpol-diduga-pukuli-warga-saat-kerusahan-di-kampung-pulo-W8EzKTTzpz.jpg" class="pic2">Dalam investigasi  yang dilakukan oleh Fakta,ditemukan adanya pelanggaran HAM dalam proses penggusuran di Kampung Pulo. Bukti pelanggaran HAM mengacu pada cara-cara penangangan yang tidak sesuai prosedur. Pengerahan ribuan aparat polisi dan Tentara menjadi salah satu acuannya. Adapun soal tindakan kekerasan yang dilakukan oleh aparat menjadi bukti nyata adanya pelanggaran HAM. 
                    </p><p class="font_kecil">Warga dalam aduannya juga mengaku menderita akibat penggusuran tersebut. Gubernur Ahok dinilai semena-mena menggusur rumah dan bangunan yang ada di Kampung Pulo. Akibatnya ratusan kepala keluarga kehilangan tempat tinggal dan penghasilan ekonomi. Kini, warga Kampung Pulo tidak menentu nasibnya.'
    ],
    'LANGGANAN'=>[
        'narasi'=>'<img src="http://dekandidat.com/cms/wp-content/uploads/2015/02/banjir.jpg" class="pic">Kampung Pulo memang kerap menjadi langganan banjir tahunan yang terjadi di Jakarta. Setiap tahun, ketika musim hujan, Kampung Pulo digenani air banjir karena letaknya yang berada di bantaran kali Ciliwung. Banjir yang datang memaksa ratusan kepala keluarga diungsikan ke tempat yang aman. 
                    </p><p class="font_kecil"><img src="http://cms.monitorday.com/statis/dinamis/detail/12325.jpg" class="pic2">Pemrov DKI Jakarta beberapa kali mencari solusi agar banjir tidak terjadi lagi di Jakarta, terutama di Kampung Pulo dengan melakukan terobosan kanal timur. Kanal air tersebut dibuat untuk agar banjir kiriman yang kerap datang ke Jakarta melalui sungi Ciliwung tidak melewati tengah kota tapi pinggiran kota. 
                    </p><p class="font_kecil">Tapi cara tersebut sepertiya tidak efektif  dan tidak menjadi solusi penangan banjir. Pemrov DKI lalu mengambil langkah penggusuran, tetapi cara tersebut justru menimbulkan masalah baru. Warga tidak terima rumah dan bangunan mereka digusur dan bersikukuh untuk tetapa mendiami kawasana tersebut. Akhirnya bentrokan massal terjadi antara warga dan aparat keamananpun terjadi.'
    ],
    'SEJARAH'=>[
        'narasi'=>'<img src="http://rileksmedia.com/content/bataviatempodoler.jpg" class="pic">Kampung Pulo sudah ada sejak zaman Kolonial Belanda. Sejarah Kampung Pulo Jakarta dimulai tahun 1930. Kampung Pulo terletak  di bantaran sungai Ciliwung.dinamai Kampung Pulo karena kawasan tersebut mirip sebuah pulo. 
                    </p><p class="font_kecil">Secara gegrafis, Kampung Pulo berada di sisi utara dan timur Jakarta. Aliran sungai Ciliwung yang mengalir diantara sisi-sisi kampung Pulo berhulu di Bogor. Tidak mengherankan jika hamper setiap musim hujan tiba, kawasan tersebut banjir dan  memaksa warga setempat untuk mengungsi. 
                    </p><p class="font_kecil"><img src="https://fitriwardhono.files.wordpress.com/2012/04/banjir-04.jpg" class="pic2">Etnis Betawi merupakan mayoritas yang mendiami kawasan Kampung Pulo. Namun, sejak tahun 1970-an banyak pendatang yang datang dan menetap di kawasan tersebut. Mereka dari daerah Bogor dan sekitarnya. Sampai sekarang, beragam etnis seperti Tionghoa, Padang, Batak, Arab dan lainnya menjadi warga Kampung Pulo.
                    </p><p class="font_kecil">Pada masa kolonial Belanda, kampung tersebut merupakan bagian dari kawasan Meester Cornelis. Kampung seluas 8.575 hektar tersebut memiliki akar dan nilai sejarah antopologi kultural yang kuat.
                    </p><p class="font_kecil"><img src="http://1.bp.blogspot.com/-43uDyVWN3dE/Vd5wzWS5UQI/AAAAAAAADNI/1M3bGoeYO2E/s400/images%2B%25288%2529.jpg" class="pic">Selama empat abad, Meester Cornelis Jatinegara adalah salah satu pusat fungsional pertumbuhan Kota Jakarta. Fakta historis tersebut berhasil dihimpun Ivana Lee, pendamping warga dari LSM Ciliwung Merdeka yang pernah melakukan penelitian di wilayah tersebut.
                    </p><p class="font_kecil">Keberadaan sejumlah situs budaya religi dan tipologi arsitektur bangunan tempo dulu juga menjadi kekhasan Kampung Pulo.'
    ],
    'PROFIL'=>[
        'narasi'=>'Dalam Pasal 263 RUU KUHP ayat 1 yang disiapkan pemerintah disebutkan bahwa "Setiap orang yang di muka umum menghina presiden atau wakil presiden dipidana dengan pidana penjara paling lama 5 (lima) tahun atau pidana denda paling banyak Kategori IV".
                    </p><p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Dalam ayat selanjutnya ditambahkan, "Tidak merupakan penghinaan jika perbuatan sebagaimana dimaksud pada ayat (1) jelas dilakukan untuk kepentingan umum atau pembelaan diri".
                    </p><p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Aturan itu diperluas melalui Pasal 264 RUU KUHP yang berbunyi, "Setiap orang yang menyiarkan, mempertunjukkan, atau menempelkan tulisan atau gambar sehingga terlihat oleh umum, atau memperdengarkan rekaman sehingga terdengar oleh umum, yang berisi penghinaan terhadap Presiden atau Wakil Presiden dengan maksud agar isi penghinaan diketahui atau lebih diketahui umum, dipidana dengan pidana penjara paling lama lima tahun atau pidana denda paling banyak Kategori IV.”',
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']
        ],
        'institusi'=>[
            ['page_id'=>'pemerintahprovinsipemprovdki54c1c6c8c3ef3'],
            ['page_id'=>'dprddki541a5b6fb78db'],
            ['page_id'=>'RMP']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c']
        ],
        'institusi'=>[
            ['page_id'=>'komnasham54c1eac938164'],
            ['page_id'=>'lbhjakarta55dde9e5dac01'],
            ['page_id'=>'frontpembelaislam5317d72f772e0'],
            ['page_id'=>'ciliwungmerdekacm55dded96d8085'],
            ['page_id'=>'gerakanlawanahok55ddf49baf66b'],
            ['page_id'=>'Fakta']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'H.M Jusuf Kalla','jabatan'=>'Wakil Presiden RI','img'=>'http://metroonline.co/wp-content/uploads/2014/02/Jusuf-Kalla.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'Jadi sama sekali Pemerintah tidak bermaksud merugikan rakyat, tapi justru untuk memperbaiki kehidupan (rakyat), dan itu bukan hanya Kampung Pulo, di Tanjung Priok akan dibangun dulu. Nanti pindah semuanya'],
        ['from'=>'Ferry Mursyidan','jabatan'=>'Menteri Agraria','img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/05/29/400271/7zZI1rj5ti.jpg?w=668','url'=>'http://www.bijaks.net/aktor/profile/ferrymursyidanbaldan511b4ad3edaa1','content'=>'Di situ saja sudah masalah, maka ditata dalam penataan kawasan untuk membebaskan Jakarta dari bahaya banjir'],
        ['from'=>'Basuki Tjahja Purnama','jabatan'=>'Gubernur DKI','img'=>'http://media.forumkeadilan.com/2013/07/Basuki-Tjahaja-Purnama-Ahok-metrotvnews.com_.jpg','url'=>'http://www.bijaks.net/aktor/profile/basukitjahajapurnama50f600df48ac5','content'=>'Itu jadinya bukan hanya seperti rusun loh ya, ini sudah seperti kayak apartemen. Kalau kamu lihat itu kalau dijual sekarang saya kira Rp400 juta orang merem juga mau beli'],
        ['from'=>'Djarot Saiful Hidayat','jabatan'=>'Wakil Gubernur DKI Jakarta','img'=>'http://www.satuharapan.com/uploads/pics/news_39528_1434648302.jpg','url'=>'http://www.bijaks.net/aktor/profile/djarotsaifulhidayat5406bfc7406d7','content'=>'Kami sudah siapkan tempatnya, dan itu sangat layak. Kami punya program memberikan perumahan yang layak dan manusiawi mereka'],
        ['from'=>'Saefullah','jabatan'=>'Sekda DKI Jakarta','img'=>'http://assets.kompas.com/data/photo/2014/06/17/0958406saefullah1p.jpg','url'=>'http://www.bijaks.net/aktor/profile/saefullah54c09e806b22b','content'=>'Program ini harus terus, karena untuk kepentingan bersama, bukan untuk kepentingan siapa-siapa. Tujuan ini untuk angkat penyakit Jakarta, kalau tidak mulai dari sekarang kapan lagi terlebih situasi dengan bagus'],
        ['from'=>'Prasetyo Edi Marsudi','jabatan'=>'Ketua DPRD DKI','img'=>'http://cdn-2.tstatic.net/pontianak/foto/bank/images/prasetyo-edi-marsudi.jpg','url'=>'http://www.bijaks.net/aktor/profile/prasetyoedimarsudi51d223cb94d7e','content'=>'Rusunnya bagus dan nyaman, seharusnya tidak ada yang menolak. Mungkin karena perubahan pola hidup saja. Diajak hidup yang lebih manusiawi masa tidak mau?'],
        ['from'=>'Bambang Musyawardana','jabatan'=>'Walikota Jakarta Timur','img'=>'https://pbs.twimg.com/media/B7claewCQAQU71V.jpg','url'=>'http://www.bijaks.net/aktor/profile/bambangmusyawardana54c1b5fe35af6','content'=>'Kami apresiasi, molornya sudah terlalu panjang, karena itu atas perintah gubernur kita akan reloksi, kurang lebih ada 520 rumah yang akan kita relokasi'],
        ['from'=>'Maruar Sirait','jabatan'=>'Ketum RMP','img'=>'http://www.luwuraya.net/wp-content/uploads/2012/03/maruarar-sirait2.jpg','url'=>'http://www.bijaks.net/aktor/profile/Maruarar','content'=>'Kalau kebijakan bagus, kita harus jujur bilang bagus. Kita dukung Pak Ahok. Di saat yang sama, warga juga harus ikut berpartisipasi dalam mendukung kebijakan pemerintah yang benar-benar pro-rakyat.'],
        ['from'=>'Kombes Pol Umar Faroq','jabatan'=>'Kapolrestro Jakarta Timur','img'=>'http://www.rmoljakarta.com/images/berita/thumb/thumb_357794_07054009062015_kapolres_metro_jakarta_timur.jpg','url'=>'','content'=>'Makanya kita bertindak represif ya. Kami sebenarnya tidak menghendaki ini. Tapi ini sudah tugas dan sewajarnya kami lakukan']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Puan Maharani','jabatan'=>'Menko PMK','img'=>'http://www.seputarrakyat.com/wp-content/uploads/2014/10/Puan-Maharani.jpg','url'=>'http://www.bijaks.net/aktor/profile/puanmaharani5104bbe40ed12','content'=>'Itu bisa dilakukan dengan lebih persuasif dan mengajak lebih berbicara dengan lebik baik. Dan saya rasa gubernur akan melakukan hal itu karena saya dengar ke depannya insyaallah enggak ada lagi hal-hal seperti itu'],
        ['from'=>'Syarif','jabatan'=>'Sekretaris Komisi A‎ DPRD DKI Jakarta','img'=>'http://www.aktual.com/wp-content/uploads/2015/06/Syarief-Hasil-Angket-Akan-Selesai-Sebelum-25-Maret-Nanti.jpg','url'=>'','content'=>'Kami mengutuk aparat. Kenapa tidak bisa melakukan penggusuran dengan cara baik-bai‎k kepada warga Kampung Pulo?'],
        ['from'=>'Muhammad Nur Khoirun','jabatan'=>'Komisioner Komnas HAM','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2014--08--75123-komisioner-komnas-ham-selidiki-pemukulan-pendukung-prabowo-datangi-polda.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadnurkhoiron51a55eeb05f62','content'=>'Kalau merampas hak kepemilikan seseorang secara paksa, itu sudah melanggar HAM, tapi kalau dari sisi masyarakat yang membakar alat berat, itu masuknya pada pelanggaran hukum'],
        ['from'=>'Sandyawan Sumardi','jabatan'=>'Direktur Ciliwung Merdeka','img'=>'http://www.satuharapan.com/uploads/pics/news_63_1400069596.JPG','url'=>'http://www.bijaks.net/aktor/profile/ignatiussandyawansumardi55ddfe2606236','content'=>'Karena warga dianggap tidak punya surat-surat tanah sama sekali sehingga tidak ada ganti rugi apapun'],
        ['from'=>'JJ Rizal','jabatan'=>'Sejarawan Betawi','img'=>'http://cdnimage.terbitsport.com/image.php?width=650&image=http://cdnimage.terbitsport.com/imagebank/gallery/large/20150821_010534_harianterbit_jj_rizal-2.jpg','url'=>'http://www.bijaks.net/aktor/profile/jjrizal55de0182e033d','content'=>'Normalisasi yang terjadi bukan normalisasi tapi betonisasi'],
        ['from'=>'Tegar Putuhena','jabatan'=>'Ketua Lawan Ahok','img'=>'https://0.academia-photos.com/16029549/4348171/5049555/s200_tegar.putuhena.jpg_oh_dc9ed1cc3e5e9673c24d8209575edb7b_oe_54702ef5___gda___1417628574_45cdd677af5285287f96def887375232','url'=>'','content'=>'Gerakan Lawan Ahok kami buat setelah rapat maraton tadi malam dengan aktivis mahasiswa dan masyarakat. Terkait tindak kekerasan yang dilakukan Ahok terhadap Kampung Pulo'],
        ['from'=>'Fadhli Nasution','jabatan'=>'Kuasa hukum Organisasi Masyarakat Lawan Ahok','img'=>'http://www.rmol.co/images/berita/normal/450623_01325810072015_fadli_nasution1.jpg','url'=>'','content'=>'Kami akan laporkan tindakan Ahok yang telah melanggar HAM ini ke Pengadilan internasional sebab, penggusuran Kampung Pulo telah mendunia'],
        ['from'=>'Nirwono','jabatan'=>'Pengamat Tata Kota','img'=>'http://gambar.radarpena.com/mei/images/nirwono%20yoga1.jpg','url'=>'','content'=>'Kalau memang berniat mengatasi banjir, jangan hanya rumah-rumah orang miskin saja yang ditertibkan. Tertibkan juga pengembang-pengembang nakal yang membangun properti di atas daerah resapan'],
        ['from'=>'Habib Salim Alatas (Habib Selon)','jabatan'=>'Tokoh FPI','img'=>'http://cdn-2.tstatic.net/tribunnews/foto/images/preview/20120516_Foto_Habib_Salim_Alatas_FPI__1929.jpg','url'=>'http://www.bijaks.net/aktor/profile/habibsalimalatas51edeb2ba5db8','content'=>'Harusnya pemerintah tanya maunya warga apa, ya diomongin. Ngopi-ngopi bareng kan biar lebih enak. Biar Mufakat juga kayak Tanah Abang waktu itu'],
        ['from'=>'Azas Tigor Nainggolan','jabatan'=>'Ketua FAKTA','img'=>'http://beritatrans.com/cms/wp-content/uploads/2014/12/IMG_20141202_115833_edit.jpg','url'=>'http://www.bijaks.net/aktor/profile/azastigornainggolan52c61d2ab63a5','content'=>'Saya kira jumlah aparat yang diturunkan terlalu berlebihan karena sebetulnya warga tidak pernah menolak direlokasi, namun ada dialog yang belum tuntas'],
        ['from'=>'Tommy Soeharto','jabatan'=>'putra sulung Soeharto','img'=>'http://rmol.co/images/berita/normal/948857_05481002062015_tommy_soeharto','url'=>'http://www.bijaks.net/aktor/profile/hutomomandalaputra-tommy504d5dedbc8cd','content'=>'Alasan rakyat bertahan tidak perlu bertanya kenapa? Ada apa? Tentu saja mereka mempertahankan Hak hidup mereka, Mempertahankan sumber Penghidupan keluarga mereka, tempat tinggal mereka..Orang tidak sekolah juga tau..!!!'],
        ['from'=>'LBH Jakarta','jabatan'=>'Atika Yuanita Paraswaty','img'=>'http://assets.kompas.com/data/photo/2015/08/26/1250015image780x390.jpg','url'=>'','content'=>'Banyak pelanggaran prosedur yang terjadi dalam proses penggusuran. Itu karena mereka (Pemprov) tidak punya aturan yang jelas. Setidaknya harus ada SOP (standard operating procedure)']
    ],
    'VIDEO'=>[
        ['id'=>'2dwqpJ-I6jU'],
        ['id'=>'Ji1mbUvsdXw'],
        ['id'=>'ktBRpWVWi20'],
        ['id'=>'ktBRpWVWi20'],
        ['id'=>'SWCD00aSKBs'],
        ['id'=>'URiXU8bwGb4'],
        ['id'=>'43F75Vj1rL8'],
        ['id'=>'82i01tPZXFc'],
        ['id'=>'GT_buT3UO1Q'],
        ['id'=>'P3C4R8H0xws'],
        ['id'=>'0503f0spBl8'],
        ['id'=>'Au_F7QhIU5Q']
    ],
    'FOTO'=>[
        ['img'=>'http://cms.monitorday.com/statis/dinamis/detail/12620.jpg'],
        ['img'=>'http://www.indoberita.com/wp-content/uploads/2015/08/Kampung-Pulo.jpg'],
        ['img'=>'http://i.ytimg.com/vi/bujUmRIsmRs/hqdefault.jpg'],
        ['img'=>'http://2.bp.blogspot.com/-CI9uqWAnMC0/VdcBMxNd6yI/AAAAAAAAA50/VpAiVnUWMB8/s320/hari-ini-kampung-pulo-digusur-KUwT5KCKTw.jpg'],
        ['img'=>'https://i.ytimg.com/vi/a3u52RvOoaI/hqdefault.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2014/08/28/id_319449/319449_620.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/963798/big/068847500_1440307279-20150823-penggusruan_Kampung_Pulo-Jakarta.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/962859/big/062511500_1440161444-20150821-Kampung_Pulo-Jakarta.jpg'],
        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/961141/big/059002000_1440052261-20150820-Kampung-Pulo4.jpg'],
        ['img'=>'http://i.ytimg.com/vi/MGtRyyVep7k/hqdefault.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/08/20/422820/AQAWFUcZgk.jpg?w=668'],
        ['img'=>'http://i.ytimg.com/vi/SQbGQ831AzI/hqdefault.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/banjarmasin/foto/bank/images/gerakan-lawan-ahok_20150824_211012.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/08/24/1057531image780x390.jpg'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/08/25/170/1036908/si-pitung-sambangi-posko-gerakan-lawan-ahok-oiM.jpg'],
        ['img'=>'http://imgix.production.liputan6.static6.com/medias/921070/original/038528300_1436246314-ahok.jpg?w=673&h=373&fit=crop&crop=faces'],
        ['img'=>'http://assets.rappler.com/90194ED25D4346E7A603C9E01BAC06E4/img/2263B171BE9B486E8D4427A1B914619E/kampung_pulo_rplr6_2263B171BE9B486E8D4427A1B914619E.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/962716/big/007600500_1440155842-20180821-Kampung-Pulo-Jakarta.jpg'],
        ['img'=>'http://assets.rappler.com/612F469A6EA84F6BAE882D2B94A4B421/img/3CAF6203D96C418294132A7049B3AC41/img_9480_3CAF6203D96C418294132A7049B3AC41.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/08/20/160099/CVgB2fiDml.jpg?w=668'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/perumahan-di-kampung-pulo-jatinegara-yang-berada-di-bantaran-_150823165228-307.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/07/28/151317/WwEQBKHL7M.jpg?w=668'],
        ['img'=>'http://assets.tokohindonesia.com/berita/ti_1813469KampungPulo231440068981-preview780x390.jpg'],
        ['img'=>'http://www.infonitas.com/public/media/images/thumb/2015/08/25/2015-08-25-11-37-39_JJ-Rizal-dan-Ahok_thumb_634_350_c.jpg'],
        ['img'=>'http://smeaker.com/nasional/wp-content/uploads/2015/08/Tak-Gentar-JJ-Rizal-Terima-Tantangan-Debat-Ahok-Tommy-Ahok-Bukan-Tukang-%E2%80%9CGusur%E2%80%9D1.jpg'],
        ['img'=>'http://i.ytimg.com/vi/ItFf4GYRGzU/0.jpg'],
        ['img'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcSjfWFhZ201wn578PRuQnMiTfQVb8PEjg5aCVTQoMe3tjH6DqQ'],
        ['img'=>'https://img.okezone.com/content/2015/08/20/338/1199561/relokasi-warga-kampung-pulo-dki-siapkan-1-100-unit-rusun-eLPp8zBEUQ.jpg'],
        ['img'=>'http://assets.kompas.com/data/2012/todaysphoto/upload/photo/808/RusunKampungPulo221440243141_preview.jpg'],
        ['img'=>'http://www.infonitas.com/public/media/images/thumb/2015/08/20/2015-08-20-13-53-54_kampung%20pulo%20rusun_thumb_634_350_c.jpg']
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/kampungpulo/top.jpg")?>') no-repeat transparent;
        height: 1352px;
        margin-bottom: -620px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .font_kecil {
        font-size: 13px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 20%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 78%;
        height: auto;
        /* background-color: green; */
        float: right;
        padding-left: 1%;
        border-left: 1px dotted black;
        display: inline-table;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
        background: rgba(0, 0, 0, 0.7);
    }
    .block_red {
        background-color: #761908;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 80%;
        height: auto;
        margin: 0 auto;
        display: inherit;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        margin-right: 10px;
        max-width: 50px;
    }
    .garis {
        border-top: 1px dotted black;
    }
    .boxgray {
        width: 99%;
        border: 5px solid lightgray;
        box-shadow: -5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }
    .penyokong {
        width: 100%;
        height: auto;
        display: inline-block;
        margin-left: 20px;
    }
    .boxpenyokong {
        width: 126px;
        height: auto;
    }
    .foto {
        width: 100px;
        height: 100px;
        border: 3px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
        padding: 10px 10px;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 113px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100%;padding: 0px !important;font-size: 8px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 110px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi2 img {width: 98px; height: 70px;  padding: 0px !important;}
    li.organisasi2 p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 121px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
    li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.dukung {float: left; padding:3px;vertical-align: top;width: 177px;height:115px;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
    li.dukung img {width: auto; height: 75px;padding: 0px !important;margin: 0 auto;display: block;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;line-height: 12px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }   
    .uprow {margin-left: 20px;margin-top:10px;border-color: #761908 transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {
        width: 260px;
        height: auto;
        margin-left: 20px;
        margin-top:0px;
        margin-bottom: 10px;
        background-color: #424040;
        background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px #761908;
        border-right:solid 2px #761908;
        border-bottom:solid 2px #761908;
        border-radius: 0 0 5px 5px;
        color: #ffffff;
        z-index: 100;
    }
    .kronologi-title {font-size:14px;font-weight:bold;background-color: #761908;color: white;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {line-height: 15px;font-size:12px;}

    .ketua {
        background-color: yellow;
        width: 30%;
        display: block;
        float: left;
        margin-right: 3%;
        box-shadow: -3px 3px 10px gray;
        border-radius: 8px;
        margin-bottom: 15px;
    }
    .ketua img {
        width: 100%;
        height: 200px;
    }
    .kritik {
        font-size: 18px;
        font-weight: bold;
        color: black;
    }
    .isi-col50 {
        float: left;
        margin: 10px 16px;
        width: 41%;
        height: auto;
        display: inline-block;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #29166f;
        width: 293px;
        height: 345px;
        float: left;
        margin-bottom: 10px;
        padding-top: 5px;
        margin-left: 10px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        float: left;
        border: 1px solid black;
    }
    #bulet {
        background-color: #ffff00; 
        text-align: center;
        width: 50px;
        height: 25px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: black;
        padding: 6px 10px;
        margin-left: -10px;
        margin-right: 10px;
    }    
    .gallery {
        list-style: none outside none;
        padding-left: 0;
        margin-left: 0px;
    }
    .gallery li {
        display: block;
        float: left;
        height: 80px;
        margin-bottom: 7px;
        margin-right: 0px;
        width: 120px;
    }
    .gallery li a {
        height: 100px;
        width: 100px;
    }
    .gallery li a img {
        max-width: 115px;
    }

</style>
<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.thumb2').click(function(){
        var target = $(this).data('homeid');
        var src = $(this).attr('src');
        $('#'+target).attr('src',src);
    });
    $(".gallery").lightGallery();
    $(".gallery2").lightGallery();
})
</script>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <h4><a id="analisa" style="color: black;">KONTROVERSI TANAH KAMPUNG PULO</a></h4>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">DALIH PENGGUSURAN</a></h4>
                <p class="font_kecil"><?php echo $data['ANALISA']['narasi'];?></p>
                <ul>
                <?php 
                foreach ($data['ANALISA']['list'] as $key => $value) { ?>
                    <li style="list-style-type: square;"><?php echo $value['isi'];?></li> <?php
                } ?>
                </ul>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="prokontra" style="color: black;">PRO DAN KONTRA</a></h4>
                <p class="font_kecil"><?php echo $data['PROKONTRA']['narasi'];?></p>
                <div class="clear"></div>
            </div>

            <div class="col_kanan">
                <div class="boxprofile white">
                    <h4 class="block_red text-center"><a id="profil" style="color: white;">PROFIL KAMPUNG PULO</a></h4>
                    <img src="http://britabagus.com/wp-content/uploads/2015/08/resapanair3889p.jpg" class="picprofil"><br>
                    <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Secara geografis wilayah kampung pulo terdiri dari dua rukun warga dan berada di sisi selatan, timur dan utara kali ciliwung yang berhulu dari kota bogor.</p>
                    <ul style="margin-left: 20px;margin-right: 20px;">Selain itu, kampung pulo berbatasan dengan kawasan Meester Cornelis di sisi barat.
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Luas Wilayah: 8.575 hektar</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Jumlah Warga : 3.809 KK</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Mushollah Tertua bernama Mushollah Al-Awwabin</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Situs: makam Kyai Lukman nul, Hakim/Datuk (sebelum 1930), makam, Habib Said.</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Etnis Asli : Betawi</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Etnis Pendatang : Bugis, Tionghoa, Padang, Batak, Arab</li>
                    </ul>
                </div>

                <!-- <h4 class="list" style="margin-left: 20px;"><a id="kronologi" style="color: black;">KRONOLOGI</a></h4>
                <?php 
                foreach ($data['KRONOLOGI']['list'] as $key => $value) {
                ?>
                <div class="uprow"></div>
                <div class="kronologi">
                    <div class="kronologi-title"><?php echo $value['date'];?></div>
                    <div class="kronologi-info">
                        <p><?php echo $value['content'];?></p>
                    </div>
                </div>
                <div class="clear"></div>
                <?php
                }
                ?> -->

                <h4 class="list" style="margin-left: 20px;margin-top: 25px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="newskampungpulo_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newskampungpulo_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>

            <div class="col_full">
                <div class="garis"></div>
                <h4 class="list"><a id="analisa" style="color: black;">TERINDIKASI MELANGGAR HAM</a></h4>
                <p class="font_kecil"><?php echo $data['INDIKASI']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">LANGGANAN BANJIR</a></h4>
                <p class="font_kecil"><?php echo $data['LANGGANAN']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">SEJARAH KAMPUNG PULO</a></h4>
                <p class="font_kecil"><?php echo $data['SEJARAH']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <div class="col_kiri50">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARTAI PENDUKUNG</p>
                    <ul style="margin-left: 0px;">
                        <?php
                        foreach($data['PENDUKUNG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi" style="margin-left: 40px;">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="col_kanan50">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENDUKUNG</p>
                    <ul style="margin-left: 0px;">
                        <?php
                        foreach($data['PENDUKUNG']['institusi'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'RMP'){
                                $photo = 'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/dsc_0179jpg_20150825_163325.jpg';
                                $pageName = 'Relawan Merah Putih (RMP)';
                            }
                            elseif($val['page_id'] == 'dprddki541a5b6fb78db'){
                                $photo = 'http://www.jpnn.com/picture/normal/20150709_232117/232117_54378_dprd_dki.jpg';
                                $pageName = 'DPRD DKI';
                            }
                            elseif($val['page_id'] == 'pemerintahprovinsipemprovdki54c1c6c8c3ef3'){
                                $photo = 'http://www.aktualpost.com/wp-content/uploads/2015/05/LOGO-DKI.jpg';
                                $pageName = 'Pemrov DKI';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
                <div class="clear"></div><h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <div class="col_kiri50">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARTAI PENENTANG</p>
                    <ul style="margin-left: 0px;">
                        <?php
                        foreach($data['PENENTANG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi" style="margin-left: 40px;">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="col_kanan50">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENENTANG</p>
                    <ul style="margin-left: 0px;">
                        <?php
                        foreach($data['PENENTANG']['institusi'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'Fakta'){
                                $photo = 'http://cdn1-a.production.liputan6.static6.com/medias/862037/big/093319200_1430043292-Demo-Anti-Rokok.jpg';
                                $pageName = 'Fakta';
                            }
                            elseif($val['page_id'] == 'komnasham54c1eac938164'){
                                $photo = 'http://bantenpos.co/wp-content/uploads/2014/10/LOGO-KOMNAS-HAM.png';
                                $pageName = 'KOMNAS HAM';
                            }
                            elseif($val['page_id'] == 'lbhjakarta55dde9e5dac01'){
                                $photo = 'http://images.hukumonline.com/frontend/lt5499ec4a7fbb2/lt5499ed062fc47.jpg';
                                $pageName = 'LBH Jakarta';
                            }
                            elseif($val['page_id'] == 'ciliwungmerdekacm55dded96d8085'){
                                $photo = 'https://ciliwungmerdekajakarta.files.wordpress.com/2012/05/cm-y-copy.jpg?w=627&h=1672';
                                $pageName = 'Ciliwung Merdeka';
                            }
                            elseif($val['page_id'] == 'gerakanlawanahok55ddf49baf66b'){
                                $photo = 'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/ormas_20150822_175105.jpg';
                                $pageName = 'Gerakan Lawan Ahok';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
                <div class="clear"></div>
                    <ul style="margin-left: 0px;margin-top: 10px;">
                        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
                            <?php 
                            for($i=0;$i<=4;$i++){
                            ?>
                            <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 77%;margin-right: 10px;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div style="float: left;width: 47%;margin-left: 20px;">
                            <?php 
                            for($i=5;$i<=8;$i++){
                            ?>
                            <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 78%;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </ul>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENENTANG</a></h4>
                <div class="clear"></div>
                <ul style="margin-left: 0px;margin-top: 10px;">
                    <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
                        <?php 
                        for($i=0;$i<=5;$i++){
                        ?>
                        <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 77%;margin-right: 10px;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div style="float: left;width: 47%;margin-left: 20px;">
                        <?php 
                        for($i=6;$i<=10;$i++){
                        ?>
                        <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 78%;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                </ul>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
            <div class="col_full" style="display: inline-block;">
                <ul id="light-gallery" class="gallery" style="margin-left: 0px;margin-top: 0px;">
                    <?php
                    foreach ($data['FOTO'] as $key => $val) { ?>                    
                        
                        <li data-src="<?php echo $val['img'];?>" style="overflow: hidden;">
                            <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                            </a>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 10px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>
            <div class="col_full">
                <div class="boxgray" style="height: 220px;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>