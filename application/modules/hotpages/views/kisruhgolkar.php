<?php 
$data = [
    'NARASI'=>[
        'title'=>'RONDE TERAKHIR KISRUH GOLKAR',
        'narasi'=>'<img src="http://www.harjasaputra.com/media/k2/items/cache/72ca15047a78e7b4a322422f38badf4b_XL.jpg" class="pic">Partai Golkar di kancah perpolitikan nasional sedang berada pada kondisi labil. Lebih dari enam dekade terkahir sejak lahirnya Golkar, eksistensi Golkar relatif stabil sebagai partai besar, yang mampu menancapkan pengaruhnya di setiap rezim yang dilaluinya. 
                </p><p>Namun di rezim pemerintahan Jokowi-JK, Golkar di bawah kepemimpinan Aburizal Bakrie diguncang kisruh internal yang berkepanjangan. Konflik internal partai berlambang beringin berimbas pada munculnya dualisme kepemimpinan, Aburizal Bakrie versi Munas Bali dan Agung Laksono versi Munas Jakarta. Tidak sampai di situ, polemik mengerucut ke perebutan kantor sekretariat fraksi di parlemen.
                </p><p><img src="http://us.images.detik.com/customthumb/2015/02/11/10/084358_dangdutmunas3.jpg?w=500" class="pic2">Surat Keputusan (SK) dari Kemenkumham mengindikasikan adanya intervensi politik yang dilakukan pemerintah terhadap polemik di internal Partai Golkar. SK tersebut merupakan pesanan Megawati ke Menteri Yasona. Megawati sebelumnya menerima masukan dari Jusuf Kalla dan surya Palloh agar mengitruksikan ke Menteri Yasona untuk mengeluarkan SK kepengerusan partai Golkar hasil Munas Jakarta. Dengan memberikan legalitas hukum pada kepengurusan Golkar munas Jakarta dan mendudukkan Agung Laksono sebagai ketua umumnya, maka Golkar akan lebih mudah digiring untuk masuk ke dalam gerbang partai koalisi pendukung pemerintah, Koalisi Indonesia hebat (KIH). Bergabungnya Golkar ke dalam koalisi indonesia hebat akan memperkokoh kekuatan partai pendukung pemerintah di parlemen.
                </p><p>Eskalasi konflik semakin meluas. Putusan sela Pengadilan Negeri Jakarta Utara membuat kubu Agung Laksono meradang. Dalam putusan itu, PN Jakarta Utara mengabulkan permintaan dari penggugat, yakni Aburizal Bakrie dan Idrus Marham dan mengembalikan Golkar ke kepengurusan Munas Riau 2009. Golkar terancam tidak bisa mengikuti kontestasi politik, Pilkada serentak desember nanti karena tidak adanya kepengurusan yang sah. Akhirnya, jalan islah dirumuskan sebagai upaya penyelamatan Golkar.'
    ],
    'PROFIL'=>[
        'sejarah'=>[
            ['no'=>'Tahun 1964. Lahir sebagai Sekretariat Bersama Golkar (Sekber) diakhir pemerintahan Presiden Soekarno. '],
            ['no'=>'Didirikan oleh golongan militer, khususnya perwira Angkatan Darat. terpilih sebagai Ketua Pertama Sekber Golkar adalah Brigadir Jenderal (Brigjen) Djuhartono sebelum digantikan Mayor Jenderal (Mayjen) Suprapto Sukowati.'],
            ['no'=>'Tanggal 4 Februari 1970, Sekber Golkar menetapkan satu nama dan tanda gambar yaitu Golongan Karya (GOLKAR). Logo dan nama ini, sejak Pemilu 1971, tetap dipertahankan sampai sekarang.'],
            ['no'=>'Tahun 1971 Golkar ikut dalam pemilu dan menjadi pemeang dengan 34.348.673 suara atau 62,79 % dari total perolehan suara. Ini adalah pemilu pertama untuk Golkar.'],
            ['no'=>'Pada tanggal 17 Juli 1971 Sekber GOLKAR mengubah dirinya menjadi GOLKAR.'],
            ['no'=>'Selama puluhan tahun Orde Baru berkuasa, jabatan-jabatan dalam struktur eksekutif, legislatif dan yudikatif, hampir semuanya diduduki oleh kader-kader Golkar.'],
            ['no'=>'Kemenangan Golkar selalu diukir dalam pemilu di tahun 1977, 1982, 1987, 1992, dan 1997.'],
            ['no'=>'Tahun 1988 Soeharto lengser dari singgasana kekuasaannya, yang kemudian berimbas pada tuntutan pembubaran Golkar.'],
            ['no'=>'AkbarTandjung terpilih sebagai ketua umum dan Golkar berubah wujud menjadi PartaGolkar.'],
            ['no'=>'Di bawah kepemimpinan Akbar Tandjung, Golkar berhasil bertahan dari serangan eksternal dan krisis citra, inilah yang membuat Akbar menjadi ketua umum Golkar yang cukup legendaris.'],
            ['no'=>'Pada pemilu pertama di Era Reformasi ini Partai Golkar mengalami penurunan suara  di peringkat ke dua di bawah PDIP. Namun pada pemilu berikutnya Golkar kembali unggul.'],
            ['no'=>'Pemilu legislatif 2009 suara Partai Golkar kembali turun ke posisi dua. Tahun 2004 Golkar menjadi pemenang pemilu legislatif dengan 24.480.757 suara atau 21,58% suara sah. Namun pada pemilu 2014 Partai  Golkar kembali harus rela berada di posisi dua dengan perolehan suara 18.432.312 (14,75 persen)']
        ]
    ],
    'SALING'=>'<img src="http://inilahdepok.com/wp-content/uploads/2015/02/agung-ical-620x330.jpg" class="pic">Aburizal Bakrie yang terpilih jadi  ketua umum Golkar versi musyawarah nasional di Bali terus mendapat perlawanan dari kubu Agung Laksono. Gugutan yang dilayangkan ke PN Jakarta Utara ditolak. Putusan pengadilan menyatakan tidak memiliki wewenang mengadili sengketa partai. Kepengurusan diselesaikan melalui mekanisme internal partai, Mahkamah Partai. Jalur hukum yang dimenangkan kubu Agung menyudutkan posisi Aburizal Bakrie.
                </p><p>Legalitas kepengurusan Agung Laksono semakin kuat setelah Kemenkumhan mengeluarkan SK sah untuk kepengurusan Agung Laksono. Namun, kubu Agung tidak tinggal diam. Melalui Pengacaranya, Yusril Ihza Mahendra melayangkan gugatan ke Pengadilan Tinggi Tata Usaha Negara Jakarta Utara, yang kemudian memenangkan kubu Ical. Putusan sela yang menetapkan penundaan pelaksanaan surat Kementerian, sehingga mengembalikan kepengurusan Golkar sesuai dengan Musyawarah Nasional Riau.
                </p><p><img src="http://img.bisnis.com/posts/2015/05/21/435866/golkar-idrus-marham.jpg" class="pic2">“Dari putusan penundaan ini, kepengurusan Pengurus Pusat Golkar Agung Laksono tak boleh mengambil tindakan administratif dan politik apa pun juga,” kata kuasa hukum Aburizal Bakrie, Yusril Ihza Mahendra.
                </p><p>Keputusan PTUN itu langsung diiukuti langkah Wakil Ketua DPR Fadli Zon dengan memutuskan pimpinan Dewan Perwakilan Rakyat akan menyatakan status quo atas kepengurusan Fraksi Partai Golkar di lembaga legislatif. “Pimpinan tak bisa membacakan dua surat perombakan fraksi, jadi kami anggap status quo sampai inkracht,” kata politikus Partai Gerindra tersebut.
                </p><p>Selain memenangkan putusan sela. Pada 18 Mei 2015 Pengadilan Tata Usaha Negara memenangkan kubu Aburizal Bakrie dalam putusan sengketa Partai Golkar. Majelis hakim memerintahkan Menteri Hukum dan HAM mencabut SK yang mengesahkan kepengurusan Agung Laksono. Sebaliknya Hakim mengesahkan SK Menkumham lama hasil Munas Riau 2009 yang mengakui kepengurusan Ical. Dengan demikian, konflik semakin memanas.',
    'SIASAT'=>'<img src="http://www.itoday.co.id/wp-content/uploads/2012/12/bakrieical.jpg" class="pic">Hasrat Aburizal Bakrie untuk kembali menduduki pucuk pimpinan partai berlambang pohon beringin mulai menguat ketika ada upaya untuk mempercepat Musyawarah Nasional IX yang sedianya akan dilaksanakan pada Januari 2015 menjadi tanggal 30 November 2014. Perubahan jadwal tersebut disinyalir sebagai siasat Ical untuk dapat dapat kembali memegang kendali kekuasaan di Golkar.
                </p><p>Percepatan munas disebut-sebut bertujuan untuk mendorong terjadinya aklamasi pemilihan Aburizal Bakrie menjadi ketua umum kembali. Di sisi lain, percepatan munas akan membatasi waktu bagi calon ketua umum lain untuk melakukan konsolidasi.
                </p><p><img src="http://liputanislam.com/wp-content/uploads/2014/04/bakrie_aburizal.jpg" class="pic2">Agung Laksono mengakui, percepatan munas bakal mempersulit langkah kandidat lain untuk bersaing. Pasalnya, waktu untuk mengumpulkan dukungan plus mempersiapkan logistik menjadi sangat singkat.
                </p><p>Percepatan Munas dari jadwal semula Januari 2015 dianggap merugikan kandidat yang mencalonkan diri. Informasi bahwa percepatan munas disengaja agar para caketum, kecuali Ical, tidak siap.',
    'MENJEGAL'=>'Tujuh calon yang sedianya akan ikut dalam bursa pemilihan ketua umum  bersatu dengan hanya mengusung satu nama untuk melawan Aburizal Bakrie ( Ical) dalam Munas IX. Agung Laksono adalah aktor dibalik manuver tersebut. Dengan begitu agenda regenerasi kepimimpinan Golkar akan terealisasi. Penolakan terhadap Aburizal Bakrie sudah jauh-jauh hari sudah terdengar.
                </p><p><img src="http://www.posmetro-medan.com/wp-content/uploads/2015/04/Agung-Laksono-yang-bersiteru-dengan-ARB.jpg" class="pic">Beberapa kader Golkar merasa kecewa dengan kinerja yang ditunjukkan Ical dalam mengawal partai Golkar di kancah perpolitikan nasional. Golkar mengalami penurunan popularitas sejak dipegang oleh Ical. Indikatornya adalah untuk pertama kalinya Golkar tidak memiliki calon dalam ajang Pilpres 2014.
                </p><p>Dan ini menunjukkan Golkar gagal menjaga tradisi sebagai parpol yang selalu menyumbangkan kader terbaiknya untuk diusung sebagai calon presiden. Fakta inilah yang menajdi acuan harus dilakukannya regenerasi kepemimpinan di dalam tubuh Golkar. Demi gengsi dan nama besar Golkar.',
    'KETUA'=>[
        ['name'=>'Agung Laksono','thn'=>'(2015)','img'=>'http://berita2bahasa.com/images/articles/2015313agung%20laksono%20-%20tempo%20co%20-%20b.jpg' ],
        ['name'=>'Aburizal Bakrie','thn'=>'(2009 - 2015)','img'=>'http://upload.wikimedia.org/wikipedia/commons/4/45/Aburizal_Bakrie_-_March_2011.jpeg' ],
        ['name'=>'Jusuf Kalla','thn'=>'(2004–2009)','img'=>'http://upload.wikimedia.org/wikipedia/commons/7/71/Jusuf_Kalla.jpg' ],
        ['name'=>'Akbar Tandjung','thn'=>'(1998–2004)','img'=>'http://upload.wikimedia.org/wikipedia/id/f/f4/Akbar_Tanjung.jpg' ],
        ['name'=>'Harmoko','thn'=>'(1993–1998)','img'=>'http://upload.wikimedia.org/wikipedia/commons/7/75/Harmoko.jpg' ],
        ['name'=>'Wahono','thn'=>'(1988–1993)','img'=>'http://upload.wikimedia.org/wikipedia/id/9/98/Wahono.jpg' ],
        ['name'=>'Sudharmono','thn'=>'(1983–1988)','img'=>'http://upload.wikimedia.org/wikipedia/commons/0/02/Sudharmono2.jpg' ],
        ['name'=>'Amir Moertono','thn'=>'(1973–1983)','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png' ],
        ['name'=>'Suprapto Sukowati','thn'=>'(1969–1973)','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png' ],
        ['name'=>'Djuhartono','thn'=>'(1964-1969)','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png' ]
    ],
    'KRONOLOGI'=>[
        ['date'=>'24 November 2014','content'=>'Senin (24/11/2015) para pengurus partai menggelar rapat pleno dengan agenda persiapan  Munas IX digelar di Kantor DPP Partai Golkar, Slipi Jakarta.'],
        ['date'=>'25 November 2014','content'=>'Sehari setelahnya, yakni selasa (25/11/2014) dua kelompok massa yakni Angkatan Muda Partai Golkar (AMPG) yang dipimpin oleh Yorrys Raweyai bentrok dengan massa AMPG pimpinan Ahmad Doli yang mengklaim diri sebagai AMPG resmi di kantor DPP Partai Golkar, Jalan Anggrek Nelly Murni XI-A, Slipi, Jakarta.'],
        ['date'=>'30 November - 3 Desember 2014','content'=>'Munas IX Partai Golkar digelar di Nusa Dua, Bali. Hasil Munas menetapkan Aburizal Bakrie sebagai ketua umum terpilih.'],
        ['date'=>'04 Desember 2014','content'=>'Kamis (4/12/2014 Ketua Umum Aburizal Bakrie akhirnya mengumumkan susunan kabinet kepengurusan DPP Golkar periode 2014-2019 hasil Munas Bali.'],
        ['date'=>'06 Desember 2014','content'=>'Sabtu (6/12/2014) Musyawarah Nasional IX Partai Golkar tandingan yang digelar kubu Agung Laksono cs resmi dibuka,  di Ballroom Hotel Mercure, Ancol, Jakarta'],
        ['date'=>'03 Maret 2015','content'=>'Selasa (3/3/2015), Mahkamah Partai Golkar menyatakan kepengurusan hasil Munas Ancol Jakarta dinyatakan sah.'],
        ['date'=>'10 Maret 2015','content'=>'Menteri Hukum dan Ham Yasonna Laoly Yasonna secara resmi mengesahkan kepemimpinan Golkar versi Munas Ancol dengan Ketua Umum Agung Laksono.'],
        ['date'=>'23 Maret 2015','content'=>'Senin (23/3/2015) Siang, Yusril Ihza Mahendra selaku kuasa hukum partai Golkar mengajukan gugatan Surat Keputusan (SK) Menteri Hukum dan Ham (Menkumham) di Pengadilan Tata Usaha Negara Jakarta (PTUN).'],
        ['date'=>'25 Maret 2015','content'=>'Kubu Agung Laksono melayangkan surat peringatan pengosongan kantor ketua dan sekretaris Fraksi Golkar di komplek DPR RI.'],
        ['date'=>'27 Maret 2015','content'=>'Wakil Pimpinan Golkar dari kubu Agung Laksono, Yorrys Raweyai mendatangi kantor sekretariat Fraksi Golkar untuk mengambil alih kantor tersebut.'],
        ['date'=>'30 mei 2015','content'=>'Penandatanganan kesepakatan islah khusus dilakukan. Penandatanganan kesepakatan islah dilakukan oleh Ketua Umum DPP Partai Golkar hasil Munas Jakarta Agung Laksono, Ketua Umum DPP Partai Golkar hasil Munas Bali Aburizal Bakrie, dan Sekretaris Jenderal dari kedua kubu, Zainuddin Amali serta Idrus Marham di kediaman yusuf Kalla.'],
        ['date'=>'1 Juni 2015','content'=>'Pengadilan Negeri Jakarta Utara mengeluarkan putusan sela yang menguntungkan kepengurusan Golkar Aburizal Bakrie.']
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaikeadialndnpersatuanindonesia5119bd7483d2a'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef']
        ]
    ],
    'DUKUNGAN'=>[
        'narasi'=>'Dua kubu yang berseteru gencar menggalang dan mencari dukungan. Hal itu tidak hanya untuk memperkuat eksistensi kepengurusan partai, tetapi untuk memetakan kekuatan di Parlemen. Partai beringin di bawah kepengurusan Agung Laksono akan mengubah haluan untuk menjadi partai pendukung pemerintah. Kondisi ini akan berdampak pada berkurangnya kekuatan koalisi oposisi di DPR. Golkar saat ini menjadi pemegang kendali di Koalisi Merah Putih karena menjadi penyokong utama dari segi kuantitas suara di Parlemen.<br><br>Para Elit Pendukung Agung dari Golkar :',
        'satu'=>[
            ['page_id'=>'drshpriyobudisantoso51131c5b8da10'],
            ['page_id'=>'drsagusgumiwangkartasasmita50ee38bcdabca'],
            ['page_id'=>'yorrysraweyai52bbea81c3597'],
            ['page_id'=>'zainuddinamali5456ee91ad531'],
            ['page_id'=>'melchiasmarkusmekeng52a54c5052ca8'],
            ['page_id'=>'leonababan52675bf49f296'],
            ['page_id'=>'sariyuliati52a53db4ed03b']
        ],
        'narasi2'=>'Mereka menempati posisi-posisi strategis di dalam struktur kepengurusan Partai Golkar.<br><br>Selain dukungan dari elit internal Golkar, Agung Laksono bergerak cepat dengan melakukan safari politik ke elit-elit parpol lain. Setidaknya para ketua umum dan elit partai yang tergabung dalam Koalisi Indonesia Hebat seperti :',
        'dua'=>[
            ['page_id'=>'megawatisoekarnoputri50ee62bce591e'],
            ['page_id'=>'suryapaloh511b4aa507a50'],
            ['page_id'=>'drshmuhammadjusufkalla50ee870b99cc9'],
            ['page_id'=>'hwirantosh50bdcb9a73d2f'],
            ['page_id'=>'letjentnipurnsutiyoso511c2b6deeb46']
        ],
        'narasi3'=>'Mereka adalah barisan elit parpol yang sudah menyatakan komitmen untuk mendukung kepengurusan Agung Laksono sebagai ketua umum Golkar yang sah.<br><br>KMP Dukung ARB<br>Aburizal Bakrie merapatkan barisan pendukungnya yang ada di KMP. Semua elit partai di KMP berada di belakang Aburizal dan terus memberi dukungan terhadapnya. Mereka adalah:',
        'tiga'=>[
            ['page_id'=>'hmanismattalc50f7ba1b1520d'],
            ['page_id'=>'fahrihamzahse5105e57490d09'],
            ['page_id'=>'fadlizon5119d8091e007'],
            ['page_id'=>'prabowosubiyantodjojohadikusumo50c1598f86d91']
        ],
        'narasi4'=>'Sedangkan dari dalam Golkar sendiri Ical didukung oleh :',
        'empat'=>[
            ['page_id'=>'akbartanjung503c2d29aaf6b'],
            ['page_id'=>'sitihediatihariyadi51b939bf7e379'],
            ['page_id'=>'Ahmadi'],
            ['page_id'=>'drfadelmuhammad51132604d3a90'],
            ['page_id'=>'adekomarudin5280ac60140b0'],
            ['page_id'=>'nurdinhalid530eb1689eb22'],
            ['page_id'=>'drssetyanovanto50f8fd3c666bc'],
            ['page_id'=>'drazizsyamsuddin50f8e5a2bef6b'],
            ['page_id'=>'drstheolsambuaga51886b9786e52'],
            ['page_id'=>'idrusmarham51132ad8423e1'],
            ['page_id'=>'sharifcicipsutarjo50ef9a0c31d50']
        ]
    ],
    'KRITIK'=>'<img src="http://dennyja-world.com/wp-content/uploads/2015/03/yasonna.jpg" class="pic2">Sejak terbitnya SK yang mengsahkan kubu Agung Laksono. Menteri Yasona mendapat kritikan yang tajam dari kubu Aburizal Bakrie. Yasono dianggap memihak dalam mengambil kebijakan yang tidak sesuai prosedur hukum yang berlaku. Seharusnya Surat Keputusan dikeluarkan oleh Kemenkumham setelah ada putusan dari PTUN.
                </p><p class="font_kecil">Selain itu,keputusannya tidak didasarkan pada penilaian yang objektif, namun sarat politis. Keputusan tersebut merupakan keputusan yang ke dua kalinya dikeluarkan oleh Menteri Yasona sejak diangkat jadi menteri. Sebelumnya keputusan serupa dikeluarkan dalam menangani kasus sengketa di Partai Persatuan Pembangunan (PPP).
                </p><p class="font_kecil"><img src="http://www.jurnal3.com/wp-content/uploads/2015/03/laoly-golkar-yusril.jpg" class="pic">Kemenkumham berdalih bahwa putusan yang dikeluarkannya sudah tepat, karena mengacu pada keputusan Mahkamah Partai Golkar yang mengabulkan untuk menerima kepengurusan DPP Partai Golkar hasil Munas Ancol, yakni berdasarkan ketentuan Pasal 32 ayat 5 UU Parpol Nomor 2/201, dinyatakan bahwa putusan MP (Mahkamah Partai Golkar) bersifat final dan mengikat secara internal.
                </p><p class="font_kecil">Kubu Aburizal melawan keputusan pemerintah ini. Mereka mengugat putusan Menkumham ke pengadilan. Mereka juga melaporkan kubu Agung ke Bareskrim Polri dengan tuduhan pemalsuan dokumen. Selain itu, Koalisi Merah Putih juga akan mengajukan hak angket di DPR. Menteri Yasonna dianggap bekerja atas dasarpolitik.',
    'USULAN'=>'<img src="http://www.sayangi.com/media/k2/items/cache/20a212aca23d09956e75057d32a25b01_XL.jpg" class="pic2">Koalisi Merah Putih mengusulkan hak angket DPR untuk meminta penjelasan Menteri Hukum dan HAM yang dinilai tidak independen dalam menyikapi perselisihan di internal Partai Golkar dan Partai Persatuan Pembangunan.
                </p><p class="font_kecil">Usulan tersebut merupakan salah satu upaya perlawanan yang dilakukan oleh kubu Aburizal melalui koalisi pendukungnya di DPR yang tergabung dalam Koalisi Merah Putih (KMP). Setidaknya sudah ada 116 anggota yang menandatangi usulan hak angket.
                </p><p class="font_kecil">Artinya hak angket yang digulirkan ke Menkumham  itu sudah memenuhi  syarat untuk dibawa ke paripurna, karena sudah ditandatangani 116  anggota DPR.',
    'ANALISA'=>'<img src="http://cdn1-a.production.liputan6.static6.com/medias/798664/big/045609700_1421832684-raker_1.jpg" class="pic2">Indikasi adanya intervensi politik yang dilakukan pemerintah terhadap polemik di internal Partai Golkar  menguat setelah Kemenkumham memutuskan sah kepengurusan Agung Laksono. Polemik tersebut merupakan babak tambahan dari kisruh Partai Golkar yang sudah berlangsung lama. Institusi Kementerian Hukum dan Hak Asasi Manusia (Kemenkumham) diduga dijadikan alat oleh oknum-oknum pemerintah dalam mengintervensi internal Golkar untuk memecah dan memperlemah kekuatan koalisi merah putih di parlemen. Seperti yang diketahui Partai Golkar adalah Oposisi di pemerintahan Jokowi-JK, yang tergabung dalam Koalisi Merah Putih (KMP). 
                </p><p>Surat Keputusan (SK) yang dikeluarkan oleh Kementerian Hukum dan Ham, yang mengesahkan kepengurusan Partai Golkar hasil Munas Jakarta merupakan hasil kesepakatan politik yang dilakukan oleh Agung Laksono, Jusuf Kalla, Surya Palloh, dan Megawati. SK tersebut adalah pesanan Megawati ke Menteri Yasona sesaat setelah kontrak politik disepakati, yakni Partai Golkar versi Munas Jakarta bersedia bergabung ke dalam Koalisi Indonesia Hebat.',
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Ari Junaedi','jabatan'=>'Pengamat politik Universitas Indonesia','img'=>'http://sp.beritasatu.com/media/images/original/20141208171806988.jpg','url'=>'','content'=>'"Jika punya cita-cita membesarkan partai maka rekonsiliasi yang dilakukan. Saya rasa ini kepentingan eli-elit yang tidak terakomodir dengan keputusan mahkamah partai."'],
        ['from'=>'Surya paloh','jabatan'=>'','img'=>'http://img2.bisnis.com/makasar/posts/2014/09/26/180862/Surya-Paloh.jpg','url'=>'','content'=>'"Pemimpin Golkar Tak Boleh Tidak Siap Kalah Ketika Bertanding."'],
        ['from'=>'Aziz Syamsuddin','jabatan'=>'','img'=>'http://teropongsenayan.com/foto_berita/201501/05/medium_27Aziz%20Syamsudin%20002.jpg','url'=>'','content'=>'"Sangat suprise ya, tanpa ada pertimbangan Dirjen, Menkumham mengelurkan surat itu."'],
        ['from'=>'Jusuf Kalla','jabatan'=>'','img'=>'http://assets.kompasiana.com/statics/files/1402277267842193257.jpg','url'=>'','content'=>'"Kita harus mentaati keputusan mahkamah partai yang kemudian disahkan oleh Menkumham."'],
        ['from'=>'Agus Gumiwang','jabatan'=>'','img'=>'http://cdn.partainasdemo250.org/uploads/images/1408447172_AGUS_GUMIWANG.jpg','url'=>'','content'=>'"Sudah jelas dan sudah tidak terbantahkan lagi bahwa DPP Partai Golkar yang sah itu adalah DPP pimpinan Pak Agung Laksono karena SK Kemenkumham itu sah."']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Abubakar Al-Habsyi','jabatan'=>'Ketua DPP Partai Keadilan Sejahtera','img'=>'http://www.wartabuana.com/pictures/Abubakar-Alhabsyi-201412181726271.jpg','url'=>'','content'=>'"Tugas pemerintah itu seharusnya hanya sebagai administrator, kalau yang sekarang sudah sampai ke intervensi."'],
        ['from'=>'Idrus Marham','jabatan'=>'Sekjen Golkar versi Munas Bali','img'=>'http://wartatujuh.com/wp-content/uploads/2015/03/drus-620x330.jpg','url'=>'','content'=>'"Negara ini hancur ketika kebijakan itu diambil berdasarkan penafsiran-penafsiran salah. Nah, itulah Menteri hukum ham kita sekarang ini"'],
        ['from'=>'Akbar Tandjung','jabatan'=>'','img'=>'http://www.solopos.com/dokumen/2010/06/akbar-tandjung-2.jpg','url'=>'','content'=>'"Keputusan Menkumham mengarah untuk memenangkan Agung"'],
        ['from'=>'Prabowo subianto','jabatan'=>'','img'=>'http://www.islamtoleran.com/wp-content/uploads/2014/07/prabowo-subianto-foto-f.jpg','url'=>'','content'=>'"Bagi saya dan bagi Gerindra yang saya pimpin, kami hanya mengakui bapak Aburizal Bakrie."'],
        ['from'=>'Fadli Zon','jabatan'=>'','img'=>'http://www.nonstop-online.com/wp-content/uploads/2015/03/fadli.jpg','url'=>'','content'=>'"Keputusan Menkumham ini sensitif, hak demokrasi parpol dipangkas dengan mudahnya melakukan intervensi politik, sehingga membuat dampak yang luas pada masyarakat. Golkar hanya salah satu korban saja, ini bisa terjadi pada yang lain, dan kalau dilakukan bisa jadi pengacau demokrasi."'],
        ['from'=>'Muntasir Hamid','jabatan'=>'Ketua Forum Silaturahmi DPD II Partai Golkar','img'=>'http://cdn-media.viva.co.id/thumbs2/2012/04/30/153002_muntasir-hamid_663_382.jpg','url'=>'','content'=>'"Dengan surat seperti ini bisa dicabut oleh Presiden. Karena diduga ada oknum di lingkaran Presiden Jokowi yang bermain di tengah kekisruhan ini."']
    ],
    'VIDEO'=>[
        ['id'=>'gycWAkep7j0'],
        ['id'=>'lp0uRAtlzo0'],
        ['id'=>'UXXvjPee7gk'],
        ['id'=>'blQg7nvA8GY'],
        ['id'=>'zl5ewqmv4P8'],
        ['id'=>'01FtCzymc5M']
    ],
    'FOTO'=>[
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img1.jpg'],
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img2.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/03/22/414462/dukungan-terhadap-ahok.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/02/27/id_375043/375043_620_tempoco.jpg'],
        ['img'=>'http://images.detik.com/customthumb/2015/03/23/10/171112_dprdkomahook4.jpg?w=460'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/03/01/31/970362/ahok-vs-dprd-dan-politik-adu-kuat-WkD.jpg'],
        ['img'=>'http://cdn-media.viva.co.id/thumbs2/2015/03/05/299841_mediasi-ahok-dprd-berujung-ricuh_663_382.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/03/03/id_376061/376061_620_tempoco.jpg'],
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/kisruhgolkar/top.jpg")?>') no-repeat transparent;
        height: 810px;
        margin-bottom: -130px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .boxprofile p, .boxprofile li, .font_kecil {
        font-size: 12px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        background: rgba(0, 0, 0, 0.7);
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
    }
    .block_red {
        background-color: orange;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 100%;
        height: auto;
        margin-bottom: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .garis {
        border-top: 1px dashed black;
    }
    .boxgray {
        width: 96%;
        border: 9px solid lightgray;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: 130px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: auto;text-align: center}
    li.organisasi2 img {width: 98px; height: 100px;  padding: 0px !important;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 293px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.gallery img {width: 215px;height: 120px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.dukung {float: left; padding:5px;vertical-align: top;text-align: center}
    li.dukung img {width: 75px; height: 75px;  padding: 0px !important;border-radius: 10px;box-shadow: 0 0 5px gray;}
    li.dukung p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }   
    .uprow {margin-left: 20px;margin-top:10px;border-color: #720502 transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {
        width: 260px;
        height: auto;
        margin-left: 20px;
        margin-top:0px;
        margin-bottom: 10px;
        background-color: #424040;
        background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px orange;
        border-right:solid 2px orange;
        border-bottom:solid 2px orange;
        border-radius: 0 0 5px 5px;
        color: #ffffff;
        z-index: 100;
    }
    .kronologi-title {font-size:14px;font-weight:bold;background-color: orange;color: #ffffff;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {line-height: 15px;font-size:12px;}

    .ketua {
        background-color: yellow;
        width: 104px;
        display: block;
        float: left;
        margin-right: 3%;
        box-shadow: -3px 3px 10px gray;
        border-radius: 8px;
        margin-bottom: 15px;
    }
    .ketua img {
        width: 100%;
        height: 125px;
    }
    .kritik {
        font-size: 18px;
        font-weight: bold;
        color: black;
    }
    .isi-col50 {
        float: left;
        margin: 10px 16px;
        width: 41%;
        height: auto;
        display: inline-block;
    }

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <h3><a id="kisruhgolkar" style="color: black;"><?php echo $data['NARASI']['title'];?></a></h3>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">SALING SIDANG</a></h4>
                <p><?php echo $data['SALING'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">PETA KEKUATAN</a></h4>
                <p><?php echo $data['DUKUNGAN']['narasi'];?></p>
                    <ul style="margin: 0px 0px 0px -2px;">
                        <?php
                        foreach($data['DUKUNGAN']['satu'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'zainuddinamali5456ee91ad531'){
                                $photo = 'http://cdn-2.tstatic.net/surabaya/foto/bank/images/zainuddin-amali-target-golkar-20-kursi-surabaya.jpg';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="dukung">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                <div class="clear"></div>
                <p><?php echo $data['DUKUNGAN']['narasi2'];?></p>
                    <ul style="margin: 0px 0px 0px -2px;">
                        <?php
                        foreach($data['DUKUNGAN']['dua'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : ''; ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="dukung">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                <div class="clear"></div>
                <p><?php echo $data['DUKUNGAN']['narasi3'];?></p>
                    <ul style="margin: 0px 0px 0px -2px;">
                        <?php
                        foreach($data['DUKUNGAN']['tiga'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : ''; ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="dukung">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                <div class="clear"></div>
                <p><?php echo $data['DUKUNGAN']['narasi4'];?></p>
                    <ul style="margin: 0px 0px 0px -2px;">
                        <?php
                        foreach($data['DUKUNGAN']['empat'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'nurdinhalid530eb1689eb22'){
                                $photo = 'http://www.rmol.co/images/berita/thumb/thumb_261225_08030202112014_ade.JPG';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="dukung">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">SIASAT ICAL</a></h4>
                <p><?php echo $data['SIASAT'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">MENJEGAL ICAL</a></h4>
                <p><?php echo $data['MENJEGAL'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="ketuaumum" style="color: black;">KETUA UMUM GOLKAR DARI MASA KE MASA</a></h4>
                <?php
                foreach ($data['KETUA'] as $key => $val) { ?>
                <div class="ketua">
                    <img src="<?php echo $val['img']; ?>">
                    <p class="text-center" style="font-size: 11px;line-height: 15px;margin-top: 5px;margin-bottom: 5px;"><?php echo $val['name']; ?><br><?php echo $val['thn']; ?></p>
                </div>
                <?php
                }
                ?>
                <div class="clear"></div>

                <h4 class="list"><a id="pendukung" style="color: black;">PARTAI POLITIK PENDUKUNG AGUNG LAKSONO</a></h4>
                <div class="boxgray_green" style="height: auto;padding-bottom: 10px;">
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div>

                <h4 class="list"><a id="penentang" style="color: black;">PARTAI POLITIK PENDUKUNG ABURIZAL BAKRIE</a></h4>
                <div class="boxgray_red" style="height: auto;padding-bottom: 10px;">
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div><br>
            </div>

            <div class="col_kanan">
                <div class="boxprofile white">
                    <h4 class="block_red text-center"><a id="sejarahsingkat" style="color: white;">SEJARAH SINGKAT</a></h4>
                    <p style="margin-left: 20px;margin-right: 20px;margin-top: 10px;">
                        <img src="http://totabuanews.com/wp-content/uploads/2014/05/logo-golkar-tepat.jpg" class="picprofil">
                    </p>
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['PROFIL']['sejarah'] as $key => $val) {
                            echo "<li style='margin-left: 20px;margin-right: 20px;'>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div><br>

                <h4 class="list" style="margin-left: 20px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="newsgolkar_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newsgolkar_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div><br>

                <h4 class="list" style="margin-left: 20px;"><a id="kronologi" style="color: black;">KRONOLOGI</a></h4>
                <?php 
                foreach ($data['KRONOLOGI'] as $key => $value) {
                ?>
                <div class="kronologi">
                    <div class="kronologi-title"><?php echo $value['date'];?></div>
                    <div class="kronologi-info">
                        <p><?php echo $value['content'];?></p>
                    </div>
                </div>
                <div class="clear"></div>
                <?php
                }
                ?>
            </div>
            <div class="clear"></div>

            <div class="col_kiri50">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">KRITIK TERHADAP PUTUSAN MENTERI</a></h4>
                <p class="font_kecil"><?php echo $data['KRITIK'];?></p>
                <div class="clear"></div>
            </div>

            <div class="col_kanan50" style="padding-top: 4px;">
                <h4 class="list"><a id="analisa" style="color: black;">USULAN HAK ANGKET</a></h4>
                <p class="font_kecil"><?php echo $data['USULAN'];?></p>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>

            <div class="col_kiri50">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
                <div class="boxgray" style="height: 550px;padding-bottom: 10px;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) {
                            ?>
                            <div class="isi-col50">
                                <a href="<?php echo $val['url'];?>">
                                    <img src="<?php echo $val['img'];?>" style="width: auto; height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;text-align: left;"><?php echo $val['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;text-align: left;"><?php echo $val['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: 0px;"><?php echo $val['content']; ?></p>
                            </div>
                        <?php } ?>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="col_kanan50" style="padding-top: 4px;">
                <h4 class="list"><a id="quotepenentang" style="color: black;">QUOTE PENENTANG</a></h4>
                <div class="boxgray" style="height: auto;padding-bottom: 10px;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <div>
                            <?php
                            foreach($data['QUOTE_PENENTANG'] as $key=>$val) {
                                ?>
                                <div class="isi-col50">
                                    <a href="<?php echo $val['url'];?>">
                                        <img src="<?php echo $val['img'];?>" style="width: auto; height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;text-align: left;"><?php echo $val['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;text-align: left;"><?php echo $val['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;"><?php echo $val['content']; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="height: 485px;width: 98%;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <!-- jika ingin border atas bawah none gunakan mqdefault -->
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait Kisruh GOLKAR</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>