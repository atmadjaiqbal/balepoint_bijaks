<?php 
$data = [
    'NARASI'=>[
        'title'=>'MEMBONGKAR PEMBEGAL DANA SILUMAN',
        'narasi'=>'<img src="http://www.fastnewsindonesia.com/sites/default/files/styles/article_image/public/field/image/278585_basuki-tjahaja-purnama.jpeg?itok=XyAN8uMJ" class="pic">Keterangan Gubernur DKI Jakarta, Basuki Tjaja Purnama (Ahok) terkait adanya penggelembungan dana ilegal  RAPBD DKI yang berlangsung sejak tahun 2012 hingga 2015 menuai polemik yang semakin meluas. Adanyana siluman yang dilontarkan Gubernur Ahok spontan mendapat reaksi dari DPRD DKI Jakarta. Mereka mempermasalahkan pernyataan mantan politisi partai Gerindera tersebut. Kisruh berawal ketika Ahok dan DPRD bersitegang lantaran ditemukannya  dana siluman dalam rancangan APBD 2015 yang diselundupkan oleh Dewan. Ahok menyebutkan nilai dana siluman tersebut sebesar Rp 12,1 triliun. Selanjutnya, Anggaran pendapatan belanja daerah (APBD) DKI 2015 tidak kunjung cair.
                </p><p><img src="http://cdn0-a.production.liputan6.static6.com/medias/793755/big/072011800_1421056562-Ahok_Sidang_Paripurna3.jpg" class="pic2">Ketegangan semakin memanas ketika terjadi ketidaksepakatan antara pihak Pemprov DKI dan DPRD DKI dalam penyerahan dokumen Rancangan Anggaran Pendapatan Belanja Daerah (RAPBD). Masing-masing pihak mengklaim memiliki dokumen sah dan sesuai aturan yang berlaku. Sehingga Kementerian Dalam Negeri (Kemendagri) turun tangan untuk menyelesaikan persoalan tersebut melalui jalan mediasi, namun hasilnya nihil dan berakhir ricuh.'
    ],
    'PROFIL'=>[
        'nama'=>'Ir. Basuki Tjahaja Purnama MM',
        'agama'=>'Kristen',
        'tmp'=>'Manggar, Bangka Belitung',
        'tgl'=>'Rabu, 29 Juni 1966',
        'jejak'=>[
            ['no'=>'Anggota Komisi II DPR RI, 2009 - 2014.'],
            ['no'=>'Direktur Eksekutif Center for Democracy and Transparency (CDT.3.1).'],
            ['no'=>'Bupati Belitung Timur, 2005 - 2006.'],
            ['no'=>'Anggota DPRD Belitung Timur bidang Komisi Anggaran, 2005 - 2006.'],
            ['no'=>'Asisten Presiden Direktur bidang analisa biaya dan keuangan PT. Simaxindo Primadaya, Jakarta, 1994 - 1995.'],
            ['no'=>'Direktur PT. Nurindra Ekapersada, Belitung Timur, 1992 - 2005.'],
            ['no'=>'Wakil Gubernur DKI Jakarta (2012).'],
            ['no'=>'Gubernur DKI Jakarta (2014).']
        ]
    ],
    'PROKONTRA'=>[
        'narasi'=>'Polemik berkepanjang di pemerintahan DKI Jakarta secara otomatis menuai pro dan kontra di masyarakat khususnya di Jakarta. Dukungan ataupun bentuk simpatik mengalir dari beragam elemen masyarakat. Ada yang mendukung langkah yang ditempuh oleh Gubernur merupakan langkah yang sudah tepat. Namun adapula yang memberikan dukungan terhadap DPRD.',
        'dukungan'=>'Berbagai dukungan mengalir untuk Gubernur Ahok, dukungan ini bukan tanpa alasan. Dukungan tersebut  muncul dari masyarakat umum, LSM, dan kalangan akademisi. Mereka menilai Gubernur Ahok harus didukung sebagai bentuk komitmen bersama dalam memberantas dan  membersihkan pemerintah dari praktek-praktek korupsi yang merugikan masyarakat'
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'nasdem5119b72a0ea62'],
        ],
        'institusi'=>[
            ['name'=>'Menteri Pemberdayaan Aparatur Negara dan Reformasi Birokrasi','img'=>'http://www.dutakarya-pu.com/wp-content/uploads/2015/02/LKAN.png','url'=>''],
            ['name'=>'Dukungan terhadap Gubernur DKI Jakarta Basuki Tjahaja Purnama atau Ahok terkait kisruh APBD 2015 semakin meningkat. Dalam petisi online www. change.org,   lebih    dari 50 ribu orang mendukung penerapan e-budgeting yang dilakukan Ahok dan mendesak rakyat Jakarta mencabut mandat DPRD.','img'=>'http://blogs-images.forbes.com/erikkain/files/2014/12/Change.org_.gif','url'=>''],
            ['name'=>'Selain dukungan lewat dunia maya, para pendukung Ahok juga akan turun ke jalan besok pagi di tengah Car Free Day di sekitar Bundaran HI. Ratusan orang yang mengatasnamakan "teman Ahok" siap menggalang dukungan masyarakat','img'=>'https://pbs.twimg.com/profile_images/574022055969992704/Y1ULIbCB.jpeg','url'=>'']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']
        ],
        'institusi'=>[
            ['name'=>'DPRD','img'=>'http://pks-jakarta.or.id/wp-content/uploads/2014/12/logo-dprd.jpg','url'=>''],
            ['name'=>'Demo Korupsi','img'=>'http://bimg.antaranews.com/bali/2010/08/ori/demokorupsi020810.jpg','url'=>'']
        ]
    ],
    'PERKIRAAN'=>[
        'kontra'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']
        ],
        'pro'=>[
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'nasdem5119b72a0ea62']            
        ],
        'belum'=>[
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']           
        ],
    ],
    'ANALISA'=>'<img src="http://www.hariandepok.com/wp-content/uploads/2015/03/Serangan-Balik-Ahok-Kepada-DPRD-KI-Jakarta-637x353.jpg" class="pic">Pasca pelimpahkan kasus dana siluman ke ranah hukum, pihak DPRD DKI berbalik menyerang sebagai upaya antisipasi untuk mempertahankan diri, Gunernur Ahok dituduh menyalahi aturan terkait dengan pengajuan APBD 2015 tanpa sepengetahuan DPRD. Selain menabrak aturan, Ahok kerap melanggar etika sebagai pejabat publik. Dewan menganggap omongan Ahok tak pantas diucapkan sebagai pejabat publik. Usulan penggunaan Hak Angket merupakan opsi yang ditempuh  oleh DPRD dalam memperkarakan Gubernur Ahok. 
                </p><p>Mayoritas anggota DPR menandatangi pengajuan hak angket kepada Gubernur Ahok perihal penyampaian RAPERDA APBD DKI 2015 ke Kementerian Dalam Negeri (Kemendagri) sesaat setelah panitia hak angket dibentuk dan disahkan oleh DPRD. Berdasarkan Peraturan Pemerintah Nomor 16 Tahun 2010 tentang Pedoman Penyusunan Tata Tertib DPRD dan mengacu pada Tata Tertib DPRD, hak angket bisa digulirkan atas usulan minimal dua fraksi atau 15 anggota Dewan. Dewan berhak memanggil pejabat daerah yang terkait dengan subyek penyelidikan. Dalam kasus APBD DKI, DPRD berhak memanggil Ahok serta tim anggaran pemerintah daerah.
                </p><p><img src="http://img.bisnis.com/posts/2015/02/14/402690/ahok-sofyan.jpg" class="pic2">Namun pengajuan hak angket dianggap terlalu berlebihan karena sangkaan pelanggaran yang dialamatkan pada Ahok bukanlah pelanggaran yang berpotensi besar merugikan pemerintahan. Selain itu, hal tersebut masih sangat premature karena mengesampingkan prosedur yang semestinya dijalankan yakni pengajuan hak interplasi (hak untuk bertanya)  terlebih dahulu sebelum prosedur hak untuk menyelidiki (hak angket) diajukan.
                </p><p><img src="http://cdn-media.viva.co.id/thumbs2/2015/03/05/299841_mediasi-ahok-dprd-berujung-ricuh_663_382.jpg" class="pic">Dalih Anggota Dewan merupakan bentuk ketakutan semata terhadap ancaman Gubernur Ahok dalam  membongkar mafia dibalik dana siluman tersebut. Hal ini mengindikasikan adanya upaya penggulingan atau pemakzulan terhadap Gubernur Ahok.  Penggulingan bertujuan untuk “mendiamkan” dan  menyerang langsung Gubernur Ahok tanpa menanyakan terlebih dahulu perihal sengketa yang sedang terjadi. 
                </p><p>Meski berada dalam tekanan dan intervensi saling silang menyerang, Gubernur Ahok tetap pada pendiriannya untuk membawa sengketa tersebut ke ranah hukum.',
    'STRATEGI'=>[
        ['img'=>'http://statik.tempo.co/data/2015/03/05/id_376741/376741_620_tempoco.jpg','no'=>'DPRD DKI  tengah menyiapkan senjata untuk serangan baru yang ditujukan untuk menggoyang bahkan juga bisa melengserkan Gubernur DKI Jakarta, Basuki Tjahaja Purnama alias Ahok dari jabatannya.'],
        ['img'=>'http://pribuminews.com/wp-content/uploads/2015/02/DPRDAngket2Chris.jpg','no'=>'Dari sembilan fraksi di DPRD DKI Jakarta, Fraksi Partai Kebangkitan Bangsa dan NasDem telah menarik diri dari hak angket. Artinya peluang DPRD cukup besar untuk menempuh upaya hak angket karena masih ada tujuh Fraksi yang mendukung upaya tersebut.'],
        ['img'=>'https://img.okezone.com/content/2015/02/27/338/1111690/dprd-tak-ciut-dengan-laporan-ahok-ke-kpk-9oi40qToiN.jpg','no'=>'Selanjutnya, mayoritas anggota dewan bersepakat menentang keterangan Ahok terkait dana siluman 12 Triliun lebih  yang ada dalam APBD DKI. Setidaknya sudah ada 95 dari 106 jumlah total anggota Dewan yang menandatangi usulan hak angket tersebut.'],
        ['img'=>'http://statik.tempo.co/?width=450&id=374728','no'=>'Wacana hak angket yang digulirkan merupakan siasat politik dalam menjatuhkan Gubernur Ahok. Lewat hak angket, DPRD akan mencari kesalahan Gubernur Ahok. Setelah menyelidik dan menemukan pelanggaran berat terhadap  UU, maka Ahok akan dilengserkan.'],
        ['img'=>'https://img.okezone.com/content/2015/03/16/338/1119019/drama-ahok-vs-dprd-dki-harus-berakhir-WPkyohKryC.jpg','no'=>'Jika Ahok lengser, PDIP memiliki hak untuk mengajukan calon gubernur sebagai partai pemenang pemilu di DKI.']
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Patrice Rio CapellaPatrice','jabatan'=>'Sekretaris Jenderal (Sekjen) Partai NasDem','img'=>'http://assets.kompas.com/data/photo/2013/08/22/13245220000-a-aario-capella780x390.jpg','url'=>'','content'=>'"Kami mengintruksikan seluruh anggota Fraksi NasDem untuk mencabut hak angket, dan meminta aparat penegak hukum melakukan tindakan"'],
        ['from'=>'Bowo','jabatan'=>'Koordinator Aksi @temanAhok','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Konsepnya yaitu mendesak penegakan hukum untuk pembegal APBD"'],
        ['from'=>'Aliansi Masyarakat Resah Dewan Perwakilan Rakyat (AMAR DPR)','jabatan'=>'','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Selamatkan APBD 12 triliun itu kan pajak dari rakyat, justru dana itu yang bisa membangun kota dan masyarakat Jakarta, ulah parpol terkait angket itu telah menghambat proses pembangunan kota Jakarta yang jelas merugikan masyarakat sebagai pembayar pajak"'],
        ['from'=>'Sebastian Salang','jabatan'=>'Koordinator PORMAPPI','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Upaya yang dilakukan oleh Ahok adalah upaya untuk menjaga agar uang negara tidak ada yang dikorup, dan di sisi yang lain menyelamatkan agar Anggota DPRD tidak ada yang masuk penjara"']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Selamat Nurdin','jabatan'=>'Ketua Fraksi Partai Keadilan Sejahtera','img'=>'http://4.bp.blogspot.com/-6o6VaLYla1Q/VPeZ2ev3r_I/AAAAAAAA9Vc/AJfh0vXyONo/s1600/Selamat%2BNurdin_PKS.jpg','url'=>'','content'=>'"Kami pengin Ahok mengakui telah melakukan hal keliru . Kami ngomong soal administrasi negara, dan dia melanggarnya. Apa yang dikirim tidak ada satu pun dari Dewan"'],
        ['from'=>'Abraham Lunggana','jabatan'=>'Wakil Ketua DPRD DKI','img'=>'http://wartatujuh.com/wp-content/uploads/2015/03/lunggana-620x330.jpg','url'=>'','content'=>'"Ahok yang berusaha men"deadlock"an kisruh APBD 2015 ini"']
    ],
    'VIDEO'=>[
        ['id'=>'QFy2IEcpcGI'],
        ['id'=>'Welz79lQoZE'],
        ['id'=>'RIbRoaY0jsM'],
        ['id'=>'zd4ngJdnUPU'],
        ['id'=>'6CKMPUltJtQ'],
        ['id'=>'Qm0fl1WMr-U']
    ],
    'FOTO'=>[
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img1.jpg'],
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img2.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/03/22/414462/dukungan-terhadap-ahok.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/02/27/id_375043/375043_620_tempoco.jpg'],
        ['img'=>'http://images.detik.com/customthumb/2015/03/23/10/171112_dprdkomahook4.jpg?w=460'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/03/01/31/970362/ahok-vs-dprd-dan-politik-adu-kuat-WkD.jpg'],
        ['img'=>'http://cdn-media.viva.co.id/thumbs2/2015/03/05/299841_mediasi-ahok-dprd-berujung-ricuh_663_382.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/03/03/id_376061/376061_620_tempoco.jpg'],
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/ahokvsdprd/top.jpg")?>') no-repeat transparent;
        height: 810px;
        margin-bottom: -160px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .boxprofile p, .boxprofile li, .font_kecil {
        font-size: 12px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        background: rgba(0, 0, 0, 0.7);
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
    }
    .block_red {
        background-color: #720502;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 100%;
        height: auto;
        margin-bottom: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .garis {
        border-top: 1px dashed black;
    }
    .boxgray {
        width: 96%;
        border: 9px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: 130px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: auto;text-align: center}
    li.organisasi2 img {width: 98px; height: 100px;  padding: 0px !important;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 293px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.gallery img {width: 215px;height: 120px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <h3><a id="ahokvsdprd" style="color: black;"><?php echo $data['NARASI']['title'];?></a></h3>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div><br>

                <h4 class="list"><a id="analisa" style="color: black;">ANALISA</a></h4>
                <p><?php echo $data['ANALISA'];?></p>
                <div class="garis"></div><br>

                <h4 class="list"><a id="kasushukum" style="color: black;">STRATEGI MENGGULINGKAN AHOK</a></h4>
                <ul class=''>
                    <?php
                    foreach ($data['STRATEGI'] as $key => $val) { ?>
                        <li class="font_kecil" style='margin-left: 5px;text-align: justify;'><img src="<?php echo $val['img'];?>" class='pic3'><?php echo $val['no'];?></li><br>
                    <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col_kanan">
                <div class="boxprofile white">
                    <h4 class="block_red text-center"><a id="profileahok" style="color: white;">PROFILE</a></h4>
                    <p style="margin-left: 20px;margin-right: 20px;margin-top: 10px;">
                        <img src="http://indonesiana.tempo.co/uploads/foto/2015/03/20/pitung.jpg" class="picprofil">
                        <span style="font-size: 17px;text-align: center;"><?php echo $data['PROFIL']['nama'];?></span>
                    </p>
                    <table style="margin-left: 20px;margin-right: 20px;margin-top: 10px;">
                        <tr>
                            <td width="85px">Agama</td>
                            <td width="15px">:</td>
                            <td><?php echo $data['PROFIL']['agama'];?></td>
                        </tr>
                        <tr>
                            <td>Tempat Lahir</td>
                            <td>:</td>
                            <td><?php echo $data['PROFIL']['tmp'];?></td>
                        </tr>
                        <tr>
                            <td>Tgl Lahir</td>
                            <td>:</td>
                            <td><?php echo $data['PROFIL']['tgl'];?></td>
                        </tr>
                    </table>
                    <br>
                    <p style="margin-left: 20px;">JEJAK KARIR :</p>
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['PROFIL']['jejak'] as $key => $val) {
                            echo "<li style='margin-left: 20px;margin-right: 20px;'>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div><br>

                <h4 class="list" style="margin-left: 20px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="newsahok_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newsahok_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div><br>

                <h4 class="list" style="margin-left: 15px;"><a id="mencaplokbandarahalim" style="color: black;">PRO KONTRA</a></h4>
                <div style="margin-left: 20px;">
                    <p style="font-size: 12px;"><?php echo $data['PROKONTRA']['narasi'];?></p>
                    <h5>Dukungan Terhadap Gubernur Ahok</h5>
                    <p style="font-size: 12px;"><?php echo $data['PROKONTRA']['dukungan'];?></p>
                </div>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="display: inline-block;height: auto;width: 98%;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) {
                            ?>
                            <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;">
                                <a href="<?php echo $val['url'];?>">
                                    <img src="<?php echo $val['img'];?>" style="width: 55px; height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;text-align: left;"><?php echo $val['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;text-align: left;"><?php echo $val['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: 0px;"><?php echo $val['content']; ?></p>
                            </div>
                        <?php } ?>
                    </ul>
                </div>
            </div>

            <div class="clear"></div>
            <h4 class="list"><a id="quotepenentang" style="color: black;">QUOTE PENENTANG</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="display: inline-block;height: auto;width: 98%;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <div>
                            <?php
                            foreach($data['QUOTE_PENENTANG'] as $key=>$val) {
                                ?>
                                <div style="float: left;margin: 10px 16px;width: 46%;height: auto;display: inline-block;">
                                    <a href="<?php echo $val['url'];?>">
                                        <img src="<?php echo $val['img'];?>" style="width: auto; height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;text-align: left;"><?php echo $val['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;text-align: left;"><?php echo $val['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;"><?php echo $val['content']; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            </div>

            <h4 class="list"><a id="perkiraan" style="color: black;">PERKIRAAN KEKUATAN DUKUNGAN DI PARLEMEN</a></h4>
            <div class='row' style="margin: 0px;">
                <div class="span4" style="height: auto;margin: 0px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;text-align: center;" class="text-center">KONTRA AHOK</p>
                    <ul>
                        <?php
                        foreach($data['PERKIRAAN']['kontra'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi2">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;text-align: center;" class="text-center">Jumlah Suara 64</p>
                </div>
                <div class="span4" style="height: auto;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;text-align: center;" class="text-center">PRO AHOK</p>
                    <ul>
                        <?php
                        foreach($data['PERKIRAAN']['pro'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi2">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;text-align: center;" class="text-center">Jumlah Suara 23</p>
                </div>

                <div class="span4" style="height: auto;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;text-align: center;" class="text-center">BELUM JELAS</p>
                    <ul>
                        <?php
                        foreach($data['PERKIRAAN']['belum'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi2">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;text-align: center;" class="text-center">Jumlah Suara 19</p>
                </div>
            </div>
            <div class="clear"></div><br>

            <div class="col_kiri50">
                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <div class="boxgray_green" style="height: 560px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">INSTITUSI PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi" style="height: auto;width: 365px;">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/ style="float: left;margin-right: 10px;">
                                    <p style="width: auto;text-align: left;"><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col_kanan50">
                <h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <div class="boxgray_red" style="height: 560px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>

                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="">
                                <li class="organisasi">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="clear"></div><br>
            <h4 class="list"><a id="gallery" style="color: black;">Gallery Foto</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div style="width: 98%;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        $no=1;
                        foreach ($data['FOTO'] as $key => $val) { ?>
                            <li class="gallery">
                                <a href="#" data-toggle="modal" data-target="#img-<?php echo $no;?>">
                                    <img src="<?php echo $val['img'];?>" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="img-<?php echo $no;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Gallery Foto</h4>
                                        </div>
                                        <div class="modal-body">
                                            <img src="<?php echo $val['img'];?>" />
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php
                        $no++;
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="clear"></div><br>
            <h4 class="list"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="height: 485px;width: 98%;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <!-- jika ingin border atas bawah none gunakan mqdefault -->
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait Lion AIR</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>
<br/>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>