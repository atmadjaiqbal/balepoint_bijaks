<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
.organisasiLogo{
    width: 110px;
    height: 120px;
}

.smalltitle{
    font-size: .8em;
}
</style>

<?php 
$headline = "Penunjukkan calon Kapolri yang ternyata dijadikan tersangka oleh KPK dalam kasus  kepemilikan rekening gendut";
$data = [
    'calonDiusung'=>[['jabatan'=>'Kalemdikpol','page_id'=>'komjenpoldrsbudigunawan51de4d9f36e8e']],
    'pengusungCalon'=>[
        'aktorPengusung'=>[
            ['jabatan'=>'Presiden','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png'],
            ['jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
            ['jabatan'=>'Ketua Umum Nasdem','page_id'=>'suryapaloh511b4aa507a50','logo'=>'http://www.bijaks.net/public/upload/image/politisi/nasdem5119b72a0ea62/thumb/nasdem5119b72a0ea62_20130212_035134.jpg'],
            ['jabatan'=>'Sekjen Nasdem','page_id'=>'patriceriocapella511b5016a9c59','logo'=>'http://www.bijaks.net/public/upload/image/politisi/nasdem5119b72a0ea62/thumb/nasdem5119b72a0ea62_20130212_035134.jpg'],
            ['jabatan'=>'Ketua Umum PPP','page_id'=>'mromahurmuziystmt50fb7f740d44e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaipersatuanpembangunan5189ad769b227/badge/partaipersatuanpembangunan5189ad769b227_20130508_014534.png'],
            ['jabatan'=>'Ketua Umum PKPI','page_id'=>'letjentnipurnsutiyoso511c2b6deeb46','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaikeadialndnpersatuanindonesia5119bd7483d2a/badge/partaikeadialndnpersatuanindonesia5119bd7483d2a_20130212_040213.jpg'],
        ],
        'partaiPengusung'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaikeadialndnpersatuanindonesia5119bd7483d2a'],
        ]
    ],
    'pendukungPencalonan'=>[
        'institusiPendukung'=>[
            ['page_id'=>'komisiiii511780f45299c'],

        ],
        'aktorPendukung'=>[
            ['jabatan'=>'Presiden','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png'],
            ['jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
        ]
            
    ],
    'penentangPencalonan'=>[
        'aktorPenentang'=>[
            ['jabatan'=>'Ketua KPK Periode 2011-2015','page_id'=>'drabrahamsamadshmh5192f6e1b2ac4','logo'=>'http://www.bijaks.net/public/upload/image/politisi/komisipemberantasankorupsi5192fb408b219/badge/66c285e30ed48d4b4cb7101e3ee3ffe7980deb29.jpg'] 
        ],
        'institusiPenentang'=>[
            ['page_id'=>'komisipemberantasankorupsi5192fb408b219','logo'=>'http://www.bijaks.net/public/upload/image/politisi/komisipemberantasankorupsi5192fb408b219/badge/66c285e30ed48d4b4cb7101e3ee3ffe7980deb29.jpg'] 
        ],
        'partaiPenentang'=>[
            ['page_id'=>'partaidemokrat5119a5b44c7e4'] 
        ]

    ],
    'analisa'=>[],
    'kontradiksi'=>[],
    'beritaTerkait'=>[],
    'kelompokPro'=>[
        'politisiPro'=>[
            ['jabatan'=>'Presiden','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png'],
            ['jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
        ],
        'tokohPro'=>[
            ['jabatan'=>'Presiden','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png'],
            ['jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
        ],
        'pengamatPro'=>[
            ['jabatan'=>'Presiden','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png'],
            ['jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
        ]
    ],
    'kelompokKontra'=>[
        'politisiKontra'=>[
            ['jabatan'=>'Presiden','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png'],
            ['jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
        ],
        'tokohKontra'=>[
            ['jabatan'=>'Presiden','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png'],
            ['jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
        ],
        'pengamatKontra'=>[
            ['jabatan'=>'Presiden','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png'],
            ['jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
        ]
    ],
    'aksiPenolakan'=>[
        ['imgId'=>'uudemo_01','imgSrc'=>'assets/images/hotpages/uupilkada/1329269demo-RUU780x390.jpg'],
        ['imgId'=>'uudemo_02','imgSrc'=>'assets/images/hotpages/uupilkada/FOTO-RUU-PILKADA-_-Demonstran-Penolak-RUU-Pilkada-Bakar-Bakar.jpg'],
        ['imgId'=>'uudemo_03','imgSrc'=>'assets/images/hotpages/uupilkada/satu_harapan.jpg'],
        ['imgId'=>'uudemo_04','imgSrc'=>'assets/images/hotpages/uupilkada/massa_bakar_bakar.jpg'],
        ['imgId'=>'uudemo_05','imgSrc'=>'assets/images/hotpages/uupilkada/95C38360-80F8-4290-907F-BFCC5232113A_w640_s.jpg'],
        ['imgId'=>'uudemo_06','imgSrc'=>'assets/images/hotpages/uupilkada/2014914093536-pilkada.jpg'],
        ['imgId'=>'uudemo_07','imgSrc'=>'assets/images/hotpages/uupilkada/Komdi.jpg'],
        ['imgId'=>'uudemo_08','imgSrc'=>'assets/images/hotpages/uupilkada/1410682755.jpg'],
        ['imgId'=>'uudemo_09','imgSrc'=>'assets/images/hotpages/uupilkada/2014-09-16-3970_3_pilkadarommy.jpg'],
        ['imgId'=>'uudemo_10','imgSrc'=>'assets/images/hotpages/uupilkada/tolak-ruu-pilkada-mahasiswa-demo-di-balai-kota-malang-tCr.jpg'],
        ['imgId'=>'uudemo_11','imgSrc'=>'assets/images/hotpages/uupilkada/demo-pdip.jpg'],
        ['imgId'=>'uudemo_12','imgSrc'=>'assets/images/hotpages/uupilkada/uploads--1--2014--09--60364-pmii-demo-surabaya-tegaskan-tolak-ruu-pilkada.jpg'],
        ['imgId'=>'uudemo_13','imgSrc'=>'assets/images/hotpages/uupilkada/mati_demokrasi.jpg'],
        ['imgId'=>'uudemo_14','imgSrc'=>'assets/images/hotpages/uupilkada/lsg1.jpg'],
        ['imgId'=>'uudemo_15','imgSrc'=>'assets/images/hotpages/uupilkada/demo_ruu.jpg']       

    ],
    'quotePro'=>[
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a pbibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit ctus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur  
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreeonsequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],

       

    ],
    'quoteKontra'=>[
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a pbibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit ctus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur  
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreeonsequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
        [
            'refName'=>'Gamawan Fauzi',
            'refTitle'=>'Menteri Dalam Negeri',
            'refWord'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus consectetur lectus eu lorem sagittis at 
            laoreet libero blandit. Morbi a purus et nulla posuere mattis. Pellentesque nulla mauris, consequat eleifend bibendum 
            sed.',
            'refUrl'=>'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
            'refImg'=>'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg'
        ],
    ],
    'quoteNetral'=>[],
    'petaKekuatan'=>[],
    'organisasiMenolak'=>[
        ['org_name' => 'APKASI', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logo-APKASI-1-300x300.jpg'],
        ['org_name' => 'APEKSI', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/Logo_Apeksi.png'],
        ['org_name' => 'ICW', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logo_icw.jpg'],
        ['org_name' => 'Partai Demokrasi Indonesia Perjuangan', 'org_logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg'],
        ['org_name' => 'Partai Kebangkitan Bangsa', 'org_logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaikebangkitanbangsa5119b257621a4/badge/6a82dd0643ed4bfabc5fb872396961cb96402fb5.jpg'],
        ['org_name' => 'Partai Nurani Rakyat', 'org_logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaihatinuranirakyathanura5119a1cb0fdc1/badge/partaihatinuranirakyathanura5119a1cb0fdc1_20130212_020239.jpg'],
        ['org_name' => 'Nasional Demokrat', 'org_logo' => 'http://www.bijaks.net/public/upload/image/politisi/nasdem5119b72a0ea62/thumb/portrait/2c9fdf0f3e55f6cc61b6b11a45224e324abecbb5.jpg'],
        ['org_name' => 'The Habibie Center', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logothc.gif'],
        ['org_name' => 'Jaringan Pendidikan & Pemilih Untuk Rakyat', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/jppr.jpg'],
        ['org_name' => 'KPP Indonesia', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/kpp_indo.jpg'],
        ['org_name' => 'Pusat Studi Hukum & Kebijakan Indonesia', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/pshk-logo2.png'],
        ['org_name' => 'Pusat Kajian Politik FISIP UI', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/puskapol.jpg'],
        ['org_name' => 'Pusat Telaah & Informasi Regional', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/PATTIRO.jpg'],
        ['org_name' => 'YAPPIKA', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/yappika.jpg'],
        ['org_name' => 'Populi Center', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/95ASBOVI.jpeg'],
        ['org_name' => 'Komite Pemantauan Pelaksanaan Otonomi Daerah', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/Logo-KPPOD.jpg'],
        ['org_name' => 'Komite Pemantau Legislatif', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logo-kopel2.jpg'],
        ['org_name' => 'LSM Perludem', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/perludem-rentetan-kebakaran.2345.jpg']
    ]
     


];


?>

<style>

    #uupilkada .tagline {background: url('http://www.bijaks.net/assets/images/hotpages/gaza/ico-pal-israel.gif') no-repeat rgba(0, 0, 0, 0); height: 32px;
        position: relative;text-align: left;}
    #uupilkada .tagline .title {padding-left:27px;font-size:22px;margin-left:10px;line-height:32px;}

    #uupilkada .pilkada-word {font-family: arial;font-size: 14px;font-weight: bold;}
    #uupilkada .ul-pilkada {font-size: 14px;list-style: disc;margin-top:-10px;}
    #uupilkada .penggagas {float:left; width: 35px;height: auto;}
    #uupilkada .penggagas img {width: 32px; height: auto;}
    #uupilkada .penggagas-p{float:left;margin-left:5px;margin-top:0px;font-size: 14px;line-height:12px;width: 160px;}

    #uupilkada li.produk-hebat {float: left; margin: 0; vertical-align: top;}
    #uupilkada li.produk-hebat img {width: auto; height: 70px;  padding: 2px !important;}

    #uupilkada li.aksi {float: left;  margin: 0; vertical-align: top;}
    #uupilkada li.aksi img {width: auto; height: 100px;  padding: 2px !important;}

    #uupilkada .box-content {background-color: #ffffff;border: 10px solid #E5E5E5;border-radius:5px;box-shadow:2px 2px 2px #878787;}

    #uupilkada .tokoh {margin: 0; vertical-align: top;border-bottom: solid 2px #cecece;}
    #uupilkada .tokoh a {color: #0000cc; text-decoration: underline;}
    #uupilkada .tokoh a:hover {color: #0000cc; text-decoration: underline;}
    #uupilkada .tokoh img {width: 50px; height: 50px;  padding: 2px !important;}
    #uupilkada .tokoh .name {width:120px;padding-left:5px;padding-top: 5px;font-size: 11px;font-weight:bold;font-family: helvetica, arial, sans-serif;color:#000;line-height: 14px;}
    #uupilkada .tokoh .title {width:120px;padding-left:5px;margin-top:-10px;font-size: 11px;font-family: helvetica, arial, sans-serif;color:#000;line-height: 14px;}
    #uupilkada .tokoh .word {width:140px;padding-left:5px;margin-top:-10px;font-style:italic;font-size: 11px;font-family: helvetica, arial, sans-serif;color:#FF0000;line-height: 14px;}

    #uupilkada li.organisasi {float: left; margin: 2px; padding:5px;vertical-align: top;width: auto;height: 100px;/*border: solid 4px #c4c4c4;*/text-align: center}
    #uupilkada li.organisasi img {width: auto; height: 80px;  padding: 2px !important;}
    #uupilkada li.organisasi p {width: 130px;padding: 2px !important;font-size: 10px;height: auto;line-height: 12px;}

</style>
<br/>
<div class="container" style="margin-top:5px;">
<div class="sub-header-container">
<div><img src="<?php echo base_url().'assets/images/hotpages/uupilkada/banner_ruupilkada.jpg';?>"></div>

<div id="uupilkada" style="background-color:#ffffff;">
<div id="gagasan">

    <div style="margin-left:20px;padding-left:100px;margin-top:-300px;">
        <p style="width:600px;font-family: arial;font-size: 30px;color: #ffffff;line-height:30px;font-weight: bold;text-shadow: 2px 2px #000000;"><?php echo strtoupper($headline);?></p>
        <!-- ul style="list-style: disc;">
           <li style="font-size: 20px;margin-left:-10px;color: #FC0000;text-shadow: 2px 2px #FCF9F9;">Gubernur dipilih tidak lagi dipilih langsung oleh rakyat, meliankan oleh DPRD provinsi.</li>
           <li style="font-size: 20px;margin-left:-10px;color: #FC0000;text-shadow: 2px 2px #FCF9F9;">Wakil gubernur dan wakil bupati/wakil walikota ditunjuk dari lingkungan PNS.</li>
           <li style="font-size: 20px;margin-left:-10px;color: #FC0000;text-shadow: 2px 2px #FCF9F9;">Pilkada hanya memilih-milih gubernur dan bupati/walikota.</li>
        </ul -->
    </div>

    <div style="margin-left:5px;margin-top:170px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">CALON</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            if(!empty($data['calonDiusung'])){
                foreach ($data['calonDiusung'] as $key => $val) {
                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 180px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;height: 120px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;">&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }};
            ?>
        </div>
    </div>

    <?php 
    if(!empty($data['pengusungCalon']['aktorPengusung'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 247px;">
        <div class="tagline"><span id="penggagas" class="title">TOKOH PENGUSUNG</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['pengusungCalon']['aktorPengusung'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 190px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;height: 120px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><img src="<?php echo $val['logo'];?>"></div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>

    <?php 
    if(!empty($data['pengusungCalon']['partaiPengusung'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 247px;">
        <div class="tagline"><span id="penggagas" class="title">PARTAI PENGUSUNG</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['pengusungCalon']['partaiPengusung'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 190px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;height: 120px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;">&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>

    <?php 
    if(!empty($data['penentangPencalonan']['institusiPenentang'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 247px;">
        <div class="tagline"><span id="penggagas" class="title">INSTITUSI YANG MENENTANG</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['penentangPencalonan']['institusiPenentang'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 207px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;margin-top: 14px;margin-left: 7px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><img src="<?php echo $val['logo'];?>"></div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>


    <?php 
    if(!empty($data['pendukungPencalonan']['institusiPendukung'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">INSTITUSI YANG MENDUKUNG</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['pendukungPencalonan']['institusiPendukung'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <div style="float: left;width:128px;height: 170px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;">&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>


    <?php 
    if(!empty($data['penentangPencalonan']['aktorPenentang'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 247px;">
        <div class="tagline"><span id="penggagas" class="title">TOKOH YANG MENENTANG</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['penentangPencalonan']['aktorPenentang'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 207px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><img src="<?php echo $val['logo'];?>"></div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>

    
    <?php 
    if(!empty($data['pendukungPencalonan']['aktorPendukung'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 247px;">
        <div class="tagline"><span id="penggagas" class="title">TOKOH YANG MENDUKUNG</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['pendukungPencalonan']['aktorPendukung'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 207px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><img src="<?php echo $val['logo'];?>"></div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>



 <?php 
    if(!empty($data['penentangPencalonan']['partaiPenentang'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">PARTAI YANG MENENTANG</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['penentangPencalonan']['partaiPenentang'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 170px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;margin-top: 14px;margin-left: 7px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;">&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>

    <?php 
    if(!empty($data['kelompokPro']['politisiPro'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">POLITISI PRO</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['kelompokPro']['politisiPro'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 170px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;margin-top: 14px;margin-left: 7px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><!-- <img src="<?php echo $val['logo'];?>"> -->&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>

    <?php 
    if(!empty($data['kelompokKontra']['politisiKontra'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">POLITISI KONTRA</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['kelompokKontra']['politisiKontra'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 170px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;margin-top: 14px;margin-left: 7px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><!-- <img src="<?php echo $val['logo'];?>"> -->&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>


    <?php 
    if(!empty($data['kelompokPro']['tokohPro'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">TOKOH PRO</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['kelompokPro']['tokohPro'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 170px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;margin-top: 14px;margin-left: 7px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><!-- <img src="<?php echo $val['logo'];?>"> -->&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>

    <?php 
    if(!empty($data['kelompokKontra']['tokohKontra'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">TOKOH KONTRA</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['kelompokKontra']['tokohKontra'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 170px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;margin-top: 14px;margin-left: 7px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><!-- <img src="<?php echo $val['logo'];?>"> -->&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>

    <?php 
    if(!empty($data['kelompokPro']['pengamatPro'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">PENGAMAT PRO</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['kelompokPro']['pengamatPro'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 170px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;margin-top: 14px;margin-left: 7px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><!-- <img src="<?php echo $val['logo'];?>"> -->&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>

    <?php 
    if(!empty($data['kelompokKontra']['pengamatKontra'])){
    ?>
    <div style="margin-left:5px;margin-bottom: 200px;">
        <div class="tagline"><span id="penggagas" class="title">PENGAMAT KONTRA</span></div>
        <div style="padding-top:10px;margin-left:-20px;">
            <?php
            foreach($data['kelompokKontra']['pengamatKontra'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                ?>
                <div style="float: left;width:128px;height: 170px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                    <div style="width: 128px;text-align: center;margin-bottom: 5px;">
                        <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                    </div>
                    <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                        <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                    </h4>
                    <div style="height: 25px;width: 132px;margin-top: 14px;margin-left: 7px;">
                        <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><!-- <img src="<?php echo $val['logo'];?>"> -->&nbsp;</div>
                        <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php //echo $val['jabatan']; ?></p>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <?php };?>


    <?php 
    if(!empty($data['quotePro'])){
    ?>
    <div class="tagline"><span id="komen-tolak" class="title">KOMENTAR PRO</span></div>
    <div style="margin-bottom: 1em;" id="masonry_container1">
        <?php foreach($data['quotePro'] as $v){?>
        <div class="item_masonry1">
            <div class="tokoh" style="width: 180px;height:auto;margin-left: 10px;margin-right: 10px;margin-top:5px;
            display: inline-block;padding-top:1px;padding-bottom: 1px;vertical-align: top;">
                <div style="width: 180px;height: 55px;">
                    <div style="float:left;height:55px;">
                        <?php echo !empty($v['refUrl']) ? '<a href="'.$v['refUrl'].'">' : '';?>
                        <img src='<?php echo $v['refImg'];?>'
                        data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $v['refName']; ?>" 
                        alt='<?php echo $v['refName']; ?>'/>
                        <div style=""></div>
                        <?php echo !empty($v['refUrl']) ? '</a>' : '';?>
                    </div>
                    <div style="float:left;height:55px;">
                        <?php echo !empty($v['refUrl']) ? '<a href="'.$v['refUrl'].'">' : '';?>
                        <p class="name"><?php echo $v['refName']; ?></p>
                        <?php echo !empty($v['refUrl']) ? '</a>' : '';?>
                        <p class="title"><?php echo $v['refTitle']; ?></p>
                    </div>
                </div>
                <div style="float:none;margin-top:15px;">
                    <p class="word"><?php echo $v['refWord']; ?></p>
                </div>
            </div>


        </div>
        <?php }?>
    </div>
    <?php };?>

    <?php 
    if(!empty($data['quoteKontra'])){
    ?>
    <div class="tagline"><span id="komen-tolak" class="title">KOMENTAR KONTRA</span></div>
    <div style="margin-bottom: 2em;" id="masonry_container2">
        <?php foreach($data['quoteKontra'] as $v){?>
        <div class="item_masonry2">
            <div class="tokoh" style="width: 180px;height:auto;margin-left: 10px;margin-right: 10px;margin-top:5px;
            display: inline-block;padding-top:1px;padding-bottom: 1px;vertical-align: top;">
                <div style="width: 180px;height: 55px;">
                    <div style="float:left;height:55px;">
                        <?php echo !empty($v['refUrl']) ? '<a href="'.$v['refUrl'].'">' : '';?>
                        <img src='<?php echo $v['refImg'];?>'
                        data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $v['refName']; ?>" 
                        alt='<?php echo $v['refName']; ?>'/>
                        <div style=""></div>
                        <?php echo !empty($v['refUrl']) ? '</a>' : '';?>
                    </div>
                    <div style="float:left;height:55px;">
                        <?php echo !empty($v['refUrl']) ? '<a href="'.$v['refUrl'].'">' : '';?>
                        <p class="name"><?php echo $v['refName']; ?></p>
                        <?php echo !empty($v['refUrl']) ? '</a>' : '';?>
                        <p class="title"><?php echo $v['refTitle']; ?></p>
                    </div>
                </div>
                <div style="float:none;margin-top:15px;">
                    <p class="word"><?php echo $v['refWord']; ?></p>
                </div>
            </div>


        </div>
        <?php }?>
    </div>
    <?php };?>

    <?php if(!empty($data['organisasiMenolak'])){?>
    <div class="tagline"><span id="komen-tolak" class="title">ORGANISASI YANG MENOLAK</span></div>
    <div style="margin-bottom: 1em;" id="masonry_container3">
    <?php 
    foreach ($data['organisasiMenolak'] as $key => $val) {
    ?>
        <div class="item_masonry3" style="width: 100px;">
        <img class="organisasiLogo" src="<?php echo $val['org_logo'];?>" alt="<?php echo $val['org_name']; ?>"/>
                    <p class="smalltitle"><?php echo strtoupper($val['org_name']);?></p>
        </div>    
    <?php        
    }
    ?>

    </div>
    <?php };?>

</div>





<div class="tagline"><span id="analisa" class="title">ANALISA & KONTRADIKSI</span></div>
<div class="box-content" style="margin-bottom: 10px;padding:10px;">
    <p style="font-size: 18px;font-weight: bold;">ANALISA</p>
    <p style="width:920px;font-family: arial;font-size: 18px;color: #880F04;font-weight: bold;">Jika disahkan pada 25 September 2014 kelak, pilkada akan berlaku serentak di 202 kabupaten/kota provinsi mulai 2015. Menurut Depdagri, Pilkada 2015 butuh dana 70 triliun.</p>
    <p class="pilkada-word" style="color:  #880F04;">Ketakutan PARPOL pendukung :</p>
    <ul class="ul-pilkada">
        <li style="font-size: 14px;margin-left:-10px;">Ketakutan pertama adalah takut dekat dengan pemilih.</li>
        <li style="font-size: 14px;margin-left:-10px;">Ketakutan kedua adalah partai politik takut dievaluasi.</li>
        <li style="font-size: 14px;margin-left:-10px;">Ketakutan ketiga adalah partai politik takut menjadi partai terbuka.</li>
        <li style="font-size: 14px;margin-left:-10px;">Ketakutan keempat adalah partai politik takut dipantau.</li>
        <li style="font-size: 14px;margin-left:-10px;">Selanjutnya adalah partai Pendukung Pilkada tak langsung pada dasarnya tidak memiliki kader.</li>
    </ul>
    <br/>
    <p style="font-size: 18px;font-weight: bold;">KONTRADIKSI</p>
    <ul class="ul-pilkada" style="margin-top:-10px;">
        <li style="font-size: 14px;margin-left:-10px;">RUU Pilkada dianggap bertentangan dengan Undang-Undang MPR, DPR, DPRD dan DPD (UU MD3) yang baru disahkan DPR.</li>
        <li style="font-size: 14px;margin-left:-10px;">Kemunduran Proses Demokrasi.</li>
        <li style="font-size: 14px;margin-left:-10px;">Indonesia Menganut system Presidensial.</li>
        <li style="font-size: 14px;margin-left:-10px;">Meniadakan hak setiap warga negara untuk berpartisipasi dalam.</li>
        <li style="font-size: 14px;margin-left:-10px;">DPR dan DPRD tugasnya adalah untuk membuat aturan, mengawasi pemerintahan dan menyusun budget. Bukan mewakili dalam memilih pemimpin.</li>
        <li style="font-size: 14px;margin-left:-10px;">DPR sudah punya banyak catatan buruk untuk merampas kewenangan partisipasi masyarakat.</li>
        <li style="font-size: 14px;margin-left:-10px;">RUU Pilkada didukung oleh partai dan fraksi Koalisi Merah Putih yang kalah dalam Pilpres 2014.Koalisi ini ingin menjadikan RUU Pilkada sebagai balas dendam kekalahan dengan cara menguasai kepala-kepala daerah.</li>
    </ul>
</div>



<?php 
if(!empty($data['aksiPenolakan'])){
?>
<div style="margin-bottom: 10px;">
    <div class="span8">
        <div class="tagline" style="margin-left: -23px;"><span id="aksi-tolak" class="title">AKSI PENOLAKAN</span></div>
        <div class="box-content" style="margin-bottom: 10px;padding:10px;height: 635px;margin-left: -23px;">
            <ul style="height: 520px;">
                <?php foreach ($data['aksiPenolakan'] as $value) {?>
                <li class="aksi">
                    <a href="#" data-toggle="modal" data-target="#<?php echo $value['imgId']?>">
                        <img src="<?php echo base_url().$value['imgSrc']; ?>" alt='aksi_brutal'/>
                    </a>
                    <div class="modal fade" id="<?php echo $value['imgId']?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                </div>
                                <div class="modal-body" style="text-align: center;">
                                    <img src='<?php echo base_url().$value['imgSrc']; ?>' style="height: 250px;width: auto;">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                    
                <?php };?>

            </ul>
            <p style="font-size: 14px;margin-left:25px;">Gerakan Rakyat Untuk Pilkada Langsung. Massa dari Gerakan Rakyat Untuk Pilkada Langsung membakar ban bekas saat menggelar unjuk rasa di depan Gedung DPRD Provinsi Jawa Barat, Jalan Diponegoro, Kota Bandung.</p>
        </div>
    </div>
    <?php };?>




    <div class="span4">
        <div id="berita" class="tagline"><span class="title">BERITA TERKAIT</span></div>
        <div style="width: 300px;height:auto;">
            <div style="padding-left:0px;background-color: #E5E5E5;">
                <div id="cakapolri_container" data-tipe="1" data-page='1' style="height: auto;margin-bottom: 9px !important;"></div>
                <div class="row-fluid" style="margin-bottom: 2px;">
                    <div class="span6 text-left">
                        <a id="cakapolri_loadmore" data-tipe="1" class="btn btn-mini" >10 Berikutnya</a>
                    </div>
                    <div class="span6 text-right">
                        <!-- a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- <div style="clear: both;margin-bottom: 15em;border-bottom: solid 1px black;"></div>


<div class="tagline"><span id="alasan" class="title">ALASAN</span></div>
<div class="box-content" style="margin-bottom: 10px;padding:10px;">
    <div style="">
        <p class="pilkada-word" style="color: #000000;">Alasan Pendukung :</p>
        <ul class="ul-pilkada">
            <li style="font-size: 14px;margin-left:-10px;">Pilkada langsung menelan biaya besar</li>
            <li style="font-size: 14px;margin-left:-10px;">Sejak 2004, pilkada langsung sudah mengantarkan 290 orang yang bermasalah dengan hukum ke kursi kekuasaan</li>
            <li style="font-size: 14px;margin-left:-10px;">Kementerian Luar Negeri mencatat sudah lebih dari 300 orang kepala daerah terpilih sejak 2004 terjerat kasus korupsi</li>
            <li style="font-size: 14px;margin-left:-10px;">Tidak menjamin tercapainya kualitas kepala daerah yang baik</li>
            <li style="font-size: 14px;margin-left:-10px;">Sangat rawan gesekan horizontal</li>
            <li style="font-size: 14px;margin-left:-10px;">Dianggap mubazir dan tidak efisien</li>
        </ul>
    </div>
</div> -->







</div>
</div>
</div>

<script type="text/javascript">
$('#masonry_container1').masonry({
  columnWidth: 200,
  itemSelector: '.item_masonry1'
});

$('#masonry_container2').masonry({
  columnWidth: 200,
  itemSelector: '.item_masonry2'
});

$('#masonry_container3').masonry({
  columnWidth: 200,
  itemSelector: '.item_masonry3'
});
</script>



