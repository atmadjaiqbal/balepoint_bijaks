<?php 
$data = [
    'NARASI'=>[
        'title'=>'INTRIK POLITIK PEMILIHAN KAPOLRI',
        'narasi'=>'<img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/analisa1.jpg" class="pic">Keputusan Jokowi memilih Budi Gunawan dan mengajukannya sebagai calon tunggal Kapolri memunculkan polemik. Polemik terjadi lantaran Komisi Pemberantasan Korupsi menetapkan Budi Gunawan sebagai tersangka dalam kasus kepemilikan rekening gendut. 
                </p><p>Lalu polemik bergulir. Ada indikasi kuat dipilihnya Kepala Lembaga Pendidikan Polri Komjen Budi Gunawan karena dekat dengan PDIP dan Megawati Soekarno Putri. Budi Gunawan dipilih oleh Presiden RI Joko Widodo menggantikan Jenderal Sutarman.'
    ],
    'PROFIL'=>[
        'title'=>'CALON TUNGGAL',
        'narasi'=>'<span style="font-size: 16px;font-weight: bold;color: black;text-align: center;">KOMJEN POL. BUDI GUNAWAN</span><br><br> <span style="font-size: 16px;font-weight: bold;">Budi Gunawan</span> adalah Calon tunggal Kepala Polisi Republik Indonesia pilihan Presiden Joko Widodo dan Megawati Soekarno Putri. Ia memang perwira yang cakap dan brilian. Beragam prestasi dan penghargaan didapat Budi Gunawan.
                </p><p style="margin-left: 20px;margin-right: 20px;">Saat menjalani pendidikan Akademi Kepolisian angkatan 1983, Budi Gunawan lulus dengan predikat terbaik. Ia juga pernah meraih penghargaan Adhi Makayasa yang sangat prestisius.
                </p><p style="margin-left: 20px;margin-right: 20px;">Di setiap pendidikan kepolisian, Budi selalu meraih peringkat satu dan lulusan terbaik seperti saat  menempuh pendidikan di Sekolah Tinggi Ilmu Kepolisian-PTIK, Sekolah Staf dan Pimpina Polri, Sespati, dan Lemhanas.<img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/bg2.jpg" class="picprofil">',
        'armada'=>[
            ['no'=>'71 Pesawat Boeing 737-900ER'],
            ['no'=>'30 Pesawat Boeing 737-800'],
            ['no'=>'2 Pesawat Boeing 737-300'],
            ['no'=>'2 Pesawat Boeing 737-400'],
            ['no'=>'2 Boeing 747-400'],
            ['no'=>'3 McDonnell Douglas MD-90']
        ],
        'anak'=>[
            ['no'=>'Wings Air'],
            ['no'=>'Lion Bizjet'],
            ['no'=>'Batik Air'],
            ['no'=>'Malindo Airline (Malaysia)'],
            ['no'=>'Thai Lion Air (Thailand)']
        ]
    ],
    'SERANGAN'=>'<img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/bg2.jpg" class="pic2">Jalan mulus Budi Gunawan mendapatkan posisi Kapolri tiba-tiba terhenti oleh putusan Komisi Pemberantasan Korupsi. Pada hari Selasa, 13 Januari 2015, lembaga anti rasuah tersebut menetapkan Budi Gunawan sebagai tersangka kasus suap dan gratifikasi ketika menjabat Kepala Biro Pembinaan Mabes Polri 2003-2006 dan jabatan lainnya. 
                </p><p><img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/bg2.jpg" class="pic">"Kita ingin menyampaikan progress report dari kasus penyelidikan transaksi korupsi tidak wajar. KPK telah melakukan penyelidikan sejak Juli 2014, jadi sudah setengah tahun lebih kami melakukan penyelidikan atas kasus transaksi mencurigakan," jelas Abraham.
                </p><p>Abraham Samad juga menjelaskan bahwa sudah sejak lama BG mendapatkan catatan merah dari KPK. KPK menyangka Budi Gunawan dengan Pasal 12 a atau b, Pasal 5 ayat 2, Pasal 11, atau Pasal 12 B Undang-Undang Pemberantasan Korupsi juncto Pasal 55 ayat 1 KUHP.',
    'POLRI'=>[
        'narasi'=>'Pasca Komisi Pemberantasan Korupsi menetapkan Budi Gunawan sebagai tersangka, serangan terhadap pimpinan KPK mulai berdatangan.',
        'isi'=>[
            ['no'=>'Beredar di media sosial foto mirip Ketua KPK Abraham Samad tengah beradegan mesra dengan seorang perempuan yang diduga Puteri Indonesia 2014, Elvira Devinamira Wirayanti.'],
            ['no'=>'Sepekan setelah penetapan Budi Guawan sebagai tersangka oleh KPK, Jumat, 23 januari 2015,Bareskrim Polri menangkap Wakil Ketua KPK Bambang Widjojanto (BW) di sebuah jalan raya di Depok, sekitar pukul 07:30, Jumat, 23 Januari 2015.
                    Polri menetapkan BW sebagai tersangka dalam kasus pemberian keterangan palsu di depan sidang sengketa Pilkada Kotawaringin Barat, Kalimantan Tengah, pada tahun 2010. Saat itu Bambang Widjojanto menjadi pengacara pasangan bupati Ujang Iskandar - Bambang Purwanto dalam sengketa Pemilukada Kotawaringin Barat, Kalimantan Tengah.'],
            ['no'=>'Selanjutnya, Sabtu, 24 Januari 2015 Wakil Ketua KPK Adnan Pandu Praja dilaporkan ke Bareskrim Mabes Polri atas tuduhan mengambil paksa saham milik PT Daisy Timber, Berau, Kalimantan Timur, pada 2006.
                    Adnan Pandu disangkakan dengan pasal Tindak Pidana memasukkan Keterangan Palsu ke Dalam Akta Autentik serta Turut Serta Melakukan Tindak Pidana Pasal 266 juncto Pasal 55 KUHP.'],
            ['no'=>'Pada 22 Januari 2015, Abraham Samad dilaporkan oleh Direktur Eksekutif KPK Watch Indonesia, Yusuf Sahide ke Bareskrim Polri. Laporan tertuang dalam surat laporan bernomor LP/75/1/2015 Bareskrim. Ia diduga melanggar Pasal 36 Juncto 65 UU Nomor 30 Tahun 2002 tentang KPK, berkaitan dengan pelanggaran etik. Samad juga dilaporkan karena adanya pertemuan dengan petinggi salah satu partai. Juga menjanjikan bantuan hukum dan bisa meringankan hukuman Emir Moeis.'],
            ['no'=>'Senin, 26 Januari, Ketua KPK Abraham Samad kembali dilaporkan ke Bareskrim Polri atas tudingan melakukan pertemuan dengan petinggi Partai Demokrasi Perjuangan (PDIP), Hasto Kristyanto dalam bursa calon Wakil Presiden Joko Widodo. Lobi politik tersebut diduga Abraham Samad memberikan imbalan bantuan hukum bagi kader partai Emir Moeis.'],
            ['no'=>'Lembaga Swadaya Masyarakat Gerakan Masyarakat Bawah Indonesia (LSM GMBI) melaporkan Abraham Samad ke Bareskrim Polri atas dugaan kepemilikan senjata api (senpi) ilegal, Senin, Januari 2015. Laporan GMBI itu diterima Bareskrim dengan Laporan Polisi Nomor: LP/160/II/2015/Bareskrim‎ dengan terlapor Dr. Abraham Samad, SH.MH. Laporannya, diduga melakukan tindak pidana kepemilikan senjata api tanpa izin.'],
            ['no'=>'Giliran Zulkarnain diadukan oleh Fathur Rosyid ke Bareskrim Polri oleh Aliansi Masyarakat Jawa Timur, Rabu 28 Januari 2015. Zulkarnain diduga melakukan tindak korupsi saat menangani kasus korupsi bantuan sosial (bansos) Program Penanganan Sosial Ekonomi Masyarakat (P2SEM) Jawa Timur pada tahun 2008. Zulkarnain diduga menerima uang suap sekitar Rp 5 miliar untuk menghentikan penyidikan perkara tersebut.']
        ],
        'bawah'=>'Praktis, semua pimpinan KPK berstatus terlapor di Bareskrim Mabes Polri. Selain itu, beberapa penyidik dan karyawan KPK belakangan ini mendapatkan teror. Seorang pegawai KPK mengungkapkan bahwa teror disampaikan lewat pesan pendek, telepon, hingga berkali-kali dibuntuti saat pulang. "Salah satu pesan yang disampaikan adalah pembunuhan," kata sumber.
                </p><p>"Fakta-fakta teror itu sedang kami teliti lebih lanjut, kami sudah membentuk tim dan pada saatnya akan kami beri tahu kepada publik," kata Bambang, Rabu, 11 Februari 2015.'
    ],
    'PENYOKONG'=>[
        ['name'=>'Joko Widodo','jabatan'=>'Presiden RI','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg' ],
        ['name'=>'Megawati Soekarnoputri','jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg','img'=>'http://www.bijaks.net/public/upload/image/politisi/megawatisoekarnoputri50ee62bce591e/badge/03a4e973ba03f1ae7ba2234d4e8c61844b8669f6.jpg'],
    ],
    'PENDUKUNG'=>[
        'partaiPolitik'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227']
        ],
        'institusi'=>[
            ['name'=>'Menteri Dalam Negeri','img'=>'http://www.bijaks.net/public/upload/image/politisi/kementriandalamnegeri531c1cbb6af24/thumb/b391f936d3d25df881427ebef81eae2180e78f1c.jpg','url'=>''],
            ['name'=>'Menteri Koordinator Politik Hukum dan Keamanan','img'=>'http://upload.wikimedia.org/wikipedia/id/e/e2/Logo_Kemenkopolhukam.png','url'=>''],
            ['name'=>'Indonesian Police Watch','img'=>'http://napzaindonesia.com/wp-content/uploads/2011/08/IPW.jpg','url'=>'']
        ]
    ],
    'PENENTANG'=>[
        'partaiPolitik'=>[
            ['page_id'=>'partaidemokrat5119a5b44c7e4'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'partaikeadialndnpersatuanindonesia5119bd7483d2a']
        ],
        'institusi'=>[
            ['name'=>'Komisi Pemberantasan Korupsi','img'=>'http://www.bijaks.net/public/upload/image/politisi/komisipemberantasankorupsi5192fb408b219/thumb/66c285e30ed48d4b4cb7101e3ee3ffe7980deb29.jpg','url'=>'http://www.bijaks.net/aktor/profile/komisipemberantasankorupsi5192fb408b219'],
            ['name'=>'TIM 9','img'=>'http://www.bijaks.net/public/upload/image/politisi/tim9independen54db1f2a63520/badge/56e00d2fb307119a5a674e430619b419e2e99837.jpeg','url'=>'#'],
            ['name'=>'KOMNAS HAM','img'=>'http://www.bijaks.net/public/upload/image/politisi/komnasham54c1eac938164/badge/e559bfdeda6bc708d46fcb9a29afb001bb2360ec.jpg','url'=>'http://www.bijaks.net/aktor/profile/komnasham54c1eac938164'],
            ['name'=>'Transparancy International Indonesia','img'=>'http://www.ti.or.id/template/tii/images/logoTII_1.jpg','url'=>'http://www.ti.or.id/'],
            ['name'=>'Pusat Pelaporan dan Analisis Transaksi Keuangan (PPATK)','img'=>'http://www.ppatk.go.id/web/images/logo_utama.png','url'=>'http://www.ppatk.go.id'],
            ['name'=>'Indonesia Corruption Watch','img'=>'http://www.bijaks.net/public/upload/image/politisi/icwindonesiancorruptionwatch534e45d49c5d9/badge/41c3255489b7e6c9e7387c77a6b573127ea9dfe4.jpg','url'=>'http://www.bijaks.net/aktor/profile/icwindonesiancorruptionwatch534e45d49c5d9'],
            ['name'=>'Kontras','img'=>'http://www.bijaks.net/public/upload/image/politisi/kontras531c20d0d0f90/badge/0cbf1fc9308057d219ee4d62d6ee754476a21523.jpg','url'=>'http://www.kontras.org/'],
            ['name'=>'YLBHI','img'=>'http://www.ylbhi.or.id/wp-content/uploads/2013/04/ylbhi_logo.png','url'=>'http://www.ylbhi.or.id/'],
            ['name'=>'Relawan Pro-Jokowi','img'=>'https://pbs.twimg.com/media/BxxvUhHCEAAnSB5.jpg:large','url'=>'#']
        ],
        'tokoh'=>[
            ['name'=>'Susilo Bambang Yudoyono','img'=>'http://www.bijaks.net/public/upload/image/politisi/susilobambangyudhoyono50ee672eb35c5/badge/39989d0890a536ffa7c758d668a09dbbb0b48a69.jpg','url'=>'http://www.bijaks.net/aktor/profile/susilobambangyudhoyono50ee672eb35c5'],
            ['name'=>'Prabowo Subianto','img'=>'http://www.bijaks.net/public/upload/image/politisi/prabowosubiyantodjojohadikusumo50c1598f86d91/badge/059fd2fa2981621a0708c6a44243a003060151e6.jpg','url'=>'http://www.bijaks.net/aktor/profile/prabowosubiyantodjojohadikusumo50c1598f86d91'],
            ['name'=>'Mahfud MD','img'=>'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/badge/3a1717b0f9432d353291a742de4479b53efc5822.jpg','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b'],
            ['name'=>'Andi Wijdjajanto','img'=>'http://kanalsatu.com/images/20150205-142155_96.jpg','url'=>'#'],
            ['name'=>'Pratikno','img'=>'http://www.bijaks.net/public/upload/image/politisi/pratikno540d1bf323949/badge/05af7cc18358cae9f41d818d58d9039adfc7099a.jpg','url'=>'http://www.bijaks.net/aktor/profile/pratikno540d1bf323949'],
            ['name'=>'Denny Indrayana','img'=>'http://www.bijaks.net/public/upload/image/politisi/dennyindrayana50f51ec9ca991/badge/6a9ea736640f3a21b59eed1c123b037b2b760bb6.jpg','url'=>'http://www.bijaks.net/aktor/profile/dennyindrayana50f51ec9ca991'],
            ['name'=>'Syafii Maarif','img'=>'http://www.thejakartapost.com/files/images/p24-a-1-1.jpg','url'=>'http://www.bijaks.net/aktor/profile/buyaahmadsyafiimaarif543f625a98a10'],
            ['name'=>'Jimly Asshiddiqie','img'=>'http://www.bijaks.net/public/upload/image/politisi/jimlyasshiddiqie51cb99e738a36/badge/0670931aed7f5658b894614263594debfc5692e8.jpeg','url'=>'http://www.bijaks.net/aktor/profile/jimlyasshiddiqie51cb99e738a36'],
            ['name'=>'Imam Prasojo','img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/f/fa/Preskon_indonesia_berprestasi_award_imam_b_prasodjo-20080520-002-wawan.jpg/220px-Preskon_indonesia_berprestasi_award_imam_b_prasodjo-20080520-002-wawan.jpg','url'=>'http://id.wikipedia.org/wiki/Imam_B._Prasodjo'],
            ['name'=>'Todung Mulya Lubis','img'=>'http://www.bijaks.net/public/upload/image/politisi/todungmulyalubis533a790b3fe41/badge/35a871a4bc67d17ff20c8ed3dfd9257eb5c15854.jpg','url'=>'http://www.bijaks.net/aktor/profile/todungmulyalubis533a790b3fe41'],
            ['name'=>'Erry Riyana Hardjapamekas','img'=>'http://www.bijaks.net/public/upload/image/politisi/erryriyanahardjapamekas51a2b3bdb7874/badge/7df3df62f8fffc92d55b501d8cdf19f56877d557.jpg','url'=>'http://www.bijaks.net/aktor/profile/erryriyanahardjapamekas51a2b3bdb7874'],
            ['name'=>'Tumpak Hatorangan Panggabean','img'=>'http://www.bijaks.net/public/upload/image/politisi/tumpakhatoranganpanggabean5193023cd7c5e/badge/1ead704c13a2634564cbb187f194d8e05ad2fc8e.jpg','url'=>'http://www.bijaks.net/aktor/profile/tumpakhatoranganpanggabean5193023cd7c5e'],
            ['name'=>'Bambang Widodo Umar','img'=>'http://www.bijaks.net/public/upload/image/politisi/bambangwidodoumar51ef336d736aa/badge/b73be3163d2946c11c01f628d086274a85b25ff0.jpg','url'=>'http://www.bijaks.net/aktor/profile/bambangwidodoumar51ef336d736aa']
        ],
        'pejabat'=>[
            ['name'=>'Jenderal (Purn) Sutanto','img'=>'http://www.bijaks.net/public/upload/image/politisi/jenderalpolisipurndrssutanto50f51010a58fa/badge/368c0259746181ae5f0a8d6120cfddadfc8e973c.jpg','url'=>'http://www.bijaks.net/aktor/profile/jenderalpolisipurndrssutanto50f51010a58fa'],
            ['name'=>'Komjen (Purn) Oegroseno','img'=>'http://www.bijaks.net/public/upload/image/politisi/oegroseno52a91aeab1520/badge/142515740357e3e84d6c0fea2dffe34be0777bb6.jpg','url'=>'http://www.bijaks.net/aktor/profile/oegroseno52a91aeab1520'],
            ['name'=>'Jendral Sutarman','img'=>'http://www.bijaks.net/public/upload/image/politisi/komjenpoldrssutarman51d2516f6eb74/badge/36548f460d48a13401e6afd25a13f18be615f234.jpg','url'=>'http://www.bijaks.net/aktor/profile/komjenpoldrssutarman51d2516f6eb74'],
            ['name'=>'Komjen Pol Suhardi Alius','img'=>'http://www.bijaks.net/public/upload/image/politisi/suhardialius525b93d8efe1a/thumb/portrait/c31c983c6a789d089d5616ecab3b46e8788e4172.jpg','url'=>'http://www.bijaks.net/aktor/profile/suhardialius525b93d8efe1a'],
        ]
    ],
    'PENOLAKAN'=>[
        ['title'=>'Aksi “Tutup Mata”','img'=>'assets/images/hotpages/cakapolri/tutupmata1.jpg','no'=>'Relawan pendukung Joko Widodo dan para aktivis antikorupsi mendesak Presiden Jokowi membatalkan pencalonan Budi Gunawan sebagai Kapolri. (15 Januari).'],
        ['title'=>'Petisi Change.org','img'=>'assets/images/hotpages/cakapolri/petisichange1.jpg','no'=>'Petisi daring "Jokowi, Jangan Menutup Mata Dalam Memilih Calon Kapolri".  Digagas Emerson melalui situs change.org. Mendesak Presiden Jokowi membatalkan pelantikan Komisaris Jenderal Budi Gunawan sebagai Kepala Polri.'],
        ['title'=>'Demonstrasi di Sukabumi','img'=>'assets/images/hotpages/cakapolri/sukabumi2.jpg','no'=>'Puluhan mahasiswa dari Ikatan Mahasiswa Muhammadiyah (IMM) Sukabumi mendemo Gedung DPRD Kota Sukabumi. Mereke menolak adanya kepentingan politik dalam proses hukum baik di Polri. (26 Januari).'],
        ['title'=>'Demonstrasi di Yogyakarta','img'=>'assets/images/hotpages/cakapolri/yogyakarta2.jpg','no'=>'Berbagai lembaga swadaya masyarakat (LSM) di Yogyakarta mendatangi kantor Dewan Pimpinan Daerah Partai Demokrasi Indonesia Perjuangan (PDIP) DIY. Mereka menuntut pembatalan Budi Gunawan sebagai calon Kapolri dan Megawati Soekarnoputri tidak menghentikan intervensi Presiden Joko Widodo. (29 Januari).'],
        ['title'=>'Demonstrasi di HI','img'=>'assets/images/hotpages/cakapolri/hi1.jpg','no'=>'Demonstrasi atas nama Koalisi Masyarakat Sipil di depan pos Polisi Bundaran HI, Jakarta Pusat saat Car Free Day. Massa mendesak Jokowi untuk  membatalkan pengangkatan Budi Gunawan jadi Kapolri. (18 Januari).']
    ],
    'ANALISA'=>'<img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/intrik.jpg" class="pic2">Setelah KPK menentapkan Budi Gunawan sebagai tersangka dalam kasus suap dan gratifikasi, Presiden Jokowi berada dalam kepungan tiga kekuatan besar. Pertama, parpol pengusungnya, PDIP dan partai koalisi yang mendesak Jokowi tetap melantik Budi Gunawan. Kedua, Jokowi menghadapi kepungan people power yang merupakan pendukungnya sendiri. Mereka aktif menolak Budi Gunawan. Ketiga, DPR itu sendiri yang sudah menyatakan Budi Gunawan layak dan patut sebagai Kapolri baru.
                </p><p><img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/intrik1.jpg" class="pic">Sebagai penyokong calon tunggal Kapolri, PDIP marah besar kepada Jokowi, lantaran tidak segera melantik Budi Gunawan. Ini kali kedua Jokowi menolak pesanan Megawati terkait Budi Gunawan. Setelah menolak Budi Gunawan sebagai Menteri Sekretaris Negara dan Kapolri.
                </p><p>Dalam situasi terjepit, Jokowi gagap dalam mengambil kebijakan. Solusi yang diambilnya adalah jalan tengah; menunda pelantikan Budi Gunawan. Namun demikian, publik tetap mengecam Presiden yang tidak segera mencabut Budi Gunawan sebagai calon Kapolri. Di lain sisi, DPR mengancam akan memakzulkan Presiden. PDIP juga menggalang dukungan untuk menjerat kebijakan Jokowi mengganti Budi Gunawan dengan calon Kapolri baru.
                </p><p><img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/intrik2.jpg" class="pic2">Diluar dugaan, untuk menjawab anacaman pemakzulan, Jokowi membuat terobosan baru. Ia membentuk TIM 9 yang diketua Syafii Maarif dan bertemu Prabowo Subianto. Pada akhirnya, Jokowi tidak akan pernah melantik Budi Gunawan sebagai Kapolri. Sumber Tim 9 mengatakan bahwa Presiden memiliki calonnya sendiri untuk posisi Kapolri.',
    'KRONOLOGI'=>[
        ['date'=>'9 Januari 2015','content'=>'Kompolnas menyerahkan lima nama calon Kapolri. Pada hari yang sama, Presiden Jokowi mengirimkan nama Komjen Budi Gunawan  sebagai calon tunggal Kapolri kepada DPR.'],
        ['date'=>'10 Januari 2015','content'=>'Ketua KPK Abraham Samad mengatakan, Jokowi tak melibatkan KPK untuk melakukan penelusuran rekam jejak saat memilih Komjen Budi Gunawan'],
        ['date'=>'12 Januari 2015','content'=>'Surat Presiden soal nama calon Kapolri dibacakan di rapat Paripurna DPR.'],
        ['date'=>'13 Januari 2015','content'=>'KPK menetapkan Komjen Budi Gunawan sebagai tersangka karena dugaan transaksi tidak wajar.'],
        ['date'=>'13 Januari 2015','content'=>'Presiden Jokowi menyatakan menunggu hasil keputusan DPR terhadap Komjen Budi Gunawan, sebelum menetapkan sikapnya sendiri.'],
        ['date'=>'14 Januari 2015','content'=>'Komisi III menyatakan Komjen Budi Gunawan layak dan patut sebagai Kapolri yang baru, menggantikan Jenderal Sutarman.'],
        ['date'=>'15 Januari 2015','content'=>'Rapat paripurna Dewan Perwakilan Rakyat (DPR) menetapkan Komisaris Jenderal Komjen Budi Gunawan sebagai calon kapolri menggantikan Jenderal Sutarman'],
        ['date'=>'16 Januari 2015','content'=>'Presiden Jokowi menunda pelantikan Komjen Budi Gunawan sebagai Kapolri yang sesuai dengan hasil rapat paripurna DPR.'],
        ['date'=>'28 Januari 2015','content'=>'Presiden Jokowi Widodo memanggil sembilan anggota tim independen untuk membahas polemik antara KPK dengan Polri.'],
        ['date'=>'29 Januari 2015','content'=>'Presiden Jokowi bertemu dengan Dewan Pembina Partai Gerindra, Prabowo Subianto. '],
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Jokowi','content'=>'"Kita menghormati KPK. Ada proses hukum di sini. Tetapi juga ada proses politik di DPR. Kita juga menghargai Dewan (Perwakilan Rakyat). Oleh sebab itu, sampai saat ini saya masih menunggu Sidang Paripurna. Sesudah selesai baru nanti akan kita putuskan, kebijakan apa yang akan kita ambil."','jabatan'=>'Presiden RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19'],
        ['from'=>'Tjahjo Kumolo','content'=>'"Ini masalah fakta dan yuridis, kalau kita mencermati dalam rangkaian proses ya Budi Gunawan sudah kapolri." ','jabatan'=>'Menteri Dalam Negeri','img'=>'http://www.bijaks.net/public/upload/image/politisi/tjahjokumolosh50f4fc41d9f04/badge/6d482165036d437e7ee1ea749dba121551c485ff.jpg','url'=>'http://www.bijaks.net/aktor/profile/tjahjokumolosh50f4fc41d9f04'],
        ['from'=>'Jenderal Badrodin Haiti','content'=>'“Pak Budi Gunawan masih akan menunggu proses praperadilannya."','jabatan'=>'Wakapolri','img'=>'http://www.bijaks.net/public/upload/image/politisi/badrodinhaiti531d430cb8e05/badge/bf1c71e9a9a4da57444be6bd6ee4204289caa29c.jpg','url'=>'http://www.bijaks.net/aktor/profile/badrodinhaiti531d430cb8e05'],
        ['from'=>'Neta S Pane','content'=>'“Presiden Jokowi harus memperjuangkan secara maksimal calon Kapolri yang sudah diusulkannya ke DPR, sehingga Jokowi tidak menjadi pecundang dan dituduh melecehkan DPR, Polri maupun melecehkan dirinya sendiri” ','jabatan'=>'Ketua Presidium Indonesia Police Watch (IPW)','img'=>'http://www.bijaks.net/public/upload/image/politisi/netaspane51be6ee808eb6/thumb/portrait/f9619dff06bb15cd22cebe278cd1b6a4d288ba6e.jpg','url'=>'http://www.bijaks.net/aktor/profile/netaspane51be6ee808eb6'],
        ['from'=>'Effendi Simbolon','content'=>'"Siapapun yang berniat menjatuhkan Jokowi, saatnya sekarang. Karena begitu banyak celahnya dan mudah-mudahan dua-duanya (Jokowi-JK) yang jatuh."','jabatan'=>'Politisi PDIP','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
        ['from'=>'Effendi Muara Sakti Simbolon','content'=>'Anda kader partai. Sadarlah. Tanpa partai, Anda bukan siapa-siapa.” ','jabatan'=>'Ketua PDIP','img'=>'http://delinewsindonesia.com/photo/1354305118effendi.jpg','url'=>''],
        ['from'=>'Fahri Hamzah','content'=>'"Kalau anda (Jokowi) tidak melantik, setidaknya ada 3 muka yang anda tampar (wajah Presiden, wajah DPR, wajah BG)."','jabatan'=>'Wakil Ketua DPR ','img'=>'http://www.bijaks.net/public/upload/image/politisi/fahrihamzahse5105e57490d09/badge/4b1333b525b60ef5d2347fa887d0525b7259068c.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09'],
        ['from'=>'Afrudin Jamal','content'=>'"Kami relawan nasional, mendukung Presiden Jokowi untuk segera melantik Komjen Pol Budi Gunawan sebagai Kapolri." ','jabatan'=>'Koordinator Relawan Nasional','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Usman Hamid','content'=>'"Belum pernah kewibawaan Presiden jatuh serendah ini. Lambatnya keputusan Presiden membuat hampir semua institusi negara jadi bulan-bulanan publik.”','jabatan'=>'Kontras','img'=>'http://www.bijaks.net/public/upload/image/politisi/usmanhamid54d41a069aa7a/badge/620b5a25d4fd36497c8e05c3eaaca075dd9b5593.jpg','url'=>'http://www.bijaks.net/aktor/profile/usmanhamid54d41a069aa7a'],
        ['from'=>'J-Flo','content'=>'"Kami akan mencabut dukungan jika Jokowi meneruskan pencalonan Budi Gunawan, dan relawan akan kembali turun ke jalan."','jabatan'=>'Aktris','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
        ['from'=>'Emerson Yuntho','content'=>'"Jokowi harus menarik surat pancalonan Budi Gunawan sebagai dianggap sebagai bentuk koreksi terhadap langkah yang terburu-terburu dalam menetapkan calon tunggal Kapolri"','jabatan'=>'Petisi Daring Change.org','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
        ['from'=>'Airlangga Pribadi','content'=>'"Ini bunuh diri politik Jokowi di hadapan pendukungnya, yang semula meyakini bahwa dia akan membawa agenda perubahan, menerapkan pemerintahan yang bersih." ','jabatan'=>'Pengamat Politik','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
        ['from'=>'Pratikno','content'=>'"Akan lebih indah jika Budi Gunawan mundur dari pencalonan."','jabatan'=>'Menteri Sekeratris Negera','img'=>'http://www.bijaks.net/public/upload/image/politisi/pratikno540d1bf323949/badge/05af7cc18358cae9f41d818d58d9039adfc7099a.jpg','url'=>'http://www.bijaks.net/aktor/profile/pratikno540d1bf323949'],
        ['from'=>'Abraham Samad','content'=>'"Jadi sejak jauh sebelumnya kita sudah beritahu sebenarnya bahwa yang bersangkutan (Budi Gunawan) sudah punya catatan merah, jadi tidak elok kalau diteruskan, jadi ini tidak serta merta." ','jabatan'=>'Ketua KPK','img'=>'http://www.bijaks.net/public/upload/image/politisi/drabrahamsamadshmh5192f6e1b2ac4/badge/0c6f90c561ff0b3f38bc2411fb3c7d5473dd9dde.jpg','url'=>'drabrahamsamadshmh5192f6e1b2ac4'],
        ['from'=>'Jimly Asshidiqie','content'=>'"Kita harapkan tidak dilantik dan kemudian diajukan calon baru. Alasannya karena dia telah berstatus tersangka sehingga bukan hanya rule of law tetapi juga rule of ethics yang harus dijadikan pegangan." ','jabatan'=>'Wakil Ketua Tim 9','img'=>'http://upload.wikimedia.org/wikipedia/id/c/c4/Jimly_asshiddiqie.jpg','url'=>'http://id.wikipedia.org/wiki/Jimly_Asshiddiqie'],
        ['from'=>'Syafii Maarif','content'=>'"Ya namanya juga politik, pasti ditujukan untuk menyandera kebijakan Presiden yang sebenarnya akan membatalkan pencalonan Budi Gunawan sebagai Kapolri." ','jabatan'=>'Ketua TIM 9','img'=>'http://www.thejakartapost.com/files/images/p24-a-1-1.jpg','url'=>''],
        ['from'=>'Mahfud MD','content'=>'“Kalau dia jadi melantik Budi Gunawan yang sudah menjadi tersangka maka akan berbenturan dengan hukum pidana nantinya.” ','jabatan'=>'Mantan Ketua MK','img'=>'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/thumb/profdrmohammadmahfudmdshsu512c527ac591b_20130226_061354.jpg','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b'],
        ['from'=>'Oce Madril','content'=>'"Kalau sudah penetapan dari KPK, seharusnya Jokowi berani menarik pencalonan Budi Gunawan.”','jabatan'=>'Direktur Advokasi Pusat Kajian Antikorupsi Universitas Gadjah Mada','img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/5/52/Oce_Madril.jpg/220px-Oce_Madril.jpg','url'=>'http://id.wikipedia.org/wiki/Oce_Madril'],
        ['from'=>'Oegroseno','content'=>'"Ternyata ada proses yang tidak biasa. Tidak ada yang perhatikan mantan Kapolri Pak Sutarman. Lalu Pak Suhardi dicopot (sebagai Kabareskrim), kok segampang ini. Jangan-jangan ini jadi budaya baru." ','jabatan'=>'Mantan WakaPolri','img'=>'http://www.bijaks.net/public/upload/image/politisi/oegroseno52a91aeab1520/badge/142515740357e3e84d6c0fea2dffe34be0777bb6.jpg','url'=>'http://www.bijaks.net/aktor/profile/oegroseno52a91aeab1520'],

    ],
    'narasiCalonBaru'=>'Presiden Joko Widodo dipastikan tidak akan melantik calon Kapolri Komjen Budi Gunawan.
            Presiden sedang menimbang-nimbang calon baru. Komisi Kepolisian Nasional (Kompolnas) sendiri telah
            menyerahkan enam nama calon Kapolri kepada Presiden Joko Widodo untuk dijadikan pertimbangan. Mereka
            adalah : ',
    'calonBaru'=>[
[
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/badrodinhaiti531d430cb8e05/badge/bf1c71e9a9a4da57444be6bd6ee4204289caa29c.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/badrodinhaiti531d430cb8e05',
                    'nama'=>'Komjen Pol Badrodin Haiti ',
                    'lulusan'=>'Akpol 1982',
                    'jabatanTerakhir'=>'Pelaksana Tugas (Plt.) Kapolri',
                    'lahir'=>'Jember, 24 Juli 1958',
                    'karir'=>[
                            ['tahun'=>'2004','jabatan'=>'Kapolda Banten'],
                            ['tahun'=>'2006','jabatan'=>'Kapolda Sulawesi Tengah'],
                            ['tahun'=>'2009','jabatan'=>'Kapolda Sumatera Utara'],
                            ['tahun'=>'2010','jabatan'=>'Kapolda Jawa Timur'],
                            ['tahun'=>'2011','jabatan'=>'Koorsahli Kapolri Mabes Polri'],
                            ['tahun'=>'2014','jabatan'=>'Wakil Kapolri'],
                        ]
                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/jenderaldwipriyatno51edcaa10f4f7/badge/3612e08f167db93f37f0817a668b834e3d5ce8b6.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/jenderaldwipriyatno51edcaa10f4f7',
                    'nama'=>'Komjen Pol Dwi Priyatno',
                    'lulusan'=>'Akpol 1982 ',
                    'jabatanTerakhir'=>'Inspektorat Pengawasan Umum (Irwasum) Polri ',
                    'lahir'=>'Purbalingga, 12 November 1959 ',
                    'karir'=>[
                        ['tahun'=>'2012','jabatan'=>'Sahlisospol Kapolri'],
                        ['tahun'=>'2013','jabatan'=>'Kapolda Jateng '],
                        ['tahun'=>'2014','jabatan'=>'Kapolda Metro Jaya '],
                    ]

                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/pututekobayuseno51bfc7763c3bd/thumb/portrait/7a9cd8745e07040c6a5315836383f4da0a6d9dd9.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/pututekobayuseno51bfc7763c3bd',
                    'nama'=>'Komjen Pol Putut Eko Bayu Seno',
                    'lulusan'=>'Akpol 1984 ',
                    'jabatanTerakhir'=>'Kepala Badan Pemeliharaan Keamanan Polri ',
                    'lahir'=>'Tulungagung, 28 Mei 1961',
                    'karir'=>[
                        ['tahun'=>'2004','jabatan'=>'Pamen Desumdaman Polri (Ajudan Presiden RI) '],
                        ['tahun'=>'2009','jabatan'=>'Wakapolda Metro Jaya'],
                        ['tahun'=>'2011','jabatan'=>'Kapolda Banten'],
                        ['tahun'=>'2011','jabatan'=>'Kapolda Jabar'],
                        ['tahun'=>'2012','jabatan'=>'Kapolda Metro Jaya'],
                    ]

                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/budiwaseso532bc404ac4cb/badge/2017f8db7c9f8d629580329f45ff441622d0adca.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/budiwaseso532bc404ac4cb',
                    'nama'=>'Komjen Pol Budi Waseso',
                    'lulusan'=>'Akademi Kepolisian 1984',
                    'jabatanTerakhir'=>'Kepala Badan Reserse Kriminal Polri ',
                    'lahir'=>'Februari 1960',
                    'karir'=>[
                        ['tahun'=>'2010','jabatan'=>'Kepala Pusat Pengamanan Internal Mabes Polri'],
                        ['tahun'=>'2012','jabatan'=>'Kapolda Gorontalo'],
                        ['tahun'=>'2013','jabatan'=>'Widyaiswara Utama Sespim Polri'],
                        ['tahun'=>'2014','jabatan'=>'Kepala Sekolah Staf dan Pimpinan Tinggi Polri'],
                    ]

                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/komjenpolanangiskandar516a6c028b562/thumb/portrait/komjenpolanangiskandar516a6c028b562_20130515_042440.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/komjenpolanangiskandar516a6c028b562',
                    'nama'=>'Komjen Pol Anang Iskandar',
                    'lulusan'=>'Akpol 1982',
                    'jabatanTerakhir'=>'Kepala Badan Narkotika Nasional (BNN) ',
                    'lahir'=>'Mojokerto, 18 Mei 1958 ',
                    'karir'=>[
                        ['tahun'=>'2006','jabatan'=>'Kapolwiltabes Surabaya'],
                        ['tahun'=>'2011','jabatan'=>'Kapolda Jambi'],
                        ['tahun'=>'2012','jabatan'=>'Kepala Divisi humas Polri'],
                    ]

                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/suhardialius525b93d8efe1a/thumb/portrait/c31c983c6a789d089d5616ecab3b46e8788e4172.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/suhardialius525b93d8efe1a',
                    'nama'=>'Komjen Pol Suhardi Alius',
                    'lulusan'=>'Akpol 1985',
                    'jabatanTerakhir'=>'Sekretaris Utama Lemhannas',
                    'lahir'=>'Jakarta, 10 Mei 1962',
                    'karir'=>[
                        ['tahun'=>'2011','jabatan'=>'Wakapolda Metro Jaya '],
                        ['tahun'=>'2012','jabatan'=>'Kepala Divisi Hubungan Masyarakat (Kadiv Humas) Polri '],
                        ['tahun'=>'2013','jabatan'=>'Kabareskrim'],
                        ['tahun'=>'2013','jabatan'=>'Kapolda Jawa Barat'],
                    ]

                ],


    ],
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url('assets/images/hotpages/cakapolri/peristiwa_polri.png')?>') no-repeat transparent;
        height: 685px;
        margin-bottom: -190px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .boxprofile p, .boxprofile li, .font_kecil {
        font-size: 12px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        background-color: #424040;background: rgba(0, 0, 0, 0.6);
        box-shadow: 5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
    }
    .boxdotted {
        width: 290px;
        margin: 0 22px;
        border-left: 3px solid black;
        display: inline-block;
    }
    .block_red {
        background-color: #720502;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        max-width: 100%;
        margin-top: 5px;
        height: auto;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 180px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 180px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .pic4 {
        max-width: 100px;
        height: auto;
        margin-left: 10px;
        float: left;
    }
    .garis {
        border-top: 1px solid black;
    }
    .boxgray {
        width: 96%;
        border: 9px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 103px;height: 125px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;padding: 0px !important;}
    li.organisasi p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 218px;height: 158px;padding: 0px !important;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }
    .history-arrow {
        background: url('<?php echo base_url("assets/images/hotpages/isis/icon-arrow2.png")?>') no-repeat rgba(0, 0, 0, 0);
        width: 163px;
        height: 32px;
        margin: 0 auto;
    }
    .penyokong {
        width: 100%;
        height: auto;
        display: inline-block;
    }
    .boxpenyokong {
        width: 33.33%;
        height: auto;
        float: left;
    }
    .foto {
        width: 130px;
        height: 130px;
        border: 3px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }
    .uprow {margin-left: 20px;margin-top:10px;border-color: #720502 transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {
        width: 260px;
        height: auto;
        margin-left: 20px;
        margin-top:0px;
        margin-bottom: 10px;
        background-color: #424040;
        background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px #720502;
        border-right:solid 2px #720502;
        border-bottom:solid 2px #720502;
        border-radius: 0 0 5px 5px;
        color: #ffffff;
        z-index: 100;
    }
    .kronologi-title {font-size:14px;font-weight:bold;background-color: #720502;color: #ffffff;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;margin-top:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {line-height: 15px;font-size:12px;}

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <h4 class="list"><a id="intrikcakapolri" style="color: black;"><?php echo $data['NARASI']['title'];?></a></h4>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">ANALISA</a></h4>
                <p><?php echo $data['ANALISA'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">SERANGAN BUAT BUDI GUNAWAN</a></h4>
                <p><?php echo $data['SERANGAN'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="kasushukum" style="color: black;">POLRI SERANG BALIK KPK</a></h4>
                <p class="font_kecil" style="margin-right: 15px;"><?php echo $data['POLRI']['narasi'];?></p>
                <ul class='list2'>
                    <?php
                    foreach ($data['POLRI']['isi'] as $key => $val) { ?>
                        <li style='margin-left: 5px;'>
                            <p class="font_kecil" style="margin-left: 5px;">
                            <?php echo $val['no'];?></p>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
                <p><?php echo $data['POLRI']['bawah'];?></p>     
                <div class="clear"></div><br>

                <h4 class="text-center"><a id="penyokong" style="color: black;">PENYOKONG</a></h4>
                <div class="penyokong">
                    <div class="boxpenyokong" style="height: 100%;">
                        <a href="<?php echo !empty($data['PENYOKONG'][0]['page_id']) ? base_url('aktor/profile/'.$data['PENYOKONG'][0]['page_id']) : '#';?>">
                            <img class="foto" src="<?php echo $data['PENYOKONG'][0]['img'];?>" style="float: right;"/>
                            <img src="<?php echo $data['PENYOKONG'][0]['logo'];?>" style="margin-right: -20px;float: right;border: 1px solid black;max-width: 40px;height: auto;border-radius: 0;border-shadow: 0 0 0 transparent;margin-top: 10px;"/>
                        </a>
                        <div class="clear"></div>
                        <h5 style="float: right;margin-right: 20px;"><?php echo strtoupper($data['PENYOKONG'][0]['jabatan']);?></h5>
                    </div>
                    <div class="boxpenyokong" style="height: 70px;border-bottom: 3px dotted lightgray"></div>
                    <div class="boxpenyokong" style="height: 100%;">
                        <a href="<?php echo !empty($data['PENYOKONG'][1]['page_id']) ? base_url('aktor/profile/'.$data['PENYOKONG'][1]['page_id']) : '#';?>">
                            <img class="foto" src="<?php echo $data['PENYOKONG'][1]['img'];?>" style="float: left;"/>
                            <img src="<?php echo $data['PENYOKONG'][1]['logo'];?>" style="float: left;margin-left: -20px;border: 1px solid black;max-width: 40px;height: auto;border-radius: 0;border-shadow: 0 0 0 transparent;margin-top: 10px;"/>
                        </a>
                        <div class="clear"></div>
                        <h5 style="float: left;margin-left: 5px;"><?php echo strtoupper($data['PENYOKONG'][1]['jabatan']);?></h5>
                        </a>
                    </div>
                </div>

                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <div class="boxgray_green" style="height: auto;padding-bottom: 10px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['partaiPolitik'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">INSTITUSI PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div><br>

                <h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <div class="boxgray_red" style="height: auto;padding-bottom: 10px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['partaiPolitik'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">INSTITUSI PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">TOKOH PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['tokoh'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi" style="height: 100px !important;">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">PEJABAT KEPOLISIAN PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['pejabat'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi" style="height: 100px !important;">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div><br>

                <h4 class="list"><a id="kasushukum" style="color: black;">GELOMBANG PENOLAKAN</a></h4>
                <ul class='list2'>
                    <?php
                    foreach ($data['PENOLAKAN'] as $key => $val) { ?>
                        <li style='font-weight: bold;margin-left: 5px;margin-right: 15px;'><?php echo $val['title'];?></li>
                        <p class="font_kecil" style="margin-left: -13px;margin-right: 15px;"><?php
                        if ($val['img'] != "") { ?>
                            <img src="<?php echo base_url().$val['img'];?>" class='pic3'>
                        <?php } ?>
                        <?php echo $val['no'];?></p>
                        <div class="clear"></div>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="col_kanan">
                <div class="history-arrow"></div>
                <div class="boxprofile white">
                    <h5 class="block_red text-center"><a id="profile" style="color: white;"><?php echo $data['PROFIL']['title'];?></a></h5>
                    <p style="margin-left: 20px;margin-right: 20px;">
                        <?php echo $data['PROFIL']['narasi'];?>
                    </p>
                </div><br>
                <div class="history-arrow"></div>
                <div class="boxprofile white">
                    <h5 class="block_red text-center"><a id="biodata" style="color: white;">BIODATA</a></h5>
                    <table style="margin-left: 20px;margin-right: 20px;">
                        <tr><td>Nama </td><td> : </td><td style="padding-left: 5px;"> Komjen Pol. Drs. Budi Gunawan S.H,MSi,Phd</td></tr>
                        <tr><td>Tempat, Tgl Lahir </td><td> : </td><td style="padding-left: 5px;"> Surakarta, 11 Desember 1959</td></tr>
                        <tr><td>Agama </td><td> : </td><td style="padding-left: 5px;"> Islam</td></tr>
                        <tr><td>Almamater </td><td> : </td><td style="padding-left: 5px;"> Akademi Kepolisian (1983)</td></tr>
                        <tr><td>Pangkat </td><td> : </td><td style="padding-left: 5px;"> Komisaris Jenderal</td></tr>
                    </table>
                </div><br>
                <div class="history-arrow"></div>
                <div class="boxprofile white">
                    <h5 class="block_red text-center"><a id="jabatan" style="color: white;">JABATAN TERAKHIR</a></h5>
                    <p style="margin-left: 20px;font-size: 14px;">KALEMDIKPOL</p>
                </div><br>
                <div class="history-arrow"></div>
                <div class="boxprofile white">
                    <h5 class="block_red text-center"><a id="profilelionair" style="color: white;">KARIR</a></h5>
                    <table style="margin-left: 20px;margin-right: 20px;">
                        <tr><td width="100px">2001 - 2004 </td><td style="padding-left: 5px;"> Ajudan Presiden RI Megawati Soekarnoputri</td></tr>
                        <tr><td>2004 - 2006</td><td style="padding-left: 5px;"> Karobinkar SSDM Polri</td></tr>
                        <tr><td>2006 - 2008</td><td style="padding-left: 5px;"> Kaselapa Lemdiklat Polri</td></tr>
                        <tr><td>2008 - 2009</td><td style="padding-left: 5px;"> Kapolda Jambi</td></tr>
                        <tr><td>2009 - 2010</td><td style="padding-left: 5px;"> Kadiv Binkum Polri</td></tr>
                        <tr><td>2010 - 2012</td><td style="padding-left: 5px;"> Kadiv Propam Polri</td></tr>
                        <tr><td>2012</td><td style="padding-left: 5px;"> Kapolda Bali</td></tr>
                        <tr><td>2012 - Sekarang</td><td style="padding-left: 5px;"> Kepala Lembaga Pendidikan dan Pelatihan Polri</td></tr>
                    </table>
                </div><br>

                <h4 class="list" style="margin-left: 20px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="cakapolri_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="cakapolri_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div>

                <h4 class="list" style="margin-left: 20px;"><a id="kronologi" style="color: black;">KRONOLOGI</a></h4>
                <?php 
                foreach ($data['KRONOLOGI'] as $key => $value) {
                ?>
                <div class="uprow"></div>
                <div class="kronologi">
                    <div class="kronologi-title"><?php echo $value['date'];?></div>
                    <div class="kronologi-info">
                        <p><?php echo $value['content'];?></p>
                    </div>
                </div>
                <div class="clear"></div>
                <?php
                }
                ?><br>

                <h4 class="list" style="margin-left: 20px;"><a id="calonbaru" style="color: black;">CALON BARU KAPOLRI</a></h4>
                <?php 
                foreach($data['calonBaru'] as $val){
                ?>
                <div class="boxdotted black">
                    <p style="margin-left: 10px;font-weight: bold;"><?php echo $val['nama']?></p>
                    <a href="<?php echo $val['url'];?>">
                    <img src="<?php echo $val['img']?>" class="pic4">
                    </a>
                    <table style="margin-left: 5px;float: left;width: 174px;">
                        <tr><td width="50px">Lahir </td><td> : </td><td style="padding-left: 5px;"><?php echo $val['lahir']?></td></tr>
                        <tr><td>Lulusan </td><td> : </td><td style="padding-left: 5px;"><?php echo $val['lulusan']?></td></tr>
                        <tr><td>Jabatan </td><td> : </td><td style="padding-left: 5px;"><?php echo $val['jabatanTerakhir']?></td></tr>
                    </table>
                    <div class="clear"></div>
                    <span style="margin-left: 10px;font-weight: bold;">KARIR</span>
                    <table style="margin-left: 10px;">
                        <?php foreach($val['karir'] as $vk){?>
                        <tr><td width="100px"><span style="margin-left: 40px;"><?php echo $vk['tahun'];?></span></td><td style="padding-left: 5px;"><?php echo $vk['jabatan'];?></td></tr>
                        <?php };?>
                    </table>
                </div>
                <div class="clear"></div><br>
                <?php };?>

            </div>
        </div>
    </div>
</div>
<br/>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>