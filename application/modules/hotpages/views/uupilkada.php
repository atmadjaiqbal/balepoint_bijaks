<style>

    #uupilkada .tagline {background: url('http://www.bijaks.net/assets/images/hotpages/gaza/ico-pal-israel.gif') no-repeat rgba(0, 0, 0, 0); height: 32px;
        position: relative;text-align: left;}
    #uupilkada .tagline .title {padding-left:27px;font-size:22px;margin-left:10px;line-height:32px;}

    #uupilkada .pilkada-word {font-family: arial;font-size: 14px;font-weight: bold;}
    #uupilkada .ul-pilkada {font-size: 14px;list-style: disc;margin-top:-10px;}
    #uupilkada .penggagas {float:left; width: 35px;height: auto;}
    #uupilkada .penggagas img {width: 32px; height: auto;}
    #uupilkada .penggagas-p{float:left;margin-left:5px;margin-top:0px;font-size: 14px;line-height:12px;width: 160px;}

    #uupilkada li.produk-hebat {float: left; margin: 0; vertical-align: top;}
    #uupilkada li.produk-hebat img {width: auto; height: 70px;  padding: 2px !important;}

    #uupilkada li.aksi {float: left;  margin: 0; vertical-align: top;}
    #uupilkada li.aksi img {width: auto; height: 100px;  padding: 2px !important;}

    #uupilkada .box-content {background-color: #ffffff;border: 10px solid #E5E5E5;border-radius:5px;box-shadow:2px 2px 2px #878787;}

    #uupilkada .tokoh {margin: 0; vertical-align: top;border-bottom: solid 2px #cecece;}
    #uupilkada .tokoh a {color: #0000cc; text-decoration: underline;}
    #uupilkada .tokoh a:hover {color: #0000cc; text-decoration: underline;}
    #uupilkada .tokoh img {width: 50px; height: 50px;  padding: 2px !important;}
    #uupilkada .tokoh .name {width:120px;padding-left:5px;padding-top: 5px;font-size: 11px;font-weight:bold;font-family: helvetica, arial, sans-serif;color:#000;line-height: 14px;}
    #uupilkada .tokoh .title {width:120px;padding-left:5px;margin-top:-10px;font-size: 11px;font-family: helvetica, arial, sans-serif;color:#000;line-height: 14px;}
    #uupilkada .tokoh .word {width:140px;padding-left:5px;margin-top:-10px;font-style:italic;font-size: 11px;font-family: helvetica, arial, sans-serif;color:#FF0000;line-height: 14px;}

    #uupilkada li.organisasi {float: left; margin: 2px; padding:5px;vertical-align: top;width: auto;height: 110px;/*border: solid 4px #c4c4c4;*/text-align: center}
    #uupilkada li.organisasi img {width: auto; height: 80px;  padding: 2px !important;}
    #uupilkada li.organisasi p {width: 130px;padding: 2px !important;font-size: 10px;height: auto;line-height: 12px;}

</style>
<br/>
<div class="container" style="margin-top:5px;">
    <div class="sub-header-container">
        <div><img src="<?php echo base_url().'assets/images/hotpages/uupilkada/revisi_uupilkada2.jpg';?>"></div>

        <div id="uupilkada" style="background-color:#ffffff;">
            <div id="gagasan">

               <div style="margin-left:20px;padding-left:100px;margin-top:-300px;">
                   <p style="width:600px;font-family: arial;font-size: 28px;color: #FC0000;line-height:30px;font-weight: bold;text-shadow: 2px 2px #FCF9F9;">GAGASAN MEMILIH KEPALA DAERAH TIDAK DARI RAKYAT LANGSUNG, TAPI OLEH DPRD</p>
                   <!-- ul style="list-style: disc;">
                      <li style="font-size: 20px;margin-left:-10px;color: #FC0000;text-shadow: 2px 2px #FCF9F9;">Gubernur dipilih tidak lagi dipilih langsung oleh rakyat, meliankan oleh DPRD provinsi.</li>
                      <li style="font-size: 20px;margin-left:-10px;color: #FC0000;text-shadow: 2px 2px #FCF9F9;">Wakil gubernur dan wakil bupati/wakil walikota ditunjuk dari lingkungan PNS.</li>
                      <li style="font-size: 20px;margin-left:-10px;color: #FC0000;text-shadow: 2px 2px #FCF9F9;">Pilkada hanya memilih-milih gubernur dan bupati/walikota.</li>
                   </ul -->
               </div>

               <div style="margin-left:20px;margin-top:170px;margin-bottom: 10px;">
                 <div class="tagline"><span id="penggagas" class="title">PENGGAGAS</span></div>
                 <div style="padding-top:10px;height: 890px;margin-left: -20px;">
                     <?php
                     $penggagas = array(
                         0 => ( array ( 'page_id' => 'gamawanfauzi50f4d25561567', 'jabatan' => 'Menteri Dalam Negeri', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/kementriandalamnegeri531c1cbb6af24/badge/b391f936d3d25df881427ebef81eae2180e78f1c.jpg')),
                         1 => ( array ( 'page_id' => 'fadlizon5119d8091e007', 'jabatan' => 'Wakil Ketua Gerindra', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigerakanindonesiarayagerindra5119a4028d14c/thumb/portrait/b60562cd822191aebd4cab750b73c1755724ad62.jpg')),
                         2 => ( array ( 'page_id' => 'tantowiyahya50f3902bf3b64', 'jabatan' => 'Juru Bicara Golkar', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigolongankarya5119aaf1dadef/thumb/portrait/b3185414f9edc4bb66735f9126ee760b7da059bc.jpg')),
                         3 => ( array ( 'page_id' => 'fahrihamzahse5105e57490d09', 'jabatan' => 'Wasekjen PKS', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaikeadilansejahtera5119b06f84fef/thumb/portrait/10caf0ec85e01ba54d6eb68a4a3346d48b19d576.jpg')),
                         4 => ( array ( 'page_id' => 'ahmadyanishmh50f8ff5584dc4', 'jabatan' => 'Anggota Komisi III', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaipersatuanpembangunan5189ad769b227/badge/partaipersatuanpembangunan5189ad769b227_20130508_014534.png')),
                         5 => ( array ( 'page_id' => 'khirabdulhakim525354bc0597a', 'jabatan' => 'Sekretaris Fraksi PKS', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaikeadilansejahtera5119b06f84fef/thumb/portrait/10caf0ec85e01ba54d6eb68a4a3346d48b19d576.jpg')),
                         6 => ( array ( 'page_id' => 'drhmhidayatnurwahidma50f7ae3aba4f8', 'jabatan' => 'Ketua Fraksi PKS', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaikeadilansejahtera5119b06f84fef/thumb/portrait/10caf0ec85e01ba54d6eb68a4a3346d48b19d576.jpg')),
                         7 => ( array ( 'page_id' => 'drsagungunanjarsudarsa50f7c7698eafd', 'jabatan' => 'Anggota Panja', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigolongankarya5119aaf1dadef/thumb/portrait/b3185414f9edc4bb66735f9126ee760b7da059bc.jpg')),
                         8 => ( array ( 'page_id' => 'mromahurmuziystmt50fb7f740d44e', 'jabatan' => 'Sekjen PPP', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaipersatuanpembangunan5189ad769b227/badge/partaipersatuanpembangunan5189ad769b227_20130508_014534.png')),
                         9 => ( array ( 'page_id' => 'aburizalbakrie514662d3c6827', 'jabatan' => 'Ketua Umum Golkar', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigolongankarya5119aaf1dadef/thumb/portrait/b3185414f9edc4bb66735f9126ee760b7da059bc.jpg')),
                         10 => ( array ( 'page_id' => 'prabowosubiyantodjojohadikusumo50c1598f86d91', 'jabatan' => 'Ketua Dewan Pembina Gerindra', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigerakanindonesiarayagerindra5119a4028d14c/thumb/portrait/b60562cd822191aebd4cab750b73c1755724ad62.jpg')),

                         12 => ( array ( 'page_id' => 'agunglaksono50ef681accbd1', 'jabatan' => 'Wakil Ketua Umum Partai Golkar', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigolongankarya5119aaf1dadef/thumb/portrait/b3185414f9edc4bb66735f9126ee760b7da059bc.jpg')),
                         13 => ( array ( 'page_id' => 'akbartanjung503c2d29aaf6b', 'jabatan' => 'Dewan Pembina Golkar', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigolongankarya5119aaf1dadef/thumb/portrait/b3185414f9edc4bb66735f9126ee760b7da059bc.jpg')),
                         14 => ( array ( 'page_id' => 'ridwanhisjam5296fb5b18a2e', 'jabatan' => 'Wakil Sekjen DPP Golkar', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigolongankarya5119aaf1dadef/thumb/portrait/b3185414f9edc4bb66735f9126ee760b7da059bc.jpg')),
                         15 => ( array ( 'page_id' => 'drsabdulhakamnajamsi50f6364555931', 'jabatan' => 'Ketua Panja RUU Pilkada', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaiamanatnasional5119b55ab5fab/thumb/portrait/d0bf6864be53707f815cc069ccdd6e3e86247988.jpg')),
                         16 => ( array ( 'page_id' => 'hermankadir521d613f22e31', 'jabatan' => 'Anggota Panja', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaiamanatnasional5119b55ab5fab/thumb/portrait/d0bf6864be53707f815cc069ccdd6e3e86247988.jpg')),
                         17 => ( array ( 'page_id' => 'hahmadmuzani50f4c6a5e54c6', 'jabatan' => 'Sekretaris Jenderal', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaigerakanindonesiarayagerindra5119a4028d14c/thumb/portrait/b60562cd822191aebd4cab750b73c1755724ad62.jpg')),
                         18 => ( array ( 'page_id' => 'drshrusliridwanmsi50f8b4b0dec99', 'jabatan' => 'Anggota Panja', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaiamanatnasional5119b55ab5fab/thumb/portrait/d0bf6864be53707f815cc069ccdd6e3e86247988.jpg')),
                         19 => ( array ( 'page_id' => 'khatibulumamwiranumhum50f8ccedcaead', 'jabatan' => 'Anggota Panja', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaidemokrat5119a5b44c7e4/badge/partaidemokrat5119a5b44c7e4_20130212_022639.gif')),
                         20 => ( array ( 'page_id' => 'yandrisusanto52e5d5a9a47a8', 'jabatan' => 'Anggota Panja', 'logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaiamanatnasional5119b55ab5fab/thumb/portrait/d0bf6864be53707f815cc069ccdd6e3e86247988.jpg')),

                     );

                     foreach($penggagas as $key=>$val)
                     {
                         $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                         $personarr = json_decode($person, true);
                         $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                         ?>
                         <div style="float: left;width:128px;height: 207px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;margin-bottom:10px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                             <div style="width: 128px;height: 120px;text-align: center;margin-bottom: 5px;">
                                 <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                             </div>
                             <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                                 <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                             </h4>
                             <div style="height: 25px;width: 132px;">
                                 <div style="float:left;margin-left:2px;width: 20px;height: 20px;"><img src="<?php echo $val['logo'];?>"></div>
                                 <p style="float:left;line-height: 12px;width:100px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php echo $val['jabatan']; ?></p>
                             </div>
                         </div>
                     <?php
                     }
                     ?>
                 </div>
               </div>

            </div>

        <?php
        $komengagas = array(
            0 => ( array ( 'ref_name' => 'Gamawan Fauzi', 'ref_title' => 'Menteri Dalam Negeri',
                    'ref_word' => '"Ya kita juga siapkan formulanya. Karena pemilihan langsung/tidak langsung, tidak ada jaminan bahwa itu tidak ada masalah. Yang kita lakukan adalah meminimalisasi masalah". "Kan sudah sering saya sampaikan, saya nggak mendukung yang mana (Pilkada langsung dan Pilkada tidak langsung)". "Kalau andaikata pemilihan langsung, kita ingin perbaiki beberapa hal. Kalau menggunakan perwakilan, jangan juga model perwakilan zaman Orde Baru dulu. Harus ada perbaikan juga,"',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/gamawanfauzi50f4d25561567',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/gamawanfauzi50f4d25561567/badge/0fdd49dcfde4650226d09c3cfdb2a74100a48d70.jpg')),

            1 => ( array ( 'ref_name' => 'Fadli Zon', 'ref_title' => 'Wakil Ketua Gerindra',
                    'ref_word' => 'Banyak yang mendukung kita. Hampir semua rakyat koalisi Merah Putih mendukung dan juga masyarakat yang lain.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/fadlizon5119d8091e007/badge/11bfa198ef6e5eabc035f5e8e858487df7e7a676.jpg')),

            2 => ( array ( 'ref_name' => 'Hidayat Nur Wahid', 'ref_title' => 'Ketua Fraksi PKS di DPR',
                    'ref_word' => 'Rakyat kan sudah memilih wakil artinya wakil dipilih rakyat, kemudian melaksanakan peran sebagai wakil untuk memilih pimpinan daerah.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/drhmhidayatnurwahidma50f7ae3aba4f8',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/drhmhidayatnurwahidma50f7ae3aba4f8/badge/06f6dee45a70caf41e5d582bd270960da2246df6.jpg')),

            3 => ( array ( 'ref_name' => 'Abu Rizal Bakrie', 'ref_title' => 'Ketua Umum Golkar',
                    'ref_word' => '"RUU (Pilkada) yang sekarang dibicarakan, harus diubah (disahkan), karena setelah melakukan eksperimen yang berani di awal tahun reformasi. Kita melihat pelaksanaanya sudah banyak terlalu menyimpang ke kanan, sifatnya liberal,"',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/aburizalbakrie514662d3c6827',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/aburizalbakrie514662d3c6827/badge/7a48fbe943d7496da58b0c39d2748f7bc302a7dc.jpg')),

            4 => ( array ( 'ref_name' => 'Martin Hutabarat', 'ref_title' => 'Anggota Dewan Pembina Partai Gerindra',
                    'ref_word' => '"Semangat pemberantasan korupsi dan tujuan kesejahteraan rakyat, pilkada langsung itu lebih banyak mudaratnya,"',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/martinhutabarat50f8f8a4765cd',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/martinhutabarat50f8f8a4765cd/thumb/portrait/c9bf0d10a4824719bd5a2757671156d34ff1d5cf.jpg')),

            5 => ( array ( 'ref_name' => 'Mahfud MD', 'ref_title' => 'Mantan Ketua Mahkamah Konstitusi',
                    'ref_word' => '"Berdasar data saya sbg Katua MK sy memang membuat neraca manfaat dan mudharat dari sengketa2 di MK. Sy mencatat memang banyak mudarat. Waktu itu ide mengevaluasi pilkadasung itu mendapat banyak dukungan. Tp parpol2 yg skrang ada di KMP, waktu itu, menolak perubahan ke DPRD."',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/thumb/portrait/1f0f6f824c7f2061975fc4308b454b9d6264036e.jpg')),

            6 => ( array ( 'ref_name' => 'Amien Rais', 'ref_title' => 'Mantan Ketua Mahkamah Konstitusi',
                    'ref_word' => '"Saya termasuk yang yakin sekali dulu termasuk pemilihan langsung dulu itu politik uang bisa diatasi karena tidak mungkin keluhan 100 juta lebih penduduk bisa diamankan dengan uang,"',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/muhammadamienrais51d645f7362c1',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/muhammadamienrais51d645f7362c1/thumb/portrait/543bdb0918e3a32d4796390e4c3bdf32f06f3e16.jpg')),

            7 => ( array ( 'ref_name' => 'Idrus Marham', 'ref_title' => 'Sekjen Partai Golkar',
                    'ref_word' => '"Akhirnya, Koalisi Merah Putih menegaskan pilkada melalui DPRD pilihan terbaik,"',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/idrusmarham51132ad8423e1',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/idrusmarham51132ad8423e1/thumb/portrait/880a6da7780cfc6439a1bb6c841b54363c44a6ee.jpg')),

            8 => ( array ( 'ref_name' => 'Hashim Djojohadikusumo', 'ref_title' => 'Wakil Ketua Dewan Pembina Gerindra ',
                    'ref_word' => '"Di Amerika Serikat, pemilihan presiden dilaksanakan secara tidak langsung. Presiden Amerika Serikat tidak dipilih langsung oleh rakyatnya. Yang memilih (presiden) adalah 540 elector. Saya tahu persis konstitusi Amerika Serikat,"',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/hashimdjojohadikusumo51b935c640e85',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/hashimdjojohadikusumo51b935c640e85/thumb/portrait/3373f0981d7dda125ce41ababce8b448bbaaac3b.jpg')),

            9 => ( array ( 'ref_name' => 'Said Aqil', 'ref_title' => 'Ketua Umum PBNU',
                    'ref_word' => '"Ini merupakan keputusan para ulama sepuh yang dengan jernih menilai mudharat (dampak buruk) pilkada langsung sudah jelas, sementara manfaatnya belum tentu tercapai."',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/saidaqilsiradj5343c56457ee7',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/saidaqilsiradj5343c56457ee7/thumb/portrait/2f6bd1c33002ff3718a332c949b3635e0bf6093d.jpg')),
        );

        ?>
            <div id="komentar-dukung" style="height: 560px;-webkit-column-count: 5;-webkit-column-gap: 2px;-moz-column-count: 5;-moz-column-gap: 2px;column-count: 5;column-gap: 2px;">
                <?php
                foreach($komengagas as $key => $val)
                {
                    ?>
                    <div class="tokoh" style="width: 180px;height:auto;margin-left: 10px;margin-right: 10px;margin-top:5px;display: inline-block;padding-top:1px;padding-bottom: 1px;vertical-align: top;
                                                   -webkit-column-break-inside: avoid;-moz-column-break-inside: avoid;column-break-inside: avoid;">
                        <div style="width: 180px;height: 55px;">
                            <div style="float:left;height:55px;">
                                <?php if(!empty($val['ref_url'])) { ?><a href="< ?php echo $val['ref_url'];?>"><?php } ?>
                                    <img src='<?php echo $val['ref_img'];?>'
                                         data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='<?php echo $val['ref_name']; ?>'/>
                                    <div style=""></div>
                                    <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                            </div>
                            <div style="float:left;height:55px;">
                                <?php if(!empty($val['ref_url'])) { ?><a href="<?php echo $val['ref_url'];?>"><?php } ?>
                                    <p class="name"><?php echo $val['ref_name']; ?></p>
                                    <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                <p class="title"><?php echo $val['ref_title']; ?></p>
                            </div>
                        </div>
                        <div style="float:none;margin-top:15px;">
                            <p class="word"><?php echo $val['ref_word']; ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="tagline"><span id="alasan" class="title">ALASAN</span></div>
            <div style="margin-bottom: 10px;padding:10px;">
                <div style="">
                    <p class="pilkada-word" style="color: #000000;">Alasan Pendukung :</p>
                    <ul class="ul-pilkada">
                      <li style="font-size: 14px;margin-left:-10px;">Pilkada langsung menelan biaya besar</li>
                      <li style="font-size: 14px;margin-left:-10px;">Sejak 2004, pilkada langsung sudah mengantarkan 290 orang yang bermasalah dengan hukum ke kursi kekuasaan</li>
                      <li style="font-size: 14px;margin-left:-10px;">Kementerian Luar Negeri mencatat sudah lebih dari 300 orang kepala daerah terpilih sejak 2004 terjerat kasus korupsi</li>
                      <li style="font-size: 14px;margin-left:-10px;">Tidak menjamin tercapainya kualitas kepala daerah yang baik</li>
                      <li style="font-size: 14px;margin-left:-10px;">Sangat rawan gesekan horizontal</li>
                      <li style="font-size: 14px;margin-left:-10px;">Dianggap mubazir dan tidak efisien</li>
                    </ul>
                </div>
            </div>

            <div class="tagline"><span id="analisa" class="title">ANALISA</span></div>
            <div style="margin-bottom: 10px;padding:10px;">
                <p style="font-size: 18px;color: #880F04;font-weight: bold;">Kenapa gagasan ini di cetuskan</p>
                <p style="width:920px;font-family: arial;font-size: 18px;color: #880F04;font-weight: bold;">Kecenderungan kader Demokrat, PKS, Gerindra, dkk untuk tidak dapat bersaing di mata rakyat.</p>
                <ul class="ul-pilkada">
                    <li style="font-size: 14px;margin-left:-10px;">Demokrat, PKS, PPP citranya terpuruk karena kasus korupsi</li>
                    <li style="font-size: 14px;margin-left:-10px;">Minim nya koalisi merah putih (Gerindra, dkk) akan tokoh-tokoh yg berprestasi di mata rakyat</li>
                    <br/>
                    <li style="font-size: 14px;margin-left:-10px;">Demokrat berubah haluan pro pilkada langsung, demi pencitraan ketua umumnya.</li>
                </ul>

                <!-- p style="width:920px;font-family: arial;font-size: 18px;color: #880F04;font-weight: bold;">Jika disahkan pada 25 September 2014 kelak, pilkada akan berlaku serentak di 202 kabupaten/kota provinsi mulai 2015. Menurut Depdagri, Pilkada 2015 butuh dana 70 triliun.</p>
                <p class="pilkada-word" style="color:  #880F04;">Ketakutan PARPOL pendukung :</p>
                <ul class="ul-pilkada">
                    <li style="font-size: 14px;margin-left:-10px;">Ketakutan pertama adalah takut dekat dengan pemilih.</li>
                    <li style="font-size: 14px;margin-left:-10px;">Ketakutan kedua adalah partai politik takut dievaluasi.</li>
                    <li style="font-size: 14px;margin-left:-10px;">Ketakutan ketiga adalah partai politik takut menjadi partai terbuka.</li>
                    <li style="font-size: 14px;margin-left:-10px;">Ketakutan keempat adalah partai politik takut dipantau.</li>
                    <li style="font-size: 14px;margin-left:-10px;">Selanjutnya adalah partai Pendukung Pilkada tak langsung pada dasarnya tidak memiliki kader.</li>
                </ul -->
                <!-- p style="font-size: 18px;font-weight: bold;">KONTRADIKSI</p>
                <ul class="ul-pilkada" style="margin-top:-10px;">
                    <li style="font-size: 14px;margin-left:-10px;">RUU Pilkada dianggap bertentangan dengan Undang-Undang MPR, DPR, DPRD dan DPD (UU MD3) yang baru disahkan DPR.</li>
                    <li style="font-size: 14px;margin-left:-10px;">Kemunduran Proses Demokrasi.</li>
                    <li style="font-size: 14px;margin-left:-10px;">Indonesia Menganut system Presidensial.</li>
                    <li style="font-size: 14px;margin-left:-10px;">Meniadakan hak setiap warga negara untuk berpartisipasi dalam.</li>
                    <li style="font-size: 14px;margin-left:-10px;">DPR dan DPRD tugasnya adalah untuk membuat aturan, mengawasi pemerintahan dan menyusun budget. Bukan mewakili dalam memilih pemimpin.</li>
                    <li style="font-size: 14px;margin-left:-10px;">DPR sudah punya banyak catatan buruk untuk merampas kewenangan partisipasi masyarakat.</li>
                    <li style="font-size: 14px;margin-left:-10px;">RUU Pilkada didukung oleh partai dan fraksi Koalisi Merah Putih yang kalah dalam Pilpres 2014.Koalisi ini ingin menjadikan RUU Pilkada sebagai balas dendam kekalahan dengan cara menguasai kepala-kepala daerah.</li>
                </ul -->
            </div>

            <div class="tagline"><span id="produk" class="title">Produk Hebat Pilkada Langsung</span></div>
            <div style="margin-bottom: 10px;padding:10px;">
                <div style="padding-top:10px;height: 215px;margin-left:-29px;">
                    <?php
                    $prohebat = array(
                        0 => ( array ( 'page_id' => 'irjokowidodo50ee1dee5bf19', 'jabatan' => 'Gubernur DKI Jakarta')),
                        1 => ( array ( 'page_id' => 'basukitjahajapurnama50f600df48ac5', 'jabatan' => 'Wakil Gubernur DKI Jakarta')),
                        2 => ( array ( 'page_id' => 'trirismaharini52db45dd476f9', 'jabatan' => 'Walikota Surabaya')),
                        3 => ( array ( 'page_id' => 'ridwankamil51c7d138bb02b', 'jabatan' => 'Walikota Bandung')),
                        4 => ( array ( 'page_id' => 'nurdinabdullah51d274694ec0c', 'jabatan' => 'Bupati Bantaeng')),
                        5 => ( array ( 'page_id' => 'bimaarya5223e9a67da2f', 'jabatan' => 'Walikota Bogor')),
                    );

                    foreach($prohebat as $key=>$val)
                    {
                        $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                        $personarr = json_decode($person, true);
                        $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                        ?>
                        <div style="float: left;width:128px;height: 200px; background-color: #EEEEF4;border-radius: 10px;margin-left:20px;border: solid 4px #cecef4;box-shadow:2px 2px 2px #878787;">
                            <div style="width: 128px;height: 120px;text-align: center;margin-bottom: 5px;">
                                <img style=" background-color: #bbbbbb;width: 128px;height: 128px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                            </div>
                            <h4 style="line-height: 12px;height:30px;padding-left:5px;padding-right:5px;margin-bottom:5px;">
                                <a style="font-size: 12px !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                            </h4>
                            <p style="line-height: 12px;font-size:12px;font-weight:bold;padding-left:5px;padding-right:5px;margin-bottom:5px;"><?php echo $val['jabatan']; ?></p>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>

            <div class="tagline"><span id="penolak" class="title">PENOLAK</span></div>
            <div style="margin-bottom: 10px;padding:10px;">
                <div style="padding-top:10px;height: 755px;">
                    <?php
                    $org_tolak = array(
                        0 => ( array ( 'org_name' => 'APKASI', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logo-APKASI-1-300x300.jpg')),
                        1 => ( array ( 'org_name' => 'APEKSI', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/Logo_Apeksi.png')),
                        2 => ( array ( 'org_name' => 'ICW', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logo_icw.jpg')),
                        3 => ( array ( 'org_name' => 'Partai Demokrasi Indonesia Perjuangan', 'org_logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg')),
                        4 => ( array ( 'org_name' => 'Partai Kebangkitan Bangsa', 'org_logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaikebangkitanbangsa5119b257621a4/badge/6a82dd0643ed4bfabc5fb872396961cb96402fb5.jpg')),
                        5 => ( array ( 'org_name' => 'Partai Nurani Rakyat', 'org_logo' => 'http://www.bijaks.net/public/upload/image/politisi/partaihatinuranirakyathanura5119a1cb0fdc1/badge/partaihatinuranirakyathanura5119a1cb0fdc1_20130212_020239.jpg')),
                        6 => ( array ( 'org_name' => 'Nasional Demokrat', 'org_logo' => 'http://www.bijaks.net/public/upload/image/politisi/nasdem5119b72a0ea62/thumb/portrait/2c9fdf0f3e55f6cc61b6b11a45224e324abecbb5.jpg')),
                        7 => ( array ( 'org_name' => 'The Habibie Center', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logothc.gif')),
                        8 => ( array ( 'org_name' => 'Jaringan Pendidikan & Pemilih Untuk Rakyat', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/jppr.jpg')),
                        9 => ( array ( 'org_name' => 'KPP Indonesia', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/kpp_indo.jpg')),
                        10 => ( array ( 'org_name' => 'Pusat Studi Hukum & Kebijakan Indonesia', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/pshk-logo2.png')),
                        11 => ( array ( 'org_name' => 'Pusat Kajian Politik FISIP UI', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/puskapol.jpg')),
                        12 => ( array ( 'org_name' => 'Pusat Telaah & Informasi Regional', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/PATTIRO.jpg')),
                        13 => ( array ( 'org_name' => 'YAPPIKA', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/yappika.jpg')),
                        14 => ( array ( 'org_name' => 'Populi Center', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/95ASBOVI.jpeg')),
                        15 => ( array ( 'org_name' => 'Komite Pemantauan Pelaksanaan Otonomi Daerah', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/Logo-KPPOD.jpg')),
                        16 => ( array ( 'org_name' => 'Komite Pemantau Legislatif', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logo-kopel2.jpg')),
                        17 => ( array ( 'org_name' => 'LSM Perludem', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/perludem-rentetan-kebakaran.2345.jpg')),
                        18 => ( array ( 'org_name' => 'Indonesian Parliamentary Center', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/Logo-IPC-transparan-kecil.jpg')),
                        19 => ( array ( 'org_name' => 'Rumah Kebangsaan', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/LogoRK-e1372233390772.png')),
                        20 => ( array ( 'org_name' => 'Satjipto Rahardjo Institut', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/Satjipto-Rahardjo-Institute.png')),
                        21 => ( array ( 'org_name' => 'Masyarakat Tranparancy Aceh', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/MaTA.jpg')),
                        22 => ( array ( 'org_name' => 'Dewan Guru Besar FE Unhas', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/jOPvPTqh5Q_universitas-hasanuddin.gif')),
                        23 => ( array ( 'org_name' => 'Malang Corruption Watch', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/mcw.jpg')),
                        24 => ( array ( 'org_name' => 'Pusako FH Universitas Andalas', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/logo-unand-bagus.jpg')),
                        25 => ( array ( 'org_name' => 'BEM FH Undip', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/lBEqlfjn.jpeg')),
                        26 => ( array ( 'org_name' => 'Permahi Semarang', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/permahi_semarang.JPG')),
                        27 => ( array ( 'org_name' => 'Dewa Orga Semarang', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/dewa_orga.jpg')),
                        28 => ( array ( 'org_name' => 'Komunitas Payung Semarang', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/1001834_346329945495209_516066654_n.jpg')),
                        29 => ( array ( 'org_name' => 'Dewan Perwakilan Daerah RI', 'org_logo' => base_url().'assets/images/hotpages/uupilkada/dpd-ri.png')),
                    );

                    ?>

                    <ul style="margin-left:-20px;">
                    <?php
                    foreach($org_tolak as $key => $val)
                    {
                    ?>
                        <li class="organisasi">
                            <img src="<?php echo $val['org_logo'];?>" alt="<?php echo $val['org_name']; ?>"/>
                            <p><?php echo strtoupper($val['org_name']);?></p>
                        </li>
                    <?php
                    }
                    ?>
                    </ul>

                </div>
            </div>

        <?php
        $komengagas = array(
            0 => ( array ( 'ref_name' => 'Richard Louhenapessy', 'ref_title' => 'Walikota Ambon',
                    'ref_word' => 'Secara pribadi saya tidak setuju kalau mekanisme pemilihan kepala daerah itu dikembalikan ke DPRD.',
                    'ref_url' => '',
                    'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/uupilkada/20091126081126.jpg')),

            1 => ( array ( 'ref_name' => 'Awang Faroek Ishak', 'ref_title' => 'Gubernur Kalimantan Timur',
                    'ref_word' => 'Saya setuju untuk pemilihan gubernur saya setuju dipilih langsung, daripada di tangan DPRD.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/awangfaroekishak525f69584bf91',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/awangfaroekishak525f69584bf91/badge/0708bf8803976e4af0de3153f4f4cab3c789a303.jpg')),

            2 => ( array ( 'ref_name' => 'Basuki Tjahja Purnama', 'ref_title' => 'Wakil Gubernur DKI Jakarta',
                    'ref_word' => 'Saya pikir, kalau sampai ini (pengesahan RUU Pilkada) dilakukan, mungkin saya akan keluar dari partai politik saja, ngapain main ke partai politik.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/basukitjahajapurnama50f600df48ac5',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/basukitjahajapurnama50f600df48ac5/badge/296098004222673583d52113cd2534df11ea40a9.jpg')),

            3 => ( array ( 'ref_name' => 'Arief Wibowo', 'ref_title' => 'Fraksi PDI Perjuangan',
                    'ref_word' => 'Ada kepentingan politik pragmatis luar biasa. Ini dampak politik dari Pilpres.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/arifwibowo50f8c2f747be3',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/arifwibowo50f8c2f747be3/badge/b83d74bee2a15dcb630933635d26ac77904abd9c.jpg')),

            4 => ( array ( 'ref_name' => 'Faisal Basri', 'ref_title' => 'Ekonom UI',
                    'ref_word' => 'Gak ada hubungan sama hemat-hematan, dengan ekonomi. Kalau mau efisiensi ada caranya, bukan begini. Jadi partai-partai yang ngotot RUU Pilkada itu bangsat. Mereka itu 63 persen. Jadi setan pun dicalonkan ya otomatis menang kalau dipilih DPRD.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/faisalbasri51b7f1e3d9a25',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/faisalbasri51b7f1e3d9a25/badge/2e75d379949d9e81740e3c7d692df07e48a4d453.jpg')),

            5 => ( array ( 'ref_name' => 'Siti Zuhro', 'ref_title' => 'Pengamat Politik',
                    'ref_word' => 'Logo dilepaskan dulu pakai saja jaket Republik Indonesia, jadi berpikirnya utuh menuju satu titik, yaitu agar otonomi daerah berhasil dan mampu menciptakan cluster baru pembangunan ekonomi dari daerah, kalau daerah maju pilkadanya mampu memilih seorang kepala daerah yang amanah.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/sitizuhro53071802b0bee',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/sitizuhro53071802b0bee/badge/cf970fdf9293c266644965eb15478e5070820180.jpg')),

            6 => ( array ( 'ref_name' => 'Susilo Bambang Yudoyono', 'ref_title' => 'Presiden',
                    'ref_word' => 'Kalau saya pribadi yang telah memimpin selama 10 tahun ini, kalau dulu kita ingin melaksanakan pemilihan secara langsung, ya itulah yang mestinya kita jaga. Tapi tidak boleh ya sudah itu saja, ada kok kelemahannya. Itu yang kita perbaiki secara fundamental. Itu yang Demokrat sedang pikirkan sekarang ini mudah-mudahan satu dua hari ini kami memiliki posisi yang tepat.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/susilobambangyudhoyono50ee672eb35c5',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/susilobambangyudhoyono50ee672eb35c5/thumb/portrait/39989d0890a536ffa7c758d668a09dbbb0b48a69.jpg')),

            7 => ( array ( 'ref_name' => 'Charles Honoris', 'ref_title' => 'Politikus muda PDI Perjuangan',
                    'ref_word' => 'Upaya Koalisi Merah Putih menghapus Pilkada langsung adalah bentuk pengkhianatan terhadap rakyat dan demokrasi, apalagi elit politik yang ingin balas dendam atas kekalahan pada Pilpres. Mereka kok teganya mengkhianati kehendak rakyat dengan cara-cara seperti itu.',
                    'ref_url' => 'http://www.bijaks.net/aktor/profile/charleshonoris526c6611c3e5c',
                    'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/charleshonoris526c6611c3e5c/thumb/portrait/724290b800e63004838a2e19ce6abf11a264acb7.JPG')),

        );

        ?>
            <div class="tagline"><span id="komen-tolak" class="title">KOMENTAR PENOLAK</span></div>
            <div style="height: 460px;-webkit-column-count: 5;-webkit-column-gap: 2px;-moz-column-count: 5;-moz-column-gap: 2px;column-count: 5;column-gap: 2px;">
                <?php
                foreach($komengagas as $key => $val)
                {
                    ?>
                    <div class="tokoh" style="width: 180px;height:auto;margin-left: 10px;margin-right: 10px;margin-top:5px;display: inline-block;padding-top:1px;padding-bottom: 1px;vertical-align: top;
                                                       -webkit-column-break-inside: avoid;-moz-column-break-inside: avoid;column-break-inside: avoid;">
                        <div style="width: 180px;height: 55px;">
                            <div style="float:left;height:55px;">
                                <?php if(!empty($val['ref_url'])) { ?><a href="< ?php echo $val['ref_url'];?>"><?php } ?>
                                    <img src='<?php echo $val['ref_img'];?>'
                                         data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='<?php echo $val['ref_name']; ?>'/>
                                    <div style=""></div>
                                    <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                            </div>
                            <div style="float:left;height:55px;">
                                <?php if(!empty($val['ref_url'])) { ?><a href="<?php echo $val['ref_url'];?>"><?php } ?>
                                    <p class="name"><?php echo $val['ref_name']; ?></p>
                                    <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                <p class="title"><?php echo $val['ref_title']; ?></p>
                            </div>
                        </div>
                        <div style="float:none;margin-top:15px;">
                            <p class="word"><?php echo $val['ref_word']; ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>

<?php
$komentar = array(
    0 => ( array ( 'ref_name' => 'Richard Louhenapessy', 'ref_title' => 'Walikota Ambon',
            'ref_word' => 'Secara pribadi saya tidak setuju kalau mekanisme pemilihan kepala daerah itu dikembalikan ke DPRD.',
            'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/uupilkada/20091126081126.jpg')),

    1 => ( array ( 'ref_name' => 'Awang Faroek Ishak', 'ref_title' => 'Gubernur Kalimantan Timur',
            'ref_word' => 'Saya setuju untuk pemilihan gubernur saya setuju dipilih langsung, daripada di tangan DPRD.',
            'ref_url' => 'http://www.bijaks.net/aktor/profile/awangfaroekishak525f69584bf91',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/awangfaroekishak525f69584bf91/badge/0708bf8803976e4af0de3153f4f4cab3c789a303.jpg')),

    2 => ( array ( 'ref_name' => 'Basuki Tjahja Purnama', 'ref_title' => 'Wakil Gubernur DKI Jakarta',
            'ref_word' => 'Saya pikir, kalau sampai ini (pengesahan RUU Pilkada) dilakukan, mungkin saya akan keluar dari partai politik saja, ngapain main ke partai politik.',
            'ref_url' => 'http://www.bijaks.net/aktor/profile/basukitjahajapurnama50f600df48ac5',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/basukitjahajapurnama50f600df48ac5/badge/296098004222673583d52113cd2534df11ea40a9.jpg')),

    3 => ( array ( 'ref_name' => 'Arief Wibowo', 'ref_title' => 'Fraksi PDI Perjuangan',
            'ref_word' => 'Ada kepentingan politik pragmatis luar biasa. Ini dampak politik dari Pilpres.',
            'ref_url' => 'http://www.bijaks.net/aktor/profile/arifwibowo50f8c2f747be3',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/arifwibowo50f8c2f747be3/badge/b83d74bee2a15dcb630933635d26ac77904abd9c.jpg')),

    4 => ( array ( 'ref_name' => 'Faisal Basri', 'ref_title' => 'Ekonom UI',
            'ref_word' => 'Gak ada hubungan sama hemat-hematan, dengan ekonomi. Kalau mau efisiensi ada caranya, bukan begini.',
            'ref_url' => 'http://www.bijaks.net/aktor/profile/faisalbasri51b7f1e3d9a25',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/faisalbasri51b7f1e3d9a25/badge/2e75d379949d9e81740e3c7d692df07e48a4d453.jpg')),

    5 => ( array ( 'ref_name' => 'Siti Zuhro', 'ref_title' => 'Pengamat Politik',
            'ref_word' => 'Logo dilepaskan dulu pakai saja jaket Republik Indonesia, jadi berpikirnya utuh menuju satu titik, yaitu agar otonomi daerah berhasil dan mampu menciptakan cluster baru pembangunan ekonomi dari daerah, kalau daerah maju pilkadanya mampu memilih seorang kepala daerah yang amanah.',
            'ref_url' => 'http://www.bijaks.net/aktor/profile/sitizuhro53071802b0bee',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/sitizuhro53071802b0bee/badge/cf970fdf9293c266644965eb15478e5070820180.jpg')),

    6 => ( array ( 'ref_name' => 'Fadli Zon', 'ref_title' => '',
            'ref_word' => 'Banyak yang mendukung kita. Hampir semua rakyat koalisi Merah Putih mendukung dan juga masyarakat yang lain.',
            'ref_url' => 'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/fadlizon5119d8091e007/badge/11bfa198ef6e5eabc035f5e8e858487df7e7a676.jpg')),

    7 => ( array ( 'ref_name' => 'Susilo Bambang Yudoyono', 'ref_title' => 'Presiden',
            'ref_word' => 'Kalau saya pribadi yang telah memimpin selama 10 tahun ini, kalau dulu kita ingin melaksanakan pemilihan secara langsung, ya itulah yang mestinya kita jaga. Tapi tidak boleh ya sudah itu saja, ada kok kelemahannya. Itu yang kita perbaiki secara fundamental. Itu yang Demokrat sedang pikirkan sekarang ini mudah-mudahan satu dua hari ini kami memiliki posisi yang tepat.',
            'ref_url' => 'http://www.bijaks.net/aktor/profile/susilobambangyudhoyono50ee672eb35c5',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/susilobambangyudhoyono50ee672eb35c5/thumb/portrait/39989d0890a536ffa7c758d668a09dbbb0b48a69.jpg')),

);

?>
            <div style="margin-bottom: 10px;">
                <div class="span8">
                    <div class="tagline"><span id="aksi-tolak" class="title">AKSI PENOLAKAN</span></div>
                    <div class="box-content" style="margin-bottom: 10px;padding:10px;height: 635px;">
                        <ul style="height: 520px;">
                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_01">
                                  <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/1329269demo-RUU780x390.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_01" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/1329269demo-RUU780x390.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_02">
                                  <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/FOTO-RUU-PILKADA-_-Demonstran-Penolak-RUU-Pilkada-Bakar-Bakar.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_02" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/FOTO-RUU-PILKADA-_-Demonstran-Penolak-RUU-Pilkada-Bakar-Bakar.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_03">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/satu_harapan.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_03" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/satu_harapan.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_04">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/massa_bakar_bakar.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_04" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/massa_bakar_bakar.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_05">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/95C38360-80F8-4290-907F-BFCC5232113A_w640_s.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_05" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/95C38360-80F8-4290-907F-BFCC5232113A_w640_s.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_06">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/2014914093536-pilkada.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_06" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/2014914093536-pilkada.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_07">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/Komdi.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_07" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/Komdi.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_08">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/1410682755.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_08" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/1410682755.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_09">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/2014-09-16-3970_3_pilkadarommy.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_09" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/2014-09-16-3970_3_pilkadarommy.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_10">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/tolak-ruu-pilkada-mahasiswa-demo-di-balai-kota-malang-tCr.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_10" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/tolak-ruu-pilkada-mahasiswa-demo-di-balai-kota-malang-tCr.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_11">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/demo-pdip.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_11" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/demo-pdip.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_12">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/uploads--1--2014--09--60364-pmii-demo-surabaya-tegaskan-tolak-ruu-pilkada.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_12" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/uploads--1--2014--09--60364-pmii-demo-surabaya-tegaskan-tolak-ruu-pilkada.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_13">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/mati_demokrasi.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_13" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/mati_demokrasi.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_14">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/lsg1.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_14" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/lsg1.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li class="aksi">
                                <a href="#" data-toggle="modal" data-target="#uudemo_15">
                                   <img src="<?php echo base_url().'assets/images/hotpages/uupilkada/demo_ruu.jpg'; ?>" alt='aksi_brutal'/>
                                </a>
                                <div class="modal fade" id="uudemo_15" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel">Aksi Penolakan RUU Pilkada</h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src='<?php echo base_url().'assets/images/hotpages/uupilkada/demo_ruu.jpg'; ?>' style="height: 250px;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>


                        </ul>
                        <p style="font-size: 14px;margin-left:25px;">Gerakan Rakyat Untuk Pilkada Langsung. Massa dari Gerakan Rakyat Untuk Pilkada Langsung membakar ban bekas saat menggelar unjuk rasa di depan Gedung DPRD Provinsi Jawa Barat, Jalan Diponegoro, Kota Bandung.</p>
                    </div>
                </div>

                <div class="span4">
                    <div id="berita" class="tagline"><span class="title">BERITA TERKAIT</span></div>
                    <div style="width: 300px;height:auto;">
                        <div style="padding-left:0px;background-color: #E5E5E5;">
                            <div id="newsuu_container" data-tipe="1" data-page='1' style="height: auto;margin-bottom: 9px !important;"></div>
                            <div class="row-fluid" style="margin-bottom: 2px;">
                                <div class="span6 text-left">
                                    <a id="newsuu_loadmore" data-tipe="1" class="btn btn-mini" >10 Berikutnya</a>
                                </div>
                                <div class="span6 text-right">
                                    <!-- a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a -->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>





