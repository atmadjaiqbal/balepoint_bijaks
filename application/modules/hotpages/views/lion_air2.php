<?php 
$data = [
    'NARASI'=>[
        'title'=>'LION AIR : POLITIK &quot;SINGA&quot; OMPONG',
        'narasi'=>'<img src="http://www.bijaks.net/assets/images/hotpages/lionair/ompong1.jpg" class="pic">Sejak Rabu, 18 Februari sebanyak 54 penerbangan di bawah manajemen Lion Air Indonesia mengalami penundaan terbang. Ribuan penumpang telantar lantaran tak ada kejelasan dari pihak Lion Air kapan mereka bisa terbang. Dalam kasus ini, para penumpang telah dirugikan oleh buruknya managemen maskapai milik salah satu anggota Dewan Pertimbangan Presiden (Wantimpres), Rusdi Kirana itu. 
                </p><p>Mantan Sekretaris Kementerian Badan Usaha Milik Negara (BUMN), Said Didu menuding Pemerintah tidak tegas dalam menyikapi kasus serius yang dilakukan Lion Air. Said menduga Lion Air diperlakukan istimewa oleh pemerintah. Dugaan lainnya, Lion Air memiliki pengaruh kuat di bandara Halim Perdana Kusuma. 
                </p><p><img src="http://www.bijaks.net/assets/images/hotpages/lionair/ompong2.jpg" class="pic2">Delay kali ini bukan pertama kalinya yang dilakukan Lion Air. Yayasan Lembaga Konsumen Indonesia mencatat bahwa Lion Air adalah maskapai paling banyak dikeluhkan masyarakat. Sepanjang 2014 saja YLKI menerima sebanyak 24 aduan terkait buruknya pelayanan Lion Air terhadap penumpangnya. 
                </p><p>Sementara itu, tak ada penjelasan memuaskan dari pihak maskapai kepada penumpang. Permohonan maaf resmi Lion Air baru disampaikan Jum’at siang, 20 Februari, setelah penumpang ditelantarkan selama tiga hari. Secara sengaja Lion Air membiarkan kisruh ini berlarut-larut hingga tiga hari. 
                </p><p><img src="http://www.bijaks.net/assets/images/hotpages/lionair/ompong3.jpg" class="pic">Hak konsumen pun belum sepenuhnya dipenuhi. Bahkan inisiatif konpensasi dan pengembalian uang tiket (refund) penumpang muncul dari PT Angkasa Pura II, bukan dari Lion Air. Bahkan semua kompensasi dan menggunakan uang PT Angkasa Pura II.  
                </p><p>Menyikapi persoalan ini, Dewan Perwakilan Rakyat dan YLKI mendesak menteri Jonan memberikan sanksi berat kepada maskapai Lion Air. Tidak cukup hanya penghentian pengajuan izin rute baru.'
    ],
    'PROFIL'=>[
        'title'=>'Profil LION AIR',
        'narasi'=>'Tahun 2000, persis dua tahun setelah reformasi bergolak, Lion Air berdiri sebagai perusahaan maskapai penerbangan swasta. Dengan satu buah pesawat saja, Boeing 737-200, Lion Air mulai menerbangkan penumpangnya di Indonesia tujuan Jakarta-Pontianak. Saat itu, Lion Air tak banyak diperhitungkan orang. 
                </p><p style="margin-left: 20px;margin-right: 20px;">Kini, Lion Air menjadi maskapai penerbangan paling banyak mengangkut penumpang. Menandingi maskapai pelat merah, Garuda Indonesia. Lion juga konsisten memberikan pelayanan tiket murah bagi penumpang. Maskapai milik politisi Partai Kebangkitan Bangsa dan Anggota Dewan Pertimbangan Presiden, Rusdi Kirana, berambisi menjadi raja penerbangan di Indonesia. Sekalipun publik lebih mengenalnya raja “delay” Indonesia.  
                </p><p style="margin-left: 20px;margin-right: 20px;">Kini Lion Air terbang ke lebih dari 36 kota di Indonesia dan banyak tujuan-tujuan penerbangan lainnya, seperti Singapura, Malaysia dan Vietnam, Timor Leste dan Saudi Arabia. Pada 2013, Lion Air menggandeng perusahaan penerbangan Malaysia meluncurkan Malindo Airline dengan 20 pesawat masuk ke pasar Malaysia. Di Thailand, perusahaan yang Komisarisnya di pegang Wakil Ketua MPR RI, Oesman Sapta Odang juga memiliki Thai Lion Air di Thailand.',
        'armada'=>[
            ['no'=>'71 Pesawat Boeing 737-900ER'],
            ['no'=>'30 Pesawat Boeing 737-800'],
            ['no'=>'2 Pesawat Boeing 737-300'],
            ['no'=>'2 Pesawat Boeing 737-400'],
            ['no'=>'2 Boeing 747-400'],
            ['no'=>'3 McDonnell Douglas MD-90']
        ],
        'anak'=>[
            ['no'=>'Wings Air'],
            ['no'=>'Lion Bizjet'],
            ['no'=>'Batik Air'],
            ['no'=>'Malindo Airline (Malaysia)'],
            ['no'=>'Thai Lion Air (Thailand)']
        ]
    ],
    'KONTROVERSI'=>[
        'title'=>'KONTROVERSI LION AIR',
        'narasi'=>'Belakangan ini, publik dikagetkan dengan berbagai terobosan yang dilakukan Lion Air. Maskapai penerbangan milik anggota Dewan Pertimbangan Presiden, Rusdi Kirana melesat cepat, membuat spekulasi “gila.” Sebuah terobosan tak biasa. Beredar rumor kuat, pemilik sesungguhnya Lion Air bukan Rusdi Kirana, melainkan jaringan pengusaha besar Singapura, “lion” sesungguhnya.',
        'isi'=>[
            ['no'=>'Pada tahun 2011, Rusdi menggelontorkan US$21 miliar untuk membeli 230 pesawat Boeing.'],
            ['no'=>'Pada tahun 2013, Rusdi mengeluarkan US$24 miliar untuk membeli 234 pesawat Airbus.Ia kemudian tercatat sebagai pemesanan pesawat komersial terbesar dalam sejarah.'],
            ['no'=>'Pada 2013, Lion Group mencatatkan laba bersih US$200 juta. '],
            ['no'=>'Pada 2014, Rusdi mengklaim, setidaknya 43 juta penumpang telah terbang dengan Lion Group.'],
            ['no'=>'Pada Februari 2015, Lion Air Group sedang dalam pembicaraan dengan Qantas Airways mengenai pembelian saham Qantas sebanyak 49 persen dalam Jetstar Asia Airways. Lion Group berafiliasi dengan rekanannya di Singapura.']
        ]
    ],
    'REPUTASI'=>[
        'title'=>'REPUTASI BURUK LION AIR',
        'narasi'=>'Terkait transportasi publik, sepanjang 2014, Yayasan Lembaga Konsumen Indonesia (YLKI) menerima 84 pengaduan dari masyarakat. Dari 84 aduan yang diterima YLKI, keluhan terhadap layanan transportasi udara menduduki peringkat pertama. Jumlahnya sebanyak 61 aduan. Transportasi darat sebanyak 22 aduan, dan transportasi laut, 1 aduan. Sementara untuk maskapai yang paling banyak diadukan :',
        'isi'=>[
            ['no'=>'Lion Air adalah maskapai yang paling banyak diadukan penumpang, sepanjang 2014. Sebanyak 24 aduan untuk Lion Air.'],
            ['no'=>'Maskapai Tiger Mandala Air sebanyak 6 aduan.'],
            ['no'=>'AirAsia Indonesia 6 aduan.'],
            ['no'=>'Garuda Indonesia Airlines mencatat 5 aduan.'],
            ['no'=>'Sriwijaya Air 5 aduan.']
        ]
    ],
    'PENDUKUNG'=>[
        'partaiPolitik'=>[
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaidemokrasiindonesia518c566e8321d'],
            ['page_id'=>'nasdem5119b72a0ea62'],
        ],
        'institusi'=>[
            ['name'=>'Kementerian Perhubungan','img'=>'http://www.bijaks.net/public/upload/image/politisi/kementerianperhubungan52fb1971da37b/badge/cbad49a576f93d2a998dca383beb205bafb34a56.JPG','url'=>'http://www.bijaks.net/aktor/profile/kementerianperhubungan52fb1971da37b'],
            ['name'=>'PT Angkasa Pura (AP) II','img'=>'http://static.inilah.com/data/berita/foto/2080459.jpg','url'=>'http://www.angkasapura2.co.id/'],
            ['name'=>'Indonesia National Air Carriers Association (INACA)','img'=>'http://kanalsatu.com/images/20140905-225503_26.jpg','url'=>'http://www.inaca.org/'],
            ['name'=>'Induk Koperasi TNI AU (Inkopau)','img'=>'http://inkopau.citozaidt.com/?q=sites/default/files/styles/cit_wide/adaptive-image/public/logo_0.png&itok=GpxEh8C0','url'=>'http://www.inkopau.com/'],
            ['name'=>'Dewan Pertimbangan Presiden (WANTIMPRES)','img'=>'http://static.republika.co.id/uploads/images/detailnews/presiden-jokowi-melantik-sembilan-wantimpres-di-istana-negara-senin-19-1-_150119235714-308.jpg','url'=>'http://www.wantimpres.go.id/'],
            ['name'=>'PT Angkasa Transportasi Selaras','img'=>'http://www.bijaks.net/assets/images/hotpages/lionair/halim.jpg','url'=>''],
            ['name'=>'MPR RI','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Logo_of_People%27s_Consultative_Assembly_Indonesia.png/170px-Logo_of_People%27s_Consultative_Assembly_Indonesia.png','url'=>'https://www.mpr.go.id/']
        ],
        'tokoh'=>[
            ['name'=>'Rusdi Kirana','img'=>'http://www.bijaks.net/public/upload/image/politisi/rusdikirana52b40f8464c5c/badge/646a7b854d43d5a381855bce5a6227fdbae46dcf.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/rusdikirana52b40f8464c5c'],
            ['name'=>'Oesman Sapta Odang','img'=>'http://www.bijaks.net/public/upload/image/politisi/droesmansapta50d2910d83a34/badge/0e95a7aef70db515ff8f1fe48fe23f44e02328e9.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/droesmansapta50d2910d83a34'],
            ['name'=>'Surya Paloh','img'=>'http://www.bijaks.net/public/upload/image/politisi/suryapaloh511b4aa507a50/badge/5b8adf5757dbdc45407f3c499223705c120d8629.jpg','jabatan'=>'Pendiri NasDem','url'=>'http://www.bijaks.net/aktor/profile/suryapaloh511b4aa507a50'],
            ['name'=>'Dahlan Iskan','img'=>'http://www.bijaks.net/public/upload/image/politisi/dahlaniskan503d887d648d6/badge/05b6500f00d2e1c5248fa983784781c2ce3bca91.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/dahlaniskan503d887d648d6'],
            ['name'=>'Muhaimin Iskandar','img'=>'http://www.bijaks.net/public/upload/image/politisi/muhaiminiskandar50ef9d0b6f4d5/badge/1e7f1131a14f3dbbb0448b74e84d637fb5bc909d.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/muhaiminiskandar50ef9d0b6f4d5'],
            ['name'=>'Muhammad Jusuf Kalla','img'=>'http://www.bijaks.net/public/upload/image/politisi/drshmuhammadjusufkalla50ee870b99cc9/badge/e47e03b37d572a59b005aaa84751a6f05b002e9f.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/drshmuhammadjusufkalla50ee870b99cc9']
        ],
    ],
    'PENENTANG'=>[
        'partaiPolitik'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab']
        ],
        'institusi'=>[
            ['name'=>'DPR RI','img'=>'http://2.bp.blogspot.com/-Iw1R4Q9NTJM/U6Q5fdwgOyI/AAAAAAAAAII/ngbOQhwJhHc/s1600/LOGO+DPR-RI.png','url'=>'http://www.dpr.go.id/'],
            ['name'=>'Kementerian Koordinator Perekonomian','img'=>'http://3.bp.blogspot.com/-i8oB_BAvxqs/UcfZrIInWJI/AAAAAAAABU4/Ivf6DOwEYdE/s200/LogoKementerianbaru-1a.jpg','url'=>'http://www.ekon.go.id/'],
            ['name'=>'Ombudsman RI','img'=>'http://i.ytimg.com/vi/c2aLEANBpBg/maxresdefault.jpg','url'=>'http://www.ombudsman.go.id/'],
            ['name'=>'Yayasan Lembaga Konsumen Indonesia','img'=>'http://www.bijaks.net/public/upload/image/politisi/yayasanlebagakonsumenindonesia533a2ee794221/badge/e5b1cd87c6003d6fe58986d936e4811ed92e9ed5.jpg','url'=>'http://www.bijaks.net/aktor/profile/yayasanlebagakonsumenindonesia533a2ee794221'],
            ['name'=>'Asosiasi Pengusaha Indonesia (Apindo)','img'=>'http://www.bijaks.net/public/upload/image/politisi/apindo52d20d8af2946/badge/55b60d14405084a1fe9cae46f4ff7435842ae647.jpg','url'=>'http://www.bijaks.net/aktor/profile/apindo52d20d8af2946'],
            ['name'=>'Asosiasi Agen Perjalanan dan Tur Indonesia','img'=>'http://www.daftarhajiumroh.com/wp-content/uploads/2015/01/130321_asita.jpg','url'=>'']
        ],
        'tokoh'=>[
            ['name'=>'Setyo Novanto','img'=>'http://www.bijaks.net/public/upload/image/politisi/drssetyanovanto50f8fd3c666bc/badge/3bd28ea8ee02353985fe8fced501bec56cb01ffd.jpg','jabatan'=>'','url'=>''],
            ['name'=>'Fahri Hamzah','img'=>'http://www.bijaks.net/public/upload/image/politisi/fahrihamzahse5105e57490d09/badge/4b1333b525b60ef5d2347fa887d0525b7259068c.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09'],
            ['name'=>'Sofyan Djalil','img'=>'http://www.bijaks.net/public/upload/image/politisi/drsofyanadjalilshmamald518c7b1b2b183/badge/45078e14613dea98f918d59bc92612bfbeea6309.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/drsofyanadjalilshmamald518c7b1b2b183'],
            ['name'=>'Zulkifli Hasan','img'=>'http://www.bijaks.net/public/upload/image/politisi/zulkiflihasan50ef907d0661c/thumb/zulkiflihasan50ef907d0661c_20130111_041156.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/zulkiflihasan50ef907d0661c'],
            ['name'=>'Ignasius Jonan','img'=>'http://www.bijaks.net/public/upload/image/politisi/ignasiusjonan51517941433fd/badge/9b325122fe82a4d77296e6f71995d0f9d3977638.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/ignasiusjonan51517941433fd'],
            ['name'=>'Sudaryatmo','img'=>'http://www.thejakartapost.com/files/images2/p28-A2_4.main%20story.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/sudaryatmo54aa095e547c6'],
            ['name'=>'Willgo Zainar','img'=>'http://seputarnusantara.com/wp-content/uploads/2014/12/Willgo-Zainar-Baju-Coklat-1.jpg','jabatan'=>'','url'=>''],
            ['name'=>'Said Didu','img'=>'http://www.bijaks.net/public/upload/image/politisi/mohammadsaiddidu51cb9f6f9be4a/badge/6ee427dafdd99ef7399f2707ba6aa1807b529f20.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/mohammadsaiddidu51cb9f6f9be4a'],
            ['name'=>'Hariyadi Sukamdani','img'=>'http://cikalnews.com/static/data/berita/foto/besar/86172646516Haryadi_B_Sukamdani.jpg','jabatan'=>'','url'=>'']
        ]
    ],
    'KASUS'=>[
        'narasi'=>'Lion Air paling banyak digugat ke pengadilan oleh penumpangnya sendiri. Berikut perkara Lion Air yang paling sering rugikan penumpang :',
        'BURUKNYA'=>[
            ['title'=>'Pembekukan Rute Penerbangan','img'=>'','no'=>'Januari 2015 Kementerian Perhubungan menjatuhkan sanksi kepada maskapai Lion Air karena melanggar izin penerbangan. Sanksi diberikan dengan membekukan 35 penerbangan Lion Air.'],
            ['title'=>'Pesawat Delay','img'=>'','no'=>'Tahun 2007, David Tobing, menuntut ganti rugi Wings Air anak perusahaan PT Lion Mentari Airlines (Lion Air) karena penundaan jadwal penerbangan. David membawa perkara ini ke Pengadilan Negeri Jakarta Pusat dalam perkara No. 309/PDT.G/2007/PN.Jkt.Pst. Hakim menghukum Lion Air membayar ganti rugi sebesar Rp718,500 dan biaya perkara Rp 234,000.'],
            ['title'=>'Menghilangkan Barang Penumpang','img'=>'','no'=>'Lion Air menghilangkan koper penumpang, Herlina Sunarti, seberat 12 kilogram dalam penerbangan rute Jakarta – Semarang pada 4 Agustus 2011. Kasus ini diselesaikan di Badan Penyelesaian Sengketa Konsumen (BPSK) yang menghukum Lion Air untuk membayar ganti rugi sejumlah Rp 25 juta. Putusan BPSK No. 12/BPSK/Smg/Put/Arbitrase/X/2011.'],
            ['title'=>'Diskriminatif Terhadap Penumpang Disabilitas','img'=>'','no'=>'Lion Air melakukan diskriminasi terhadap penumpang disabilitas, Ridwan Hakim.  Petugas Lion Air beberapa kali mengacuhkan Ridwan yang menggunakan kursi roda. Hakim menghukum Lion Air membayar ganti rugi sebesar Rp 25 juta (tanggung renteng dengan PT (Persero) Angkasa Pura II sebagai Tergugat II dan Kementerian Perhubungan Republik Indonesia sebagai Tergugat III).</p><p class="font_kecil" style="margin-left: 5px;margin-right: 15px;">Lion Air dikenai Pasal 134 ayat (1) UU Penerbangan memberikan hak kepada penyandang cacat, orang lanjut usia, serta anak-anak di bawah usia 12 (dua belas) tahun agar memperoleh pelayanan berupa perlakuan dan fasilitas khusus.']
        ],
        'MENCAPLOK'=>'<img src="http://www.bijaks.net/assets/images/hotpages/lionair/caplok1.jpg" class="pic">Dengan memanfaatkan buruknya hukum di Indonesia, anak perusahaan Lion Group, PT Angkasa Transportindo Selaras (ATS) merebut Bandara Halim Perdana Kusuma dari PT Angkasa Pura (AP) II dan Induk koperasi TNI AU (Inkopau). 
                    </p><p class="font_kecil" style="margin-left: 15px;">Untuk merebut bandara Halim, Lion Group mengakali surat perjanjian antara Inkopau dengan ATS Nomor Sperjan/10-09/03/01/Inkopau Nomor 003/JT-WON/PKS/II/2005. Surat itu mengatur pengelolaan bandara Halim Perdanakusumah. Lalu saham ATS sebesar 80 persen dikuasai Lion Air, dan 20 persen Inkopau.
                    </p><p class="font_kecil" style="margin-left: 15px;"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/caplok3.jpg" class="pic2">Seharusnya pemerintah memberikan sikap tegas terhadap Lion Group yang secara sepuhak merebut Bandara halim. Karena Halim merupakan aset Angkasa Pura II. Milik negara. Tidak seharusnya pihak swasta seperti Lion Group mengambil alih bandara nasional tersebut. Selain itu AP II sudah mengelola Bandara Halim sejak 1985, dan mengeluarkan investasi yang tidak sedikit.
                    </p><p class="font_kecil" style="margin-left: 15px;">Hak AP II sudah diatur dalam surat persetujuan bersama, antara Kepala Staf TNI AU dengan Dirjen Perhubungan Udara pada 5 Juni 1997, pengelolaan bandara sipil menjadi hak AP II. 
                    </p><p class="font_kecil" style="margin-left: 15px;"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/caplok2.jpg" class="pic">Juga merujuk Surat Keputusan yang dikeluarkan Menteri Perhubungan Nomor 23 Tahun 2003 tentang Pengoperasian bandara Soekarno-Hatta dan Bandara Halim Perdanaksuma.'
    ],
    'ANALISA'=>'<img src="http://www.bijaks.net/assets/images/hotpages/lionair/analisa1.jpg" class="pic2">Insiden delay Lion Air kali ini merupakan yang terparah. Penumpang Lion Air seluruh bandara di Indonesia meresponnya dengan boikot bandara dan merusak fasilitas bandara. Publik Indonesia secara umum marah besar terhadap Lion Air. Hal ini bisa dipahami, mengingat delay ini berlangsung selama 3 hari berturut turut. Sejak Rabu 18 Februari 2015 hingga Jum’at 20 Februari 2015, ribuan penumpang di berbagai bandara ditelantarkan Lion Air akibat penundaan terbang. 
                </p><p><img src="http://www.bijaks.net/assets/images/hotpages/lionair/analisa2.jpg" class="pic">Lion Air secara resmi baru meminta maaf kepada penumpang dua hari setelahnya, maskapai milik Dewan Pertimbangan Presiden (Watimpres), Rusdi Kirana berdalih bahwa delay disebabkan adanya kerusakan mesin dan pecahnya kaca jendela pesawat.
                </p><p>Tak sekadar persoalan tekhnis, kasus delay Lion Air ini mewartakan bururknya pengelolaan penerbangan di Indonesia. Dan tidak ada penanganan secara cepat dan tegas dari Kementerian Perhubungan. Sehingga mengakibatkan penumpukan penumpang terjadi di banyak bandara. Refund dan konpensasi penumpang juga baru dilakukan di hari Jum’at dengan menggunakan dana Angkasa Pura II, bukan Lion Air.
                </p><p><img src="http://www.bijaks.net/assets/images/hotpages/lionair/analisa3.jpg" class="pic2">Selain itu, kasus Lion Air mengesankan kuatnya aroma politik dan bisnis di dunia penerbangan. Pengaruh Lion Air semakin kuat sehingga Menteri Jonan tidak berani menghukum maskapai yang saat ini dipimpin Oesman Sapta (Komisaris Lion Air), Wakil Majelis Permusyawaran Rakyat RI. Seperti banyak diberitakan, Lion Group-perusahaan yang menaungi Lion Air, akan merebut bandara Halim Perdana Kusuma. Lion melawan Angkasa Pura II.
                </p><p><img src="http://www.bijaks.net/assets/images/hotpages/lionair/analisa4.jpg" class="pic">Dalam menyikapai kasus Lion Air, Kementerian Perhubungan harus bertindak tegas.  Menteri Jonan harus mencabut izin terbang Lion Air, hingga maskapai tersebut benar-benar memperbaiki standar pelayanan. 
                </p><p>Menteri Jonan harus segera menerbitkan Peraturan Menteri (PM) Perhubungan tentang standar pelayanan penumpang untuk terkait transportasi. Mempertegas sanksi bagi maskapai yang buruk dalam melayani penumpang. Dan mempertagas aturan pengeloaan penerbangan di Indonesia.',
    'PETAKA'=>[
        ['no'=>'14 Januari 2002, Lion Air Penerbangan 386 PK-LID, Boeing 737-200 rute Jakarta-Pekanbaru-Batam gagal mengudara (take off) dan terjerembab setelah lebih dari lima meter badan pesawat meninggalkan landasan pacu di Bandara Sultan Syarif Kasim II, Pekanbaru. Tujuh orang penumpangnya luka-luka dan patah tulang.'],
        ['no'=>'31 Oktober 2003, Lion Air Penerbangan 787, MD-82 rute Ambon-Makassar-Denpasar, keluar jalur saat mendarat di Bandara Hasanuddin, Makassar.'],
        ['no'=>'3 Juli 2004, Lion Air Penerbangan 332, MD-82 rute Jakarta-Palembang mendarat tidak sempurna di Bandara Sultan Mahmud Badaruddin II, Palembang.'],
        ['no'=>'30 November 2004, Lion Air Penerbangan 538 PK-LMN, MD-82 rute Jakarta-Solo-Surabaya tergelincir saat melakukan pendaratan di Bandara Adisumarmo, Solo. 26 orang penumpangnya tewas.'],
        ['no'=>'10 Januari 2005, Lion Air Penerbangan 789, MD-82 gagal take off di Bandara Wolter Monginsidi, Kendari akibat salah satu bannya kempes.'],
        ['no'=>'3 Februari 2005, Lion Air Penerbangan 791, MD-82 rute Ambon-Makassar tergelincir saat mendarat di Bandara Hasanuddin, Makassar.'],
        ['no'=>'12 Februari 2005, Lion Air Penerbangan 1641, MD-82 rute Mataram-Surabaya ketika akan take off di Bandara Selaparang, Mataram. Roda bagian depan tergelincir keluar landasan, sekitar setengah meter di sebelah utara dari pinggir landasan pacu.'],
        ['no'=>'6 Mei 2005, Lion Air Penerbangan 778, MD-82 rute Jakarta-Makassar pecah ban saat mendarat di Bandara Hasanuddin, Makassar. Akibatnya, pilot terpaksa menghentikan pesawat di landasan pacu sebelum mencapai lapangan parkir.'],
        ['no'=>'24 Desember 2005, Lion Air Penerbangan 792, MD-82 rute Jakarta-Makassar-Gorontalo tergelincir saat melakukan pendaratan di Bandara Hasanuddin, Makassar.'],
        ['no'=>'18 Januari 2006, Lion Air Penerbangan 778, MD-82 rute Ambon-Makassar-Surabaya tergelincir saat melakukan pendaratan di Bandara Hasanuddin, Makassar.'],
        ['no'=>'4 Maret 2006, Lion Air penerbangan 8987, MD-82 rute Denpasar-Surabaya tergelincir saat mendarat di Bandara Juanda, Surabaya karena cuaca buruk.'],
        ['no'=>'7 April 2006, Lion Air Penerbangan 391, MD-82 rute Pekanbaru-Jakarta batal lepas landas karena gangguan pada roda kiri di Bandara Sultan Syarif Kasim II, Pekanbaru. Pesawat itu tak jadi lepas landas karena roda kirinya tiba-tiba tak bergerak walaupun sudah bergerak dari apron menuju ujung landasan dan siap terbang.'],
        ['no'=>'24 Desember 2006, Lion Air Penerbangan 792,PK-LIJ Boeing 737-400 rute Jakarta-Makassar-Gorontalo tergelincir saat melakukan pendaratan di Bandara Hasanuddin, Makassar.'],
        ['no'=>'19 Maret 2007, Lion Air Penerbangan 311, MD-82 rute Banjarmasin-Surabaya batal lepas landas walaupun sempat meluncur di landasan pacu Bandar Udara Sjamsudin Noor, Banjarmasin.'],
        ['no'=>'23 Februari 2009, Lion Air Penerbangan 972 PK-LIO, MD-90 rute Medan-Batam-Surabaya mendarat darurat di Bandara Hang Nadim Batam akibat macetnya roda depan. Semua penumpang selamat. '],
        ['no'=>'9 Mei 2009, MD-90 Lion Air PK-LIL tergelincir di Bandara Soekarno-Hatta.'],
        ['no'=>'3 November 2010, Lion Air Penerbangan 712 ,PK-LIQ Boeing 737-400 rute Jakarta-Pontianak-Jakarta tergelincir di Bandara Supadio Pontianak.'],
        ['no'=>'14 Februari 2011, Lion Air Penerbangan 598, Boeing 737-900ER rute Jakarta-Pekanbaru tergelincir saat mendarat di Bandara Sultan Syarif Kasim II, Pekan Baru. Semua Penumpang selamat namun hal itu di tanggapi oleh Dirjen Perhubungan Darat dengan menyatakan bahwa semua pesawat jenis Boeing 737-900ER Dilarang Mendarat di Kota Pekanbaru apabila landasan basah. Lion Air memutuskan menggunakan pesawat Boeing 737-400 untuk melayani rute tersebut (Hal ini kemungkinan akan menunda niat Lion Air untuk memensiunkan Boeing 737-400).'],
        ['no'=>'15 Februari 2011, Lion Air tujuan Medan-Pekanbaru-Jakarta dengan nomor penerbangan JT 0295 berjenis Boeing 737-900 ER tergelincir di Pekanbaru pada pukul 17.00 WIB. Seluruh roda pesawat keluar dari lintasan bandara. Seluruh penumpang tidak mengalami luka-luka.'],
        ['no'=>'17 Februari 2011 sebuah Lion Air Boeing 737-900 ER (pesawat yang sama yang tergelincir di Pekanbaru 2 hari sebelumnya) sedang didorong oleh traktor di bandara Jakarta dan tanpa sengaja mengarah ke pesawat Lion lainnya. Pesawat mengalami kerusakan pada stabilizer bagian belakang. Tidak ada laporan korban luka. '],
        ['no'=>'23 Oktober 2011, Lion Air JT 673 tergelincir di Bandar Udara Sultan Aji Muhammad Sulaiman, Balikpapan, Kalimantan Timur sekitar pukul 07.24 Wita.'],
        ['no'=>'13 April 2013, Kecelakaan Lion Air Bali dengan rute Bandung menuju Denpasar terperosok ke laut di Bandara Ngurah Rai, Denpasar tanpa sempat menyentuh landasan pacu.'],
        ['no'=>'19 April 2013, Lion Air tujuan Denpasar - Jakarta batal terbang karena mengalami masalah dengan mesin. '],
        ['no'=>'21 April 2013, Lion Air dengan nomor penerbangan 0689 dari Bandar Udara Supadio, Pontianak tujuan Bandar Udara Internasional Soekarno-Hatta, Jakarta, setelah 20 menit terbang secara tiba-tiba masker oksigen keluar di kabin pesawat. ']
    ],
    'BERITA'=>[
        ['img'=>'http://news.bijaks.net/uploads/2015/02/dirut-lion-air.jpg','shortText'=>'Terlantarkan Ribuan Penumpang, Dirut Lion Air: Kami Terus Pelajari Situasi','link'=>'http://www.bijaks.net/news/article/9-96599/terlantarkan-ribuan-penumpang-dirut-lion-air-kami-terus-pelajari-situasi'],
        ['img'=>'http://news.bijaks.net/uploads/2015/02/lion-air2.jpg','shortText'=>'Penumpang Lion Air: Boro-boro Hotel, Snack Aja Enggak Mampu','link'=>'http://www.bijaks.net/news/article/9-96610/penumpang-lion-air-boro-boro-hotel-snack-aja-enggak-mampu'],
        ['img'=>'http://news.bijaks.net/uploads/2015/02/lion-air1.jpg','shortText'=>'Terlantarkan Penumpang Hingga Belasan Jam, Kemenhub Akan Panggil Lion Air','link'=>'http://www.bijaks.net/news/article/9-96549/terlantarkan-penumpang-hingga-belasan-jam-kemenhub-akan-panggil-lion-air'],
        ['img'=>'http://news.bijaks.net/uploads/2015/02/Yudi-Widiana-Adia-612x353.jpg','shortText'=>'DPR Minta Menteri Jonan Berani Tegur Lion Air','link'=>'http://www.bijaks.net/news/article/3-96647/dpr-minta-menteri-jonan-berani-tegur-lion-air'],
        ['img'=>'http://news.bijaks.net/uploads/2015/02/zulkifli.jpg','shortText'=>'Maskapai Lion Air Delay, Ketua MPR: Karyawannya Pada Mogok Tuh','link'=>'http://www.bijaks.net/news/article/9-96529/maskapai-lion-air-delay-ketua-mpr-karyawannya-pada-mogok-tuh'],
        ['img'=>'http://news.bijaks.net/uploads/2015/02/Muhiddin-M-Said-627x313.jpg','shortText'=>'DPR Minta Menteri Jonan Hukum Lion Air','link'=>'http://www.bijaks.net/news/article/7-96629/dpr-minta-menteri-jonan-hukum-lion-air'],
        ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/76524c12675c2dcef16200fdf8d13793a6f9078b.jpg','shortText'=>'Buruknya Tata Kelola Transportasi Udara Indonesia','link'=>'http://www.bijaks.net/scandal/index/15441-buruknya_tata_kelola_transportasi_udara_indonesia'],
        ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/feb43e0e18b6db12cfa4aefcc454db09664ef6ce.jpg','shortText'=>'Polemik Pengelolaan Bandara Halim Perdanakusuma','link'=>'http://www.bijaks.net/scandal/index/14586-polemik_pengelolaan_bandara_halim_perdanakusuma']
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Oesman Sapta Oedang','jabatan'=>'Komisaris Lion Air & Wakil Ketua MPR RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/droesmansapta50d2910d83a34/badge/0e95a7aef70db515ff8f1fe48fe23f44e02328e9.jpg','url'=>'http://www.bijaks.net/aktor/profile/droesmansapta50d2910d83a34','content'=>'"Jangan terlalu keraslah menanggapi keterlambatan Lion Air, hal ini bisa menimbulkan keresahan pada calon penumpang kami. Jika ini terjadi maka akan merugikan sebuah perusahaan penerbangan, dalam hal ini Lion Air."'],
        ['from'=>'Dwiyanto Akbar Hidayat','jabatan'=>'Sekretaris Perusahaan Lion Air','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Atas nama manajemen Lion Air, kami mohon maaf sebesar-besarnya atas ketidaknyamanan yang dialami oleh para penumpang. Kami pastikan bahwa kami menjalankan Permen 77 mengenai ganti rugi penumpang. Selain itu, kami juga telah memberikan pilihan untuk full refund."']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Fahri Hamzah','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/fahrihamzahse5105e57490d09/badge/4b1333b525b60ef5d2347fa887d0525b7259068c.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09','content'=>'"Ini karena belum menyebabkan nyawa melayang saja, kalau sudah begitu baru panik pecat sana pecat sini. Ini justru gejala awal yang harus dilacak kementerian sehingga yang menyebabkan nyawa melayang ini harus dituntaskan dulu. Jangan menunggu muncul korban."'],
        ['from'=>'Said Didu','jabatan'=>'Mantan Sekretaris Kementerian Badan Usaha Milik Negara (BUMN)','img'=>'http://www.bijaks.net/public/upload/image/politisi/mohammadsaiddidu51cb9f6f9be4a/badge/6ee427dafdd99ef7399f2707ba6aa1807b529f20.jpg','url'=>'http://www.bijaks.net/aktor/profile/mohammadsaiddidu51cb9f6f9be4a','content'=>'"Saya dari dulu mengatakan Lion Air ini kayak anak istimewa di negara ini dan saya pernah mengalaminya sendiri. Coba ada yang menegur tidak? Ada apa sih di balik Lion Air ini?"'],
        ['from'=>'Hendra Nurtjahjo','jabatan'=>'Ombudsman','img'=>'http://pedomannews.com/images/resized/images/photos/hendra_nurtjahjo_ombudsman_-_pdn_250_200.jpg','url'=>'','content'=>'"Manajemen penerbangan memiliki banyak sisi yang seyogyanya diawasi ketat oleh Kementerian Perhubungan karena melibatkan tidak hanya bisnis bernilai triliunan melainkan juga keselamatan penumpang."'],
        ['from'=>'Nawek Aliun','jabatan'=>'Penumpang Pesawat Lion Air','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Saya dapat kabar karena pilot dan crew mogok kerja dan demo gara-gara tidak dibayar atau apalah. Jadi bukan karena pesawat menabrak burung. Itu ditutupi oleh staf Lion Air di lokasi."'],
        ['from'=>'Djoko Murjatmodjo','jabatan'=>'Direktur Operasional dan Teknik PT Angkasa Pura II','img'=>'http://www.angkasapura2.co.id/nuweb/NUWEB_PUBLIC_FILES/angkasapura2/xBOD_BOC_21_01_2015__12_58_39.jpg.pagespeed.ic._uzzZFLEUw.webp','url'=>'','content'=>'"Kalau sekali (delay) oke, dua kali berarti ada masalah yang diperkirakan, tiga kali berarti ada masalah yang fundamental."'],
        ['from'=>'Sofyan Djalil','jabatan'=>'Menteri Koordinator Perekonomian','img'=>'http://www.bijaks.net/public/upload/image/politisi/drsofyanadjalilshmamald518c7b1b2b183/badge/45078e14613dea98f918d59bc92612bfbeea6309.jpg','url'=>'http://www.bijaks.net/aktor/profile/drsofyanadjalilshmamald518c7b1b2b183','content'=>'"Saya pikir perlu ada evaluasi secara menyeluruh bagaimana operasional dari LCC (Low Cost Carriers/maskapai penerbangan murah)."'],
        ['from'=>'Willgo Zainar','jabatan'=>'Anggota Komisi Keuangan Dewan Perwakilan Rakyat','img'=>'http://seputarnusantara.com/wp-content/uploads/2014/12/Willgo-Zainar-Baju-Coklat-1.jpg','url'=>'','content'=>'"Jangan sampai satu persatu maskapai nasional kita runtuh dan hilang dari udara karena manajemen SDM, dan keuangannya yang amburadul."'],
        ['from'=>'Ignasius Jonan','jabatan'=>'Menteri Perhubungan','img'=>'http://www.bijaks.net/public/upload/image/politisi/ignasiusjonan51517941433fd/badge/9b325122fe82a4d77296e6f71995d0f9d3977638.jpg','url'=>'http://www.bijaks.net/aktor/profile/ignasiusjonan51517941433fd','content'=>'"Soal sanksi nanti kita rapat, paling tidak sekarang pengajuan izin rute baru dari Lion Air kita hentikan dulu."'],
        ['from'=>'Rudiana','jabatan'=>'Wakil Ketua Asita (Asosiasi Agen Perjalanan dan Tur Indonesia) Jakarta','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Keterlambatan penerbangan maskapai Lion Air yang terjadi sejak Rabu kemarin mencoreng citra penerbangan Indonesia."'], 
        ['from'=>'Setya Novanto','jabatan'=>'Ketua DPR RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/drssetyanovanto50f8fd3c666bc/badge/3bd28ea8ee02353985fe8fced501bec56cb01ffd.jpg','url'=>'http://www.bijaks.net/aktor/profile/drssetyanovanto50f8fd3c666bc','content'=>'"Ini bukan kejadian pertama kalinya Lion Air bermasalah. Ini jadi tanggung jawab Kemenhub dan pemilik Lion Air."'],
        ['from'=>'Ahmad Syahir','jabatan'=>'Public Relations PT Angkasa Pura II','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Ini refund inisiatif dari Angkasa Pura II karena kami lihat penumpang butuh kepastian, kalau menunggu Lion Air sampai sekarang tidak ada."'],
        ['from'=>'Ferry Djemi Francis','jabatan'=>'Ketua Komisi V','img'=>'http://www.politikindonesia.com/images/berita/ori/171557911.jpg','url'=>'','content'=>'"Pertama, Komisi V menyesalkan dan menyatakan prihatin atas kejadian delay penerbangan dalam jumlah besar dalam tiga hari terakhir ini."'],
        ['from'=>'Caca (28)','jabatan'=>'Penumpang Lion Air Tujuan Suranaya-Manado','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Kalau kayak gini kapok naik Lion. Mikir-mikir lagi lah kalau naik Lion."'],
        ['from'=>'Joko Widodo','jabatan'=>'Presiden RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'"Pelayanan yang diberikan harus sebaik-baiknya."'],
        ['from'=>'Sudaryatmo','jabatan'=>'Ketua Pengurus Harian YLKI','img'=>'http://www.thejakartapost.com/files/images2/p28-A2_4.main%20story.jpg','url'=>'http://www.bijaks.net/aktor/profile/sudaryatmo54aa095e547c6','content'=>'"Dari enam maskapai terbesar Lion Air paling banyak, 24 aduan."'],
        ['from'=>'Dwina Fannia','jabatan'=>'Penumpang Lion Air Tujuan Kupang-Surabaya','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Jadwalnya jam 15.00 Wita, tapi sampai sekarang tak kunjung ada kepastian pemberangkatannya."']
    ],
    'VIDEO'=>[
        ['id'=>'u1l_WLvUuT8'],
        ['id'=>'JO2tOwuTLt4'],
        ['id'=>'vdqCe3qsO2s'],
        ['id'=>'grnK9y3BmY8'],
        ['id'=>'8H_pGVMKi8U'],
        ['id'=>'QpCCJcR1x4Q'],
        ['id'=>'kSsRMHqhPLg'],
        ['id'=>'ugIbsymnjvo']
    ],
    'PARODI'=>[
        ['img'=>'meme3'],
        ['img'=>'meme2'],
        ['img'=>'meme1'],
        ['img'=>'meme4'],
        ['img'=>'meme5'],
        ['img'=>'meme6'],
        ['img'=>'meme7'],
        ['img'=>'meme8'],
        ['img'=>'meme9'],
        ['img'=>'meme10'],
        ['img'=>'meme11'],
        ['img'=>'meme12'],
        ['img'=>'meme13'],
        ['img'=>'meme14']
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
// <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.qtip.min.js" ></script>
// <script type="text/javascript">
//     // Create the tooltips only when document ready
//     $(document).ready(function() {
//         // Use the each() method to gain access to each elements attributes
//         $('area').each(function() {
//             $(this).qtip( {
//                 content: $(this).attr('alt'), // Use the ALT attribute of the area map
//                 position: {
//                     corner: {
//                         target: 'topRight',
//                         tooltip: 'bottomLeft'
//                     }
//                 },
//                 style: {
//                     name: 'cream',
//                     padding: '7px 13px',
//                     width: {
//                         max: 320,
//                         min: 0
//                     },
//                     tip: true
//                 }
//             });
//         });
//     });
// </script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/lionair/top2.jpg")?>') no-repeat transparent;
        height: 685px;
        margin-bottom: -190px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .boxprofile p, .boxprofile li, .font_kecil {
        font-size: 12px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        background: rgba(0, 0, 0, 0.7);
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
    }
    .block_red {
        background-color: #720502;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 100%;
        height: auto;
        margin-bottom: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .garis {
        border-top: 1px solid black;
    }
    .boxgray {
        width: 96%;
        border: 9px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: 115px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 218px;height: 158px;padding: 0px !important;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri" style="margin-top: 100px;">
                <h4 class="list"><a id="lionair" style="color: black;"><?php echo $data['NARASI']['title'];?></a></h4>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">ANALISA</a></h4>
                <p><?php echo $data['ANALISA'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="reputasiburuklionair" style="color: black;"><?php echo $data['REPUTASI']['title'];?></a></h4>
                <p><?php echo $data['REPUTASI']['narasi'];?></p>
                <img src="<?php echo base_url('assets/images/hotpages/lionair/lion.png')?>" class="pic2">
                <ol>
                    <?php
                    foreach ($data['REPUTASI']['isi'] as $key => $val) {
                        echo "<li style='margin-left: 20px;margin-right: 20px;margin-bottom: 10px;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
            </div>
            <div class="col_kanan">
                <div class="boxprofile white">
                    <h4 class="block_red text-center"><a id="profilelionair" style="color: white;"><?php echo $data['PROFIL']['title'];?></a></h4>
                    <p style="margin-left: 20px;margin-right: 20px;">
                        <img src="<?php echo base_url('assets/images/hotpages/lionair/lion.png')?>" class="picprofil">
                        <?php echo $data['PROFIL']['narasi'];?>
                    </p>
                    <p style="margin-left: 20px;">ARMADA :</p>
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['PROFIL']['armada'] as $key => $val) {
                            echo "<li style='margin-left: 20px;margin-right: 20px;'>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                    <p style="margin-left: 20px;">ANAK PERUSAHAAN :</p>
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['PROFIL']['anak'] as $key => $val) {
                            echo "<li style='margin-left: 20px;margin-right: 20px;'>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div><br>

                <h4 class="list" style="margin-left: 20px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="newslionair_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <a id="skemaperistiwa" style="color: black;"><img src="<?php echo base_url('assets/images/hotpages/lionair/skema.png')?>" alt="Skema" style="width: 100%;" usemap="#map"></a>

            <map name="map">
              <!-- <area shape="rect" coords="20,575,81,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi I</span><br>
                        <span style='color: black'>1. Dra. HJ. IDA FAUZIYAH, MSi<br>
                        2. Drs. A. MUHAIMIN ISKANDAR, M.Si<br> 
                        3. M. Syaiful Bahri Anshori</span>
                " href="">
              <area shape="rect" coords="90,575,151,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi II</span><br>
                        <span style='color: black'>1. Ir. H.M. LUKMAN EDY, M.Si.<br>
                        2. BERTU MERLAS, ST<br> 
                        3. H. YANUAR PRIHATIN, M.Si</span>
                " href="">
              <area shape="rect" coords="160,575,221,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi III</span><br>
                        <span style='color: black'>1. H. ABDUL KADIR KARDING, S.Pi, M.Si<br>
                        2. H. IRMAWAN, S.Sos., M.M.<br> 
                        3. HJ. ROHANI VANATH<br>
                        4. H. YAQUT CHOLIL QOUMAS</span>
                " href="">
              <area shape="rect" coords="230,575,291,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi IV</span><br>
                        <span style='color: black'>1. Drs. H. IBNU MULTAZAM<br>
                        2. H. CUCUN AHMAD SYAMSURIJAL, S.Ag<br> 
                        3. H. ACEP ADANG RUHIAT, M.si<br>
                        4. Drs. H. TAUFIQ R. ABDULLAH<br>
                        5. DANIEL JOHAN, SE</span>
                " href="">
              <area shape="rect" coords="300,575,361,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi V</span><br>
                        <span style='color: black'>1. PEGGI PATRISIA PATTIPI<br>
                        2. H. ALAMUDDIN DIMYATI ROIS<br> 
                        3. Drs. H. MUSA ZAINUDDIN<br>
                        4. Drs. MOHAMMAD TOHA, S.Sos., M.Si.</span>
                " href="">
              <area shape="rect" coords="370,575,431,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi VI</span><br>
                        <span style='color: black'>1. NENG EEM MARHAMAH ZULFA HIZ, S.Th.I<br>
                        2. SITI MUKAROMAH, S.Ag<br> 
                        3. Ir. M. NASIM KHAN<br> 
                        4. Dr. KH. KHOLILURRAHMAN, SH, M.Si</span>
                " href="">
              <area shape="rect" coords="440,575,501,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi VII</span><br>
                        <span style='color: black'>1. H. AGUS SULISTIYONO, S.E, MT<br>
                        2. SYAIKHUL ISLAM, Lc, M.Sosio<br> 
                        3. dr. HM ZAIRULLAH AZHAR<br> 
                        4. ARVIN HAKIM THOHA</span>
                " href="">
              <area shape="rect" coords="510,575,571,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi VIII</span><br>
                        <span style='color: black'>1. Drs. FATHAN<br>
                        2. Drs. H. BISRI ROMLY, MM <br> 
                        3. H. AN'IM FALACHUDDIN MAHRUS<br>
                        4. ARZETTY BILBINA SETYAWAN, SE.</span>
                " href="">
              <area shape="rect" coords="580,575,641,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi IX</span><br>
                        <span style='color: black'>1. MARWAN DASOPANG<br>
                        2. H. HANDAYANI, SKM<br> 
                        3. Hj. NIHAYATUL WAFIROH, MA<br>
                        4. Dra. Hj. SITI MASRIFAH, MA</span>
                " href="">
              <area shape="rect" coords="650,575,711,627" 
                alt="<span style='color: black;font-weight: bold;'>Komisi XI</span><br>
                        <span style='color: black'>1. Dr. Hj. ANNA MU'AWANAH<br>
                        2. HADI ZAINAL ABIDIN, S.Pd, MM, MPH</span>
                " href=""> -->
            </map>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="display: inline-block;height: auto;width: 98%;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) {
                            ?>
                            <div style="float: left;margin: 10px 16px;width: 46%;height: auto;display: inline-block;">
                                <a href="<?php echo $val['url'];?>">
                                    <img src="<?php echo $val['img'];?>" style="width: 55px; height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $val['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $val['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: 0px;"><?php echo $val['content']; ?></p>
                            </div>
                        <?php } ?>
                    </ul>
                </div>
            </div>

            <div class="clear"></div>
            <h4 class="list"><a id="quotepenentang" style="color: black;">QUOTE PENENTANG</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="display: inline-block;height: auto;width: 98%;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <div>
                            <?php 
                            for($i=0;$i<=3;$i++){
                            ?>
                                <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                        <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 55px; height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=4;$i<=7;$i++){
                            ?>
                                <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                        <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 55px; height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=8;$i<=11;$i++){
                            ?>
                                <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                        <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 55px; height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=12;$i<=15;$i++){
                            ?>
                                <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                        <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 55px; height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?></p>
                                </div>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            </div>

            <div class="clear"></div><br>
            <div class="col_full">
                <h4 class="text-center"><a id="petapetakalionair" style="color: black;">PETA PETAKA LION AIR</a></h4>
                <div class="garis"></div>
                <br>
                <img src="<?php echo base_url().'assets/images/hotpages/lionair/peta.png'; ?>" style="width:100%;height: auto;">
            </div>
            <div class="clear"></div>

            <div class="col_kiri50">
                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <div class="boxgray_green" style="height: 1070px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['partaiPolitik'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">INSTITUSI PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">TOKOH PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['tokoh'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi" style="height: 100px !important;">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col_kanan50">
                <h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <div class="boxgray_red" style="height: 1070px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['partaiPolitik'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">INSTITUSI PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 15px;" class="text-center">TOKOH PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['tokoh'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi" style="height: 100px !important;">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div><br>

            <div class="col_kiri50" style="margin-right: -1px;border-right:1px dashed black;">
                <h4 class="list"><a id="kasushukum" style="color: black;">KASUS HUKUM LION AIR</a></h4>
                <p class="font_kecil" style="margin-right: 15px;"><?php echo $data['KASUS']['narasi'];?></p>
                <ul class='list2'>
                    <?php
                    foreach ($data['KASUS']['BURUKNYA'] as $key => $val) { ?>
                        <li style='font-weight: bold;margin-left: 5px;margin-right: 15px;'><?php echo $val['title'];?></li>
                        <p class="font_kecil" style="margin-left: 5px;margin-right: 15px;"><?php
                        if ($val['img'] != "") { ?>
                            <img src="<?php echo base_url().$val['img'];?>" class='pic'>
                        <?php } ?>
                        <?php echo $val['no'];?></p>
                    <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col_kanan50">
                <h4 class="list" style="margin-left: 15px;"><a id="mencaplokbandarahalim" style="color: black;">MENCAPLOK BANDARA HALIM</a></h4>
                <p class="font_kecil" style="margin-left: 15px;"><?php echo $data['KASUS']['MENCAPLOK'];?></p>
            </div>

            <div class="clear"></div>
            <h4 class="list"><a id="parodi" style="color: black;">PARODI LION AIR</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme3.jpg" style="max-width: 30%;height: auto;float: left;margin-right: 1%;"/>
                <div style="width: 21%;float: left;">
                    <a href="#" data-toggle="modal" data-target="#parodi-meme10"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme10.jpg" style="max-width: 188px;height: auto;float: left;"/></a>
                </div>
                <div style="width: 48%;float: left;">
                    <div>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme1"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme1.jpg" class="parodi"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme2"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme2.jpg" class="parodi"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme4"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme4.jpg" class="parodi"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme5"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme5.jpg" class="parodi" style="margin-right: 0px;"/></a>
                    </div>
                    <div>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme6"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme6.jpg" class="parodi"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme7"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme7.jpg" class="parodi"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme8"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme8.jpg" class="parodi"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme9"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme9.jpg" class="parodi" style="margin-right: 0px;"/></a>
                    </div>
                    <div>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme11"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme11.jpg" class="parodi" style="margin-bottom: 0px;"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme12"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme12.jpg" class="parodi" style="margin-bottom: 0px;"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme13"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme13.jpg" class="parodi" style="margin-bottom: 0px;"/></a>
                        <a href="#" data-toggle="modal" data-target="#parodi-meme14"><img src="http://www.bijaks.net/assets/images/hotpages/lionair/meme14.jpg" class="parodi" style="margin-right: 0px;margin-bottom: 0px;"/></a>
                    </div>
                </div>
            </div>

            <?php
            foreach ($data['PARODI'] as $key => $val) { ?>
                <div class="modal hide fade" style="overflow-y: scroll;height: 550px;width: auto;" id="parodi-<?php echo $val['img'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Parodi Lion AIR</h4>
                            </div>
                            <div class="modal-body" style="top: 1px;right: 1px;">
                                <img src="http://www.bijaks.net/assets/images/hotpages/lionair/<?php echo $val['img'];?>.jpg">
                            </div>
                            <!-- <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div> -->
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>

            <div class="clear"></div><br>
            <h4 class="list"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="height: 360px;width: 98%;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <!-- jika ingin border atas bawah none gunakan mqdefault -->
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait Lion AIR</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>
<br/>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>