<?php 
$data = [
    'NARASI'=>[
        'title'=>'Polemik Dana Aspirasi DPR RI',
        'narasi'=>'<img src="http://www.jawapos.com/jppic/19564_17246_19150_16829_18937_16620_18599_16296_sidang%20paripurna.jpg" class="pic">Dewan Perwakilan Rakyat (DPR) mengesahkan Peraturan DPR tentang tata cara pengusulan pembangunan daerah pemilihan (UP2DP) atau dana aspirasi dalam rapat paripurna. Total dana tersebut sebesar Rp 11,2 triliun atau 20 miliar bagi setiap anggota DPR RI.
                    </p><p>Dana yang akan dikucurkan melalui APBN 2016 merupakan usulan dari DPR RI untuk menjadi platform perealisasian pembangunan daerah pemilihan. Meski mendapat penolakan dari presiden Jokowi, DPR tetap memaksakan peraturan tersebut untuk diloloskan sehingga memicu pro dan kontra. 
                    </p><p>Polemikpun bergulir, ada indikasi bahwa dana aspirasi merupakan  “dana politik” yang hanya akan menguntungkan parpol bukan rakyat. Selain itu, potensi penyalahgunaan dana tersebut mengintai.'
    ],
    'ANALISA'=>[
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2014/10/02/015656420141002-012646780x390.jpg" class="pic2">Pasca disahkannya peraturan tersebut, Presiden Jokowi langsung bereaksi tegas dengan mengutus bawahannnya, Menteri Keuangan Bambang Brodjonegoro ke DPR untuk menyampaikan sikap penolakannya. Sebelumnya, partai pendukung pemerintah di DPR juga memberikan intrupsi penolakan saat dilangsungkan rapat paripurna DPR. 
                    </p><p class="font_kecil">Sikap pemerintah tersebut membuat berang pimpinan DPR dan mayoritas anggota dewan karena tanpa persetujuan presiden dana aspirasi tidak akan pernah bisa terealisasikan. DPR pun mendesak dan mencoba melobi presiden agar mau memasukkan dana aspirasi ke dalam RAPBN 2016. 
                    </p><p class="font_kecil"><img src="http://cdn-media.viva.id/thumbs2/2014/10/02/271512_pimpinan-dpr-periode-2014-2019_663_382.jpg" class="pic">Sementara itu, sejumlah aktivis dan lembaga swadaya masyarakat menolak dana aspirasi karena menganggapnya sebagai bentuk pemborosan anggaran dan rawan diselewengkan sebagai dana kampanye.  
                    </p><p class="font_kecil">Sampai saat ini kinerja anggota dewan masih sangat mengecewakan. Dana aspirasi yang disahkan DPR  yang didengungkan akan dialokasikan untuk kepentingan di dapilnya masing-masing belum melalui hasil kajian mendalam. 
                    </p><p class="font_kecil">Usulan peraturan tersebut terlalu dipaksakan karena belum  ada keperluan yang begitu mendesak sehingga DPR tidak layak mendapat suntikan dana aspirasi tersebut.
                    </p><p class="font_kecil"><img src="http://cdn.metrotvnews.com/dynamic/content/2015/06/15/404743/kUsoWP7zNM.jpg?w=668" class="pic2">Apalagi, program dana aspirasi tersebut disinyalir sebagai upaya anggota DPR untuk mengamankan konsituen di dapil mereka masing-masing. Dengan dana aspirasi sebesar 20 miliar per dapil, agenda-agenda politik parpol akan berjalan, dan tentunya akan menguntungkan partai bukan rakyat. 
                    </p><p class="font_kecil">Dana tersebut juga merupakan bentuk pemborosan anggaran, sebab dana yang sudah ada di dalam APBN/APBD sudah cukup untuk memenuhi semua kebutuhan konsituen di daerah manapun. '
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="http://img1.beritasatu.com/data/media/images/medium/241434432594.jpg" class="pic">Sebelumnya Presiden Jokowi memberikan sinyal ketidaksetujuannnya atas usulan  DPR tentang dana aspirasi. Alasan Jokowi mengacu pada kondisi ekonomi Indonesia yang sedang lesu. Alasan lainnya,  anggaran tersebut bertentangan dengan visi misi pemerintah yang tertuang dalam program nawa cita. 
                    </p><p class="font_kecil">Presiden juga beranggapan bahwa dana aspirasi itu akan berbenturan dengan program pembangunan yang telah ditetapkan pemerintah. Penolakan tersebut juga karena tidak sesuai dengan kewenangan DPR dalam penentuan anggaran. Usulan DPR melabrak kewenangan fungsi eksekutif.
                    </p><p class="font_kecil"><img src="http://nawaberita.com/wp-content/uploads/2015/06/dana-aspirasi-620x330.jpg" class="pic2">Di sisi lain, anggota berdalih dan tersebut ialah perwujudan pasal 80 huruf J Undang-Undang No. 17 Tahun 2014 tentang MPR, DPR, DPD, dan DPRD yang berbunyi anggota DPR berhak mengusulkan dan memperjuangkan program pembangunan daerah pemilihan.
                    </p><p class="font_kecil">Atas dasar demikian, DPR  bersikeras agar dana aspirasi dimasukkan dalam alokasi RAPBN 2016. Namun, pemerintah tetap akan mempertimbangkan usulan tersebut. Jika hal tersebut tumpang tindih dengan hasil Musyawarah Perencanaan Pembangunan Nasional 2016 atau rencana kerja pemerintah 2016, usulan tersebut tidak akan dimasukkan dalam RAPBN 2016.'
    ],
    'PROFIL'=>[
        'narasi'=>'Pasal 80 huruf J Undang-Undang Nomor 17 Tahun 2014 tentang MPR, DPR, DPD, dan DPRD untuk mengusulkan peningkatan dana aspirasi bagi setiap anggota Dewan.
                    </p><p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Pasal 80 huruf J pada UU MD3 menyebutkan bahwa anggota DPR berhak mengusulkan dan memperjuangkan program pembangunan daerah pemilihan. Pasal tersebut tidak mengatur besarnya dana aspirasi.',
    ],
    'MENABRAK'=>[
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2014/05/28/161926520140528-172737-1780x390.jpg" class="pic">UP2DP yang rencananya dimasukkan melalui program transfer daerah Dana Alokasi Khusus (DAK) sangat berpotensi menabrak UU Nomor 23 Tahun 2014 tentang Pemerintah Daerah. Selain UU No. 23 Tahun 2014, usulan dana aspirasi ‎juga berpotensi menabrak UU Nomor 25 Tahun 2004 tentang Perencanaan Pembangunan Nasional dan UU Nomor 17 Tahun 2003 tentang Tata Kelola Keuangan Negara.
                    </p><p class="font_kecil">Sementara, sebagaimana amanat Undang-undang, yakni disalurkan melalui Pemda di dalam Musrenbang (Musyawarah Rencana Pembangunan) 2016, mulai dari Musrenbang Desa, Kecamatan, Kabupaten, Provinsi, dan Nasional untuk persiapan APBN 2017.
                    </p><p class="font_kecil">Andrianof Chaniago, Menteri Bappenas menegaskan bahwa dana aspirasi bisa bertabrakan dengan visi misi Presiden. Sebab, didalam undang-undang program pembangunan yang direncanakan itu harus diambil dari visi misi Presiden.
                    </p><p class="font_kecil">Dana aspirasi rawan penyelewengan dan efeknya pun bisa besar apalagi besarannya mencapai Rp20 miliar per anggota dewan maka bisa Rp11 triliun efeknya bagi pembangunan.'
    ],
    'KRONOLOGI'=>[
        'narasi'=>'Setiap DPR mengajukan dana atau anggaran tambahan selalu memicu polemik dan prokontra. Dana-dana yang diminta dinilai tidak relevan. Selama tahun 2015 saja DPR kerap mengajukan tambahan anggaran atas nama kepentingan rakyat dan atas yang memicu kontroversi. Berikut daftar dana-dana yang diajukan oleh DPR :',
        'list'=>[
            ['date'=>'Januari  2015 – Tambahan Tenaga Ahli','content'=>'Anggaran sebesar Rp 1.635 triliun yang berasal dari APBNP 2015 dikucurkan untuk menambah tenaga ahli dan untuk kebutuhan kesekretariatan DPR. Sebelumnya setiap anggota DPR dibantu dua tenaga ahli dan sekretaris. Dengan adanya usulan tersebut, nantinya setiap anggota DPR akan mendapat dua tambahan tenaga ahli.'],
            ['date'=>'Februari 2015 – Dana Rumah Asipirasi','content'=>'Usulan dana rumah aspirasi memicu kontroversi. DPR mengajukan usulan tersebut secara tiba-tiba tanpa ada alasan yang kuat. Dengan dana rumah aspirasi, nantinya setiap anggota DPR akan mendapat suntikan dana Rp 150 juta per tahun, setara dengan Rp 12,5 juta per bulan untuk dana rumah aspirasi. Dana rumah aspirasi itu dialokasikan untuk biaya sewa rumah dan keperluan operasional rumah aspirasi.'],
            ['date'=>'April 2015 – Polisi Parlemen','content'=>'Diam-diam DPR mengusulkan untuk membentuk polisi parlemen. Nantinya, seorang brigadir jenderal polisi akan memimpin 1.194 polisi dan PNS. Saat ini MPR diperkuat 29 anggota pengamanan dalam (pamdal), DPR dijaga 489 anggota pamdal, dan DPD oleh 50 anggota pamdal. Namun rencana yang akan menghabiskan dana miliaran itu tenggelam karena memperoleh penolakan publik termasuk polisi sendiri.'],
            ['date'=>'Mei 2015 - Alun-Alun Demokrasi','content'=>'Pembangunan Alun-alun Demokrasi di komplek Parlemen-Senayan diresmikan pada hari Kamis (21/5/2015). Dengan dalih dibangunnya alun-alun demokrasi, menurut para anggota dewan alun-alun berfungsi untuk ruang publik bagi rakyat, dan memfasiltasi ruang bagi pendemo. Pembangunan alun-alun demokrasi ini menarik perhatian dari mana sumber anggaran pembangunan tersebut.'],
            ['date'=>'Juni 2015 – Dana Aspirasi 20 Miliar','content'=>'Dana sebesar Rp 20 miliar untuk setiap anggota dewan dengan total Rp 11,2 triliun  sudah disahkan oleh DPR. Melalui rapat paripurna, mayoritas anggota DPR menyetujui program dana asprasi. Dana tersebut akan diajukan untuk dimasukkan dalam RAPBN 2016. Namun, sejauh ini usulan tersebut masih mendapat penolakan dari pemerintah dan publik.']
        ]
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaidemokrat5119a5b44c7e4'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1']
        ],
        'institusi'=>[
            ['name'=>'KPK','img'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/KPK_Logo.svg/915px-KPK_Logo.svg.png','url'=>'http://www.bijaks.net/aktor/profile/komisipemberantasankorupsi5192fb408b219'],
            ['name'=>'Indonesian Corruption Watch (ICW)','img'=>'http://ethixbase.com/wp-content/uploads/2014/08/ICW-x-250x1801.png','url'=>'http://www.bijaks.net/aktor/profile/icwindonesiancorruptionwatch534e45d49c5d9'],
            ['name'=>'Lingkar Madani untuk Indonesia (LIMA)','img'=>'http://www.zonalima.com/images/view/-Ray%20Rangkuti.jpg','url'=>''],
            ['name'=>'Forum Indonesia untuk Transparansi Anggaran (FITRA)','img'=>'http://nttonlinenow.com/images/stories/2011/april-mei2012/fitra.jpg','url'=>''],
            ['name'=>'Forum Masyarakat Peduli Parlemen Indonesia (Formapi)','img'=>'http://www.luwuraya.net/wp-content/uploads/2011/10/frmappi.jpg','url'=>''],
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Aburizal Bakrie','jabatan'=>'ketua umum Partai Golkar','img'=>'https://vensca81.files.wordpress.com/2014/03/bakrie.jpg','url'=>'http://www.bijaks.net/aktor/profile/aburizalbakrie514662d3c6827','content'=>'Ini cara agar DPR itu bekerja, bekerja membela daerahnya. Jika tidak kerja maka mereka bukan anggota dewan yang baik'],
        ['from'=>'Fahri Hamzah','jabatan'=>'wakil ketua DPR RI','img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150520_065925_harianterbit_Fahri_Hamzah.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09','content'=>'Jadi jangan takut dana dicuri karena penggunaannya dilaporkan. Yang dicuri itu yang lalu-lalu'],
        ['from'=>'Ahmadi Noor Supit','jabatan'=>'ketua badan anggaran DPR RI','img'=>'http://cms.monitorday.com/statis/dinamis/detail/7665.jpg','url'=>'http://www.bijaks.net/aktor/profile/Ahmadi','content'=>'Logika menolaknya apa? Barangnya saja belum ada. Saya yakin presiden mendapat informasi yang tidak benar kalau ada kata-kata menolak'],
        ['from'=>'Desmond J Mahesa','jabatan'=>'Fraksi Gerindra','img'=>'http://kulitinta.com/wp-content/uploads/2015/04/Desmond-J-Mahesa-Pembangunan-Gedung-Baru-DPR.jpg','url'=>'http://www.bijaks.net/aktor/profile/desmondjunaidimahesa50f91e722d81d','content'=>'da aspirasi yang belum diwadahi, disitu kami harus diberi semacam nama dan dicari wadahnya'],
        ['from'=>'M. Qodari','jabatan'=>'Pengamat politik dari Indobarometer','img'=>'http://rmol.co/images/berita/normal/82645_06545909102014_M_Qodari.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadqodari55957f2f47214','content'=>'Saya kira pemerintah harus legowo dan melihat kepentingan yang lebih luas. Mau tidak mau pemerintah harus meloloskan dana aspirasi'],
        ['from'=>'Edhie Baskoro Yudhiono','jabatan'=>'Fraksi Partai Demokrat','img'=>'http://www.ceritamu.com/uploads/posts/HBC/files/C1157B4F-24C6-48C4-9148-557EA942AE1C.jpg','url'=>'http://www.bijaks.net/aktor/profile/edhiebaskoroyudhoyonobcommsc50f54a548f1ed','content'=>'Masyarakat harus melihat ini secara utuh. Jangan sampai melihat anggota DPR memegang dana langsung, itu salah besar'],
        ['from'=>'Bambang Soesatyo','jabatan'=>'anggota banggar dari fraksi Golkar','img'=>'http://voiceofjakarta.co.id/wp-content/uploads/2015/05/bambang-soesatyo1.jpg','url'=>'http://www.bijaks.net/aktor/profile/hbambangsoesatyosemba50f8fbabbfadf','content'=>'Realisasinya dalam bentuk program sesuai kebutuhan daerah masing-masing dan akan ada audit untuk itu'],
        ['from'=>'Yandri Susanto','jabatan'=>'sekretaris Fraksi PAN','img'=>'http://lintaspos.com/wp-content/uploads/2015/06/Yandri.jpg','url'=>'http://www.bijaks.net/aktor/profile/yandrisusanto52e5d5a9a47a8','content'=>'Rp 20 Miliar bentuk kami peduli pada terhadap dapil. Terkadang mereka tidak punya akses untuk itu'],
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Pratikno','jabatan'=>'Menteri Sekretaris Negara (Mensesneg)','img'=>'http://www.rmol.co/images/berita/normal/276459_09470108042015_5-201502062024461.jpg','url'=>'http://www.bijaks.net/aktor/profile/pratikno540d1bf323949','content'=>'Presiden mengharapkan semua pihak untuk ikut prihatin dengan kondisi rakyat, berhati-hati dalam memanfaatkan anggaran semaksimal mungkin, seefektif mungkin'],
        ['from'=>'Jhonni G Plate','jabatan'=>'Wakil ketua Fraksi partai NasDem','img'=>'http://images1.rri.co.id/thumbs/berita_156902_800x600_Jhonny.JPG','url'=>'http://www.bijaks.net/aktor/profile/johnnygplate5243d1a42a8c3','content'=>'Kami meminta pada Presiden Jokowi untuk tidak mengakomadasi in  ke dalam APBN'],
        ['from'=>'Donal Fariz','jabatan'=> 'Peneliti Indonesia Corruption Watch (ICW)','img'=>'http://img.jogjakartanews.com/20141123194025_baharuddin_kamba_2.jpg','url'=>'','content'=>'Desain-desain seperti ini sering dipergunakan sebelum pilkada, ada upaya mendapatkan feed back yang potensial diterima anggota DPR jika itu benar-benar direalisasikan'],
        ['from'=>'Indiarto Seno Adji Plt','jabatan'=>'Wakil Ketua KPK','img'=>'http://beritabuana.co/fileuploadmaster/950640seno%20adji.jpg','url'=>'http://www.bijaks.net/aktor/profile/indriyantosenoadji53d8ba6a501a7','content'=>'DPR sebaiknya menjelaskan secara transparan mengenai tujuan dana aspirasi itu. Jangan sampai dana aspirasi memiliki potensi dan celah terjadinya korupsi'],
        ['from'=>'Basuki Tjahaja Purnama (Ahok)','jabatan'=>'Gubernur DKI Jakarta','img'=>'https://cgnpolitik.files.wordpress.com/2014/04/screenshot_2014-04-07-05-15-23-1.png?w=640','url'=>'http://www.bijaks.net/aktor/profile/basukitjahajapurnama50f600df48ac5','content'=>'Kalau kayak gitu, fungsi musrenbang jadi kacau-balau. Tetapi, kalau DPR sudah memutuskan, ya mau enggak mau, kita mesti ikut. Saya sih enggak setuju, aneh banget'],
        ['from'=>'Bambang P. Brodjonegoro','jabatan'=>'Menteri Keuangan RI','img'=>'http://katadata.co.id/sites/default/files/konten/2015/01/Bambang-Brodjonegoro-Menkeu-Katadata-Arief2.jpg','url'=>'http://www.bijaks.net/aktor/profile/bambangbrodjonegoro51a7f8ee9f40b','content'=>'Pembahasan anggaran harus sesuai ketentuan dan tidak ada penambahan anggaran yang baru. Jadi kalau mau dicoba pun harus mengikuti aturan yang ada'],
        ['from'=>'Apung Widadi','jabatan'=>'Kordinator Advokasi dan Investigasi FITRA','img'=>'http://bolanasional.co/wp-content/uploads/2015/05/Apung-Widadi.jpg','url'=>'','content'=>'Untuk itu, kami dengan tegas  menolak Dana Aspirasi masuk dalam RAPBN 2016'],
        ['from'=>'Andrinof Chaniago','jabatan'=>'Menteri Perencanaan Pembangunan Nasional','img'=>'https://fokusterkinidotcom.files.wordpress.com/2014/10/menteri-perencanaan-pembangunan-negara-kepala-bappenas-andrinof-chaniago.jpg','url'=>'http://www.bijaks.net/aktor/profile/andrinofachaniago52aa74d77395b','content'=>'Kalau berdasarkan UU, perencanaan program pembangunan diambil dari visi misi Presiden. Jadi kalau pakai konsep dana aspirasi, bisa bertabrakan dengan visi misi Presiden'],
        ['from'=>'Baharuddin Kamba','jabatan'=>'Jogja Corruption Watch ','img'=>'https://upload.wikimedia.org/wikipedia/id/archive/6/6d/20141128000621!Ray_Rangkuti.jpg','url'=>'http://www.bijaks.net/aktor/profile/rayrangkuti525f3c7e380f7','content'=>'Kami sangat khawatir, sampai daerah, dana itu jadi bancakan dan hanya untuk kelompok tertentu'],
        ['from'=>'Ray Rangkuti','jabatan'=>'Direktur Eksekutif LIMA','img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/06/24/139802/NMCa0vQsjL.jpg?w=668','url'=>'http://www.bijaks.net/aktor/profile/donalfariz559574981daf0','content'=>'Ini namanya pengeluaran macam apa? Uang disediakan dulu programnya menyusul'],
        ['from'=>'Uchok Sky Khadafi','jabatan'=>'Direktur for Center Budget of Analysis ','img'=>'http://nawaberita.com/wp-content/uploads/2015/04/uchok.jpg','url'=>'http://www.bijaks.net/aktor/profile/uchokskykhada531fe1572d14a','content'=>'Kalau DPR itu betul serius ingin bantu rakyat, caranya bukan melalui dana aspirasi. Tapi dana desa tuh diperjuangkan sama, satu desa Rp 1 miliar lebih'],
    ],
    'VIDEO'=>[
        ['id'=>'HadkxhZynSk'],
        ['id'=>'H-WrVOHVb-g'],
        ['id'=>'XJu2Tood6N4'],
        ['id'=>'3hb5MGzxJNI'],
        ['id'=>'sk0WkHUHZIo'],
        ['id'=>'BEq_l8EhjAU'],
        ['id'=>'-a04XMamNNQ'],
        ['id'=>'VZynIVUtbKE'],
        ['id'=>'yn51rELJX4U'],
        ['id'=>'pPxvC39afaQ'],
        ['id'=>'xXEtquFbhJg'],
        ['id'=>'6vYr9KEMty4'],
        ['id'=>'TzGCYxNYYF8']
    ],
    'FOTO'=>[
        ['img'=>'http://www.fiskal.co.id/img/news/1435205238golkar-minta-dana-aspirasi-598x422.jpg'],
        ['img'=>'http://www.sabangnews.info/wp-content/uploads/2015/01/Dana-Aspirasi.jpg'],
        ['img'=>'http://beritakabar.com/wp-content/uploads/2015/06/dana_aspirasi_dpr.jpg'],
        ['img'=>'http://sp.beritasatu.com/media/images/original/20150616165805335.jpg'],
        ['img'=>'https://scontent.cdninstagram.com/hphotos-xfa1/t51.2885-15/s320x320/e15/11430224_876389189083380_1130609759_n.jpg'],
        ['img'=>'http://sp.beritasatu.com/media/images/original/20150618182251972.jpg'],
        ['img'=>'http://www.aktual.com/wp-content/uploads/2015/06/Aksi-Tolak-Dana-Aspirasi-1.jpg'],
        ['img'=>'http://radaronline.co.id/wp-content/uploads/HI-Dana-Aspirasi.jpg'],
        ['img'=>'http://www.aktualpost.com/wp-content/uploads/2015/06/demo-tolak-dana-aspirasi.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/05/08/591f1d52-570c-49a1-860d-430dec06d504_169.jpg?w=650'],
        ['img'=>'http://store.tempo.co/cover/medium/koran/2015/06/26/dana-aspirasi-tabrak-banyak-aturan.jpg'],
        ['img'=>'http://i.ytimg.com/vi/cGqcR3Y2jZw/0.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/06/16/405126/KoOj6AY4LA.jpg?w=668'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/06/24/1420235011-fot01.JPG20-780x390.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/06/19/445339/nasdem-dana-aspirasi.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/demo-tolak-dana-aspirasi-anggota-dpr_20150619_121757.jpg'],
        ['img'=>'http://metroterkini.com/asset/foto_berita/09dec042b8c239c27718fd119a16a999.jpg'],
        ['img'=>'http://statik.tempo.co/?id=412995'],
        ['img'=>'http://cdnimage.terbitsport.com/image.php?width=650&image=http://cdnimage.terbitsport.com/imagebank/gallery/large/20150629_113841_harianterbit_Dana_Aspirasi.jpg'],
        ['img'=>'http://fajar.co.id/wp-content/uploads/2015/06/Ilustrasi-DPR-RI-Sultra-Dana-Aspirasi.jpg'],
        ['img'=>'http://analisadaily.com/assets/image/news/big/2015/06/dana-aspirasi-demi-kepentingan-wakil-rakyat-143735-1.jpg'],
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/danaaspirasi/top.png")?>') no-repeat transparent;
        height: 1352px;
        margin-bottom: -655px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .font_kecil {
        font-size: 13px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
        background: rgba(0, 0, 0, 0.7);
    }
    .block_red {
        background-color: #761908;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 80%;
        height: auto;
        margin: 0 auto;
        display: inherit;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        margin-right: 10px;
        max-width: 50px;
    }
    .garis {
        border-top: 1px dotted black;
    }
    .boxgray {
        width: 99%;
        border: 5px solid lightgray;
        box-shadow: -5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }
    .penyokong {
        width: 100%;
        height: auto;
        display: inline-block;
        margin-left: 20px;
    }
    .boxpenyokong {
        width: 126px;
        height: auto;
    }
    .foto {
        width: 100px;
        height: 100px;
        border: 3px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
        padding: 10px 10px;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 121px;height: 115px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 110px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi2 img {width: 98px; height: 70px;  padding: 0px !important;}
    li.organisasi2 p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 121px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
    li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.dukung {float: left; padding:3px;vertical-align: top;width: 177px;height:115px;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
    li.dukung img {width: auto; height: 75px;padding: 0px !important;margin: 0 auto;display: block;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;line-height: 12px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }   
    .uprow {margin-left: 20px;margin-top:10px;border-color: #29166f transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {
        width: 260px;
        height: auto;
        margin-left: 20px;
        margin-top:0px;
        margin-bottom: 10px;
        background-color: #424040;
        background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px #29166f;
        border-right:solid 2px #29166f;
        border-bottom:solid 2px #29166f;
        border-radius: 0 0 5px 5px;
        color: #ffffff;
        z-index: 100;
    }
    .kronologi-title {font-size:14px;font-weight:bold;background-color: #29166f;color: white;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {line-height: 15px;font-size:12px;}

    .ketua {
        background-color: yellow;
        width: 30%;
        display: block;
        float: left;
        margin-right: 3%;
        box-shadow: -3px 3px 10px gray;
        border-radius: 8px;
        margin-bottom: 15px;
    }
    .ketua img {
        width: 100%;
        height: 200px;
    }
    .kritik {
        font-size: 18px;
        font-weight: bold;
        color: black;
    }
    .isi-col50 {
        float: left;
        margin: 10px 16px;
        width: 41%;
        height: auto;
        display: inline-block;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #29166f;
        width: 293px;
        height: 345px;
        float: left;
        margin-bottom: 10px;
        padding-top: 5px;
        margin-left: 10px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        float: left;
        border: 1px solid black;
    }
    #bulet {
        background-color: #ffff00; 
        text-align: center;
        width: 50px;
        height: 25px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: black;
        padding: 6px 10px;
        margin-left: -10px;
        margin-right: 10px;
    }

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <h3 style="font-size: 23px;"><a id="pssivskemenpora" class="black"><?php echo strtoupper($data['NARASI']['title']);?></a></h3>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="polemik" style="color: black;">ANALISA</a></h4>
                <p class="font_kecil"><?php echo $data['ANALISA']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="polemik" style="color: black;">PRO DAN KONTRA</a></h4>
                <p class="font_kecil"><?php echo $data['PROKONTRA']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="polemik" style="color: black;">MENABRAK VISI PRESIDEN</a></h4>
                <p class="font_kecil"><?php echo $data['MENABRAK']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENDUKUNG</p>
                <ul style="margin-left: 0px;">
                    <?php
                    foreach($data['PENDUKUNG']['partai'] as $key=>$val) {
                        $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                        $personarr = json_decode($person, true);
                        $pageName = strtoupper($personarr['page_name']);
                        if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                            $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                        }
                        else {
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                        }
                        ?>
                        <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                            <li class="organisasi">
                                <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                <p><?php echo $pageName;?></p>
                            </li>
                        </a>
                    <?php
                    }
                    ?>
                </ul>
                <div class="clear"></div>

                <h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENENTANG</p>
                <ul style="margin-left: 0px;">
                    <?php
                    foreach($data['PENENTANG']['partai'] as $key=>$val) {
                        $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                        $personarr = json_decode($person, true);
                        $pageName = strtoupper($personarr['page_name']);
                        $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                        ?>
                        <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                            <li class="organisasi">
                                <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                <p><?php echo $pageName;?></p>
                            </li>
                        </a>
                    <?php
                    }
                    ?>
                </ul>
                <div class="clear"></div>

                <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENENTANG</p>
                <ul style="margin-left: -10px;">
                    <?php
                    foreach($data['PENENTANG']['institusi'] as $key=>$val) {
                        ?>
                        <a href="<?php echo $val['url'];?>">
                            <li class="organisasi2">
                                <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                <p><?php echo $val['name'];?></p>
                            </li>
                        </a>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="col_kanan">
                <div class="boxprofile white">
                    <h4 class="block_red text-center"><a id="profil" style="color: white;">PASAL 80 UU MD3</a></h4>
                    <img src="http://kebebasaninformasi.org/wp-content/uploads/2015/02/UU-MD3.jpg" class="picprofil"><br>
                    <p style="margin-left: 20px;margin-right: 20px;" class="font_kecil"><?php echo $data['PROFIL']['narasi'];?></p>
                </div><br>

                <h4 class="list" style="margin-left: 20px;"><a id="kronologi" style="color: black;">DANA KONTROVERSI</a></h4>
                <p class="font_kecil" style="margin-left: 20px;margin-right: 17px;"><?php echo $data['KRONOLOGI']['narasi'];?></p>
                <?php 
                foreach ($data['KRONOLOGI']['list'] as $key => $value) {
                ?>
                <div class="uprow"></div>
                <div class="kronologi">
                    <div class="kronologi-title"><?php echo $value['date'];?></div>
                    <div class="kronologi-info">
                        <p><?php echo $value['content'];?></p>
                    </div>
                </div>
                <div class="clear"></div>
                <?php
                }
                ?>

                <h4 class="list" style="margin-left: 20px;margin-top: 25px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="newsdanaaspirasi_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newsdanaaspirasi_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
                <div class="clear"></div>
                    <ul style="margin-left: 0px;margin-top: 10px;">
                        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
                            <?php 
                            for($i=0;$i<=3;$i++){
                            ?>
                            <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 77%;margin-right: 10px;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div style="float: left;width: 47%;margin-left: 20px;">
                            <?php 
                            for($i=4;$i<=7;$i++){
                            ?>
                            <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 78%;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </ul>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENENTANG</a></h4>
                <div class="clear"></div>
                <ul style="margin-left: 0px;margin-top: 10px;">
                    <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
                        <?php 
                        for($i=0;$i<=3;$i++){
                        ?>
                        <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 77%;margin-right: 10px;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div style="float: left;width: 47%;margin-left: 20px;">
                        <?php 
                        for($i=4;$i<=7;$i++){
                        ?>
                        <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 78%;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                </ul>
            </div>
            <div class="clear"></div>

            <h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
            <div class="col_full" style="display: inline-block;">
                <ul style="margin-left: 0px;margin-top: 0px;">
                    <?php
                    $no=1;
                    foreach ($data['FOTO'] as $key => $val) { ?>
                        <li class="gallery">
                            <a href="#" data-toggle="modal" data-target="#img-<?php echo $no;?>">
                                <img src="<?php echo $val['img'];?>" />
                            </a>
                        </li>
                        <div class="modal hide fade" style="overflow-y: scroll;height: 550px;width: auto;" id="img-<?php echo $no;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Gallery Foto</h4>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?php echo $val['img'];?>" />
                                    </div>
                                    <!-- <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    <?php
                    $no++;
                    }
                    ?>
                </ul>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 10px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>
            <div class="col_full">
                <div class="boxgray" style="height: 220px;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>