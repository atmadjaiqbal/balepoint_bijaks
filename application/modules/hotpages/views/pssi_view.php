<?php 
$data = [
    'NARASI'=>[
        'title'=>'LONCENG KEMATIAN SEPAKBOLA INDONESIA',
        'narasi'=>'<img src="http://images.cnnindonesia.com/visual/2014/10/16/7306dc03-25d2-416a-a717-170905f2150c_169.jpg?w=650" class="pic">Nasib persepakbolaan Indonesia berada diujung kehancuran. Organisasi sepakbola dunia, FIFA secara resmi memberikan sanksi terhadap PSSI. Surat yang dirilis oleh FIFA tersebut ditandangai oleh sekjen FIFA, Jerome Valcke dengan dasar hukuman yaitu pelanggaran statuta FIFA, yakni intervensi pemerintah terhadap aktivitas PSSI melalui Kemenpora.
                    </p><p><img src="http://img.bisnis.com/posts/2015/05/31/438914/imam-nachrowi-menpora-2.jpg" class="pic2">Adapun sanksi yang diterima PSSI adalah berupa skorsing sampai batas waktu yang tidak ditentukan di luar ajang SEA Games  2015 di Singapura. Sanksi tersebut  merupakan klimaks dari polemik yang berlarut-larut di dalam tubuh PSSI. Dan sekaligus menjadi tamparan keras bagi persepakbolaan Indonesia, karena Timnas Indonesia tidak diperbolehkan mengikuti semua ajang kompetisi Internasional. Akibatnya peringkat Timnas Indonesia akan semakin melorot. Klub-klub Indonesia juga terancam gulung tikar akibat vakumnya kompetisi liga. Selain itu, nasib para pemain menjadi tidak menentu.
                    </p><p>Kondisi persepakbolaan Indonesia semakin terpuruk setelah menguatnya dugaan pengaturan skor Timnas U23 di ajang SEA Games Singapura.  Beberapa pihak diantaranya para pemain, manajer, pengurus PSSI, dan bahkan disebut-sebut ada oknum Kemenpora yang terlibat.'
    ],
    'PROFIL'=>[
        'title'=>'PERSATUAN SEPAKBOLA SELURUH INDONESIA',
        'narasi'=>'Persatuan Sepakbola Seluruh Indonesia (PSSI)adalah organisasi  induk yang bertugas mengatur kegiatan olahragasepak bola di Indonesia. PSSI berdiri pada tanggal 19 April 1930 dengan nama awal Persatuan Sepak Raga Seluruh Indonesia.PSSI bergabung dengan FIFA pada tahun 1952, kemudian dengan AFC pada tahun 1954. PSSI menggelar kompetisi Liga Indonesia setiap tahunnya, dan sejak tahun 2005, diadakan pula Piala Indonesia'
    ],
    'POLEMIK'=>[
        'narasi'=>'<img src="http://cdn.sindonews.net/dyn/620/content/2015/05/22/58/1004244/ini-dia-isi-curhatan-la-nyalla-kepada-menpora-t78.jpg" class="pic">Setelah FIFA memberikan sanksi terhadap PSSI, kemenpora dihujani kritik dari berbagai kalangan. Kemenpora dianggap paling bertanggungjawab atas jatuhnya sanksi keras tersebut. Sejak awal, kemenpora memang mengintervensi PSSI dengan membekukan PSSI di bawah kepengurusan La Nyalla Mahmud, Ketua PSSI hasil Kongres Luar Biasa (KLB) Surabaya.
                    </p><p class="font_kecil">Keputusan Kemenpora tersebut merupakan langkah tegas atas kisruh yang terjadi di liga indonesia. Kisruh tersebut terkait keikutsertaan Arema Cronus dan Persebaya dalam kompetisi liga Indonesia. Dua Klub asal jawa timur tersebut tetap diikut sertakan oleh PSSI untuk berkompetisi di ajang liga, padahal Badan Olahraga Profesional Indonesia (BOPI) menyatakan tidak lolos persyaratan peserta kompetisi karena masalah legalitas dan dualisme kepemilikan klub. Tindakan tersebut menyulut protes dari Ketua PSSI La Nyalla Mahmud Mattalitti.
                    </p><p class="font_kecil"><img src="http://www.sindotrijaya.com/uploads/news/resize3/320473.jpg" class="pic2">Di tengah kondisi PSSI yang tak menentu dan ancaman kematian sepakbola Indonesia akibat sanksi dari FIFA. Kemenpora membentuk Tim Transisi untuk menghidupkan kembali persepakbolaan Indonesia. Hal tersebut juga merupakan upaya solutif untuk mereformasi organisasi PSSI yang diduga sebagai sarang mafia bola. Mafia bola kembali menjadi isu hangat di persepakbloaan Indonesia setelah Timnas U23 yang berlaga di ajang SEA Games menuai hasil buruk. Diduga dua kekalahan mencolok  atas Thailand dan Vietnam  karena adanya pengaturan skor.'
    ],
    'KONTROVERSI2'=>[
        'narasi'=>'<img src="http://cdn.metrotvnews.com/dynamic/content/2015/05/18/127216/l6fLBDY8P7.jpg?w=668" class="pic">FIFA menjatuhkan sanksi berat terhadap PSSI karena alasan adanya intervensi dari pemerintah. Pemerintah terlalu ikut campur, yang membuat PSSI tidak memiliki kewenangan penuh dalam membenahi persoalan yang terjadi di dalam sepakbola Indonesia. PSSI merupakan organisasi sepakbola yang hanya tunduk pada otoritas di atasnya, FIFA dan AFC.
                    </p><p class="font_kecil">Setiap persoalan yang terjadi di PSSI tidak boleh diintervensi oleh pihak manapun, termasuk pemerintah.  PSSI harus menyelesaikan masalahnya sendiri dibawah arahan FIFA sesuai dengan statuta.
                    </p><p class="font_kecil"><img src="http://cdn1-a.production.liputan6.static6.com/medias/863040/big/027256700_1430141934-Pertemuan_Menpora_dengan_PT_Liga__foto_1_.jpg" class="pic2">Namun, kenyataannya pemerintah tetap memaksakan diri untuk mengintervensi polemik yang terjadi di tubuh PSSI. Melalui kemenpora, pemerintah membekukuan PSSI  Akibat dari tindakan tersebut, FIFA memberikan sanksi berat berupa dicabutnya keanggotaan Indonesia di FIFA dan larangan mengikuti semua ajang yang diselenggarakan oleh FIFA maupun AFC.
                    </p><p class="font_kecil">Kemenpora kemudian membentuk Tim Transisi yang bertugas untuk mengambil alih kewenangan PSSI.  Pembentukan Tim yang diisi 17 nama tersebut justru menuai kontroversi. Pertama, kemenpora tidak memiliki hak dan kewenangan untuk membentuk Tim untuk mengambil alih tugas  PSSI.  Hanya FIFA yang berhak melakukan hal tersebut. Seperti halnya FIFA pernah membentuk  Komite Normalisasi pada tahun 2011 ketika kisruh soal kepemimpinan   Nurdin Halid yang tidak diakui oleh FIFA.
                    </p><p class="font_kecil">Kedua, Tim Transisi diisi oleh orang yang tidak kredibel dan tidak memiliki latar belakang tentang sepakbola.  Ditakutkan Tim tersebut tidak dapat menyelesaikan masalah di PSSI dan justru malah memperkeruh suasana.
                    </p><p class="font_kecil"><img src="http://www.bolaindo.com/wp-content/uploads/2015/05/Frank-Rijkaard-Kandidat-CEO-Operator-Kompetisi-Bentukan-Tim-Transisi.jpg" class="pic">Ketiga, keberadaan Tim Transisi justru sebagai bentuk nyata dari pemaksaan yang dilakukan oleh pemerintah terhadap sepakbola.
                    </p><p class="font_kecil">Menanggapi kontroversi tersebut, Menpora Imam Nahrawi memiliki jawabannya sendiri, Tim yang dibentuknya nanti akan mampu menjalankan tugasnya dengan baik. Imam berkilah, pilihannya dinilai cukup kredibel.  Tim tersebut akan menjalankan empat tugas pokok. Diantaranya melaksankan pemilihan ketua umum baru, dan akan menggulirkan kompetisi agar sepakbola Indonesia bisa hidup lagi. Dan yang paling utama adalah sanksi FIFA bisa segera dicabut.'
    ],
    'MAFIA'=>[
        'narasi'=>'Skandal mafia sepakbola kembali mencoreng wajah sepakbola Indonesia. Belum selesai masalah sanksi FIFA yang diterima oleh PSSI. Skandal suap ini mencuat kepermukaan setelah Timnas Indonesia mengalami dua kekalahan yang cukup mencolok di ajang SEA Games 2015 Singapura.
                    </p><p class="font_kecil"><img src="http://1.bp.blogspot.com/-sfRWXnmqAxE/VYDTzkvcBwI/AAAAAAABGpg/4yXOrMfmciw/s1600/menpora_pengaturan%2Bskor%2Bbola.jpg" class="pic2">Diduga Timnas sengaja mengalah atas Vietnam dan Thailand karena sebelumnya sudah ada kongkalikong antar Bandar dan pengatur skor.  Bandar judi di Malaysia yang berinisial BS ditengarai mengatur pertandingan. BS memiliki kaki-tangan di Indonesia dalam menjalankan praktek sogok-menyogok di sepakbola tersebut.
                    </p><p class="font_kecil">Beberapa anggota mafia merupakan mantan pelatih dan pemain Persatuan Sepak Bola Seluruh Indonesia. "Bukan langsung orang PSSI, tapi berkaitan dengan PSSI." kata Kepala Bidang Penanganan Kasus Lembaga Bantuan Hukum Jakarta Muhammad Isnur.
                    </p><p class="font_kecil">Sebelumnya, Biro Investigasi Korupsi Singapura atau Corrupt Practices Investigation Bureau (CPIB) Singapura menangkap warga negara Singapura dan beberapa warga negara lain yang diduga terkait dengan pengaturan pertandingan.
                    </p><p class="font_kecil"><img src="http://www.bolabanget.com/img_proxy.php?id=85884227" class="pic">Pertandingan yang akan diatur adalah pertandingan sepak bola SEA Games ke-28 yang telah dimulai pada Jumat, 29 Mei 2015, di Singapura. Pengungkapan ini bermula dari informasi yang diterima otoritas setempat soal praktek pengaturan pertandingan.
                    </p><p class="font_kecil">Di Indonesia sendiri , praktek suap mafia sepakbola sudah berlangsung sejak tahun 2000an. Selama 15 tahun terakhir, sepakbola Indonesia banyak dikendalikan oleh para mafia. Bahkan sebelum liga digulirkan sudah ada skenario dalam menentukan skor dan pemenang pertandingan. Tentu, skandal ini secara langsung ikut mempengaruhi kualitas sepakbola di Indonesia.'
    ],
    'ULTIMATUM'=>[
        'narasi'=>'Organisasi sepakbola dunia FIFA berekasi atas kisruh yang terjadi antara kemenpora dan PSSI. FIFA mengeluarkan ultimatum terhadap Kemenpora untuk tidak  ikut campur mengurusi persoalan yang terjadi di liga Indonesia.
                    </p><p class="font_kecil"><img src="http://cdn-media.viva.id/thumbs2/2015/04/13/307169_logo-di-markas-fifa_663_382.jpg" class="pic">Dalam surat yang dikirimkan ke Menteri Imam Nahrawi, FIFA mengultimatum bahwa PSSI harus mengelola urusan mereka secara independen dan tanpa pengaruh dari pihak ketiga, seperti diatur dalam Pasal 13 dan 17 Statuta FIFA.
                    </p><p class="font_kecil">FIFA juga mengingatkan bahwa hanya anggota FIFA  yang bisa memberi lisensi dan bertanggung jawab mengatur dan memaksakan kriteria yang harus dipenuhi klub yang berpartisipasi sesuai yang tercantum dalam poin 2 dan 3 di Peraturan Perizinan Klub FIFA.
                    </p><p class="font_kecil"><img src="https://aws-dist.brta.in/2012-07/e3025e377013c0fb87f166e45386e93e.jpg" class="pic2">Selanjutnya, FIFA menegaskan bahwa Pemerintah tidak memiliki kewenangan untuk mengintervensi badan sepokbola dengan memaksakan kriteria terhadap klub-klub yang akan mengikuti kompetisi liga domesti. FIFA mengancam akan memberikan sanksi yang berat berupa terhadap PSSI apabila persoalan tersebut tidak segera diselesaikan dan mengembalikan kewenangan PSSI dalam mengelola kompetisi sesuai dengan ketentuan yang berlaku.'
    ],
    'KONTROVERSI'=>[
        ['title'=>'Kasus korupsi Nurdin Halid','no'=>'Pada 13 Agustus 2007, Ketua Umum Nurdin Halid divonis dua tahun penjara akibat tindak pidana korupsi dalam pengadaan minyak goreng. Nurdin Halid kemudian dipenjara ats kasus tersebut, namun posisi sebagai ketua umum tetap ia pegang, dan tetap menjalankan kepemimpinan PSSI dari dalam penjara.'],
        ['title'=>'Reaksi atas LPI','no'=>'Pada Oktober 2010, Liga Primer Indonesia dideklarasikan di Semarang oleh Konsorsium dan 17 perwakilan klub. Namun LPI akhirnya mendapatkan izin dari pemerintah melalui Menteri Pemuda dan Olahraga Andi Mallarangeng'],
        ['title'=>'Kisruh dan Pembentukan komite Normalisasi','no'=>'Kisruh di PSSI semakin menjadi-jadi semenjak munculnya LPI. Ketua Umum Nurdin Halid melarang segala aktivitas yang dilakukan oleh LPI.  Pada 1 April 2011, Komite Darurat FIFA memutuskan untuk membentuk Komite Normalisasi yang akan mengambil alih kepemimpinan PSSI. FIFA juga menyatakan bahwa 4 orang calon Ketua Umum PSSI yaitu Nurdin Halid, Nirwan Bakrie, Arifin Panigoro, dan George Toisutta tidak dapat mencalonkan diri sebagai ketua umum sesuai dengan keputusan Komite Banding PSSI tanggal 28 Februari 2011. Selanjutnya, FIFA mengangkat Agum Gumelar sebagai Ketua Komite Normalisasi PSSI. Pada tanggal 9 Juli 2011 diadakan Kongres Luar Biasa di Solo, Djohar Arifin Husin terpilih sebagai Ketua Umum PSSI periode 2011-2015.'],
        ['title'=>'Pemecatan Alfred Riedl','no'=>'Pemecatan dan penunggakan gaji Alfred Riedl menimbulkan hal yang kontroversial karena pihak PSSI mengaku bahwa Alfred Riedl dikontrak oleh Nirwan Bakrie dan bukan oleh PSSI akan tetapi Alfred Riedl membantah hal tersebut dan membawa persoalan ini ke FIFA dan kasus ini belum terselesaikan.'],
        ['title'=>'Kisruh Indonesia Premier league','no'=>'Pergantian kepengurusan Ketua umum PSSI dari Nurdin Halid ke Djohar Arifin Husin dimulai era kompetisi baru. Dalam pembentukan IPL banyak masalah yang terjadi karena aturan-aturan yang ditetapkan oleh PSSI.Pembentukan IPL mendapat tekanan dari 12 klub sepak bola atau kelompok 14 karena kompetisi berjumlah 24 klub dan 6 klub diantaranya langsung menjadi klub IPL']
    ],
    'DAMPAK'=>[
        'narasi'=>'<img src="http://cdn.sindonews.net/dyn/620/content/2015/06/03/58/1008494/7-dampak-buruk-hukuman-fifa-terhadap-sepak-bola-indonesia-O1Z.JPG" class="pic2">Efek dari sanksi FIFA tentu sangat merugikan sepakbola Indonesia. Dampak dari sanksi tersebut membuat Indonesia kehilangan hak keanggotaannya di FIFA sehingga sepakbola indonesia akan semakin lesu. Berikut daftar kerugian yang dialami sepakbola Indonesia akibat sanksi tersebut :',
        'list'=>[
            ['title'=>'Indonesia tidak dapat mengikuti turnamen internasional baik timnas maupun klub, bisa sepanjang satu tahun atau dua tahun, tergantung keputusan Exco FIFA'],
            ['title'=>'Tidak ada kompetisi lokal yang diakui oleh FIFA.'],
            ['title'=>'Sanksi FIFA tidak cuma berimbas di level klub/timnas, tapi juga kepelatihan, dan perwasitan.'],
            ['title'=>'Kerugian bagi industri media, tidak bisa menyiarkan, mengabarkan, atau memberitakan pertandingan klub maupun Timnas Indonesia, karena sanksi larangan bermain di turnamen internasional pada level usia berapapun. Situasi ini berimbas pada minimnya sponsor.'],
            ['title'=>'Suporter tak akan lagi bisa mendukung Timnas Indonesia di ajang seperti, Asian Games, Pra Olimpiade, Kualifikasi Piala Asia, Kualifikasi Piala Dunia, Piala AFF, dan lain-lain selama sanksi FIFA masih berlaku.']
        ]
    ],
    'SURAT'=>[
        'narasi'=>'<img src="http://cdn1-a.production.liputan6.static6.com/medias/889271/original/006852900_1432986898-IMG-20150530-WA0001_1_.jpg" class="pic2">Federasi Sepak bola tertinggi dunia menjatuhkan sanksi untuk PSSI. FIFA mencabut keanggotaan Indonesia dan melarang Indonesia terlibat dalam seluruh kompetisi yang digelar FIFA.
                </p><p class="font_kecil">Keputusan dikeluarkan FIFA dalam rapat Komite Eksekutif (Exco) yang digelar di Zurich, Swiss, Sabtu (30/5/2015). Dasar hukum sanksi yakni intervensi Pemerintah yang membekukan PSSI.
                </p><p class="font_kecil">Berikut isi Surat Sanksi FIFA :
                </p><p class="font_kecil">22 Mei 2015, FIFA mengingatkan Kementerian terkait surat mereka tertanggal 18 Februari 2015 dan 4 Mei 2015 dan tenggat yang ditetapkan, dan menginformasikan tidak bisa bertemu selama berlangsungnya Kongres FIFA. PSSI memberikan FIFA laporan terakhir tertanggal 29 Mei yang mengonfirmasi Kementerian tidak mencabut tindakan di atas.
                </p><p class="font_kecil">Berdasarkan korespondensi dengan FIFA tanggal 4 Mei, persoalan ini dibawa ke Komite Eksekutif FIFA dan dirundingkan dalam rapat pada 30 Mei. Exco FIFA menyimpulkan pihak kementerian (atau badan lainnya) telah ikut menganggu aktivitas PSSI yakni dalam kategori pelanggaran serius sesuai Pasal 13 dan 17 Statuta FIFA. Dengan begitu, kami memberitahukan Anda bahwa Exco FIFA memutuskan, sesuai dengan Pasal 14 ayat 1 dari Statuta FIFA, bahwa :
                </p><p class="font_kecil">Sanksi bagi PSSI langsung berlaku dan untuk waktu yang tidak ditentukan sampai PSSI bisa mematuhi peraturan Pasal 13 dan 17 Statuta FIFA. Hukuman untuk PSSI baru bisa dicabut bila :',
        'list'=>[
            ['title'=>'Exco PSSI terpilih bisa menyelesaikan permasalahan mereka secara independen tanpa pengaruh dari pihak ketiga, termasuk Kementerian (atau badan lainnya).'],
            ['title'=>'Tanggung jawab untuk Tim Nasional indonesia dikembalikan kepada PSSI'],
            ['title'=>'Tanggung jawab seluruh kompetisi dikembalikan kepada PSSI atau klub yang berada di bawahnya'],
            ['title'=>'Semua klub yang memiliki lisensi sesuai regulasi lisensi klub PSSI bisa bermain di seluruh kompetisi di bawah PSSI'],
            ['title'=>'Suporter tak akan lagi bisa mendukung Timnas Indonesia di ajang seperti, Asian Games, Pra Olimpiade, Kualifikasi Piala Asia, Kualifikasi Piala Dunia, Piala AFF, dan lain-lain selama sanksi FIFA masih berlaku.']
        ],
        'narasi2'=>'<img src="http://obsessionnews.com/wp-content/uploads/2015/05/surat-FIFA21.jpg" class="pic">Selama disanksi, PSSI kehilangan hak keanggotaan (c.f Pasal 12 ayat 1 Statuta FIFA) dan seluruh wakil asal Indonesia (timnas maupun klub) dilarang melakukan hubungan internasional, termasuk terlibat di kompetisi FIFA dan AFC (c.f khususnya Pasal 14 ayat 3 dari Statuta FIFA).
                </p><p class="font_kecil">Sanksi kepada PSSI juga membuat setiap anggota mereka dan ofisial tidak bisa mendapatkan keuntungan apapun dari program pengembangan FIFA dan AFC, kursus, atau latihan selama dijatuhi sanksi.
                </p><p class="font_kecil">Terakhir, Exco FIFA mengetahui timnas Indonesia sedang ambil bagian di SEA Games 2015 di Singapura. Dengan sebuah pengecualian dan tidak sama dengan sanksi, Exco FIFA memutuskan timnas Indonesia bisa berpartisipasi di SEA Games hingga partisipasi mereka selesai.'
    ],
    'DAFTAR'=>[
        'narasi'=>'FIFA sebagai otoritas tertinggi sepakbola dunia cukup tegas dalam memberikan sanksi terhadap anggota-anggotanya yang dianggap tidak becus dalam mengelola sepakbola. Persolan yang kerap menimpa organisasi induk sepakbola ditiap negara-negara anggotanya kebanyakan karena adanya intervensi dari Pemerintah. Dan saat ini, PSSI juga dalam ancaman sanksi karena persoalan serupa.<br>Berikut Negara yang pernah mendapat sanksi dari FIFA :',
        'list'=>[
            ['title'=>'Yunani','link'=>'http://www.bijaks.net/aktor/profile/yunani553f01508d3c0','img'=>'http://www.bijaks.net/public/upload/image/politisi/yunani553f01508d3c0/badge/abbec18b160e0b54db93586c07c87f9b8442bd1f.png','no'=>'Federasi Sepakbola Yunani, HFF mendapat sanksi dari FIFA pada 3 Juli 2006, karena adanya campur tangan pemerintah dan politisasi dalam sepakbola. Hal itu dianggap melanggar statuta FIFA. Tapi HFF lalu meresponsnya dengan mengajukan perubahan. Alhasil, FIFA pun memberi tenggat waktu hingga 15 Juli 2006, sebelum Yunani benar-benar dinonaktifkan dari persepakbolaan internasional. Dan, pada 7 Juli 2006, HFF memiliki undang-undang olahraga baru, yang sejalan dengan FIFA. Sanksi pun dicabut.'],
            ['title'=>'Iran','link'=>'http://www.bijaks.net/aktor/profile/iran537ad62310221','img'=>'http://www.bijaks.net/public/upload/image/politisi/iran537ad62310221/badge/129b0fb2cbcaf494bddbb63d133a42541ee0a22a.png','no'=>'November 2006, atau lima bulan setelah Iran tampil di Piala Dunia 2006, FIFA memberi sanksi kepada Federasi Sepakbola Iran, IRIFF. Hal tersebut tak terlepas dari campur tangan pemerintah perihal terpilihnya kembali Mohammad Dadkan sebagai Presiden IRIFF. FIFA menilai tak ada independensi terkait pemilihan presiden baru IRIFF, sehingga dianggap telah melanggar pasal 17 Statuta FIFA. Namun, setelah dilakukan pemilihan ulang Presiden IRIFF, tepatnya Desember 2006, sanksi tersebut dicabut.'],
            ['title'=>'Kenya','link'=>'http://www.bijaks.net/aktor/profile/kenya5371bddb5d31f','img'=>'http://www.bijaks.net/public/upload/image/politisi/kenya5371bddb5d31f/badge/63723683ae33bbb122ba69b4c114bba05e54f9bd.png','no'=>'Pada November 2006, Federasi Sepakbola Kenya, KFF mendapat sanksi dari FIFA, setelah adanya campur tangan pemerintah dalam hal sepakbola. Namun pada 2007, FIFA akhirnya mencabut sanksi, setelah adanya perbaikan dalam tata kelola federasi. Pada 2008, pemegang otoritas tertinggi sepakbola Kenya sendiri berpindah ke FKL. Sebelum akhirnya pada 2011, hanya ada satu badan yang membawahi sepakbola, yaitu FKF.'],
            ['title'=>'Kuwait','link'=>'http://www.bijaks.net/aktor/profile/kuwait538170b2aecee','img'=>'http://www.bijaks.net/public/upload/image/politisi/kuwait538170b2aecee/badge/14e7226e1d4bcd94201fed5113e4782ca1512775.png','no'=>'Pada Oktober 2007, Federasi Sepakbola Kuwait (KFA) mendapat sanksi oleh FIFA, berupa larangan mengikuti segala kegiatan sepakbola di level internasional. Itu merupakan buntut dari adanya intervensi pemerintah dalam pemilihan ketua umum dan anggota.
                    <br>Sebulan kemudian, sanksi tersebut dicabut sementara oleh FIFA, setelah KFA menyetujui adanya beberapa perubahan. Tapi, lalu sanksi diberikan lagi dan baru dilepas secara kondisional pada kongres FIFA pada 2009. Situasi mereka pun terus dimonitor oleh FIFA.'],
            ['title'=>'Ethiopia','link'=>'http://www.bijaks.net/aktor/profile/ethiopia537d83176e078','img'=>'http://www.bijaks.net/public/upload/image/politisi/ethiopia537d83176e078/badge/716b2d61ee859a1602624d75f517181934f02102.png','no'=>'Majelis Umum Ethiopia memecat Presiden Federasi Sepakbola Ethiopia, EFF, Ashebir Woldegiorgis pada Januari 2008. Tapi, Ashebir menolaknya, sehingga terjadi kekisruhan. Alhasil, FIFA memberi sanksi, karena pemerintah dianggap telah mengintervensi.
                    <br>Ethiopia pun mesti absen dari laga-laga internasional, termasuk kualifikasi Piala Dunia 2010. Namun, setelah terpilih presiden EFF yang baru, Sahlu Gebrewold Gebremariam, FIFA kemudian mencabut sanksi tersebut.'],
            ['title'=>'Madagaskar','link'=>'http://www.bijaks.net/aktor/profile/madagaskar53797834186f1','img'=>'http://www.bijaks.net/public/upload/image/politisi/madagaskar53797834186f1/badge/7a6584e36adfb151ecc37b1549f7f6f88395a939.png','no'=>'Pada 19 Maret 2008 FIFA memberi sanksi kepada Federasi Sepakbola Madagaskar, FMF. Itu dilakukan setelah pemerintah membubarkan kepengurusan FMF dan meminta dilakukannya pembentukan ulang. FIFA menganggap ada intervensi dari pemerintah.
                    <br>Alhasil, Timnas Madagaskar pun dilarang berpartisipasi dalam segala kompetisi di level internasional. Namun, Mahkamah Agung setempat kemudian tak memuluskan upaya pemerintah untuk membubarkan FMF. Sanksi pun dicabut pada 19 Mei 2008.'],
            ['title'=>'Peru','link'=>'http://www.bijaks.net/aktor/profile/peru553efe775c056','img'=>'http://www.bijaks.net/public/upload/image/politisi/peru553efe775c056/badge/64ce7d33d0f85f56f32c6daba98b27e7995aadd4.png','no'=>'Federasi Sepakbola Peru, FPF mendapat sanksi dari FIFA pada November 2008. Hal tersebut dikarenakan adanya percekcokan antara federasi dan pemerintah. FPF pun kemudian diberi waktu sebulan untuk mengendalikan situasi.
                    <br>Setelah dilarang tampil di semua pertandingan level Internasional, FPF pun akhirnya mampu memenuhi apa yang disyaratkan FIFA. Pada Desember 2008, atau sebulan sejak sanksi diberikan, FIFA akhirnya mencabut hukuman tesebut.'],
            ['title'=>'Brunei Darussalam','link'=>'http://www.bijaks.net/aktor/profile/bruneidarussalam537311be844bf','img'=>'http://www.bijaks.net/public/upload/image/politisi/bruneidarussalam537311be844bf/badge/8c321b0581c42089e49ebd9edbf16497cc4663d7.jpg','no'=>'Brunei dinonaktifkan dari segala urusan sepakbola internasional oleh FIFA pada September 2009. Hal tersebut merupakan imbas dari tindakan pemerintah yang melenyapkan Federasi Sepakbola Brunei, BAFA, dan membentuk kepengurusan anyar, FABD pada 2008.
                    <br>Setelah dibentuk komite normalisasi, akhirnya terbentuk federasi baru dengan nama NFABD. FIFA pun kemudian menyetujui pembentukan federasi tersebut dan resmi mencabut sanksi untuk Brunei pada Mei 2011.'],
            ['title'=>'Irak','link'=>'http://www.bijaks.net/aktor/profile/irak5371bcc81e12c','img'=>'http://www.bijaks.net/public/upload/image/politisi/irak5371bcc81e12c/badge/b6ff0864f180b7b932af06838d7c4735094e86df.png','no'=>'Pada November 2009, Komite Olimpiade Irak membubarkan Federasi Sepakbola Irak, IFA. Pihak keamanan pun kemudian menguasai kantor federasi. Dan, FIFA lalu memberi tenggat waktu 72 jam untuk memulihkan situasi, namun tetap tak ada perubahan.
                    <br>Sanksi diberikan, sehingga Irak nonaktif dari segala aktivitas sepakbola internasional. Baru setelah empat bulan, atau tepatnya pada Maret 2010, segala permasalahan tersebut bisa diatasi dan FIFA pun mencabut sanksi untuk Irak.'],
            ['title'=>'Nigeria','link'=>'http://www.bijaks.net/aktor/profile/nigeria5371bf6fb065b','img'=>'http://www.bijaks.net/public/upload/image/politisi/nigeria5371bf6fb065b/badge/babf934bc6a57f6c15d4b4fab8632d31e6c65b22.png','no'=>'Pada 4 Oktober 2010, Federasi Sepakbola Nigeria, NFF mendapat sanksi dari FIFA. Hal itu dilakukan setelah adanya tindakan sepihak dari pemerintah yang mencampuri urusan sepakbola nasional. Namun, empat hari berselang, hukuman tersebut dicabut.
                    <br>Hal serupa kembali terulang pada 9 Juli 2014. Lagi-lagi, pemerintah melakukan intervensi dan kali ini perihal pengangkatan sepihak Lawrence Katiken sebagai pemimpin federasi. Namun, 10 hari kemudian, situasi kembali normal dan FIFA mencabut sanksinya.
                    <br>(Dari berbagi sumber)']
        ]
    ],
    'KETUA'=>[
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/8/8c/Soeratin.jpg/150px-Soeratin.jpg','url'=>'http://www.bijaks.net/aktor/profile/soeratinsosrosoegondo553f193ec3b53','thn'=>'(1930-1940)','name'=>'Soeratin Sosrosoegondo'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/Artono','thn'=>'(1941-1949)','name'=>'Artono Martosoewignyo'],
        ['img'=>'http://www.tokohindonesia.com/index2.php?option=com_resource&task=show_file&id=47638&type=thumbnail_article','url'=>'http://www.bijaks.net/aktor/profile/maladi517cb841aa32f','thn'=>'(1950-1959)','name'=>'Maladi'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/Abdul','thn'=>'(1960-1964)','name'=>'Abdul Wahab Djojohadi koesoemo'],
        ['img'=>'http://4.bp.blogspot.com/-ixwbJ-KBVtY/TfqMwRfEg8I/AAAAAAAAAM4/i4TKuWWgL_Y/s1600/maulwi.jpg','url'=>'http://www.bijaks.net/aktor/profile/maulwisaelan553f2bbd7b5c5','thn'=>'(1964-1967)','name'=>'Maulwi Saelan'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/b/b9/Kosasih_opurwanegara_ris.jpg','url'=>'http://www.bijaks.net/aktor/profile/kosasihpurwanegara553f1371b17e5','thn'=>'(1967-1974)','name'=>'Kosasih Poerwanegara'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/Bardosono','thn'=>'(1975-1977)','name'=>'Bardosono'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/Moehono','thn'=>'(1977-1977)','name'=>'Moehono'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/commons/f/fa/Ali_Sadikin_%281975%29.jpg','url'=>'http://www.bijaks.net/aktor/profile/letnanjenderalpurnkkoalhalisadikin518093c1a0915','thn'=>'(1977-1981)','name'=>'Ali Sadikin'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/sjarnoebisaid553f1f9fd78b5','thn'=>'(1982-1983)','name'=>'Sjarnoebi Said'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/7/74/Kardono-First-President-of-the-AFF.jpg/220px-Kardono-First-President-of-the-AFF.jpg','url'=>'http://www.bijaks.net/aktor/profile/kardono553f09a62683f','thn'=>'(1983-1991)','name'=>'Kardono'],
        ['img'=>'http://cdn-media.viva.id/thumbs2/2011/03/23/107496_azwar-anas_663_382.jpg','url'=>'http://www.bijaks.net/aktor/profile/irhazwaranas51872cb2788a2','thn'=>'(1991-1999)','name'=>'Azwar Anas'],
        ['img'=>'https://pacpdipciawiebang.files.wordpress.com/2008/02/agum.jpg','url'=>'http://www.bijaks.net/aktor/profile/agumgumelar518768633e53b','thn'=>'(1999-2003)','name'=>'Agum Gumelar'],
        ['img'=>'http://statik.tempo.co/data/2012/06/29/id_128356/128356_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/nurdinhalid530eb1689eb22','thn'=>'(2003-2011)','name'=>'Nurdin Halid'],
        ['img'=>'http://kanalsatu.com/images/20150119-141948_32.jpg','url'=>'http://www.bijaks.net/aktor/profile/djohararifinhusin552770aa7c1db','thn'=>'(2011-2015)','name'=>'Djohar Arifin Husin'],
        ['img'=>'http://www.bolaskor.com/wp-content/uploads/2014/01/La-Nyalla-Mattalitti.jpg','url'=>'http://www.bijaks.net/aktor/profile/lanyallamattalitti553482e51ddbe','thn'=>'(18 April 2015)','name'=>'La Nyalla Mattalitti']
    ],
    'KRONOLOGI'=>[
        ['date'=>'9 April 2015','content'=>'Komite Eksekutif (Exco) PSSI telah menyutujui usulan tentang penghentian sementara kompetisi QNB League akibat polemik dengan Badan Olahraga Profesional Indonesia (BOPI)/Kemenpora tentang keikutsertaan Persebaya Surabaya dan Arema Cronus yang tak lolos syarat verifikasi yaitu asas legalitas.<br><br>Perwakilan 18 klub telah disampaikan opsi-opsi yang akan diambil dalam rapat Exco ketika dipanggil untuk berkumpul bersama PSSI dan PT Liga Indonesia di Jakarta pada sehari sebelumnya.'],
        ['date'=>'11 April 2015','content'=>'Beredar surat ancaman sanksi dari FIFA yang ditandatangani Sekretaris Jenderal Jerome Valcke.<br><br>Sehari sebelumnya, 10 April 2015. Dalam surat itu FIFA menilai BOPI telah menambah kriteria tentang keikutsertaan klub. FIFA meminta pemerintah membatasi urusan rumah tangga PSSI.'],
        ['date'=>'12 April 2015','content'=>'Seluruh pertandingan QNB League resmi dihentikan sementara oleh PT Liga Indonesia berdasarkan keputusan Exco.<br><br>Pada saat yang sama Kemenpora merespon surat FIFA dan menyatakan tak ada penambahan kriteria yang dilakukan BOPI. Semua kriteria itu tertera dalam PSSI Club Licensing Regulation yang disetujui Exco PSSI pada rapat 28 September 2013.'],
        ['date'=>'13 April 2015','content'=>'Penghentian liga mulai mendapatkan korban. Persija Jakarta menunggak gaji pemain selama tiga bulan terakhir. Alasan yang dilontarkan manajemen Persija adalah dana sponsor yang tak turun akibat kompetisi berhenti.'],
        ['date'=>'16 April 2015','content'=>'Kemenpora mengirimkan surat peringatan ketiga (SP3) terhadap PSSI tentang pengabaian teguran dalam dua surat peringatan sebelumnya. Dalam surat peringatan ketiga tersebut Kemenpora memberi tenggat waktu 1 X 24 Jam terhadap PSSI sejak surat itu diterima pada pukul 18.40 WIB.<br><br>Pada malam harinya terjadi penyerbuan dengan kekerasan oleh sekelompok orang ke diskusi tentang Persebaya Surabaya yang disiarkan langsung pada malam hari di stasiun televisi lokal, Gedung Graha Pena, Surabaya.'],
        ['date'=>'17 April 2015','content'=>'Bonek alias suporter sepak bola Persebaya 1927 menolak rencana KLB PSSI di kota tersebut. Mereka menuntut Persebaya dikembalikan seperti semula. Ribuan bonek pun menyatakan akan melakuakn long march untuk menuntut pendapat mereka.<br><br>Bonek memasuki kawasan Jalan Embong Malang untuk melakukan aksi proters kepada PSSI. Sabtu (18/4). (CNN Indonesia/M. Arby Putratama)'],
        ['date'=>'18 April 2015','content'=>'Sebanyak 1450 personel aparat keamanan gabungan mengamankan KLB PSSI yang berlangsung di Hotel JW Marriott, Surabaya. Sekitar 300 meter jalan Embong dari lokasi JW Marriott disterilkan polisi dengan barikade empat lapis hingga lokasi hotel.<br><br>Ribuan bonek telah memadati area sekitar jalan Embong sejak pagi hari.<br><br>Tengah hari, Kemenpora memublikasikan surat pembekuan PSSI yang ditandatangani Menpora Imam Nahrawi. Dalam surat tersebut dinyatakan persiapan timnas untuk ikut Sea Games 2015 akan diambil alih KONI dan KOI.<br><br>Beberapa saat setelahnya KLB PSSI menghasilkan La Nyalla Mahmud Mattalitti akan menjad ketua umum organisasi tersebut menggantikan Djohar Arifin.<br><br>Djohar dan Sekretaris Jenderal PSSI Joko Driyono mundur dari pencalonan mereka menjadi Ketua Umum organisasi itu.<br><br>La Nyalla terpilih dengan perolehan 92 suara dari total 106 pemilik hak suara.<br><br>FIFA menjatuhkan Sanksi terhadap PSSI.'],
        ['date'=>'22 Mei 2015','content'=>'FIFA mengingatkan Kementerian terkait surat mereka tertanggal 18 Februari 2015 dan 4 Mei 2015 dan tenggat yang ditetapkan, dan menginformasikan tidak bisa bertemu selama berlangsungnya Kongres FIFA. PSSI memberikan FIFA laporan terakhir tertanggal 29 Mei yang mengonfirmasi Kementerian tidak mencabut tindakan di atas.'],
        ['date'=>'29 Mei 2015','content'=>'Indonesia akhirnya tetap mengikuti SeaGames ke 28 di Singapura. Dalam ajang pertandingan di olahraga Indonesia yang di wakili oleh Timnas U-23, tidak meraih apa-apa. Pada pertandingan semifinal SEA Games 2015 yang digelar di Stadion Nasional, Jalan Kallang, Singapura, Sabtu, 13 Juni 2015, timnas U 23 kalah 0-5 dari Thailand. Timnas U-23 juga gagal meraih medali perunggu setelah kembali takluk dari Vietnam dengan skor serupa, 0-5, Senin, 15 Juni 2015.'],
        ['date'=>'16 Juni 2015','content'=>'Diduga kuat kekalahan timnas U-23 di ajang pertandingan seagames di Singapura penuh dengan rekayasa. Di depan Sekretaris Kementerian Pemuda dan Olahraga, dan beberapa anggota tim litigasi pegungkap kasus mafia bola, tentang pengaturan skor sepakbola antara Indonesia U23 dengan Vietnam U23.'],
        ['date'=>'17 Juni 2015','content'=>'Ketua PSSI La Nyalla Akan Menempuh Jalur Hukum. Ia mengatakan akan menempuh jalur hukum ihwal tuduhan pengaturan skor tim nasional Indonesia usia di bawah 23 tahun kala melawan Thailand dan Vietnam dalam SEA Games 2015.']
    ],
    'PENYOKONG'=>[
        ['name'=>'BNPT','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'http://2.bp.blogspot.com/-k5jeVQ1svsA/U_grEHrkP-I/AAAAAAAAAT4/8K2k_WM_500/s1600/LOGO-BNPT.png' ],
        ['name'=>'POLRI','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'http://2.bp.blogspot.com/-GBErBcGBQlo/VIzWFgVVidI/AAAAAAAAAgw/VGDR74ZsPZ0/s1600/polisi-polri.png'],
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef']
        ],
        'institusi'=>[
            ['name'=>'Bonek','img'=>'http://2.bp.blogspot.com/__ed956rZIkE/TPJyiz2qZzI/AAAAAAAAADY/H4aC62ss4hQ/s1600/27180_105731152786509_100000488752405_153904_4840158_n.jpg','url'=>''],
            ['name'=>'Viking','img'=>'http://2.bp.blogspot.com/-z4LhYlN_T34/TtI1uDcdJQI/AAAAAAAAACk/QXqDhsxYSeI/s150/VIKing.jpg','url'=>''],
            ['name'=>'Kemenpora','img'=>'http://www.fdsinews.com/wp-content/uploads/wpid-wp-1425427806616.jpeg','url'=>''],
            ['name'=>'BOPI','img'=>'https://chairilzm.files.wordpress.com/2013/05/logo_kemenpora.jpg','url'=>''],
            ['name'=>'PT. Liga Indonesia','img'=>'http://www.hariandepok.com/wp-content/uploads/2014/09/ISL.jpg','url'=>'']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227']
        ],
        'institusi'=>[
            ['name'=>'KONI','img'=>'http://2.bp.blogspot.com/-xC_gsQA7uN4/UTtgudTALxI/AAAAAAAAAIc/adugfUIV5hY/s320/LOGO+KONI+NARU+CR.png','url'=>''],
            ['name'=>'PSSI','img'=>'http://images.solopos.com/2013/03/logo-pssi3.jpg','url'=>''],
            ['name'=>'Jak Mania','img'=>'https://lh5.googleusercontent.com/-yWWR3Cu3FFw/TXTYqDbkPsI/AAAAAAAAABo/oHc8yVnnt0U/s320/1_105840306l.jpg','url'=>''],
            ['name'=>'FIFA','img'=>'http://img4.wikia.nocookie.net/__cb20150211093302/logopedia/images/5/5d/FIFA_Logo.svg','url'=>''],
            ['name'=>'SSB Telaga Gorontalo','img'=>'http://1.bp.blogspot.com/-N2GQ-rnmKcU/T3fmkTg1mHI/AAAAAAAAAqU/VpVhs40cI80/s400/1.jpg','url'=>''],
            ['name'=>'Sekretaris Asosiasi Provinsi Aceh','img'=>'','url'=>''],
            ['name'=>'PSM Makassar','img'=>'http://spartacks.org/wp-content/uploads/2011/11/PSM-Makassar.png','url'=>''],
            ['name'=>'Komite Exco PSSI','img'=>'','url'=>'']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'HM Ridwan Hisjam','jabatan'=>'Wakil Ketua Komisi X DPR RI','img'=>'http://assets.kompas.com/data/photo/2015/04/19/1753594RidwanHisjam780x390.jpg','url'=>'','content'=>'Tidak ada, karena saya tahu betul siapa Imam Nahrawi. Kita sepakat, apa yang menjadi kebijakan pemerintahan harus didukung biar berjalan dengan baik. Mari kita selesaikan kisruh PSSI ini dengan baik'],
        ['from'=>'Herru Joko','jabatan'=>'Viking Persib Club (VPC)','img'=>'http://static.inilah.com/data/berita/foto/2125810.jpg','url'=>'','content'=>'Saya kira sikap tegas Menpora dalam menyelesaikan kusutnya sepakbola di dalam negeri sudah tepat, dan layak mendapat apresiasi'],
        ['from'=>'Eko','jabatan'=>'Pengamat Bola','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Di mata hukum nasional, yang dilakukan Menpora memang sudah tepat dan sesuai aturan. Namun saya menyayangkan keputusan ini sedikit keluar dari jalur yang sebelumnya dibangun'],
        ['from'=>'Roy Suryo','jabatan'=>'Mantan Kemenpora','img'=>'http://www.artefak.org/wp-content/uploads/2015/01/Roy-Suryo.jpg','url'=>'','content'=>'Saya percaya Menteri Pemuda dan Olahraga saat ini Imam Narhrawi punya pikiran sekelas menteri yang baik dan bisa menyelesaikan'],
        ['from'=>'Andie Pecie','jabatan'=>'Ketua Presidium Bonek 1927','img'=>'http://msports.net/id-content/id-upload/150416andiepeci-792813-idcms.jpg','url'=>'','content'=>'Jadi ini momentum penting. Dalam usia yang ke-85 tahun, PSSI memang harus dibenahi untuk perkembangan sepak bola Indonesia ke depan. Jadi kami dari bonek mengapresias langkah Kemenpora'],
        ['from'=>'Ari Junaedi','jabatan'=>'Pengamat sepakbola','img'=>'http://rmolsumsel.com/images/berita/normal/333759_08121921042014_arijunaedi.jpg','url'=>'','content'=>'Menurut saya pembekuan dilakukan tidak gegabah, kita bisa melihat permasalahan konflik, kepentingan, dan politik, pun bisnis yang akhirnya tidak sehat'],
        ['from'=>'Gatot S Dewa Broto','jabatan'=>'Deputi V Kemenpora','img'=>'http://us.images.detik.com/content/2013/07/01/328/gatotkominfo460.jpg','url'=>'','content'=>'Jadi, saat ini fokus kami untuk membentuk tim transisi, kalau mereka (PSSI) ajukan gugatan, silahkan saja kami terbuka untuk kemajuan sepakbola Indonesia'],
        ['from'=>'Herru Joko','jabatan'=>'Viking Persib Club (VPC)','img'=>'http://static.inilah.com/data/berita/foto/2125810.jpg','url'=>'','content'=>'Persib tidak akan hilang dari hati. Santai saja. Pemerintah sudah berusaha yang terbaik untuk mengurusi sepak bola Indonesia.']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Tono','jabatan'=>'Ketua Umum KONI','img'=>'http://www.sportanews.com/wp-content/uploads/a-ketua-koni-tono-suratman.jpg','url'=>'','content'=>'Yang jelas saya yang buka kongres, tentu saya mengakui. Kecuali saat membuka saya bilang ada masalah. Kan ada perwakilan dari AFC dan FIFA di sana'],
        ['from'=>'La Nyalla Mattalitti','jabatan'=>'Ketua Umum Terpilih PSSI 2015','img'=>'http://www.bolaskor.com/wp-content/uploads/2014/01/La-Nyalla-Mattalitti.jpg','url'=>'','content'=>'Yang jelas pengurus PSSI sudah dinyatakan sah oleh FIFA. Semua berlangsung demokratis dan transparan. Saya mendapat ucapan selamat dari FIFA karena FIFA tidak menganggap adanya pembekuan'],
        ['from'=>'Richard Ahmad','jabatan'=>'Ketua Jak Mania','img'=>'http://www.indopos.co.id/wp-content/uploads/2015/02/2128.jpg','url'=>'','content'=>'Penyelesaian masalah ini bisa dibicarakan atau dikompromikan. Saya dengar PSSI akan menghadap Wakil Presiden lagi untuk menyelesaikan masalah ini. Dengan DPR juga. Tapi jika masih belum, maka presiden harus turun tangan'],
        ['from'=>'David Noemi','jabatan'=>'FIFA','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Kegagalan (pemerintah Indonesia) melakukan hal itu (tidak mencampuri urusan PSSI) akan membuat FIFA tidak punya pilihan selain menjatuhkan sanksi kepada PSSI'],
        ['from'=>'Reni Marlinawati','jabatan'=>'Anggota Komisi X DPR RI','img'=>'http://www.suara-islam.com/images/berita/reni_marlinawati-ppp_20130917_174620.jpg','url'=>'','content'=>'Langkah Menpora dengan menerbitkan keputusan sanksi administrasi terhadap PSSI merupakan langkah ekstrem'],
        ['from'=>'Rudi Moonti','jabatan'=>'Kepala Sekolah Sepak Bola (SSB) Gelora Telaga Gorontalo','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Persoalan dua institusi itu bisa berdampak pada pembinaan olah raga hingga ke daerah-daerah'],
        ['from'=>'Teguh Juwarno','jabatan'=>'anggota komisi X DPR RI','img'=>'http://static.republika.co.id/uploads/images/kanal_sub/anggota-komisi-iii-dpr-ri-dari-fraksi-partai-amanat-_111128172620-118.jpg','url'=>'','content'=>'Kemenpora yang membekukan PSSI padahal persoalannya hanya verifikasi klub-klub yang berlaga di ISL, itu sama saja menangkap tikus dengan membakar lubung padi'],
        ['from'=>'Hinca Panjaitan','jabatan'=>'kata Wakil Ketua Umum PSSI','img'=>'http://rmol.co/images/berita/normal/44796_01060712112014_hinca_panjaitan1.jpg','url'=>'','content'=>'Ibu menteri mempelajarai data tentang hasil kongres dan permasalahan SK (surat keputusan) Menpora, dan berjanji akan memediasi PSSI dengan Menpora secepatnya'],
        ['from'=>'Nurdin Halid','jabatan'=>'mantan Ketua Umum PSSI','img'=>'http://statik.tempo.co/?id=29567&width=475','url'=>'','content'=>'Tindakan Menpora dalam rangka membenahi profesionalisme PSSI itu benar, tapi tindakan membekukan itu keliru besar karena berimplikasi pada FIFA menghukum PSSI'],
        ['from'=>'Sutan Adil Hendra','jabatan'=>'anggota Komisi X DPR dari Fraksi Partai Gerindra','img'=>'http://infojambi.com/images/tokoh_politik/suta-aidil-dpr.jpg','url'=>'','content'=>'PSSI sudah menjadi kebanggan nasional. “Siapapun yang suka sepak bola, termasuk saya tentu kecewa dengan kebijakan yang baru saja dibuat  Menpora'],
        ['from'=>'Djohar Arifin','jabatan'=>'mantan Ketua umum PSSI, dan Dewan Kehormatan PSSI','img'=>'http://img.lensaindonesia.com/uploads/1/2012/04/Rekonsiliasi-johar.jpg','url'=>'','content'=>'Ada miskomunikasi antara PSSI dan Menpora. Oleh karena itu, harus ada dialog untuk memecahkan masalah. Saya akan dorong pengurus baru'],
        ['from'=>'Tengku Mahmud Khaidir','jabatan'=>'Sekretaris Asosiasi Provinsi Aceh','img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/04/18/528868/540x270/pssi-dibekukan-persebaya-menolak-tunduk-ke-menpora-20150418161742.jpg','url'=>'','content'=>'Jadi kami harap, Menpora (Imam Nahrawi) segera mencabut keputusan pembekuan. Dahulukan kepentingan bangsa dan negara, jangan sepak bola malah menjadi korban kebuasan kekuasaan ini'],
        ['from'=>'Erwin Aksa','jabatan'=>'CEO Bosowa Corporation dan pemilik PSM Makassar','img'=>'http://www.bosowa.co.id/asset/image/_47K68251%281%291.jpg','url'=>'','content'=>'Saya pikir pemerintah dalam hal ini Menpora tidak berhak melakukan hal itu karena PSSI bukan organisasi terlarang'],
    ],
    'VIDEO'=>[
        ['id'=>'PiwNSxpeSNM'],
        ['id'=>'IB1J3kk_xTI'],
        ['id'=>'_qpHo6lhpCM'],
        ['id'=>'O77LzQ7pk5s'],
        ['id'=>'B270beaE9HM'],
        ['id'=>'FUyjpU-HWfc'],
        ['id'=>'oq-PvvOseF0'],
        ['id'=>'jwSWD9Xw02I'],
        ['id'=>'P5ZQLG94iA0']
    ],
    'FOTO'=>[
        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/855323/big/098564400_1429343948-pssi-dibekukan-1.jpg'],
        ['img'=>'http://www.cikalnews.com/static/data/berita/foto/besar/97634839685kongres-pssi-bandung.jpg'],
        ['img'=>'http://www.acehfootball.com/wp-content/uploads/2013/10/Timnas-u_19.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/05/02/393292/HyUf9SA6bf.jpg?w=485'],
        ['img'=>'http://cdn.klimg.com/bola.net/library/p/headline/0000112802.jpg'],
        ['img'=>'http://static.inilah.com/data/berita/foto/1306032.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2013/03/17/1930194-klb-pssi-780x390.jpg'],
        ['img'=>'http://statik.tempo.co/?id=390848&width=620'],
        ['img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2015--04--11538-bonek-pssi-ribuan-1927-mulai-kurung-lokasi-kongres.jpg'],
        ['img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150417_095502_harianterbit_Kongres_PSSI_Surabaya_2015_3.JPG'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/20140126_193117_bonek-kepung-kongres-pssi-2014-di-surabaya.jpg'],
        ['img'=>'http://merahputih.com/news/_timthumb-project-code.php?src=http://server5.merahpoetih.com/gallery/public/2015/04/18/4pU6fjxYum1429337532.jpg&w=462&h=306&a=c,t'],
        ['img'=>'http://www.covesia.com/photos/berita/210415065940_pssi-gugat-kemenpora-ke-ptun.jpeg'],
        ['img'=>'http://borneonews.co.id/images/upload/1429437701-foto_istimewa_1__3_.jpg'],
        ['img'=>'http://www.klikpositif.com/media/images/news/bekukan-pssi-kemenpora-fokus-bentuk-tim-transisi_20150420172754.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/04/20/388595/JbTkl87EVn.jpg?w=668'],
        ['img'=>'http://prediksibolaindonesia.com/wp-content/uploads/2015/04/47.-Kisruh-PSSI-vs-Menpora.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/855509/big/057518200_1429363099-La_Nyalla_Mattalitti_Resmi_Pimpin_PSSI__foto_5_.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/04/28/120202/4Mye8qyIwf.jpg?w=668'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/04/23/0112249promo-267-2780x390.JPG'],
        ['img'=>'https://i.ytimg.com/vi/-SmZGZ8G-Tk/hqdefault.jpg'],
        ['img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150430_053849_harianterbit_Gatot_S_Dewabroto.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/jabar/foto/bank/images/persib-diarak-lima.jpg'],
        ['img'=>'http://cdn0-e.production.liputan6.static6.com/medias/798231/big/025436700_1421751813-raker2.jpg']
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/pssi/top.jpg")?>') no-repeat transparent;
        height: 1352px;
        margin-bottom: -600px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .boxprofile p, .boxprofile li, .font_kecil {
        font-size: 12px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
        background-color: #c1f2fb;
    }
    .block_red {
        background-color: #29166f;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 80%;
        height: auto;
        margin: 0 auto;
        display: inherit;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .garis {
        border-top: 1px dashed black;
    }
    .boxgray {
        width: 99%;
        border: 5px solid lightgray;
        box-shadow: -5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }
    .penyokong {
        width: 100%;
        height: auto;
        display: inline-block;
        margin-left: 20px;
    }
    .boxpenyokong {
        width: 126px;
        height: auto;
    }
    .foto {
        width: 100px;
        height: 100px;
        border: 3px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
        padding: 10px 10px;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: 130px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: 115px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi2 img {width: 98px; height: 90px;  padding: 0px !important;}
    li.organisasi2 p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 293px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
    li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.dukung {float: left; padding:3px;vertical-align: top;width: 45%;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/pssi/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }   
    .uprow {margin-left: 20px;margin-top:10px;border-color: #29166f transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {
        width: 260px;
        height: auto;
        margin-left: 20px;
        margin-top:0px;
        margin-bottom: 10px;
        background-color: #424040;
        background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px #29166f;
        border-right:solid 2px #29166f;
        border-bottom:solid 2px #29166f;
        border-radius: 0 0 5px 5px;
        color: #ffffff;
        z-index: 100;
    }
    .kronologi-title {font-size:14px;font-weight:bold;background-color: #29166f;color: white;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {line-height: 15px;font-size:12px;}

    .ketua {
        background-color: yellow;
        width: 30%;
        display: block;
        float: left;
        margin-right: 3%;
        box-shadow: -3px 3px 10px gray;
        border-radius: 8px;
        margin-bottom: 15px;
    }
    .ketua img {
        width: 100%;
        height: 200px;
    }
    .kritik {
        font-size: 18px;
        font-weight: bold;
        color: black;
    }
    .isi-col50 {
        float: left;
        margin: 10px 16px;
        width: 41%;
        height: auto;
        display: inline-block;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #29166f;
        width: 48%;
        height: 245px;
        float: left;
        margin-bottom: 10px;
        padding-top: 5px;
        margin-left: 10px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        float: left;
        border: 1px solid black;
    }
    #bulet {
        background-color: #ffff00; 
        text-align: center;
        width: 50px;
        height: 25px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: black;
        padding: 6px 10px;
        margin-left: -10px;
        margin-right: 10px;
    }

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <h3><a id="pssivskemenpora" style="color: #ffff00;text-shadow: 4px 4px 5px black;"><?php echo $data['NARASI']['title'];?></a></h3>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="clear"></div><br><div class="garis"></div>

                <h4 class="list"><a id="polemik" style="color: black;">POLEMIK</a></h4>
                <p class="font_kecil"><?php echo $data['POLEMIK']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="ultimatum" style="color: black;">ULTIMATUM FIFA</a></h4>
                <p><?php echo $data['ULTIMATUM']['narasi'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="kontroversi" style="color: black;">KONTROVERSI</a></h4>
                <ol style="list-style-type: square;margin-left: 0px !important;">
                    <?php
                    foreach ($data['KONTROVERSI']as $key => $val) {
                        echo "<li style='margin-left: 20px;font-weight: bold;'>".$val['title']."</li><p class='font_kecil'>".$val['no']."</p>";
                    }
                    ?>
                </ol>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="kontroversi" style="color: black;">DAMPAK SANKSI FIFA</a></h4>
                <p><?php echo $data['DAMPAK']['narasi'];?></p>
                <ol style="list-style-type: square;margin-left: 0px !important;">
                    <?php
                    foreach ($data['DAMPAK']['list'] as $key => $val) {
                        echo "<li style='margin-left: 30px;'>".$val['title']."</li>";
                    }
                    ?>
                </ol>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="polemik" style="color: black;">KONTROVERSI TIM TRANSISI</a></h4>
                <p class="font_kecil"><?php echo $data['KONTROVERSI2']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="polemik" style="color: black;">MAFIA BOLA</a></h4>
                <p class="font_kecil"><?php echo $data['MAFIA']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="kontroversi" style="color: black;">SURAT SANKSI FIFA</a></h4>
                <p><?php echo $data['SURAT']['narasi'];?></p>
                <ol style="list-style-type: square;margin-left: 0px !important;">
                    <?php
                    foreach ($data['SURAT']['list'] as $key => $val) {
                        echo "<li style='margin-left: 30px;'>".$val['title']."</li>";
                    }
                    ?>
                </ol>
                <p><?php echo $data['SURAT']['narasi2'];?></p>
                <div class="clear"></div>
            </div>

            <div class="col_kanan">
                <div class="boxprofile black">
                    <h4 class="block_red text-center"><a id="profil" style="color: #ffff00;">PROFIL</a></h4>
                    <p style="margin-left: 20px;margin-right: 20px;margin-top: 10px;">
                        <img src="http://bijaks.portal/assets/images/hotpages/pssi/logo.png" class="picprofil">
                    </p>
                    <p style="margin-left: 20px;margin-right: 20px;"><?php echo $data['PROFIL']['narasi']; ?></p>
                </div><br>

                <h4 class="list" style="margin-left: 20px;"><a id="daftarketua" style="color: black;">DAFTAR KETUA UMUM</a></h4>
                <div style="margin-left: 20px;">
                    <ul style="margin: 0px 0px 0px -2px;">
                        <?php
                        foreach($data['KETUA'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="dukung">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p>Tahun<br><?php echo $val['thn']; ?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
                <div class="clear"></div>

                <h4 class="list" style="margin-left: 20px;"><a id="kronologi" style="color: black;">KRONOLOGI</a></h4>
                <?php 
                foreach ($data['KRONOLOGI'] as $key => $value) {
                ?>
                <div class="uprow"></div>
                <div class="kronologi">
                    <div class="kronologi-title"><?php echo $value['date'];?></div>
                    <div class="kronologi-info">
                        <p><?php echo $value['content'];?></p>
                    </div>
                </div>
                <div class="clear"></div>
                <?php
                }
                ?>

                <h4 class="list" style="margin-left: 20px;margin-top: 25px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="newspssi_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newspssi_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>

            <div class="col_full">
                <div class="garis"></div>

                <h4 class="list"><a id="daftarnegara" style="color: black;">DAFTAR NEGARA YANG PERNAH DISANKSI FIFA</a></h4>
                <p class="font_kecil"><?php echo $data['DAFTAR']['narasi'];?></p>
                <?php
                $no=1;
                foreach($data['DAFTAR']['list'] as $key=>$val){
                    ?>
                    <div class="black boxdotted">
                        <?php echo "<span style='font-size: 16px;font-weight: bold;'><span id='bulet'>".$no."</span>".$val['title']."</span>";?>
                        <a href="<?php echo $val['link'];?>">
                            <p style="font-size: 12px;color: black;margin-left: 10px;margin-right: 10px;margin-top: 15px;">
                                <img class="bendera" src="<?php echo $val['img'];?>" alt="<?php echo $val['no'];?>"  data-toggle="tooltip" data-original-title="<?php echo $val['title'];?>"/>
                                <?php echo $val['no'];?>
                            </p>
                        </a>
                    </div>
                    <?php
                    $no++;
                }
                ?>
                <div class="clear"></div>
            </div>

            <div class="col_kiri50">
                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <div class="boxgray_green" style="height: 665px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 20px;" class="text-center">INSTITUSI PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi2">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col_kanan50">
                <h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <div class="boxgray_red" style="height: 665px;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>

                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 20px;" class="text-center">INSTITUSI PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="">
                                <li class="organisasi2">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
                <div class="clear"></div>
                <div class="boxgray" style="display: inline-block;height: auto;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <div>
                            <?php 
                            for($i=0;$i<=3;$i++){
                            ?>
                            <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>">
                                    <img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=4;$i<=7;$i++){
                            ?>
                            <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>">
                                    <img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                            </div>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            </div>

            <div class="clear"></div>
            <h4 class="list"><a id="quotepenentang" style="color: black;">QUOTE PENENTANG</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="display: inline-block;height: auto;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <div>
                            <?php 
                            for($i=0;$i<=3;$i++){
                            ?>
                                <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                        <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=4;$i<=7;$i++){
                            ?>
                                <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                        <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 55px; height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=8;$i<=11;$i++){
                            ?>
                                <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                        <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=12;$i<13;$i++){
                            ?>
                                <div style="float: left;margin: 10px 16px;width: 21%;height: auto;display: inline-block;border-bottom: 1px solid black;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                        <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                    </a>
                                    <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                    <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                    <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
            <div class="col_full" style="display: inline-block;">
                <ul style="margin-left: 0px;margin-top: 0px;">
                    <?php
                    $no=1;
                    foreach ($data['FOTO'] as $key => $val) { ?>
                        <li class="gallery">
                            <a href="#" data-toggle="modal" data-target="#img-<?php echo $no;?>">
                                <img src="<?php echo $val['img'];?>" />
                            </a>
                        </li>
                        <div class="modal hide fade" style="width: auto;" id="img-<?php echo $no;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Gallery Foto</h4>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?php echo $val['img'];?>" />
                                    </div>
                                    <!-- <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    <?php
                    $no++;
                    }
                    ?>
                </ul>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 10px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>
            <div class="col_full">
                <div class="boxgray" style="height: 710px;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <!-- jika ingin border atas bawah none gunakan mqdefault -->
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>