<?php

$kementrian = array(

    0 => ( array ( 'page_id' => 'departemenhukumdanham5313f91a94735', 'view_type' => 2, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => base_url().'assets/images/hotpages/kementrian/luhut_panjaitan.png',
            'seleksi' => array (0 => 'jendralpurnluhutbinsarpanjaitan5189af75dc288', 1 => 'jenderaltnimoeldoko5227e0280b4dc',
                2 => 'letjentnipurnsutiyoso511c2b6deeb46', 3 => 'letnanjenderaltnibudiman5227e9114ec01'))),

    1 => ( array ( 'page_id' => 'kementeriankoordbperekonomian531fdb30b8d14', 'view_type' => 2, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => base_url().'assets/images/hotpages/kementrian/Chairul_tanjung.jpg',
            'seleksi' => array (0 => 'chairultanjung51db5b0c9061a', 1 => 'dahlaniskan503d887d648d6',
                2 => 'gitawirjawan50f5159e39ce6', 3 => 'srimulyaniindrawatiphd5189d71174e32'))),

    2 => ( array ( 'page_id' => 'kemenkobidkesejahteraanrakyat5321386a5323c', 'view_type' => 2, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => base_url().'assets/images/hotpages/kementrian/muhaimin-iskandar.png',
            'seleksi' => array (0 => 'muhaiminiskandar50ef9d0b6f4d5', 1 => 'kuntoromangkusubroto50f50bb5aa850',
                2 => 'dralwiabdurrahmanshihab5170c3e79726b', 3 => 'drakhofifahindarparawansa5189c941e1c45'))),

    3 => ( array ( 'page_id' => 'kementriandalamnegeri531c1cbb6af24', 'view_type' => 2, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => base_url().'assets/images/hotpages/kementrian/abraham_samad.png',
            'seleksi' => array (0 => 'drabrahamsamadshmh5192f6e1b2ac4', 1 => 'agustinterasnarangsh50fdffc977dba',
                2 => 'pratikno540d1bf323949', 3 => 'basukitjahajapurnama50f600df48ac5'))),

    4 => ( array ( 'page_id' => 'kementerianluarnegeriindonesia531be6259f9cc', 'view_type' => 2, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => base_url().'assets/images/hotpages/kementrian/Dino_Patti_Djalal.png',
            'seleksi' => array (0 => 'dinopattidjalaldjalal521c3eb12ca04', 1 => 'makmurkeliat541017c8844d9',
                2 => 'martynatalegawa50ef6d0b48132', 3 => 'donkmarut541018e5091e5'))),

    5 => ( array ( 'page_id' => 'kementerianpertahanan5305bb682e70f', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'andiwidjajanto529828f684199', 1 => 'tbhasanuddinsemm50ee39facac7f',
                2 => 'jenderalryamizardryacudu52d35477b3460', 3 => 'prabowosubiyantodjojohadikusumo50c1598f86d91'))),

    6 => ( array ( 'page_id' => 'kementerianhukumdanhakasasimanusia540e76759d3bb', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'artidjoalkostar52c50a4e25b27', 1 => 'saldiisra540d1e8cb1477',
                2 => 'zainalarifinmochtar53155ff075894', 3 => 'drabrahamsamadshmh5192f6e1b2ac4'))),

    7 => ( array ( 'page_id' => 'kementeriankeuangan530eab5a7267e', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => base_url().'assets/images/hotpages/kementrian/Agus_Smaller.jpg',
            'seleksi' => array (0 => 'agusdwmartowardojo50ef76e597642', 1 => 'radenpardede52155b9191ba5',
                2 => 'profdrhendrawansupratikno5104f7c918e91', 3 => 'srimulyaniindrawatiphd5189d71174e32'))),

    8 => ( array ( 'page_id' => 'kementerianesdm530d82b2c0ac2', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'Arif', 1 => 'luluksumiarso514a86c1d0ea7',
                2 => 'irtumiran5410267edbd9e', 3 => 'hkurtubi5243b80d5cce5'))),

    9 => ( array ( 'page_id' => 'kementerianperindustriankemenperin5326593d4a5c9', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'antonjoenoessupit541027343805b', 1 => 'poempidahidayatulloh526a0a87b1aa5',
                2 => 'triyogiyuwono54103bfbad7ae', 3 => 'nusronwahid5109b917e9be0'))),

    10 => ( array ( 'page_id' => 'menteriperdaganganindonesia51d4bf0de10bf', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'drmarielkapangestu50ef8750d3096', 1 => 'soetrisnobachir53951be843c51',
                2 => 'sriadiningsih54102a1b1ed1b', 3 => 'gitawirjawan50f5159e39ce6'))),

    11 => ( array ( 'page_id' => 'kementrianpertanian531d2f81a4829', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'arifwibowo50f8c2f747be3', 1 => 'bustanilarifin5186083b8fe1e',
                2 => 'imansugema54102e88232e0', 3 => 'prabowosubiyantodjojohadikusumo50c1598f86d91'))),

    12 => ( array ( 'page_id' => 'kementeriankehutanan530d8273603f3', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'franswanggai541025b1ee4a4', 1 => 'drmuhammadprakosa50fbd5833af31',
                2 => 'imansugema54102e88232e0', 3 => 'zulkiflihasan50ef907d0661c'))),

    13 => ( array ( 'page_id' => 'kementerianperhubungan52fb1971da37b', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'marsekaltnipurnchappyhakim541021c7acb39', 1 => 'danangparikesit5409276a0c5d1',
                2 => 'ignasiusjonan51517941433fd', 3 => 'letjentnipurnsutiyoso511c2b6deeb46'))),

    14 => ( array ( 'page_id' => 'kementeriankelautandanperikanan5327b0eae5745', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'jamaluddinjompa54101cb58292d', 1 => 'drkadarusmanphd54101addaabb8',
                2 => 'profdrirrokhmindahurims51888b6c709d5', 3 => 'drfadelmuhammad51132604d3a90'))),

    15 => ( array ( 'page_id' => 'kemenakertrans51e226ab3bd2c', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'riekediahpitaloka51090e9289dc8', 1 => 'rizalsukma540922da87922',
                2 => 'wahyususilo540ffc5ada001', 3 => 'muhaiminiskandar50ef9d0b6f4d5'))),

    16 => ( array ( 'page_id' => 'kementerianpekerjaanumum531c1a8f4d5a3', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'drbayukrisnamurthimsi50f5319d5e336', 1 => 'ilhamakbarhabibie540d573150b04',
                2 => 'trimumpuniwiyatno540ff5d75d0aa', 3 => 'irdjokokirmantodiplhe51a7053b57f68'))),

    17 => ( array ( 'page_id' => 'kementeriankesehatan52f9bdfc6e729', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'faslijalal540d58eb38418', 1 => 'drribkatjiptaning5108fe0ca2b10',
                2 => 'profdralighufrommuktimscphd50f53b79bf202', 3 => 'drdrsitifadilahsuparispjpk518c78b852b69'))),

    18 => ( array ( 'page_id' => 'kementerianpendidikandankebudayaan530d82f25081c', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'abdulmunirmulkhan51a569fe45387', 1 => 'hilmarfarid540ff167e2966',
                2 => 'yudilatif540d30ec826e9', 3 => 'aniesbaswedan521c38cd331a1'))),

    19 => ( array ( 'page_id' => 'kementeriansosial530d8208a2c5a', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'dadangjuliantara540ff02a2b10a', 1 => 'draevakusumasundarimamde50f91dd8e99b3',
                2 => 'hastokristiyanto5301bd1343234', 3 => 'nusronwahid5109b917e9be0'))),

    20 => ( array ( 'page_id' => 'kementerianagama51dd066da966d', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'azyumardiazra540eb6c058c3c', 1 => 'drshlukmanhakimsaifuddin511756084eb20',
                2 => 'musdahmulia540969ae97434', 3 => 'dralwiabdurrahmanshihab5170c3e79726b'))),

    21 => ( array ( 'page_id' => 'kementerianpariwisatakemenparekraf53633a1fcac49', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'garinnugroho540fedd2e2e9b', 1 => 'jeffriegeovanie50f8ab624f790',
                2 => 'miralesmana540d36ba9104e', 3 => 'drmarielkapangestu50ef8750d3096'))),

    22 => ( array ( 'page_id' => 'kementeriankominfo531eec3b4f618', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'ferrymursyidanbaldan511b4ad3edaa1', 1 => 'nezarpatria51c91f6fa079e',
                2 => 'onnowpurbo540feb2a39ae3', 3 => 'suryapaloh511b4aa507a50'))),

    23 => ( array ( 'page_id' => 'kementeriansekretariatnegara53213a4c0309b', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'Maruarar', 1 => 'irhpramonoanungwibowomm50fb45f79eaff',
                2 => 'yuddychrisnandy5119c37be6d9f', 3 => 'aniesbaswedan521c38cd331a1'))),

    24 => ( array ( 'page_id' => 'kementerianrisetdanteknologi5327c284810b7', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'gedewenten540fe573a302b', 1 => 'romisatriawahono540fe4d00bbfd',
                2 => 'yohannessurya540fe3677f517', 3 => 'ilhamakbarhabibie540d573150b04'))),

    25 => ( array ( 'page_id' => 'kementeriankoperasiukm5327bc6c71bf4', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'abdulkadirkarding5285dbba60e22', 1 => 'drakhofifahindarparawansa5189c941e1c45',
                2 => 'nusronwahid5109b917e9be0', 3 => 'rhenaldkasali540d3b1b7b7cd'))),

    26 => ( array ( 'page_id' => 'kementerianpemberdayaanpdanpanak5327af3e64264', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'liesmarcoesnatsir540fde511450d', 1 => 'nanizulminarni540fdd54347cd',
                2 => 'puanmaharani5104bbe40ed12', 3 => 'drakhofifahindarparawansa5189c941e1c45'))),

    27 => ( array ( 'page_id' => 'kementerianlingkunganhidupri52e48dc2f0ae1', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'chalidmuhammad540fdc050b449', 1 => 'charlieheatubun5419366da2629',
                2 => 'widodosambodo540d3be87bbba', 3 => 'profdremilsalim5185e3157caf7'))),

    28 => ( array ( 'page_id' => 'kementerianpanreformasibirokrasi536c957889937', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'ekoprasojo540fd9a4d8dde', 1 => 'trirismaharini52db45dd476f9',
                2 => 'agungadiprasetyo54103f5e8a3d4', 3 => 'basukitjahajapurnama50f600df48ac5'))),

    29 => ( array ( 'page_id' => 'kementerianpembangunandtertinggal5327b8371673e', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'drsakbarfaizalmsi50f778b33337e', 1 => 'andrinofachirchaniago540d3faf44682',
                2 => 'indrajayapiliang5215b4986acc3', 3 => 'budimansudjatmikomscmphil50f8c130dca48'))),

    30 => ( array ( 'page_id' => 'kementerianperencanaanpembnasional5327c12a80069', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'aviliani516b780a967b3', 1 => 'faisalbasri51b7f1e3d9a25',
                2 => 'revrisondbaswir51495a7da0ea5', 3 => 'rhenaldkasali540d3b1b7b7cd'))),

    31 => ( array ( 'page_id' => 'kementerianpeumahanrakyatri528da70e2c401', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'rhenaldkasali540d3b1b7b7cd', 1 => 'suprihantonotodarmojo54103222e6ba3',
                2 => 'ridwankamil51c7d138bb02b', 3 => 'hmarwanjafarsesh50f8f52b84acd'))),

    32 => ( array ( 'page_id' => 'badanusahamiliknegarabumn51f86c5b60980', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'hendrisaparini541033901b044', 1 => 'hkurtubi5243b80d5cce5',
                2 => 'emirsyahsatar51514de7b36f1', 3 => 'dahlaniskan503d887d648d6'))),

    33 => ( array ( 'page_id' => 'kemenpora52f05885a7efd', 'view_type' => 1, 'dept_name' => '', 'dept_img' => '',
            'dept_about' => '', 'large_img' => '',
            'seleksi' => array (0 => 'adhiems5410349908e89', 1 => 'aniesbaswedan521c38cd331a1',
                2 => 'herryzudianto5410359c07df0', 3 => 'drsututadianto5109157318565'))),

);

?>


<br/>
<div class="container" style="margin-top:9px;">
    <div class="sub-header-container">
        <div>
            <img src="<?php echo base_url().'assets/images/hotpages/kementrian/prediksi-kabinet-jokowi2.png';?>">
        </div>
        <div style="margin-top:-90px;background: transparent;">
<?php
foreach($kementrian as $key => $val)
{

    $institusi = $this->redis_slave->get('profile:detail:'.$val['page_id']);
    $insarr = json_decode($institusi, true);

    $img_institusi = (count($insarr['profile_photo']) > 0) ? $insarr['profile_photo'][0]['badge_url'] : '';
?>
    <div class="row-fluid" style="background-color: #ffffff;background: transparent;padding-bottom:20px;border-bottom: solid 2px #BBBBBB;margin-bottom: 10px;">
       <div style="width:960px;height: 235px;;background: transparent;">

          <div style="float:left; width: 170px;min-height: 200px;height: auto;background: transparent;">
             <img style=" background-color: #bbbbbb;width: 170px;height: auto;" src="<?php echo $img_institusi;?>">
          </div>

          <div style="float:left; width: 745px;margin-left:10px;min-height: 200px;height: 235px;overflow: hidden;">
             <h3 style="margin: 0px;line-height: 30px;">
                <a href="<?php echo base_url('aktor/profile/') .'/'. $insarr['page_id'];?>"><?php echo strtoupper($insarr['page_name']);?></a>
             </h3>
             <div class="clearfix"></div>
             <?php $about = strip_tags($insarr['about'], '<p>'); ?>             	
             <p style="padding-right: 10px;"><?php echo character_limiter(nl2br($about), 400);?> [<a href="<?php echo base_url('aktor/profile/') .'/'. $insarr['page_id'];?>">selengkapnya</a>]</p>

          </div>

       </div>

<?php

    if($val['view_type'] == 1)
    {

?>
       <div class="row-fluid" style="margin-left:160px;">
<?php

    foreach($val['seleksi'] as $key => $row)
    {

        $person = $this->redis_slave->get('profile:detail:'.$row);
        $personarr = json_decode($person, true);
        $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

        $where = array('page_id' => $personarr['page_id']);
        $scandal_count = count($personarr['scandal']);
        $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
        $news_count = $news_terkait['cou'];
        $foll = $this->aktor_lib->get_follower($personarr['page_id']);
        $follower_count = $foll->num_rows();

        switch($personarr['partai_id'])
        {
            case 'nasdem5119b72a0ea62' : $_backcolor = "#210068"; $_color = "#000000"; break;
            case 'partaiamanatnasional5119b55ab5fab' : $_backcolor = "#310089"; $_color = "#000000"; break;
            case 'partaidemokrasiindonesiaperjuangan5119ac6bba0ddb' : $_backcolor = "#FF0000"; $_color = "#000000"; break;
            case 'partaikebangkitanbangsa5119b257621a4' : $_backcolor = "#00540D"; $_color = "#000000"; break;
            case 'partaikeadilansejahtera5119b06f84fef' : $_backcolor = "#000000"; $_color = "#ffffff"; break;
            case 'partaigerakanindonesiarayagerindra5119a4028d14c' : $_backcolor = "#993233"; $_color = "#000000"; break;
            case 'partaigolongankarya5119aaf1dadef' : $_backcolor = "#F1FF00"; $_color = "#000000"; break;
            case 'partaihatinuranirakyathanura5119a1cb0fdc1' : $_backcolor = "#FE7E00"; $_color = "#000000"; break;
            case 'partaikeadialndnpersatuanindonesia5119bd7483d2a' : $_backcolor = "#E76241"; $_color = "#000000"; break;
            case 'partaipersatuan518c5908a9e47' : $_backcolor = "#007100"; $_color = "#000000"; break;
            case 'partaibulanbintang52155330a9408' : $_backcolor = "#488774"; $_color = "#000000"; break;

            default :
                $_backcolor = "#cecece"; $_color = "#000000";break;
        }

        if($personarr['page_id'] == 'basukitjahajapurnama50f600df48ac5') { $_backcolor = "#cecece"; $_color = "#000000"; }

?>
            <div style="float: left;width:177px;height: 320px; background-color: #EEEEF4;border-radius: 10px;margin-left:5px;border: solid 4px <?php echo $_backcolor; ?>;">
                <div style="width: 177px;height: 180px;text-align: center;margin-bottom: 5px;">
                    <img style=" background-color: #bbbbbb;width: 177px;height: 177px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                </div>

                <h4 style="line-height: 18px;height:55px;padding-left:5px;padding-right:5px;margin-bottom:5px;font-size: 14.5px;">
                   <a href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>" style="color:<?php echo $_color;?> !important;"><?php echo strtoupper($personarr['page_name']);?></a>
                </h4>

                <div style="width: 177px;margin-bottom: 5px;text-align: left;">
                   <a class="a-block" style="background-color: #cecece;width: 172px;" href="<?php echo base_url();?>aktor/scandals/<?php echo $personarr['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $scandal_count;?></span></a>
                   <a class="a-block" style="width: 172px;" href="<?php echo base_url();?>aktor/news/<?php echo $personarr['page_id'];?>">BERITA <span class="clearfix"><?php echo $news_count;?></span></a>
                   <a class="a-block" style="background-color: #cecece;width: 172px;"  href="<?php echo base_url();?>aktor/follow/<?php echo $personarr['page_id'];?>">FOLLOWER <span class="clearfix"><?php echo $follower_count;?></span></a>
                </div>
            </div>

<?php
     }
?>
       </div>
<?php
    }  // end if view tipe 1


   if($val['view_type'] == 2)
   {
        $scandal_count = array(); $news_count = array(); $follower_count = array();$photo = array();$_color = array();$_backcolor = array();
        foreach($val['seleksi'] as $key => $row)
        {
            $person = $this->redis_slave->get('profile:detail:'.$row);
            $personarr[$key] = json_decode($person, true);
            $photo[$key] =  (count($personarr[$key]['profile_photo']) > 0) ? $personarr[$key]['profile_photo'][0]['badge_url'] : '';

            $where = array('page_id' => $personarr[$key]['page_id']);
            $scandal_count[$key] = count($personarr[$key]['scandal']);
            $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
            $news_count[$key] = $news_terkait['cou'];
            $foll = $this->aktor_lib->get_follower($personarr[$key]['page_id']);
            $follower_count[$key] = $foll->num_rows();

            switch($personarr[$key]['partai_id'])
            {
                case 'nasdem5119b72a0ea62' : $_backcolor[$key] = "#210068"; $_color[$key] = "#000000"; break;
                case 'partaiamanatnasional5119b55ab5fab' : $_backcolor[$key] = "#310089"; $_color[$key] = "#000000"; break;
                case 'partaidemokrasiindonesiaperjuangan5119ac6bba0ddb' : $_backcolor[$key] = "#FF0000"; $_color[$key] = "#000000"; break;
                case 'partaikebangkitanbangsa5119b257621a4' : $_backcolor[$key] = "#00540D"; $_color[$key] = "#000000"; break;
                case 'partaikeadilansejahtera5119b06f84fef' : $_backcolor[$key] = "#000000"; $_color[$key] = "#ffffff"; break;
                case 'partaigerakanindonesiarayagerindra5119a4028d14c' : $_backcolor[$key] = "#993233"; $_color[$key] = "#000000"; break;
                case 'partaigolongankarya5119aaf1dadef' : $_backcolor[$key] = "#F1FF00"; $_color[$key] = "#000000"; break;
                case 'partaihatinuranirakyathanura5119a1cb0fdc1' : $_backcolor[$key] = "#FE7E00"; $_color[$key] = "#000000"; break;
                case 'partaikeadialndnpersatuanindonesia5119bd7483d2a' : $_backcolor[$key] = "#E76241"; $_color[$key] = "#000000"; break;
                case 'partaipersatuan518c5908a9e47' : $_backcolor[$key] = "#007100"; $_color[$key] = "#000000"; break;
                case 'partaibulanbintang52155330a9408' : $_backcolor[$key] = "#488774"; $_color[$key] = "#000000"; break;

                default :
                    $_backcolor[$key] = "#cecece"; $_color[$key] = "#000000";break;
            }

            if($personarr[$key]['page_id'] == 'basukitjahajapurnama50f600df48ac5') { $_backcolor[$key] = "#cecece"; $_color[$key] = "#000000";}
        }


?>
       <div class="row-fluid" style="height: auto;background: url('<?php echo $val['large_img'];?>') no-repeat;">
           <div style="float:left;width: 350px;height: 470px;">
               <!-- img style="width: 300px;height: auto" src="< ?php echo $val['large_img'];?>"-->
               <div style="width: 350px;height: 400px;"></div>
               <div style="background-color: #000000;margin-top: 0px;width: 300px;margin-left:30px;opacity:0.6">
                  <a class="a-block a-block-1" style="width: 300px;background: transparent;color: #ffffff;" href="<?php echo base_url();?>aktor/scandals/<?php echo $personarr[0]['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $scandal_count[0];?></span></a>
                  <a class="a-block" style="width: 300px;background: transparent;color: #ffffff;" href="<?php echo base_url();?>aktor/news/<?php echo $personarr[0]['page_id'];?>">BERITA <span class="clearfix"><?php echo $news_count[0];?></span></a>
                  <a class="a-block a-block-1" style="width: 300px;background: transparent;color: #ffffff;" href="<?php echo base_url();?>aktor/follow/<?php echo $personarr[0]['page_id'];?>">FOLLOWER <span class="clearfix"><?php echo $follower_count[0];?></span></a>
                  <div style="background-color: <?php echo $_backcolor[0];?>;width: 300px;height: 10px;"></div>
               </div>
           </div>

           <div style="float:left;width: 590px;">
              <h4 style="line-height: 18px;font-size: 14.5px;margin-bottom: 5px;">
                 <a style="color:<?php echo $_color[0];?> !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr[0]['page_id'];?>"><?php echo strtoupper($personarr[0]['page_name']);?></a>
              </h4>
              <p><?php echo character_limiter(nl2br($personarr[0]['about']), 380);?>  [<a href="<?php echo base_url('aktor/profile/') .'/'. $personarr[0]['page_id'];?>">selengkapnya</a>]</p>

               <?php
               for ($_i = 1; $_i <= 3; $_i++)
               {
               ?>

               <div style="float: left;width:177px;height: 320px; background-color: #EEEEF4;border-radius: 10px;
                   margin-left:5px;border: solid 4px <?php echo $_backcolor[$_i]; ?>;">
                  <div style="width: 177px;height: 180px;text-align: center;margin-bottom: 5px;">
                      <img style=">background-color: #bbbbbb;width: 177px;height: 177px;border-radius: 6px 6px 0 0;" src="<?php echo $photo[$_i];?>">
                  </div>

                  <h4 style="line-height: 18px;height:55px;padding-left:5px;padding-right:5px;margin-bottom:5px;font-size: 14.5px;">
                      <a style="color:<?php echo $_color[$_i];?> !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr[$_i]['page_id'];?>"><?php echo strtoupper($personarr[$_i]['page_name']);?></a>
                  </h4>

                  <div style="width: 177px;margin-bottom: 5px;text-align: left;">
                      <a class="a-block" style="background-color: #cecece;width: 172px;" href="<?php echo base_url();?>aktor/scandals/<?php echo $personarr[$_i]['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $scandal_count[$_i];?></span></a>
                      <a class="a-block" style="width: 172px;" href="<?php echo base_url();?>aktor/news/<?php echo $personarr[$_i]['page_id'];?>">BERITA <span class="clearfix"><?php echo $news_count[$_i];?></span></a>
                      <a class="a-block" style="background-color: #cecece;width: 172px;" href="<?php echo base_url();?>aktor/follow/<?php echo $personarr[$_i]['page_id'];?>">FOLLOWER <span class="clearfix"><?php echo $follower_count[$_i];?></span></a>
                  </div>
               </div>

               <?php
               }
               ?>

           </div>

       </div>
<?php
   }   // end if view tipe 2
?>
    </div>

<?php
}


?>

        </div>
    </div>
</div>