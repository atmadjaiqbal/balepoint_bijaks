<!-- audio id="mars-pemilu" autoplay>
    <source src="< ?php echo base_url('assets/images/mars_pemilu.mp3'); ?>" type="audio/mpeg">
</audio -->

<!-- div class="container" style="margin-top: 30px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="< ?php echo base_url(); ?>">
                <img src="< ?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="< ?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right" style="background: none;">
                    <h1>KRISIS GAZA</h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='< ?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>
</div -->
<br/>
<div class="container" style="margin-top:9px;">
    <div class="sub-header-container">
        <div id="gazas" class="row-fluid">
            <div class="span12 gazas-top-section">
                <table>
                    <tr style="height: 300px;">
                        <td colspan="5"></td>
                    </tr>

                    <tr>
                        <span id="korban"></span>
                        <td style="width: 300px;padding: 10px;">
                            <div class="row-fluid flyinfo-p">
                                <div class="overlay-date">
                                    <p style="margin-top: -4px;font-size: 12px;margin-left:-11;">MINGGU 5</p>
                                    <p style="margin-top: -27px;font-size:10px;margin-left:14px;">05/8-11/08</p>
                                </div>
                                <div style="float:left;width: 2px;height: 85px;background-color: #ffffff;margin-left:-23px;"></div>
                                <div class="overlay-info">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:15px;">
                                        <li style="line-height: 18px;">
                                            <span  style="font-size: 20px; font-weight: bold;">>50</span> <span style="font-size: 18px; font-weight: bold;">tewas</span>
                                        </li>
                                        <li style="line-height: 15px;">
                                            <span  style="font-size: 13px;">Serangan ke masjid & RS.</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row-fluid flyinfo-p">
                                <div class="overlay-date">
                                    <p style="margin-top: -4px;font-size: 12px;margin-left:-11;">MINGGU 4</p>
                                    <p style="margin-top: -27px;font-size:10px;margin-left:14px;">29/7-04/8</p>
                                </div>
                                <div style="float:left;width: 2px;height: 85px;background-color: #ffffff;margin-left:-23px;"></div>
                                <div class="overlay-info">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:7px;width:170px;">
                                        <li style="line-height: 18px;"><span  style="font-size: 20px; font-weight: bold;">>400</span> <span style="font-size: 18px; font-weight: bold;">tewas</span></li>
                                        <li style="line-height: 14px;">
                                            <span  style="font-size: 13px;">1 UK tewas; Listrik padam; Serangan ke UN shelter, univ& masjid;ratusan luka2</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row-fluid flyinfo-p">
                                <div class="overlay-date">
                                    <p style="margin-top: -4px;font-size: 12px;margin-left:-11;">MINGGU 3</p>
                                    <p style="margin-top: -27px;font-size:10px;margin-left:14px;">22/7-28/7</p>
                                </div>
                                <div style="float:left;width: 2px;height: 85px;background-color: #ffffff;margin-left:-23px;"></div>
                                <div class="overlay-info">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:20px;">
                                        <li style="line-height: 18px;"><span  style="font-size: 20px; font-weight: bold;">823</span> <span style="font-size: 18px; font-weight: bold;">tewas</span></li>
                                        <li style="line-height: 15px;"><span  style="font-size: 13px;">800 warga; 10 di RS;13 di UN Shelter;Ribuan luka2</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row-fluid flyinfo-p">
                                <div class="overlay-date">
                                    <p style="margin-top: -4px;font-size: 12px;margin-left:-11;">MINGGU 2</p>
                                    <p style="margin-top: -27px;font-size:10px;margin-left:14px;">15/7-21/7</p>
                                </div>
                                <div style="float:left;width: 2px;height: 85px;background-color: #ffffff;margin-left:-23px;"></div>
                                <div class="overlay-info">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:25px;">
                                        <li style="line-height: 28px;"><span  style="font-size: 20px; font-weight: bold;">0</span> <span style="font-size: 18px; font-weight: bold;">korban</span></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row-fluid flyinfo-p">
                                <div class="overlay-date">
                                    <p style="margin-top: -4px;font-size: 12px;margin-left:-11;">MINGGU 1</p>
                                    <p style="margin-top: -27px;font-size:10px;margin-left:8px;">08/07-14/07</p>
                                </div>
                                <div style="float:left;width: 2px;height: 85px;background-color: #ffffff;margin-left:-23px;"></div>
                                <div class="overlay-info">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:10px;">
                                        <li style="line-height: 18px;"><span  style="font-size: 20px; font-weight: bold;">125</span> <span style="font-size: 18px; font-weight: bold;">tewas</span></li>
                                        <li style="line-height: 15px;"><span  style="font-size: 13px;">125 warga,<br/>22 perempuan & anak2, <br/>370 luka2</li>
                                    </ul>
                                </div>
                            </div>

                        </td>
                        <td style="width: 5px;"></td>
                        <td style="width: 300px;">
                            <div class="center-info-victim">
                                <table style="background: url(<?php echo base_url().'assets/images/hotpages/gaza/background-gaza.png'; ?>) no-repeat left;height: 300px;">
                                   <tr>
                                      <td class="center-judul-palestina" style="border-radius: 5px 0 0 0;">PALESTINA</td>
                                      <td class="center-judul-israel" style="border-radius: 0 5px 0 0;">ISRAEL</td>
                                   </tr>
                                   <tr>
                                      <td class="center-total-palestina">
                                          <p style="font-size: 28px;margin-top:10px;color:#DC0000;font-weight: bold;">1.922 tewas</p>
                                          <p style="font-size:16px;margin-top:-5px;margin-left:30px;">1.407 sipil</p>
                                          <p style="font-size:16px;margin-top:-10px;margin-left:30px;">448 anak-anak</p>
                                          <p style="font-size:16px;margin-top:-10px;margin-left:30px;">235 wanita</p>
                                      </td>
                                      <td class="center-total-israel">
                                          <p style="font-size: 28px;margin-top:10px;color:#DC0000;font-weight: bold;">67 tewas</p>
                                          <p style="font-size:16px;margin-top:-5px;margin-right:30px;">64 tentara</p>
                                          <p style="font-size:16px;margin-top:-10px;margin-right:30px;">2 sipil</p>
                                          <p style="font-size:16px;margin-top:-10px;margin-right:30px;">1 warga Thailand</p>
                                      </td>
                                   </tr>
                                   <tr>
                                      <td colspan="2" class="center-total-palestina">
                                         <div style="margin-left:12px;margin-top:-10px;height: auto;width:390px;border-top:solid 2px #E5E5E5">
                                            <p style="font-size: 20px;">
                                               <ul style="margin-left:15px;">
                                                 <li style="font-size: 18px;line-height: 24px;"><span style="font-size: 18px;font-weight: bold">485.000</span> warga tinggal di penampungan</li>
                                                 <li style="font-size: 18px;line-height: 35px;"><span style="font-size: 18px;font-weight: bold">1.500.000</span> di penampungan tanpa sumber air</li>
                                                 <li style="font-size: 18px;line-height: 24px;"><span style="font-size: 18px;font-weight: bold">373.000</span> anak anak menderita psychosocial</li>
                                               </ul>
                                            </p>
                                         </div>
                                      </td>
                                   </tr>
                                </table>
                            </div>
                        </td>
                        <td style="width: 5px;"></td>
                        <td style="width: 300px;padding: 10px;">
                            <div class="row-fluid flyinfo-i">
                                <div class="overlay-date-i">
                                    <p style="margin-top: 0px;font-size: 10px;margin-left:14px;">05/8-11/08</p>
                                    <p style="margin-top: -27px;font-size:12px;margin-left:6px;">MINGGU 5</p>
                                </div>
                                <div style="float:right;width: 2px;height: 85px;background-color: #ffffff;margin-right:-24px;"></div>
                                <div class="overlay-info-i text-right">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:29px;">
                                        <li style="line-height: 18px;"><span  style="font-size: 20px; font-weight: bold;">3</span> <span style="font-size: 18px; font-weight: bold;">tewas</span></li>
                                        <li style="line-height: 15px;"><span  style="font-size: 13px;">2 luka2.</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row-fluid flyinfo-i">
                                <div class="overlay-date-i">
                                    <p style="margin-top: 0px;font-size: 10px;margin-left:14px;">29/7-04/8</p>
                                    <p style="margin-top: -27px;font-size:12px;margin-left:6px;">MINGGU 4</p>
                                </div>
                                <div style="float:right;width: 2px;height: 85px;background-color: #ffffff;margin-right:-24px;"></div>
                                <div class="overlay-info-i text-right">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:25px;">

                                        <li style="line-height: 28px;">
                                            <span  style="font-size: 20px; font-weight: bold;">0</span> <span style="font-size: 18px; font-weight: bold;">korban</span>
                                        </li>
                                        <li style="line-height: 15px;"><span  style="font-size: 13px;">1 tentara di culik.</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row-fluid flyinfo-i">
                                <div class="overlay-date-i">
                                    <p style="margin-top: 0px;font-size: 10px;margin-left:14px;">22/7-28/7</p>
                                    <p style="margin-top: -27px;font-size:12px;margin-left:6px;">MINGGU 3</p>
                                </div>
                                <div style="float:right;width: 2px;height: 85px;background-color: #ffffff;margin-right:-24px;"></div>
                                <div class="overlay-info-i text-right">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:15px;">
                                        <li style="line-height: 18px;"><span  style="font-size: 20px; font-weight: bold;">>35</span> <span style="font-size: 18px; font-weight: bold;">tewas</span></li>
                                        <li style="line-height: 15px;"><span  style="font-size: 13px;">1 warga ashkelon.</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row-fluid flyinfo-i">
                                <div class="overlay-date-i">
                                    <p style="margin-top: 0px;font-size: 10px;margin-left:14px;">15/7-21/7</p>
                                    <p style="margin-top: -27px;font-size:12px;margin-left:6px;">MINGGU 2</p>
                                </div>
                                <div style="float:right;width: 2px;height: 85px;background-color: #ffffff;margin-right:-24px;"></div>
                                <div class="overlay-info-i text-right">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:29px;">
                                        <li style="line-height: 18px;"><span  style="font-size: 20px; font-weight: bold;">5</span> <span style="font-size: 18px; font-weight: bold;">tewas</span></li>
                                        <li style="line-height: 15px;"><span  style="font-size: 13px;"></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="row-fluid flyinfo-i">
                                <div class="overlay-date-i">
                                    <p style="margin-top: 0px;font-size: 10px;margin-left:9px;">08/07-14/07</p>
                                    <p style="margin-top: -27px;font-size:12px;margin-left:6px;">MINGGU 1</p>
                                </div>
                                <div style="float:right;width: 2px;height: 85px;background-color: #ffffff;margin-right:-24px;"></div>
                                <div class="overlay-info-i text-right">
                                    <ul style="list-style-type: none;;padding-left: 0px;margin-top:20px;">
                                        <li style="line-height: 18px;"><span  style="font-size: 20px; font-weight: bold;">1</span> <span style="font-size: 18px; font-weight: bold;">tewas</span></li>
                                        <li style="line-height: 15px;"><span  style="font-size: 13px;">warga sipil di Haifa</li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <p class="text-right" style="font-size: 11px;line-height: 14px;margin-right: 35px;margin-top: -20px">Source: UN Office For The Coordination Of Humanitarian affairs (OCHA) &nbsp; &nbsp; <strong>|</strong> &nbsp; &nbsp;  Data per tanggal 11 Agustus 2014.</p>
                        </td>
                    </tr>

                    <tr>
                       <td colspan="5">
                       <span id="perang"></span>
                         <table style="background: none;">
                           <tr><td colspan="3">
                                 <div class="tagline"><span class="title">PERANG SEBELUMNYA</span></div>
                               </td>
                           </tr>
                           <tr><td colspan="3" class="batas-row1"></td></tr>
                           <tr><td colspan="3">
                                   <span style="margin-left:10px;font-size:14px;">2008-2009 Operation Cast Lead 27 December - 18 Januari 2009 (22 Hari)</span>
                               </td>
                           </tr>
                           <tr>
                             <td style="width: 600px;padding-top: 5px;padding-left:10px;">
                                 <div class="box-content" style="width: 600px;height:150px;">
                                     <div style="width: 600px;border-radius: 5px 5px 0 0;background-color: #BAFCD9">
                                         <p style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">PALESTINA</p>
                                     </div>
                                     <div style="height: 2px;background-color: #ffffff;margin-top: -10px;"></div>
                                     <table style="background: none;">
                                         <tr>
                                             <td style="width: 200px;border-bottom: solid 2px #E5E5E5;">
                                                 <p style="padding-left: 10px;font-size: 24px;font-weight: bold;margin-top:15px;">1.391 Tewas:</p>
                                             </td>
                                             <td style="width: 370px;border-bottom: solid 2px #E5E5E5;">
                                                <ul>
                                                <?php
                                                for ($k = 1; $k <= (154 - $k); $k++)
                                                {
                                                    ?>
                                                    <li class="icon-death">
                                                        <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people.png'; ?>">
                                                    </li>
                                                <?php
                                                }
                                                ?>
                                                </ul>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td style="width: 200px;border-bottom: solid 2px #E5E5E5;">
                                                 <p style="padding-left: 30px;font-size: 16px;">759 Masyarakat Sipil</p>
                                             </td>
                                             <td style="width: 370px;border-bottom: solid 2px #E5E5E5;">
                                                 <ul>
                                                     <?php
                                                     for ($k = 1; $k <= (84 - $k); $k++)
                                                     {
                                                         ?>
                                                         <li class="icon-death">
                                                             <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people.png'; ?>">
                                                         </li>
                                                     <?php
                                                     }
                                                     ?>
                                                 </ul>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td style="width: 200px;">
                                                 <p style="padding-left: 30px;font-size: 16px;">632 Warga Lainnya</p>
                                             </td>
                                             <td style="width: 370px;">
                                                 <ul>
                                                     <?php
                                                     for ($k = 1; $k <= (72 - $k); $k++)
                                                     {
                                                         ?>
                                                         <li class="icon-death">
                                                             <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people.png'; ?>">
                                                         </li>
                                                     <?php
                                                     }
                                                     ?>
                                                 </ul>
                                             </td>
                                         </tr>
                                     </table>


                                 </div>
                             </td>

                             <td style="width: 10px;"></td>

                             <td style="width: 285px;padding-top: 5px;">
                                 <div class="text-right box-content" style="width: 285px;height:150px;">
                                     <div style="width: 285px;border-radius: 5px 5px 0 0;background-color: #A4C0FC;">
                                         <p style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">ISRAEL</p>
                                     </div>
                                     <div style="height: 2px;background-color: #ffffff;margin-top: -10px;"></div>
                                     <table style="background: none;">
                                         <tr>
                                             <td style="width: 150px;border-bottom: solid 2px #E5E5E5;">
                                                 <ul class="text-right">
                                                     <li class="icon-death" style="float: right !important;">
                                                         <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people-03.png'; ?>">
                                                     </li>
                                                 </ul>
                                             </td>
                                             <td style="width: 200px;border-bottom: solid 2px #E5E5E5;">
                                                 <p style="padding-right: 10px;font-size: 24px;font-weight: bold;margin-top:15px;">9 Tewas:</p>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td style="width: 150px;border-bottom: solid 2px #E5E5E5;">
                                                 <ul>
                                                     <li class="icon-death" style="float: right !important;">
                                                         <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people-03.png'; ?>">
                                                     </li>
                                                 </ul>
                                             </td>
                                             <td style="width: 200px;border-bottom: solid 2px #E5E5E5;">
                                                 <p style="padding-right: 30px;font-size: 16px;">3 Masyarakat Sipil</p>
                                             </td>
                                         </tr>
                                         <tr>
                                             <td style="width: 150px;">
                                                 <ul>
                                                     <li class="icon-death" style="float: right !important;">
                                                         <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people-03.png'; ?>">
                                                     </li>
                                                 </ul>
                                             </td>
                                             <td style="width: 200px;">
                                                 <p style="padding-right: 30px;font-size: 16px;">6 Warga Lainnya</p>
                                             </td>
                                         </tr>
                                     </table>
                                 </div>
                               </td>
                             </tr>

                             <tr><td colspan="3" class="batas-row1"></td></tr>
                             <tr><td colspan="3" style="padding-top:10px;">
                                     <span style="margin-top:5px;margin-left:10px;font-size:14px;">2012: Operation Pillar of Defence 14-21 Nov (8 Hari)</span>
                                 </td>
                             </tr>

                           <tr>
                               <td style="width: 600px;padding-top: 5px;padding-left:10px;">
                                   <div class="box-content" style="width: 600px;height:150px;">
                                       <div style="width: 600px;border-radius: 5px 5px 0 0;background-color: #BAFCD9">
                                           <p style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">PALESTINA</p>
                                       </div>
                                       <div style="height: 2px;background-color: #ffffff;margin-top: -10px;"></div>
                                       <table style="background: none;">
                                           <tr>
                                               <td style="width: 200px;border-bottom: solid 2px #E5E5E5;">
                                                   <p style="padding-left: 10px;font-size: 24px;font-weight: bold;margin-top:15px;">167 Tewas:</p>
                                               </td>
                                               <td style="width: 370px;border-bottom: solid 2px #E5E5E5;">
                                                   <ul>
                                                       <?php
                                                       for ($k = 1; $k <= 27; $k++)
                                                       {
                                                           ?>
                                                           <li class="icon-death">
                                                               <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people.png'; ?>">
                                                           </li>
                                                       <?php
                                                       }
                                                       ?>
                                                   </ul>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td style="width: 200px;border-bottom: solid 2px #E5E5E5;">
                                                   <p style="padding-left: 30px;font-size: 16px;">87 Warga Sipil</p>
                                               </td>
                                               <td style="width: 370px;border-bottom: solid 2px #E5E5E5;">
                                                   <ul>
                                                       <?php
                                                       for ($k = 1; $k <= 14; $k++)
                                                       {
                                                           ?>
                                                           <li class="icon-death">
                                                               <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people.png'; ?>">
                                                           </li>
                                                       <?php
                                                       }
                                                       ?>
                                                       <li class="icon-death">
                                                           <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people-03.png'; ?>">
                                                       </li>
                                                   </ul>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td style="width: 200px;">
                                                   <p style="padding-left: 30px;font-size: 16px;">80 Warga Lainnya</p>
                                               </td>
                                               <td style="width: 370px;">
                                                   <ul>
                                                       <?php
                                                       for ($k = 1; $k <= 13; $k++)
                                                       {
                                                           ?>
                                                           <li class="icon-death">
                                                               <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people.png'; ?>">
                                                           </li>
                                                       <?php
                                                       }
                                                       ?>
                                                   </ul>
                                               </td>
                                           </tr>
                                       </table>


                                   </div>
                               </td>
                               <td style="width: 10px;"></td>

                               <td style="width: 285px;padding-top: 5px;">
                                   <div class="text-right box-content" style="width: 285px;height:150px;">
                                       <div style="width: 285px;border-radius: 5px 5px 0 0;background-color: #A4C0FC;">
                                           <p style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">ISRAEL</p>
                                       </div>
                                       <div style="height: 2px;background-color: #ffffff;margin-top: -10px;"></div>
                                       <table style="background: none;">
                                           <tr>
                                               <td style="width: 150px;border-bottom: solid 2px #E5E5E5;">
                                                   <ul class="text-right">
                                                       <li class="icon-death" style="float: right !important;">
                                                           <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people-03.png'; ?>">
                                                       </li>
                                                   </ul>
                                               </td>
                                               <td style="width: 200px;border-bottom: solid 2px #E5E5E5;">
                                                   <p style="padding-right: 10px;font-size: 24px;font-weight: bold;margin-top:15px;">6 Tewas:</p>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td style="width: 150px;border-bottom: solid 2px #E5E5E5;">
                                                   <ul>
                                                       <li class="icon-death" style="float: right !important;">
                                                           <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people-03.png'; ?>">
                                                       </li>
                                                   </ul>
                                               </td>
                                               <td style="width: 200px;border-bottom: solid 2px #E5E5E5;">
                                                   <p style="padding-right: 30px;font-size: 16px;">4 Masyarakat Sipil</p>
                                               </td>
                                           </tr>
                                           <tr>
                                               <td style="width: 150px;">
                                                   <ul>
                                                       <li class="icon-death" style="float: right !important;">
                                                           <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iicon-people-03.png'; ?>">
                                                       </li>
                                                   </ul>
                                               </td>
                                               <td style="width: 200px;">
                                                   <p style="padding-right: 30px;font-size: 16px;">2 Tentara</p>
                                               </td>
                                           </tr>
                                       </table>
                                   </div>
                               </td>
                           </tr>

                           </table>
                       </td>
                    </tr>

                    <tr><td colspan="3" class="batas-row1"></td></tr>

                    <tr>
                        <td colspan="5">
                           <span id="pemain_berita"></span>
                           <table style="background: none;">
                               <tr>
                                   <td style="width: 610px;"><div class="tagline"><span class="title">NEGARA PEMAIN</span></div></td>
                                   <td style="width: 5px;"></td>
                                   <td style="width: 300px;"><div class="tagline"><span class="title">BERITA TERKAIT</span></div></td>
                               </tr>
                               <td style="width: 610px;padding-left: 10px;">
                                   <div id="accordion" class="panel-group" style="width: 610px;height:auto;">
                                       <div class="panel panel-default box-content" style="margin-bottom: 10px;">
                                           <div class="panel-heading title-pemain">
                                               <a data-toggle="collapse" data-parent="#accordion" href="#palestina" style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">OTORITAS PALESTINA &nbsp;&nbsp;<span class="judul-coll" style="font-size: 10px;font-weight: normal;">Klik untuk selengkapnya</span></a>
                                               <span class="text-right" style="float:right;"><img src="<?php echo base_url().'assets/images/hotpages/gaza/Palestine-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;"></span>
                                           </div>
                                           <div id="palestina" class="panel-collapse collapse in">
                                               <div class="panel-body" style="margin-top:5px;">
                                                   <p style="padding-left: 10px;padding-right: 10px;">Fatah, fraksi Palestina yang mengendalikan Tepi Barat. Fatah dan Hamas telah lama saling berperang, namun awal tahun ini telah melakukan upaya lain pada persatuan pemerintahan.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pemimpin Palestina Mahmoud Abbas yang mempunyai kekuasaan pada pemerintahan di Tepi Barat menyebutkan bahwa sepertinya secara politik telah kelelahan dengan semua lika-liku yang ada dan  akhirnya mencari solusi yang lebih bertahan lama.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Dan salah satu kesempatan untuk menegaskan kembali otoritas melalui pemerintahan gabungan akan memaksa Hamas menjadi bagian dari pemerintahan itu dan peran militan akan berkurang dan hilang.</p>
                                                   <!-- p style="padding-left: 10px;padding-right: 10px;">Membayar harga untuk semua ini adalah pemain lain kunci: Fatah, faksi Palestina yang menguasai Tepi Barat. Fatah dan Hamas telah lama berperang satu sama lain, tapi awal tahun ini membuat upaya lain di pemerintah persatuan.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pemimpin Palestina Mahmoud Abbas, yang bertanggung jawab dari pemerintah di Tepi Barat, "tampaknya politik kelelahan oleh semua liku-liku yang dibuatnya pada mencari solusi tahan lama," kata Soufan Gruop. "Dan satu kesempatan menegaskan kembali otoritasnya melalui pemerintah persatuan yang akan memaksa Hamas menjadi peran bawahan dan kurang militan kini telah menghilang. Dia sekarang harus menyaksikan tanpa daya ketika protes di Tepi Barat membatalkan kemajuan apa pun telah membuat arah dua- solusi negara."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Keberanian Hamas dan pembunuhan atas warga Israel dapat memberikan dorongan popularitas ke grup. Tapi Palestina merenungkan taktik Hamas. Mereka akan menyalahkan terutama Israel, tidak ada pertanyaan, tetapi mereka akan melihat pada strategi Hamas yang mengakibatkan begitu banyak kematian dan kehancuran.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Presiden Otorita Palestina Mahmoud Abbas adalah salah satu yang pertama untuk mengatakan secara terbuka. Ketika Israel memperingatkan Hamas untuk menghentikan serangan roket yang memicu serangan darat, Abbas terdengar jengkel . "Apa yang Anda coba untuk mencapai meluncurkan roket?" tanyanya Hamas, menambahkan, "Kami lebih memilih untuk bertarung dengan kebijaksanaan."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Dengan pejuang Hamas tersembunyi di terowongan, menggambar tembakan Israel ke wilayah sipil, tidak peduli berapa banyak Anda bencilah musuhmu, adalah taktik yang menjamin pemeriksaan jika tidak kecaman langsung.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Dalam pergeseran mencolok dari konflik sebelumnya, kritik Hamas tersebar luas di media Arab. Wartawan Mesir, khususnya, mengecam keras Hamas .</p -->
                                               </div>
                                           </div>
                                       </div>

                                       <div class="panel panel-default box-content" style="margin-bottom: 10px;">
                                           <div class="panel-heading title-pemain"">
                                               <a data-toggle="collapse" data-parent="#accordion" href="#israel" style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">ISRAEL &nbsp;&nbsp;<span  class="judul-coll" style="font-size: 10px;font-weight: normal;">Klik untuk selengkapnya</span></a>
                                               <span class="text-right" style="float:right;"><img src="<?php echo base_url().'assets/images/hotpages/gaza/Israel-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;"></span>
                                           </div>
                                           <div id="israel" class="panel-collapse collapse">
                                               <div class="panel-body" style="margin-top:5px;">
                                                   <p style="padding-left: 10px;padding-right: 10px;">Seperti ketika bangsa Israel berlindung dari roket roket-roket Hamas dan kelompok Islam yang berbasis di Gaza mengumumkan bahwa pesawat yang terbang dari dan ke bandara Israel menjadi sasaran roket tersebut, seorang pembawa acara televisi lebanon menawarkan solusi yang paling tidak membantu. Dia mengatakan bahwa Iran seharusnya memberikan senjata nuklir kepada Hamas untuk melawan Israel.Sementara itu pemimpin tertinggi Iran menegaskan kembali harapannya untuk melihat Israel hancur.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pesan ini mengingatkan bangsa Israel akan seriusnya situasi dan kondisi saat ini. Hamas tidak tertarik dengan solusi untuk dua negara. Mereka berkomitmen untuk menghancurkan Israel. Israel memang memiliki ketidaksepakatan mengenai penyelesaian dan penarikan dari Tepi Barat. Namun ketika mengenai Hamas maka tidak diragukan lagi. Piagam Hamas sendiri berbunyi “Perjuangan kami untuk melawan kaum Yahudi merupakan hal yang sangat besar dan serius.”</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Seperti yang ditulis oleh David Grossman bahwa warga sayap kiri Israel kini telah melihat ketakutan warga sayap kanan bukan karena paranoia dan mereka melihat bahwa ada batasan untuk penggunaan kekuatan.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pertempuran ini akan mengikis posisi ekstrim kiri dan ekstrim kanan di Israel, memberikan dorongan untuk pendukung pragmatis  keamanan. Debat akan menghidupkan kembali seruan untuk menemukan pilihan lain dan memperkuat pasukan yang lebih moderat di antara rakyat Palestina yang telah kehilangan tempat tinggal untuk Hamas.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Keharusan untuk menemukan cara baru yang lebih moderat bagi rakyat Palestina akan muncul kembali setelah ketegangan mereda. Namun hambatan lama telah berkembang dimana ketakutan terbesar Israel adalah negara Palestina akan jatuh untuk kelompok ekstrim seperti yang terjadi di jalur Gaza dimana Hamas menerima senjata dari Iran dan negara lainnya. Tidak ada lagi cara bagi Israel untuk menerima pasukan bersenjata Palestina dalam jarak tertentu dari kota-kota besar terutama jika Hamas tetap menjadi salah satu pemain yang berbahaya.</p>
                                                   <!-- p style="padding-left: 10px;padding-right: 10px;">Sebagai orang Israel berlindung dari roket Hamas dan kelompok Islam yang berbasis di Gaza mengumumkan juga menargetkan pesawat terbang masuk dan keluar dari bandara utama Israel, seorang penyiar televisi di Lebanon menawarkan saran paling tidak membantu. Iran, katanya, harus memberikan senjata nuklir untuk Hamas untuk melawan Israel. Sementara itu, Pemimpin Tertinggi Iran menegaskan keinginannya untuk melihat Israel menyeka.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pesan ini mengingatkan Israel dari keseriusan situasi mereka. Hamas tidak memiliki kepentingan dalam solusi dua-negara. Hal ini berkomitmen untuk melenyapkan Israel. Israel memang memiliki perbedaan pendapat mengenai permukiman dan penarikan dari Tepi Barat. Tapi ketika datang ke Hamas, tidak ada pertanyaan. The Hamas piagam berbunyi: "Perjuangan kita melawan Yahudi sangat besar dan sangat serius."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Sebagai penulis David Grossman meletakkannya , kiri Israel sekarang melihat bahwa "ketakutan sayap kanan itu tidak hanya paranoia," dan kanan akan melihat bahwa ada batas untuk penggunaan kekuatan.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pertarungan ini akan mengikis posisi ekstrim kiri dan ekstrim kanan di Israel, memberikan dorongan untuk pendukung pragmatis keamanan. Perdebatan akan menghidupkan kembali panggilan untuk menemukan pilihan lain dan memperkuat pasukan yang lebih moderat di kalangan rakyat Palestina yang telah kehilangan tanah untuk Hamas.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Keharusan untuk menemukan cara baru ke depan dengan Palestina moderat akan muncul kembali setelah saraf berjumbai keren. Tapi hambatan tua telah berkembang: ketakutan terbesar Israel 'adalah negara Palestina jatuh ke ekstremis, Gaza lakukan untuk Hamas, menerima senjata dari Iran dan lain-lain. Tidak ada cara Israel sekarang akan menerima sebuah negara Palestina yang bersenjata dalam jarak tunneling dari kota-kota besar, terutama jika Hamas tetap menjadi pemain berbahaya.</p -->
                                               </div>
                                           </div>
                                       </div>

                                       <div class="panel panel-default box-content" style="margin-bottom: 10px;">
                                           <div class="panel-heading title-pemain"">
                                               <a data-toggle="collapse" data-parent="#accordion" href="#iran" style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">IRAN DAN SURIAH &nbsp;&nbsp;<span  class="judul-coll" style="font-size: 10px;font-weight: normal;">Klik untuk selengkapnya</span></a>
                                               <span class="text-right" style="float:right;">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/Iran-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/Syria-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;">
                                               </span>
                                           </div>
                                           <div id="iran" class="panel-collapse collapse">
                                               <div class="panel-body" style="margin-top:5px;">
                                                   <p style="padding-left: 10px;padding-right: 10px;">Iran telah lama mendukung Hammas, memasok senjata dan Meshaal digunakan untuk berbasis di Suriah.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Namun itu berubah. Pada tahun 2012, Meshaal meninggalkan Suriah sebagai negara dengan perang saudara yang mendalam, keputusan yang diyakini akan menyebabkan gangguan pada hubungan dengan Iran, disebutkan oleh Firas Abi Ali. Tehran sejalan dengan rezim Presiden Suriah, Bashar al-Assad.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Saat ini, Syria (Tetangga Israel untuk bagian utara) telah terkunci dalam kebrutalan perang saudara dengan kelompuok ekstrimis Islam.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Telah terjadi beberapa bentrokan di masyarakat Palestina antara Hamas dan bagian lain dari otoritas Palestina. ada Sunnis vs Shaia, kemudian Iran vs Saudi Arabia dan Arab. Ada sekularis vs rakyat yang memeluk agama di ruang politik.</p>

                                                   <!-- p style="padding-left: 10px;padding-right: 10px;">Iran telah lama mendukung Hamas, memasok dengan senjata. Dan Meshaal digunakan untuk berbasis di Suriah.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Tapi itu berubah. Pada tahun 2012, Meshaal meninggalkan Suriah sebagai perang saudara di negara itu diperdalam - keputusan diyakini telah menyebabkan gangguan dalam hubungannya dengan Iran juga, kata Firas Abi Ali, kepala Timur Tengah dan Afrika Utara Country Risk dan Peramalan di informasi global Perusahaan IHS. Tehran sejalan dengan rezim Presiden Suriah Bashar al-Assad.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Sekarang, Suriah - tetangga Israel ke utara - terkunci dalam brutal, perang saudara multipartai, dengan ekstrimis Islam mengangkat kepala terputus ke kutub . Perang, diyakini telah menewaskan lebih dari 115.000 orang, hanyalah salah satu dari banyak perkembangan menekankan berapa banyak "jalur patahan" yang ada di wilayah tersebut, Richard Haass, presiden Council on Foreign Relations, mengatakan "CNN Tonight."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Ada jalur patahan dalam Palestina antara Hamas dan bagian lain dari Otoritas Palestina. Anda memiliki Sunni vs Syiah. Anda memiliki Iran vs Arab Saudi dan Arab. Anda memiliki sekuler vs orang-orang yang memeluk agama dalam ruang politik."</p  -->
                                               </div>
                                           </div>
                                       </div>

                                       <div class="panel panel-default box-content" style="margin-bottom: 10px;">
                                           <div class="panel-heading title-pemain"">
                                               <a data-toggle="collapse" data-parent="#accordion" href="#turki" style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">TURKI DAN QATAR &nbsp;&nbsp;<span  class="judul-coll" style="font-size: 10px;font-weight: normal;">Klik untuk selengkapnya</span></a>
                                               <span class="text-right" style="float:right;">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/Turkey-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/Qatar-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;">
                                               </span>
                                           </div>
                                           <div id="turki" class="panel-collapse collapse">
                                               <div class="panel-body" style="margin-top:5px;">
                                                   <p style="padding-left: 10px;padding-right: 10px;">Turki dan Qatar tetap mendunkung Hamas.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Qatar mendukung pemerintahan Muslim Brotherhood Mesir, dan membangun sebuah jaringan  Al Jazeera Egypt-centric yang menjadi terkenal karena garis pro Muslim Brotherhood yang sangat kuat.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Qatar juga mendanai banyak tokoh Muslim Brotherhood di pengasingan, termasuk pemimpin politik Hamas Khaled Mashaal, yang diyakini telah mengatur berbagai serangan teroris Hamas.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Ini merupakan kasus dari negara dengan uang yang banyak membuat perhitungan tertentu di tahun 2011 yang membuat sangat masuk akan pada waktu itu bahwa Muslim Brotherhood adalah sesuatu yang besar yang akan mendominasi negara-negara di wilayah itu berikutnya.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Turki mempunyai lebih dari simpatis idologis dengan Muslim Brotherhood.</p>

                                                   <!-- p style="padding-left: 10px;padding-right: 10px;">Turki dan Qatar tetap mendukung Hamas.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Qatar yang didukung pemerintah Ikhwanul Muslimin Mesir, dan membangun "jaringan Mesir-centric Al Jazeera yang menjadi terkenal karena lini sangat pro-Ikhwanul Muslimin," kata Trager.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Qatar juga dana banyak tokoh Ikhwanul Muslimin di pengasingan, termasuk pemimpin politik Hamas Khaled Mashaal, yang diyakini telah mengatur berbagai serangan teroris Hamas.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Saya rasa ini adalah kasus negara dengan banyak uang untuk membakar membuat perhitungan tertentu pada tahun 2011 yang membuat banyak akal pada saat itu: bahwa Ikhwanul adalah hal besar berikutnya yang akan mendominasi banyak negara daerah, "kata Trager. "Realistis, masuk akal untuk bertaruh."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Turki memiliki "lebih dari simpati ideologis dengan Ikhwanul," katanya.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pekan lalu, Perdana Menteri Turki Recep Tayyip Erdogan berbicara dengan CNN, menuduh Israel "genosida."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Erdogan telah mencoba menggunakan penyebab Ikhwan untuk meningkatkan kepercayaan Islam sendiri di rumah," kata Trager. Tahun lalu, Erdogan menindak demonstrasi massal di negaranya.</p  -->
                                               </div>
                                           </div>
                                       </div>

                                       <div class="panel panel-default box-content" style="margin-bottom: 10px;">
                                           <div class="panel-heading title-pemain"">
                                               <a data-toggle="collapse" data-parent="#accordion" href="#saudi" style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">SAUDI ARABIA, UAE, YORDANIA &nbsp;&nbsp;<span  class="judul-coll" style="font-size: 10px;font-weight: normal;">Klik untuk selengkapnya</span></a>
                                               <span class="text-right" style="float:right;">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/Saudi-Arabia-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/United-Arab-Emirates-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/Jordan-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;">
                                               </span>
                                           </div>
                                           <div id="saudi" class="panel-collapse collapse in">
                                               <div class="panel-body" style="margin-top:5px;">
                                                   <p style="padding-left: 10px;padding-right: 10px;">Kerajaan Saudi Arabia, United Arab Emirates dan Jordania telah meminta Hamas untuk menerima usulan gencatan senjata apa adanya.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Kami mengutuk agresi Israel dan mendukung usulan gencatan senjata Mesir, kata Raja Yordania King Abdullah.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Negara-negara seperti Saudi Arabia dan UAE ditantang oleh Islamis yang berkuasa melalui suara rakyat bukan melalui kesukseskan kerajaan.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Saudi Arabia adalah pemimpinnya, sebagian mendukung kudeta dan membiayai laporan media pemerintah yang menyerang Muslim Brotherhood.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Mesir, Yordania, Saudi Arabia dan UAE semuanya melihat kehancuran Hamas sebagai keuntungan bagi keamanan dalam negeri mereka masing-masing serta stabilitas regional.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Arab Saudi dan Mesir sekarang lebih takut kepada Fundamentalis Islam daripada kepada Israel.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Kerajaan Arab Saudi lebih menghawatirkan prospek dan kemenangan Hamas, yang mana dapat menjadi keberanian Islamis lainnya di bagian lain Timur Tengah. Dan karena itu berpotensi menjadi oposisi Islamist di Arab Saudi.</p>

                                                   <!-- p style="padding-left: 10px;padding-right: 10px;">Para monarki Arab Saudi, Uni Emirat Arab dan Yordania telah menyerukan Hamas untuk menerima usulan gencatan senjata seperti.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Kami mengutuk agresi Israel dan kami mendukung usulan gencatan senjata Mesir," kata Raja Yordania Abdullah pekan lalu.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Negara-negara seperti Arab Saudi dan UEA adalah "ditantang oleh Islamis yang berkuasa melalui kotak suara bukan melalui suksesi kerajaan," kata Trager.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Jadi negara-negara tersebut telah secara langsung mendukung kudeta di Mesir karena dihapus terpilih Islamis dan karena mendiskreditkan model itu."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Arab Saudi "memimpin muatan," sebagian melalui dukungan laporan kudeta dan pembiayaan media pemerintah yang menyerang persaudaraan, kata Younes.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Mesir, Yordania, Arab Saudi dan UEA semua melihat kehancuran Hamas sebagai bermanfaat bagi keamanan dalam negeri mereka serta stabilitas regional."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Saudi dan Mesir sekarang lebih takut fundamentalisme Islam daripada mereka dari Israel," kata Zakaria.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Monarki Saudi lebih khawatir tentang prospek kemenangan Hamas, yang akan memberanikan Islamis di bagian lain di Timur Tengah, dan karena itu berpotensi menjadi oposisi Islamis di Arab Saudi." Tapi Hamas tidak sendirian.</p -->
                                               </div>
                                           </div>
                                       </div>

                                       <div class="panel panel-default box-content" style="margin-bottom: 10px;">
                                           <div class="panel-heading title-pemain"">
                                               <a data-toggle="collapse" data-parent="#accordion" href="#mesir" style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">MESIR &nbsp;&nbsp;<span  class="judul-coll" style="font-size: 10px;font-weight: normal;">Klik untuk selengkapnya</span></a>
                                               <span class="text-right" style="float:right;"><img src="<?php echo base_url().'assets/images/hotpages/gaza/Egypt-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;"></span>
                                           </div>
                                           <div id="mesir" class="panel-collapse collapse">
                                               <div class="panel-body" style="margin-top:5px;">
                                                   <p style="padding-left: 10px;padding-right: 10px;">Presiden baru Mesir berjanji selama kampanye bahwa ia akan menghabisi Muslim Bortherhood. Abdel Fattah el-Sisi, mantan kepala militer, digulingkan pemimpin yang dipilih secara bebas pertama Mesir, Presiden Mohamed Morsy dari Muslim Bortherhood, tahun lalu menyusul protes massal menentang kekuasaan Morsy ini. El-Sisi terpilih secara resmi pada bulan Juni.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Di Mesir Anda memiliki sebuah rezim yang berkuasa dengan menggulingkan pemerintahan Muslim Bortherhood," kata Trager. "Ini karena itu dalam konflik eksistensial dengan Ikhwanul. Jadi tidak ingin melihat Hamas, Muslim Bortherhood Palestina, muncul lebih kuat di wilayah tetangga."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Mesir juga memiliki alasan lain untuk melawan Hamas: meningkatnya kekerasan dan ketidakstabilan di Sinai, bagian utara Mesir yang berbatasan dengan Israel dan Gaza. Jaringan Hamas terowongan mencakup beberapa masuk dan keluar dari Mesir yang digunakan untuk menyelundupkan barang-barang termasuk senjata untuk attackson warga sipil Israel.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pemerintah baru Mesir telah "menindak secara agresif karena dihapus persaudaraan dari kekuasaan," kata Trager.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">El-Sisi menutup penyeberangan perbatasan antara Mesir dan Gaza, yang telah membantu blok militan Hamas dari melarikan diri atau penyelundupan di lebih banyak senjata selama serangan Israel. Tetapi juga telah memberikan kontribusi terhadap krisis kemanusiaan orang yang terjebak di Gaza.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Mesir mengusulkan gencatan senjata, dan Israel dengan cepat menerimanya - yang menunjukkan bahwa itu berisi istilah Israel cari, analis mengatakan. Hamas menolaknya. Sementara Mesir telah bekerja mati-matian untuk mencoba untuk menengahi gencatan senjata di masa lalu, Kairo saat ini menunjukkan sedikit terburu-buru untuk mengubah proposal untuk satu jauh lebih menguntungkan untuk Hamas, kata para analis.</p>

                                                   <!-- p style="padding-left: 10px;padding-right: 10px;">Presiden baru Mesir berjanji selama kampanye bahwa ia akan menghabisi Muslim Bortherhood. Abdel Fattah el-Sisi, mantan kepala militer, digulingkan pemimpin yang dipilih secara bebas pertama Mesir, Presiden Mohamed Morsy dari Muslim Bortherhood, tahun lalu menyusul protes massal menentang kekuasaan Morsy ini. El-Sisi terpilih secara resmi pada bulan Juni.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">"Di Mesir Anda memiliki sebuah rezim yang berkuasa dengan menggulingkan pemerintahan Muslim Bortherhood," kata Trager. "Ini karena itu dalam konflik eksistensial dengan Ikhwanul. Jadi tidak ingin melihat Hamas, Muslim Bortherhood Palestina, muncul lebih kuat di wilayah tetangga."</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Mesir juga memiliki alasan lain untuk melawan Hamas: meningkatnya kekerasan dan ketidakstabilan di Sinai, bagian utara Mesir yang berbatasan dengan Israel dan Gaza. Jaringan Hamas terowongan mencakup beberapa masuk dan keluar dari Mesir yang digunakan untuk menyelundupkan barang-barang termasuk senjata untuk attackson warga sipil Israel.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Pemerintah baru Mesir telah "menindak secara agresif karena dihapus persaudaraan dari kekuasaan," kata Trager.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">El-Sisi menutup penyeberangan perbatasan antara Mesir dan Gaza, yang telah membantu blok militan Hamas dari melarikan diri atau penyelundupan di lebih banyak senjata selama serangan Israel. Tetapi juga telah memberikan kontribusi terhadap krisis kemanusiaan orang yang terjebak di Gaza.</p>
                                                   <p style="padding-left: 10px;padding-right: 10px;">Mesir mengusulkan gencatan senjata, dan Israel dengan cepat menerimanya - yang menunjukkan bahwa itu berisi istilah Israel cari, analis mengatakan. Hamas menolaknya. Sementara Mesir telah bekerja mati-matian untuk mencoba untuk menengahi gencatan senjata di masa lalu, Kairo saat ini menunjukkan sedikit terburu-buru untuk mengubah proposal untuk satu jauh lebih menguntungkan untuk Hamas, kata para analis.</p -->
                                               </div>
                                           </div>
                                       </div>

                                      <div class="panel panel-default box-content" style="margin-bottom: 10px;">
                                            <div class="panel-heading title-pemain"">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#amerika" style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">AMERIKA SERIKAT &nbsp;&nbsp;<span  class="judul-coll" style="font-size: 10px;font-weight: normal;">Klik untuk selengkapnya</span></a>
                                            <span class="text-right" style="float:right;"><img src="<?php echo base_url().'assets/images/hotpages/gaza/United-States-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;"></span>
                                        </div>
                                        <div id="amerika" class="panel-collapse collapse">
                                            <div class="panel-body" style="margin-top:5px;">
                                                <p style="padding-left: 10px;padding-right: 10px;">Pertempuran di Gaza yang diwarnai dengan tewasnya rakyat Palestina serta ribuan roket Hamas yang ditembakkan ke penduduk Israel telah menimbulkan teka-teki diplomatik bagi Washington. Israel adalah sekutu utama Amerika di wilayah itu. Tetapi Presiden Barack Obama dan Perdana Menteri Benjamin Netanyahu memiliki perbedaan pendapat. Alih-alih mengambil sikap tegas di sisi Israel, Amerika Serikat malah terlibat dalam diplomasi dengan Qatar yang mendukung musuh Israel paling mematikan serta Turki yang pemimpinnya telah difitnah dan dicoreng oleh Israel.</p>
                                                <p style="padding-left: 10px;padding-right: 10px;">Rincian proses diplomatik, dimana secara luar biasa tidak mengikut sertakan pihak moderat Palestina telah menyebabkan ketakutan di pihak Israel, kemarahan di pihak Palestina, dan penghinaan bagi Mesir. Ketika gencatan senjata dilakukan maka Amerika Serikat harus mengkaji bagaimana menyeimbangkan sekutu dengan lawannya di masa krisis, karena bahkan setelah tembahan berhenti, konflik ini masih jauh dari selesai.</p>

                                                <!-- p style="padding-left: 10px;padding-right: 10px;">Pertempuran di Gaza, dengan gambar-gambar dramatis dari Palestina kematian warga sipil dan ribuan roket ditembakkan oleh Hamas terhadap warga sipil Israel, telah menyebabkan teka-teki diplomatik untuk Washington. Israel adalah sekutu utama Amerika di wilayah tersebut. Namun Presiden Barack Obama dan Perdana Menteri Benjamin Netanyahu memiliki perbedaan pendapat. Alih-alih mengambil sikap tegas di sisi Israel, Amerika Serikat telah terlibat dalam diplomasi dengan Qatar, yang mendukung musuh-musuh paling mematikan Israel, dan Turki, yang para pemimpinnya telah memfitnah dan diolesi Israel.</p>
                                                <p style="padding-left: 10px;padding-right: 10px;">Rincian proses diplomasi, yang biasa dikeluarkan moderat Palestina, telah menyebabkan kekhawatiran di Israel, kemarahan kalangan Palestina dan penghinaan di Mesir. Ketika gencatan senjata datang, Amerika Serikat harus meninjau bagaimana menyeimbangkan sekutu dan antagonis mereka di saat krisis, karena bahkan setelah penembakan berhenti, konflik ini masih jauh dari selesai.</p  -->
                                            </div>
                                        </div>
                                      </div>

                                      <div class="panel panel-default box-content" style="margin-bottom: 10px;">
                                            <div class="panel-heading title-pemain"">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#eropa" style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">EROPA &nbsp;&nbsp;<span  class="judul-coll" style="font-size: 10px;font-weight: normal;">Klik untuk selengkapnya</span></a>
                                            <span class="text-right" style="float:right;"><img src="<?php echo base_url().'assets/images/hotpages/gaza/European-Union-Flag-icon.png'; ?>" style="width: 32px;height: auto;margin-top:-7px;margin-right:10px;"></span>
                                        </div>
                                        <div id="eropa" class="panel-collapse collapse">
                                            <div class="panel-body" style="margin-top:5px;">
                                                <p style="padding-left: 10px;padding-right: 10px;">Penderitaan yang dirasakan oleh rakyat Palestina di Gaza merupakan salah satu aspek yang memilukan dari konflik ini dan tidak mengherankan bahwa hal tersebut memicu adanya protes terutama di kota-kota yang memiliki populasi muslim yang besar. Namun apa yang terjadi di beberapa kota di negara Eropa seperti Perancis, Jerman, Belgia, Belanda, dan lainnya adalah sesuatu yang jauh melampaui bentuk simpati kepada korban perang serta penolakan terhadap taktik Israel.</p>
                                                <p style="padding-left: 10px;padding-right: 10px;">Eropa telah melihat letusan perang yang paling buruk dari anti-Semitisme serta dilakukan secara terang-terangan sejak 1940-an. Kritikus Israel seringkali mengklaim bahwa Israel tidak adil dengan menyembunyikan anti-Semitisme namun teriakan akan “kematian orang-orang Yahudi”, “menggorok leher orang Yahudi”, atau “Orang Yahudi dikirim ke kamar gas” bersamaan dengan menghancurkan serta membakar toko milik orang Yahudi serta serangan ke sinagoga telah membuka kembali sentimen anti Yahudi yang masih dirasakan dan tetap tidak dapat terucapkan. Kita akan mengetahui seberapa serius pemimpin Eropa membahas mengenai masalah ini karena saat ini kita sudah menemukan apa yang tersembunyi di balik tirai wacana sipil.</p>
                                            </div>
                                        </div>
                                      </div>

                                   </div>


                               </td>

                               <td style="width: 5px;"></td>

                               <td style="width: 310px;padding-left: 10px;">
                                   <div class="box-content" style="width: 290px;height:1000px;">
                                       <div style="margin-top: -10px;padding-left:0px;background-color: #E5E5E5;">
                                           <div id="newsgaza_container" data-tipe="1" data-page='1' class="home-issue-container komunitas-scoring" style="height: 960px;margin-bottom: 9px !important;"></div>
                                           <div class="row-fluid" style="margin-bottom: 2px;">
                                               <div class="span6 text-left">
                                                   <a id="newsgaza_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a>
                                               </div>
                                               <div class="span6 text-right">
                                                   <!-- a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a -->
                                               </div>
                                           </div>

                                       </div>
                                   </div>
                               </td>
                           </table>

                        </td>


                    </tr>

                    <tr>
                        <td colspan="5">
                           <span id="kuatmiliter"></span>
                           <table style="background: none;">
                             <tr><td colspan="3" style="padding-top: 10px;"><div class="tagline"><span class="title">KEKUATAN MILITER</span></div></td></tr>
                             <tr>
                                 <td style="width: 435px;padding-left: 10px;">
                                     <div class="box-content" style="width: 435px;height:700px;">
                                         <div style="width: 435px;border-radius: 5px 5px 0 0;background-color: #BAFCD9">
                                             <p style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">PALESTINA</p>
                                         </div>
                                         <div style="height: 2px;background-color: #ffffff;margin-top: -10px;"></div>
                                         <div style="color: #000000;">
                                           <p style="font-size: 16px;padding-left:10px;margin-top:10px;">ARSENAL YANG DI PAKAI:</p>
                                             <p style="font-size: 12px;padding-left:10px;line-height:17px;">Soviet-era technologies, homegrown dengan komponen dari Syria dan Iran di smuggle melalui tunnels Roket di tembakan dari Gaza:
                                             <div style="float:left;width:200px;height: 425px;">
                                             <ul style="list-style-type: square;">
                                                 <li style="font-size: 12px;">Qassam roket; jarak 17 km; bisa mencapai kota Ashkelon, Sderot di Israel</li>
                                                 <li style="font-size: 12px;">Grad roket; jarak 20 km; bisa mencapai Ashkelon, Sderot</li>
                                                 <li style="font-size: 12px;">WS-1E; jarak 40km; bisa mencapai kota Beersheba</li>
                                                 <li style="font-size: 12px;">Fajr-5/M-75; jarak 75 km; bisa mencapai kota Dimona, Jerusalem, Tel Aviv</li>
                                                 <li style="font-size: 12px;">Khaibar-1; jarak 160 km; bisa mencapai kota Hadera, Zikron Yacoov, Haifa di utara dan Jordania barat</li>
                                                 <li style="font-size: 12px;">Antiaircraft Missiles and Rockets</li>
                                                 <li style="font-size: 12px;">Sagger missiles that are more accurate and sophisticated than standard or upgraded RPGs</li>
                                                 <li style="font-size: 12px;">Kalashnikov Guns & Stones</li>
                                                 <li style="font-size: 12px;">Over 100s Tunnels</li>
                                             </ul>
                                             </p>
                                           </div>
                                           <div style="float:left:width:170px;;height: 425px;">
                                               <img src="<?php echo base_url().'assets/images/hotpages/gaza/hamas_rockets_scope.gif'; ?>" alt="arsenal palestina hamas" style="margin-left:5px;width: 225px;height: auto;">
                                           </div>
                                           <div style="float:none;">
                                             <p style="font-size: 16px;padding-left:10px;margin-top:20px;">PERBANDINGAN ARSENAL</p>
                                             <p style="font-size: 12px;padding-left:10px;line-height:17px;">Antiaircraft Missiles and Rockets; Sagger missiles that are more accurate and sophisticated than standard or upgraded RPGs; Kalashnikov Guns & Stones; Over 100s Tunnels.</p>
                                           </div>
                                         </div>
                                     </div>
                                 </td>

                                 <td style="width: 25px;"></td>

                                 <td style="width: 435px;">
                                     <div class="box-content" style="width: 435px;height:700px;">
                                         <div style="width: 435px;border-radius: 5px 5px 0 0;background-color: #A4C0FC;" class="text-right">
                                             <p style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">ISRAEL</p>
                                         </div>
                                         <div style="height: 2px;background-color: #ffffff;margin-top: -10px;"></div>
                                         <div style="color: #000000;">
                                             <p style="font-size: 16px;padding-left:10px;margin-top:10px;">ARSENAL YANG DI PAKAI:</p>
                                             <ul style="list-style-type: square;/*direction: rtl;margin-right: 25px; */margin-top:10px;">
                                                 <li style="font-size: 12px;">Iron Dome sebegai defense</li>
                                                 <li style="font-size: 12px;">Air strikes dan ground offensives</li>
                                             </ul>
                                             <img src="<?php echo base_url().'assets/images/hotpages/gaza/iron_dome.gif'; ?>" alt="iron dome" style="margin-left:50px;width: 335px;height: auto;">
                                             <p style="font-size: 16px;padding-left:10px;margin-top:20px;">PERBANDINGAN ARSENAL</p>
                                             <p style="font-size: 12px;padding-left:10px;padding-right:10px;line-height:17px;">Small arms; Dror light machine gun; IMI Negev light machine gun; Uzi submachine gun; Uzi pistol; Desert Eagle pistol; Jericho 941 pistol; BUL M-5 pistol; BUL Storm pistol; SP-21 Barak pistol; IMI Galil assault rifle; IMI Tavor assault rifle; M89SR sniper rifle; Hezi SM-1 semi-automatic PDW; Anti-tank rockets and missiles; MATADOR Shoulder-launched missile weapon; B-300 Shoulder-launched missile weapon; Shoulder-Launched Multipurpose Assault Weapon; Shipon Shoulder-launched missile weapon; FGM-172 SRAW; MAPATS ATGM; Spike ATGM; LAHAT ATGM; Nimrod ATGM; Other missiles; Guided Advanced Tactical Rocket - Laser; Shafrir air-to-air missile; Derby air-to-air missile; Python air-to-air missile; Gabriel naval anti-ship missile; ADM-141 TALD; Popeye AGM-142 air-to-surface missile; Delilah cruise missile / anti-radiation missile; LORA theater ballistic missile; Jericho medium-range ballistic missile; Aircraft; IAI Arava cargo aircraft; IAI Sea Scan surveillance aircraft; IAI Nesher fighter aircraft; IAI Kfir fighter aircraft; IAI Namer fighter aircraft; IAI Lavi fighter aircraft; ATG Javelin jet trainer aircraft; Watercraft; Shaldag class fast patrol boat; Dabur class patrol boat; Dvora class fast patrol boat; Super Dvora Mk II class fast patrol boat;</p>
                                             <p style="font-size: 12px;padding-left:10px;padding-right:10px;line-height:17px;display: none;">Super Dvora Mk III class fast patrol boat; Sa'ar 3-class missile boat; Sa'ar 4-class missile boat; Sa'ar 4.5-class missile boat; Sa'ar 5-class corvette; Gal class submarine; Dolphin class submarine; Spaceflight; Shavit spaceflight launch vehicle; EROS earth observation satellite; Ofeq reconnaissance satellite; TecSAR reconnaissance satellite; Weapon stations; CornerShot SWAT weapon; Rafael Overhead Weapon Station; Samson Remote Controlled Weapon Station; Typhoon Weapon System; Active protection systems; Trophy active protection system; Iron Fist active protection system; Flight Guard airborne IR countermeasures system; Radars; EL/M-2032 fire-control radar; EL/M-2052 AESA radar; EL/M-2075 Phalcon AEW&C radar; EL/M-2080 Green Pine target tracking radar; EL/M-2083 AEW&C radar; Optronics; ITL MARS reflex sight; LITENING targeting pod; Spice EO-GPS PGM guidance kit; Skystar 300 ISR system; Tanks; Isherman tank; Sho't tank; Magach tank; Sabra tank; Merkava tank; Fighting vehicles; M113 variants; Nimda Shoet APC; Trail Blazer ARV; IDF Nagmachon APC; IDF Nakpadon CEV; IDF Puma CEV; IDF Achzarit APC; IDF Namer IFV; Nemmera ARV; AIL Abir; AIL Storm; Plasan Sand Cat; Wolf Armoured Vehicle; Golan Armored Vehicle; Artillery; Davidka mortar; Soltam M-66 mortar; Soltam M-68 howitzer; Soltam M-71 howitzer; Soltam M-120 mortar; L-33/39 Ro'em self-propelled howitzer; Makmat self-propelled mortar; MAR-240/290 rocket artillery launcher; LAR-160 rocket artillery launcher; LAROM rocket artillery launcher; Cardom mortar; Rascal self-propelled howitzer; ATMOS 2000 self-propelled howitzer; Sholef self-propelled howitzer; Unmanned aerial vehicles; Tadiran Mastiff UAV; Casper 250 UAV; Silver Arrow Micro-V UAV; Silver Arrow Sniper UAV; IAI Scout UAV; IAI Searcher UAV; IAI Harpy UAV; IAI Harop UAV; IAI Bird-Eye UAV; IAI I-View UAV; IAI Ranger UAV; IAI Heron UAV; IAI Eitan UAV; IAI Panther UAV; IAI RQ-2 Pioneer UAV; IAI RQ-5 Hunter UAV; Elbit Skylark UAV; Elbit Hermes 90 UAV; Elbit Hermes 450 UAV; Elbit Hermes 900 UAV; Aeronautics Dominator UAV; Aeronautics Orbiter UAV; Urban Aeronautics X-Hawk UAV; Unmanned surface vehicles; VIPeR UGCV; Protector USV; Guardium UGV; Raam HaShachar unmanned Caterpillar D9 armored bulldozer; Air-defense systems; Machbet self-propelled anti-aircraft weapon; Barak 1 naval surface-to-air missile; Barak 8 naval surface-to-air missile; SPYDER air-defense system; Arrow anti-ballistic missile; Tactical High Energy Laser; Iron Dome short-range rocket defense system; David's Sling medium-range rocket defense system; Miscellaneous; Mitznefet helmet camouflage; Tomcar all-terrain vehicle; MG251/253 smoothbore tank gun; Kilshon anti-radiation missile launcher; IDF Caterpillar D9 armored bulldozer; Skunk riot control agent; Scream riot control agent; SIMON breach grenade; Enhanced Tactical Computer;</p><br/>
                                         </div>
                                     </div>
                                 </td>
                             </tr>
                           </table>
                        </td>
                    </tr>

                    <tr><td colspan="5" class="batas-row1"></td></tr>
                    <tr><td colspan="5"><div class="tagline"><span class="title">PENDUKUNG DAN REAKSI</span></div></td></tr>
                    <?php
                      $tokohnopro = array(
                          0 => ( array ( 'ref_name' => 'Barack Obama', 'ref_title' => 'President USA', 'ref_word' => 'Israel has rigth to defend', 'ref_url' => '',
                                  'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_barack_obama.jpg')),
                          1 => ( array ( 'ref_name' => 'Benjamin Netanyahu', 'ref_title' => 'Prime Minister Israel', 'ref_word' => '"Israel will do whatever it must do to protect its people"', 'ref_url' => '',
                                  'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_benjamin_netanyahu.jpg')),
                          2 => ( array ( 'ref_name' => 'David Cameron', 'ref_title' => 'Prime Minister UK', 'ref_word' => 'Israel has rigth to defend', 'ref_url' => '',
                                  'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_david_cameron.jpg')),
                          3 => ( array ( 'ref_name' => 'Military leaders', 'ref_title' => 'Israel', 'ref_word' => '"trying to minimise the civilian impact".', 'ref_url' => '',
                                  'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_military_israel.jpg')),
                          4 => ( array ( 'ref_name' => '', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
                                  'ref_img' => '')),
                          5 => ( array ( 'ref_name' => 'John Kerry', 'ref_title' => 'Secretary of State USA', 'ref_word' => 'he US fully supported Israel\'s right to defend itself against militant rocket attacks, but also supported Palestinians\' desire for more freedoms and a better standard of living.', 'ref_url' => '',
                                  'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_john_kerry.jpg')),
                      );

                    $tokohpro = array (
                        0 => ( array ( 'ref_name' => 'Ban Ki-moon', 'ref_title' => 'UN Secretary General UN', 'ref_word' => '"moral outrage and a criminal act" pada serangan di UN School', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_ban_ki_moon.jpg')),
                        1 => ( array ( 'ref_name' => 'Francois Hollande', 'ref_title' => 'French President', 'ref_word' => 'an end to the "massacres in Gaza"', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_francois_hollande.jpg')),
                        2 => ( array ( 'ref_name' => 'Recep Tayyip Erdogan', 'ref_title' => 'Prime Minister Turkey', 'ref_word' => 'He accused Israel of "genocide… reminiscent of the Holocaust" during the Gaza crisis', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_recep_tayyip.jpg')),
                        3 => ( array ( 'ref_name' => 'Desmond Tutu', 'ref_title' => 'Nobel Peace Prize laureate and retired Anglican Archbishop', 'ref_word' => 'compared the Israeli government to South Africa’s former apartheid regime.', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_desmond_tutu.jpg')),
                        4 => ( array ( 'ref_name' => 'Moussa Abu Marzouk', 'ref_title' => 'Palestinian delegation at the Cairo talks', 'ref_word' => 'There is no agreement to extend the ceasefire." (Twitter)', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_moussa_abu.jpg')),
                        5 => ( array ( 'ref_name' => 'Hanan Ashrawi', 'ref_title' => 'Palestinian legislator', 'ref_word' => 'a long-term truce would be hard to achieve', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_hanan_ashrawi.jpg')),
                        6 => ( array ( 'ref_name' => 'Fidel Castro', 'ref_title' => 'Mantan Presiden Kuba', 'ref_word' => '"I think that a new and disgusting form of fascism is emerging with considerable force at this moment in human history," Castro wrote in a column in the newspaper Granma titled "Palestinian Holocaust in Gaza.”', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_fidel_castro.jpg')),
                        7 => ( array ( 'ref_name' => 'Pierre Krahenbuhl', 'ref_title' => 'UNRWA Commissioner-General', 'ref_word' => 'civilians "were paying the highest price of the current military escalation. I condemn this callous shelling and the extensive loss of life"', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_pierre_krahenbuhl.jpg')),
                        8 => ( array ( 'ref_name' => 'Laurent Fabius', 'ref_title' => 'Foreign Minister France', 'ref_word' => 'Israel\'s right to security did not justify the "killing of children and slaughter of civilians"', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_laurent_Fabius.jpg')),
                        9 => ( array ( 'ref_name' => 'White House', 'ref_title' => 'US Govt', 'ref_word' => 'Attack on UN School is  "totally unacceptable and totally indefensible"', 'ref_url' => '',
                                'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_whitehouse.jpg')),
                    );



                    $tokoh_in = array(
                        0 => ( array ( 'ref_name' => 'Susilo Bambang Yudoyono', 'ref_title' => 'Presiden', 'ref_word' => '"Akan jadi dosa sejarah kalau manusia se-dunia, tidak punya kepedulian dan tanggung jawab moral atas tragedi kemanusiaan yang tengah terjadi di jalur Gaza,"',
                                'ref_url' => 'http://www.bijaks.net/aktor/profile/susilobambangyudhoyono50ee672eb35c5', 'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/susilobambangyudhoyono50ee672eb35c5/thumb/39989d0890a536ffa7c758d668a09dbbb0b48a69.jpg')),
                        1 => ( array ( 'ref_name' => 'Jokowi', 'ref_title' => 'Capres RI 2014', 'ref_word' => 'Serangan Israel ke Palestina adalah pelanggaran terhadap hak asasi manusia, terjadi di Jalur Gaza. Ini memprihatinkan terjadi pembunuhan massal',
                                'ref_url' => 'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19', 'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/thumb/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg')),
                        2 => ( array ( 'ref_name' => 'Prabowo', 'ref_title' => 'Capres RI 2014', 'ref_word' => 'Masyarakat Indonesia harus membantu Palestina dalam bentuk bantuan medis ataupun dana',
                                'ref_url' => 'http://www.bijaks.net/aktor/profile/prabowosubiyantodjojohadikusumo50c1598f86d91', 'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/prabowosubiyantodjojohadikusumo50c1598f86d91/thumb/059fd2fa2981621a0708c6a44243a003060151e6.jpg')),
                        3 => ( array ( 'ref_name' => 'Hidayat Nur Wahid', 'ref_title' => 'Politisi PKS', 'ref_word' => 'Kita bersatu padu dan kita berharap bersatu padunya kita di sini menjadi inspirasi umat Islam di dunia',
                                'ref_url' => 'http://www.bijaks.net/aktor/profile/drhmhidayatnurwahidma50f7ae3aba4f8', 'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/drhmhidayatnurwahidma50f7ae3aba4f8/thumb/06f6dee45a70caf41e5d582bd270960da2246df6.jpg')),
                        4 => ( array ( 'ref_name' => 'Din Syamsuddin', 'ref_title' => 'Ketua Prakarsa Persahabatan Indonesia-Palestina', 'ref_word' => 'Ada penjajahan, aksi kebiadaban, terhadap manusia lain. Kita harus bisa bergerak minimal membantu untuk krisis di Gaza ini',
                                'ref_url' => 'http://www.bijaks.net/aktor/profile/dinsyamsuddin51c7bbdfbc558', 'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/dinsyamsuddin51c7bbdfbc558/thumb/226933ef75dc5626b82e419af760cb701a6a06da.jpg')),
                        5 => ( array ( 'ref_name' => 'Marzuki Alie', 'ref_title' => 'Ketua DPR', 'ref_word' => 'JIni sudah melanggar HAM. Harusnya dunia bergerak melawan ini. Selama ini dunia tidak berdaya. Harus ada dukungan konkrit dari OKI dan PBB',
                                'ref_url' => 'http://www.bijaks.net/aktor/profile/drhmarzukialiesemm51be7bc78e14e', 'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/drhmarzukialiesemm51be7bc78e14e/thumb/eee884fcf838f28c396a3d47002cca44099cd27a.jpg')),
                        6 => ( array ( 'ref_name' => 'Heru Joko', 'ref_title' => 'Ketua Umum Klub suporter Persib Bandung', 'ref_word' => 'Kami mengutuk keras pembantaian yang dilakukan Israel terhadap saudara-saudara kami di sana',
                                'ref_url' => '', 'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_heru_joko.jpg')),
                        7 => ( array ( 'ref_name' => 'Harun Syaifullah', 'ref_title' => 'Kepala Seksi Politik dan Keamanan Timur Tengah Kementerian Luar Negeri Republik Indonesia', 'ref_word' => 'Harus dipahami juga bahwa konflik ini adalah persoalan kemanusiaan dan masalah penjajahan. Ini bukan masalah agama',
                                'ref_url' => '', 'ref_img' => 'http://www.bijaks.net/assets/images/no-image-politisi.png')),
                        8 => ( array ( 'ref_name' => 'Marwan Jafar', 'ref_title' => 'Ketua DPP Partai Kebangkitan Bangsa (PKB)', 'ref_word' => 'Jokowi-JK akan mampu membantu mewujudkan Palestina menjadi negara yang merdeka dan menjadi anggota tetap PBB. Tentunya akan dilakukan dengan membuka komunikasi dengan sejumlah negara di Timur Tengah, Eropa dan Amerik Serikat ',
                                'ref_url' => 'http://www.bijaks.net/aktor/profile/hmarwanjafarsesh50f8f52b84acd', 'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/hmarwanjafarsesh50f8f52b84acd/thumb/6d730a36bfee3fc3fefb2a472d111b3b8caffc1d.jpg')),
                        9 => ( array ( 'ref_name' => 'HE Fariz Mehdawi', 'ref_title' => 'Duta Besar Palestina untuk Indonesia', 'ref_word' => 'Kami hidup di situasi yang sulit. ini bisa saja menghancurkan semua kemungkinan digelarnya perundingan damai. Soal keinginan politik, untungnya, masih ada politik internasional, yang masih membujuk Israel',
                                'ref_url' => '', 'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_fariz_mehdawi.jpg')),
                        10 => ( array ( 'ref_name' => 'Slamet Effendy Yusuf', 'ref_title' => 'Ketua MUI', 'ref_word' => 'Kita seharusnya bisa mengharapkan Liga Arab unjuk gigi dalam masalah ini, tetapi melihat kondisi dan situasi negara-negara di kawasan tersebut, kita tidak bisa berharap banyak',
                                'ref_url' => '', 'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_Slamet-Effendy-Yusuf.jpg')),
                        11 => ( array ( 'ref_name' => 'Hajriyanto Y Tohari', 'ref_title' => 'Wakil Ketua MPR RI', 'ref_word' => 'PBB bukan hanya harus bisa menghentikan pembantaian tersebut, melainkan harus bisa membawa Israel ke Pengadilan HAM Internasional atau International Tribunal',
                                'ref_url' => '', 'ref_img' => base_url().'assets/images/hotpages/gaza/tokoh_hajriyanto.jpg')),
                        12 => ( array ( 'ref_name' => 'Triyono Wibowo', 'ref_title' => 'Wakil Tetap Indonesia untuk PBB di Jenewa', 'ref_word' => 'Indonesia mengecam keras serangan membabi buta Israel terhadap bangsa Palestina terutama di Jalur Gaza dan meminta agar hak asasi manusia bangsa Palestina dilindungi.',
                                'ref_url' => 'http://www.bijaks.net/aktor/profile/triyonowibowo50f519cfd7b28', 'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/triyonowibowo50f519cfd7b28/thumb/triyonowibowo50f519cfd7b28_20130115_085817.jpg')),
                        13 => ( array ( 'ref_name' => 'Muhammad Djazuli Ambari', 'ref_title' => 'Ketua Umum Bulan Sabit Merah Indonesia (BSMI) ', 'ref_word' => 'Serangan militer tak hanya dengan pesawat tempur, namun juga roket, helikopter dan lain-lain terhadap masyarakat sipil tersebut sangat tidak bisa ditolerir secara hukum, secara Hak Asasi Manusia, secara etika, apalagi secara budaya dan agama',
                                'ref_url' => '', 'ref_img' => 'http://www.bijaks.net/assets/images/no-image-politisi.png')),
                    );

                    ?>
                    <tr>
                        <td colspan="5" style="padding-left: 10px;">
                            <span id="pendukung_tokoh"></span>
                            <div class="box-content" style="width: 915px;height: 1510px;">
                                <div style="height: 40px;padding-top:10px;background-color: #E5E5E5"><span style="margin-left:10px;font-size:18px;">Tokoh Dunia</span></div>
                                <div style="height: 2px;background-color: #E5E5E5;margin-top: -10px;"></div>
                                <div style="color: #000000;margin-top:20px;margin-left:10px;height: 635px;">
                                  <div style="float:left;width: 420px;">
                                    <table style="background: none;margin-left:10px;">
                                    <?php
                                    foreach($tokohpro as $key=>$val)
                                    {
                                        ?>
                                        <?php if($key % 2 == 0){ ?>
                                        <tr class="" style="width: 200px;">
                                    <?php } ?>
                                        <td class="tokoh" style="">
                                            <div style="margin-top:10px;">
                                            <?php if(!empty($val['ref_url'])) { ?><a href="< ?php echo $val['ref_url'];?>"><?php } ?>
                                                <img src='<?php echo $val['ref_img'];?>'
                                                     data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='<?php echo $val['ref_name']; ?>'/>
                                                <div style=""></div>
                                                <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                            </div>
                                        </td>
                                        <td class="tokoh">
                                            <div style="margin-top:10px;">
                                            <?php if(!empty($val['ref_url'])) { ?><a href="<?php echo $val['ref_url'];?>"><?php } ?>
                                                <p class="name"><?php echo $val['ref_name']; ?></p>
                                                <p class="title"><?php echo $val['ref_title']; ?></p>
                                                <p class="word"><?php echo $val['ref_word']; ?></p>
                                                <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                            </div>
                                        </td>
                                        <?php if($key % 2 == 1 || $key == count($tokohpro) - 1){  ?>
                                        </tr>
                                    <?php } ?>
                                    <?php } ?>
                                    </table>
                                    </div>

                                    <div style="float:left;width: 420px; margin-left:50px;">
                                        <table style="background: none;margin-left:10px;">
                                            <?php
                                            foreach($tokohnopro as $key=>$val)
                                            {
                                                ?>
                                                <?php if($key % 2 == 0){ ?>
                                                <tr class="" style="width: 200px;">
                                            <?php } ?>
                                                <td class="tokoh">
                                                    <div style="margin-top:10px;">
                                                    <?php if(!empty($val['ref_url'])) { ?><a href="< ?php echo $val['ref_url'];?>"><?php } ?>
                                                        <img src='<?php echo $val['ref_img'];?>'
                                                             data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='<?php echo $val['ref_name']; ?>'/>
                                                        <div style=""></div>
                                                        <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                                    </div>
                                                </td>
                                                <td class="tokoh">
                                                    <div style="margin-top:10px;">
                                                    <?php if(!empty($val['ref_url'])) { ?><a href="<?php echo $val['ref_url'];?>"><?php } ?>
                                                        <p class="name"><?php echo $val['ref_name']; ?></p>
                                                        <p class="title"><?php echo $val['ref_title']; ?></p>
                                                        <p class="word"><?php echo $val['ref_word']; ?></p>
                                                        <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                                    </div>
                                                </td>
                                                <?php if($key % 2 == 1 || $key == count($tokohnopro) - 1){  ?>
                                                </tr>
                                            <?php } ?>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>

                                <div style="height: 40px;padding-top:10px;background-color: #E5E5E5"><span style="margin-left:10px;font-size:18px;">Tokoh Indonesia</span></div>
                                <div style="height: 2px;background-color: #E5E5E5;margin-top: -10px;"></div>
                                <div style="color: #000000;margin-top:20px;margin-left:10px;">
                                    <?php
                                    foreach($tokoh_in as $key=>$val)
                                    {
                                        ?>
                                        <div style="display: inline-block;padding-top:1px;padding-bottom: 1px;
                                               -webkit-column-break-inside: avoid;-moz-column-break-inside: avoid;column-break-inside: avoid;
                                                width: 200px;height:auto;margin-left: 10px;margin-right: 10px;margin-top:5px;vertical-align: top;">
                                            <div class="tokoh" style="float:left;">
                                                <?php if(!empty($val['ref_url'])) { ?><a href="< ?php echo $val['ref_url'];?>"><?php } ?>
                                                    <img src='<?php echo $val['ref_img'];?>'
                                                         data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='<?php echo $val['ref_name']; ?>'/>
                                                    <div style=""></div>
                                                    <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                            </div>
                                            <div class="tokoh" style="float:left;">
                                                <?php if(!empty($val['ref_url'])) { ?><a href="<?php echo $val['ref_url'];?>"><?php } ?>
                                                    <p class="name"><?php echo $val['ref_name']; ?></p>
                                                    <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                                    <p class="title"><?php echo $val['ref_title']; ?></p>
                                                    <p class="word"><?php echo $val['ref_word']; ?></p>
                                            </div>

                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr><td colspan="5" class="batas-row1"></td></tr>
                           <?php

                           $plt_seleb = array(
                               0 => ( array ( 'ref_name' => 'Rihanna', 'ref_title' => 'Singer, USA', 'ref_word' => '"#freePaletine" di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_rihanna.jpg')),
                               1 => ( array ( 'ref_name' => 'Dwight Howard', 'ref_title' => 'NBA Player, USA', 'ref_word' => 'Pro-palestina di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_dwight_howard.jpg')),
                               2 => ( array ( 'ref_name' => 'Selena Gomez', 'ref_title' => 'Singer, USA', 'ref_word' => '"It\'s About Humanity. Pray for Gaza." di Instagram', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_selena_gomez.jpg')),
                               3 => ( array ( 'ref_name' => 'Zayn Malik', 'ref_title' => 'Singer One Direction, UK', 'ref_word' => '"#FreePalestine" di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_zayn_malik.jpg')),
                               4 => ( array ( 'ref_name' => 'Joey Barton', 'ref_title' => 'Footballer, UK', 'ref_word' => 'An innocent child killed on average every hour in Gaza. How can this continue? #StopKillingChildrenInGaza  di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_joey_barton.jpg')),
                               5 => ( array ( 'ref_name' => 'Penelope Cruz', 'ref_title' => 'Actress, Spain', 'ref_word' => 'an open letter accusing Israel of "genocide".', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_penelope_cruz.jpg')),
                               6 => ( array ( 'ref_name' => 'Javier Bardem', 'ref_title' => 'Actor, Spain', 'ref_word' => 'an open letter accusing Israel of "genocide".', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_javier_bardem.jpg')),
                               7 => ( array ( 'ref_name' => 'Mia Farrow', 'ref_title' => 'Actress and Unicef ambassador, USA', 'ref_word' => '"I can find no moral justification for  bombing schools, hospitals, ambulances and defenseless civilians who cannot even flee to safety" di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_mia_farrow.jpg')),
                               8 => ( array ( 'ref_name' => 'John Cusak', 'ref_title' => 'Actor, USA', 'ref_word' => '"Israel\'s operation is not self-defence" di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_john_cusak.jpg')),
                               9 => ( array ( 'ref_name' => 'Mario Balotelli', 'ref_title' => 'Footballer, Italy', 'ref_word' => '"Children playing at the beach should never be a negative thing. #stopwar #gaza" di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_mario_balotelli.jpg')),
                               10 => ( array ( 'ref_name' => 'Russell Brand', 'ref_title' => 'English comedian and author, UK', 'ref_word' => '"One of the definitions of \'terrorist\' is using intimidation to reach your goals," on Sean Hannity di YouTube', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_russell_brand.jpg')),
                               11 => ( array ( 'ref_name' => 'John Legend', 'ref_title' => 'Singer, USA', 'ref_word' => '"So sick watching our Secretary of State have to grovel so hard to tell Israel how much he loves them while Israeli cabinet shits on him" di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_john_egend.jpg')),
                               12 => ( array ( 'ref_name' => 'Madonna', 'ref_title' => 'Singer, USA', 'ref_word' => 'posted on her Facebook page a picture of flowers that she compared to Palestinian children. "Who has the right to destroy them?" she wrote. "No one!" di Facebook', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_madoona.jpg')),
                           );

                           $zion_seleb = array(
                               0 => ( array ( 'ref_name' => 'Jon Voight', 'ref_title' => 'Actor, USA', 'ref_word' => 'Pro Israel message: "...Penelope Cruz and Javier Bardem could incite anti-Semitism all over the world and are oblivious to the damage they have caused." in open Letter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_jon_voight.jpg')),
                               1 => ( array ( 'ref_name' => 'Bar Refaeli', 'ref_title' => 'Supermodel, Israel', 'ref_word' => '"These days are so difficult to bare. Brave young men who have protected our country from terror. My… http://instagram.com/p/qyK..." in support of Israel di Tweeter dan Facebook', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_bar_refaeli.jpg')),
                               2 => ( array ( 'ref_name' => 'Howard Stern', 'ref_title' => 'Radio host, USA', 'ref_word' => '“If you are anti-Israel, you are anti-America." di radio', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_howard_stern.jpg')),
                               3 => ( array ( 'ref_name' => 'Joan Rivers', 'ref_title' => 'comedienne, USA', 'ref_word' => '“Let me just tell you, if New Jersey were firing rockets into New York, we would wipe them out," terhadap TMZ', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_joan_rivers.jpg')),
                               4 => ( array ( 'ref_name' => 'Simon Cowell', 'ref_title' => 'English-born music producer, UK', 'ref_word' => 'donated $150,000 to the Friends of the Israeli Defense Forces', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_simon_cowell.jpg')),
                               5 => ( array ( 'ref_name' => 'Scarlett Johansson', 'ref_title' => 'Actress, USA', 'ref_word' => 'she refused to resign as brand ambassador of SodaStream. The company produces carbonation devices at a factory in the West Bank, which is claimed by Palestinians.', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_scarlett_johansson.jpg')),
                               6 => ( array ( 'ref_name' => 'The Rolling Stones', 'ref_title' => 'Music Band, USA', 'ref_word' => 'Rejected Roger Waters and Nick Mason of Pink Floyd to cancel their first-ever concert in Tel Aviv di Salon.com', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_the_rolling_stones.jpg')),
                               7 => ( array ( 'ref_name' => 'Gal Gadot', 'ref_title' => 'Wonder Woman actress, Israel', 'ref_word' => '"I am sending my love and prayers to my fellow Israeli citizens. Especially to all the boys and girls who are risking their lives protecting my country against the horrific acts conducted by Hamas, who are hiding like cowards behind women and children." di Instagram', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_gal_gadot.jpg')),
                               8 => ( array ( 'ref_name' => 'Yossi Benayoun ', 'ref_title' => 'Footballer di EPL, Israel', 'ref_word' => '"@Joey7Barton mate things can never changed  you have been stupid and you will stay stupid all your life..embarrassing ." to Joey Barton di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_yossi_benayoun.jpg')),
                               9 => ( array ( 'ref_name' => 'Bill Maher', 'ref_title' => 'Comedian, USA', 'ref_word' => '"Dealing w/ Hamas is like dealing w/ a crazy woman who\'s trying to kill u - u can only hold her wrists so long before you have to slap her" di Tweeter', 'ref_url' => '',
                                       'ref_img' => base_url().'assets/images/hotpages/gaza/seleb_bill_maher.jpg')),
                           );

                           ?>

                    <tr>
                       <td colspan="5">
                         <span id="selebritis"></span>
                         <table style="background: none;">
                           <tr>

                               <td style="width: 450px;padding-left: 10px;padding-top:10px;">
                                   <div class="box-content" style="width: 450px;height:815px;min-height: 442px;">
                                       <div style="width: 450px;border-radius: 5px 5px 0 0;background-color: #BAFCD9">
                                           <p style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">PRO PALESTINA</p>
                                       </div>
                                       <div style="height: 2px;background-color: #ffffff;margin-top: -10px;"></div>
                                       <div style="color: #000000;">

                                           <table style="background: none;margin-left:10px;">
                                               <?php
                                               foreach($plt_seleb as $key=>$val)
                                               {
                                                   ?>
                                                   <?php if($key % 2 == 0){ ?>
                                                   <tr class="">
                                               <?php } ?>
                                                   <td class="tokoh">
                                                       <?php if(!empty($val['ref_url'])) { ?><a href="< ?php echo $val['ref_url'];?>"><?php } ?>
                                                           <img src='<?php echo $val['ref_img'];?>'
                                                                data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='<?php echo $val['ref_name']; ?>'/>
                                                           <div style=""></div>
                                                           <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                                   </td>
                                                   <td class="tokoh">
                                                       <?php if(!empty($val['ref_url'])) { ?><a href="<?php echo $val['ref_url'];?>"><?php } ?>
                                                           <p class="name"><?php echo $val['ref_name']; ?></p>
                                                           <p class="title"><?php echo $val['ref_title']; ?></p>
                                                           <p class="word"><?php echo $val['ref_word']; ?></p>
                                                           <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                                   </td>
                                                   <?php if($key % 2 == 1 || $key == count($plt_seleb) - 1){  ?>
                                                   </tr>
                                               <?php } ?>
                                               <?php } ?>
                                           </table>

                                       </div>
                                   </div>
                               </td>

                               <td style="width: 15px;"></td>

                               <td style="width: 410px;padding-top: 10px;">
                                   <div class="box-content" style="width: 410px;height:815px;min-height: 442px;">
                                       <div style="width: 410px;border-radius: 5px 5px 0 0;background-color: #A4C0FC;" class="text-right">
                                           <p style="font-size: 15px;color: #000000;font-weight: bold;padding: 10px;">PRO ISRAEL</p>
                                       </div>
                                       <div style="height: 2px;background-color: #ffffff;margin-top: -10px;"></div>
                                       <div style="color: #000000;">

                                           <table style="background: none;margin-left:10px;">
                                               <?php
                                               foreach($zion_seleb as $key=>$val)
                                               {
                                                   ?>
                                                   <?php if($key % 2 == 0){ ?>
                                                   <tr class="">
                                               <?php } ?>
                                                   <td class="tokoh">
                                                       <?php if(!empty($val['ref_url'])) { ?><a href="< ?php echo $val['ref_url'];?>"><?php } ?>
                                                           <img src='<?php echo $val['ref_img'];?>'
                                                                data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='<?php echo $val['ref_name']; ?>'/>
                                                           <div style=""></div>
                                                           <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                                   </td>
                                                   <td class="tokoh">
                                                       <?php if(!empty($val['ref_url'])) { ?><a href="<?php echo $val['ref_url'];?>"><?php } ?>
                                                           <p class="name"><?php echo $val['ref_name']; ?></p>
                                                           <p class="title"><?php echo $val['ref_title']; ?></p>
                                                           <p class="word"><?php echo $val['ref_word']; ?></p>
                                                           <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                                   </td>
                                                   <?php if($key % 2 == 1 || $key == count($zion_seleb) - 1){  ?>
                                                   </tr>
                                               <?php } ?>
                                               <?php } ?>
                                           </table>

                                       </div>
                                   </div>
                               </td>
                           </tr>

                           <tr>
                               <td colspan="5" style="padding: 10px;">
                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/banner-gaza-war.png';?>" style="width: 940px; height: auto;margin-top:10px;">
                               </td>
                           </tr>

                           <tr>
                               <td style="padding-top: 10px;">
                                   <span id="kepedulian_game"></span>
                                   <div class="tagline"><span class="title">KEPEDULIAN MEDIA DUNIA</span></div>
                               </td>
                               <td style="width: 5px;"></td>
                               <td style="padding-top: 10px;">
                                   <div class="tagline" style="margin-left:0px;"><span class="title">GAME KONFLIK YANG DILARANG</span></div>
                               </td>
                           </tr>
                           <tr>
                               <td style="width: 450px;padding-left: 10px;">
                                   <div class="box-content" style="width: 450px;height:2000px;">
                                       <div style="color: #000000;">
                                           <div style="text-align: center;border-bottom: solid 1px #E5E5E5;">
                                               <img style="width: 260px;height: auto;" src="<?php echo base_url().'assets/images/hotpages/gaza/Qatar-Chronicle-Media-Bias.jpg';?>" alt="">
                                           </div>
                                           <div  class="row-fluid media"  style="padding-top:5px;">
                                               <h4>Media di Palestina:</h4>
                                               <div class="media-logo">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_al_aqsa_logo.jpg';?>" alt=''/>
                                               </div>

                                               <div class="media-desc">
                                                    <ul style="list-style: square;">
                                                      <li>Fokus pada gambar yang sangat grafis & mengerikan</li>
                                                      <li>Fakus pada anak-anak tewas dan luka-luka</li>
                                                      <li>Fokus pada rumah sakit dan penderitaan korban</li>
                                                      <li>Tayangkan film-film yg memuliakan militan dan penembakan roket ke Israel</li>
                                                      <li>Liputan akan Israel berfokus pada kepanikan warga Israel akan roket dan peti mati tentara Israel</li>
                                                    </ul>
                                               </div>

                                           </div>

                                           <div  class="row-fluid media"  style="margin-top:5px;">
                                               <h4>TV di Israel:</h4>
                                               <div class="media-logo">
                                                   <div style="width: 55px;height: 55px;"></div>
                                               </div>

                                               <div class="media-desc">
                                                   <ul style="list-style: square;">
                                                       <li>Fokus pada rincian operasi militer</li>
                                                       <li>Tentara Israel di gambarkan sebagai pahlawan</li>
                                                       <li>Fokus pada nasib buruk warga Israel dalam jangkauan roket Hamas</li>
                                                       <li>Situasi kemanusiaan di Gaza sedikit di siarkan</li>
                                                   </ul>
                                               </div>

                                           </div>

                                           <div class="row-fluid media"  style="margin-top:5px;">
                                               <h4>Media di Negara Tetangga Konflik:</h4>

                                               <div class="row media"  style="margin-top:2px;border-bottom:none;">
                                                  <div class="media-logo">
                                                     <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_arab_aljazeera_logo.jpg';?>" alt=''/>
                                                  </div>
                                                  <div class="media-desc">
                                                     <p class="desc">Arab Al-Jazeera:
                                                       <ul style="list-style: square;margin-top:-10px;">
                                                         <li>Mendukung perjuangan Palestina dan Hamas seperti sebelumnya</li>
                                                         <li>Liputan bebas, mengerikan dan grafis, seperti korban dan mutilasi badan</li>
                                                       </ul>
                                                     </p>
                                                  </div>
                                               </div>
                                               <div class="row media"  style="margin-top:2px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_aljazeera_logo.jpe';?>" alt=''/>
                                                   </div>
                                                   <div class="media-desc">
                                                      <p class="desc">English Al-Jazeera:
                                                       <ul style="list-style: square;margin-top:-10px;">
                                                         <li>Jauh kurang grafis di bandingkan Arab Al-Jazeera</li>
                                                         <li>Tapi lebih grafis dibandingkan media barat, seperti BBC dan CNN</li>
                                                       </ul>
                                                     </p>
                                                   </div>
                                               </div>
                                               <div class="row media"  style="margin-top:2px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <div style="width: 55px;height: 10px;"></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p class="desc">Abu-Dhabi Sky News TV:<br/>Lebih fokus pada saat kejadian, tapi tidak terlalu grafis</p>
                                                   </div>
                                               </div>
                                               <div class="row media"  style="margin-top:2px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_al_arabiya_logo.jpg';?>" alt=''/>
                                                   </div>
                                                   <div class="media-desc">
                                                      <p class="desc">al-Arabiya media milik Arab-Saudi:
                                                         <ul style="list-style: square;margin-top:-10px;">
                                                            <li>Fokus pada situasi setelah serangan udara Israel</li>
                                                            <li>Lebih pada civilian, rumah sakit dan tempat berlindung</li>
                                                         </ul>
                                                      </p>
                                                   </div>
                                               </div>
                                               <div class="row media"  style="margin-top:2px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_al_alam_logo.jpg';?>" alt=''/>
                                                   </div>
                                                   <div class="media-desc">
                                                      <p class="desc">al-Alam media milik negara Iran dan media lainnya di Iran:
                                                         <ul style="list-style: square;margin-top:-10px;">
                                                            <li>Penyiaran gambar yang sangat grafis dan mengerikan</li>
                                                            <li>Fokus pada pujian akan militan Palestina dan peluncuran roket oleh Hamas</li>
                                                            <li>Pro-TV Bahasa Inggris milik negara Iran lebih berimbang</li>
                                                         </ul>
                                                      </p>
                                                   </div>
                                               </div>
                                               <div class="row media"  style="margin-top:2px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <div style="width: 55px;height: 10px;"></div>
                                                   </div>
                                                   <div class="media-desc">
                                                      <p class="desc">Media national dan komersial di Egypt:
                                                         <ul style="list-style: square;margin-top:-10px;">
                                                            <li>Fokus akan serangan Israel secara garis besar</li>
                                                            <li>kadang kritik andil Hamas di konflik</li>
                                                            <li>Tidak terlalu grafis dan fokus lebih pada tewas dan luka nya anak anak</li>
                                                         </ul>
                                                      </p>
                                                   </div>
                                               </div>
                                               </div>


                                           <div  class="row-fluid media"  style="margin-top:5px;">
                                               <h4>Media di Rusia:</h4>
                                               <div class="media-logo">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_rossiya_russia.jpg';?>" alt=''/>
                                                   <div style=""></div>
                                               </div>

                                               <div class="media-desc">
                                                   <ul style="list-style: square;">
                                                      <li>Russia media domestik tidak terlalu meliput akan konflik gaza</li>
                                                      <li>Media bahasa Inggris lebih tertarik dan menayangkan korban tewas dan tidak grafis</li>
                                                      <li>TV negara Rossiya 1 hanya menayangkan kilasan dan tidak berpihak</li>
                                                   </ul>
                                               </div>

                                           </div>

                                           <div  class="row-fluid media"  style="margin-top:5px;">
                                               <h4>Media di Cina:</h4>
                                               <div class="media-logo">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_phoenix.jpg';?>" alt=''/>
                                                   <img style="margin-top:5px;" src="<?php echo base_url().'assets/images/hotpages/gaza/media_english_cctv_news_china.jpg';?>" alt=''/>
                                                   <div style=""></div>
                                               </div>
                                               <div class="media-desc">
                                                   <ul style="list-style: square;">
                                                       <li>TV pemerintah seperti CCTV dan PhoenixTV di Hong Kong tidak grafis</li>
                                                       <li>Media sympati terhadap penduduk sipil Gaza, tapi hanya menayangkan insident utama</li>
                                                   </ul>
                                               </div>
                                           </div>

                                           <div  class="row-fluid media"  style="margin-top:5px;">
                                               <h4>Media di India:</h4>
                                               <div class="media-logo">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_ndtv_news_india.jpg';?>" alt=''/>
                                                   <img style="margin-top:5px;" src="<?php echo base_url().'assets/images/hotpages/gaza/media_cnn_ibn_india..jpg';?>" alt=''/>
                                               </div>
                                               <div class="media-desc">
                                                   <ul style="list-style: square;">
                                                       <li>Media utama seperti CNN IBN, Times Now, di India tidak terlalu meliput konflik Gaza</li>
                                                       <li>Satu media NDTV yang peduli dan lebih pro-palestina dalam laporannya</li>
                                                   </ul>
                                               </div>

                                           </div>

                                           <div  class="row-fluid media"  style="margin-top:5px;">
                                               <h4>Media di Pakistan:</h4>
                                               <div class="media-logo">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_geo_news_pakistan.jpg';?>" alt=''/>
                                               </div>
                                               <div class="media-desc">
                                                   <ul style="list-style: square;">
                                                      <li>Liputan media, seperti Geo News dan Dunya News sangat pro Palestina dan sangat kritis akan Israel</li>
                                                      <li>Liputan sangat grafis dan mengerikan</li>
                                                   </ul>
                                               </div>
                                           </div>

                                           <div  class="row-fluid media"  style="margin-top:5px;">
                                               <h4>Media di Amerika:</h4>
                                               <div class="media-logo">
                                                   <img src="<?php echo base_url().'assets/images/hotpages/gaza/media_cnn_tv.jpg';?>" alt=''/>
                                               </div>
                                               <div class="media-desc">
                                                   <ul style="list-style: square;">
                                                       <li>CNN domestik dan internasional meliput situasi Gaza secara reguler</li>
                                                       <li>Media di US lebih codong terhadap kesusahan Israel akan roket dari Hamas</li>
                                                       <li>Media tidak terlalu grafis</li>
                                                   </ul>
                                               </div>

                                           </div>

                                       </div>
                                   </div>
                               </td>

                               <td style="width: 30px;"></td>

                               <td style="width: 410px;">
                                   <div class="row-fluid box-content" style="width: 410px;height:700px;">
                                       <div style="color: #000000;">
                                           <div  style="padding-top:5px;">
                                              <h4 style="font-size:14px;margin-left:10px;">GAMES BERHUBUNGAN DENGAN KONFLIK</h4>

                                               <div  class="row-fluid media"  style="padding-top:5px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img style="width: 50px;height: 50px;" src="<?php echo base_url().'assets/images/hotpages/gaza/game_rocket_pride.jpg';?>" alt=''/>
                                                       <div style=""></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p><b>Rocket Pride</b> by Best Arabic Games, di mana pemain mencoba untuk mengakali Iron Dome sistem pertahanan rudal Israel <span style="color:red;">(dilarang di Google Play)</span></p>
                                                   </div>
                                               </div>

                                               <div  class="row-fluid media"  style="padding-top:5px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img style="width: 50px;height: 50px;" src="<?php echo base_url().'assets/images/hotpages/gaza/game_iron_dome.png';?>" alt=''/>
                                                       <div style=""></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p><b>Iron Dome</b> by Gamytech, yang menantang pemain untuk mencegat roket yang diluncurkan oleh Hamas <span style="color:red;">(dilarang di Google Play)</span></p>
                                                   </div>
                                               </div>

                                               <div  class="row-fluid media"  style="padding-top:5px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img style="width: 50px;height: 50px;" src="<?php echo base_url().'assets/images/hotpages/gaza/game_bom_gaza.jpg';?>" alt=''/>
                                                       <div style=""></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p><b>Bomb Gaza</b>, di mana pemain mencoba untuk membunuh militan tapi menghindari korban sipil, sambil mendengarkan musik Israel <span style="color:red;">(dilarang)</span></p>
                                                   </div>
                                               </div>

                                               <div  class="row-fluid media"  style="padding-top:5px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img style="width: 50px;height: 50px;" src="<?php echo base_url().'assets/images/hotpages/gaza/game_gaza_assault.png';?>" alt=''/>
                                                       <div style=""></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p><b>Gaza Assault:</b> Code Red, di mana pemain mengontrol sebuah pesawat tak berawak Israel yang menjatuhkan bom pada orang-orang dan bangunan dari atas <span style="color:red;">(di larang)</span></p>
                                                   </div>
                                               </div>

                                               <div  class="row-fluid media"  style="padding-top:5px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img style="width: 50px;height: 50px;" src="<?php echo base_url().'assets/images/hotpages/gaza/game_whack_the_hamas.jpg';?>" alt=''/>
                                                       <div style=""></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p><b>Whack the Hamas</b>, di mana gamer diperintahkan untuk menargetkan anggota Hamas karena mereka muncul dari terowongan dan digambarkan oleh pengembang sebagai "for fun and relaxation, for the people who are being killed every day by a terrorist group" <span style="color:red;">(dilarang)</span></p>
                                                   </div>
                                               </div>

                                               <div  class="row-fluid media"  style="padding-top:5px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img style="width: 50px;height: 50px;" src="<?php echo base_url().'assets/images/hotpages/gaza/game_iron_dome_missile_defense.jpg';?>" alt=''/>
                                                       <div style=""></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p><b>Iron Dome - Missile Defense</b>, by Shy Rosenzweig <span style="color:green;">(tak dilarang karena tak menyebut nama musuh)</span></p>
                                                   </div>
                                               </div>

                                               <div  class="row-fluid media"  style="padding-top:5px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img style="width: 50px;height: 50px;" src="<?php echo base_url().'assets/images/hotpages/gaza/game_gaza_hero.jpg';?>" alt=''/>
                                                       <div style=""></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p><b>Gaza Hero</b>, sebuah permainan di mana pemain keran karakter tentara Israel untuk mengubahnya menjadi makanan dan obat-obatan, yang menyatakan "kutukan Israel" pada layar diperkenalkan <span style="color:green;">(tak dilarang)</span></p>
                                                   </div>
                                               </div>

                                               <div  class="row-fluid media"  style="padding-top:5px;border-bottom:none;">
                                                   <div class="media-logo">
                                                       <img style="width: 50px;height: 50px;" src="<?php echo base_url().'assets/images/hotpages/gaza/game_gaza_defender.png';?>" alt=''/>
                                                       <div style=""></div>
                                                   </div>
                                                   <div class="media-desc">
                                                       <p><b>Gaza Defender</b>, yang melibatkan menembaki pesawat di atas <span style="color:green;">(tak dilarang)</span></p>
                                                   </div>
                                               </div>

                                           </div>
                                       </div>
                                   </div>

                                   <span id="donasi"></span>
                                   <div class="row-fluid tagline" style="margin-left:0px;margin-top:15px;"><span style="font-size:22px;padding-top:0px !important;margin-left:35px;line-height:32px;">DONASI CENTER LEGITIMASI</span></div>
                                   <div class="row-fluid box-content" style="width: 410px;height:425px;">
                                       <div style="color: #000000;">
                                           <ul style="list-style-type: square;padding: 15px;">
                                               <li style="font-size: 12px;">Komite Nasional untuk Rakyat Palestina (KNRP) dan Adara International Relief, sebesar Rp 4,5 miliar, kepada General Manager Aliansi Internasional untuk Penyelamatan Al Quds dan Palestina Usamah Owni di kantornya di Istanbul, Turki.</li>
                                               <li style="font-size: 12px;">Muhammadiyah , sebesar Rp 474,4 Juta.</li>
                                               <li style="font-size: 12px;">Grup band d'Masiv , sebesar Rp 8.308.100, kepada etikcom4palestine</li>
                                               <li style="font-size: 12px;">Bulan Sabit Merah Indonesia (BSMI), 2 Miliar dan 10 orang dari BSMI yang akan berangkat ke Gaza. Tim ini terdiri dari dokter spesialis, anastesi, orthopedi, penyakit dalam, kesehatan masyarakat, anak dan juga penerjemah bahasa.</li>
                                               <li style="font-size: 12px;">Pemerintah Indonesia, sebesar 1 juta Dollar Amerika</li>
                                               <li style="font-size: 12px;">Prabowo, sebesar Rp, 1 Miliar.</li>
                                               <li style="font-size: 12px;">DONASI: UK 17 juta pounds.</li>
                                               <li style="font-size: 12px;">DONASI: 50 ribu poundsterlings dari Guernsey's Overseas Aid Commission.</li>
                                               <li style="font-size: 12px;">DONASI: The Jersey Overseas Aid Commission has approved £120,000 for GAZA.</li>
                                               <li style="font-size: 12px;">Tanggal 7 Aug: Norwegia memulai konferensi untuk donasi guna rekontruksi Gaza.</li>
                                           </ul>
                                       </div>
                                   </div>

                                   <span id="domonstrasi"></span>
                                   <div class="row-fluid tagline" style="margin-left:0px;margin-top: 15px;"><span style="font-size:22px;padding-top:0px !important;margin-left:35px;line-height:32px;">DEMONSTRASI & AKSI LAINNYA</span></div>
                                   <div class="row-fluid box-content" style="width: 410px;height:740px;">
                                       <div style="color: #000000;">
                                         <div style="margin-top:5px;margin-left:10px;margin-right:10px;">
                                          <p>Ribuan demo di London dipelopori oleh "Stop the War Coalition & Palistine Solidarity Campaign"</p>
                                          <ul style="list-style-type: square;padding-left: 0px;">
                                              <li style="font-size: 12px;">Ratusan demo di Edinburgh</li>
                                              <li style="font-size: 12px;">50 ribu demo di Cape Town, South Africa</li>
                                              <li style="font-size: 12px;">Ribuan demo di Paris</li>
                                              <li style="font-size: 12px;">2 ribu demo di Sydney</li>
                                              <li style="font-size: 12px;">Anggota Jamiat-I-Hind demo di Bangalore, India</li>
                                          </ul>
                                         </div>
                                         <div style="margin-top:5px;margin-left:10px;height: 100px;">
                                             <div style="float:left;width: 175px;height: 100px;"><img style="width: 175px;height: 100px;" src="<?php echo base_url().'assets/images/hotpages/gaza/demo-gaza-edinburg.jpg';?>" alt=''/></div>
                                             <div style="float:left;width: 175px;height: 100px;margin-left:2px;"><img style="width: 175px;height: 100px;" src="<?php echo base_url().'assets/images/hotpages/gaza/demo-gaza-capetown.jpg';?>" alt=''/></div>
                                         </div>
                                         <div style="margin-top:5px;margin-left:10px;height: 100px;">
                                             <div style="float:left;width: 175px;height: 100px;margin-left:2px;"><img style="width: 175px;height: 100px;" src="<?php echo base_url().'assets/images/hotpages/gaza/demo-gaza-paris.jpg';?>" alt=''/></div>
                                             <div style="float:left;width: 175px;height: 100px;margin-left:2px;"><img style="width: 175px;height: 100px;" src="<?php echo base_url().'assets/images/hotpages/gaza/demo-gaza-jakarta.jpg';?>" alt=''/></div>
                                         </div>
                                         <div style="margin-top:5px;margin-left:10px;height: 110px;">
                                             <div style="float:left;width: 175px;height: 100px;margin-left:2px;"><img style="width: 175px;height: 100px;" src="<?php echo base_url().'assets/images/hotpages/gaza//demo-gaza-amman.jpg';?>" alt=''/></div>
                                             <div style="float:left;width: 175px;height: 100px;margin-left:2px;"><img style="width: 175px;height: 100px;" src="<?php echo base_url().'assets/images/hotpages/gaza/demo-gaza-bonn-germany.jpg';?>" alt=''/></div>
                                         </div>
                                         <div style="margin-top:5px;margin-left:10px;margin-right:10px;">
                                          <p style="font-size: 12px;margin-top:2px;line-height:20px;"><span style="font-size:14px;font-weight: bold;">Tanggal 5 Aug: </span>Norwegia memulai konferensi untuk donasi guna rekontruksi Gaza</p>
                                          <p style="font-size: 12px;margin-top:2px;line-height:20px;"><span style="font-size:14px;font-weight: bold;">Tanggal 3 Aug: </span>President Barack Obama dan Congress memberikan US $ 225 juta untuk teruskan operasi Iron Dome</p>
                                          <p style="font-size: 12px;margin-top:2px;line-height:20px;"><span style="font-size:14px;font-weight: bold;">Tanggal 31 Jul: </span>Israel panggil 59.000 tentara reservist</p>
                                          <p style="font-size: 12px;margin-top:2px;line-height:20px;"><span style="font-size:14px;font-weight: bold;">Tanggal 28 Jul: </span>>40 tunnels teridentifkasi</p>
                                          <p style="font-size: 12px;margin-top:2px;line-height:20px;"><span style="font-size:14px;font-weight: bold;">Tanggal 17 Jul: </span>86.000 reservists tambahan tentara Israel</p>
                                          <p style="font-size: 12px;margin-top:2px;line-height:20px;"><span style="font-size:14px;font-weight: bold;">Tanggal 11 Jul: </span>Penculik dan pembunuh 3 remaja Israel di tangkap.</p>
                                         </div>
                                       </div>
                                   </div>

                               </td>

                           </tr>

                         </table>

                       </td>
                    </tr>

                    <tr><td colspan="5" class="batas-row1"></td></tr>
                    <tr>
                       <td colspan="5"><div class="tagline"><span class="title">GAZA KONFLIK DALAM PHOTO</span></div></td>
                    </tr>
                    <span id="photogaza"></span>
                    <tr>
                       <td colspan="5" style="padding-left:10px;">
                          <div class="box-content" style="width: 920px;height:550px;">
                              <div style="color: #000000;padding-top:10px;">
                                  <ul>
                                      <?php
                                      $photobrutal = array(
                                          0 => ( array ( 'ref_name' => 'Demonstrasi di Amman', 'ref_url' => 'Demonstrasi di Amman',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-amman.jpg')),
                                          1 => ( array ( 'ref_name' => 'Demonstrasi di Amsterdam', 'ref_url' => 'Demonstrasi di Amsterdam',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-amsterdam.jpg')),
                                          2 => ( array ( 'ref_name' => 'Demonstrasi di Jakarta', 'ref_url' => 'Demonstrasi di Jakarta',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-jakarta.jpg')),
                                          3 => ( array ( 'ref_name' => 'Demonstrasi di Austin, Texas (US)', 'ref_url' => 'Demonstrasi di Austin, Texas (US)',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-austin-texas.jpg')),
                                          4 => ( array ( 'ref_name' => 'Demonstrasi di Bangalore, India', 'ref_url' => 'Demonstrasi di bangalore, India',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-bangalore.png')),
                                          5 => ( array ( 'ref_name' => 'Demonstrasi di Barcelona, Spanyol', 'ref_url' => 'Demonstrasi di Barcelona, Spanyol',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-barcelona.jpg')),
                                          6 => ( array ( 'ref_name' => 'Demonstrasi di Berlin, Jerman', 'ref_url' => 'Demonstrasi di Berlin, Jerman',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-berlin.jpg')),
                                          7 => ( array ( 'ref_name' => 'Demonstrasi di Bonn, Jerman', 'ref_url' => 'Demonstrasi di Bonn, Jerman',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-bonn-germany.jpg')),
                                          8 => ( array ( 'ref_name' => 'Demonstrasi di Boston', 'ref_url' => 'Demonstrasi di Boston',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-boston.jpg')),
                                          9 => ( array ( 'ref_name' => 'Demonstrasi di Brussel', 'ref_url' => 'Demonstrasi di Brussel',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-brussel.jpg')),
                                          10 => ( array ( 'ref_name' => 'Demonstrasi di Capetown', 'ref_url' => 'Demonstrasi di Capetown, Afrika Selatan',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-brussel.jpg')),
                                          11 => ( array ( 'ref_name' => 'Demonstrasi di Edinburg', 'ref_url' => 'Demonstrasi di Edinburg',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-edinburg.jpg')),
                                          12 => ( array ( 'ref_name' => 'Demonstrasi di Kuala Lumpur, Malaysia', 'ref_url' => 'Demonstrasi di Kuala Lumpur, Malaysia',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-kualalumpur.jpg')),
                                          13 => ( array ( 'ref_name' => 'Demonstrasi di Melbourne', 'ref_url' => 'Demonstrasi di Melbourne',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-melbourne.jpg')),
                                          14 => ( array ( 'ref_name' => 'Demonstrasi di London, Inggris', 'ref_url' => 'Demonstrasi di London, Inggris',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-london.jpg')),
                                          15 => ( array ( 'ref_name' => 'Demonstrasi di Chicago', 'ref_url' => 'Demonstrasi di Chicago',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-chicago.jpg')),
                                          16 => ( array ( 'ref_name' => 'Demonstrasi di Montreal', 'ref_url' => 'Demonstrasi di Montreal',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-montreal.jpg')),
                                          17 => ( array ( 'ref_name' => 'Demonstrasi di Nairobi', 'ref_url' => 'Demonstrasi di Nairobi',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-nairobi.jpg')),
                                          18 => ( array ( 'ref_name' => 'Demonstrasi di Mozzambique', 'ref_url' => 'Demonstrasi di Mozzambique',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-mozzambique.png')),
                                          19 => ( array ( 'ref_name' => 'Demonstrasi di Tokyo, Jepang', 'ref_url' => 'Demonstrasi di Tokyo, Jepang',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-tokyo.jpg')),
                                          20 => ( array ( 'ref_name' => 'Demonstrasi di Toronto, Kanada', 'ref_url' => 'Demonstrasi di Toronto, Kanada',
                                                  'ref_img' => base_url().'assets/images/hotpages/gaza/demo-gaza-toronto.jpg')),
                                          21 => ( array ( 'ref_name' => 'Repeated bombing is devastating Gaza\'s fragile water infrastructure', 'ref_url' => 'http://www.icrc.org/eng/assets/images/photos/2014/palestine-gaza-rtr3xs8j.jpg',
                                                  'ref_img' => 'http://www.icrc.org/eng/assets/images/photos/2014/palestine-gaza-rtr3xs8j.jpg')),
                                          22 => ( array ( 'ref_name' => 'Mourning the Shabir children. Gaza, July 17, 2014', 'ref_url' => 'http://muftah.org/wp-content/uploads/2014/07/shabir-children-gaza-1.jpg?daad1e',
                                                  'ref_img' => 'http://muftah.org/wp-content/uploads/2014/07/shabir-children-gaza-1.jpg?daad1e')),
                                          23 => ( array ( 'ref_name' => 'Mourning the Shabir children. Gaza, July 17, 2014', 'ref_url' => 'http://muftah.org/wp-content/uploads/2014/07/shabir-children-gaza-1.5.jpg?daad1e',
                                                  'ref_img' => 'http://muftah.org/wp-content/uploads/2014/07/shabir-children-gaza-1.5.jpg?daad1e')),
                                          24 => ( array ( 'ref_name' => 'Mourning the Shabir children. Gaza, July 17, 2014', 'ref_url' => 'http://muftah.org/wp-content/uploads/2014/07/shabir-children-gaza-3.jpg?daad1e',
                                                  'ref_img' => 'http://muftah.org/wp-content/uploads/2014/07/shabir-children-gaza-3.jpg?daad1e')),
                                          25 => ( array ( 'ref_name' => 'Mourning the Shabir children. Gaza, July 17, 2014', 'ref_url' => 'http://muftah.org/wp-content/uploads/2014/07/shabir-children-gaza-6.jpg?daad1e',
                                                  'ref_img' => 'http://muftah.org/wp-content/uploads/2014/07/shabir-children-gaza-6.jpg?daad1e')),
                                          26 => ( array ( 'ref_name' => 'Rafah, Gaza Strip.', 'ref_url' => 'http://static.guim.co.uk/sys-images/Guardian/Pix/pictures/2014/7/10/1405017527929/907876e6-b3ec-489e-9bb7-2c3847d907fc-620x372.jpeg',
                                                  'ref_img' => 'http://static.guim.co.uk/sys-images/Guardian/Pix/pictures/2014/7/10/1405017527929/907876e6-b3ec-489e-9bb7-2c3847d907fc-620x372.jpeg')),
                                          27 => ( array ( 'ref_name' => 'More than 40 Palestinian civilians were killed overnight, and more than 400 were wounded, in Al-Sheja\'ia neighbourhood in the east of Gaza City', 'ref_url' => 'https://www.middleeastmonitor.com/images/article_images/middle-east/man-carrying-injured-baby-in-2014-gaza-attacks.jpg',
                                                  'ref_img' => 'https://www.middleeastmonitor.com/images/article_images/middle-east/man-carrying-injured-baby-in-2014-gaza-attacks.jpg')),
                                          28 => ( array ( 'ref_name' => 'Palestinians carry the bodies of members of the Kaware family that hospital officials said were killed in an Israeli air strike on their house', 'ref_url' => 'http://img.rt.com/files/news/29/dd/c0/00/palestine-gaza-israel-offensive.si.jpg',
                                                  'ref_img' => 'http://img.rt.com/files/news/29/dd/c0/00/palestine-gaza-israel-offensive.si.jpg')),
                                          29 => ( array ( 'ref_name' => 'A Palestinian man holds a girl injured during shelling at a U.N.-run school sheltering Palestinians, at a hospital in the northern Gaza Strip on July 24, 2014.', 'ref_url' => 'http://timedotcom.files.wordpress.com/2014/07/israel-gaza-conflict.jpeg?w=1280',
                                                  'ref_img' => 'http://timedotcom.files.wordpress.com/2014/07/israel-gaza-conflict.jpeg?w=1280')),
                                          30 => ( array ( 'ref_name' => 'A Palestinian woman reacts in front of the remains of her house', 'ref_url' => 'http://www.timeslive.co.za/Feeds/Reuters_Images/2014/07/14/2014-07-14t114706z_01_gaz28_rtridsp_3_palestinians-israel-gaza-14-07-2014-13-07-58-230.jpg/ALTERNATES/crop_630x400/2014-07-14T114706Z_01_GAZ28_RTRIDSP_3_PALESTINIANS-ISRAEL-GAZA-14-07-2014-13-07-58-230.jpg',
                                                  'ref_img' => 'http://www.timeslive.co.za/Feeds/Reuters_Images/2014/07/14/2014-07-14t114706z_01_gaz28_rtridsp_3_palestinians-israel-gaza-14-07-2014-13-07-58-230.jpg/ALTERNATES/crop_630x400/2014-07-14T114706Z_01_GAZ28_RTRIDSP_3_PALESTINIANS-ISRAEL-GAZA-14-07-2014-13-07-58-230.jpg')),
                                          31 => ( array ( 'ref_name' => 'an Israeli Air Force F-15I super imposed in front of air-to-ground munitions detonations impacting targets within a urban operating sector of Gaza', 'ref_url' => 'http://cryptome.org/2014-info/gaza-bomb/pict17.jpg',
                                                  'ref_img' => 'http://cryptome.org/2014-info/gaza-bomb/pict17.jpg')),
                                          32 => ( array ( 'ref_name' => 'The mother of three-year-old Palestinian girl Jud al-Danaf touches her body during her funeral in Gaza City June 25, 2014. (Reuters)', 'ref_url' => 'http://vid.alarabiya.net/images/2014/07/10/8a5cb537-dcb3-4184-8b49-e6a68f5135c6/8a5cb537-dcb3-4184-8b49-e6a68f5135c6_16x9_600x338.jpg',
                                                  'ref_img' => 'http://vid.alarabiya.net/images/2014/07/10/8a5cb537-dcb3-4184-8b49-e6a68f5135c6/8a5cb537-dcb3-4184-8b49-e6a68f5135c6_16x9_600x338.jpg')),
                                          33 => ( array ( 'ref_name' => 'Palestinians mourn victims of Israeli air strikes in Gaza City, 12 July', 'ref_url' => 'http://www.popularresistance.org/wp-content/uploads/2014/07/Gaza-2014-mourn-the-dead.jpg',
                                                  'ref_img' => 'http://www.popularresistance.org/wp-content/uploads/2014/07/Gaza-2014-mourn-the-dead.jpg')),
                                          34 => ( array ( 'ref_name' => 'Blankfort on how some Israelis see war as "mowing grass" ', 'ref_url' => 'http://www.ancreport.com/wp-content/uploads/2014/07/israel-bomb-gaza-680x336.jpg',
                                                  'ref_img' => 'http://www.ancreport.com/wp-content/uploads/2014/07/israel-bomb-gaza-680x336.jpg')),
                                          35 => ( array ( 'ref_name' => 'Pakistan condemns Israeli attacks on Gaza\'', 'ref_url' => 'http://www.dailytimes.com.pk/digital_images/456/2014-07-09/pakistan-condemns-israeli-military-attacks-on-gaza-1404903831-5553.jpg',
                                                  'ref_img' => 'http://www.dailytimes.com.pk/digital_images/456/2014-07-09/pakistan-condemns-israeli-military-attacks-on-gaza-1404903831-5553.jpg')),
                                          36 => ( array ( 'ref_name' => 'A dozen Palestinians were killed by Israeli attacks overnight on Saturday in the occupied Gaza Strip', 'ref_url' => 'http://muslimvillage.com/wp-content/uploads/2014/07/Gaza-toll-passes-105-as-Israel-raids-continue.jpg',
                                                  'ref_img' => 'http://muslimvillage.com/wp-content/uploads/2014/07/Gaza-toll-passes-105-as-Israel-raids-continue.jpg')),
                                          37 => ( array ( 'ref_name' => 'Many Palestinians claim that the boy, who was burned to death, was murdered by Jewish extremists.', 'ref_url' => 'http://www.dw.de/image/0,,17762016_303,00.jpg',
                                                  'ref_img' => 'http://www.dw.de/image/0,,17762016_303,00.jpg')),
                                          38 => ( array ( 'ref_name' => 'Palestinians men buried the bodies of member of the Abu jamey Family.', 'ref_url' => 'http://static01.nyt.com/images/2014/07/21/world/middleeast/20140722-ISRAEL-slide-747K/20140722-ISRAEL-slide-747K-superJumbo.jpg',
                                                  'ref_img' => 'http://static01.nyt.com/images/2014/07/21/world/middleeast/20140722-ISRAEL-slide-747K/20140722-ISRAEL-slide-747K-superJumbo.jpg')),
                                          39 => ( array ( 'ref_name' => 'Smoke is seen following what police said was an Israeli air strike in Rafah in the southern Gaza Strip. July 2014', 'ref_url' => 'http://d.ibtimes.co.uk/en/full/1387999/israel-airstrike-gaza-hamas.jpg',
                                                  'ref_img' => 'http://d.ibtimes.co.uk/en/full/1387999/israel-airstrike-gaza-hamas.jpg')),
                                          40 => ( array ( 'ref_name' => 'Mourners in a refugee camp in Khan Younis pray at the funeral on July 10, 2014 for eight members of one family who were killed by an Israeli airstrike on Gaza.', 'ref_url' => 'http://previous.presstv.ir/photo/20140711/370787_Israel-attack-Gaza.jpg',
                                                  'ref_img' => 'http://previous.presstv.ir/photo/20140711/370787_Israel-attack-Gaza.jpg')),
                                          41 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'http://i.dailymail.co.uk/i/pix/2014/07/18/article-2697019-1FC0E2AB00000578-447_964x649.jpg',
                                                  'ref_img' => 'http://i.dailymail.co.uk/i/pix/2014/07/18/article-2697019-1FC0E2AB00000578-447_964x649.jpg')),
                                          42 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'http://iranian.com/data/images/raeskqieaudw.jpg',
                                                  'ref_img' => 'http://iranian.com/data/images/raeskqieaudw.jpg')),
                                          43 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'http://kaplak.net/images/gaza_massacre1.jpg',
                                                  'ref_img' => 'http://kaplak.net/images/gaza_massacre1.jpg')),
                                          44 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'https://pbs.twimg.com/media/Bsctt8rCIAE6R-A.jpg',
                                                  'ref_img' => 'https://pbs.twimg.com/media/Bsctt8rCIAE6R-A.jpg')),
                                          45 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'https://pbs.twimg.com/media/BsP_oRsIgAAG56l.jpg',
                                                  'ref_img' => 'https://pbs.twimg.com/media/BsP_oRsIgAAG56l.jpg')),
                                          46 => ( array ( 'ref_name' => 'Pengeboman Gaza', 'ref_url' => 'http://www.newsbomb.gr/media/k2/items/cache/c0f50b27947058ca6e010fdeb4ad0ae6_XL.jpg',
                                                  'ref_img' => 'http://www.newsbomb.gr/media/k2/items/cache/c0f50b27947058ca6e010fdeb4ad0ae6_XL.jpg')),
                                          47 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'http://d56amtpp2y9sx.cloudfront.net/sites/default/files/imagecache/full_page_image/uploads/Israel-bombing-Palestine.jpg',
                                                  'ref_img' => 'http://d56amtpp2y9sx.cloudfront.net/sites/default/files/imagecache/full_page_image/uploads/Israel-bombing-Palestine.jpg')),
                                          48 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'http://www.newsbomb.gr/media/k2/items/cache/c0f50b27947058ca6e010fdeb4ad0ae6_XL.jpg',
                                                  'ref_img' => 'http://www.newsbomb.gr/media/k2/items/cache/c0f50b27947058ca6e010fdeb4ad0ae6_XL.jpg')),
                                          49 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'http://i.huffpost.com/gen/1932757/thumbs/o-GAZA-900.jpg?1',
                                                  'ref_img' => 'http://i.huffpost.com/gen/1932757/thumbs/o-GAZA-900.jpg?1')),
                                          50 => ( array ( 'ref_name' => 'Kekejaman israel terhadap anak anak.', 'ref_url' => 'http://i.ytimg.com/vi/9BLdIdxp-Bg/0.jpg',
                                                  'ref_img' => 'http://i.ytimg.com/vi/9BLdIdxp-Bg/0.jpg')),
                                          51 => ( array ( 'ref_name' => 'Demonstrasi di Gaza', 'ref_url' => 'http://timedotcom.files.wordpress.com/2014/07/452716538.jpg?w=1100',
                                                  'ref_img' => 'http://timedotcom.files.wordpress.com/2014/07/452716538.jpg?w=1100')),
                                          52 => ( array ( 'ref_name' => 'Mobil PBB di bom', 'ref_url' => 'http://daylifeimages.newscred.com/imageserve/c8fdc90b21516abef93b7c8ca74d6f69/650x.jpg?center=0.5,0&background=000000',
                                                  'ref_img' => 'http://daylifeimages.newscred.com/imageserve/c8fdc90b21516abef93b7c8ca74d6f69/650x.jpg?center=0.5,0&background=000000')),
                                          53 => ( array ( 'ref_name' => 'Kota hancur oleh bom', 'ref_url' => 'https://pbs.twimg.com/media/BuNr4exCQAEqhT7.jpg:large',
                                                  'ref_img' => 'https://pbs.twimg.com/media/BuNr4exCQAEqhT7.jpg:large')),

                                      );
                                      foreach($photobrutal as $key=>$val)
                                      {
                                          ?>
                                          <li class="poster">
                                              <a href="#" data-toggle="modal" data-target="#poster-brutal-<?php echo $key;?>">
                                                  <img src='<?php echo $val['ref_img'];?>'
                                                       data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='aksi_brutal'/>
                                                  <div style=""></div>
                                              </a>
                                              <div class="modal fade" id="poster-brutal-<?php echo $key;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                                  <div class="modal-dialog">
                                                      <div class="modal-content">
                                                          <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                              <h4 class="modal-title" id="myModalLabel">Photo Aksi Brutal</h4>
                                                          </div>
                                                          <div class="modal-body" style="text-align: center;">
                                                              <img src='<?php echo $val['ref_img'];?>' style="height: 250px;width: auto;">
                                                              <p><?php echo $val['ref_name']; ?></p>
                                                          </div>
                                                          <div class="modal-footer">
                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </li>
                                      <?php
                                      }
                                      ?>
                                  </ul>
                              </div>
                          </div>
                       </td>
                    </tr>

                    <tr><td colspan="5" class="batas-row2"><span id="history"></span></td></tr>
                    <tr><td colspan=5"><div class="tagline"><span class="title">SEJARAH DAN HISTORY KONFLIK</span></div></td></tr>
                    <tr>
                      <td style="padding-left: 10px;" colspan="5">
                          <div class="box-content" style="width: 920px;height:1130px;">
                              <div style="color: #000000;">
                                  <p style="font-size: 12px;padding-top:30px;padding-left:20px;padding-right:20px;">Sebenarnya perseteruan antara Palestina dengan Israel tidak pantas disebut sebagai sebuah peperangan, melainkan merupakan penajajahan yang dilakukan bangsa Israel terhadap Palestina.</p>
                                  <p style="font-size: 12px;padding-left:20px;padding-right:20px;">Penjajahan bangsa yang dilakukan oleh Israel terhadap Palestinapada dasarnya bermotifkan perluasan kekuasaan oleh Israel . Sejarah ini berawal pada tahun 1934 hingga 1945, dimana pada tahun tersebut adalah tahun kekusaan penguasa yang terkenal ganas dan brutal dari Jerman yang bernama Adolf Hitler. Pada massa kepemimpinannya Hitler menumpas habis seluruh Bangsa Yahudi yang berada di Eropa, hingga bangsa Yahudi tersebut ketakutan dan diusir dari tanah  Eropa. Melalui tekanan dan kebrutalan Hitler, akhirnya warga Yahudi lari dan kabur ke daerah Timur tengah, yang kebetulan daerah tersebut pada tahun 1946 sedang dijajah oleh Inggris. Melalui perundingan yahudi dengan Inggris, mereka meminta izin kepada Inggris untuk membentuk suatu Negara, akhirnya diberi sedikit daerah untuk warga Yahudi untuk mendirikan Negara yang diberi nama Jewish Land, atau yang sekarang lebih Kita kenal dengan bangsa Israel.</p>
                                  <p style="font-size: 12px;padding-left:20px;padding-right:20px;">Dengan persekutuan antara Israel dengan Inggris mereka mampu memperluas kekuasaan dan menjajah bangsa yang berada disekitarnya terutama Palestine.</p>
                                  <p style="font-size: 12px;padding-left:20px;padding-right:20px;">Masyarakat dunia khususnya negara-negara Arab yang semula memihak bangsa Palestina dan berperang dengan Israel untuk membela hak-hak bangsa Palestina yang dijajah, akhirnya lebih banyak berdiam diri. Terutama sejak berdirinya negara Palestina secara resmi pada tanggal 15 Nopember 1988, dukungan negara Arab semakin melemah terhadap perjuangan bangsa Palestina menghadapi rezim zionis yang sekarang didukung mutlak oleh Amerika Serikat (AS). Sepertinya negara-negara Arab melihat Palestina bukan lagi sebagai bangsa yang lemah yang harus didukung sepenuhnya oleh sesama bangsa Arab, tapi sudah sebagai negara yang berdaulat dan mempunyai kekuatan sendiri. Atau karena ada kepentingan politik dan ekonomi sehingga rasa persaudaraan kearaban dan keislaman di antara bangsa-bangsa arab meluntur.</p>
                                  <p style="font-size: 12px;padding-left:20px;padding-right:20px;">Kebingungan kaum Muslimin dan masyarakat dunia akan nasib rakyat Palestina selanjutnya dan kapan konflik mereka dengan pemerintah Israel akan berakhir adalah pertanyaan-pertanyaan dengan jawaban memilukan. Kalau dilihat dari sepak terjang rezim zionis, maka jawabannya adalah sampai seluruh tanah Palestina habis dikuasai oleh mereka dan sebagian bangsa Palestina yang tersisa mau menjadi rakyat jajahan, bangsa kelas dua atau bahkan menjadi budak. Sedangkan bila dilihat dari semangat perjuangan bangsa Palestina melawan rezim zionis, maka jawabannya adalah sampai titik darah penghabisan dari para pejuang, mujahid mereka yang membela tanah air dan keberadaan mereka sebagai bangsa merdeka dan berdaulat di tanah sendiri.</p>
                                  <p style="font-size: 12px;padding-left:20px;padding-right:20px;">Kedaulatan bangsa Palestina dengan berdirinya negara Palestina merdeka yang diproklamirkan di Aljazair ternyata tidak sepenuhnya diakui oleh Israel . Israel menganggap Jerusalem dan Gaza sebagai bagian dari tanah perjanjian seperti yang disebutkan di dalam kitab suci mereka, yang masih dikuasai oleh bangsaPalestina . Inilah alasan kenapa bangsa Yahudi dengan semangat zionismenya lebih memilih tanah Palestina sebagai tempat untuk mendirikan negara.</p>
                                  <img src="<?php echo base_url().'assets/images/hotpages/gaza/israel-palestine_map_19225_2469.jpg';?>" alt="" style="float:right;margin:10px;width: 650px; height: auto;">
                                  <p style="font-size: 12px;padding-left:20px;padding-right:20px;">Amerika Serikat sendiri masih menerapkan standar ganda dalam hal ini. Sebagai anggota dewan keamanan PBB mengakui legalitas negara Palestina , namun di sisi lain membantu Israel secara politik, militer dan ekonomi untuk menguasaiPalestina.</p>
                                  <p style="font-size: 12px;padding-left:20px;padding-right:20px;">Dunia arab dan Islam menganggap berdirinya negara Israel adalah bentuk dari pemaksaan atas keberadaan orang-orang Yahudi di tanah Palestina. Bagi bangsa Palestinarezim zionis Israel dan bangsa Yahudinya adalah penjajah yang mendatangi dan ingin merebut tanah air mereka, bukan sebuah negara tetangga yang sedang bertengkar dengan mereka. Bagi para pejuang Palestina peperangan yang mereka lakukan adalah sebuah perjuangan heroik mempertahankan keberadaan tanah air dan bangsanya, persis seperti pejuangan kita memerdekan diri dari penjajah Belanda dan Jepang.</p>
                                  <p style="font-size: 12px;padding-left:20px;padding-right:20px;">Memang benar perang antara Palestina dan Israel bukan perang agama, tetapi tidak bisa dilepaskan dari sebab-sebab pemikiran keagamaan yang berasal dari kitab suci. Alasan utama mereka berperang adalah memperebutkan tanah air, termasuk juga daerah Jerusalem yang merupakan tempat suci bagi tiga agama samawi di dunia, di mana di sana berdiri mesjid Alaqsa (Alharam alqudsi ashsharif) yang dijadikan tempat ibadah umat Islam atau disebut juga sebagai Bukit Bait Allah (The Temple Mount / Har ha-Bayit) bagi umat Yahudi dan Nasrani. Dan terkenal dengan dinding ratapan (The Western Wall/The Wailing wall/Ha Kotel Ha Ma’aravi) yang terletak di sebelah barat masjid Alaqsa sebagai tempat ibadah umat Yahudi, atau disebut Alburaq Wall oleh kaum Muslimin.</p>
                              </div>
                          </div>
                      </td>
                    </tr>

                    <tr><td colspan="5" class="batas-row2"></td></tr>

                    <tr><td colspan=5" style="padding: 10px;"><div class="tagline"><span id='bloggaza' class="title">BLOG</span></div></td></tr>
                    <tr>
                        <td colspan="5" style="padding-left: 10px;">
                           <div class="span12">
                              <div class="row-fluid" id="home-latest">
                                 <div class="blog-col effect-1" style="margin-left:10px;padding-top:0px;">
                                    <div id="bloggaza_container" data-tipe="1" data-page='1'></div>
                                 </div>
                              </div>
                           </div>
                        </td>
                    </tr>

                </table>


            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('a.collapse').click(function() {
            if ( $(this).next('.judul-coll').hasClass('in') ) {
                $('.judul-coll').text('Tutup detil');
            } else {
                $('.judul-coll').text('Klik untuk selengkapnya');
            }
        });

        $(window).scroll(function() {
            $(".pin").each( function() {
                if( $(window).scrollTop() > $(this).offset().top - 500 ) {
                    $(this).css('opacity',1);

                    console.debug($(this).offset().top - 500 );
                }
            });

        });

    });

</script>