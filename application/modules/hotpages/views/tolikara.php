<?php 
$data = [
    'NARASI'=>[
        'title'=>'DAMAI DI TOLIKARA',
        'narasi'=>'<img src="http://www.klikpositif.com/media/images/news/insiden-tolikara-kedua-tersangka-akui-turut-lempar-jemaah_20150727145008.jpg" class="pic">Kerusuhan berdarah terjadi disalah satu distrik di kabupaten Tolikara, Papua, menyusul protes yang dilakukan ratusan anggota jemaat Gereja Injil  Indonesia (GIDI) terhadap penyelenggaraan Salat Idul Fitri di lapangan Markas Komando (MAKO) rayon Militer pada Jumat, (17/07/2015).
                    </p><p>Ratusan massa mendatangi lapangan tempat diselenggarakannya Salat Ied. Mereka meminta agar umat Islam tidak menyelenggarakan Salat Ied di lapangan, tapi di dalam tempat ibadah.
                    </p><p><img src="http://statis.dakwatuna.com/wp-content/uploads/2015/07/insiden-tolikar.jpg" class="pic2" style="height: 140px;">Polisi yang saat itu mengamankan jalannya Salat idul Fitri tersebut langsung membubarkan ratusan massa tersebut dengan menembakkan senjata.  Jemaat GIDI membalasnya dengan melakukan tindakan anarkis. Akibatnya, kericuhan pun pecah.
                    </p><p>Massa lalu membakar dan menghanguskan puluhan rumah dan toko serta mushallah. Dalam kerusuhan tersebut, puluhan orang mengalami luka serius dan seorang meninggal akibat terkena peluru.
                    </p><p><img src="http://www.jurnalitas.com/go/file/uploads/2015/07/masjid-di-tolikara-mojok.jpg" class="pic">Pihak kepolisian mengklaim tindakan penembakan tersebut sebagai upaya untuk mengamankan situasi, karena ratusan jemaat GIDI berpotensi melakukan aksi penyerangan terhadap umat muslim yang sedang melakukan Salat, namun dari pihak jemaat GIDI berdalih bahwa protes mereka akan dilukakan dengan negosiasi. Tetapi kericuhan justru pecah akibat ulah kepolisian yang bertindak sewenang-wenang.
                    </p><p>Pasca pecahnya konflik berdarah tersebut menyulut beragam tanggapan dari banyak kalangan. Banyak pihak yang menyayangkan pecahnya konflik tersebut. Akibat kelalaian pihak kepolisian diduga sebagai pemicu terjadinya kerusuhan yang melibatkan kepolisian dan pihak jemaat GIDI. Puluhan jemaat dan seorang lainnya tewas terkena timah panas aparat kepolisian.'
    ],
    'ANALISA'=>[
        'narasi'=>'<img src="http://www.intiwarta.com/wp-content/uploads/2015/07/papua-6.jpg" class="pic2">Peristiwa Tolikara menambah daftar panjang kasus intoleransi antar umat beragama di Indonesia. Kekerasan yang bermotif agama memang sering terjadi di tanah air. Kasus kekerasan terhadap Ahmadiyah, kasus Sampang Madura, Ambon dan Poso merupakan peristiwa yang mencoreng kerukunan antar umat beragama di Indonesia.
                    </p><p class="font_kecil">Kerukunan antar agama dan perbedaan keyakinan di Indonesia masih menjadi polemik yang berpotensi melahirkan konflik bernuangsa SARA. Sikap abai masyarakat dan pemerintah terhadap keberadaan kelompok minoritas menjadi salah satu pemicu meningkatnya kasus diskriminasi.
                    </p><p class="font_kecil">Umumnya, kasus semacam ini dilakukan oleh kelompok yang lebih mayoritas. Di beberapa tempat, kasus kekerasan terhadap kelompok minoritas sering diabaikan oleh pemerintah sehingga memicu konflik dengan skala yang lebih besar.
                    </p><p class="font_kecil"><img src="http://www.szaktudas.com/wp-content/uploads/2015/07/Aksi-Damai-muslim-surakarta-untuk-papua.jpg" class="pic">Pemerintah seakan tidak hadir untuk melindungi hak warganegaranya dalam beragama dan berkeyakinan. Kerusuhan di Tolikara akibat kegagalan pemerintah dalam membangun toleransi di daerah. Masyarakat yang berbeda keyakinan dengan mudah melakukan tindakan anarkis terhadap kelompok lain karena tidak adanya tatanan kerukunan dan toleransi  yang dibangun sebelumnya oleh pemerintah.
                    </p><p class="font_kecil">Selain itu, penanganan yang dilakukan oleh pemerintah terhadap konflik tersebut masih menggunakan cara-cara yang justru dapat menyulut emosi pihak yang bertikai sehingga menimbulkan konflik yang lebih besar.'
    ],
    'SOLUSI'=>[
        'narasi'=>'<img src="http://elshinta.com/upload/article/mef-voaindonesia3566634924.jpg" class="pic">Respon pemerintah dalam hal ini Presiden Jokowi bersama menteri terkait, terbilang tangkas dan cepat. Seminggu setelah insiden tolikara terjadi, Presiden langsung menginstruksikan penanganan komprehensif atas insiden tersebut. Hal ini ditegaskan Presiden Joko Widodo dalam rapat kabinet terbatas di Istana Negara, Jakarta, Rabu tanggal 22 Juli 2015. 
                    </p><p class="font_kecil">Penanganan itu meliputi penindakan hukum terhadap pelaku, rehabilitasi bangunan rusak, dan menjalin dialog dengan tokoh-tokoh di Papua.
                    </p><p class="font_kecil">Sehari setelah itu, presiden juga memanggil seluruh pemuka agama ke Istana membahas soal kerukunan umat beragama menyusul kerusuhan di Tolikara, Papua. Pertemuan itu dihadiri oleh tokoh-tokoh tersebut antara lain Azyumardi Azra, Ketua Pengurus Besar Nahdlatul Ulama (PBNU) Said Aqil Siradj, dan Ustad Yusuf Mansyur, hadir juga perwakilan dari PWI, PGI, pada Kamis, 23/072015.
                    </p><p class="font_kecil"><img src="http://brightcove01.brightcove.com/23/4077388032001/201507/2544/4077388032001_4369476713001_150723-TEMPO-CHANNEL-TEMPO-KINI-BEGINI-CARA-MUSLIM-DAN-GIGI-BERDAMAI-DI-TOLIKARA.jpg?pubId=4077388032001" class="pic2">Berikut intruksi presiden mengenai Insiden Tolikara. Pertama, Jokowi meminta penegakan hukum diselesaikan. Kedua, Presiden memerintahkan segera dilakukan kembali pembangunan fasilitas yang rusak di Tolikara. Ketiga, Presiden akan dialog dengan tokoh agama dan tokoh masyarakat Papua untuk sama sama menenangkan situasi di sana dan nasional.
                    </p><p class="font_kecil">Komite Umat untuk Tolikara (Komat Tolikara) menyatakan bantuan dari masyarakat umum untuk korban peristiwa di Karubaga, Kabupaten Tolikara, Papua, mencapai Rp 1,3 miliar.
                    </p><p class="font_kecil">Diketahui ada enam rumah, sebelas kios, dan satu musala ludes terbakar. Sebanyak 11 warga terluka dan 1 anak tewas. Ketum Persekutuan Gereja-Gereja dan Lembaga-Lembaga Injili Indonesia (PGLII), Ronny Mandang, menjelaskan bahwa semua warga Tolikara yang jadi korban dalam kerusuhan di Tolikara, Jumat 17 Juli 2015 adalah warga jemaat Gereja Injili Di Indonesia (GIDI).'
    ],
    'BERUJUNG'=>[
        'narasi'=>'<img src="http://cdn.sindonews.net/dyn/620/content/2015/07/30/14/1027703/kapolda-papua-yotje-mende-digantikan-kapolda-papua-barat-alR.jpg" class="pic">Pasca-insiden yang terjadi di Tolikara, Papua, Kapolri Jenderal Badrodin Haiti melakukan mutasi terhadap Kapolda Papua Irjen Yotje Mende. Posisi Yotje digantikan Kapolda Papua Barat Brigjen Paulus Waterpaw.
                    </p><p class="font_kecil">Hal ini tertuang dalam Surat Telegram Kapolri Nomor ST/195//VII/2015 tertanggal Kamis 30 Juli 2015, posisi Waterpaw akan diisi oleh Brigjen Royke Lumowa yang sebelumnya bertugas di Kementerian Koordinator Politik, Hukum, dan Keamanan sebagai Asisten Deputi Koordinasi Penanganan Daerah Konflik dan Kontijensi. 
                    </p><p class="font_kecil">Seperti diketahui, di akhir masa pensiun, Yotje dihadapkan dengan insiden penyerangan umat Islam di Distrik Karubaga, Tolikara, Papua, saat hari raya Idul Fitri 1436 Hijriah, Jumat 17 Juli 2015. Selain itu Yotje juga disibukkan dengan  keikutsertaannya di seleksi calon pimpinan Komisi Pemberantasan Korupsi (KPK).'
    ],
    'PROFIL'=>[
        'narasi'=>'<img src="https://upload.wikimedia.org/wikipedia/id/e/e1/Karubaga_Tolikara_1.JPG" class="pic2">Kabupaten Tolikara memiliki luas wilayah 5.234 km2 yang terbagi menjadi 4 kecamatan dengan Karubaga sebagai ibukota kabupaten. Kabupaten ini memiliki penduduk sebanyak 54.821 jiwa (2003). Wilayahnya berbatasan dengan Kabupaten Sarmi di sebelah utara, Kabupaten Jayawijaya di sebelah Selatan, Kabupaten Puncak Jaya di sebelah barat dan Kabupaten Jawawijaya di sebelah Timur. Dari keempat distrik yang ada (Karubaga, Kanggime, Kembu dan Bokondini), hanya distrik Karubaga dan Kanggime yang dapat dijangkau melalui udara dan jalan darat. Melalui udara, Distrik Karubaga atau Kanggimi dapat dicapai dari Wamena dalam waktu sekitar 20 menit.
                    </p><h5>Perekonomian</h5><p class="font_kecil">Kontribusi utama perekonomian daerah ini datang dari pertanian. Di daerah pedalaman yang merupakan ulayat mereka secara turun temurun, kegiatan pertanian dilakukan secara tradisional. Lahan tanaman bahan pangan sebagian besar ditanami ubi jalar. Tanaman rambat ini memang merupakan makanan pokok penduduk kabupaten ini. Sentra penghasil ubi jalar berada di Distrik Karubaga. Sama seperti daerah lain di Papua, babi merupakan ternak utama masyarakat. Karubaga dan Kanggime merupakan distrik yang terbanyak memelihara ternak ini. Tolikara juga merupakan daerah penghasil batu gamping yang digunakan sebagai bahan baku pengolahan semen. Potensi batu gamping mencapai jutaan ton kubik menyebar dari Tolikara sampai Yahukimo dan Jayawijaya.
                    </p><h5>Peta Kabupaten Tolikara</h5><p class="font_kecil">Kabupaten ini akan dimekarkan dari Kabupaten Tolikara dan Kabupaten Lanny Jaya. Distrik yang mungkin bergabung ke dalam kabupaten ini meliputi :<br>Dari Kabupaten Tolikara : Yuneri, Tagineri, Tagime, Yaleka, Danime, Poganeri<br>Dari Kabupaten Lanny Jaya: Maki, Poga, Dimaba, Gamelia
                    </p><h5>Kota Karubaga</h5><p class="font_kecil">Rencana pemerintah untuk pemekaran daerah otonomi Kota Karubaga yang  akan pisah dengan Kota induknya yakni Kabupaten Tolikara masih menunggu  proses lebih lanjut. Rencananya Kota Karubaga ada 9 kecamatan yang nantinya masuk kedalam kota Karubaga.'
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="http://cdn.rimanews.com/bank/masjid-di-bakar.jpg" class="pic2">Kekerasan yang berlatar belakang agama yang terjadi di Papua memicu polemik dari beragam kalangan. Pasca insiden tersebut, pemerintah cenderung melihat konflik tersebut disebabkan karena kurangnya komunikasi di antara komunitas Gereja Injili di Indonesia (GIDI) dan Muslim lokal.
                    </p><p class="font_kecil">Kekerasan tersebut merupakan ekspresi ketidakpuasan masyarakat yang emosional. Bahkan kasus tersebut hanya tindakan kriminal biasa, bukan konflik agama.
                    </p><p class="font_kecil"><img src="http://hizbut-tahrir.or.id/wp-content/uploads/2015/07/IMG_9003.jpg" class="pic">Sikap berbeda justru datang dari para aktivis penggiat HAM. Mereka menilai konflik tersebut merupakan pelanggaran HAM, yakni kebebasan beragama dan berkeyakinan.
                    </p><p class="font_kecil">Untuk itu, pemerintah dihimbau agar lebih peka terhadap keberadaan kelompok minoritas yang rentang terhadap tindakan diskriminasi. Pemerintah harus dapat memberikan jaminan perlindungan hukum dan menjamin kekerasan atas nama agama tidak terulang lagi.'
    ],
    'KRONOLOGI'=>[
        'list'=>[
            ['date'=>'','content'=>'Tanggal 11 Juli 2015, beredar selebaran yang yang berisi larangan terhadap agama lain untuk melaksanakan kegiatan keagamaan. Selebaran ditandangani oleh Pendeta Mathem Jingga, S.Th, MA dan Pendeta Nayus Wendas, S.Th.'],
            ['date'=>'','content'=>'Pukul 07.00 WIT saat jamaah muslim akan memulai kegiatan Salat Ied di lapangan Makoramil 1702-11, Karubaga. Ratusan Jemaat GIDI mendatangi lapangan tempat berlangsungnya kegiatan salat Idul Fitri. Massa beroperasi menghimbau kepada Jemaah Salat Ied untuk tidak melakasanakan ibadah Salat Ied di lapangan.'],
            ['date'=>'','content'=>'Selang beberapa menit kemudian, massa yang dikoordinir oleh pendeta Marthen Jingga mulai melempari jamaah salat ied dengan batu. Mereka memaksa kegiatan salat Ied agar segera dibubarkan.'],
            ['date'=>'','content'=>'Selanjutnya, kericuhan semakin memanas saat aparat kepolisian melepaskan tembakan peringatan. Namun massa jemaat GIDI tidak mengindahkan peringatan tersebut.'],
            ['date'=>'','content'=>'Pukul 08.00, massa justru melempari polisi dengan batu. Kericuhan terjadi antara aparat polisi dengan massa Jemaat GIDI'],
            ['date'=>'','content'=>'Api mulai membakar puluhan rumah, kios dan sebuah mushallah'],
            ['date'=>'','content'=>'Pukul 09.10 WT, massa dari dari pendeta Marthen Jingga berkumpul di ujung bandara karubaga untuk bersiaga.']
        ]
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'komiteummatuntuktolikarakomat55bc370dbd56b'],
            ['page_id'=>'majelisulamaindonesia53097864039ce'],
            ['page_id'=>'komnasham54c1eac938164'],
            ['page_id'=>'nahdlatululamanu53310bea6f11d'],
            ['page_id'=>'ppmuhammadiyah5296b45eefcaa'],
            ['page_id'=>'gerejainjilidiindonesiagidi55a9bc4d6dfca']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'setarainstitute533a7f7211a39'],
            ['page_id'=>'jaringangusdurian55bc3b936972d'],
            ['page_id'=>'frontpembelaislam5317d72f772e0'],
            ['page_id'=>'gmki55bc475d79ca6']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Ir. H. Joko Widodo','jabatan'=>'Presiden RI','img'=>'https://yt3.ggpht.com/-p0NW5C1v22g/AAAAAAAAAAI/AAAAAAAAAAA/_Bm12bemzeo/s900-c-k-no/photo.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'Saya kira tidak ada kata terlambat sehingga tidak ada gesekan kecil dan lebih baik aparat tegas menyelesaikan masalah (Tolikara) ini'],
        ['from'=>'Jusuf Kalla','jabatan'=>'Wapres RI','img'=>'http://www.thestage.luwuraya.net/wp-content/uploads/2012/10/jusuf-kalla.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'Sekarang sudah membaik. Di Papuanya sudah baik, sudah damai. Presiden sudah memerintahkan untuk membangun kembali. Pemdanya, Mensos di situ juga membantu, semua ikut bantu, jadi secara umum di Papua sudah aman'],
        ['from'=>'Tedjo Edhy','jabatan'=>'Menkopulhakam RI','img'=>'http://cdn.sindonews.net/dyn/620/content/2014/10/27/12/915782/tedjo-edhy-eks-ksal-banting-setir-jadi-politikus-tA4.jpg','url'=>'http://www.bijaks.net/aktor/profile/laksamanatnipurntedjoedhie540925438ba50','content'=>'Itu kami redam. Itu hanya solidaritas sempit'],
        ['from'=>'Lukman Hakim Syarifuddin','jabatan'=>'Menteri Agama RI','img'=>'https://upload.wikimedia.org/wikipedia/id/4/4c/Lukman_Hakim_Saifuddin_MPR.jpg','url'=>'http://www.bijaks.net/aktor/profile/drshlukmanhakimsaifuddin511756084eb20','content'=>'Substansinya harus dipahami dulu, apa maksud dari wacana perda itu, bagaimana isinya, dan hak-hak umat beragama. Jadi itu harus didalami dulu. Maknanya harus dikaji lebih mendalam menurut saya supaya kita tidak beda persepsi'],
        ['from'=>'Romo Benny Susetyo','jabatan'=>'Sekretaris Dewan Nasional Setara','img'=>'http://www.hidupkatolik.com/foto/bank/images/sajut-a-benny-susetyo-pr-hidup-katolik.jpg','url'=>'http://www.bijaks.net/aktor/profile/romobennysusetyo51cba6402881a','content'=>'Masalah belum jelas kita tunggu tim independen untuk menjelaskan duduk masalah, lebih baik media memberitakan yang damai karena penting untuk menjaga persatuan bangsa diatas segalanya'],
        ['from'=>'Dorman Wandikmbo','jabatan'=>'Presiden Gereja Injil di Indonesia (GIDI)','img'=>'https://pbs.twimg.com/media/CKcinELUYAILgB3.jpg:medium','url'=>'','content'=>'Kami menyetujui dan menyepakati bahwa yang pertama adalah masalah Tolikara akan kami selesaikan secara adat dengan damai, aman, untuk sekarang dan seterusnya'],
        ['from'=>'Luhut Panjaitan','jabatan'=>'Kepala Staff Kepresidenan','img'=>'http://cdn.tmpo.co/data/2014/10/22/id_336533/336533_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/jendralpurnluhutbinsarpanjaitan5189af75dc288','content'=>'Selama ini kita suka tidak menuntaskan. Tapi saya lihat sekarang Presiden minta menuntaskan semua. Tapi langkah-langkah awal, sudah diambil. Saya kira sudah bagus'],
        ['from'=>'Maruar Sirait','jabatan'=>'Politisi PDIP','img'=>'http://assets.jaringnews.com//3/2013/05/16/d8668197f46f255baa5fecda33f2b039_1.jpg','url'=>'http://www.bijaks.net/aktor/profile/Maruarar','content'=>'Kejadian ini telah mengusik kita semua. Para pelakuanya harus mendapatkan sanksi hukum dan negara tidak boleh melakukan pembiaran atas kasus ini'],
        ['from'=>'KH Tengku Zulkarnain','jabatan'=>'Wakil Sekretaris Jenderal Majelis Ulama Indonesia (Wasekjen MUI)','img'=>'http://cdn.ar.com/images/stories/2014/03/wakil-ketua-majelis-ulama-indonesia-mui-tengku-zulkarnaen-_130905141032-172.jpg','url'=>'http://www.bijaks.net/aktor/profile/tengkuzulkarnain54a618f7d273d','content'=>'Dengan bersatunya tokoh-tokoh agama dan telah diselesaikannya masalah Tolikara secara adat itu memang tujuan kita agar tercapai kedamaian sampai hari kiamat'],
        ['from'=>'Ustaz Ali Muchtar','jabatan'=>'Tokoh Agama Islam di Tolikara','img'=>'http://www.suara-islam.com/images/berita/tolikara-ali-mukhtar_20150723_172725.jpg','url'=>'','content'=>'Kami mewakili umat Muslim dan selaku tokoh agama Islam yang ada di Tolikara menyampaikan bahwa insiden yang terjadi di Tolikara diselesaikan secara damai dan kami setuju bahwa insiden tersebut bukan masalah SARA atau agama'],
        ['from'=>'Teten Masduki','jabatan'=>'Anggota Tim Komunikasi Presiden','img'=>'http://radarsukabumi.com/wp-content/uploads/2012/10/Teten-Masduki.jpg','url'=>'http://www.bijaks.net/aktor/profile/tetenmasduki51a78cb913c9c','content'=>'Bukan khusus Tolikara, tetapi pada dasarnya mempertahankan NKRI dengan keragaman suku agama'],
        ['from'=>'Sutiyoso','jabatan'=>'KaBIN RI','img'=>'https://upload.wikimedia.org/wikipedia/id/a/a3/Sutiyosodki.jpg','url'=>'http://www.bijaks.net/aktor/profile/letjentnipurnsutiyoso511c2b6deeb46','content'=>'Kemajemukan itu juga jadi titik lemah kita. Tapi saya berharap justru kemajemukan itulah yang menjadi persatuan bangsa kita']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Ahmad Shabri Lubis','jabatan'=>'Ketua Dewan Pimpinan Pusat Front Pembela Islam (FPI)','img'=>'http://www.fastrack-funschool.com/images/upload/images/alisa.jpg','url'=>'http://www.bijaks.net/aktor/profile/meilanyalissa51c29f5ac5a47','content'=>'FPI mendorong Kepolisian untuk menegakkan hukum yang berkeadilan, karena kami lihat ada pergeseran yang seakan-akan menganggap ini sebuah permasalahan kecil, tidak tahu dampak ke depannya'],
        ['from'=>'Hendardi','jabatan'=>'Ketua SETARA Institute','img'=>'http://www.satuharapan.com/uploads/pics/news_36101_1430131190.jpg','url'=>'','content'=>'meminta polisi untuk segera melakukan penyelidikan yang adil terkait kerusuhan di Tolikara, Papua'],
        ['from'=>'Alissa Wahid','jabatan'=>'Jaringan Gusdurian','img'=>'http://img.lensaindonesia.com/uploads/1/2015/05/31373-hendardi-setara-percuma-reshuffle-kalau-kepemimpinan-jokowi-tak-berubah.jpg','url'=>'http://www.bijaks.net/aktor/profile/hendardi54f6811b30c7e','content'=>'Kami juga menyayangkan kegagalan aparat penegak hukum dalam melindungi kebebasan beribadah dan mencegah terjadinya konflik sosial sebagaimana diamanatkan konstitusi dan undang-undang'],
        ['from'=>'Ayub Manuel','jabatan'=>'Ketua Umum GMKI','img'=>'http://cdn.ar.com/images/stories/2012/06/shobri-lubis.jpg','url'=>'','content'=>'GMKI menilai ada kejanggalan dari sejumlah informasi yang beredar']
    ],
    'VIDEO'=>[
        ['id'=>'mN-ap5cdNOs'],
        ['id'=>'NqQ9alT5xHo'],
        ['id'=>'gwT_sHF3cEA'],
        ['id'=>'_JWlbIpyc9Q'],
        ['id'=>'7d6C3B7MOBM'],
        ['id'=>'nsA5Vr5Gdxg'],
        ['id'=>'cing7RISNU0'],
        ['id'=>'3Ya1AHuBh4M'],
        ['id'=>'XAx7WGbdWFg'],
        ['id'=>'dwNNEtnXcyM'],
        ['id'=>'x6r6vnliRPQ'],
        ['id'=>'cVWbdLT3NRg'],
        ['id'=>'0S-9N2Xvr_o'],
        ['id'=>'XYnp4TBNChA']
    ],
    'FOTO'=>[
        ['img'=>'http://islamedia.id/wp-content/uploads/2015/07/papua_damai-600x330.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/07/23/414924/UeMkUratFO.jpg?w=668'],
        ['img'=>'http://www.piyunganonline.org/sites/default/files/field/image/19b18c61-de2f-412b-a1f0-02d476392a60_169.jpg'],
        ['img'=>'http://www.metrosulawesi.com/sites/default/files/styles/img_780_439/public/main/articles/foto-3_10.jpg?itok=x3JJcW_F'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/07/20/6eb00f49-af1a-4611-ac40-96482289aa8d_169.jpg?w=780&q=90'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/sejumlah-tokoh-agama-saling-bergandengan-tangan-saat-pembacaan-pernyataan-_150724143532-520.jpg'],
        ['img'=>'http://img1.beritasatu.com/data/media/images/medium/1437707476.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/07/31/337/1188418/jelang-pensiun-yotje-mende-bersyukur-tolikara-damai-AXlwF3N2uQ.jpg'],
        ['img'=>'http://kriminalitas.com/wp-content/uploads/2015/07/IMB_000224715.jpg'],
        ['img'=>'http://cdn-media.viva.id/thumbs2/2015/07/23/326368_forum-komunikasi-pimpinan-daerah-riau-_663_382.jpg'],
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/07/18/569210/670x335/5-butir-kesepakatan-damai-tokoh-agama-terkait-insiden-tolikara.jpg'],
        ['img'=>'http://images.detik.com/customthumb/2015/07/22/157/panglim1.jpg?w=780&q=90'],
        ['img'=>'http://img1.beritasatu.com/data/media/images/medium/871437117056.jpg'],
        ['img'=>'http://analisadaily.com/assets/image/news/big/2015/07/presiden-jokowi-ditemui-tokoh-masyarakat-tolikara-di-istana-merdeka-154075-1.jpg'],
        ['img'=>'http://www.jpnn.com/picture/watermark/20150723_170619/170619_105235_Ulama_Tolikara_ric_d.JPG'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/07/22/141832020150721-120602780x390.jpg'],
        ['img'=>'http://beritahati.com/images/artikel/1169.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/07/23/cb22d279-5715-445b-aa42-39a8ce119927_169.jpg?w=650'],
        ['img'=>'http://elshinta.com/upload/article/mef-voaindonesia3566634924.jpg'],
        ['img'=>'http://static.skalanews.com/media/news/thumbs-396-263/kerusuhan_tolikara_papua-pojoksatu.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/07/25/5303a0ff-46b2-42f0-9626-5992386ac4ba_169.jpg?w=650'],
        ['img'=>'http://img.antaranews.com/new/2015/07/ori/20150725antarafoto-presiden-tokoh-tolikara-240715-ym-1.jpg']
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/tolikara/top.png")?>') no-repeat transparent;
        height: 1352px;
        margin-bottom: -795px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .font_kecil {
        font-size: 13px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
        background: rgba(0, 0, 0, 0.7);
    }
    .block_red {
        background-color: #009be0;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 80%;
        height: auto;
        margin: 0 auto;
        display: inherit;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        margin-right: 10px;
        max-width: 50px;
    }
    .garis {
        border-top: 1px dotted black;
    }
    .boxgray {
        width: 99%;
        border: 5px solid lightgray;
        box-shadow: -5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }
    .penyokong {
        width: 100%;
        height: auto;
        display: inline-block;
        margin-left: 20px;
    }
    .boxpenyokong {
        width: 126px;
        height: auto;
    }
    .foto {
        width: 100px;
        height: 100px;
        border: 3px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
        padding: 10px 10px;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 121px;height: 115px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 110px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi2 img {width: 98px; height: 70px;  padding: 0px !important;}
    li.organisasi2 p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 121px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
    li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.dukung {float: left; padding:3px;vertical-align: top;width: 177px;height:115px;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
    li.dukung img {width: auto; height: 75px;padding: 0px !important;margin: 0 auto;display: block;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;line-height: 12px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }   
    .uprow {margin-left: 20px;margin-top:10px;border-color: #00923f transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {
        width: 260px;
        height: auto;
        margin-left: 20px;
        margin-top:0px;
        margin-bottom: 10px;
        background-color: #424040;
        background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px #00923f;
        border-right:solid 2px #00923f;
        border-bottom:solid 2px #00923f;
        border-radius: 0 0 5px 5px;
        color: #ffffff;
        z-index: 100;
    }
    .kronologi-title {font-size:14px;font-weight:bold;background-color: #00923f;color: white;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {line-height: 15px;font-size:12px;}

    .ketua {
        background-color: yellow;
        width: 30%;
        display: block;
        float: left;
        margin-right: 3%;
        box-shadow: -3px 3px 10px gray;
        border-radius: 8px;
        margin-bottom: 15px;
    }
    .ketua img {
        width: 100%;
        height: 200px;
    }
    .kritik {
        font-size: 18px;
        font-weight: bold;
        color: black;
    }
    .isi-col50 {
        float: left;
        margin: 10px 16px;
        width: 41%;
        height: auto;
        display: inline-block;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #29166f;
        width: 293px;
        height: 345px;
        float: left;
        margin-bottom: 10px;
        padding-top: 5px;
        margin-left: 10px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        float: left;
        border: 1px solid black;
    }
    #bulet {
        background-color: #ffff00; 
        text-align: center;
        width: 50px;
        height: 25px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: black;
        padding: 6px 10px;
        margin-left: -10px;
        margin-right: 10px;
    }

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">ANALISA</a></h4>
                <p class="font_kecil"><?php echo $data['ANALISA']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="prokontra" style="color: black;">PRO DAN KONTRA</a></h4>
                <p class="font_kecil"><?php echo $data['PROKONTRA']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENDUKUNG</p>
                <ul style="margin-left: 0px;">
                    <?php
                    foreach($data['PENDUKUNG']['partai'] as $key=>$val) {
                        $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                        $personarr = json_decode($person, true);
                        $pageName = strtoupper($personarr['page_name']);
                        if($val['page_id'] == 'komiteummatuntuktolikarakomat55bc370dbd56b'){
                            $photo = 'http://www.kemanusiaan.id/content_images/toliharadamai_thumbmedium.jpg';
                            $pageName = 'Komat (Komite Ummat Untuk Tolikara)';
                        }
                        elseif($val['page_id'] == 'komnasham54c1eac938164'){
                            $photo = 'https://upload.wikimedia.org/wikipedia/id/f/fa/Logo-Komnas-HAM.png';
                            $pageName = 'Komnas HAM';
                        }
                        elseif($val['page_id'] == 'gerejainjilidiindonesiagidi55a9bc4d6dfca'){
                            $photo = 'http://img.eramuslim.com/media/thumb/500x0/2015/07/logo-GIdI.jpg';
                            $pageName = 'GIDI';
                        }
                        else {
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                        }
                        ?>
                        <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                            <li class="organisasi">
                                <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                <p><?php echo $pageName;?></p>
                            </li>
                        </a>
                    <?php
                    }
                    ?>
                </ul>
                <div class="clear"></div>

                <h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENENTANG</p>
                <ul style="margin-left: 0px;">
                    <?php
                    foreach($data['PENENTANG']['partai'] as $key=>$val) {
                        $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                        $personarr = json_decode($person, true);
                        $pageName = strtoupper($personarr['page_name']);
                        if($val['page_id'] == 'setarainstitute533a7f7211a39'){
                            $photo = 'http://setara-institute.org/wp-content/uploads/2014/11/si-logo-2.png';
                            $pageName = 'SETARA INSTITUTE';
                        }
                        elseif($val['page_id'] == 'jaringangusdurian55bc3b936972d'){
                            $photo = 'http://3.bp.blogspot.com/-jvrw0-Qbies/VbCouVTHwdI/AAAAAAAABIA/l4zHYhShdbU/s1600/1392106706844.jpg';
                            $pageName = 'Jaringan Gusdurian';
                        }
                        elseif($val['page_id'] == 'gmki55bc475d79ca6'){
                            $photo = 'https://upload.wikimedia.org/wikipedia/id/9/9c/Gmki_02.jpg';
                            $pageName = 'GMKI';
                        }
                        else {
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                        }
                        ?>
                        <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                            <li class="organisasi">
                                <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                <p><?php echo $pageName;?></p>
                            </li>
                        </a>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <div class="col_kanan">
                <div class="boxprofile white">
                    <h4 class="block_red text-center"><a id="profil" style="color: white;">TOLIKARA</a></h4>
                    <img src="http://tolikarakab.go.id/wp-content/uploads/2015/04/LOGO-KABUPATEN-TOLIKARA.png" class="picprofil"><br>
                    <p style="margin-left: 20px;margin-right: 20px;">
                        <ul>Kabupaten Tolikara
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Semboyan : Nawi Arigi</li>
                        </ul>

                        <ul>Peta lokasi Kabupaten Tolikara
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Koordinat : Mien Kogoya</li>
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Provinsi :Papua</li>
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Ibu kota : Karubaga</li>
                        </ul>


                        <ul>Pemerintahan :
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Bupati : Usman Wanimbo</li>
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Wakil Bupati : Amos Jikwa</li>
                        </ul>

                        <ul>DAU : Rp. 507.270.132.000,- (2013)</ul>
                        <ul>Luas : 14.564 km2</ul>
                        <ul>Populasi :
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Total 147.750 jiwa (2000)</li>
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Kepadatan: 10,14 jiwa/km2</li>
                        </ul>
                        <ul>Demografi
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Kode area telepon</li>
                            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">12250</li>
                        </ul>
                    </p>
                </div><br>

                <h4 class="list" style="margin-left: 20px;"><a id="kronologi" style="color: black;">KRONOLOGI</a></h4>
                <?php 
                foreach ($data['KRONOLOGI']['list'] as $key => $value) {
                ?>
                <div class="uprow"></div>
                <div class="kronologi">
                    <div class="kronologi-title"><?php echo $value['date'];?></div>
                    <div class="kronologi-info">
                        <p><?php echo $value['content'];?></p>
                    </div>
                </div>
                <div class="clear"></div>
                <?php
                }
                ?>

                <h4 class="list" style="margin-left: 20px;margin-top: 25px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="newstolikara_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newstolikara_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">PROFIL TOLIKARA</a></h4>
                <img src="https://upload.wikimedia.org/wikipedia/id/b/b8/Peta_Infrastruktur_Kabupaten_Tolikara_%282012%29.gif">
                <p class="font_kecil"><?php echo $data['PROFIL']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">SOLUSI DAMAI DARI ISTANA</a></h4>
                <p class="font_kecil"><?php echo $data['SOLUSI']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="analisa" style="color: black;">BERUJUNG MUTASI KAPOLDA PAPUA</a></h4>
                <p class="font_kecil"><?php echo $data['BERUJUNG']['narasi'];?></p>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
                <div class="clear"></div>
                    <ul style="margin-left: 0px;margin-top: 10px;">
                        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
                            <?php 
                            for($i=0;$i<=5;$i++){
                            ?>
                            <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 77%;margin-right: 10px;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div style="float: left;width: 47%;margin-left: 20px;">
                            <?php 
                            for($i=6;$i<=11;$i++){
                            ?>
                            <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 78%;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </ul>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENENTANG</a></h4>
                <div class="clear"></div>
                <ul style="margin-left: 0px;margin-top: 10px;">
                    <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
                        <?php 
                        for($i=0;$i<=1;$i++){
                        ?>
                        <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 77%;margin-right: 10px;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div style="float: left;width: 47%;margin-left: 20px;">
                        <?php 
                        for($i=2;$i<=3;$i++){
                        ?>
                        <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 78%;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                </ul>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
            <div class="col_full" style="display: inline-block;">
                <ul style="margin-left: 0px;margin-top: 0px;">
                    <?php
                    $no=1;
                    foreach ($data['FOTO'] as $key => $val) { ?>
                        <li class="gallery">
                            <a href="#" data-toggle="modal" data-target="#img-<?php echo $no;?>">
                                <img src="<?php echo $val['img'];?>" />
                            </a>
                        </li>
                        <div class="modal hide fade" style="overflow-y: scroll;height: 550px;width: auto;" id="img-<?php echo $no;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Gallery Foto</h4>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?php echo $val['img'];?>" />
                                    </div>
                                    <!-- <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    <?php
                    $no++;
                    }
                    ?>
                </ul>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 10px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>
            <div class="col_full">
                <div class="boxgray" style="height: 220px;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>