<?php
$data = [
    'NARASI'=>[
        'title'=>'KONTROVERSI LARANGAN KURBAN DI JAKARTA',
        'narasi'=>'<img src="http://media.suara.com/thumbnail/650x365/images/2014/10/04/Idul-Adha.jpg?watermark=true" class="pic"><p>Gubernur DKI Jakarta Basuki Tjahaja Purnama (Ahok) mengakui dahulu Pemerintah Provinsi (Pemprov) DKI sempat mengizinkan penjualan serta pemotongan hewan kurban di sembarang tempat, termasuk pinggir jalan. Hal itu disebabkan karena belum dilakukan tes kesehatan sebelumnya pada hewan kurban.</p>
                   <p>Banyaknya penjual hewan kurban yang berjualan di trotoar menjelang Idul Adha membuat Gubernur Ahok mulai mengatur tempat berjualan mereka. Ahok juga mengatur tempat pemotongan hewan kurban.</p>
                   <p>Mantan politikus Partai Gerindra tersebut menjelaskan, kebijakan itu diinstruksikannya memiliki tujuan untuk menjaga kesehatan masyarakat. Sebab, darah hewan kurban yang dipotong sembarangan di tanah dapat menimbulkan spora yang berbahaya bagi kesehatan.</p>'
    ],

    'PROKONTRA'=>[
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2015/09/06/175346820150906-103738780x390.JPG" class="pic"><p>Sejak Gubernur DKI Jakarta Basuki Tjahaja Purnama mengeluarkan Ingub Nomor 168 Tahun 2015, pro dan kontra bermunculan. Misalnya, Anggota DPRD dari Fraksi PKS, Nasrullah, bahkan menuding Ahok telah kebablasan karena mengatur cara ibadah umat Islam. Pernyataan Nasrullah ini terkait Instruksi Gubernur yang mengatur tentang Pengendalian, Penampungan dan Pemotongan Hewan.</p>
                   <p>Menurut Nasrullah, kalau Gubernur sudah mengatur-atur masalah ibadah, maka ini akan terjadi konflik. Karena orang ibadah itu di mana saja dan kapan saja, yang penting baik lokasinya, kemudian dibersihkan, tempatnya mencukupi, dan tidak mengganggu warga lainnya.‎</p>
                   <p>Staf Hubungan Masyrakat (Humas) Masjid Istiqlal, Abu Hurairah Abdul Salam, mengingatkan Gubernur Ahok yang melarang penyembelihan hewan kuran di tempat sembarangan.</p>
                   <p>“Berdasarkan Pergub (Peraturan Gubernur), ketika Idul Adha terdapat pengecualian mengenai tempat penyembelihan hewan. Proses penyembelihan boleh dilakukan di luar Rumah Pemotongan Hewan (RPH),” katanya. Kamis (10/9/2015).</p>
                   <p>Tak dijelaskan Pergub mana yang dimaksud, namun lelaki yang akrab disapa Abuh itu menjelaskan, dalam Pergub itu ditetapkan bahwa selama tempat penyembelihan mempermudah dokter dari Suku Dinas Kesehatan untuk melakukan pemeriksaan, maka tempat tersebut boleh digunakan. Namun demikian ia menyarankan agar setiap masjid di Jakarta memiliki RPH sendiri guna mempermudah proses pemantauan hewan kurban dan proses penyembelihannya.</p>'
    ],

    'KESEHATAN'=>[
        'narasi'=>'<img src="http://cdn1-a.production.liputan6.static6.com/medias/743441/big/061998300_1411885177-periksa_hewankurban.jpg" class="pic"><p>Gubernur DKI Jakarta Basuki Tjahaja Purnama alias Ahok yang melarang penjualan sekaligus pemotongan hewan kurban yang digelar di atas trotoar. Beralasan, larangan ini dilakukan karena bisa berdampak buruk terhadap kesehatan masyarakatnya.</p>
                   <p>Untuk mendukung pelarangan tersebut, Ahok membandingkan dengan aturan pemotongan hewan kurban yang berlangsung di negara-negara Islam, terutama Arab Saudi. Dia meyakini, negara yang menjadi tempat tujuan umat Islam melaksanakan rukun Islam kelima itu tidak melaksanakan pemotongan hewan kurban di sembarang tempat.</p>
                   <p>"Itu dulu kenapa diizinkan potong hewan kurban semua di jalan? Dulu ilmu kesehatan belum tahu penyakit-penyakit berbahaya. Contoh kita enggak ngerti kenapa anak kecil habis main di tanah kok tiba-tiba pulang kena penyakit terus meninggal, sekarang kita teliti baru ngerti itu karena kena darah hewan, dia (hewan) punya spora itu baru bisa mati sekian bulan, jadi orang meninggal," kata Ahok di Jakarta, Selasa (7/9).</p>
                   <p>Untuk mengantisipasi hal itu, Ahok mengaku telah menginstruksikan bawahannya untuk menyiapkan Rumah Pemotongan Hewan (RPH) sebagai lokasi pemotongan resmi. Seluruh pedagang yang akan menjual hewan kurban juga di tempat-tempat yang sesuai dengan peruntukannya.</p>'
    ],

    'INTRUKSI'=>[
        'narasi'=>'<img src="http://mirajnews.com/id/wp-content/uploads/sites/3/2015/09/INGUB-DKI-168-660x330.jpg" class="pic"><p>Gubernur Ahok menerbitkan Instruksi Gubernur (Ingub) Nomor 168 Tahun 2015 tentang Pengendalian, Penampungan, dan Pemotongan Hewan. Kebijakan ini dalam rangka menyambut Idul Adha tahun 2015/1436 Hijriah.</p>
                   <img src="http://media.suara.com/thumbnail/650x365/images/2014/10/04/Idul-Adha.jpg?watermark=true" class="pic2"><p>Aturan itu mencantumkan pelarangan penjualan serta pemotongan hewan kurban di pinggir jalan. Selain itu, Pemprov DKI juga melarang pemotongan hewan kurban di sekolah-sekolah. Kemudian hewan-hewan yang akan dijual dan disembelih juga harus dites kesehatan terlebih dahulu.</p>
                   <p>Penjualan hewan kurban akan difokuskan pada lapangan tertentu. Sementara Pemprov DKI mendorong pemotongan hewan kurban di rumah potong hewan (RPH).</p>'
    ],

    'KECAMAN'=>[
        'narasi'=>'<img src="http://manjanik.com/wp-content/uploads/2015/09/Ahok-vs-FPI-Soal-Qurban.jpg" class="pic"><p class="font_kecil">Front Pembela Islam (FPI) menyatakan statement Gubernur DKI Jakarta Basuki Tjahja Purnama (Ahok) tentang sekolah dilarang menyembelih kurban itu sejatinya telah melecehkan umat Islam.</p>
                   <img src="http://cdn.tmpo.co/data/2013/09/29/id_224016/224016_620.jpg" class="pic2"><p class="font_kecil">>Wakil Ketua Umum Dewan Pimpinan Pusat Front Pembela Islam (FPI), KH Ja’far Sidiq mengatakan, sejatinya berqurban baik di masjid dan sekolah sudah menjadi tradisi warga Jakarta. Qurban di sekolah pun memberikan pelajaran terhadap anak-anak di sekolah.</p>
                   <p class="font_kecil">Menurut Kyai Ja’far, Ahok itu sudah tidak pantas lagi di anggap sebagai Gubernur DKI Jakarta lagi. Selain selalu bertutur kata buruk, Ahok juga selalu menyakiti hati rakyatnya. Dia pun tidak pernah sekali pun memberikan pengajaran baik terhadap generasi penerus di Jakarta.</p>'
    ],

    'LANGGAR'=>[
        'narasi'=>'<img src="http://www.hidayatullah.com/files/bfi_thumb/MUI-pus-2yiuqewbwcebnsdu0lr3ls.jpg" class="pic"><p class="font_kecil">“Dalam UUD 1945 disebutkan kalau setiap orang berhak memeluk agamanya masing-masing dan beribadah sesuai dengan agamanya. Kalau Ahok melarang pemotongan hewan kurban di depan masjid sebab masjid bukan RPH, Ahok melanggar undang-undang," kata Wasekjen Majelis Ulama Indonesia (MUI), Tengku Zulkarnain, Jumat (11/9/2015).</p>
                   <img src="http://static.republika.co.id/uploads/images/detailnews/daging-kurban-_141006141024-758.JPG" class="pic2"><p class="font_kecil">Lagi pula, ujar Tengku, selama ini umat Muslim tak pernah memotong hewan kurban di pinggir jalan. Pemotongan hanya dilakukan di halaman masjid, lapangan, ataupun halaman rumah.</p>
                   <p class="font_kecil">"Ahok tak berhak melarang umat Muslim memotong hewan kurban di halaman masjid. Saya tetap akan memotong hewan kurban di depan masjid, kalau berani melarang silakan datang ke masjid saya," kata Tengku.</p>'
    ],

    'TANGGAPAN'=>[
        'narasi'=>'<img src="http://www.rmol.co/images/berita/normal/191908_11121904062015_dprd_dki.jpg" class="pic"><p class="font_kecil">Wakil Ketua DPRD DKI Jakarta Triwisaksana (Sani) berharap Pemprov DKI tidak melakukan penertiban terkait adanya Intruksi Gubernur Nomor 168 Tahun 2015 tentang pemotongan hewan di Rumah Pemotongan Hewan (RPH).</p>
                   <img src="http://www.intelijen.co.id/wp-content/uploads/2014/09/hewan-kurban-sitnurulilmi-595x279.jpg" class="pic2"><p class="font_kecil">Menurut Sani, pelarangan pemotongan hewan ditempat umum seharusnya telebih dahulu dilakukan sosialisai agar warga ibu kota dapat memahami bahaya penyakit yang ditimbulkan dari pemotongan hewan tersebut.</p>
                   <p class="font_kecil">Politisi Partai Keadilan Sejahtera (PKS) ini mengimbau, Pemprov DKI selanjutnya melakukan edukasi terhadap dampak bahaya pemotongan hewan kurban di tempat umum. Ia menilai, edukasi jauh lebih baik dilakukan ketimbang pelarangan.</p>'
    ],

    'QUOTE_PENENTANG'=>[
        ['from'=>'Prof Dr KH Ali Mustafa Yakub MA','jabatan'=>'Imam Besar Masjid Istiqlal','img'=>'https://www.banksinarmas.com/id/syariah/images/aliImam.jpg','url'=>'#','content'=>'““Kalau memang dipotong di RPH (rumah pemotongan hewan) cukup gak menampung hewan yang sebanyak itu?” Prof Dr KH Ali Mustafa Yakub MA - Imam Besar Masjid Istiqlal'],
        ['from'=>'Amir Ma ruf','jabatan'=>'Direkur Lembaga Amil Zakat','img'=>'https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/6/005/094/061/2c2c75f.jpg','url'=>'#','content'=>'"Padahal kurban di kawasan Sekolah Dasar bisa dijadikan media pembelajaran bagi anak usia dini," Amir Ma ruf - Direkur Lembaga Amil Zakat, Infaq dan Sodaqoh PBNU'],
        ['from'=>'Ismail Yusanto','jabatan'=>'Juru Bicara HTI','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2013--12--50688-ismail-yusanto-pks-tak-jauh-beda-dengan-partai-sekuler.jpg','url'=>'#','content'=>'“Gubernur ini tidak paham nilai islam akhirnya tidak bisa berpikir tepat,” Ismail Yusanto - Juru Bicara HTI'],
        ['from'=>'Harry Kurniawan','jabatan'=>'Aktifis Lembaga Bantuan Hukum','img'=>'http://statis.dakwatuna.com/wp-content/uploads/2014/09/Harry-Kurniawan-Skretaris-LBH-Adil-Sejahtera.jpg','url'=>'#','content'=>'“ "Apabila Instruksi ini tetap dilaksanakan akan menjauhkan anak didik dari nilai-nilai religius, khususnya di Sekolah Dasar. Pemotongan hewan kurban adalah rangkaian pelaksanaan ibadah bagi umat islam dalam rangka hari raya Idul Adha," Adil Sejahtera Harry Kurniawan  - Aktifis Lembaga Bantuan Hukum'],
        ['from'=>'Lulung Lunggana','jabatan'=>'Ketua DPW PPP Jakarta','img'=>'http://cdn0-a.production.liputan6.static6.com/medias/822604/big/053234400_1425549157-DPRD_Preskon_4.jpg','url'=>'#','content'=>'“"Kebijakan tersebut jelas merupakan bentuk penghapusan budaya yang sudah lama ada" Lulung Lunggana - Ketua DPW PPP Jakarta.'],
        ['from'=>'Saiful Rachmad Dasuki','jabatan'=>'Ketua Gerakan Pemuda (GP) Ansor','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'#','content'=>'"Kebijakan gubernur ini sangat aneh, pemotongan hewan kurban kan ritual keagamaan yang harus dihormati, bukan malah dilarang," Saiful Rachmad Dasuki - Ketua Gerakan Pemuda (GP) Ansor DKI Jakarta'],
        ['from'=>'Lucky P Sastrawirya','jabatan'=>'Anggota DPRD DKI Jakarta','img'=>'http://www.rmoljakarta.com/images/berita/thumb/thumb_956283_06475329072015_Lucky_P_S.jpg','url'=>'#','content'=>'"Instruksi ini ngawur. Prosesi pemotongan hewan kurban tidak bisa sembarangan, ada tata cara menurut ajaran Islam," Lucky P Sastrawirya - Anggota DPRD DKI Jakarta'],
        ['from'=>'Ardy Purnawan Sani','jabatan'=>'Wakil Ketua Dewan Kota Jakarta Pusat','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'#','content'=>'"Larangan itu sudah terjadi dua kali, sejak era Ahok masih jadi Wagub dan kini Gubernur DKI. Banyak penolakan dari masyarakat, tapi anehnya kok Ahok malah jalan terus?," Ardy Purnawan Sani - Wakil Ketua Dewan Kota Jakarta Pusat'],
        ['from'=>'Prof Dr Hj Tuty Alawiyah','jabatan'=>'Ketua Majelis Ulama Indonesia','img'=>'http://mirajnews.com/id/wp-content/uploads/sites/3/2015/03/Tuty-Alawiyah.jpg.jpg','url'=>'#','content'=>'"Pelaksanaan penyembelihan hewan kurban di dekat masyarakat dilakukan karena tujuan yang jelas, yaitu menjangkau lapisan masyarakat lebih dalam. Karena itu, pelaksanaan penyembelihan yang dilakukan di kalangan masyarakat, seperti masjid dan mushala, tidak perlu dilarang," Prof Dr Hj Tuty Alawiyah -  Ketua Majelis Ulama Indonesia'],
        ['from'=>'Tegar Putuhena','jabatan'=>'Ketua #lawanAhok','img'=>'http://mirajnews.com/id/wp-content/uploads/sites/3/2015/03/Tuty-Alawiyah.jpg.jpg','url'=>'#','content'=>'"kami mendesak Ahok untuk  meminta maaf  dan meralat pernyataannnya di hadapan media dalam waktu 3 x 24 Jam karena tidak sesuai dengan instruksi yang telah ditandatanganinya sendiri," Tegar Putuhena - Ketua #lawanAhok'],
        ['from'=>'Tengku Zulkarnain','jabatan'=>'Wasekjen MUI','img'=>'http://cdn.ar.com/images/stories/2014/03/wakil-ketua-majelis-ulama-indonesia-mui-tengku-zulkarnaen-_130905141032-172.jpg','url'=>'#','content'=>'"Selama ini kami memotong hewan kurban di halaman masjid. Darahnya dibuatkan lubang sendiri agar mengalir ke dalam tanah, tidak dibuang sembarangan," Tengku Zulkarnain - Wasekjen MUI'],
        ['from'=>'Ratna Sarumpaet','jabatan'=>'Aktivis','img'=>'http://img.lensaindonesia.com/uploads/1/2012/11/Ratna-Sarumpaet-Soal-Lady-Gaga.jpg','url'=>'#','content'=>'“Kualitas moral, kepemimpinanya dan cintanya ke Indonesia hangus. Seperti memotong kurban di jalan, dia bilang dia gubernur bersih tapi kemudian melarang potong kurban, dia sombong, nggak mau belajar dari kultur (budaya) orang Jakarta,” Ratna Sarumpaet - Aktivis'],
        ['from'=>'Rahmat HS','jabatan'=>'Tokoh Pemuda Tanah Abang','img'=>'http://www.teropongsenayan.com/foto_berita/201503/04/medium_51Rahmat%20HS%20(emka).jpg','url'=>'#','content'=>'"Ini sangat sensitif dan tidak semestinya Ahok masuk ke ranah yang tidak dipahaminya. Karena bisa menimbulkan keresahan umat," Rahmat HS - Tokoh Pemuda Tanah Abang'],
    ],

    'VIDEO'=>[
        ['id'=>'96tP2NeZqyI'],
        ['id'=>'IXxtvx8jXUA'],
        ['id'=>'zy2XjTRqDIo'],
        ['id'=>'t6sQbHlrpn0'],
        ['id'=>'OT0nY8AMqu0'],
        ['id'=>'_6a-HUI907g'],
        ['id'=>'Lk8WNULBPbc'],
        ['id'=>'8pvzrh6J4GY'],
        ['id'=>'hAdluFV9akI'],
        ['id'=>'RdXPRTtStAg'],
        ['id'=>'GnBkqkfK5h8'],
        ['id'=>'21YToVCOQYY'],
        ['id'=>'Ml1ogXblTy0'],
        ['id'=>'J2QpaFKsL0s'],
        ['id'=>'soNQGPipVIE'],
        ['id'=>'BWNou8_mUB4']
    ],

    'FOTO'=>[
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/larangan-berjualan-hewan-kurban-di-jalur-hijau-trotoar-taman-_150908133151-532.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/20141007_151720_pemotongan-hewan-kurban.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/09/08/105501520150902-133028780x390.JPG'],
        ['img'=>'http://www.suara-islam.com/images/berita/ahok_20130726_074823.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/09/25/296692/l4R3QSuEvy.jpg?w=668'],
        ['img'=>'http://2.bp.blogspot.com/-purpJq6J4R4/VCIIN_OVEQI/AAAAAAAAzxE/xmFKFVz9Lcw/s1600/hewan%2Bkurban.jpg'],
        ['img'=>'http://statis.dakwatuna.com/wp-content/uploads/2015/09/AHok-NTT-Sapi-660x371.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/daging-kurban-_141006141024-758.JPG'],
        ['img'=>'https://kabarislam.files.wordpress.com/2014/09/hewan-kurban.jpg?w=468'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/09/06/175346820150906-103738780x390.JPG'],
        ['img'=>'http://media.suara.com/thumbnail/650x365/images/2014/09/17/rs20140916012-e1410921683738.jpg?watermark=true'],
        ['img'=>'http://images.detik.com/customthumb/2014/11/12/10/105826_wawancaraahok5.jpg?w=780&q=90'],
        ['img'=>'http://cdn-2.tstatic.net/wartakota/foto/bank/images/20140930-wakil-wali-kota-bekasi-ahmad-syaikhu-dan-sapi.jpg'],
        ['img'=>'http://baranews.co/system/application/views/main-web/foto_news/ori/895540490-potong_kurban.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/sejumlah-sapi-kurban-dijajakan-di-sebuah-spbu-di-_131003184021-109.jpg'],
        ['img'=>'http://poskotanews.com/cms/wp-content/uploads/2014/09/ilusahok4.jpg'],
        ['img'=>'http://4.bp.blogspot.com/-tqO_2ao7Qwc/VCPeR3XRneI/AAAAAAAACjc/myoXh2YdRxM/s1600/domba%2Bkurban.jpg'],
        ['img'=>'http://cdn.ansideng.com/dynamic/article/2015/09/08/21012/83o85xVZnO.jpg?w=630'],
        ['img'=>'http://media.suara.com/thumbnail/650x365/images/2014/10/04/Idul-Adha.jpg?watermark=true'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/969826/big/025791700_1440751453-ahok_jaksel.jpg'],
        ['img'=>'http://www.voa-islam.com/photos3/abuvakha/jual_hewan_kurban_tanabang.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/petugas-suku-dinas-pertanian-dan-peternakan-jakarta-pusat-melakukan-_150910164611-906.jpg'],
        ['img'=>'http://cdn.ar.com/images/_t/500x0/stories/2013/09/sembelih-kurban.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/09/29/298255/aIG5Byf8n5.jpg?w=668'],
        ['img'=>'http://cdn.ar.com/images/_t/500x0/stories/2013/10/potong-hewan-kurban-1.jpg'],
        ['img'=>'http://cdn.ar.com/images/_t/500x0/stories/2013/10/potong-hewan-kurban-1.jpg'],
        ['img'=>'http://1.bp.blogspot.com/-3mE8hxBMly4/VCIqav6HX7I/AAAAAAAABBM/l6KU7AalkL8/s1600/Hewan%2BKurban.jpg'],
        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/15542/big/hewan-kurban-131014b.jpg'],
        ['img'=>'http://media.suara.com/thumbnail/650x365/images/2015/09/09/o_19uojl90t18r95k91ded16961n58a.jpg?watermark=true'],
        ['img'=>'http://media.viva.co.id/thumbs2/2013/10/15/225935_pemotongan-hewan-kurban_663_382.jpg']
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
.allpage {width: 100%;height: auto;margin-top: 8px;}
.col_top {background: url('<?php echo base_url(); ?>assets/images/hotpages/kontroqurban/top.jpg') no-repeat transparent;height: 1352px;margin-bottom: -590px;}
.col_kiri {width: 64%;height: auto;background: transparent;float: left;padding-right: 2%;}
.col_kiri p, .col_kiri li, .font_kecil {font-size: 13px;}
.col_kanan {width: 34%;height: auto;background: transparent;float: left;}
.col_kiri2 {width: 34%;height: auto;background: transparent;float: left;padding-right: 2%;}
.col_kanan2 {width: 64%;height: auto;background: transparent;float: left;}
.col_kiri50 {width: 20%;height: auto;/*background-color: red;*/float: left;padding-right: 1%;}
.col_kanan50 {width: 78%;height: auto;/* background-color: green; */float: right;padding-left: 1%;border-left: 1px dotted black;display: inline-table;}
.col_full {width: 100%;/*background-color: lightgray;*/}
.boxprofile {box-shadow: -5px 5px 10px gray;border-radius: 10px 10px 10px 10px;width: 290px;margin: 0 auto;padding-bottom: 10px;background: rgba(0, 0, 0, 0.7);}
.block_red {background-color: #761908;border-radius: 10px 10px 0 0;padding-top: 5px;padding-bottom: 5px;}
.picprofil {width: 80%;height: auto;margin: 0 auto;display: inherit;}
.pic {float: left;margin-right: 10px;max-width: 200px;margin-top: 5px;}
.pic2 {float: right;margin-left: 10px;max-width: 200px;margin-top: 5px;}
.pic3 {margin-right: 10px;max-width: 50px;}
.garis {border-top: 1px dotted black;}
.boxgray {width: 99%;border: 5px solid lightgray;box-shadow: -5px 5px 10px gray;}
.boxgray_red {width: 96%;border: 9px solid #a60008;border-radius: 8px;box-shadow: -5px 5px 10px gray;}
.boxgray_green {width: 96%;border: 9px solid #00a651;border-radius: 8px;box-shadow: -5px 5px 10px gray;}
.penyokong {width: 100%;height: auto;display: inline-block;margin-left: 20px;}
.boxpenyokong {width: 126px;height: auto;}
.foto {width: 100px;height: 100px;border: 3px solid lightgray;border-radius: 8px;box-shadow: 5px 5px 10px gray;padding: 10px 10px;}
li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 113px;border: solid 4px #c4c4c4;text-align: center}
li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
li.organisasi p {width: 100%;padding: 0px !important;font-size: 8px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 110px;border: solid 4px #c4c4c4;text-align: center}
li.organisasi2 img {width: 98px; height: 70px;  padding: 0px !important;}
li.organisasi2 p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
li.video img {width: 121px;height: auto;padding: 0px !important;}
li.video img:hover {box-shadow: 0px 0px 5px black;}
li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
li.gallery img:hover {box-shadow: 0px 0px 5px black;}
li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

li.dukung {float: left; padding:3px;vertical-align: top;width: 177px;height:115px;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
li.dukung img {width: auto; height: 75px;padding: 0px !important;margin: 0 auto;display: block;}
li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;line-height: 12px;}

.black {color: black;}
.white {color: white;}
.list {
    background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
    padding-left: 30px;
}
.list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
p, li {text-align: justify;font-size: 14px;}
.clear {clear: both;}
.qpenentang {float: left;width: 30%;height: auto;background-color: red;display: inline-block;border-bottom: 1px solid black;}
.parodi {
    width: 107px;
    height: 87px;
    float: left;
    margin-right: 10px;
    margin-bottom: 10px;
}
.uprow {margin-left: 20px;margin-top:10px;border-color: #761908 transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
.kronologi {
    width: 260px;
    height: auto;
    margin-left: 20px;
    margin-top:0px;
    margin-bottom: 10px;
    background-color: #424040;
    background: rgba(0, 0, 0, 0.6);
    border-left:solid 2px #761908;
    border-right:solid 2px #761908;
    border-bottom:solid 2px #761908;
    border-radius: 0 0 5px 5px;
    color: #ffffff;
    z-index: 100;
}
.kronologi-title {font-size:14px;font-weight:bold;background-color: #761908;color: white;padding:5px;text-align: center;}
.kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
.kronologi-info p {line-height: 15px;font-size:12px;}

.ketua {
    background-color: yellow;
    width: 30%;
    display: block;
    float: left;
    margin-right: 3%;
    box-shadow: -3px 3px 10px gray;
    border-radius: 8px;
    margin-bottom: 15px;
}
.ketua img {
    width: 100%;
    height: 200px;
}
.kritik {
    font-size: 18px;
    font-weight: bold;
    color: black;
}
.isi-col50 {
    float: left;
    margin: 10px 16px;
    width: 41%;
    height: auto;
    display: inline-block;
}
.boxdotted {
    border-radius: 10px;
    border: 2px dotted #29166f;
    width: 293px;
    height: 345px;
    float: left;
    margin-bottom: 10px;
    padding-top: 5px;
    margin-left: 10px;
}
.bendera {
    width: 150px;
    height: 75px;
    margin-right: 10px;
    float: left;
    border: 1px solid black;
}
#bulet {
    background-color: #ffff00;
    text-align: center;
    width: 50px;
    height: 25px;
    border-radius: 50px 50px 50px 50px;
    -webkit-border-radius: 50px 50px 50px 50px;
    -moz-border-radius: 50px 50px 50px 50px;
    color: black;
    padding: 6px 10px;
    margin-left: -10px;
    margin-right: 10px;
}
.gallery {
    list-style: none outside none;
    padding-left: 0;
    margin-left: 0px;
}
.gallery li {
    display: block;
    float: left;
    height: 80px;
    margin-bottom: 7px;
    margin-right: 0px;
    width: 120px;
}
.gallery li a {
    height: 100px;
    width: 100px;
}
.gallery li a img {
    max-width: 115px;
}

</style>
<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.thumb2').click(function(){
            var target = $(this).data('homeid');
            var src = $(this).attr('src');
            $('#'+target).attr('src',src);
        });
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<br/>
<div class="container">
<div class="sub-header-container">
<div class="allpage">
<div class="col_top"></div>
<div class="col_kiri">
    <h4><a id="analisa" style="color: black;"><?php echo $data['NARASI']['title'];?></a></h4>
    <p><?php echo $data['NARASI']['narasi'];?></p>
    <div class="garis"></div>

    <h4 class="list"><a id="prokontra" style="color: black;">PRO DAN KONTRA</a></h4>
    <p class="font_kecil"><?php echo $data['PROKONTRA']['narasi'];?></p>
    <div class="clear"></div>

    <h4 class="list"><a id="prokontra" style="color: black;">ALASAN DEMI KESEHATAN</a></h4>
    <p class="font_kecil"><?php echo $data['KESEHATAN']['narasi'];?></p>
    <div class="clear"></div>

    <h4 class="list"><a id="prokontra" style="color: black;">INTRUKSI GUBERNUR</a></h4>
    <p class="font_kecil"><?php echo $data['INTRUKSI']['narasi'];?></p>
    <div class="clear"></div>

    <h4 class="list"><a id="prokontra" style="color: black;">KECAMAN FPI</a></h4>
    <p class="font_kecil"><?php echo $data['KECAMAN']['narasi'];?></p>
    <div class="clear"></div>

    <h4 class="list"><a id="prokontra" style="color: black;">MUI ANGGAP AHOK LANGGAR UUD 1945</a></h4>
    <p class="font_kecil"><?php echo $data['LANGGAR']['narasi'];?></p>
    <div class="clear"></div>

    <h4 class="list"><a id="prokontra" style="color: black;">TANGGAPAN DPRD DKI JAKARTA</a></h4>
    <p class="font_kecil"><?php echo $data['TANGGAPAN']['narasi'];?></p>
    <div class="clear"></div>

</div>

<div class="col_kanan">
    <div class="boxprofile white">
        <h4 class="block_red text-center"><a id="profil" style="color: white;">TENTANG IDUL ADHA</a></h4>
        <img src="https://arsiparmansyah.files.wordpress.com/2013/10/ilustrasi-nb-ibrahim-ismail.jpg" class="picprofil"><br>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Idul Adha di Republik Indonesia, dikenal sebagai Hari Raya Haji, adalah sebuah hari raya Islam. Pada hari ini diperingati peristiwa kurban, yaitu ketika Nabi Ibrahim (Abraham), yang bersedia untuk mengorbankan putranya Ismail untuk Allah, akan mengorbankan putranya Ismail, kemudian digantikan oleh-Nya dengan domba.</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;"><img src="http://cdn0-a.production.liputan6.static6.com/medias/664552/big/tawaf02--anri+syaiful.jpg" class="pic">Pada hari raya ini, umat Islam berkumpul pada pagi hari dan melakukan salat Ied bersama-sama di tanah lapang atau di masjid, seperti ketika merayakan Idul Fitri. Setelah salat, dilakukan penyembelihan hewan kurban, untuk memperingati perintah Allah kepada Nabi Ibrahim yang menyembelih domba sebagai pengganti putranya.</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Hari Raya Idul Adha jatuh pada tanggal 10 bulan Dzulhijjah, hari ini jatuh persis 70 hari setelah perayaan Idul Fitri. Hari ini juga beserta hari-hari Tasyrik diharamkan puasa bagi umat Islam.</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Pusat perayaan Idul Adha adalah sebuah desa kecil di Arab Saudi yang bernama Mina, dekat Mekkah. Di sini ada tiga tiang batu yang melambangkan Iblis dan harus dilempari batu oleh umat Muslim yang sedang naik Haji.</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Hari Idul Adha adalah puncaknya ibadah Haji yang dilaksanakan umat Muslim. Terkadang Idul Adha disebut pula sebagai Idul Qurban atau Lebaran Haji.</p>
    </div>
    <div class="clear"></div>
    <br/>

    <div class="boxprofile white">
        <h4 class="block_red text-center"><a id="profil" style="color: white;">PENETAPAN IDUL ADHA</a></h4>
        <img src="http://static.republika.co.id/uploads/images/detailnews/jamaah-haji-wukuf-di-padang-arafah-makkah-arab-saudi-_140912095033-724.jpg" class="picprofil"><br>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Umat Islam meyakini, bahwa pilar dan inti dari ibadah Haji adalah wukuf di Arafah, sementara Hari Arafah itu sendiri adalah hari ketika jamaah haji di tanah suci sedang melakukan wukuf di Arafah, sebagaimana sabda Nabi saw. :</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">"Ibadah haji adalah (wukuf) di Arafah." HR At Tirmidzi, Ibnu Majah, Al Baihaqi, ad Daruquthni, Ahmad, dan al Hakim. Al Hakim berkomentar, “Hadits ini sahih, sekalipun dia berdua [Bukhari-Muslim] tidak mengeluarkannya”.</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Dalam hadits yang dituturkan oleh Husain bin al-Harits al-Jadali berkata, bahwa amir Makkah pernah menyampaikan khutbah, kemudian berkata :</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">"Rasulullah saw. telah berpesan kepada kami agar kami menunaikan ibadah haji berdasarkan ru’yat (hilal Dzulhijjah). Jika kami tidak bisa menyaksikannya, kemudian ada dua saksi adil (yang menyaksikannya), maka kami harus mengerjakan manasik berdasarkan kesaksian mereka." HR Abu Dawud, al Baihaqi dan ad Daruquthni. Ad Daruquthni berkomentar, “Hadits ini isnadnya bersambung, dan sahih.”</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Hadits ini menjelaskan: Pertama, bahwa pelaksanaan ibadah haji harus didasarkan pada hasil ru’yat hilal 1 Dzulhijjah, sehingga kapan wukuf dan Idul Adhanya bisa ditetapkan. Kedua, pesan Nabi kepada amir Makkah, sebagai penguasa wilayah, tempat di mana perhelatan haji dilaksanakan untuk melakukan ru’yat; jika tidak berhasil, maka ru’yat orang lain, yang menyatakan kesaksiannya kepada amir Makkah.</p>
        <img src="http://2.bp.blogspot.com/-qD54cPYYP3c/VGBGXBFkk2I/AAAAAAAABck/N9y8T4p-b6M/s1600/Kalender%2BSeptember%2B2015.png" class="picprofil"><br>
    </div>
    <div class="clear"></div>
    <br/>

    <!-- h4 class="list" style="margin-left: 20px;margin-top: 25px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
    <div style="margin-left: 20px;background-color: #E5E5E5;">
        <div id="newskampungpulo_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
        <div class="row-fluid" style="margin-bottom: 2px;">
            <div class="span6 text-left">
                <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
    <!-- /div>
    <div class="span6 text-right">
        <a id="newskampungpulo_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
        <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
    <!--/div>
</div>
</div -->
</div>
<div class="clear"></div>

<div class="col_full">
    <div class="garis"></div>

    <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
    <div>
        <ul style="margin-left: 0px;">
            <li class="organisasi">
                <img src="http://bptsp.jakarta.go.id/logo_dki.png" data-toggle="tooltip" data-original-title="PEMPROV DKI JAKARTA"/>
                <p>PEMPROV DKI JAKARTA</p>
            </li>
        </ul>

    </div>

    <div class="clear"></div><h4 class="list"><a id="penentang" style="color: black;">INSTITUSI PENENTANG</a></h4>
    <div>
        <ul style="margin-left: 0px;">
            <li class="organisasi">
                <a href="http://www.bijaks.net/aktor/profile/majelisulamaindonesia53097864039ce">
                   <img src="http://4.bp.blogspot.com/-kVfvrWrUgsU/VRvZS2wb5gI/AAAAAAAAAEE/2WikG6tgBPI/s1600/logo%2Bmui.png" data-toggle="tooltip" data-original-title="MUI"/>
                   <p>MUI</p>
                </a>
            </li>
            <li class="organisasi">
                <a href="http://www.bijaks.net/aktor/profile/fpi54d982a78bedb">
                   <img src="http://1.bp.blogspot.com/-bE1R-B3go1E/UCYkigD6zWI/AAAAAAAABBY/xMe9GHn5pxk/s1600/FPI.png" data-toggle="tooltip" data-original-title="FPI"/>
                   <p>FPI</p>
                </a>
            </li>
            <li class="organisasi">
                <a href="http://www.bijaks.net/aktor/profile/pengurusbesarnahdatululamapbnu551cb9cd17c97">
                   <img src="https://justnurman.files.wordpress.com/2010/04/logo-nu.jpg" data-toggle="tooltip" data-original-title="PBNU"/>
                   <p>PBNU</p>
                </a>
            </li>
            <li class="organisasi">
                <a href="http://www.bijaks.net/aktor/profile/hizbuttahririndonesiahti54d01c454d064">
                   <img src="http://img1.eramuslim.com/fckfiles/image/info_umat/htilogo.jpg" data-toggle="tooltip" data-original-title="HTI (Hizbut Tahrir Indonesia)"/>
                   <p>HTI (Hizbut Tahrir Indonesia)</p>
                </a>
            </li>
            <li class="organisasi">
                <a href="http://www.bijaks.net/aktor/profile/lembagabantuanhukumlbh53167fe28e225">
                   <img src="http://lbh-semarang.or.id/wp-content/uploads/2014/10/Logo-LBH-SMG-2007_1.jpg" data-toggle="tooltip" data-original-title="LBH (Lembaga Bantuan Hukum)"/>
                   <p>LBH (Lembaga Bantuan Hukum)</p>
                </a>
            </li>
            <li class="organisasi">
                <img src="http://2.bp.blogspot.com/-KmFn3XbqR24/TZdX7b0DgHI/AAAAAAAAAA0/lYumr90kT0U/s760/logo%2Bfbr.jpeg" data-toggle="tooltip" data-original-title="FBR (Forum Betawi Rempug)"/>
                <p>FBR (Forum Betawi Rempug)</p>
            </li>
            <li class="organisasi">
                <img src="http://lh5.googleusercontent.com/-Ftcze-QoIiI/AAAAAAAAAAI/AAAAAAAAAE4/vX5IC-SgeI0/photo.jpg" data-toggle="tooltip" data-original-title="GP Ansor Jakarta"/>
                <p>GP Ansor Jakarta</p>
            </li>
            <li class="organisasi">
                <img src="http://4.bp.blogspot.com/_L64nMBrys-g/Sm6qHZyO6jI/AAAAAAAAAI8/x7Xk6jA1GTA/s400/IKADI.jpg" data-toggle="tooltip" data-original-title="Ikatan Dai Indonesia (Ikatan Dai Indonesia)"/>
                <p>Ikatan Dai Indonesia (Ikatan Dai Indonesia)</p>
            </li>

        </ul>
    </div>


</div>
<div class="clear"></div><br><div class="garis"></div>

<div class="col_full">
    <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENENTANG</a></h4>
    <div class="clear"></div>
    <ul style="margin-left: 0px;margin-top: 10px;">
        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
            <?php
            for($i=0;$i<=6;$i++){
                ?>
                <div style="width: 100%;height: auto;display: inline-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 77%;margin-right: 10px;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div style="float: left;width: 47%;margin-left: 20px;">
            <?php
            for($i=7;$i<=12;$i++){
                ?>
                <div style="width: 100%;height: auto;display: inline-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 78%;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </ul>
</div>
<div class="clear"></div><br><div class="garis"></div>

<h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
<div class="col_full" style="display: inline-block;">
    <ul id="light-gallery" class="gallery" style="margin-left: 0px;margin-top: 0px;">
        <?php
        foreach ($data['FOTO'] as $key => $val) { ?>

            <li data-src="<?php echo $val['img'];?>" style="overflow: hidden;">
                <a href="#">
                    <img src="<?php echo $val['img'];?>" />
                </a>
            </li>
        <?php
        }
        ?>
    </ul>
</div>

<div class="clear"></div>
<h4 class="list" style="margin-top: 10px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>
<div class="col_full">
    <div class="boxgray" style="height: 330px;">
        <ul style="margin-left: 15px;margin-top: 10px;">
            <?php
            foreach ($data['VIDEO'] as $key => $val) { ?>
                <li class="video">
                    <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                        <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                    </a>
                </li>
                <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                            </div>
                            <div class="modal-body">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </ul>
    </div>
</div>
<div class="clear"></div>

</div>
</div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>