<?php
$data = [
    'NARASI'=>[
        'title'=>'POLITIK ATAS BAWAH ALA BARESKRIM',
        'narasi'=>'<img src="http://img.bisnis.com/posts/2015/09/04/468961/buwas.jpg" class="pic"><p>Kepala Kepolisian RI Jenderal (Pol) Badrodin Haiti pada Kamis (3/9/2015) malam memastikan ada pergeseran posisi antara Komjen. Budi Waseso dan Komjen Anang Iskandar. Budi Waseso yang sebelumnya menjabat sebagai Kepala Badan Reserse dan Kriminal Polri menjadi Kepala Badan Narkotika Nasional, sementara Anang Iskandar yang sebelumnya menjabat sebagai Kepala Badan Narkotika Nasional menjadi Kepala Badan Reserse dan Kriminal Polri.</p>
                   <p>Isu Soal Pencopotan Budi Waseso sebagai Kabareskrim Polri berawal dari pernyataan Menteri Koordinator Politik, Hukum dan Keamanan Luhut Binsar Pandjaitan, Selasa (1/9/2015), yang menyebutkan akan ada pejabat yang dicopot karena sering membuat gaduh sehingga berdampak pada kondisi ekonomi. meski tak menyebut siapa yang dimaksud, dugaan langsung mengarah pada sosok Budi Waseso.</p>
                   <p><img src="http://cdn.tmpo.co/data/2015/01/17/id_361826/361826_620.jpg" class="pic2">Seperti diketahui sebelumnya Budi Waseso selama menjabat sebagai Kabareskrim Polri banyak menangani kasus-kasus besar yang cukup menarik perhatian publik diantaranya : kasus saksi palsu yang menjerat Wakil Ketua KPK non-aktif Bambang Widjojanto, kasus pemalsuan KTP yang menjerat Katua KPK non-aktif Abraham Samad, kasus dugaan korupsi paspor elektronik (payment gateway) di Kemenkumham yang menjerat mantan Wamenkumham Denny Indrayana, kasus lawas yang menjerat penyidik KPK Novel Baswedan, kasus dugaan korupsi pengadaan alat cadangan listrik Uninterruptible Power Suplay (UPS) di Lingkungan Pemprov DKI Jakarta, kasus pengadaan printer scanner di lingkuungan Pemprov DKI Jakarta, kasus korupsi kakap dalam penjualan kondensat (minyak mentah) dari Satuan Khusus Pelaksana Kegiatan Usaha Hulu, Minyak dan Gas Bumi (SKK Migas) ke PT. Trans Pacific Pethrohimecal Indotama, kasus dugaan korupsi proyek pengadaan mobile crane PT. Pelindo II, dan beberapa kasus lainnya.</p>
                   <p>Selama tujuh bulan menjabat Kabareskrim, langkah penegakan hukum yang dilakukan kepolisian beberapa kali mengundang pro dan kontra serta dianggap menimbulkan kegaduhan. namun, benarkah Budi Waseso penyebab dari kegaduhan tersebut ?.</p>'
    ],
    'ANALISA'=>[
        'narasi'=>'<img src="http://cdnimage.terbitsport.com/imagebank/gallery/large/20150529_072725_harianterbit_buwas.jpeg" class="pic"><p>Sejak Komjen. Pol Budi Waseso menduduki jabatan Kabareskrim Polri, dirinya dikenal sangat kontroversial, keberaniannya boleh dikatakan luar biasa. Sayangnya Buwas dinilai sering menabrak rambu-rambu etika sebagai pemimpin. Langkah-langkah Buwas dinilai terlalu berani dan terlalu dominan sehingga terkesan mendikte Kapolri.</p>
                   <p>Selama ini banyak orang bertanya dan menduga-duga apa sebenarnya posisi Buwas di lingkaran elit politik negeri ini. Banyak orang bingung dengan keberanian Buwas, bahkan beberapa kali terjadi seolah-olah Buwas menantang kebijakan Presiden. Saat Buwas menjabat Kabareskrim tentulah ada fungsi dan tanggung jawab yang diembannya, kalau menurut fungsinya Bareskrim Polri bisa dibilang jantung dari Kepolisian Negara Republik Indonesia. karena tugas utama Polri adalah penegakan hukum maka semua kasus-kasu kriminal merupakan wewenang dari Bareskrim.</p>
                   <p>Semua tugas utama dari kepolisian ada dibagian ini. Hal itulah yang membuat posisi Kabareskrim sangat vital bahkan bisa dibilang Jenderal Lapangan sebenarnya dari Polri adalah Kabareskrim. Makanya tidak heran selama ini mayoritas petinggi Polri yang menjadi Kapolri sebelumnya sempat menjabat Kabareskrim. Dominannya posisi Kabareskrim sudah nampak pada pejabat-pejabat sebelumnya. Sutarman sewaktu masih Komjen pernah mendominasi Timur Pradopo sebagai Kapolri, begitu juga dengan Susno Duadji. Fenomena itu sudah terjadi tetapi yang paling mendominasi dan sangat agresif adalah ketika Kabareskrim ditangani oleh Budi Waseso.</p>
                   <p>Terkait dengan keberanian Buwas dalam usahanya untuk mengungkap kasus-kasus besar mungkin karena dirinya memiliki kedekatan hubungan dengan para elit di negeri ini. Tetapi disisi lain karakter Buwas memang sangat berani dan agresif.</p>
                   <p><img src="http://citraindonesia.com/wp-content/uploads/2015/02/budi-waseso-kriminal-foto.merdeka-670.jpg" class="pic2">Namun pertanyan kemudian, apa kesalahan fatal dari Buwas sehingga dirinya dicopot dari jabatannya ?. Tentu jawaban yang paling gampang adalah dimana beberapa waktu terakhir ini Buwas dinilai menimbulkan kehebohan yang bagi sebagaian kalangan dianggap membuat gaduh.</p>
                   <p>Pada saat polemik KPK Vs Polri Buwas melakukan banyak langkah-langkah kontroversial, diantaranya : Mengkasuskan semua pimpinan KPK dan khususnya mentersangkutkan Abraham Samad dan Bambang Widjojanto, Mengkasuskan Deny Indrayana, mensomasi Komnas HAM dan menakut-nakuti semua pendukung KPK, membela hakim Sarpin yang mengkasuskan Ketua Komisi Yudisial, langkah ini memperkuat dugaan banyak orang bahwa hakim Sarpin dikendalikan orang kuat di Kepolisian, berusaha keras untuk menyaingi kinerja KPK dalam pemberantasan korupsi, ada kasus-kasus besar yang langsung mentersangkakan orang seperti kasus GOR Bandung dan kasus Kondensat yang disebut-sebut Bareskrim merugikan negara hingga trilyunan rupiah.</p>
                   <p>Selain langkah-langkah kontroversial tersebut, Budi Waseso sepertinya berusaha keras mengangkat citra positif Polri yang selama ini sudah jatuh di mata masyarakat. Sayangnya langkah-langkah tersebut dinilai banyak kekeliruannya. Betul bahwa Buwas berprestasi untuk beberapa kasus yang tidak diprioritaskan Polri seperti beberapa kasus korupsi, pelanggaran hak cipta dan lain-lainnya. tetapi beberapa langkah Buwas tersebut dianggap banyak berlebihan. Contohnya : mengurusi dan membesar-besarkan kasus-kasus sepele seperti prostitusi artis dan lain-lain sebagainya. disini Buwas terkesan sepertinya menggunakan media untuk memblow-up kasusnya. begitu juga dengan beberapa kasus lainnya, Buwas sering terlalu cepat menjadikan orang sebagai tersangka. Apalagi kalaua melakukan penggeledahan dan penyidikan Buwas sering mengajak wartawan untuk menyaksikannya. Mungkin tujuan Buwas untuk mengangkat citra Polri tetapi sayangnya hal itu terlalu instan.</p>
                   <p>Buwas dinilai sering mencampuri urusan orang lain. Contohnya kasus Sarpin dengan KY. Hal itu juga memicu banyak orang memanfaatkan Buwas untuk kepentingan mereka. Contohnya ARB dan Bambang Susatyo sering memanfaatkan Bareskrim untuk menakut-nakuti kubu Agung Laksono. OC Kaligis juga demikian, bahkan Ahok pun demikian. Contoh lain Buwas ikut campur urusan yang terakhir adalah Buwas mencampuri urusan Pansel KPK. Bahkan informasinya Buwas mengancam akan memidanakan Pansel KPK bila meloloskan Capim yang memiliki kasus hukum sesuai dengan stabilo merah dari Kabareskrim.</p>
                   <p>Dan terakhir Buwas membuat kehebohan di Pelabuhan Tanjung Priuk. Buwas selama ini memang tidak peduli dengan siapa yang menguasai ini siapa yang menguasai itu. Salah satu contohnya yaitu kasus Kondensat dimana sebenarnya banyak orang penting berada dilingkaran kasus itu. Kasus Kondensat itu mirip seperti kasus Century yang sangat pelik, tapi oleh Buwas dianggap kasus biasa dan langsung diblow-up.</p>
                   <p>Sementara untuk kasus 10 Crane di pelabuhan itu adalah kasus yang sebenarnya bisa dikatakan bukan kasus. Posisi pelabuhan adalah posisi vital dan menyangkut banyak kepentingan, baik kepentingan ekonomi Indonesia maupun kepentingan segelintir orang kuat. Sebenarnya kasus 10 crane itu sudah diselesaikan secara internal dan melibatkan beberapa institusi.</p>'
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="http://static.republika.co.id/uploads/images/detailnews/tampilan-petisi-copot-kabareskrim-budi-waseso-yang-terpajang-di-_150716092852-384.jpg" class="pic"><p>Pencopotan Komjen. Pol. Budi Waseso dari jabatan Kepala Badan Reserse dan Kriminal Polri seakan menggema kemana-mana, bahkan menimbulkan pro dan kontra, sebagaimana yang diketahui "Buwas" sapaan akrab Budi Waseso saat dirinya  menjabat sebagai Kabareskrim Polri dinilai membuat kegaduhan dengan aktifnya Kepolisian menangani beberapa kasus yang menarik perhatian publik, salah satunya pihak yang pro atas pencopotan Buwas adalah Buya Syafii Maarif. Mantan Ketua Umum PP Muhammadiyah ini menuding Budi telah melakukan kriminalisasi terhadap pimpinan KPK saat mengangkat kasus yang melibatkan Ketua KPK non-aktif Abraham Samad dan Wakil Ketua KPK non-aktif Bambang Widjojanto ditambah lagi salah satu penyidik KPK Novel Baswedan.</p>
                   <p>"Budi Waseso adalah sumber masalah yang melakukan segala bentuk kriminalisasi terhadap pimpinan Komisi Pemberantasan Korupsi, semua ini kelakuan dia," tegas Syafii. Dukungan tersebut juga datang dari Koordinator ICW Febri Hendri, dirinya menyatakan bahwa pergantian Budi Waseso tersebut merupakan langkah tepat. Menurut Febri, selama ini Buwas telah melakukan kriminalisasi kepada aktivis anti korupsi dan pimpinan KPK. "Kami setuju, itu sudah tepat. Buwas sudah banyak melakukan kriminalisasi aktivis anti korupsi dan Pimpinan KPK," kata Febri di Jakarta, Kamis (3/9/2015).</p>
                   <p><img src="http://www.rmol.co/images/berita/normal/881556_11431017022015_buya_2.jpg" class="pic2">Febri juga sempat menghubungkan sikap Buwas dengan rendahnya serapan APBN 2015. "selama Buwas menjabat sebagai Kabareskrim telah menyebabkan rendahnya serapan APBN. sebab, penegakan hukum yang dilakukan semata-mata hanya mencari kesalahan," katanya.</p>
                   <p>Sebelumnya pada bulan Juli 2015 petisi yang meminta Presiden Joko Widodo (Jokowi) mencopot Kepala Badan Reserse dan Kriminal Polri Komjen. Pol. Budi Waseso bergulir. Petisi berjudul Pergantian Kabareskrim Polri ini diluncurkan di Change.org, Rabu (15/7/2015).</p>
                   <p><img src="http://antikorupsi.info/sites/antikorupsi.org/files/styles/max-600/public/image/Wawancara/64993_peneliti_icw__febri_diansyah_663_382.jpg?itok=8kuYnUEi&slideshow=true&slideshowAuto=false&slideshowSpeed=4000&speed=350&transition=elastic" class="pic">Selain pihak yang pro atas pencopotan Buwas dari kursi Kabareskrim ada juga pihak yang kontra atau menolak atas peristiwa tersebut, diantaranya Anggota Komisi IX DPR RI dari Fraksi PDIP Rieke Diah Pitaloka yang mengatakan bahwa isu tentang pencopotan Komjen. Pol. Budi Waseso sebagai Kabareskrim sungguh mengejutkan.</p>
                   <p>Bagaimana tidak, lanjut Rieke, disaat Buwas mampu mengungkap kasus-kasus korupsi besar seperti dweling time, Pertamina dan penimbunan daging sapi, justru kini karirnya terancam.</p>
                   <p>Ditambah lagi bagi Rieke, pencopotan Buwas cukup menggelikan karena dianggap mengancam situasi ekonomi. Padahal, secara logika hubungan penegakan hukum dengan ekonomi sangat jauh berbeda.</p>
                   <p><img src="http://rmol.co/images/berita/normal/838574_10095128042015_rieke.jpg" class="pic2">"Apa hubungannya tindakan Buwas mengungkap kasus-kasus korupsi dengan ketidakstabilan ekonomi yang sedang terjadi sekarang," Tukasnya.</p>
                   <p>Rieke pernah mengatakan sebelumnya "atau jangan-jangan isu pencopotan Buwas memang dikembangkan oleh segelintir orang dan kelompoknya yang merasa stabilitas ekonominya terganggu karena sepak terjang Buwas yang "buas" mengungkap kasus korupsi. Termasuk yang terjadi di BUMN PT. Pelindo II," katanya.</p>
                   <p><img src="http://rmol.co/images/berita/normal/168521_12163808092015_Fahri_Hamzah3.jpg" class="pic">Pencopotan Budi Waseso juga dinilai sebagai suatu kebijakan yang tidak memperbolehkannya untuk melakukan pemberantasan korupsi, seperti yang diungkapkan Politikus Partai Keadilan Sejahtera (PKS) Fahri Hamzah, "kenapa Jenderal Budi Waseso tidak boleh menagkap koruptor ?," kata Wakil Ketua DPR RI ini, melalui akun Twitter pribadinya, kamis (3/9/2015).</p>
                   <p>Ia juga mengkritik alasan pencopotan Buwas karena dianggap bikin kegaduhan politik. Padahal, mereka tidak pernah bilang KPK bising ketika kampanye anti korupsi yang dilakukannya memenuhi headline pemberitaan siang malam. "12 Tahun KPK bikin headline setiap pagi siang malam tak pernah dituduh bising...kenapa #POLRI?," katanya.</p>
                   <p><img src="http://i.ytimg.com/vi/NLVUzrGi3tQ/0.jpg" class="pic">Pasca resminya Komjen. Pol Budi Waseso dicopot dari jabatan Kabareskrim muncul petisi baru yang berisi dukungan kepada Kepala Badan Reserse dan Kriminal Polri itu. Petisi berjudul "Dukung Kabareskrim Budi Waseso Menegakkan Hukum Tanpa Tebang Pilih" itu diantaranya digagas ketua Presidium Indonesia Police Watch (IPW) Neta S. Pane.</p>
                   <p>Hingga saat ini, petisi tersebut telah ditanda tangani sebanyak 3.393 orang. targetnya petisi ini mendapat partisipan 5.000 tanda tangan. Bila sudah mencapi target, petisi akan diserahkan ke Presiden Joko Widodo.</p>'
    ],

    'KASUS'=>[
        'narasi'=>'<img src="http://fajar.co.id/wp-content/uploads/2015/08/Pelindo_II.jpg" class="pic"><p>Kasus Pelindo II bermula dari adanya laporan yang masuk ke Bareskrim Polri dengan nomor LP-A/1000/VIII/2015/Bareskrim tanggal 27 Agustus 2015. Diketahui pada 2012, Pelindo II membeli 10 mobile crane senilai Rp. 45 Miliar untuk mendukung kegiatan operasional di 8 pelabuhan cabang Pelindo yaitu di Bengkulu, Teluk Bayur, Palembang, Banten, Jambi dan Cirebon.</p>
                   <p>Pembelian tersebut melibatkan pihak kedua yakni Guangshi Narasi Century Equipment Co. Proses pembelian ini menggunakan anggaran Pelindo II tahun 2012.</p>
                   <p>Proses pengadaan Mobile Crane tersebut diduga tidak melalui prosedur dengan menunjuk langsung pemenang tender. Selain itu, Pelindo juga tidak menggunakan analisis kebutuhan barang atas investasi untuk mendukung kegiatan bisnisnya.</p>
                   <p>10 Mobile Crane yang diterima Pelindo sejak tahun 2013 hingga kini belum bisa dioperasikan dan hanya mangkrak di pelabuhan Tanjung Priok.</p>
                   <p><img src="http://cdn.rimanews.com/bank/antarafoto-pengembangan-terminal-peti-kemas-pelindo-080115-sp-1.jpg" class="pic2">Dalam pengembangan pada Jumat (28/8/2015) penyidik telah menggeledah kantor PT. Pelabuhan Indonesia (Pelindo) II di Tanjung Priok, Jakarta Utara.</p>
                   <p>Dari Penggeledahan yang melibatkan Bareskrim, Polda Metro dan Polres Pelabuhan Tanjung Priok ini polisi menyita 26 dokumen dari ruangan Direktur Utama PT. Pelindo II, RJ Lino. Selain itu, 10 Mobile Crane juga sudah dipolice line.</p>
                   <p>Kapolri Jenderal Polisi Badrodin Haiti mengakui dihubungi Menteri Negara Badan Usaha Milik Negara (BUMN), Rini Soemarno, melalui sambungan telepon terkait penggeledahan kantor PT. Pelindo II, Jumat (28/8/2015).</p>
                   <p>Badrodin juga mengaku dirinya telah berkoordinasi dengan sejumlah kementrian terkait penggeledahan yang sempat dihalangi oleh Dirut PT. Pelindo II, RJ Lino.</p>'
    ],

    'WIBAWA'=>[
        'narasi'=>'<img src="http://images.detik.com/customthumb/2015/02/27/10/komjenbuwas.jpg?w=600" class="pic"><p class="font_kecil">Perihal pencopotan Komjen (Pol) Budi Waseso dari Jabatan Kabareskrim Polri adalah hal yang bisa dianggap biasa saja, namun jika dikait-kaitkan dengan beberapa kasus yang diangkat saat dirinya menjabat sebagai Kabareskrim maka ada kemungkinan pencopotan Buwas adalah upaya politisasi yang dilakukan untuk mengamankan beberapa kepentingan yang tersembunyi dibalik kasus-kasus tersebut.</p>
                   <p class="font_kecil">Terkait dengan hal ini Politisi PDIP Tubagus Hasanuddin memberikan komentar bahwa dalam perspektif hukum, bila penyidik melakukan kesalahan maka bisa saja diselesaikan lewat jalur hukum seperti PTUN, praperadilan dll. Namun, ketika pemerintah menghentikan penyidikan apalagi dengan mencopot pimpinan tertinggi di Bareskrim Polri, maka sesungguhnya bisa dikatakan pemerintah telah melakukan intervensi terhadap penyidik.</p>
                   <img src="http://www.indopos.co.id/wp-content/uploads/2014/03/Tubagus-Hasanuddin-Anggota-DPR-RI-Fraksi-PDIP.jpg" class="pic2"><p class="font_kecil">Dalam konteks ini dua hal dapat terjadi, kemungkinan pertama pemerintah memang lemah terhadap tekanan dari kiri kanan yang meminta agar penyidikan kasus-kasus itu dihentikan. Yang kedua, bisa jadi pemerintah memang terlibat dalam kasus tersebut dan berharap kasus tersebut dihentikan salah satunya dengan cara mencopot Buwas dari posisi Kabareskrim.</p>
                   <p class="font_kecil">Maka berangkat dari dua kemungkinan itu peran pemerintah dianggap buruk bagi penegakan hukum di Indonesia. Dan pastinya kewibawaan presiden pun bisa anjlok.</p>
                   <img src="http://cdn-2.tstatic.net/bangka/foto/bank/images/jokowi-badrodinh.jpg" class="pic"><p class="font_kecil">Sementara itu Komjen Budi Waseso menegaskan meski dirinya telah dimutasi menjadi Kepala BNN dia akan tetap mengawal kasus-kasus yang ditangani Bareskrim. Buwas mengaku merasa ada beban moril karena meninggalkan beberapa kasus yang belum rampung ditangani Bareskrim. “karena kalu ada hambatan, saya yang bertanggung jawab,” kata Buwas di kantornya, Jumat (4/9/2015).</p>
                   <p class="font_kecil">Seperti yang diketahui sejak Bareskrim dipimpin Buwas pada Januari lalu, kasus-kasus yang ditangani Bareskrim kerap dinilai kontroversial. Bahkan, khususnya kasus yang menjerat para Pimpinan KPK dinilai oleh sejumlah kalangan sebagai aksi balas dendam kepolisian atas penetapan Budi Gunawan sebagai tersangka atas kasus dugaan korupsi di KPK.</p>'
    ],
    'MUTASI'=>[
        'narasi'=>'<img src="http://static.cikalnews.com/size/620x380/r/data/berita/foto/besar/27921619499budi-waseso-jadi-ketua-bnn.jpg" class="pic"><p class="font_kecil">Belum sempat menyelesaikan pekerjaan rumahnya, Budi kini dicopot dan dimutasi menjadi Kepala Badan Narkotika Nasional. Budi bertukar jabatan dengan Komisaris Jenderal Anang Iskandar yang didapuk menggantikan dirinya sebagai Kabareskrim.</p>
                   <p class="font_kecil">Sebelum Budi resmi dicopot, Menteri Koordinator Bidang Politik Hukum dan Keamanan Luhut Binsar Pandjaitan mengatakan, pemberantasan korupsi mesti dilakukan tanpa membuat gaduh. Luhut juga pernah mengingatkan Polri untuk tidak sembarangan dan lebih berhati-hati menetapkan seseorang menjadi tersangka kasus ekonomi.</p>
                   <p class="font_kecil">Kegaduhan memang terjadi setelah Direktur Utama PT Pelabuhan Indonesia II RJ Lino mengadu kepada Menteri Perencanaan Pembangunan Nasional/Kepala Bappenas Sofyan Djalil di muka publik. Peristiwa itu sampai ke telinga Istana dan dikaitkan dengan isu pencopotan Budi.</p>
                   <p class="font_kecil">Mutasi Budi dari Kabareskrim memang disambut baik sejumlah pihak. Pencopotan mantan Kepala Polda Gorontalo itu dari jabatan strategis disebut sebagai akhir dari era ‘kriminalisasi’. Dengan segala kontroversi selama Budi menjabat sebagai orang nomor satu di Bareskrim, pendapat itu menurut saya sah-sah saja meski kepentingan politik juga membayangi jabatan yang diemban Budi.</p>
                   <p class="font_kecil">Atas sejumlah proses penyidikan yang tengah berlangsung di Bareskrim Polri, ada geliat perbaikan kinerja dan semangat para penyidik di bawah kepemimpinan Budi. Direktur Tindak Pidana Ekonomi dan Khusus Brigadir Jenderal Victor Simanjuntak, salah satu ujung tombak Budi dalam mengungkap kasus korupsi besar, mengancam mengundurkan diri jika atasannya dicopot.</p>
                   <p class="font_kecil">Victor menilai ada intervensi pihak luar yang menghendaki Budi ditarik dari jabatan. Intervensi yang muncul tidak terlepas dari penanganan kasus yang belakangan dilakukan tim penyidik Bareskrim. "Kalau Pak Buwas benar dicopot, seluruh penyidik Polri mentalnya bisa terganggu," kata Victor.</p>
                   <p class="font_kecil">Di luar pernyataan dan tindakan Budi yang memancing kontroversi, ada hal positif dari kepemimpinan Budi. Namun keputusan pimpinan Polri kini sudah bulat, Budi resmi meninggalkan kursi yang dia duduki selama delapan bulan. Semoga kepentingan politik dan keuntungan pihak tertentu tidak membayangi keputusan ini.</p>'
    ],
    'INTERVENSI'=>[
        'narasi'=>'<img src="http://cdn.metrotvnews.com/dynamic/content/2015/05/05/122819/6eKLeLqbMe.jpg?w=668" class="pic"><p class="font_kecil">Adanya kabar Wakil Presiden Jusuf Kalla menambah kehebohan publik dan mengundang berbagai macam pertanyaan, apa maksud JK yang terkesan mengintervensi Budi Waseso saat pihak kepolisian menyorot PT. Pelindo II?</p>
                   <p class="font_kecil">Komjen. Budi Waseso, yang baru saja melepas jabatan sebagai Kabareskrim Polri, membenarkan bahwa dirinya pernah ditelepon Wapres Jusuf Kalla. Menurut Budi, Kalla meminta dirinya agar tak memidanakan kebijakan BUMN. Menanggapi permintaan Kalla, Budi meminta agar Wapres tak langsung mengambil kesimpulan. “saya bilang, ini penegakan hukum, Pak. Baru proses awal,: kata Budi.</p>
                   <p class="font_kecil">Budi meminta Kalla membiarkan penyidikan kasus ini berjalan dan menyerahkan pengawasan kasus pada dirinya. “saya akan mengawasi ini dan Bapak awasi saya,” demikian Budi mengulang ucapannya pada JK.</p>
                   <p class="font_kecil">Saat berbicara dengan JK, Budi juga meminta agar tak buru-buru menyimpulkan karena yang dia lakukan justru akan menyelamatkan keuangan negara.</p>'
    ],
    'PROFIL'=>[
        'narasi'=>'Dalam Pasal 263 RUU KUHP ayat 1 yang disiapkan pemerintah disebutkan bahwa "Setiap orang yang di muka umum menghina presiden atau wakil presiden dipidana dengan pidana penjara paling lama 5 (lima) tahun atau pidana denda paling banyak Kategori IV".
                    </p><p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Dalam ayat selanjutnya ditambahkan, "Tidak merupakan penghinaan jika perbuatan sebagaimana dimaksud pada ayat (1) jelas dilakukan untuk kepentingan umum atau pembelaan diri".
                    </p><p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Aturan itu diperluas melalui Pasal 264 RUU KUHP yang berbunyi, "Setiap orang yang menyiarkan, mempertunjukkan, atau menempelkan tulisan atau gambar sehingga terlihat oleh umum, atau memperdengarkan rekaman sehingga terdengar oleh umum, yang berisi penghinaan terhadap Presiden atau Wakil Presiden dengan maksud agar isi penghinaan diketahui atau lebih diketahui umum, dipidana dengan pidana penjara paling lama lima tahun atau pidana denda paling banyak Kategori IV.”',
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Farouk Muhammad','jabatan'=>'','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2014--12--29973-wakil-ketua-dpd-ri-farouk-muhammad-amandemen-uud-1945-dekati-dpr.jpg','url'=>'#','content'=>'"Itu hal yang biasa. Ya berarti kedua belah pihak tinggal saling melanjutkan saja apa yang menjadi tanggung jawab masing masing,"-Komjen (Purn) Farouk Muhammad'],
        ['from'=>'Buya Syafii Maarif','jabatan'=>'','img'=>'http://rmol.co/images/berita/normal/841465_08225317022015_syafii_maarif.jpg','url'=>'#','content'=>'"‎Saya berharap dia bekerja lebih baik dan profesional, dan tidak menimbulkan huru hara lagi, Namun dengan pergantian itu, semoga di tempat yang baru nanti dia jadi lebih baik,"-  Syafii Maarif. Mantan Ketua Umum PP Muhammadiyah'],
        ['from'=>'Edhy Baskoro Yudhoyono','jabatan'=>'Ketua Partai Demokrat','img'=>'http://www.wartabuana.com/pictures/ibasdettt-201401110928121.jpg','url'=>'#','content'=>'"Bedanya dengan pemerintah yang dulu, tidak ada kegaduhan media. Polri sekali lagi tidak boleh berpolitik. Jadi kalau ingin mendapatkan karier yang lebih baik kita bekerja secara profesional,"  Edhie Baskoro Yudhoyono -  Ketua Fraksi Demokrat'],
        ['from'=>'Febri Henri','jabatan'=>'','img'=>'http://www.jpnn.com/picture/watermark/20140319_122145/122145_23482_Febri_Hendri_ICW_bsr.jpg','url'=>'#','content'=>'"Kami setuju, itu sudah tepat. Buwas sudah banyak melakukan kriminalisasi aktivis antikorupsi dan Pimpinan KPK," Febri Henri _Koordinator Investigasi ICW'],
        ['from'=>'Jusuf Kalla','jabatan'=>'Wakil Presiden RI','img'=>'http://jusufkalla.info/wp-content/uploads/2013/06/jusuf-kalla2.jpg','url'=>'#','content'=>'"Kepolisian bagian dari pemerintah, pemerintah itu dipimpin presiden dan tentu saya wakilnya, ya kalau berbicara polisi ya berarti bukan intervensi dong.... Tidak ada itu istilah intervensi kalau presiden berbicara tentang aparat di bawah presiden itu bukan intervensi, tidak ada itu," Yusuf Kalla_ Wapres'],
        ['from'=>'Haris Azhar','jabatan'=>'','img'=>'http://www.matamatra.com/wp-content/uploads/2015/06/kontras.jpg','url'=>'#','content'=>'Direktur Tindak Pidana Ekonomi Khusus Bareskrim Polri Brigjen". Haris Azhar_Koordinator Kontras'],
        ['from'=>'Oce Madril','jabatan'=>'','img'=>'https://upload.wikimedia.org/wikipedia/id/archive/5/52/20141120215936!Oce_Madril.jpg','url'=>'#','content'=>'"Banyak kritik terkait kinerja Bareskrim, baik kasus di pemerintah pusat maupun daerah, sehingga sudah tepat Budi Waseso dipecat," Oce Madril_ Direktur Advokasi UGM']
    ],

    'QUOTE_PENENTANG'=>[
        ['from'=>'Sofyano Zakaria','jabatan'=>'MP3K','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'#','content'=>'“Presiden menurut konstitusi hanya berhak mengganti Kapolri, itu pun atas persetujuan DPR. Sedangkan penunjukan penyidik dan posisi lain di tubuh Polri 100 persen wewenang Kapolri sesuai aturan di internal Polri. Apalagi sekelas wapres maupun Menko, sama sekali tidak berhak,”- Sofyano Zakaria-Koordinator Nasional Masyarakat Peduli Polri Perangi Korupsi (MP3K)‎'],
        ['from'=>'M Nasser','jabatan'=>'Komisioner Kompolnas','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'#','content'=>'"Yang tidak boleh itu mutasi yang didasarkan order dari luar. Ini bisa ganggu kinerja kepolisian," M Nasser- Komisioner Kompolnas‎'],
        ['from'=>'Fahri Hamzah','jabatan'=>'','img'=>'http://www.harianaceh.co/file/2014/08/152934.jpg','url'=>'#','content'=>'"Kalau pak Jokowi yang ngeluh dweling time paling lambat. Ya diselesaikan dong, kok malah intervensi kepolisian," Fahri Hamzah_Wakil Ketua DPR‎'],
        ['from'=>'Rieke Diah Pitaloka','jabatan'=>'','img'=>'http://fajar.co.id/wp-content/uploads/2015/04/Rieke-Diah-Pitaloka.jpg','url'=>'#','content'=>'“Jangan-jangan isu pencopotan Buwas memang dikembangkan oleh segelintir orang dan kelompoknya yang merasa stabilitas ekonominya terganggu karena sepak terjang Buwas yang "buas" mengungkap kasus korupsi? Termasuk yang terjadi di BUMN, PT Pelindo II?" Rieke Diah Pitaloka_ Anggota DPR Fraksi PDIP'],
        ['from'=>'Brigjen Victor Simanjuntak','jabatan'=>'','img'=>'http://www.jokowinomics.com/media/static/images/2015/05/Victor-Simanjuntak-640x359.jpg','url'=>'#','content'=>'“Apa kita mau Bareskrim ini lemah lembut seperti dulu? Tidak kan?” “Berimbas ke bawah dong. Sekarang ini anggota Bareskrim bersemangat bekerja kan karena dipimpin oleh Kabareskrim yang semangat juga. Kalau diganti, semangat di lapangan pun turun,” Victor Simanjuntak_Direktur Tindak Pidana Ekonomi Khusus Bareskrim Polri Brigjen'],
        ['from'=>'Trimedya Pandjaitan','jabatan'=>'','img'=>'http://pekanews.com/wp-content/uploads/2015/01/trimedia.jpg','url'=>'#','content'=>'“"Kalau alasannya menyangkut kinerja, Buwas cukup baik. Itu yang harus dijelaskan, menghambat ekonomi seperti apa. Kaitan RJ Lino kalau ada pidana masa disetop, masa dibiarkan, jadi salah besar kalau mencopot karena alasan menghambat ekonomi," Trimedya Pandjaitan_Wakil Ketua Komisi III DPR'],
        ['from'=>'Wenny Worouw','jabatan'=>'','img'=>'  http://www.dpr.go.id/dokblog/berita-foto/P_20150325_5149.jpg','url'=>'#','content'=>'“"Mungkin (ada) oknum yang ‎terganggu. Bagaimana jika benar Buwas akan memimpin Badan Nasional Penanggulangan Terorisme "Dia orang baik, dia gak cocok di BNPT," Wenny Worouw_ Politisi Gerindra'],
        ['from'=>'Masinton Pasaribu','jabatan'=>'','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'#','content'=>'“"Bukan kita membela institusi-institusi. Jangan sampai terjebak, hanya membela KPK dengan tameng pemberantasan korupsi. Karena apa pun, rencana pencopotan Komjen Buwas ini sudah menimbulkan kegaduhan sendiri,” Masinton Pasaribu_Anggota dewan dari Fraksi PDIP'],
        ['from'=>'Tubagus (TB) Hasanuddin','jabatan'=>'','img'=>' http://www.luwuraya.net/wp-content/uploads/2013/03/Tubagus-Hasanuddin.jpg','url'=>'#','content'=>'“"Kelompok yang pro Buwas dicopot adalah mereka yang saat itu menentang Buwas memerintahkan penyidikan terhadap pimpinan KPK , dan kelompok yang kontra alias menolak Buwas dicopot, karena Buwas dianggap berani membongkar kasus Pelindo 2, penimbun sapi dll. Tapi mayoritas sesungguhnya, tidak setuju Buwas dicopot,"Tubagus (TB) HasanuddinTubagus_ Politisi PDIP']

    ],

'VIDEO'=>[
        ['id'=>'hM8eH0vbthM'],
        ['id'=>'Ylewa5PXpTQ'],
        ['id'=>'yn7M84_xMck'],
        ['id'=>'B_P_UrhkMhQ'],
        ['id'=>'LW6J4QJw7kI'],
        ['id'=>'cQGOE02vj6I'],
        ['id'=>'hfaYHdrexDY'],
        ['id'=>'rYqOhYWS_lk'],
        ['id'=>'eK1iquI4zWM'],
        ['id'=>'IdCQihF4Zxk'],
        ['id'=>'PrX7v9aGds8'],
        ['id'=>'kX0FJisiV20'],
        ['id'=>'BxkHM5EtQCU']
    ],
    'FOTO'=>[
        ['img'=>'http://cdn.img.print.kompas.com/getattachment/3ba190c5-e695-4607-b0b9-9eb291a50405/155670'],
        ['img'=>'http://jakartagreater.com/wp-content/uploads/2015/01/image108.jpg'],
        ['img'=>'http://www.kabarjempol.com/wp-content/uploads/2015/07/Joko-Widodo-Meminta-Komjen-Pol-Budi-Waseso-Untuk-Bersih-Bersih.jpg'],
        ['img'=>'http://fajar.co.id/wp-content/uploads/2015/04/Buwas.jpg'],
        ['img'=>'http://www.dnaberita.com/foto_berita/97Budi.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/kepala-bareskrim-polri-komjen-budi-waseso-_150504144828-616.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/09/03/337/1207952/kapolri-tukar-posisi-kabareskrim-dengan-kepala-bnn-T805emGrIR.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/02/16/403162/kabareskrim-waseso.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/02/27/298499_bambang-widjojanto-tidak-memenuhi-panggilan_663_382.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2015/09/03/id_433547/433547_620.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/976145/big/022921800_1441266554-20150903-Budi-Waseso2.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/02/27/298465_bambang-widjojanto-tidak-memenuhi-panggilan_663_382.jpg'],
        ['img'=>'http://tribratanews.com/wp-content/uploads/2015/07/Budi-Waseso-@-Wahyu-Wening-02-441x294-2.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/939029/big/073544500_1438071877-20150728-Wawancara_khusus_Budi_Waseso-Jakarta-_Budi_Waseso-05.jpg'],
        ['img'=>'http://www.radarpekalongan.com/wp-content/uploads/2015/09/Komjen-Budi-Waseso.jpg'],
        ['img'=>'http://www.harnas.co/files/images/760420/2015/03/04/budi-waseso1425471154.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/939015/big/095145600_1438071448-20150728-Wawancara_khusus_Budi_Waseso-Jakarta-_Budi_Waseso-04.jpg'],
        ['img'=>'http://batampos.co.id/wp-content/uploads/Budi-Waseso.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/09/02/427061/AEoghtZMos.jpg?w=668'],
        ['img'=>'http://www.luwuraya.net/wp-content/uploads/2015/07/Budi-Wasso-antara.jpg'],
        ['img'=>'http://assets-a2.kompasiana.com/items/album/2015/09/02/48339670-4a30-45e0-8df3-60345bea6052-169-55e69b2722afbde310c01206.jpg?t=o&v=760'],
        ['img'=>'http://detik.us/wp-content/plugins/RSSPoster_PRO/cache/ba869_20150216_majalahdetik_168_page_073.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2015/04/14/id_389469/389469_620.jpg'],
        ['img'=>'http://degorontalo.co/wp-content/uploads/2015/02/Kapolda-Gorontalo-Brigjen-Budi-Waseso-500x300.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/04/21/425189/kapolri33.jpg'],
        ['img'=>'http://2.bp.blogspot.com/-nY7qydCVvnc/VLnuCtt84bI/AAAAAAAACCE/qkueL3HinqM/s1600/WASESO-741835.bmp'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/05/11/1527130IMG-20150511-WA0000780x390.jpg'],
        ['img'=>'http://www.dw.com/image/0,,18691199_401,00.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/976386/big/064462700_1441272776-20150903-Budi-Waseso-Jakarta-05.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/806528/big/025127400_1423122649-Budi_waseso_4.jpg']
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
.allpage {
    width: 100%;
    height: auto;
    margin-top: 8px;
}
.col_top {
    background: url('<?php echo base_url(); ?>assets/images/hotpages/polababareskrim/buwas.jpg') no-repeat transparent;
    height: 1352px;
    margin-bottom: -590px;
}
.col_kiri {
    width: 64%;
    height: auto;
    background: transparent;
    float: left;
    padding-right: 2%;
}
.col_kiri p, .col_kiri li, .font_kecil {
    font-size: 13px;
}
.col_kanan {
    width: 34%;
    height: auto;
    background: transparent;
    float: left;
}
.col_kiri2 {
    width: 34%;
    height: auto;
    background: transparent;
    float: left;
    padding-right: 2%;
}
.col_kanan2 {
    width: 64%;
    height: auto;
    background: transparent;
    float: left;
}
.col_kiri50 {
    width: 20%;
    height: auto;
    /*background-color: red;*/
    float: left;
    padding-right: 1%;
}
.col_kanan50 {
    width: 78%;
    height: auto;
    /* background-color: green; */
    float: right;
    padding-left: 1%;
    border-left: 1px dotted black;
    display: inline-table;
}
.col_full {
    width: 100%;
    /*background-color: lightgray;*/
}
.boxprofile {
    box-shadow: -5px 5px 10px gray;
    border-radius: 10px 10px 10px 10px;
    width: 290px;
    margin: 0 auto;
    padding-bottom: 10px;
    background: rgba(0, 0, 0, 0.7);
}
.block_red {
    background-color: #761908;
    border-radius: 10px 10px 0 0;
    padding-top: 5px;
    padding-bottom: 5px;
}
.picprofil {
    width: 80%;
    height: auto;
    margin: 0 auto;
    display: inherit;
}
.pic {
    float: left;
    margin-right: 10px;
    max-width: 200px;
    margin-top: 5px;
}
.pic2 {
    float: right;
    margin-left: 10px;
    max-width: 200px;
    margin-top: 5px;
}
.pic3 {
    margin-right: 10px;
    max-width: 50px;
}
.garis {
    border-top: 1px dotted black;
}
.boxgray {
    width: 99%;
    border: 5px solid lightgray;
    box-shadow: -5px 5px 10px gray;
}
.boxgray_red {
    width: 96%;
    border: 9px solid #a60008;
    border-radius: 8px;
    box-shadow: -5px 5px 10px gray;
}
.boxgray_green {
    width: 96%;
    border: 9px solid #00a651;
    border-radius: 8px;
    box-shadow: -5px 5px 10px gray;
}
.penyokong {
    width: 100%;
    height: auto;
    display: inline-block;
    margin-left: 20px;
}
.boxpenyokong {
    width: 126px;
    height: auto;
}
.foto {
    width: 100px;
    height: 100px;
    border: 3px solid lightgray;
    border-radius: 8px;
    box-shadow: 5px 5px 10px gray;
    padding: 10px 10px;
}
li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 113px;border: solid 4px #c4c4c4;text-align: center}
li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
li.organisasi p {width: 100%;padding: 0px !important;font-size: 8px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 92px;height: 110px;border: solid 4px #c4c4c4;text-align: center}
li.organisasi2 img {width: 98px; height: 70px;  padding: 0px !important;}
li.organisasi2 p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
li.video img {width: 121px;height: auto;padding: 0px !important;}
li.video img:hover {box-shadow: 0px 0px 5px black;}
li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
li.gallery img:hover {box-shadow: 0px 0px 5px black;}
li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

li.dukung {float: left; padding:3px;vertical-align: top;width: 177px;height:115px;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
li.dukung img {width: auto; height: 75px;padding: 0px !important;margin: 0 auto;display: block;}
li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;line-height: 12px;}

.black {
    color: black;
}
.white {
    color: white;
}
.list {
    background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
    padding-left: 30px;
}
.list2 {
    list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
}
p, li {
    text-align: justify;
    font-size: 14px;
}
.clear {
    clear: both;
}
.qpenentang {
    float: left;
    width: 30%;
    height: auto;
    background-color: red;
    display: inline-block;
    border-bottom: 1px solid black;
}
.parodi {
    width: 107px;
    height: 87px;
    float: left;
    margin-right: 10px;
    margin-bottom: 10px;
}
.uprow {margin-left: 20px;margin-top:10px;border-color: #761908 transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
.kronologi {
    width: 260px;
    height: auto;
    margin-left: 20px;
    margin-top:0px;
    margin-bottom: 10px;
    background-color: #424040;
    background: rgba(0, 0, 0, 0.6);
    border-left:solid 2px #761908;
    border-right:solid 2px #761908;
    border-bottom:solid 2px #761908;
    border-radius: 0 0 5px 5px;
    color: #ffffff;
    z-index: 100;
}
.kronologi-title {font-size:14px;font-weight:bold;background-color: #761908;color: white;padding:5px;text-align: center;}
.kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
.kronologi-info p {line-height: 15px;font-size:12px;}

.ketua {
    background-color: yellow;
    width: 30%;
    display: block;
    float: left;
    margin-right: 3%;
    box-shadow: -3px 3px 10px gray;
    border-radius: 8px;
    margin-bottom: 15px;
}
.ketua img {
    width: 100%;
    height: 200px;
}
.kritik {
    font-size: 18px;
    font-weight: bold;
    color: black;
}
.isi-col50 {
    float: left;
    margin: 10px 16px;
    width: 41%;
    height: auto;
    display: inline-block;
}
.boxdotted {
    border-radius: 10px;
    border: 2px dotted #29166f;
    width: 293px;
    height: 345px;
    float: left;
    margin-bottom: 10px;
    padding-top: 5px;
    margin-left: 10px;
}
.bendera {
    width: 150px;
    height: 75px;
    margin-right: 10px;
    float: left;
    border: 1px solid black;
}
#bulet {
    background-color: #ffff00;
    text-align: center;
    width: 50px;
    height: 25px;
    border-radius: 50px 50px 50px 50px;
    -webkit-border-radius: 50px 50px 50px 50px;
    -moz-border-radius: 50px 50px 50px 50px;
    color: black;
    padding: 6px 10px;
    margin-left: -10px;
    margin-right: 10px;
}
.gallery {
    list-style: none outside none;
    padding-left: 0;
    margin-left: 0px;
}
.gallery li {
    display: block;
    float: left;
    height: 80px;
    margin-bottom: 7px;
    margin-right: 0px;
    width: 120px;
}
.gallery li a {
    height: 100px;
    width: 100px;
}
.gallery li a img {
    max-width: 115px;
}

</style>
<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.thumb2').click(function(){
            var target = $(this).data('homeid');
            var src = $(this).attr('src');
            $('#'+target).attr('src',src);
        });
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<br/>
<div class="container">
<div class="sub-header-container">
<div class="allpage">
<div class="col_top"></div>
<div class="col_kiri">
    <h4><a id="analisa" style="color: black;"><?php echo $data['NARASI']['title'];?></a></h4>
    <p><?php echo $data['NARASI']['narasi'];?></p>
    <div class="garis"></div>

    <h4 class="list"><a id="analisa" style="color: black;">ANALISA</a></h4>
    <p class="font_kecil"><?php echo $data['ANALISA']['narasi'];?></p>
    <div class="clear"></div><div class="garis"></div>

    <h4 class="list"><a id="prokontra" style="color: black;">PRO DAN KONTRA</a></h4>
    <p class="font_kecil"><?php echo $data['PROKONTRA']['narasi'];?></p>
    <div class="clear"></div>

    <h4 class="list"><a id="prokontra" style="color: black;">KASUS PELINDO II</a></h4>
    <p class="font_kecil"><?php echo $data['KASUS']['narasi'];?></p>
    <div class="clear"></div>

</div>

<div class="col_kanan">
    <div class="boxprofile white">
        <h4 class="block_red text-center"><a id="profil" style="color: white;">PROFIL BARESKRIM POLRI</a></h4>
        <img src="http://img12.deviantart.net/6871/i/2013/095/c/7/logo_reskrim_polri_by_crime1985-d60iln6.png" class="picprofil"><br>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Badan Reserse Kriminal Polri (Bareskrim Polri) adalah unsur pelaksana utama Kepolisian Negara Republik Indonesia (Polri) pada tingkat Markas Besar dipimpin oleh Kepala Bareskrim (Kabareskrim Polri) yang bertanggung jawab di bawah Kepala Kepolisian Negara Republik Indonesia (Kapolri). Kabareskrim Polri bertugas membantu Kapolri dalam membina dan menyelenggarakan fungsi penyelidikan dan penyidikan tindak pidana, pengawasan dan pengendalian penyidikan, penyelenggaraan identifikasi, laboratorium forensik dalam rangka penegakan hukum serta pengelolaan informasi kriminal nasional</p>
    </div>
    <div class="clear"></div>
    <br/>

    <div class="boxprofile white">
        <h4 class="block_red text-center"><a id="profil" style="color: white;">STRUKTUR ORGANISASI BARESKRIM POLRI</a></h4>
        <img src="http://sumbawanews.com/sumbawanews/sites/default/files/images/stories/photo/struktur_organisasi_bareskrim.gif" class="picprofil"><br>
        <ul style="margin-left: 5px;margin-right: 20px;">
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Kepala Badan Reserse Kriminal Polri (Kabareskrim) : Komisaris Jenderal Polisi Dr. Anang Iskandar, S.H., M.H.</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Wakil Kepala Badan Reserse Kriminal Polri (Kabareskrim) : Inspektur Jenderal Polisi Drs. Johny Mangasi Samosir, S.H., M.Sc.</li>
        </ul>
        <ul style="margin-left: 5px;margin-right: 20px;">Unsur Pembantu Pimpinan dan Pelaksana Staf
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Biro Pembinaan dan Operasional (Robinopsnal) : Brigadir Jenderal Polisi Drs. Wilmar Marpaung, S.H.</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Biro Perencanaan dan Administrasi (Rorenmin) : Brigadir Jenderal Polisi Drs. Edward Syah Pernong, S.H.</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Biro Pengawasan Penyidikan (Rowassidik) : Brigadir Jenderal Polisi Drs. Zulkarnain, S.H.
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Biro Koordinasi dan Pengawasan Penyidik Pegawai Negeri Sipil (Rokorwas PPNS) : Brigadir Jenderal Polisi Drs. Agus Kusnadi
        </ul>
        <ul style="margin-left: 5px;margin-right: 20px;">Unsur Pelaksanaan Staf Khusus/Teknis
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Pusat Laboratorium Forensik (Puslabfor) : Brigadir Jenderal Polisi Drs. Alexander M. Mandalika</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Pusat Indonesia Automatic Fingerprint Identification System (Pusinafis) asalnya Pusident : Brigadir Jenderal Polisi Drs. Bekti Suhartono</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Pusat Informasi Kriminal Nasional (Pusiknas) : Brigadir Jenderal Polisi Drs. Hendrawan, S.H., M.H.</li>
        </ul>
        <ul style="margin-left: 5px;margin-right: 20px;">Unsur Pelaksana Utama Direktorat Bareskrim Polri (Ditbareskrim Polri), terdiri dari 5 Direktorat
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Direktorat Tipidum, menangani tindak pidana terhadap keamanan Negara dan tindak pidana umum : Brigadir Jenderal Polisi Drs. Herry Prastowo, S.H., M.Si.</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Direktorat Tipideksus, menangani tindak pidana dalam bidang ekonomi dan keuangan / perbankan serta kejahatan khusus lainnya : Brigadir Jenderal Polisi Drs. Victor Edi Simanjuntak</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Direktorat Tipidkor, menangani tindak pidana korupsi, kolusi, dan nepotisme : Brigadir Jenderal Polisi Drs. Akhmad Wiyagus, M.Si.</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Direktorat Tipidnarkoba, menangani tindak pidana narkoba : Brigadir Jenderal Polisi Drs. Anjan Pramuka Putra, S.H, M.Hum.</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Direktorat Tipidter, menangani tindak pidana tertentu yang tidak ditangani oleh Dit I sampai dengan Dit IV : Brigadir Jenderal Polisi Drs. Yazid Fanani, M.Si.</li>
        </ul>
    </div>
    <div class="clear"></div>
    <br/>

    <div class="boxprofile white">
        <h4 class="block_red text-center"><a id="profil" style="color: white;">MANTAN KABARESKRIM POLRI</a></h4>
        <ul style="margin-left: 5px;margin-right: 20px;">
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Brigjen Koesparmono Irsan (Ketika Bareskrim Polri masih dibawah Deputy Kapolri Bidang Operasi)</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Brigjen Tony Sidharta</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Brigjen Oetoyo Soetopo</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Brigadir Jenderal Polisi Nurfaizi</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Brigadir Jenderal Polisi Da'i Bachtiar</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Inspektur Jenderal Polisi Chairudin Ismail (Ketika Bareskrim Polri bernama Korps Reserse Polri)</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Inspektur Jenderal Polisi Engkesman Hillep</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Erwin Mappaseng</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Suyitno Landung</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Makbul Padmanegara</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Bambang Hendarso Danuri</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Susno Duadji</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Ito Sumardi</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Sutarman</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Suhardi Alius</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Komisaris Jenderal Polisi Budi Waseso</li>
        </ul>
    </div>
    <div class="clear"></div>
    <br/>

    <div class="boxprofile white">
        <h4 class="block_red text-center"><a id="profil" style="color: white;">SOSOK KONTROVERSI BUWAS</a></h4>
        <img src="http://images.jurnalasia.com/2015/02/Kenaikan-Pangkat-Pati-Polri-050215-AGR-1.jpg" class="picprofil"><br>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Budi Waseso dikenal sebagai sosok yang kontroversial. Namanya mencuat ke publik ketika atasannya di kepolisian, Kapolri Budi Gunawan ditetapkan sebagai tersangka oleh KPK. Hingga akhirnya kasus penetapan Kapolri sebagai tersangka korupsi menyeret dua lembaga tersebut terlibat kisruh, KPK vs Polri. Diduga Budi Waseso memiliki peran yang sangat krusial dalam kisruh tersebut. Ia juga mendapat sorotan tajam dari media ketika  atasannya yang saat itu menjabat Kabareskrim, Komjen Susno Duadji ditangkap di Bandara Soekarno-Hatta akibat terlibat korupsi.</p>
        <p style="margin-left: 20px;margin-right: 20px;font-size: 12px;">Karirnya di kepolisian terbilang cukup cemerlang.  Diawal tahun 2015, ia mendapatkan promosi dan menempati posisi Kabareskrim Mabespolri. Sebelumnya ia menjabat sebagai Kepala Bidang program Polda Jateng. Selanjutnya, ia ditempatkan di Mabespolri dan menempati posisi sebagai Kepala Pusat Pengamanan Internal Mabes Polri.</p>
        <ul style="margin-left: 20px;margin-right: 20px;">BIODATA
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Nama : Komisaris Jenderal Polisi Drs. Budi Waseso, S.H</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Tempat Tgl lahir: Pati, Jawa Tengah, 19 Februari 1960</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Lulusan: Akademi Kepolisian (1984)</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Pangkat: Jenderal Bintang Tiga</li>
        </ul>
        <ul style="margin-left: 20px;margin-right: 20px;">PERJALANAN KARIR
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">2009: Kepala Bidang Propam Polda Jateng</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">2010: Kepala Pusat Pengamanan Internal Mabes Polri</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">2012: Kepala Kepolisian Daerah Gorontalo</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">2013: Widyaiswara Utama Sespim Polri</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">2014: Kepala Sekolah Staf dan Pimpinan Tinggi Polri</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">2015: Kepala Badan Reserse Kriminal Polri[3]</li>
            <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">2015: Kepala Badan Narkotika Nasional</li>
        </ul>
    </div>
    <div class="clear"></div>
    <br/>


    <!-- h4 class="list" style="margin-left: 20px;margin-top: 25px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
    <div style="margin-left: 20px;background-color: #E5E5E5;">
        <div id="newskampungpulo_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
        <div class="row-fluid" style="margin-bottom: 2px;">
            <div class="span6 text-left">
                <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
            <!-- /div>
            <div class="span6 text-right">
                <a id="newskampungpulo_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
            <!--/div>
        </div>
    </div -->
</div>
<div class="clear"></div>

<div class="col_full">
    <div class="garis"></div>
    <h4 class="list"><a id="analisa" style="color: black;">WIBAWA PRESIDEN ANJLOK</a></h4>
    <p class="font_kecil"><?php echo $data['WIBAWA']['narasi'];?></p>
    <div class="clear"></div><div class="garis"></div>

    <h4 class="list"><a id="analisa" style="color: black;">BUWAS DIMUTASI KE BNN</a></h4>
    <p class="font_kecil"><?php echo $data['MUTASI']['narasi'];?></p>
    <div class="clear"></div><div class="garis"></div>

    <h4 class="list"><a id="analisa" style="color: black;">JK INTERVENSI BUWAS ?</a></h4>
    <p class="font_kecil"><?php echo $data['INTERVENSI']['narasi'];?></p>
    <div class="clear"></div><div class="garis"></div>

    <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
    <div>
        <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENDUKUNG</p><br/>
        <ul style="margin-left: 0px;">
           <li class="organisasi">
               <img src="http://i.ytimg.com/vi/P87ko6AZeIs/maxresdefault.jpg" data-toggle="tooltip" data-original-title="Indonesian Corruption Watch"/>
               <p>Indonesian Corruption Watch</p>
           </li>
            <li class="organisasi">
                <img src="http://demak.muhammadiyah.or.id/muhfile/demak/foto/Logo%20Pemuda.jpg" data-toggle="tooltip" data-original-title="Pemuda Muhammadiyah"/>
                <p>Pemuda Muhammadiyah</p>
            </li>
            <li class="organisasi">
                <img src="http://3.bp.blogspot.com/-GxUYoyUSTx4/UTx9_C0CnOI/AAAAAAAABVk/0JMLy_37-w0/s1600/Logo+POLRI+Format+Vector+Coreldraw.jpg" data-toggle="tooltip" data-original-title="Kepolisian Negara Republik Indonesia"/>
                <p>Kepolisian Negara Republik Indonesia</p>
            </li>
            <li class="organisasi">
                <img src="http://mediamalang.com/wp-content/uploads/2015/05/kontras_Cover-koran.jpg" data-toggle="tooltip" data-original-title="KONTRAS"/>
                <p>KONTRAS</p>
            </li>
            <li class="organisasi">
                <img src="http://www.bijaks.net/assets/images/avatar-institusi-acc.jpg" data-toggle="tooltip" data-original-title="Lingkar Madani Indonesia (Lima)"/>
                <p>Lingkar Madani Indonesia (Lima)</p>
            </li>
            <li class="organisasi">
                <img src="http://www.intelijen.co.id/wp-content/uploads/2014/10/ICW-592x279.jpg" data-toggle="tooltip" data-original-title="ICW (Indonesian Corruption Watch)"/>
                <p>ICW (Indonesian Corruption Watch)</p>
            </li>
        </ul>

    </div>

    <div class="clear"></div><h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
    <div>
        <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENENTANG</p><br/>
        <ul style="margin-left: 0px;">
            <li class="organisasi">
                <img src="http://kbppp.ilmci.com/wp-content/uploads/2010/10/logo-kbppp-b-copy1a1.jpg" data-toggle="tooltip" data-original-title="KBPPP"/>
                <p>KBPPP</p>
            </li>
            <li class="organisasi">
                <img src="http://www.bijaks.net/assets/images/avatar-institusi-acc.jpg" data-toggle="tooltip" data-original-title="MP3K"/>
                <p>MP3K</p>
            </li>
            <li class="organisasi">
                <img src="http://www.fspbun.org/wp-content/uploads/2014/09/Logo-FSP-BUN_New_Warna.jpg" data-toggle="tooltip" data-original-title="Federasi Serikat Pekerja BUMN Bersatu"/>
                <p>Federasi Serikat Pekerja BUMN Bersatu</p>
            </li>
            <li class="organisasi">
                <img src="http://www.bijaks.net/assets/images/avatar-institusi-acc.jpg" data-toggle="tooltip" data-original-title="Sustainable Development Indonesia"/>
                <p>Sustainable Development Indonesia</p>
            </li>
            <li class="organisasi">
                <img src="http://www.bijaks.net/assets/images/avatar-institusi-acc.jpg" data-toggle="tooltip" data-original-title="Aliansi Pemuda Penyelamat Aset Negara"/>
                <p>Aliansi Pemuda Penyelamat Aset Negara</p>
            </li>
        </ul>
    </div>


</div>
<div class="clear"></div><br><div class="garis"></div>

<div class="col_full">
    <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
    <div class="clear"></div>
    <ul style="margin-left: 0px;margin-top: 10px;">
        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
            <?php
            for($i=0;$i<=3;$i++){
                ?>
                <div style="width: 100%;height: auto;display: inline-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 77%;margin-right: 10px;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div style="float: left;width: 47%;margin-left: 20px;">
            <?php
            for($i=4;$i<=6;$i++){
                ?>
                <div style="width: 100%;height: auto;display: inline-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 78%;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </ul>
</div>
<div class="clear"></div><br><div class="garis"></div>

<div class="col_full">
    <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENENTANG</a></h4>
    <div class="clear"></div>
    <ul style="margin-left: 0px;margin-top: 10px;">
        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
            <?php
            for($i=0;$i<=4;$i++){
                ?>
                <div style="width: 100%;height: auto;display: inline-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 77%;margin-right: 10px;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div style="float: left;width: 47%;margin-left: 20px;">
            <?php
            for($i=5;$i<=8;$i++){
                ?>
                <div style="width: 100%;height: auto;display: inline-block;">
                    <div style="float: left;width: 20%;">
                        <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                    </div>
                    <div style="float: right;width: 78%;">
                        <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                        <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </ul>
</div>
<div class="clear"></div><br><div class="garis"></div>

<h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
<div class="col_full" style="display: inline-block;">
    <ul id="light-gallery" class="gallery" style="margin-left: 0px;margin-top: 0px;">
        <?php
        foreach ($data['FOTO'] as $key => $val) { ?>

            <li data-src="<?php echo $val['img'];?>" style="overflow: hidden;">
                <a href="#">
                    <img src="<?php echo $val['img'];?>" />
                </a>
            </li>
        <?php
        }
        ?>
    </ul>
</div>

<div class="clear"></div>
<h4 class="list" style="margin-top: 10px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>
<div class="col_full">
    <div class="boxgray" style="height: 220px;">
        <ul style="margin-left: 15px;margin-top: 10px;">
            <?php
            foreach ($data['VIDEO'] as $key => $val) { ?>
                <li class="video">
                    <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                        <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                    </a>
                </li>
                <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                            </div>
                            <div class="modal-body">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </ul>
    </div>
</div>
<div class="clear"></div>

</div>
</div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>