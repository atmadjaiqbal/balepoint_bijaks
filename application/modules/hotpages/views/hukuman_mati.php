<?php 
$data = [
    'NARASI'=>[
        'title'=>'HUKUMAN MATI REZIM JOKOWI',
        'narasi'=>'Kehadiran pemerintahan baru di bawah Joko Widodo tidak mengubah banyak praktik hukuman mati di Indonesia. Di awal kepemimpinannya, Jokowi bahkan telah mengeksekusi 14 terpidana mati kasus narkoba di Nusa Kambangan dan Boyolali. Banyak kalangan menilai--terutama dari kalangan aktivis HAM - Jokowi tetap menerapkan hukum yang tidak manusiawi. Padahal ada alternatif lain yang lebih berat selain hukuman mati. Seperti hukuman 300 tahun.
                   </p><p>Namun tak sedikit masyarakat yang juga ikut mendukung kebijakan hukuman mati Jokowi tersebut, mengingat narkoba telah meracuni anak bangsa Indonesia. Presiden Jokowi sendiri menyatakan Indonesia kini menghadapi “darurat narkoba.” Sekalipun banyak menuai protes, Jokowi akan tetap menjalankan hukuman mati bagi pelaku kejahatan luar biasa, termasuk bandar dan pengedar narkoba serta pelaku tindak terorisme. '
    ],
    'PASAL_MEMBUNUH'=>[
        'narasi'=>'Presiden Joko Widodo selalu mengatakan kepada publik, bahwa bukan dirinya yang memerintahkan hukuman mati, tetapi hukum Indonesia. Hukuman mati di Indonesia merujuk kepada',
        'isi'=>[
            ['no'=>'Kitab Undang-Undang Hukum Pidana (KUHP), ketentuan Pasal 10 tegas menyatakan bahwa hukuman mati merupakan salah satu dari hukuman pokok.
                    Sementara pelaksanaan eksekusi hukuman mati diatur dalam Undang-Undang No.2/PNPS/1964 tentang Tata Cara Pelaksanaan Pidana Mati yang Dijatuhkan oleh Pengadilan di Lingkungan Peradilan Umum dan Militer dan tata pelaksanaannya diatur dalam Peraturan Kapolri No.12 Tahun 2010 tentang Tata Cara Pelaksanaan Pidana Mati.'],
            ['no'=>'Indonesia telah terikat dengan konvensi internasional narkotika dan psikotropika yang telah diratifikasi menjadi hukum nasional dalam UU Nomor 22 Tahun 1997 tentang Narkotika.'],
            ['no'=>'Putusan Mahkamah Konstitusi (MK) tertanggal 30 Oktober 2007 yang menolak uji materi hukuman mati dalam UU Narkotika. MK menegaskan bahwa hukuman mati dalam UU Narkotika tidak bertentangan dengan hak hidup yang dijamin UUD 1945 lantaran jaminan hak asasi manusia dalam UUD 1945 tidak menganut asas kemutlakan.']
        ]
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaidemokrat5119a5b44c7e4'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'partaikeadialndnpersatuanindonesia5119bd7483d2a'],
        ],
        'institusi'=>[
            ['name'=>'Mahkamah Konstitusi','img'=>'http://www.bijaks.net/public/upload/image/politisi/mahkamahkonstitusi51cf91379d144/badge/ac88aaa62f71902ffbcb42cdeb7a046d7b461320.jpg','url'=>'http://www.bijaks.net/aktor/profile/mahkamahkonstitusi51cf91379d144'],
            ['name'=>'Jaksa Agung','img'=>'http://www.bijaks.net/public/upload/image/politisi/kejaksaanagung530d6083297e1/badge/abda99f8e850c0bca085b2a6fafc5a76831e9c2d.jpg','url'=>'http://www.bijaks.net/aktor/profile/kejaksaanagung530d6083297e1'],
            ['name'=>'Polisi Republik Indonesia','img'=>'http://www.bijaks.net/public/upload/image/politisi/kepolisiannegararipolri536756e743fd0/badge/33009cc90cd7ab2779e8f44446da80fce94e496f.png','url'=>'http://www.bijaks.net/aktor/profile/kepolisiannegararipolri536756e743fd0'],
            ['name'=>'Badan Narkotika Nasional','img'=>'http://www.bijaks.net/public/upload/image/politisi/badannarkotikanasionalbnn5192fb6e2ffa5/badge/badannarkotikanasionalbnn5192fb6e2ffa5_20130515_031610.jpg','url'=>'http://www.bijaks.net/aktor/profile/badannarkotikanasionalbnn5192fb6e2ffa5'],
            ['name'=>'Mayoritas DPR RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/dewanperwakilanrakyatdpr51da66b0a7ac4/badge/be53b09d1120ec01cb37eb34c0417babe9b93f37.jpg','url'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdpr51da66b0a7ac4'],
            ['name'=>'KEMENKO POLHUKAM','img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/e/e2/Logo_Kemenkopolhukam.png/100px-Logo_Kemenkopolhukam.png','url'=>'http://id.wikipedia.org/wiki/Kementerian_Koordinator_Bidang_Politik,_Hukum,_dan_Keamanan_Indonesia']
        ],
        'tokoh'=>[
            ['name'=>'Joko Widodo','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/2c95a588c6879fc7358c019ccbf6ff744042dea7.jpg','jabatan'=>'Presiden RI','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19'],
            ['name'=>'Jusup Kalla','img'=>'http://www.bijaks.net/public/upload/image/politisi/drshmuhammadjusufkalla50ee870b99cc9/badge/e47e03b37d572a59b005aaa84751a6f05b002e9f.jpg','jabatan'=>'Wakil Presiden RI','url'=>'http://www.bijaks.net/aktor/profile/drshmuhammadjusufkalla50ee870b99cc9'],
            ['name'=>'Setyo Novanto','img'=>'http://www.bijaks.net/public/upload/image/politisi/drssetyanovanto50f8fd3c666bc/badge/49cb048fd43bcfc141df78187bf0df8ccf8b0034.jpg','jabatan'=>'Ketua DPR RI','url'=>'http://www.bijaks.net/aktor/profile/drssetyanovanto50f8fd3c666bc'],
            ['name'=>'Fadli Zon','img'=>'http://www.bijaks.net/public/upload/image/politisi/fadlizon5119d8091e007/badge/11bfa198ef6e5eabc035f5e8e858487df7e7a676.jpg','jabatan'=>'Wakil Ketua DPR RI','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007'],
            ['name'=>'Fahri Hamzah','img'=>'http://www.bijaks.net/public/upload/image/politisi/fahrihamzahse5105e57490d09/badge/3319f60b624e03209d687d0af9b1c4aa2661218e.jpg','jabatan'=>'Wakil Ketua DPR RI','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09'],
            ['name'=>'Muhammad Prasetyo','img'=>'http://www.bijaks.net/public/upload/image/politisi/muhammadprasetyo54d46b2010b68/badge/956df1c81c9bd7793bde99b52c6aa938cfa9fe96.jpg','jabatan'=>'Jaksa Agung','url'=>'http://www.bijaks.net/aktor/profile/muhammadprasetyo54d46b2010b68'],
            ['name'=>'Surya Paloh','img'=>'http://www.bijaks.net/public/upload/image/politisi/suryapaloh511b4aa507a50/badge/5b8adf5757dbdc45407f3c499223705c120d8629.jpg','jabatan'=>'Pendiri NasDem','url'=>'http://www.bijaks.net/aktor/profile/suryapaloh511b4aa507a50'],
            ['name'=>'Yusril Ihza Mahendra','img'=>'http://www.bijaks.net/public/upload/image/politisi/yusrilihsamahendra51675526bbc70/badge/030eee299b8489acd2955ff3328a160310712c35.jpg','jabatan'=>'Pakar Hukum','url'=>'http://www.bijaks.net/aktor/profile/yusrilihsamahendra51675526bbc70'],
            ['name'=>'Mahfud MD','img'=>'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/badge/profdrmohammadmahfudmdshsu512c527ac591b_20130226_082408.jpg','jabatan'=>'Mantan Ketua MK','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b'],
        ],
    ],
    'PENENTANG'=>[
        'tokoh'=>[
            ['name'=>'Syafii Maarif','img'=>'http://www.bijaks.net/public/upload/image/politisi/buyaahmadsyafiimaarif543f625a98a10/badge/90e1d21718e6139f03f240f89a80be5582905b21.jpg','jabatan'=>'Tokoh Bangsa','url'=>'http://www.bijaks.net/aktor/profile/buyaahmadsyafiimaarif543f625a98a10'],
            ['name'=>'Todung Mulya Lubis','img'=>'http://www.bijaks.net/public/upload/image/politisi/todungmulyalubis533a790b3fe41/badge/4dbe1a0e9ee9325980bb723dafc1b09a85d04860.jpg','jabatan'=>'Pakar Hukum','url'=>'http://www.bijaks.net/aktor/profile/todungmulyalubis533a790b3fe41'],
            ['name'=>'Imam Prasojo','img'=>'http://www.bijaks.net/public/upload/image/politisi/imambprasodjo54d07a740feb9/badge/4c4f1c96acd8dc5671daf276bbea6e6059d35832.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/imambprasodjo54d07a740feb9'],
            ['name'=>'Erry Riyana Hardjapamekas','img'=>'http://www.bijaks.net/public/upload/image/politisi/erryriyanahardjapamekas51a2b3bdb7874/badge/6460dba04e6dd657b5132f9721d90a2e197b6ab4.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/erryriyanahardjapamekas51a2b3bdb7874'],
            ['name'=>'Tumpak Hatorangan Panggabean','img'=>'http://www.bijaks.net/public/upload/image/politisi/tumpakhatoranganpanggabean5193023cd7c5e/badge/1ead704c13a2634564cbb187f194d8e05ad2fc8e.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/tumpakhatoranganpanggabean5193023cd7c5e'],
            ['name'=>'Usman Hamid','img'=>'http://www.bijaks.net/public/upload/image/politisi/usmanhamid54d41a069aa7a/badge/620b5a25d4fd36497c8e05c3eaaca075dd9b5593.jpg','jabatan'=>'Pendiri Kontras','url'=>'http://www.bijaks.net/aktor/profile/usmanhamid54d41a069aa7a'],
            ['name'=>'Bonar Tigor Naipospos','img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/08/27/283626/MolJ7snx4V.jpg?w=700','jabatan'=>'Setara Institute','url'=>'http://en.wikipedia.org/wiki/Setara_Institute'],
            ['name'=>'Mgr Ignasius Suharyo Pr','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Mgr._Ign._Suharyo.JPG/120px-Mgr._Ign._Suharyo.JPG','jabatan'=>'Uskup Agung','url'=>'http://id.wikipedia.org/wiki/Ignatius_Suharyo'],
            ['name'=>'Natalius Pigai','img'=>'http://upload.wikimedia.org/wikipedia/id/9/92/Natalius_Pigai.jpg','jabatan'=>'Komisioner Komnas HAM','url'=>'http://id.wikipedia.org/wiki/Natalius_Pigai'],
            ['name'=>'Andreas Harsono','img'=>'http://www.internetfreedomfellows.com/wp-content/uploads/2013/02/Harsano21.jpg','jabatan'=>'Peneliti dan Aktivis','url'=>'http://www.andreasharsono.net/'],
        ],
        'institusi'=>[
            ['name'=>'Komnas HAM','img'=>'http://www.bijaks.net/public/upload/image/politisi/komnasham54c1eac938164/badge/e559bfdeda6bc708d46fcb9a29afb001bb2360ec.jpg','url'=>'http://www.bijaks.net/aktor/profile/komnasham54c1eac938164'],
            ['name'=>'Amnesty International','img'=>'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAkFBMVEX88SoAAAD/9Sv/9yv//iz//CxkYxH/+Svw5ib/8yr28irj2SXe2CV7dhQEAAFZVQ6NiRUsKQbAuB9fWw7XzyNIRAv17CkcGgTn3ibRyiPKwSBvahElIwWFfxWVjhiimho3NQmqohtBPgoNCwAxLgdRTQ21rh0gHQMXFAN2cBPAviG4th/++a788kKsqh6YlhqwMc81AAAJ5ElEQVR4nOWcC5OiOhOGyQWTjHBAROSiKDiiON/R///vTgKEi4szzArGqq+rzikXa4dnk87bnU5nNO3/yZBqgAfmeFA1Qo9Bfx19MNUUfxiDEQARUY1xb0y3AbfszcCQlwNwtMDGeKuZ1P0lH6ww4TOJVbM0hrxEzOHKC/n/g3dRC4jDo8ACO+Ju+IC9iVYQY1ZQgSOB/l4M2DuAEbg7lVhnFzGagrfwMOJ9bUBlIdEYXYlPVPGSZHowk1Qg0fmDYrxAolTDGJpHNVWpp6V/gUglFvHsc4P1pReoxkL8Ye8rm0iGnEVDtQjLmUNh+WdlK5JQE7SwAr18DLPK1xQ5GHI3LazUkAqPtuWTra4EC2ctzwIrvx4dUs2tpUTB0LVFBawmR4VG9eyoggt3sJat1Blv5UMFXChoY0WwkYRKvbjZr/evStJ7sDSUyMfO61Md3BaImdYW0I+T1I3X6yoyWljHDpa+k8+3rx8u2Bquvd+WdeZ/yi/8l8s9M1rKFXZUHdXE9utXY+Pa/PVdLFc+TxWkX3jZzCLtzKK3qhejguCIm2ncdTQK1U5vqoiNuJlGrz1bzTJdeSpyL71R1I4WQEs+d5XsHhuurP3+ZjlcFWU4h1okWl7PqHysQFFLLrkeN26Li8j9x5IoSuyJXHanFpfM6kH6eqGvjNXqeWnNWFmdAGdDXcXEkxvZxu9JplBQawhHymozNlVSr7ZSiKucdFXvgSrSWG0BE7mVJsjyW5X5qFKI2lBccs2q8fGKAeymiEoMV65fJvGMFgqhJCp2jXllMDwWAwbnoCjJqabiBmEpWEUVonD7i5qoeG/IL/ZqG6GjQr3ydxgtYaisV4pcS4/B+W0q43zEiqk0dcb3k/s3OnqBelGLWPpkf5dRKzaGArFfTKMDcNVrRNuQ3GC/gXZ1jRTp86enmqMwVlrxmWzfZLyYDitjuq5jXByFxhhjXSfFA2E6eXXaWgSdHvuMtnYcb7emGeWmud2FLx7CR1x/2PbjPbleXCkfzgVeGgN+wfXSPPEXXErmcZ9cszAM3blPqed5Gv/Po7RdQM9fWjOUXEvCDSEEWTNbDHotrteeqEmuWe9o0AbrxSej33KxFtfLMkX2K67pOwJKHoTd33CllE2tEj4nY1qYlycr0B/EFVFKNdIcHTE4Nibc5RDRHBQVJIawM2weP1efUeJqmDBGdMzmLh1bzsjt6F9Azlc9gjSoCl0/+72w88ZKqJ/Zq836vF6OnGHw/c4i4iqJcWif6lcO4uradqyACbmDCB1P+A89mNtV+yU/cVm2tS9OIPb1WdYhHEc6WHizzeXxs33wDxaHYVwJxjRI4t0X9euWlJSOM2JOnB/3BQf/N5tGlgW+9xEO49oRvkgQRIhBmKX7fZryn5GGo3SRIoKxJkKx7V/5APB4yDQ0UFfbO12CeWDX6I3Pa7obSTBIII6wIck25YuG6n333F2wMOKKMuxylLSM0cXtIkpbxLbR33OVRuaisJiP4WTQdMTs8Q+erz3LpSFjDUZq3aF3a5sNi0Mg7pV3XRT6N/MJQqd89U9cQf+7veXdwdKLua49cwV5yEBCzK6CC467XRrGteiZRTa3lnYuvjWRWJyZAq5bn3d5YVyeEaYh8W7bUbEGcvV35yCilQdc5+PxNnJCNowrexChZVa5MsZOs4dxmQ9ei0qudTB6jWDgeuxVAgKrPur9+EWoJ3RCy1Yyr0zKIPJ6rj7Hp3kchNcym4uykQ/Ch3Gt+/2HIFL0a+ZukIQquB7uaskXEDEd8pxxVKyBXNvutzxnLd2JfXBlXU1RGLvjQtVskC5XJ5uAun+5uJqu66TI9SdpGujmX8yZF/PhJd4jLqhTOwXgZJm3OC8OnCepVN/lhbNUcJGsJKm56qwQkjAHXXsUC8bkQrci+9SXpTC09B4WRyKo03RefjVNue5uHr0NCHW+yKwPSHRIZY/MInEC13UD2bazkJvbxVQngrItboYR4mqEE55+otXawNSxo+O+NTDn9br8sEpcwzfCeJYuE3+qap3sXFoluyTbxdmlGIl0Bh7Y8Uox4ZMKoTg6GrUJi9Q/kPGPX3cvPvTiSNtSfaL6HKLXT3CKfT5rmmGv7jA+q7pTGlmWtTSll6+jz/U+tcxgsmomKRsSwMJ2s9m6BWTbO8dxDP8EYks0JGvU8wiqvt0Rw/V9Siar/ZKwfV+iZUtMCISQ+72JcfIPWBc9th/VtwFkcMoCq141Qy93ThCESasCNitvC9EN4LtBEuzLXXTNNWnRF5ILn7k0nnO/R3xwCJ7PZTWr0tUMLMU5I98aFkFPck2xc5VGMLW5l5+o3ryEs5ltLs1zOy3akmu6m6xMy0qHv+tTkr3scmfR3W5NzyXvJ93ullTN1RtPJudCxTE/OMT3AjSI65+puKrEYPbnPk8pF4QmOGwdryecSa7++0HTchFxEzvvD7IKuZBQrUeuq46LUYE1e/CtOq7ylw9cHoRcdVxY9IAuHn2rkEtE54fXBCVXf5fLpPMolP7hRl6hfola9qdXthhr951wMH+S6++J0ZwnXUcXQ42hbXLn/4O4+kWmqIzr/l9zlZ3/h9wnHzGI7zKpv+ci2fmDsfiZ7XZ5t+S0mwHrvpjw91x4lSK8zfEzVRNSZTnifiMUXTjPclGGXBB4+cZ1kie4GCtPAGJMsP/Ft6/NReiKq3/BPsijGT35eHvYcWVcP3cLBHrFxjBNHHPDg1JrNjv5/UAujUQreWXmycuRyG9qDU47sxjEdb8fQl79Kw4s+tyeBNU7x5MLmzXkLX/LxRDz5jf5s+Knq3JEtpbwqOTKEWPUGsIVlr08fPdGdP+6rQcrff78BQWtnfYhp1rRDvDhH4dyIc01l4ljNb+kYj1KP3y6ti/Z+ZDIBZhcd/ZyL+smP3KhoBnvcqy+3G3qPzteyAEGQlz5fXIFPfYDF6mvrJW22ZxDHSFntaNPcoWFzOh7A+LsT6wfuBKaSA9IrSiKv6i7MKDoVAjHaYlHEZdu5C4O4HBaHBc/ccl+zFMhMqckDH3qaR5CegYuQh/YSJWnD1v8+xByw4DqGIbx5luudp/V4gvqdQSDCchGraVUwy4agApAWt1NSx0+Cn8c1rW4VkYn3EBnyhqPKLHWbz7unDnr7jJb/ZjefWFj4jbb5p40EOVVM/MhFG2/Ha6j/+rLfazdaix9Lf4KfIi57kqu3ct/nwLr7yc/pVaeBPV6nOYM6FtDvVyVydCl4LebkGzzHZkyLlF3dTM7St+NSxwLE+JRt0hl+02Bf9VwDGHshbf82MN1VXwdkiHCqOHY+/cZr4aNIR453Z25XLwVV2mIIM8Ikuis0O8fWelybrK13uyepjDucvp7XIf8zt5v3AqD//v313/nP5XNlzPrVW+zAAAAAElFTkSuQmCC','url'=>'http://www.amnesty.org/'],
            ['name'=>'Setara Institute','img'=>'http://setara-institute.org/wp-content/uploads/2014/11/si-logo-2.png','url'=>'http://setara-institute.org/'],
            ['name'=>'Institut Titian Perdamaian','img'=>'http://www.titiandamai.or.id/file/profil/logo.png','url'=>'http://www.titiandamai.or.id/'],
            ['name'=>'Kontras','img'=>'http://sp.beritasatu.com/media/images/original/20120104130115874.jpg','url'=>'http://www.kontras.org/'],
            ['name'=>'The Wahid Institute','img'=>'http://2.bp.blogspot.com/-EXBACyHGda0/VNBtshWA2_I/AAAAAAAAFTE/E2gdTVC63j8/s1600/The%2BWahid%2BInstitute.jpg','url'=>'http://www.wahidinstitute.org'],
            ['name'=>'Human Rights Watch','img'=>'http://www.hrw.org/sites/all/themes/hrw/images/logo.png','url'=>'http://www.hrw.org/'],
            ['name'=>'Komnas Perempuan','img'=>'https://rangselbudi.files.wordpress.com/2012/03/komnasperempuan.jpg','url'=>'http://www.komnasperempuan.or.id/'],
            ['name'=>'Yayasan Lembaga Bantuan Hukum Indonesia','img'=>'http://infid.org/images/1373488207.jpg','url'=>'http://www.ylbhi.or.id/']
        ],
        'negara'=>[
            ['name'=>'Belanda','img'=>'http://www.bijaks.net/public/upload/image/politisi/belanda5373275a1e68e/badge/247e500c1d5bff90bdad74244e02c13064ec05d2.png','url'=>'http://www.bijaks.net/aktor/profile/belanda5373275a1e68e'],
            ['name'=>'Jerman','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/125px-Flag_of_Germany.svg.png','url'=>'http://www.bijaks.net/aktor/profile/republikfederaljerman5371f1f61c897'],
            ['name'=>'Uni Eropa','img'=>'http://www.bijaks.net/public/upload/image/politisi/unieropa537eb21c45dff/badge/a0dee47380d10daa6118d48f2cbac4d90ab34069.png','url'=>'http://www.bijaks.net/aktor/profile/unieropa537eb21c45dff'],
            ['name'=>'Inggris','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/125px-Flag_of_the_United_Kingdom.svg.png','url'=>'http://id.wikipedia.org/wiki/Britania_Raya'],
            ['name'=>'Australia','img'=>'http://upload.wikimedia.org/wikipedia/en/thumb/b/b9/Flag_of_Australia.svg/640px-Flag_of_Australia.svg.png','url'=>'http://www.bijaks.net/aktor/profile/australia5372f806e167b'],
        ]
    ],
    'TERPIDANA'=>[
        'narasi'=>'<img src="http://www.bijaks.net/assets/images/hotpages/hukumanmati/pic6.jpg" class="pic2">Minggu, 18 Januari 2015 Jokowi telah mengeksekusi mati enam terpidana kasus narkoba di Nusa Kambangan dan Boyolali. Lima diantaranya merupakan warga negara asing, selebihnya warga negara Indonesia. Berikut nama-namanya :',
        'isi'=>[
            ['no'=>'Namaona Denis (48), warga negara Malawi, diputus PN pada tahun 2001. Grasi ditolak pada 20 Desember 2014.'],
            ['no'=>'Marco Archer Cardoso Moreira (53), warga negara Brasil, diputus PN pada 2004.'],
            ['no'=>'Daniel Enemuo alias Diarrassouba Mamadou (38), warga negara Nigeria, diputus PN pada 2004 dan grasi ditolak 30 Desember 2014.'],
            ['no'=>'Ang Kiem Soei alias Kim Ho alias Ance Tahir alias Tommi Wijaya (52), warga negara Belanda. Grasinya ditolak 30 Desember 2014.'],
            ['no'=>'Tran Thi Bich Hanh (37), warga negara Vietnam, tidak mengajukan kasasi dan permohonan gransinya ditolak pada 30 Desember 2014.'],
            ['no'=>'Rani Andriani alias Melisa Aprilia, WNI asal Cianjur, Jawa Barat. Diputus PN pada tahun 2000. Grasi ditolak 30 Desember 2014.']
        ],
        'bawah'=>'Sejak 2000 hingga 2015 ini sudah ada 35 terpidana mati yang telah dieksekusi.'
    ],
    'DAFTAR'=>[
        'narasi'=>'Data Kejaksaan Agung mencatat sebanyak 128 terpidana mati yang masuk daftar tunggu eksekusi mati. Dengan rincian :',
        'isi'=>[
            ['no'=>'56 terjerat kasus narkoba '],
            ['no'=>'72 terpidana dari kasus non-narkoba '],
            ['no'=>'2 orang lainnya terbukti melakukan tindak terorisme']
        ]
    ],
    'BALI_NINE'=>[
        'narasi'=>'<img src="http://www.bijaks.net/assets/images/hotpages/hukumanmati/pic2.jpg" class="pic"><b>Kelompok Bali Nine</b> merupakan sebutan untuk sebilan warga negara Australia yang ditangkap di Bali, 17 April 2005 dalam upaya menyelundupkan heroin seberat 8,2 kilogram dari Australia. 
                   <br>Kesembilan orang itu adalah :',
        'isi'=>[
            ['no'=>'Andrew Chan'],
            ['no'=>'Myuran Sukumaran'],
            ['no'=>'Si Yi Chen'],
            ['no'=>'Micel Czugaj'],
            ['no'=>'Renae Lawrence '],
            ['no'=>'Tach Duc Thanh Nguyen '],
            ['no'=>'Mattew Norma '],
            ['no'=>'Scott Rush '],
            ['no'=>'Martin Stephens']
        ],
        'bawah'=>'Pengadilan Negeri Denpasar telah memvonis hukuman mati kepada Myuran Sukumaran dan Andrew Chan dan pada rabu 29/4/2015 telah dieksekusi di Nusakambangan. Sementara Lawrence, Czugaj, Stephens, dan Rush divonis dengan hukuman seumur hidup.'
    ],
    'MENUNGGU'=>[
        'narasi'=>'Minggu, 18 Januari 2015 Jokowi telah mengeksekusi mati enam terpidana kasus narkoba di Nusa Kambangan dan Boyolali. Lima diantaranya merupakan warga negara asing, selebihnya warga negara Indonesia. Berikut nama-namanya :',
        'isi'=>[
            ['no'=>'Namaona Denis (48), warga negara Malawi, diputus PN pada tahun 2001. Grasi ditolak pada 20 Desember 2014.'],
            ['no'=>'Marco Archer Cardoso Moreira (53), warga negara Brasil, diputus PN pada 2004.'],
            ['no'=>'Daniel Enemuo alias Diarrassouba Mamadou (38), warga negara Nigeria, diputus PN pada 2004 dan grasi ditolak 30 Desember 2014.'],
            ['no'=>'Ang Kiem Soei alias Kim Ho alias Ance Tahir alias Tommi Wijaya (52), warga negara Belanda. Grasinya ditolak 30 Desember 2014.'],
            ['no'=>'Tran Thi Bich Hanh (37), warga negara Vietnam, tidak mengajukan kasasi dan permohonan gransinya ditolak pada 30 Desember 2014.'],
            ['no'=>'Rani Andriani alias Melisa Aprilia, WNI asal Cianjur, Jawa Barat. Diputus PN pada tahun 2000. Grasi ditolak 30 Desember 2014.']
        ],
        'bawah'=>'Sejak 2000 hingga 2015 ini sudah ada 27 terpidana mati yang telah dieksekusi.'
    ],
    'ANALISA'=>'<img src="http://www.bijaks.net/assets/images/hotpages/hukumanmati/pic.jpg" class="pic">Indonesia memang memiliki dasar hukum untuk memberlakukan hukuman mati. Persoalannya adalah pada dampaknya. Hukuman mati tidak berdiri sendiri. Ada dampak yang akan mempengaruhi pada situasi politik, ekonomi dan kelangsungan hidup masyarakat Indonesia di negara lain. Setidaknya ada tiga persoalan yang membayangi Indonesia : 
                </p><p>Pertama, hukuman mati berpotensi dipolitisir dalam sistem hukum yang tidak transparan. Hukum di Indonesia masih mengandung kerumitan macam itu. Dalam kasus narkoba, hukuman mati masih belum menyentuh pelaku utama. Bagaiamana dengan oknum TNI dan Polri yang berperan sebagai pengedar bahkan melindungi gembong narkoba?
                </p><p>Kedua, dampak hubungan luar negeri. Semakin Indonesia keras kepala dalam memberlakukan hukuman mati, tidak menutup kemungkinan akan mempengaruhi hubungan ekonomi dan politik Indonesia dengan negara sahabat.
                </p><p>Ketiga, tenaga kerja kita di luar negeri banyak yang bermasalah. Badan Nasional Penempatan dan Perlindungan Tenaga Kerja Indonesia (BNP2TKI) telah mencatat, di tahun 2015 akan ada lima sampai tujuh kasus terkait hukuman gantung dan hukuman mati untuk TKI. Ada di Arab Saudi, Malaysia, Emirat Arab dan Hongkong. Semantara Indonesia mati-matian membebaskan warga negaranya yang terancam hukuman mati. Pada prinsipnya, Indonesia merasa ngeri dengan hukuman mati tersebut.',
    'DIGEMPUR'=>'Kepala Badan Narkotika Nasional (BNN) Anang Iskandar menegaskan Indonesia saat ini telah menjadi sasaran jaringan narkotika internasional. Kurang lebih 40-50 jaringan internasional dan nasional. Kebanyakan berasal dari Afrika, China, Iran, Belanda dan Australia. Mereka tidak main sendirian, tetapi membentuk jejaring internasional yang sangat rapi. Lemahnya penegakan hukum di Indonesia menjadi salah satu faktor meluasnya narkotika dan kasus kejahatan luar lainnya di Indonesia.',
    'BERITA'=>[
        ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/90d6bf2588f6c43832088a3036f77ce8bc229e1c.jpg','shortText'=>'Eksekuti Mati Tertunda di Tangan Jaksa Parsetyo','link'=>'http://www.bijaks.net/scandal/index/15637-eksekuti_mati_tertunda_di_tangan_jaksa_parsetyo'],
        ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/9f56af2ea29c3c05bf5b6953f2929fc91f10b3c7.jpg','shortText'=>'Brasil dan Belanda Melawan Hukuman Mati','link'=>'http://www.bijaks.net/scandal/index/15675-brasil_dan_belanda_melawan_hukuman_mati'],
        ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/54e9c2b93beffc6a59ba7161b680a8676f38953e.jpg','shortText'=>'Sindikat Narkoba Lurah Mamasa','link'=>'http://www.bijaks.net/scandal/index/15781-sindikat_narkoba_lurah_mamasa'],
        ['img'=>'http://news.bijaks.net/uploads/2015/02/Inggris-Kecam-Hukuman-Mati-di-Indonesia-627x261.jpg','shortText'=>'Inggris Kecam Hukuman Mati di Indonesia','link'=>'http://www.bijaks.net/news/article/0-91498//inggris-kecam-hukuman-mati-di-indonesia'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/setya-novanto2.jpg','shortText'=>'Setya Novanto Minta Hukuman Mati Tidak Dilakukan Massal','link'=>'http://www.bijaks.net/news/article/0-86934/setya-novanto-minta-hukuman-mati-tidak-dilakukan-massal'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/unduhan-33.jpg','shortText'=>'Hukuman Mati Pengedar Narkoba, BNN Contoh Singapura','link'=>'http://www.bijaks.net/news/article/0-86841/hukuman-mati-pengedar-narkoba-bnn-contoh-singapura'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/surya-paloh.jpg','shortText'=>'Surya Paloh Dukung Penuh Hukuman Mati Bagi Para Koruptor','link'=>'http://www.bijaks.net/news/article/0-86560/surya-paloh-dukung-penuh-hukuman-mati-bagi-para-koruptor'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/download-52.jpg','shortText'=>'Pemerintah Brazil: Hukuman Mati Bisa Rusak Hubungan Kedua Negara','link'=>'http://www.bijaks.net/news/article/0-85616/pemerintah-brazil-hukuman-mati-bisa-rusak-hubungan-kedua-negara'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/u100.jpg','shortText'=>'Dunia Desak RI Batalkan Hukuman Mati','link'=>'http://www.bijaks.net/news/article/0-85245//dunia-desak-ri-batalkan-hukuman-mati'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/sajut.jpg','shortText'=>'DPR Anggap Hukuman Mati Tak Sesuai dengan Karakter Bangsa','link'=>'http://www.bijaks.net/news/article/0-84880/dpr-anggap-hukuman-mati-tak-sesuai-dengan-karakter-bangsa'],
    ],
    'GELOMBANG'=>[
        'narasi'=>'Kebijakan hukum mati yang dikeluarkan Presiden Joko Widodo mendapatkan kecaman keras dari berbagai penjuru dari dalam dan luar negeri :',
        'isi'=>[
            ['title'=>'Dari dalam negeri','img'=>'assets/images/hotpages/hukumanmati/pic3.jpg','no'=>'Dari dalam negeri, protes datang dari peneliti aktivis Hak Asasi Manusia. Kontras, Komnas HAM, Setara Institute, Institut Titian Perdamaian. Lembaga Bantuan Hukum seleuruh Indonesia juga mengecam hukuman mati Jokowi. Mereka menempatkan kebijakan hukuman mati Jokowi sebagai indikator RAPOR MERAH 100 Hari kerja Jokowi-JK.'],
            ['title'=>'Pasca eksekusi mati warga negaranya','img'=>'assets/images/hotpages/hukumanmati/pic5.jpg','no'=>'Pasca eksekusi mati warga negaranya, Pemerintah Brazil dan Kerajaan Belanda menarik duta besarnya dari Indonesia.​ Tindakan tersebut sebagai protes keras terhadap kebijakan Jokowi yang dianggap tidak manusiawi.'],
            ['title'=>'Protes juga datang dari negara Inggris','img'=>'','no'=>'Protes juga datang dari negara Inggris. Kepada Wakil Presiden Jusuf Kalla dan Menteri Luar Negeri Retno, Menteri Luar Negeri Inggris, Philip Hammond mengecam keras Indonesia yang masih menggunakan hukuman mati.'],
            ['title'=>'Dari negera Australia','img'=>'assets/images/hotpages/hukumanmati/pic4.jpg','no'=>'Australia bereaksi keras terhadap pemerintah Indonesia yang tetap mengeksekusi dua warga negaranya, Andrew Chan dan Myuran Sukumaran. Australia mengancam akan memboikot pariwisata Indonesia. Menteri, bahkan Dubes mereka telah dipulangkan, sikap tersebut sebagai reaksi protes terhadap pemerintah Indonesia
                        </p><p>Luar Negeri Australia, Julie Bishop mengatakan “Saya pikir orang-orang Australia akan menunjukkan ketidaksetujuan mereka yang dalam terkait aksi itu, termasuk dengan membuat keputusan tentang ke mana mereka ingin melakukan liburan.“']
        ]
    ],
    'GELOMBANG2'=>[
        'narasi'=>'Sebanyak delapan terpidana mati telah dieksekusi di depan regu tembak Brimob Polda Jateng pada rabu 29/4/2015. Mereka adalah :<br>Duo Bali Nine',
        'isi'=>[
            ['no'=>'Andrew Chan ( Warga Negara Australia )'],
            ['no'=>'Myuran Sukumaran ( Warga Negara Australia )'],
            ['no'=>'Raheem Agbaje Salami ( Warga Negara Nigeria )'],
            ['no'=>'Zainal Abidin ( Warga Negara Indonesia )'],
            ['no'=>'Rodrigo Gularte ( Warga Negara Brazil )'],
            ['no'=>'Okwudili Oyatanze ( Warga Negara Nigeria )'],
            ['no'=>'Silvester Obiekwe ( Warga Negara Nigeria )'],
            ['no'=>'Martin Anderson ( Warga Negara Ghana )']
        ],
        'bawah'=>'Sementar dua terpidana mati lainnya yang sebelumnya masuk dalam daftar eksekusi gelombang kedua ditunda untuk dieksekusi, yakni Serge Areski Atlaoui warga negara Perancis dan Mary Jane Fiesta Veloso warga negara Filipina.',
        'lolos'=>'<img src="http://www.rancahpost.co.id/wp-content/uploads/2015/04/Mary-Jane-lolos-dari-eksekusi-mati-1-710x434.jpg" class="pic">Serge Areski Atlaoui, terpidana mati kasus narkoba untuk sementar lolos dari maut. Sebelumnya warga negaara Perancis yang divonis mati terkait perannya dalam pabrik ekstasi terbesar di Asia dan nomor tiga di dunia yang berlokasi di Serang, Banten. Serge lolos dari eksekusi mati karena sedang melakukan upaya hukum gugatan atas penolakan grasi oleh Presiden Jokowi ke Pengadilan Tata Usaha Negara.
                    </p><p>Mary Jane warga negara Filipina yang juga terpidana mati atas kasus narkoba ditunda pelaksanaannya beberapa jam sebelum dieksekusi. Mahkamah Agung mempertimbangkan desakan dari Pemerintah Fhilipina yang menyatakan Mary Jane diduga korban sindikat human trafficking internasional. Dugaan ini diperkuat setelah perekrut Mary Jane menyerahkan diri ke pihak kepolisian Fhilipina.'
    ],
    'PERINGKAT'=>[
        ['name'=>'Cina','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/125px-Flag_of_the_People%27s_Republic_of_China.svg.png','url'=>'','isi'=>'Cina termasuk yang paling getol menjalankan eksekusi mati. Tahun 2013 saja tercatat sebanyak 2400 tahanan menemui ajal di tangan algojo. Hukuman mati dipertontonkan di depan publik. Mayoritas penduduk mendukung hukuman mati. <br><b>Metode Hukuman Mati </b><br>Pada awalnya hukuman mati dilakukan dengan cara ditembak dibagian belakang kepala dengan satu peluru dari jarak dekat.  Karena banyak diprotes warga, Pemerintah Cina mengganti dengan "Bus Eksekusi". Yaitu eksekusi dengan suntikan mematikan dengan tangan terikat diatas tempat tidur dalam Bus Eksekusi.'],
        ['name'=>'Iran','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Flag_of_Iran.svg/125px-Flag_of_Iran.svg.png','url'=>'','isi'=>'Lebih dari 370 tahanan tewas lewat eksekusi mati tahun 2013 silam. Termasuk didalamnya jurnalis, aktivis HAM dan individu dengan dakwaan ringan.<br><b>Metode Hukuman Mati </b><br>Iran memiliki tiga metode eksekusi, yakni tembak mati, hukuman gantung atau rajam. Sama seperti di Cina, hukum di Iran mewajibkan pelaksanaan hukuman mati di depan publik.'],
        ['name'=>'Irak','img'=>'http://www.bijaks.net/public/upload/image/politisi/irak5371bcc81e12c/badge/b6ff0864f180b7b932af06838d7c4735094e86df.png','url'=>'http://www.bijaks.net/aktor/profile/irak5371bcc81e12c','isi'=>'Hukuman mati di Irak terutama marak digunakan sebagai instrumen kekuasaan pada masa diktator Sadam Husein. Tahun 2013 Irak mengeksekusi 177 tahanan yang sebagian besar tersangka teroris. Sementara 1.724 lainnya masih mendekam di penjara dan menunggu regu penembak beraksi.<br><b>Metode Hukuman Mati </b><br>Metode paling sering digunakan dengan pemenggalan kepala dan ditembak.'],
        ['name'=>'Arab Saudi','img'=>'http://www.bijaks.net/public/upload/image/politisi/arabsaudi538175cd1a906/badge/cd0509f5929a33f696c75d244811d89546bba4b2.png','url'=>'http://www.bijaks.net/aktor/profile/arabsaudi538175cd1a906','isi'=>'Lebih dari 80 tahanan tewas di tangan algojo di Arab Saudi 2013 lalu, termasuk di antaranya tiga remaja yang berusia di bawah 18 tahun. Kasus berkisar antara pembunuhan, penyeludupan hingga praktik dukun.<br><b>Metode Hukuman Mati </b><br>Metode paling sering digunakan dengan pemenggalan kepala.'],
        ['name'=>'Amerika Serikat','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flag_of_the_United_States.svg/125px-Flag_of_the_United_States.svg.png','url'=>'http://www.bijaks.net/aktor/profile/amerikaserikat519c86ef28572','isi'=>'Sedikitnya 80 vonis hukuman mati dijatuhkan tahun 2013 di Amerika Serikat. Saat yang bersamaan 39 tahanan dieksekusi.<br><b>Metode Hukuman Mati </b><br>Pilihan hukuman di AS dengan menggunakan dengan menggunakan suntikan racun. Pernah ada seorang tahanan sekarat selama 39 menit setelah mendapat suntikan racun.'],
        ['name'=>'Indonesia','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/125px-Flag_of_Indonesia.svg.png','url'=>'','isi'=>'Sebaliknya orang nomer satu di Istana Negara itu berjanji akan segera melaksanakan sejumlah eksekusi yang tertunda. 2013 lalu Indonesia menghukum mati lima tahanan, kebanyakan tersangkut kasus penyeludupan obat-obatan terlarang.']
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Joko Widodo','content'=>'"Vonis hukuman mati itu dari pengadilan, bukan dari Presiden. Polri, Kejaksaan Agung, dan instansi lain harus menegakkan hukum betul dengan tegas, termasuk narkoba. Kalau mereka minta pengampunan, tak ada pengampunan."','jabatan'=>'Presiden RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19'],
        ['from'=>'Kombespol Sumirat Dwiyanto','content'=>'"Siapapun yang memasukan narkoba ke sana termasuk kemarin ada warga Australia yang memasukan narkoba ke sana pun dieksekusi mati oleh Singapura, akhirnya apa, peredaran narkotika di Singapura itu, jarang sekali." ','jabatan'=>'BNN','img'=>'http://www.bijaks.net/public/upload/image/politisi/sumiratdwiyanto54cb16fc7db98/badge/8acaf9dc218afee75aa66954d77fd2e202bef68e.jpg','url'=>'http://www.bijaks.net/aktor/profile/sumiratdwiyanto54cb16fc7db98'],
        ['from'=>'Yusril Ihza Mahendra','content'=>'"Ancaman narkotika bagi bangsa dan negara ini sangat serius, karena bisa menghancurkan masa depan generasi muda, dan ini terkait kejahatan terorganisir internasional. Karena itu saya setuju hukuman mati bagi mereka yang mengedarkan, tapi tidak bagi para pemakai."','jabatan'=>'Menteri Hukum Indonesia tahun 1999 hingga 2004','img'=>'http://www.bijaks.net/public/upload/image/politisi/yusrilihsamahendra51675526bbc70/badge/030eee299b8489acd2955ff3328a160310712c35.jpg','url'=>'http://www.bijaks.net/aktor/profile/yusrilihsamahendra51675526bbc70'],
        ['from'=>'Ahmad Heryawan','content'=>'"Kita menghargai komitmen Pak Presiden Jokowi untuk menjaga anak bangsa ini dari bahaya narkoba. Kita apresiasi ketegasan beliau terhadap para pengedar, cukong narkoba."','jabatan'=>'Gubernur Jawa Barat','img'=>'http://www.bijaks.net/public/upload/image/politisi/ahmadheryawanheryawan51c91b31197b8/badge/f9eaef3b06035d43e036fab74738deaadcbf417f.jpg','url'=>'http://www.bijaks.net/aktor/profile/ahmadheryawanheryawan51c91b31197b8'],
        ['from'=>'Muhammad Prasetyo','content'=>'"Mudah-mudahan ini akan memiliki efek jera."','jabatan'=>'Jaksa Agung','img'=>'http://www.bijaks.net/public/upload/image/politisi/muhammadprasetyo54d46b2010b68/badge/956df1c81c9bd7793bde99b52c6aa938cfa9fe96.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadprasetyo54d46b2010b68'],
        ['from'=>'Aidil Chandra Salim','content'=>'"Hukuman matinya sudah benar. Tinggal kita harus mempercepat eksekusi mereka begitu peninjauan kembali di Mahkamah Agung ditolak."','jabatan'=>'Deputi Hukum dan Kerja Sama Badan Narkotika Nasional','img'=>'http://upload.wikimedia.org/wikipedia/id/b/b1/Aidil_Chandra_Salim.jpg','url'=>'http://id.wikipedia.org/wiki/Aidil_Chandra_Salim'],
        ['from'=>'Surya Paloh','content'=>'"Setuju sekali (koruptor dihukum mati). Karena memang ada hukumnya untuk itu."','jabatan'=>'Pendiri Partai NasDem','img'=>'http://www.bijaks.net/public/upload/image/politisi/suryapaloh511b4aa507a50/badge/3bbe78f966747a478c9416e6361a5bc28202f63d.jpg','url'=>'http://www.bijaks.net/aktor/profile/suryapaloh511b4aa507a50'],
        ['from'=>'Mahfud MD','content'=>'"Coba dalam waktu dekat ini ada koruptor yang dihukum mati." ','jabatan'=>'Mantan Ketua MK','img'=>'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/thumb/profdrmohammadmahfudmdshsu512c527ac591b_20130226_061354.jpg','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b'],
        ['from'=>'Setyo Novanto','content'=>'"Namun perlu dipertimbangkan berbagai aspek misalnya pelaksanaan hukuman mati tidak dilakukan secara massal,” ujarnya dalam seminar." ','jabatan'=>'Ketua DPR RI','img'=>'http://news.bijaks.net/uploads/2015/01/setya-novanto2.jpg','url'=>'http://id.wikipedia.org/wiki/Setya_Novanto']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Usman Hamid','content'=>'"Karena pada hakikatnya, hukuman mati mengambil hak hidup yang melekat pada setiap manusia. Alasan lainnya, ada banyak dalil para pendukung hukuman mati yang tidak terbukti."','jabatan'=>'Pendiri Kontras','img'=>'http://www.bijaks.net/public/upload/image/politisi/usmanhamid54d41a069aa7a/badge/620b5a25d4fd36497c8e05c3eaaca075dd9b5593.jpg','url'=>'http://www.bijaks.net/aktor/profile/usmanhamid54d41a069aa7a'],
        ['from'=>'Bonar Tigor Naipospos','content'=>'"Jokowi enggak paham HAM. Sayangnya Menko Polhukam dan Kejagung juga enggak peka HAM. Jokowi harus moratorium hukuman mati. Pemerintah-DPR (harus) bahas draft KUHAP dan hapuskan hukuman mati dari pidana kita." ','jabatan'=>'Setara Institute','img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/08/27/283626/MolJ7snx4V.jpg?w=700','url'=>'http://en.wikipedia.org/wiki/Setara_Institute'],
        ['from'=>'Georg Witschel','content'=>'"Jerman, seperti seluruh anggota Uni Eropa lainnya, menentang diberlakukannya hukuman mati. Hal itu tidak efektif dalam menurunkan angka kejahatan. Terutama dalam kasus pelanggaran narkotik." ','jabatan'=>'Duta Besar Jerman untuk Indonesia','img'=>'http://whatsnewjakarta.com/quotesfile/germanygeorgwitschel2.jpg','url'=>'http://de.wikipedia.org/wiki/Georg_Witschel'],
        ['from'=>'Mgr Ignasius Suharyo Pr','content'=>'"Tak ada seorang pun yang berhak atas hidup orang lain. Kejahatan di sana (Cina dan Amerika Serikat) tetap saja tinggi dan teori hukuman mati memberikan efek jera itu sangat tidak terbukti."','jabatan'=>'Uskup Agung Jakarta','img'=>'http://parokiarnoldus.net/wp-content/uploads/2014/04/sajut-mgr-ign-suharyo-pr-hidup-katolik.jpg','url'=>'http://id.wikipedia.org/wiki/Ignatius_Suharyo'],
        ['from'=>'Natalius Pigai','content'=>'"Kebijakan eksekusi mati harus dihapus di Indonesia. Efek jera itu, kan, untuk orang yang bersalah, bukan untuk orang lain."','jabatan'=>'Komisioner Komisi Nasional Hak Asasi Manusia (Komnas HAM)','img'=>'http://upload.wikimedia.org/wikipedia/id/9/92/Natalius_Pigai.jpg','url'=>'http://www.bijaks.net/aktor/profile/nataliuspigai51a5607bce402'],
        ['from'=>'Andreas Harsono','content'=>'"Pemerintah Joko Widodo harus memahami bahwa eksekusi mati ialah hukuman yang sangat barbar dan tak mengurangi kejahatan."','jabatan'=>'Peneliti dan Aktivis HAM','img'=>'http://www.internetfreedomfellows.com/wp-content/uploads/2013/02/Harsano21.jpg','url'=>'http://www.andreasharsono.net/'],
        ['from'=>'Bert Koenders','content'=>'"Ini adalah hukuman kejam dan tidak manusiawi. Hukuman ini tidak dapat diterima oleh martabat dan integritas kemanusiaan."','jabatan'=>'Menteri Luar Negeri Belanda','img'=>'http://cdn.oneworld.nl/sites/oneworld.nl/files/bert_koenders_0.jpg','url'=>'http://en.wikipedia.org/wiki/Bert_Koenders'],
        ['from'=>'Human Rights Watch','content'=>'"Pemerintah Indonesia yang mengejar grasi bagi Ahmad di Arab Saudi sementara terus memberlakukan hukuman mati adalah kemunafikan terhadap hak untuk hidup." ','jabatan'=>'','img'=>'http://www.hrw.org/sites/all/themes/hrw/images/logo.png','url'=>'http://www.hrw.org/'],
        ['from'=>'Inang Winarso','content'=>'"Mereka bukan bandar utama, tapi kurir."','jabatan'=>'Direktur Eksekutif Perkumpulan Keluarga Berencana Indonesia (PKBI)','img'=>'http://jjk.co.id/wp-content/uploads/2015/01/closer-look-inang-winarso-1024x974.jpg','url'=>''],
        ['from'=>'Rupert Abbott','content'=>'"Ini akan menjadi kemunduran besar jika pemerintah meneruskan rencana mengeksekusi 20 orang tersebut tahun ini." ','jabatan'=>'Direktur Riset Asia Tenggara dan Pasifik Amnesty International','img'=>'http://www.rfa.org/vietnamese/in_depth/desp-imp-r-con-rem-vn-05232013065612.html/Rupert-new-170-rfa.jpg/image','url'=>'http://uk.linkedin.com/pub/rupert-abbott/10/b28/2a7'],
        ['from'=>'Federica Mogherini','content'=>'"Pengumuman akan dilaksanakannya eksekusi mati terhadap enam terpidana narkoba di Indonesia, termasuk seorang warga Negara Belanda, sangat disesalkan." ','jabatan'=>'Perwakilan Tinggi Uni Eropa Urusan Luar Negeri dan Kebijakan Keamanan','img'=>'https://pbs.twimg.com/profile_images/528268962505568258/_1M6gJaT_400x400.jpeg','url'=>'http://en.wikipedia.org/wiki/Federica_Mogherini'],
        ['from'=>'Philip Hammond','content'=>'"Kami menegaskan sikap Inggris dan Uni Eropa pada umumnya yang menentang hukuman mati." ','jabatan'=>'Menteri Luar Negeri Inggris','img'=>'http://i2.mirror.co.uk/incoming/article282447.ece/alternates/s615/philip-hammond-pic-pa-550274315.jpg','url'=>'http://en.wikipedia.org/wiki/Philip_Hammond']
    ],
    'FOTO'=>[
        ['img'=>'http://img2.bisnis.com/bali/posts/2015/01/18/48891/myuran-sukumaran-bali-nine.jpg'],
        ['img'=>'http://gdb.voanews.com/C1328428-5AFC-4CEB-B5A9-EF533B5CBC14_mw1024_s_n.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/11/23/322566/FxYaAKDWha.jpg?w=668'],
        ['img'=>'http://cdn.tmpo.co/data/2015/03/02/id_375666/375666_620.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2014/12/14/071807820141214-065642-resized780x390.JPG'],
        ['img'=>'https://img.okezone.com/content/2015/02/26/18/1110810/abbott-yakin-jokowi-pertimbangkan-pembatalan-hukuman-mati-1i7gWnmtsJ.jpg'],
        ['img'=>'http://sosialberita.net/wp-content/uploads/2015/04/Mary-Jane.png'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/mary-jane-terpidana_20150325_081432.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/821236/big/052763900_1425385883-983198-75190548-af25-11e4-90ab-ef3fd79aaa94.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/04/29/5019a3fe-c223-4f07-9fa8-b92d6897d6c2_169.jpg?w=650'],
        ['img'=>'http://images.solopos.com/2015/02/FOTO-HUKUMAN-MATI-_-Begini-Cara-Polisi-Kawal-Terpidana-Mati-370x211.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/warga-filipina-tuntut-pembatalan-hukuman-mati-mary-jane-veloso_20150424_163342.jpg'],
        ['img'=>'http://assets.rappler.com/612F469A6EA84F6BAE882D2B94A4B421/img/81798B43C54B47EBA42D144813C9BEB7/mary-jane_81798B43C54B47EBA42D144813C9BEB7.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/04/28/1954036Puluhan-pemuda-yang-tergabung-di-dalam-Komunitas-SantEgidio-Kupang-menggelar-aksi-1.000-lilin-untuk-menolak-hukuman-mati-terhadap-terpidana-mati-Mary-Jane-780x390.JPG'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/03/12/c0830254-4c12-4fe0-a35f-9a524e6ed97e_169.jpg?w=650'],
        ['img'=>'http://resources1.news.com.au/images/2015/04/27/1227322/462173-54cebcfa-ec52-11e4-a82d-68ff75e6542c.jpg'],
        ['img'=>'http://images.solopos.com/2015/04/FOTO-HUKUMAN-MATI-.-Eksekusi-Mary-Jane-Langgar-HAM-Kenapa.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/03/04/337/1113379/terpidana-mati-asal-spanyol-dipindah-ke-nusakambangan-xvN7Jx6UoS.jpg'],
        ['img'=>'http://cdn-media.viva.id/thumbs2/2015/04/28/310599_kunjungan-terakhir-keluarga-tereksekusi-hukuman-mati_663_382.jpg'],
        ['img'=>'http://cdn-media.viva.id/thumbs2/2015/04/28/310602_kunjungan-terakhir-keluarga-tereksekusi-hukuman-mati_663_382.jpg'],
        ['img'=>'http://www.covesia.com/photos/berita/280115121606_jadwal-pelaksanaan-eksekusi-terpidana-mati-bali-nine.jpeg'],
        ['img'=>'http://assets2.jpnn.com/picture/normal/20150426_071415/071415_145090_nusakambangan_Myuran.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/795549/big/083237200_1421313692-jaksa_3.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2015/02/23/id_373651/373651_620.jpg']
    ],
    'VIDEO'=>[
        ['id'=>'FXt-6s5KvWo'],
        ['id'=>'89ehMVZ8YPQ'],
        ['id'=>'jjmDYeI6ytU'],
        ['id'=>'1ScHuyqZtiY']
    ]
]

?>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/top2.jpg")?>') no-repeat transparent;
        height: 1352px;
        margin-bottom: -700px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kiri p, .col_kiri li, .boxprofile p, .boxprofile li, .font_kecil {
        font-size: 12px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        margin-top: -30px;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
        background: rgba(0, 0, 0, 0.7);
    }
    .block_red {
        background-color: black;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 80%;
        height: auto;
        margin: 0 auto;
        display: inherit;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .garis {
        border-top: 1px dashed black;
    }
    .boxgray {
        width: 99%;
        border: 5px solid lightgray;
        box-shadow: -5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }
    .penyokong {
        width: 100%;
        height: auto;
        display: inline-block;
        margin-left: 20px;
    }
    .boxpenyokong {
        width: 126px;
        height: auto;
    }
    .foto {
        width: 100px;
        height: 100px;
        border: 3px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
        padding: 10px 10px;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: 125px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 100px;height: 115px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi2 img {width: 98px; height: 90px;  padding: 0px !important;}
    li.organisasi2 p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 450px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
    li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.dukung {float: left; padding:3px;vertical-align: top;width: 45%;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }   
    .uprow {margin-left: 20px;margin-top:10px;border-color: #29166f transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {
        width: 260px;
        height: auto;
        margin-left: 20px;
        margin-top:0px;
        margin-bottom: 10px;
        background-color: #424040;
        background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px #29166f;
        border-right:solid 2px #29166f;
        border-bottom:solid 2px #29166f;
        border-radius: 0 0 5px 5px;
        color: #ffffff;
        z-index: 100;
    }
    .kronologi-title {font-size:14px;font-weight:bold;background-color: #29166f;color: white;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {line-height: 15px;font-size:12px;}

    .ketua {
        background-color: yellow;
        width: 30%;
        display: block;
        float: left;
        margin-right: 3%;
        box-shadow: -3px 3px 10px gray;
        border-radius: 8px;
        margin-bottom: 15px;
    }
    .ketua img {
        width: 100%;
        height: 200px;
    }
    .kritik {
        font-size: 18px;
        font-weight: bold;
        color: black;
    }
    .isi-col50 {
        float: left;
        margin: 10px 16px;
        width: 41%;
        height: auto;
        display: inline-block;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #29166f;
        width: 100%;
        height: auto;
        float: left;
        margin-bottom: 10px;
        padding-top: 5px;
        margin-left: 10px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        float: left;
        border: 1px solid black;
    }
    #bulet {
        background-color: #ffff00; 
        text-align: center;
        width: 50px;
        height: 25px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: black;
        padding: 6px 10px;
        margin-left: -10px;
        margin-right: 10px;
    }
    .quote {
        float: left;
        margin: 10px 16px;
        width: 29%;
        height: auto;
        display: inline-block;
        border-bottom: 1px solid black;
    }

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <h3><a id="hukumanmati" class="black"><?php echo $data['NARASI']['title'];?></a></h3>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div><br>

                <h4 class="list"><a id="analisa" style="color: black;">ANALISA</a></h4>
                <p class="font_kecil"><?php echo $data['ANALISA'];?></p>
                <div class="clear"></div><div class="garis"></div><br>

                <h4 class="list"><a id="terpidana" style="color: black;">EKSEKUSI MATI GELOMBANG PERTAMA</a></h4>
                <p><?php echo $data['TERPIDANA']['narasi'];?></p>
                <div class="clear"></div>
                <ol>
                    <?php
                    foreach ($data['TERPIDANA']['isi'] as $key => $val) {
                        echo "<li style='margin-left: 20px;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
                <p><?php echo $data['TERPIDANA']['bawah'];?></p>
                <div class="clear"></div><div class="garis"></div><br>

                <h4 class="list"><a id="eksekuasidua" style="color: black;">EKSEKUSI MATI GELOMBANG KEDUA</a></h4>
                <p><?php echo $data['GELOMBANG2']['narasi'];?></p>
                <ol>
                    <?php
                    foreach ($data['GELOMBANG2']['isi'] as $key => $val) {
                        echo "<li style='margin-left: 20px;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
                <p><?php echo $data['GELOMBANG2']['bawah'];?></p>
                <p style="font-weight: bold;font-size: 16px;">Lolos Dari Maut</p>
                <p><?php echo $data['GELOMBANG2']['lolos'];?></p>
                <div class="clear"></div><div class="garis"></div><br>

                <h4 class="list"><a id="digempur" style="color: black;">DIGEMPUR JARINGAN NARKOBA DUNIA</a></h4>
                <p><?php echo $data['DIGEMPUR'];?></p>
                <div class="clear"></div><div class="garis"></div><br>
        
                <h4 class="list"><a id="daftar" style="color: black;">DAFTAR TUNGGU EKSEKUSI MATI</a></h4>
                <p><?php echo $data['DAFTAR']['narasi'];?></p>
                <ol>
                    <?php
                    foreach ($data['DAFTAR']['isi'] as $key => $val) {
                        echo "<li style='margin-left: 20px;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>

                <p style="font-weight: bold;font-size: 16px;"><a id="balinine" style="color: black;">"BALI NINE"</a></p>
                <p><?php echo $data['BALI_NINE']['narasi'];?></p><div class="clear"></div>
                <ol>
                    <?php
                    foreach ($data['BALI_NINE']['isi'] as $key => $val) {
                        echo "<li style='margin-left: 20px;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
                <p><?php echo $data['BALI_NINE']['bawah'];?></p>
                <div class="clear"></div><div class="garis"></div><br>

                <h4 class="list"><a id="protes" style="color: black;">GELOMBANG PROTES</a></h4>
                <p><?php echo $data['GELOMBANG']['narasi'];?></p>
                <ul class='list2'>
                    <?php
                    foreach ($data['GELOMBANG']['isi'] as $key => $val) { ?>
                        <li style='font-weight: bold;'><?php echo $val['title'];?></li>
                        <p><?php
                        if ($val['img'] != "") { ?>
                            <img src="<?php echo base_url().$val['img'];?>" class='pic3'>
                        <?php } ?>
                        <?php echo $val['no'];?></p>
                    <?php
                    }
                    ?>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="col_kanan">
                <div class="boxprofile black">
                    <h4 class="block_red text-center"><a id="pasal" style="color: white;">PASAL MEMBUNUH</a></h4>
                    <p style="margin-left: 20px;margin-right: 20px;margin-top: 10px;">
                        <img src="http://bijaks.portal/assets/images/hotpages/hukumanmati/pasal.png" class="picprofil">
                    </p>
                    <p style="margin-left: 20px;margin-right: 20px;color: white;"><?php echo $data['PASAL_MEMBUNUH']['narasi'];?></p>
                    <ol>
                        <?php
                        foreach ($data['PASAL_MEMBUNUH']['isi'] as $key => $val) {
                            echo "<li style='color: white;margin-left: 10px;margin-right: 20px;'>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div>

                <h4 class="list" style="margin-left: 15px;margin-top: 25px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 15px;background-color: #E5E5E5;">
                    <div id="newshukuman_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newshukuman_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div>

                <h4 class="list" style="margin-left: 15px;"><a id="peringkat" style="color: black;">PERINGKAT NEGARA DENGAN HUKUMAN MATI TERBANYAK</a></h4>
                <div style="margin-left: 15px;">
                    <?php
                    $no=1;
                    foreach($data['PERINGKAT'] as $key=>$val){
                        ?>
                        <div class="col_kanan2 black boxdotted">
                            <?php echo "<span style='font-size: 16px;font-weight: bold;'><span id='bulet'>".$no."</span>".$val['name']."</span>";?>                                
                            <p style="font-size: 12px;color: black;margin-left: 10px;margin-right: 10px;margin-top: 15px;">
                                <a href="<?php echo $val['url'];?>"><img class="bendera" src="<?php echo $val['img'];?>" alt="<?php echo $val['isi'];?>" /></a>
                                <?php echo $val['isi'];?>
                            </p>                                
                        </div>
                        <div class="clear"></div>
                        <?php
                        $no++;
                    }
                    ?>
                </div>
            </div>
            <div class="clear"></div><div class="garis"></div>

            <div class="col_kiri50">
                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <div class="boxgray_green" style="height: auto;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 10px;" class="text-center">PARPOL PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                            }
                            else {
                                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            }
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 10px;" class="text-center">INSTITUSI PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi2">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                    
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 10px;" class="text-center">TOKOH PENDUKUNG</p>
                    <ul>
                        <?php
                        foreach($data['PENDUKUNG']['tokoh'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi2">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col_kanan50">
                <h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <div class="boxgray_red" style="height: auto;">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 10px;" class="text-center">TOKOH PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['tokoh'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>

                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 10px;" class="text-center">INSTITUSI PENENTANG</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="">
                                <li class="organisasi2">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>

                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 10px;" class="text-center">NEGARA YANG MENGANCAM</p>
                    <ul>
                        <?php
                        foreach($data['PENENTANG']['negara'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi2">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div>
            </div>

            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
                <div class="clear"></div>
                <div class="boxgray" style="display: inline-block;height: auto;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <div>
                            <?php 
                            for($i=0;$i<=2;$i++){
                            ?>
                            <div class="quote">
                                <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>">
                                    <img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=3;$i<=5;$i++){
                            ?>
                            <div class="quote">
                                <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>">
                                    <img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=6;$i<=8;$i++){
                            ?>
                            <div class="quote">
                                <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>">
                                    <img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                            </div>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            </div>

            <div class="clear"></div>
            <h4 class="list"><a id="quotepenentang" style="color: black;">QUOTE PENENTANG</a></h4>
            <div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="display: inline-block;height: auto;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <div>
                            <?php 
                            for($i=0;$i<=2;$i++){
                            ?>
                            <div class="quote">
                                <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                    <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=3;$i<=5;$i++){
                            ?>
                            <div class="quote">
                                <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                    <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 55px; height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div>
                            <?php 
                            for($i=6;$i<=8;$i++){
                            ?>
                            <div class="quote">
                                <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>">
                                    <img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 55px;float: left;margin-right: 10px;">
                                </a>
                                <p style="font-size: 12px;font-weight: bold;margin-top: 5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?></p>
                                <p style="font-size: 11px;line-height: 14px;margin-top: -10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p><br>
                                <p style="font-style: italic;font-size: 11px;color: red;line-height: 14px;margin-top: 0px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                            </div>
                            <?php } ?>
                        </div>
                    </ul>
                </div>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
            <div class="col_full" style="display: inline-block;">
                <ul style="margin-left: 0px;margin-top: 0px;">
                    <?php
                    $no=1;
                    foreach ($data['FOTO'] as $key => $val) { ?>
                        <li class="gallery">
                            <a href="#" data-toggle="modal" data-target="#img-<?php echo $no;?>">
                                <img src="<?php echo $val['img'];?>" />
                            </a>
                        </li>
                        <div class="modal hide fade" style="width: auto;" id="img-<?php echo $no;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Gallery Foto</h4>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?php echo $val['img'];?>" />
                                    </div>
                                    <!-- <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    <?php
                    $no++;
                    }
                    ?>
                </ul>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>

            <div class="col_full">
                <div class="boxgray" style="height: 710px;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <!-- jika ingin border atas bawah none gunakan mqdefault -->
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>