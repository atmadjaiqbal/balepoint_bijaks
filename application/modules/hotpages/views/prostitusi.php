<?php 
$data = [
    'NARASI'=>[
        'title'=>'BISNIS PROSTITUSI KELAS WAHID',
        'narasi'=>'<img src="http://static.republika.co.id/uploads/images/detailnews/sejumlah-tersangka-jaringan-prostitusi-dengan-sistem-online-yang-berhasil-_130212162808-515.jpg" class="pic">Menjamurnya prostitusi online jelas menambah daftar lokalisasi terselebung, seperti lokalisasi modus indekost, Penginapan dan Hotel atau Apartemen. Modus transaksinya lewat pesan Blackberri atau WA dengan mucikari, transfer uang muka lalu kencan dengan pelacur.  Hal ini kemudian membuat pemerintah gencar melakukan razia untuk memberantas praktek pelacuran modus online.
                    </p><p>Beberapa kasus yang diungkap oleh kepolisian, seperti penangkapan mucikari di Apartemen Kalibata City, yang menjajakan puluhan perempuan cantik. Pengungkapan kasus pembunuhan yang dialami Deuhdeuh, seorang PSK online yang dibunuh oleh pelanggangnya di kamar kostnya. Dan yang paling baru adalah terbongkarnya sindikat prostitusi online kelas wahid yang terdiri dari artis dan model, yang dijalankan oleh seorang mucikari berinisial “AR”.'
    ],
    'PROFIL'=>[
        'satu'=>'Kota Yogyakarta',
        'dua'=>'Islam, Kejawen',
        'tiga'=>'Jawa 1755-1950, Belanda 1755-1811; 1816-1942, Inggris 1811-1816,  Jepang 1942-1945, Indonesia 1945-1950',
        'empat'=>'Monarki (kesultanan) Sultan',
        'list'=>[
            ['no'=>'“Barang siapa yang pencahariannya dan kebiasaannya yaitu dengan sengaja mengadakan atau memudahkan perbuatan cabul dengan orang lain dihukum penjara selama-lamanya satu tahun empat bulan atau denda sebanyak-banyaknya Rp. 15.000,-.”'],
            ['no'=>'Sultan terakhir sebelum penurunan status negara (1940-1950; wafat 1988) - ISKS Hamengku Buwono IX'],
            ['no'=>'Sekarang (sejak 1989) - ISKS Hamengku Buwono X Pepatih Dalem (Menteri Pertama)'],
            ['no'=>'Pertama (1755-1799) - Danurejo I'],
            ['no'=>'Terakhir (1933-1945) - Danurejo VIII']
        ]
    ],
    'GRATIFIKASI'=>[
        'narasi'=>'<img src="http://www.aktualpost.com/wp-content/uploads/2015/05/Nama-17-Artis-Prostitusi-Online-yang-Diduga-Teman-AA.jpg" class="pic2">Pengakuan “AR” seputar bisnis prostitusi online yang digarapnya cukup menghebohkan. Beberapa inisial artis dan model disebut-sebut menjadi PSK dengan tarif yang cukup fantastis. Untuk dapat menikmati layanan jasa PSK artis, para pelanggangnya harus merogoh kocek yang dalam-dalam. Untuk sekali goyang saja, tarifnya berkisar antara 20 juta sampai 200 juta.
                    </p><p class="font_kecil">Bisnis syahwat dengan harga selangit ini kemudian menuai polemik dan pertanyaan dari beragam kalangan. Siapa pelanggan yang mau mengeluarkan uang sebanyak itu hanya untuk sekali kencan? 
                    </p><p class="font_kecil"><img src="http://www.rmoljakarta.com/images/berita/normal/910679_10261124042015_prostitusi_online.jpg" class="pic">Banyak yang berpendapat para pejabat dan pengusaha kaya yang paling bisa menikmati layanan esek-esek mahal tersebut. Hal ini kemudiaan dikaitkan dengan praktek gratifikasi seks yang kerap dilakukan oleh para pejabat yang korup. Jadi tidak menutup kemungkinan jasa seks artis ini kerap jadikan sebagai sarana pencucian uang dan gratifikasi seks oleh para pelaku korupsi. Apalagi "AR" sendiri mengakui bahwa salah satu pengguna prostitusi online datang dari kalangan pejabat dan pengusaha. Bahkan, Kepala Badan Reserse Kriminal Mabes Polri Komisaris Jenderal Budi Waseso tidak menampik jika kasus tersebut memiliki keterlibatan dengan sejumlah pejabat teras.'
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="https://i.ytimg.com/vi/IKW_28iIbuI/hqdefault.jpg" class="pic2">Terkait  maraknya praktek prostitusi yang berhasil diungkap oleh kepolisian menuai beragam tanggapan dari berbagai kalangan. Gubernur Ahok misalkan, mewacanakan akan menertibkan para PSK dengan cara melokalisirnya dengan cara menyediakan tempat khusus. Solusi yang ditawarkan ini untuk meminimalisir penyebaran praktek-praktek prostitusi yang semakin meresahkan.
                    </p><p class="font_kecil">Menurut Ahok, cara ini tentu tidak bisa seratus persen bisa menekan praktek pelacuran di Jakarta, karena menurutnya praktek akan terus ada sehingga untuk memberantas tentulah sangat sulit. Jadi pilihan realistisnya adalah melokalisir, agar tertib dan tidak berserakan di mana-mana. Cara ini juga diyakini efektif untuk menekan angka jumlah pengguna jasa syahwat tersebut.
                    </p><p class="font_kecil">Namun wacana ini mendapat reaksi penolakan dari banyak pihak. Beberapa kalangan menganggap solusi yang ditawarkan oleh Gubernur Ahok tidak akan memberikan hasil apa-apa. Justru tawaran baiknya adalah memberlakukan aturan yang tegas soal pelarangan praktek prostitusi dengan cara merevisi Undang-Undang tentang prostitusi. Solusi lainnya adalah razia pelacuran harus gencar dilakukan dengan melibatkan banyak kalangan termasuk masyarakat dan kepolisian.'
    ],
    'PSK'=>[
        'narasi'=>'<img src="http://i.ytimg.com/vi/STTe1N2AO_g/hqdefault.jpg" class="pic2">Bisnis seks artis yang dijalankan oleh "AR" terbilang bisnis kelas satu. Dari keterangan sang Mucikari, harga sekali kencan dengan artis binaannya dipatok  mulai dari 20 juta sampai 200 juta. Tentu harga yang cukup mahal.Tarif selangit yang harus dikeluarkan oleh penikmat jasa ini karena yang dijajakan adalah artis dan model. Status artis atau model memang menjadi faktor utama yang bisa menjelaskan mengapa bisnis syahwat ini sangatlah mahal.
                    </p><p class="font_kecil">Selama ini, banyak yang menilai bahwa artis atau model pastilah wanita-wanita cantik dan seksi. Sehingga bagi yang memiliki uang lebih akan rela mengeluarkan uangnya agar dapat menikmati layanan seks artis atau model tersebut. Selain kepuasan sekseual yang didapat, seorang akan merasa bangga karena bisa menikmati kemolekan tubuh para artis ataupun model tersebut.
                    </p><p class="font_kecil">Berikut inisial artis dan model beserta tarifnya :',
        'harga'=>[
            ['no'=>'TB','jml'=>'200'],
            ['no'=>'JD','jml'=>'150'],
            ['no'=>'BB','jml'=>'70'],
            ['no'=>'RF','jml'=>'60'],
            ['no'=>'RF','jml'=>'60'],
            ['no'=>'MT','jml'=>'55'],
            ['no'=>'KA','jml'=>'55'],
            ['no'=>'SB','jml'=>'55'],
            ['no'=>'CW','jml'=>'50'],
            ['no'=>'PUA','jml'=>'45'],
            ['no'=>'NM','jml'=>'40'],
            ['no'=>'CT','jml'=>'40'],
            ['no'=>'LM','jml'=>'35'],
            ['no'=>'DL','jml'=>'30'],
            ['no'=>'BS','jml'=>'30'],
            ['no'=>'AA','jml'=>'25'],
            ['no'=>'FNP','jml'=>'20']
        ]
    ],
    'MODUS'=>[
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2013/07/03/1912467Ilustrasi-Prostitusi780x390.jpg" class="pic">Prostitusi online merupakan modus baru dalam praktek pelacuran yang ada di Indonesia. Modus ini berkembang massif seiring dengan berkembangnya tekhnologi yang semakin canggih. Di kota-kota besar seperti Jakarta, prostitusi online mewabah dan tumbuh subur. Di samping faktor tekhnologi, prostitusi online adalah fenomena perpindahan dari praktek  yang dilokalisir dalam satu tempat khusus (lokalisasi) ke pola praktek yang lebIh fleksibel, yakni transaksi ini bisa dilakukan dimana saja tergantung kesepakan antara PSK dan pelangganya.
                    </p><p class="font_kecil">Fenomena perpindahan ini juga disebabkan alasan ekonomi dan keamanan. Prostitusi online lebih efisien dari segi biaya karena para pelaku, khususnya PSK bisa menjalankan bisnis tersebut dengan solo karir (tidak menggunakan mucikari) sehingga tidak perlu berbagi hasil, selain itu mereka tidak perlu mengeluarkan biaya sewa tempat karena biasanya sewa tempat ditangung oleh pelanggan. Soal keamanan juga menjadi faktor mengapa seks online menjamur. Para pelaku ini menganggap bahwa bisnis seks online lebih aman dijalankan karena transaksinya bersifat privat, yakni melalui pesan singkat atau via BBM.
                    </p><p class="font_kecil">Pelaku prostitusi biasanya bekerja dengan sangat teliti dan selektif sehingga aman dari jangkauan polisi.  Modus baru seputar pengelolaan bisnis prostitusi  bisa dipesan secara online dengan menggunakan media sosial seperti Twitter, SMS, BBM, dan lainnya.'
    ],
    'DAFTAR'=>[
        ['img'=>'http://cdn-2.tstatic.net/surabaya/foto/bank/images/2006dolly-peta.jpg','text'=>'Gang Dolly-Surabaya'],
        ['img'=>'http://www.nonstop-online.com/wp-content/uploads/2014/11/Ilustrasi-Gang-Kalijodo-.jpg','text'=>'Kramat Tunggak, Kali Jodoh-Jakarta'],
        ['img'=>'http://3.bp.blogspot.com/-90zeYNbGJL8/UKm6jo5xJpI/AAAAAAAAAzY/bL_Z5CnlKTE/s1600/unduhan.jpg','text'=>'Jl.Pajajaran-Malang'],
        ['img'=>'http://3.bp.blogspot.com/_G8TS4L9RjhM/TIACr0nly-I/AAAAAAAAAmU/CM12Lthm5DM/s320/jalan+pasar+kembang.jpg','text'=>'Sarkem-Yogyakarta'],
        ['img'=>'http://i.imgur.com/0JRzI.jpg','text'=>'Saritem-Bandung'],
        ['img'=>'http://jurnalpatrolinews.com/wp-content/uploads/2011/07/tempat-prostitusi.jpg','text'=>'Limusnunggal Cileungsi-Bogor'],
        ['img'=>'https://irs2.4sqi.net/img/general/width960/cg9Vt6fqunW7zz14Twr204QsJp-nOUnXsCZGRgfyjEE.jpg','text'=>'Sunan Kuning-Semarang'],
        ['img'=>'http://1.bp.blogspot.com/-bAJ0_udZJeQ/TZTC-PPWLEI/AAAAAAAAAFQ/B6JwD7VmjG4/s1600/Cewek_dugem.jpg','text'=>'Cileugeng Indah (CI)-Indramayu'],
        ['img'=>'http://cdn.tmpo.co/data/2010/08/04/id_43127/43127_620.jpg','text'=>'Lokalisasi Dadap-Tangerang'],
        ['img'=>'http://majuteruz.files.wordpress.com/2010/07/gg-sadar-banyumas.jpg','text'=>'Gang Sadar I & II-Baturraden,Purwokerto'],
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c']
        ],
        'institusi'=>[
            ['name'=>'Gubernur DKI','img'=>'http://statik.tempo.co/data/2012/10/24/id_146866/146866_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/pemerintahprovinsipemprovdki54c1c6c8c3ef3 '],
            ['name'=>'DPRD DKI JAKARTA','img'=>'http://jurnalpatrolinews.com/wp-content/uploads/2015/02/DPRD-DKI-.jpg','url'=>'http://www.bijaks.net/aktor/profile/dprddki541a5b6fb78db']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrat5119a5b44c7e4']
        ],
        'institusi'=>[
            ['name'=>'MUI','img'=>'http://upload.wikimedia.org/wikipedia/id/f/f5/Logo_MUI.png','url'=>'http://www.bijaks.net/aktor/profile/majelisulamaindonesia53097864039ce'],
            ['name'=>'NU','img'=>'http://www.realita.co/photos/bigs/20141112200922nu.jpg','url'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTC_zvkC5V5Vo6LkBdv5iQ0AIFRbhdmJXXs8CH5rTTgQL2CCf72'],
            ['name'=>'Muhammadiyah','img'=>'http://www.daririau.com/foto_berita/55Muhammadiyah.jpg','url'=>'http://www.bijaks.net/aktor/profile/ppmuhammadiyah5296b45eefcaa'],
            ['name'=>'IMWU','img'=>'http://www.satuharapan.com/uploads/pics/news_31280_1423907247.jpg','url'=>''],
            ['name'=>'Komisi III','img'=>'http://risamariska.com/wp-content/uploads/2015/04/risamariska.com-logo-dprri-box.jpg','url'=>'http://www.bijaks.net/aktor/profile/komisiiiidprri54d30e3f5bbd7'],
            ['name'=>'DPD RI','img'=>'http://fokusberita.co/wp-content/uploads/2015/04/DPD-RI.jpg','url'=>'http://www.bijaks.net/aktor/profile/dewanperwakilandaerah531d2cecd99f7'],
            ['name'=>'Forensik Polri','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTC_zvkC5V5Vo6LkBdv5iQ0AIFRbhdmJXXs8CH5rTTgQL2CCf72','url'=>'']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Fadli Zon','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://images.solopos.com/2014/06/Fadli-Zon-twitter.jpeg','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007','content'=>'Ini kan dari zaman Nabi Adam sudah ada prostitusi'],
        ['from'=>'Basuki Djahaja Purnama (Ahok)','jabatan'=>'Gubernur DKI Jakarta','img'=>'http://fajar.co.id/wp-content/uploads/2015/03/ahok13.jpg','url'=>'http://www.bijaks.net/aktor/profile/basukitjahajapurnama50f600df48ac5','content'=>'Yang makainya juga banyak oknum pejabat kok. Bisa bayar mahal mana ada duit (kalau bukan didapat dari hasil) duit korup. (Uang hasil) korup baru bisa pakai tuh (sewa PSK papan atas)'],
        ['from'=>'Abraham Lingguna (H. Lulung)','jabatan'=>'Anggota DPRD DKI Jakarta','img'=>'http://slazhpardede.net/wp-content/uploads/2015/03/Haji-Lulung-1.jpeg','url'=>'http://www.bijaks.net/aktor/profile/lulunglunggana5114dd9dd23b1','content'=>'Saya tidak mau komentarin Ahok lagi yah. Kalau soal pernyataan Ahok tanya yang lain, jangan gue. Tapi jangan lah, jangan menuduh orang sembarangan. Kalau belum cukup bukti'],
        ['from'=>'Farhat Abbas','jabatan'=>'Pengacara','img'=>'http://www.beranda.co.id/wp-content/uploads/2015/04/farhat-abbas1.jpg','url'=>'http://www.bijaks.net/aktor/profile/farhatabbas51cce8399d1cd','content'=>'Harusnya semua artis yang baik-baik, berkumpul dan protes dengan kejadian iklan artis jadi pelacur, bersihkan nama profesi mereka, pasti polisi tak ada bukti'],
        ['from'=>'AH Bimo Suryono','jabatan'=>'Praktisi hukum','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Boleh terlokalisasi asalkan jangan ada PSK di bawah umur. Kalau enggak berikan hukuman seberat-seberatnya seperti di negara lain'],
        ['from'=>'Moammar Emka','jabatan'=>'Budayawan','img'=>'http://www.indonesianfilmcenter.com/images/gallery/Moammar%20Emka4.jpg','url'=>'http://www.bijaks.net/aktor/profile/moammaremka555ac075d83d1','content'=>'Murni karena gaya hidup. Ada sebagian yang karena faktor ekonomi, tapi kalangan bawah. Motivasinya agar lebih bergaya. Kalau kalangan bawah, untuk hidup (faktor ekonomi). Kalau kalangan atas, motivasinya untuk bikin hidup lebih bergaya'],
        ['from'=>'Vicky Shu','jabatan'=>'Artis','img'=>'http://bengkuluekspress.com/wp-content/uploads/2012/06/vickyshu.jpg','url'=>'http://www.bijaks.net/aktor/profile/vickyveranitayudhasokavickyshu555ac4c4c82d9','content'=>'Hal itu bukan lagi menjadi rahasia umum di kalangan salebritis tanah air'],
        ['from'=>'Maia Estianty','jabatan'=>'Artis','img'=>'http://4.bp.blogspot.com/-RhCY6cdaGYs/VBwUoEdLcDI/AAAAAAAABJw/wVZGHESuuP0/s1600/Rahasia%2BDiet%2Bala%2BArtis%2BMaia%2BEstianty.jpg','url'=>'http://www.bijaks.net/aktor/profile/maiaestiantiy555acea4b73d2','content'=>'Kemungkinan karena gaya hidup tinggi, job lagi sepi, dan dia ingin jalan pintas'],
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Hasanudin AF','jabatan'=>'Ketua Komisi Fatwa MUI','img'=>'http://www.halalmui.org/newMUI/images/content/KF_Hasanuddin.jpg','url'=>'','content'=>'Yang jelas namanya zina di dalam syariah Islam itu haram. Prostitusi online, PSK atau WTS dan istilah aneh lainnya, sampai pada istilah pria tuna susila itu sebuah perkembangan dari zina pada umumnya'],
        ['from'=>'Said Aqil Siradj','jabatan'=>'Ketua Umum Pengurus Besar Nahdlatul Ulama (PBNU)','img'=>'http://1.bp.blogspot.com/-MXgWJ6FEe1k/U-QW-f_QtkI/AAAAAAAADnU/9tE8HsiVl30/s1600/KH+Said+Aqil+Siradj.jpg','url'=>'http://www.bijaks.net/aktor/profile/saidaqilsiradj5343c56457ee7','content'=>'Itu mengerikan, menjijikan, memalukan, ya pemerintah harus segera mengambil sikap tegas. Minimal meminimalisir'],
        ['from'=>'Din Syamsuddin','jabatan'=>'Ketua Umum PP Muhammadiyah','img'=>'http://www.rmolsumsel.com/images/berita/normal/734512_04071423122014_din-syamsudin.jpg','url'=>'http://www.bijaks.net/aktor/profile/dinsyamsuddin51c7bbdfbc558','content'=>'Kami tersentak adanya prostitusi di kalangan high class yang melibatkan tokoh yang berpengaruh di masyarakat dan kerap dijadikan idola'],
        ['from'=>'Tuti Alawiyah','jabatan'=>'Ketua International Moslem Women Union (IMWU)','img'=>'http://www.suara-islam.com/images/berita/tuty_alawiyah_20131220_175147.JPG','url'=>'http://www.bijaks.net/aktor/profile/tutyalawiyah555af3a0cfea2','content'=>'Pemerintah, polisi harus menindak tegas. Tenaga pendidik, sekolah harus ikut berpartisipasi untuk mencegah'],
        ['from'=>'Ruhut Sitompul','jabatan'=>'Komisi III DPR RI','img'=>'http://cdn.tmpo.co/data/2013/09/24/id_222734/222734_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/ruhutpoltaksitompulsh50f91f7018b5c','content'=>'Kita (DPR) maunya ada aturan itu, tapi banyak yang tidak mau hal tersebut ada. Kalau mau dicoba pasti akan mentah'],
        ['from'=>'Fahira Idris','jabatan'=>'Anggota DPD RI','img'=>'http://www.rmol.co/images/berita/normal/352170_03135816042015_fahira_idris1.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahirafahmiidris54eed6b448f2b','content'=>'Saya rasa RUU ini sudah sangat mendesak untuk dirumuskan dan dibahas bersama. Makanya DPD segera mengusulkan RUU ini kepada DPR RI'],
        ['from'=>'Maya Rumantir','jabatan'=>'Anggota DPD','img'=>'http://cdn1-a.production.liputan6.static6.com/medias/229731/big/hot300803maya_logo.jpg','url'=>'http://www.bijaks.net/aktor/profile/mayarumantir555afa3d10e0c','content'=>'Banyak saat ini artis-artis sudah merubah motivasi sebenarnya jika menjadi artis. Mereka hanya tergiur dengan uang tanpa adanya karya yang dihasilkan'],
        ['from'=>'Reza Indragiri Amriel','jabatan'=>'Psikologi forensik','img'=>'http://www.jpnn.com/uploads/berita/dir08012010/img08012010561311.jpg','url'=>'http://www.bijaks.net/aktor/profile/rezaiindragiriamriel555b04f90c1fc','content'=>'Tapi sayang, sanksi sosial kita tidak bekerja. Justru (lewat kasus) ini untuk melontarkan (popularitas) mereka lebih tinggi'],
    ],
    'VIDEO'=>[
        ['id'=>'DTHmWGpwrTs'],
        ['id'=>'8pRTZfkoJV8'],
        ['id'=>'99eOTUEwM_Q'],
        ['id'=>'vWJ6iXgLFz0'],
        ['id'=>'YTZvA_zfZGM'],
        ['id'=>'FPTS0Eb10ak'],
        ['id'=>'aneAYinZFuA'],
        ['id'=>'zKeg53aUjww'],
        ['id'=>'uMQ1E5cnY7I'],
        ['id'=>'015Tfo-tkJY'],
        ['id'=>'MCx0n_nDFVE'],
        ['id'=>'Az1aqkqIQws'],
        ['id'=>'volBTH-IoU4'],
        ['id'=>'-QKG-eWc8lo']
    ],
    'FOTO'=>[
        ['img'=>'http://1.bp.blogspot.com/-54Oc5dFwt8E/UTYCagOmgYI/AAAAAAAAA5Y/PirNKR-EEzg/s1600/Adegan+Lelang+Ciuman+Nikita+Disaksikan+Pejabat+Negri+ini.jpg'],
        ['img'=>'http://pojoksatu.id/wp-content/uploads/2015/01/1-psk.jpg'],
        ['img'=>'http://3.bp.blogspot.com/-pEPtYBq-Bi8/URyt4AIKETI/AAAAAAAAAiA/D48dvWf_DAs/s1600/razia+PSK+masih+telanjang+.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/kupang/foto/bank/images/prostitusi-psk-buka-bra-bh.jpg'],
        ['img'=>'http://assets.kompasiana.com/statics/files/14300197371137491623.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/prostitusi-online_20150416_223802.jpg'],
        ['img'=>'http://fajar.co.id/wp-content/uploads/2015/04/psk1.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/04/29/340/1142192/begini-modus-prostitusi-online-via-bbm-di-bandung-lGQZQxboUS.jpg'],
        ['img'=>'http://www.mediaindonesia.com/assets/upload/newcontent/955_newcontentisi_tata2.jpg'],
        ['img'=>'http://2.bp.blogspot.com/-idMLviHidWQ/VTAn1zzur7I/AAAAAAAAFtU/BxuKfT6imU4/s1600/Prostitusi%2BOnline.jpg'],
        ['img'=>'http://warta.sumedang.info/wp-content/uploads/2015/05/Ilustrasi-prostitusi-online.jpg'],
        ['img'=>'http://www.hizbut-tahrir.or.id/wp-content/uploads/2015/04/prostitusi-online.jpg'],
        ['img'=>'http://stat.ks.kidsklik.com/statics/files/2013/06/1371971641690395271.png'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/Situs-prostitusi-online.jpg'],
        ['img'=>'http://kabargue.com/wp-content/uploads/2015/05/Daftar-Nama-Artis-Terlibat-Prostitusi-Online-Milik-Mucikari-RA.jpg'],
        ['img'=>'http://www.rmol.co/images/berita/normal/579721_04162821042015_psk.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/04/24/338/1139577/jadi-member-prostitusi-online-bayar-rp500-ribu-JSGi1XgnKa.jpg'],
        ['img'=>'http://cms.jakartapress.com/files/news/201302/prostitusi%20online_pic.jpg'],
        ['img'=>'http://beforeitsnews.com/mediadrop/uploads/2013/38/155bbb1458ba1e93bcaeb888ad54f28c1881397e.jpg'],
        ['img'=>'http://www.skanaa.com/assets/images/news/20150424/553a71e2a81bb75d1a8b4567.jpg'],
        ['img'=>'http://www.tabloidnova.com/var/gramedia/storage/images/media/images/prostitusi/9250818-1-ind-ID/prostitusi_reference.jpg'],
        ['img'=>'http://www.aktualpost.com/wp-content/uploads/2014/10/FB.jpg'],
        ['img'=>'http://edisimedan.com/wp-content/uploads/2015/05/amel-alvi1.jpg']
    ]
]

?>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<style type="text/css">
    .allpage {
        width: 100%;
        height: auto;
        margin-top: 8px;
    }
    .col_top {
        background: url('<?php echo base_url("assets/images/hotpages/prostitusi/top.jpg")?>') no-repeat transparent;
        height: 1352px;
        margin-bottom: -1000px;
    }
    .col_kiri {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
        margin-top: 145px;
    }
    .col_kiri p, .col_kiri li, .font_kecil {
        font-size: 13px;
    }
    .col_kanan {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri2 {
        width: 34%;
        height: auto;
        background: transparent;
        float: left;
        padding-right: 2%;
    }
    .col_kanan2 {
        width: 64%;
        height: auto;
        background: transparent;
        float: left;
    }
    .col_kiri50 {
        width: 49%;
        height: auto;
        /*background-color: red;*/
        float: left;
        padding-right: 1%;
    }
    .col_kanan50 {
        width: 49%;
        height: auto;
        /*background-color: green;*/
        float: left;
        padding-left: 1%;
    }
    .col_full {
        width: 100%;
        /*background-color: lightgray;*/
    }
    .boxprofile {
        box-shadow: -5px 5px 10px gray;
        border-radius: 10px 10px 10px 10px;
        width: 290px;
        margin: 0 auto;
        padding-bottom: 10px;
        background: rgba(0, 0, 0, 0.7);
    }
    .block_red {
        background-color: #761908;
        border-radius: 10px 10px 0 0;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .picprofil {
        width: 80%;
        height: auto;
        margin: 0 auto;
        display: inherit;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        margin-right: 10px;
        max-width: 50px;
    }
    .garis {
        border-top: 1px dotted black;
    }
    .boxgray {
        width: 99%;
        border: 5px solid lightgray;
        box-shadow: -5px 5px 10px gray;
    }
    .boxgray_red {
        width: 96%;
        border: 9px solid #a60008;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }  
    .boxgray_green {
        width: 96%;
        border: 9px solid #00a651;
        border-radius: 8px;
        box-shadow: -5px 5px 10px gray;
    }
    .penyokong {
        width: 100%;
        height: auto;
        display: inline-block;
        margin-left: 20px;
    }
    .boxpenyokong {
        width: 126px;
        height: auto;
    }
    .foto {
        width: 100px;
        height: 100px;
        border: 3px solid lightgray;
        border-radius: 8px;
        box-shadow: 5px 5px 10px gray;
        padding: 10px 10px;
    }    
    li.organisasi {float: left; margin: 7px; padding:5px;vertical-align: top;width: 125px;height: 115px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi img {width: 98px; height: 75px;  padding: 0px !important;}
    li.organisasi p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.organisasi2 {float: left; margin: 7px; padding:5px;vertical-align: top;width: 88px;height: 95px;border: solid 4px #c4c4c4;text-align: center}
    li.organisasi2 img {width: 98px; height: 70px;  padding: 0px !important;}
    li.organisasi2 p {width: 100%;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.video {list-style-type: none;float: left;padding: 5px;vertical-align: top;width: auto;height: auto;}
    li.video img {width: 121px;height: auto;padding: 0px !important;}
    li.video img:hover {box-shadow: 0px 0px 5px black;}
    li.video p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.gallery {list-style-type: none;float: left;vertical-align: top;width: 150px;height: auto;float: left;margin-right: 10px;margin-bottom: 5px;}
    li.gallery img {width: 215px;height: 100px;padding: 0px !important;border: 3px solid lightgray;}
    li.gallery img:hover {box-shadow: 0px 0px 5px black;}
    li.gallery p {width: 100px;padding: 0px !important;font-size: 10px;height: auto;line-height: 12px;text-align: center;margin-top: 5px;}

    li.dukung {float: left; padding:3px;vertical-align: top;width: 177px;height:115px;border: 2px solid lightgray;color: black;margin-right: 5px;margin-bottom: 5px;}
    li.dukung img {width: auto; height: 75px;padding: 0px !important;margin: 0 auto;display: block;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;line-height: 12px;}

    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    p, li {
        text-align: justify;
        font-size: 14px;
    }
    .clear {
        clear: both;
    }
    .qpenentang {
        float: left;
        width: 30%;
        height: auto;
        background-color: red;
        display: inline-block;
        border-bottom: 1px solid black;
    }
    .parodi {
        width: 107px;
        height: 87px;
        float: left;
        margin-right: 10px;
        margin-bottom: 10px;
    }   
    .uprow {margin-left: 20px;margin-top:10px;border-color: #29166f transparent; border-style: solid; border-width: 0px 130px 25px 130px; height: 0px; width: 0px;}
    .kronologi {
        width: 260px;
        height: auto;
        margin-left: 20px;
        margin-top:0px;
        margin-bottom: 10px;
        background-color: #424040;
        background: rgba(0, 0, 0, 0.6);
        border-left:solid 2px #29166f;
        border-right:solid 2px #29166f;
        border-bottom:solid 2px #29166f;
        border-radius: 0 0 5px 5px;
        color: #ffffff;
        z-index: 100;
    }
    .kronologi-title {font-size:14px;font-weight:bold;background-color: #29166f;color: white;padding:5px;text-align: center;}
    .kronologi-info {padding:5px;background-color: #ffffff;color:#000000;}
    .kronologi-info p {line-height: 15px;font-size:12px;}

    .ketua {
        background-color: yellow;
        width: 30%;
        display: block;
        float: left;
        margin-right: 3%;
        box-shadow: -3px 3px 10px gray;
        border-radius: 8px;
        margin-bottom: 15px;
    }
    .ketua img {
        width: 100%;
        height: 200px;
    }
    .kritik {
        font-size: 18px;
        font-weight: bold;
        color: black;
    }
    .isi-col50 {
        float: left;
        margin: 10px 16px;
        width: 41%;
        height: auto;
        display: inline-block;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #29166f;
        width: 293px;
        height: 345px;
        float: left;
        margin-bottom: 10px;
        padding-top: 5px;
        margin-left: 10px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        float: left;
        border: 1px solid black;
    }
    #bulet {
        background-color: #ffff00; 
        text-align: center;
        width: 50px;
        height: 25px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: black;
        padding: 6px 10px;
        margin-left: -10px;
        margin-right: 10px;
    }

</style>

<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="allpage">
            <div class="col_top"></div>
            <div class="col_kiri">
                <h3 style="font-size: 23px;"><a id="pssivskemenpora" class="black"><?php echo $data['NARASI']['title'];?></a></h3>
                <p><?php echo $data['NARASI']['narasi'];?></p>
                <div class="garis"></div>

                <h4 class="list"><a id="polemik" style="color: black;">GRATIFIKASI SEKS PEJABAT ?</a></h4>
                <p class="font_kecil"><?php echo $data['GRATIFIKASI']['narasi'];?></p>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="ultimatum" style="color: black;">PSK ARTIS KELAS ATAS</a></h4>
                <p class="font_kecil"><?php echo $data['PSK']['narasi'];?></p>
                <ol style="list-style-type: square;margin-left: 0px !important;">
                    <?php
                    foreach ($data['PSK']['harga'] as $key => $val) {
                        echo "<li style='margin-left: 20px;font-size: 14px;width: 46%;float: left;' class='font_kecil'><b>".$val['no']."</b> - Rp. ".$val['jml']." Juta</li>";
                    }
                    ?>
                </ol>

                <div class="clear"></div>
            </div>

            <div class="col_kanan">
                <div class="boxprofile white">
                    <h4 class="block_red text-center"><a id="profileahok" style="color: #ffe219;">ATURAN PENJERAT</a></h4>
                    <p style="margin-left: 20px;margin-right: 20px;margin-top: 10px;font-size: 12px !important;">
                        <img src="http://sp.beritasatu.com/media/images/original/20130326125607133.jpg">
                    </p>
                    <p style="margin-left: 20px;margin-right: 20px;font-size: 12px !important;">
                        Indonesia salah satu negara yang melarang praktek prostitusi. Larangan tersebut sudah diatur dalam Pasal 296 KUHP.
                    </p>
                    <ol style="list-style-type: square;">
                        <li style='margin-left: 20px;margin-right: 20px;font-size: 12px !important;'>“Barang siapa yang pencahariannya dan kebiasaannya yaitu dengan sengaja mengadakan atau memudahkan perbuatan cabul dengan orang lain dihukum penjara selama-lamanya satu tahun empat bulan atau denda sebanyak-banyaknya Rp. 15.000,-.”</li><br>
                        <li style="list-style-type: none;font-size: 12px !important;">dan Pasal 506 KUHP :</li>
                        <li style='margin-left: 20px;margin-right: 20px;font-size: 12px !important;'>“Barang siapa sebagai mucikari (souteneur) mengambil untung dari pelacuran perempuan, dihukum kurungan selama-lamanya tiga bulan.”</li>
                    </ol>
                    <p style="margin-left: 20px;margin-right: 20px;font-size: 12px !important;">Pasal penjerat yang berlaku sekarang belum mampu memberikan efek jerah para pelaku prostitusi karena masih lemahnya aturan tersebut. Hingga saat ini, aturan tersebut hanya berlaku untuk menjerat mucikari, sedangkan untuk PSK dan pelanggannya belum ada aturan yang mengaturnya. Tidak mengherankan kalau praktek-praktek prostitusi masih mewabah dan sulit untuk dikendalikan. Terkait dengan hal tersebut, DPR kembali mewacakan untuk merevisi UU tentang prostitusi.</p>
                </div>

                <h4 class="list" style="margin-left: 20px;margin-top: 25px;"><a id="beritaterkait" style="color: black;">BERITA TERKAIT</a></h4>
                <div style="margin-left: 20px;background-color: #E5E5E5;">
                    <div id="newsprostitusi_container" data-tipe="1" data-page='1' class="home-issue-container" style="height: auto;margin-bottom: 9px !important;"></div>
                    <div class="row-fluid" style="margin-bottom: 2px;">
                        <div class="span6 text-left">
                            <!-- <a id="newslionair_loadmore" data-tipe="1" class="btn btn-mini" >15 Berikutnya</a> -->
                        </div>
                        <div class="span6 text-right">
                            <a id="newsprostitusi_loadmore" data-tipe="1" class="btn btn-mini" style="margin-right: 10px;">Berikutnya</a>
                            <!-- <a href="< ?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <div class="col_kiri50">
                    <h4 class="list"><a id="sabdaraja1" style="color: black;">PROKONTRA</a></h4>
                    <p class="font_kecil" style="margin-right: 10px;"><?php echo $data['PROKONTRA']['narasi'];?></p>
                    <div class="clear"></div>
                </div>
                <div class="col_kanan50">
                    <h4 class="list"><a id="sabdaraja1" style="color: black;"><?php echo strtoupper("Daftar Tempat Lokalisasi di Indonesia"); ?></a></h4>
                    <p class="font_kecil">Berikut ini daftar kota dengan Lokalisasinya :</p>
                    <?php
                    foreach ($data['DAFTAR'] as $key => $val) {
                        ?>
                        <div style="width: 49%;height: auto;display: inline-block;">
                            <li style='text-align: left;list-style-type: none;float: left;margin-bottom: 10px;margin-left: 15px;' class='font_kecil'><img src="<?php echo $val['img']; ?>" style='float: left;margin-left: 10px;max-width: 69px;margin-top: 5px;margin-right: 10px;'><?php echo $val['text']; ?></li>
                        </div>                        
                        <?php
                    }
                    ?>
                    <p class="font_kecil">Dan masih ada ratusan tempat lokalisasi yang tersebar di seluruh Indonesia.</p>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div><div class="garis"></div>

                <h4 class="list"><a id="sabdaraja1" style="color: black;">MODUS BARU PRAKTEK PROSTITUSI</a></h4>
                <p class="font_kecil" style="margin-right: 10px;"><?php echo $data['MODUS']['narasi'];?></p>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>

            <div class="col_full">
                <h4 class="list"><a id="pendukung" style="color: black;">PENDUKUNG</a></h4>
                <div class="col_kiri50">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENDUKUNG</p>
                    <ul style="margin-left: 155px;">
                        <?php
                        foreach($data['PENDUKUNG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="col_kanan50">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">INSTITUSI PENDUKUNG</p>
                    <ul style="margin-left: 80px;">
                        <?php
                        foreach($data['PENDUKUNG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
                <div class="clear"></div>

                <h4 class="list"><a id="penentang" style="color: black;">PENENTANG</a></h4>
                <div class="col_kiri50">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 5px;" class="text-center">PARPOL PENENTANG</p>
                    <ul style="margin-left: 155px;">
                        <?php
                        foreach($data['PENENTANG']['partai'] as $key=>$val) {
                            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                            $personarr = json_decode($person, true);
                            $pageName = strtoupper($personarr['page_name']);
                            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                            ?>
                            <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                                <li class="organisasi">
                                    <img src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                    <p><?php echo $pageName;?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="col_kanan50">
                    <p style="font-weight: bold;font-size: 16px;margin-bottom: -5px;margin-top: 20px;" class="text-center">INSTITUSI PENENTANG</p>
                    <ul style="margin-left: -10px;">
                        <?php
                        foreach($data['PENENTANG']['institusi'] as $key=>$val) {
                            ?>
                            <a href="<?php echo $val['url'];?>">
                                <li class="organisasi2">
                                    <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                    <p><?php echo $val['name'];?></p>
                                </li>
                            </a>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENDUKUNG</a></h4>
                <div class="clear"></div>
                    <ul style="margin-left: 0px;margin-top: 10px;">
                        <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
                            <?php 
                            for($i=0;$i<=3;$i++){
                            ?>
                            <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 77%;margin-right: 10px;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div style="float: left;width: 47%;margin-left: 20px;">
                            <?php 
                            for($i=4;$i<=7;$i++){
                            ?>
                            <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 78%;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENDUKUNG'][$i]['content']; ?>"</p>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </ul>
            </div>
            <div class="clear"></div><br><div class="garis"></div>

            <div class="col_full">
                <h4 class="list" style="margin-top: 15px;"><a id="quotependukung" style="color: black;">QUOTE PENENTANG</a></h4>
                <div class="clear"></div>
                <ul style="margin-left: 0px;margin-top: 10px;">
                    <div style="float: left;width: 49%;border-right: 1px dotted black;margin-left: 15px;">
                        <?php 
                        for($i=0;$i<=3;$i++){
                        ?>
                        <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 77%;margin-right: 10px;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div style="float: left;width: 47%;margin-left: 20px;">
                        <?php 
                        for($i=4;$i<=7;$i++){
                        ?>
                        <div style="width: 100%;height: auto;display: inline-block;">
                                <div style="float: left;width: 20%;">
                                    <a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>" ><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="height: 60px;float: right;margin-right: 10px;width: auto;"></a>
                                </div>
                                <div style="float: right;width: 78%;">
                                    <p style="font-size: 12px;font-weight: bold;margin-top: -5px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from']; ?> | <?php echo $data['QUOTE_PENENTANG'][$i]['jabatan']; ?></p>
                                    <p style="font-style: italic;font-size: 11px;color: blue;line-height: 14px;margin-top: -5px;">"<?php echo $data['QUOTE_PENENTANG'][$i]['content']; ?>"</p>
                                </div>
                        </div>
                        <?php } ?>
                    </div>
                </ul>
            </div>
            <div class="clear"></div>

            <h4 class="list" style="margin-top: 20px;"><a id="video" style="color: black;">GALERI FOTO</a></h4><div class="clear"></div>
            <div class="col_full" style="display: inline-block;">
                <ul style="margin-left: 0px;margin-top: 0px;">
                    <?php
                    $no=1;
                    foreach ($data['FOTO'] as $key => $val) { ?>
                        <li class="gallery">
                            <a href="#" data-toggle="modal" data-target="#img-<?php echo $no;?>">
                                <img src="<?php echo $val['img'];?>" />
                            </a>
                        </li>
                        <div class="modal hide fade" style="width: auto;" id="img-<?php echo $no;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Gallery Foto</h4>
                                    </div>
                                    <div class="modal-body">
                                        <img src="<?php echo $val['img'];?>" />
                                    </div>
                                    <!-- <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    <?php
                    $no++;
                    }
                    ?>
                </ul>
            </div>

            <div class="clear"></div>
            <h4 class="list" style="margin-top: 10px;"><a id="video" style="color: black;">VIDEO TERKAIT</a></h4><div class="clear"></div>
            <div class="col_full">
                <div class="boxgray" style="height: 220px;">
                    <ul style="margin-left: 15px;margin-top: 10px;">
                        <?php
                        foreach ($data['VIDEO'] as $key => $val) { ?>
                            <li class="video">
                                <a href="#" data-toggle="modal" data-target="#video-<?php echo $val['id'];?>">
                                    <img src="http://img.youtube.com/vi/<?php echo $val['id'];?>/0.jpg" />
                                </a>
                            </li>
                            <div class="modal hide fade" style="width: auto;" id="video-<?php echo $val['id'];?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="font-size: 20px;color: black;">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Video Terkait</h4>
                                        </div>
                                        <div class="modal-body">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>
</div>

<?php echo $peristiwaIndex; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>