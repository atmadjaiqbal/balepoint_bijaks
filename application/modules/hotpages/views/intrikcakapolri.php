<?php 
$left = '534px';
$right = '420px';
?>
<meta property="og:image" content="http://www.bijaks.net/assets/images/hotpages/cakapolri/peristiwa_polri.png" />

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<link rel="stylesheet" href="<?php echo base_url('assets/css/cakapolri.css'); ?>" type="text/css" media="screen">
<style type="text/css">

#main-container {
    width: 960px;
    min-height:600px;
    height: 6400px;
    margin-top:25px;
    margin-right:auto;
    margin-bottom:100px;
    margin-left:auto;
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/peristiwa_polri.png')?>") no-repeat scroll 0 0 rgba(0,0,0,0);

}

.nodes{
    width: 20px;
    height: 20px;
    float: left;
/*    background: url("*/<?php //echo base_url('assets/images/hotpages/cakapolri/point.png')?>/*");*/
}

#left_container{
    float: left;
    width: <?php echo $left;?>;
    border: none 1px red;
    margin-top: 85px;
}

#right_container{
    float: left;
    width: <?php echo $right;?>;
    border: none 1px blue;
    margin-top: 14px;
}


#dotbar{
    float: left;
    border: none 1px red;
    width: 162px;
    height: 3px;
    margin-top: 122px;
    margin-left: 3px;
    border-radius: 10px 10px 10px 10px;
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/titik-penyokong.png');?>");

}


#node_pendukung{
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/pendukung.png')?>");
    width: 523px;
    height: 44px;
    margin-left: -5px;

}

#node_penentang{
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/penentang.png')?>");
    width: 523px;
    height: 41px;
    margin-left: -5px;

}

#node_kronologi{
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/penentang.png')?>");
    width: 523px;
    height: 44px;
    margin-left: -5px;

}


#kronologi_box{
    padding-left: 10px;
    padding-top: 10px;
    width: 430px;
    margin-left: 24px;
    height: auto;
    background: url('<?php echo base_url("assets/images/hotpages/cakapolri/back-kronologi.png");?>');
}

#titik{
    float: left;
    margin-left: 21px;
    margin-top: 24px;
    width: 11px;
    height: 751px;
    background: url('<?php echo base_url("assets/images/hotpages/cakapolri/kronologi.png");?>') no-repeat;
}

.bg-img{
    float: left;
    margin-left: 43px;
    margin-top: 10px;
}

.timg{
    width: 160px;
}

.berita_text {
    border-bottom: 1px none #c4c4c4;
    /*font-size: 12px;*/
    /*margin-left: 11px;*/
    /*width: 460px;*/
}

.texting1 {
    /*color: #000000;*/
    /*font-size: 14px;*/
    /*margin-left: 5px;*/
    /*text-align: justify;*/
    width: 440px !important;
    /*word-wrap: break-word;*/
}
</style>




<div id="main-container" class="container-fluid">
    <div id="container_container">
        <div id="left_container">



            <div class="row">
                <div class="span12">
                    <div class="nodes"><img src="<?php echo base_url('assets/images/hotpages/cakapolri/point.png');?>"/></div>
                    <span class="node_text"><?php echo $data['title'];?></span>
                    <div class="clear"></div>
                    <div id="narration">
                        <?php echo $data['narration'];?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="span12">
                    <div class="nodes"><img src="<?php echo base_url('assets/images/hotpages/cakapolri/point.png');?>"/></div>
                    <span class="node_text">ANALISA</span>
                    <div class="clear"></div>
                    <div id="analisa">
                        <?php echo $data['analisa'];?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="span12">
                    <div class="nodes"><img src="<?php echo base_url('assets/images/hotpages/cakapolri/point.png');?>"/></div>
                    <span class="node_text">PENYOKONG</span>
                    <div class="clear"></div>
                    <div id="penyokong">
                        <div id="penyokong1">
                            <a href="<?php echo !empty($data['penyokong'][0]['page_id']) ? base_url('aktor/profile/'.$data['penyokong'][0]['page_id']) : '#';?>">
                            <img id="penyokong1_image" src="<?php echo $data['penyokong'][0]['img'];?>"/>
                            </a>
                            <div id="penyokong1_name"><?php echo strtoupper($data['penyokong'][0]['name']);?></div>
                            <div id="penyokong1_jabatan"><img id="penyokong1_logo" src="<?php echo $data['penyokong'][0]['logo'];?>"/>&nbsp;<?php echo $data['penyokong'][0]['jabatan'];?></div>
                        </div>
                        <div id="dotbar">
                        </div>
                        <div id="penyokong2">
                            <a href="<?php echo !empty($data['penyokong'][1]['page_id']) ? base_url('aktor/profile/'.$data['penyokong'][1]['page_id']) : '#';?>">
                            <img id="penyokong2_image" src="<?php echo $data['penyokong'][1]['img'];?>"/>
                            </a>
                            <div id="penyokong2_name"><?php echo strtoupper($data['penyokong'][1]['name']);?></div>
                            <div id="penyokong2_jabatan"><img id="penyokong1_logo" src="<?php echo $data['penyokong'][1]['logo'];?>"/>&nbsp;<?php echo $data['penyokong'][1]['jabatan'];?></div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="span12">
                    <div class="clear"></div>
                    <div class="green_box">
                        <div id="node_pendukung"><div id="node_pendukung_text">PENDUKUNG</div></div>

                        <div id="parpol_pendukung_box">
                            <div id="parpol_pendukung_title">PARPOL PENDUKUNG</div>
                            <div class="row-fluid">
                                <?php 
                                foreach($data['partaiPolitikPendukung'] as $key=>$val){
                                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                                    $personarr = json_decode($person, true);
                                    $pageName = strtoupper($personarr['page_name']);
                                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                                    if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                        $photo = 'http://www.dpp.pkb.or.id/unlocked/unlock/lambangresmipkb.jpg';
                                    }
                                ?>
                                    <div class="span3 " style="text-align: center;">
                                        <a href="<?php echo base_url('aktor/profile/'.$val['page_id']);?>">
                                        <img class="partai_images" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                        </a>
                                    </div>
                                <?php    
                                }
                                ?>

                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="green_box">
                        <div id="institusi_pendukung_box">
                            <div id="institusi_pendukung_title">INSTITUSI PENDUKUNG</div>
                            <div class="row-fluid">
                                <?php 
                                foreach($data['institusiPendukung'] as $key=>$val){
                                ?>
                                    <div class="span3 ">
                                        <img class="partai_images" src="<?php echo $val['logo'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                        <div class="institusi_pendukung_text"><?php echo $val['name'];?></div>
                                    </div>
                                <?php    
                                }
                                ?>

                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="span12">
                    <div class="clear"></div>
                    <div class="red_box">
                        <div id="node_penentang"><div id="node_penentang_text">PENENTANG</div></div>

                        <div id="parpol_penentang_box">
                            <div id="parpol_penentang_title">PARPOL PENENTANG</div>
                            <div class="row-fluid">
                                <?php 
                                foreach($data['partaiPolitikPenentang'] as $key=>$val){
                                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                                    $personarr = json_decode($person, true);
                                    $pageName = strtoupper($personarr['page_name']);
                                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                                    if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                                        $photo = 'http://www.dpp.pkb.or.id/unlocked/unlock/lambangresmipkb.jpg';
                                    }
                                ?>
                                    <div class="span3 " style="text-align: center;">
                                        <a href="<?php echo base_url('aktor/profile/'.$val['page_id']);?>">
                                        <img class="partai_images" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                                        </a>
                                    </div>
                                <?php    
                                }
                                ?>

                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="red_box">
                        <div id="institusi_penentang_box">
                            <div id="institusi_penentang_title">INSTITUSI PENENTANG</div>
                            <div class="row-fluid">
                                <?php 
                                foreach($data['institusiPenentang'] as $key=>$val){
                                ?>
                                    <div class="span3 ">
                                        <a href="<?php echo !empty($val['page_id']) ? base_url('aktor/profile/'.$val['page_id']) : '#'; ?>">
                                        <img class="partai_images" src="<?php echo $val['logo'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                        </a>
                                            <!-- <div class="institusi_penentang_text"><?php //echo $val['name'];?></div> -->
                                    </div>
                                <?php    
                                }
                                ?>

                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="red_box">
                        <div id="tokoh_penentang_box">
                            <div id="tokoh_penentang_title">TOKOH PENENTANG</div>
                            <div class="row-fluid">
                                <?php 
                                foreach($data['tokohPenentang'] as $key=>$val){
                                ?>
                                    <div class="span3 ">
                                        <a href="<?php echo $val['url'];?>">
                                        <img class="partai_images" src="<?php echo $val['logo'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                        </a>
                                            <!-- <div class="institusi_penentang_text"><?php //echo $val['name'];?></div> -->
                                    </div>
                                <?php    
                                }
                                ?>

                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="red_box">
                        <div id="pk_penentang_box">
                            <div id="pk_penentang_title">PEJABAT KEPOLISIAN PENENTANG</div>
                            <div class="row-fluid">
                                <?php 
                                foreach($data['pejabatKepolisianPenentang'] as $key=>$val){
                                ?>
                                    <div class="span3 ">
                                        <a href="<?php echo $val['url'];?>">
                                        <img class="partai_images" src="<?php echo $val['logo'];?>" alt="<?php echo $val['name'];?>"  data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                        </a>
                                            <!-- <div class="institusi_penentang_text"><?php //echo $val['name'];?></div> -->
                                    </div>
                                <?php    
                                }
                                ?>

                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="span12">
                    <span class="node_text plain_title">GELOMBANG PENOLAKAN</span>
                    <div class="clear"></div>
                    <div id="gelombang_penolakan">
                        <?php 
                        foreach($data['gelombangPenolakan'] as $key=>$val){
                        ?>
                        <div class="gelombang">
                            <div style="font-size: 14px;padding: 1px;">
                                <img src="<?php echo base_url('assets/images/hotpages/cakapolri/pointles.jpg')?>"/>&nbsp;
                                <span style="font-weight: bold;font-size: 14px;"><?php echo $val['title'];?></span>
                            </div>
                            <div style="margin-left: 20px;">
                                <?php echo $val['aksi'];?>
                            </div>
                        </div>
                        <?php
                        }
                        ?>

                    </div>
                </div>
            </div>            


            <div class="row">
                <div class="span12">
                    <span class="node_text plain_title2">BERITA TERKAIT</span>
                    <div class="clear"></div>
                    <div id="berita_terkait">
                        <div class='row-fluid'>
                            <?php 
                            foreach ($data['beritaTerkait'] as $key => $value) {
                                $addClass = $key == 0 ? 'small_gap' : '';
                            ?>    
                            <div class="span12" style="margin-left: -7px;margin-top: 10px;">
                                <div class="berita_text">
                                    <a href="<?php echo $value['link']?>">
                                        <?php echo substr($value['shortText'],0,200).'...';?></a>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
<?php /*
            <div class="row">
                <div class="span12">
                    <span class="node_text plain_title2">BERITA TERKAIT</span>
                    <div class="clear"></div>
                    <div id="berita_terkait">
                        <div class='row-fluid'>
                            <?php
                            foreach ($data['beritaTerkait'] as $key => $value) {
                                $addClass = $key == 0 ? 'small_gap' : '';
                                ?>
                                <div class="span4" style="margin-left: -7px;margin-top: 10px;">
                                    <img src="<?php echo $value['img'];?>" class="berita_terkait_class"/>
                                    <div class="berita_text"><a href="<?php echo $value['link']?>"><?php echo substr($value['shortText'],0,35).'...';?></a></div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
*/?>



            <div class="row">
                <div class="span12">
                    <div class="nodes"><img src="<?php echo base_url('assets/images/hotpages/cakapolri/point.png');?>"/></div>
                    <span class="node_text">QUOTE PENDUKUNG</span>
                    <div class="clear"></div>
                    <br/>
                    <div class="quote_left_container">
                        <?php
                        for($ii=0;$ii<=3;$ii++){
                            ?>
                            <div class="one_quote">
                                <div class="from_photo">
                                    <a href="<?php echo !empty($data['quotePendukung'][$ii]['url']) ? $data['quotePendukung'][$ii]['url'] : '#';?>">
                                    <img src="<?php echo $data['quotePendukung'][$ii]['img'];?>" style="width: 40px;"/>
                                    </a>
                                </div>
                                <div class="from_detail">
                                    <div class="from_name"><?php echo $data['quotePendukung'][$ii]['from'];?></div>
                                    <div class="from_jabatan"><?php echo $data['quotePendukung'][$ii]['jabatan'];?></div>
                                </div>
                                <div class="clear"></div>
                                <div class="quote_pendukung_content">
                                    <?php echo $data['quotePendukung'][$ii]['content'];?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="quote_right_container">
                        <?php
                        for($ii=4;$ii<=7;$ii++){
                            ?>
                            <div class="one_quote">
                                <div class="from_photo">
                                    <a href="<?php echo !empty($data['quotePendukung'][$ii]['url']) ? $data['quotePendukung'][$ii]['url'] : '#';?>">
                                    <img src="<?php echo $data['quotePendukung'][$ii]['img'];?>" style="width: 40px;"/>
                                        </a>
                                </div>
                                <div class="from_detail">
                                    <div class="from_name"><?php echo $data['quotePendukung'][$ii]['from'];?></div>
                                    <div class="from_jabatan"><?php echo $data['quotePendukung'][$ii]['jabatan'];?></div>
                                </div>
                                <div class="clear"></div>
                                <div class="quote_pendukung_content">
                                    <?php echo $data['quotePendukung'][$ii]['content'];?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        <?php
                        }
                        ?>

                    </div>


                </div>
            </div>


        </div>


        <div id="right_container">
            
            <div class="row calon_tunggal">
                <div class="span12" id="top_calon_tunggal">
                    <div class="nodes"><img src="<?php echo base_url('assets/images/hotpages/cakapolri/point.png');?>"/></div>
                    <span class="node_text" style="color: #ffffff;">CALON TUNGGAL</span>
                    <div class="clear"></div>
                    <div id="calon_name"><?php echo $data['calonDiusung'][0]['name'];?></div>
                    <div class="clear"></div>

                    <div id="narasibg">
                        <a href="<?php echo !empty($data['calonDiusung'][0]['page_id']) ? base_url('aktor/profile/'.$data['calonDiusung'][0]['page_id']) : '#';?>">
                        <img src="<?php echo $data['calonDiusung'][0]['img']?>" style="width: 134px;height: 134px;" id="textwrap"/>
                        </a>
                        <?php echo $data['narasibg'];?>
                    </div>

                    <div class="clear"></div>
                    <div class="subtitling">BIODATA</div>
                    <div class="biodata_title">Nama</div>
                    <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['name']?></div>
                    <div class="clear"></div>
                    <div class="biodata_title">Tempat/Tanggal Lahir</div>
                    <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['ttl']?></div>
                    <div class="clear"></div>
                    <div class="biodata_title">Agama</div>
                    <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['agama']?></div>
                    <div class="clear"></div>
                    <div class="biodata_title">Almamater</div>
                    <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['almamater']?></div>
                    <div class="clear"></div>
                    <div class="biodata_title">Pangkat</div>
                    <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['pangkat']?></div>
                    <div class="clear"></div>



                    <div class="subtitling">JABATAN TERAKHIR</div>
                    <div class="biodata_content2"><?php echo $data['calonDiusung'][0]['jabatan'];?></div>

                    <div class="subtitling">KARIR</div>
                    <div class="biodata_content2">
                        <?php
                        foreach($data['karir'] as $key=>$val){
                            $mulai = $val['mulai'];
                            $berakhir = !empty($val['berakhir']) ? ' - '.$val['berakhir'] : str_repeat('&nbsp;',12);
                            $sbg = $val['sbg'];
                        ?>
                            <div style="float: left;width: 95px;font-size: 14px;">
                                <?php echo $mulai.$berakhir;?>
                            </div>
                            <div style="float: left;font-size: 14px;word-wrap: break-word;width: 250px;">
                                <?php echo $sbg;?>
                            </div>
                            <div class="clear"></div>

                        <?php
                        }
                        ?>

                    </div>
                    <?php
                        //echo $data['calonDiusung'][0]['images'][1]['imgUrl'];
                    ?>
<!--                    <div>-->
<!--                        --><?php
//                        foreach($data['calonDiusung'][0]['images'] as $vv){
//                        ?>
<!--                        <div class="bg-img">-->
<!--                            <img src="--><?php //echo $vv['imgUrl'];?><!--" class="timg"/>-->
<!--                        </div>-->
<!--                        --><?php
//                        }
//                        ?>
<!--                        <div style="clear: both"></div>-->
<!--                    </div>-->

                    <br/>
                </div>
            </div>






            <div class="row">
                <div id="kronologi_box">
                    <div class="row">
                        <div class="span12">
                            <div class="nodes"><img src="<?php echo base_url('assets/images/hotpages/cakapolri/point.png');?>"/></div>
                            <span class="node_text">KRONOLOGI</span>
                            <div class="clear"></div>
                            
                            <div id="titik"></div>
                            <div id="kronologi_list">
                                <?php 
                                foreach ($data['kronologi'] as $key => $value) {
                                ?>    
                                <div id="kronologi_<?php echo $key;?>">
                                    <div class="kronologi_date">
                                        <?php echo $value['date'];?>
                                    </div>
                                    <div class="kronologi_details">
                                        <?php echo $value['content'];?>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="span12">
                    <div class="redblock">SERANGAN BUAT BUDI GUNAWAN</div>
                    <div class="texting1"><?php echo $data['seranganBuatBudiGunawan'];?></div>
                </div>


            </div>

            <div class="row">
                <div class="span12">
                    <div class="redblock">POLRI SERANG BALIK KPK</div>
                    <div class="texting1">
                        <?php echo $data['polriSerangBaliKpk']['narasi1']?>
                        <?php
                        foreach($data['polriSerangBaliKpk']['serangan'] as $key=>$val){
                        ?>
                        <div>
                            <div style="float: left;width: 15px;">
                            <img src="<?php echo base_url('assets/images/hotpages/cakapolri/pointles.jpg');?>"/>
                            </div>
                            <div style="float: left;word-wrap: break-word;width: 420px;font-size: 14px;text-align: justify;">
                            <?php echo $val['aksi'];?>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <?php
                        }
                        ?>
                        <br/>
                        <?php echo $data['polriSerangBaliKpk']['narasi2']?>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="span12">
                    <div class="nodes"><img src="<?php echo base_url('assets/images/hotpages/cakapolri/point.png');?>"/></div>
                    <span class="node_text">CALON BARU KAPOLRI</span>
                    <div class="clear"></div>
                    <div class="texting1">
                        <?php echo $data['narasiCalonBaru'];?>
                    </div>

                    <?php
                    foreach($data['calonBaru'] as $k=>$v){
                        if($k == 0){
                            $hh = '163px';
                        }elseif($k == 1){
                            $hh = '165px';
                        }elseif($k == 2){
                            $hh = '170px';
                        }elseif($k == 3){
                            $hh = '222px';
                        }elseif($k == 4){
                            $hh = '147px';
                        }elseif($k == 5){
                            $hh = '150px';
                        }else{
                            $hh = '100px';
                        };
                    ?>
                    <div class="box_calon_baru" style="height: <?php echo $hh;?>">
                        <div class="cal_bar_left">
                            <div class="cal_bar_name"><?php echo $v['nama'];?></div>
                            <div>
                                <div class="small_foto">
                                    <a href="<?php echo $v['url'];?>">
                                    <img src="<?php echo $v['img'];?>" width="78px;height: 60px;"/>
                                    </a>
                                </div>
                                <div class="small_info">
                                    <div>Lahir:<?php echo $v['lahir'];?></div>
                                    <div>Lulusan:<?php echo $v['lulusan'];?></div>
                                    <div>Jabatan Terakhir:<?php echo $v['jabatanTerakhir'];?> </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="cal_bar_right">
                            <div style="font-weight: bold;margin-top: 1px;">KARIR</div>
                            <?php
                            foreach($v['karir'] as $key=>$val){
                            ?>
                            <div style="float: left;width: 30px;">
                                <?php echo $val['tahun'];?>
                            </div>
                            <div style="float: left;">
                                <?php echo $val['jabatan'];?>
                            </div>
                            <div class="clear"></div>
                            <?php
                            }
                            ?>
                        </div>


                    </div>
                    <div class="clear"></div>
                    <?php
                    }
                    ?>

                </div>
            </div>


            <div class="row">
                <div class="span12">
                    <div class="nodes"><img src="<?php echo base_url('assets/images/hotpages/cakapolri/point.png');?>"/></div>
                    <span class="node_text">QUOTE PENENTANG</span>
                    <div class="clear"></div>
                    <br/>
                    <div class="quote_left_container">
                        <?php
                        for($ii=0;$ii<=5;$ii++){
                            ?>
                            <div class="one_quote">
                                <div class="from_photo">
                                    <a href="<?php echo !empty($data['quotePenentang'][$ii]['url']) ? $data['quotePenentang'][$ii]['url'] : '#';?>">
                                    <img src="<?php echo $data['quotePenentang'][$ii]['img'];?>" style="width: 40px;"/>
                                        </a>
                                </div>
                                <div class="from_detail">
                                    <div class="from_name"><?php echo $data['quotePenentang'][$ii]['from'];?></div>
                                    <div class="from_jabatan"><?php echo $data['quotePenentang'][$ii]['jabatan'];?></div>
                                </div>
                                <div class="clear"></div>
                                <div class="quote_penentang_content">
                                    <?php echo $data['quotePenentang'][$ii]['content'];?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        <?php
                        }
                        ?>

                    </div>
                    <div class="quote_right_container">
                        <?php
                        for($ii=6;$ii<=10;$ii++){
                            ?>
                            <div class="one_quote">
                                <div class="from_photo">
                                    <a href="<?php echo !empty($data['quotePenentang'][$ii]['url']) ? $data['quotePenentang'][$ii]['url'] : '#';?>">
                                    <img src="<?php echo $data['quotePenentang'][$ii]['img'];?>" style="width: 40px;"/>
                                        </a>
                                </div>
                                <div class="from_detail">
                                    <div class="from_name"><?php echo $data['quotePenentang'][$ii]['from'];?></div>
                                    <div class="from_jabatan"><?php echo $data['quotePenentang'][$ii]['jabatan'];?></div>
                                </div>
                                <div class="clear"></div>
                                <div class="quote_penentang_content">
                                    <?php echo $data['quotePenentang'][$ii]['content'];?>
                                </div>
                            </div>
                            <div class="clear"></div>
                        <?php
                        }
                        ?>

                    </div>
                </div>
            </div>




        </div>
    </div>
</div>
<div class="clear" style="padding-bottom: 10px;"></div>


<script type="text/javascript">
//$('#masonry_container0').masonry({
//  columnWidth: 200,
//  itemSelector: '.item_masonry0'
//});
//
//$('#masonry_container1').masonry({
//  columnWidth: 200,
//  itemSelector: '.item_masonry1'
//});
//
//
//
//$('#masonry_container3').masonry({
//  columnWidth: 200,
//  itemSelector: '.item_masonry3'
//});
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
})

</script>



