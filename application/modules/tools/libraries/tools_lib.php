<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools_Lib {

	protected  $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database('slave');
	}


    public function getToptenList(){
        $sql = "SELECT id_topten_category FROM master_topten_category ORDER BY id_topten_category DESC";
        $result = $this->CI->db->query($sql)->result_array();
        $this->CI->db->close();
        return $result;
    }

    public function getToptenDetail($id = 0)
    {
        $wh = 'WHERE id_topten_category = "'.$id.'"';
        $sql = 'SELECT * FROM master_topten_category '.$wh.'
            ORDER BY set_headline ASC';
        $result = $this->CI->db->query($sql)->result_array();
        $this->CI->db->close();
        return $result;
    }

    public function getToptenContent($params=array())
    {
        switch ($params['toptenType']) {
            case '1':
                // Fetch content from tobject_page_topten
                $sql = "SELECT * FROM tobject_page_topten where id_topten_category='".$params['toptenId']."'";
                $result = $this->CI->db->query($sql)->result_array();
                $this->CI->db->close();
                break;
            case '2':
                // Fetch content from topten_youtube
                $sql = "SELECT * FROM topten_youtube where id_topten_category='".$params['toptenId']."'";
                $result = $this->CI->db->query($sql)->result_array();
                $this->CI->db->close();
                break;
            case '3':
                // Fetch content from topten_news
                $sql = "SELECT * FROM topten_news where id_topten_category='".$params['toptenId']."'";
                $result = $this->CI->db->query($sql)->result_array();
                $this->CI->db->close();
                break;
            default:
                # code...
                break;
        }
        return $result;    

    }

    public function getActiveBanner()
    {
        $sql = "SELECT * FROM tcontent_banner WHERE is_active='1'";
        $result = $this->CI->db->query($sql)->result_array();
        $this->CI->db->close();
        return $result;
    }

}
/* End of file file.php */