<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('core/bijak');
		$this->load->library('tools/tools_lib');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
		$this->current_url = '/'.$this->uri->uri_string();
		$this->session->set_flashdata('referrer', $this->current_url);

        $this->member = $this->session->userdata('member');
	}


	function ptt($return='false')
	{
        # populate topten title
        $list = $this->tools_lib->getToptenList();
        $key = 'topten:list';
        $this->redis_slave->delete($key);
        foreach($list as $val){
            $this->redis_slave->rPush($key,$val['id_topten_category']);

            $key2 = 'topten:detail:'.$val['id_topten_category'];
            $this->redis_slave->delete($key2);
            $content = $this->tools_lib->getToptenDetail($val['id_topten_category']);
            $this->redis_slave->set($key2,json_encode($content[0]));
        }
        
        $redisList = $this->redis_slave->lRange($key,0,100);
        foreach($redisList as $val){
            $key3 = 'topten:detail:'.$val;
            $comp = json_decode($this->redis_slave->get($key3));
            //echo '<pre>';print_r($comp);echo '</pre>'; 
            echo '<p>keys formed: '.$key3.'</p>';
            echo '<p>Content '.$key3.':</p>';
            echo '<pre>';print_r($comp);echo '</pre>';
            echo '<br/><hr/>';

        }
        exit;

	}

    function ptc()
    {
        # populate topten content
        $key = 'topten:list';
        $idList = $this->redis_slave->lRange($key,0,200);
        foreach($idList as $val){
            $key2 = 'topten:detail:'.$val;
            $key3 = 'topten:content:'.$val;
            $temp = json_decode($this->redis_slave->get($key2));
            $params = ['toptenType'=>$temp->topten_type,'toptenId'=>$temp->id_topten_category];
            
            $contentTop10 = $this->tools_lib->getToptenContent($params);
            $this->redis_slave->delete($key3);
            $this->redis_slave->set($key3,json_encode($contentTop10));

            $tempkey = $this->redis_slave->get($key3);
            echo '<p>'.$key3.'</p>';
            echo '<p>'.$tempkey.'</p>';
            echo '<hr/>';
        }
    }

    function ptts($id=0)
    {
        # Use this to populate a title
        # http://bijaks.dev/tools/ptts/[id]
        $key = 'topten:list';
        $this->redis_slave->rPush($key,$id);
        $key2 = 'topten:detail:'.$id;
        $this->redis_slave->delete($key2);
        $content = $this->tools_lib->getToptenDetail($id);
        $this->redis_slave->set($key2,json_encode($content[0]));

        $key3 = 'topten:detail:'.$id;
        $comp = json_decode($this->redis_slave->get($key3));
        echo '<p>keys formed: '.$key3.'</p>';
        echo '<p>Content '.$key3.':</p>';
        echo '<pre>';print_r($comp);echo '</pre>';
        echo '<br/><hr/>';

    }

    function ptcs($id=0)
    {
        # Use this to populate a title's content
        # http://bijaks.dev/tools/ptcs/[id]
        $key = 'topten:content:'.$id;
        $this->redis_slave->delete($key);

        $key2 = 'topten:detail:'.$id;
        $temp = json_decode($this->redis_slave->get($key2));
        $params = ['toptenType'=>$temp->topten_type,'toptenId'=>$temp->id_topten_category];
        $contentTop10 = $this->tools_lib->getToptenContent($params);
        $this->redis_slave->set($key,json_encode($contentTop10));

        $tempkey = $this->redis_slave->get($key);
        echo '<p>'.$key.'</p>';
        echo '<p>'.$tempkey.'</p>';
        echo '<hr/>';


    }

    function pb()
    {
        #populate banner
        $key = 'list:banners';
        $this->redis_slave->delete($key);

        $banners = json_encode($this->tools_lib->getActiveBanner());
        $this->redis_slave->set($key,$banners);

        $bannersJson = $this->redis_slave->get($key);
        echo '<pre>';echo $bannersJson;echo '</pre>';


    }

}
