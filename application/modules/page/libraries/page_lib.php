<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->db = $this->CI->load->database('slave', true);
    }

    public function get_event($select='*', $limit=0, $offset=0, $order_by='no_urut')
    {
        $this->db->select($select);
        $this->db->from('tcontent_event');
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by($order_by);
        $data = $this->db->get();
        return $data;
    }

    public function get_capres()
    {
        $sql = "SELECT * FROM master_pilpress ORDER BY no_urut ASC";
        $qry = $this->CI->db->query($sql);
        return $qry->result_array();
    }

    public function get_head2head()
    {
        $this->db->order_by('id_head', 'asc');
        $data = $this->db->get('headtohead_category');
        return $data->result_array();
    }

    public function get_headdetail($idhead, $pos=null)
    {
        $sql = "SELECT * FROM headtohead_detail ";
        $where = "WHERE id_head = '".$idhead."' ";
        if($pos) $where .= "AND head_pos = '".$pos."' ";
        $sql = $sql.$where;

    }


}

?>