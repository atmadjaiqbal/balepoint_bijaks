<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends Application {

	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
        $this->load->library('page/page_lib');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();

        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');
        $this->load->module('headline');
        $this->load->module('survey');

	}

	
	public function index()
	{
		$this->tentang();		
	}

	public function tentang()
	{
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['topic_last_activity'] =  '';
        $html['html']['content']  = $this->load->view('page/tentang', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
	}

    function about()
    {
        $this->load->view('about');
    }

    function contact()
    {
        $this->load->view('contact');
    }

    function privacy()
    {
        $this->load->view('privacy');
    }

    function term_condition()
    {
        $this->load->view('term_condition');
    }

	public function kontak()
	{
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['topic_last_activity'] =  '';
        $html['html']['content']  = $this->load->view('page/kontak', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
	}


	public function privasi()
	{
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['topic_last_activity'] =  '';
        $html['html']['content']  = $this->load->view('page/privasi', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
	}

	public function syarat_ketentuan()
	{
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['topic_last_activity'] =  '';
        $html['html']['content']  = $this->load->view('page/syarat_ketentuan', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
	}

    public function jadwalpemilu()
    {
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js', 'bijaks.profile.js', 'bijaks.page.js');
        $data['topic_last_activity'] = 'profile';
        $html['html']['content']  = $this->load->view('page/jadwal_pemilu', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function jadwal_list()
    {
        $page = $this->uri->segment(3);
        $limit = 20;
        $offset = ($limit*intval($page)) - $limit;
        $list_jadwal = $this->page_lib->get_event('*', $limit, $offset)->result_array();
        $data['result'] = $list_jadwal;
        $data['count_result'] = count($list_jadwal);
        $this->load->view('jadwal_list', $data);
    }

    public function fail_aktivasi()
    {
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['topic_last_activity'] =  '';
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js', 'bijaks.profile.js', 'bijaks.page.js');
        $data['status'] = 'Gagal';
        $data['category'] = 'GAGAL AKTIVASI';
        $data['class'] = 'error';
        $data['msg'] = 'Aktifasi gagal, silahkan klik kembali link di email anda';
        $html['html']['content']  = $this->load->view('page/aktivasi', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function success_aktivasi()
    {
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js', 'bijaks.profile.js', 'bijaks.page.js');
        $data['topic_last_activity'] =  '';
        $data['status'] = 'Berhasil';
        $data['category'] = 'BERHASIL AKTIVASI';
        $data['class'] = 'success';
        $data['msg'] = 'Aktivasi berhasil, anda telah berhasil login, halaman otomatis akan didirect.';
        $html['html']['content']  = $this->load->view('page/aktivasi', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function pilpress()
    {
        $data = $this->data;
        $this->load->library('aktor/aktor_lib');
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js');
        $data['topic_last_activity'] =  '';

        $prabowo = $this->redis_slave->get('profile:detail:prabowosubiyantodjojohadikusumo50c1598f86d91');

        $data['prabowo'] = @json_decode($prabowo, true);
        $hatta = $this->redis_slave->get('profile:detail:hattarajasa50459f43e3b51');
        $data['hatta'] = @json_decode($hatta, true);
        $jokowi = $this->redis_slave->get('profile:detail:irjokowidodo50ee1dee5bf19');
        $data['jokowidodo'] = @json_decode($jokowi, true);
        $kalla = $this->redis_slave->get('profile:detail:drshmuhammadjusufkalla50ee870b99cc9');
        $data['kalla'] = @json_decode($kalla, true);

        $count_prabowo = $this->aktor_lib->get_count_post('prabowosubiyantodjojohadikusumo50c1598f86d91', 'prabowosubiyantodjojohadikusumo50c1598f86d91');
        foreach($count_prabowo->result_array() as $val){
            $data['prabowo_count_'.$val['tipe']] = $val['total'];
        }

        $count_hatta = $this->aktor_lib->get_count_post('hattarajasa50459f43e3b51', 'hattarajasa50459f43e3b51');
        foreach($count_hatta->result_array() as $val){
            $data['hatta_count_'.$val['tipe']] = $val['total'];
        }

        $count_jokowi = $this->aktor_lib->get_count_post('irjokowidodo50ee1dee5bf19', 'irjokowidodo50ee1dee5bf19');
        foreach($count_jokowi->result_array() as $val){
            $data['jokowi_count_'.$val['tipe']] = $val['total'];
        }

        $count_kalla = $this->aktor_lib->get_count_post('drshmuhammadjusufkalla50ee870b99cc9', 'drshmuhammadjusufkalla50ee870b99cc9');
        foreach($count_kalla->result_array() as $val){
            $data['kalla_count_'.$val['tipe']] = $val['total'];
        }

        $topik_prabowo = $this->aktor_lib->get_news_by_aktor('prabowosubiyantodjojohadikusumo50c1598f86d91', 5, 0);

        $rkey_min = 'news:detail:'; $result = array();
        $have_redis_empty = false;$news_prabowo = array();
        foreach($topik_prabowo->result_array() as $row=>$val)
        {
            $newsprabo = $this->redis_slave->get($rkey_min.$val['news_id']);
            $berprabo_json = json_decode($newsprabo, true);
            if(empty($berprabo_json)){ $have_redis_empty = true; continue;}
            $berprabo_json['entry_date'] = $val['entry_date'];
            $news_prabowo[$row] = $berprabo_json;
        }

        $topik_hatta = $this->aktor_lib->get_news_by_aktor('hattarajasa50459f43e3b51', 5, 0);
        $rkey_min = 'news:detail:'; $result = array();
        $have_redis_empty = false; $news_hatta = array();
        foreach($topik_hatta->result_array() as $row=>$val)
        {
            $newshatta = $this->redis_slave->get($rkey_min.$val['news_id']);
            $berhatta_json = json_decode($newshatta, true);
            if(empty($berhatta_json)){ $have_redis_empty = true; continue;}
            $berhatta_json['entry_date'] = $val['entry_date'];
            $news_hatta[$row] = $berhatta_json;
        }

        $topik_jokowi = $this->aktor_lib->get_news_by_aktor('irjokowidodo50ee1dee5bf19', 5, 0);
        $rkey_min = 'news:detail:'; $result = array();
        $have_redis_empty = false; $news_jokowi = array();
        foreach($topik_jokowi->result_array() as $row=>$val)
        {
            $newsjoko = $this->redis_slave->get($rkey_min.$val['news_id']);
            $berjoko_json = json_decode($newsjoko, true);
            if(empty($berhatta_json)){ $have_redis_empty = true; continue;}
            $berjoko_json['entry_date'] = $val['entry_date'];
            $news_jokowi[$row] = $berjoko_json;
        }

        $topik_kalla = $this->aktor_lib->get_news_by_aktor('drshmuhammadjusufkalla50ee870b99cc9', 5, 0);
        $rkey_min = 'news:detail:'; $result = array();
        $have_redis_empty = false; $news_kalla = array();
        foreach($topik_kalla->result_array() as $row=>$val)
        {
            $newskalla = $this->redis_slave->get($rkey_min.$val['news_id']);
            $berkalla_json = json_decode($newskalla, true);
            if(empty($berkalla_json)){ $have_redis_empty = true; continue;}
            $berkalla_json['entry_date'] = $val['entry_date'];
            $news_kalla[$row] = $berkalla_json;
        }

        $resultSuksesi = $this->redis_slave->lrange("suksesi:list:hot", 0, 0);
        if(count($resultSuksesi))
        {
            foreach($resultSuksesi as $key => $value)
            {
                $rowSuksesi = $this->redis_slave->get('suksesi:detail:'.$value);
                $arrsuksesi = @json_decode($rowSuksesi, true);
                $rsSuksesi[$key] = $arrsuksesi;
            }
        }

        //$list_head = $this->page_lib->get_head2head();
        //$data['headcategory'] = $list_head;

        $data['news_prabowo'] = $news_prabowo;
        $data['news_hatta'] = $news_hatta;
        $data['news_jokowi'] = $news_jokowi;
        $data['news_kalla'] = $news_kalla;
        $data['suksesi'] = $rsSuksesi;

        $html['html']['content']  = $this->load->view('page/pilpress', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);

    }

    public function comingsoon()
    {
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['topic_last_activity'] =  '';
        $html['html']['content']  = $this->load->view('page/comingsoon', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }


    public function election()
    {
        $data = $this->data;
        $this->load->library('aktor/aktor_lib');
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js');
        $data['topic_last_activity'] =  '';

        $resultSuksesi = $this->redis_slave->lrange("suksesi:list:hot", 0, 0);
        if(count($resultSuksesi))
        {
            foreach($resultSuksesi as $key => $value)
            {
                $rowSuksesi = $this->redis_slave->get('suksesi:detail:'.$value);
                $arrsuksesi = @json_decode($rowSuksesi, true);
                $rsSuksesi[$key] = $arrsuksesi;
            }
        }
        $data['suksesi'] = $rsSuksesi;
        $html['html']['content']  = $this->load->view('page/election', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = '';
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);

    }


    public function quickcount()
    {
        $data = $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['topic_last_activity'] =  '';
        $html['html']['content']  = $this->load->view('page/quickcount', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

}
