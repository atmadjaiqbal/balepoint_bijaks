<div class="container" style="margin-top: 31px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right" style="background: none;">
                    <h1>KEBIJAKAN PRIVASI</h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>

    <div class="sub-header-container" style="background-color: #fff;margin-top:170px;">
        <div style="margin-left: 20px;padding-top: 30px;margin-right: 20px;">
                <ol>
                    <li>Ketika anda mendaftar menjadi pengguna <strong class='brand'>bijaks.net</strong> anda diminta untuk memberikan informasi dengan sebenar-benarnya, baik informasi dasar berupa nama dan alamat email, maupun informasi lengkap berupa foto, jenis kelamin, tanggal lahir, alamat, nomor telepon dan biografi anda. <strong class='brand'>bijaks.net</strong> tidak akan pernah meminta anda untuk memberikan informasi tambahan lainnya, berupa informasi keuangan anda.</li>
                    <li>Segala bentuk informasi yang anda berikan, bersifat publik dan bisa dilihat oleh semua pengguna <strong class='brand'>bijaks.net</strong>. Karena itu, harap berhati-hati dan selektif dalam memberikan informasi, yang dapat diakses oleh publik. <strong class='brand'>bijaks.net</strong> tidak bertanggung jawab atas penyalahgunaan informasi,  yang anda bagikan.</li>
                    <li><strong class='brand'>bijaks.net</strong> berhak dan wajib memberikan data maupun informasi yang ada, untuk kepentingan hukum yang berlaku di Indonesia. <strong class='brand'>bijaks.net</strong> berhak menggunakan data yang ada untuk kepentingan lain, tanpa pemberitahuan sebelumnya kepada pengguna. Karena semua data dan informasi yang ada di <strong class='brand'>bijaks.net</strong>, merupakan milik <strong class='brand'>bijaks.net</strong>. Segala penggunaan materi yang ada di <strong class='brand'>bijaks.net</strong> harus mendapatkan ijin tertulis dari <strong class='brand'>bijaks.net</strong>.</li>
                    <li><strong class='brand'>bijaks.net</strong> berhak untuk mengganti, mengubah dan menambah kebijakan privasi ini, tanpa pemberitahuan terlebih dahulu.</li>
                </ol>
        </div>
    </div>

</div>
<br/>
