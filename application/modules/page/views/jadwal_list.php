<div class="row-fluid" id="detail-jadwal" data-jumlah="<?php echo $count_result;?>">
    <ul>
        <?php
        foreach ($result as $rwJadwal)
        {
            if($rwJadwal['set_active'] == 1) { $_style = 'Style="background-color: #cecece;font-weight:bold;"'; } else { $_style = '';}
            ?>
            <li <?php echo $_style; ?>>
                <div class="span2 jad-content" style="margin-left:20px;"><?php echo date('d F Y', strtotime($rwJadwal['event_date'])); ?></div>
                <div class="span2 jad-content"></div>
                <div class="span7 jad-content"><?php echo $rwJadwal['event_name']; ?></div>
            </li>
            <?php
        }
        ?>
    </ul>
</div>
