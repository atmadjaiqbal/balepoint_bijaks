<div class="container" style="margin-top: 31px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right" style="background: none;">
                    <h1>JADWAL KEGIATAN</h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>

    <div class="sub-header-container" style="background-color: #fff;margin-top:230px;">
        <div id="jadwalpemilu" >

            <div class="row-fluid title-jadwal">
                <div class="span2">TANGGAL</div>
                <div class="span10">ACARA KEGIATAN</div>
            </div>

            <div id="jadwal_container" data-page='1'></div>
            <br>
            <div id="load_more_place" data-rows='0' class="row-fluid komu-wall-list komu-wall-list-bg">
                <div class="span12 text-center">
                    <a id="jadwal_load_more" class="komu-wall-list-more" href="#">LOAD MORE</a>
                </div>
            </div>

        </div>
    </div>

</div>
<br/>
