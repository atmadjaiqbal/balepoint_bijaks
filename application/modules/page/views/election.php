<div class="container" style="margin-top: 30px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right" style="background: none;">
                    <h1>ELECTION</h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>
</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <div class="row-fluid">
                <?php
                foreach($suksesi as $key => $val)
                {
                    ?>
                    <div class="span12">
                        <?php $dt['val'] = $val; ?>
                        <?php $this->load->view('template/suksesi/tpl_suksesi_home_capres', $dt); ?>
                    </div>
                <?php
                }
                ?>
            </div><!-- end row-fluid -->

        </div>
    </div>
</div>