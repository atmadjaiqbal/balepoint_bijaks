<?php foreach($result as $row=>$val){ ?>

    <?php
    $cat_id = (count($val['categories']) > 0) ? $val['categories'][0]['id'] : '0';
    $news_url = base_url() . 'news/article/'.$cat_id.'-'.$val['id'].'/'.urltitle($val['title']);

    if(!empty($val['title']))
    {
        $short_title = (strlen ($val['title']) > 43) ? substr($val['title'], 0, 43). '...' : $val['title'];
        //$short_title = $val['title'];
    } else {
        $short_title = "";
    }
    ?>

    <div class="media-news media"> <!--id="media-footer"-->
        <div class="media-news-image pull-left" style="background:
            url('<?=$val['image_thumbnail'];?>') no-repeat; background-position : center; background-size:auto 55px;">

        </div>
        <div class="media-body">
            <h5><a title="<?php echo !empty($value['title']) ? $value['title'] : ''; ?>" href="<?php echo $news_url; ?>"><?php echo $short_title; ?></a></h5>
            <p class="news-date" style="margin-top:-2px;"><?php echo !empty($val['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($val['date'])) : ''; ?></p>
            <p class="article"><?php echo substr(!empty($val['excerpt']) ? $val['excerpt'] : '',0, 120) . ' ... ';?></p>


        </div>
    </div>
    <div class="line-divider"></div>
<?php } ?>
