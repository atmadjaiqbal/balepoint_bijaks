<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends Application
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('m_date');

		$this->load->helper('text');
		$this->load->helper('seourl');

		$this->load->library('search/search_lib');

      $this->load->module('timeline/timeline');
      $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
      $this->redis_slave = $this->redis_slave->connect();		
	}

	public function politisi()
	{
		$term = $_GET['term'];

		$query = $this->search_lib->searchPolitisi($term);

		$row = $query;

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($row));
	}

	public function index()
	{
      $data 			= $this->data;
      $data['scripts'] = array('bijaks.home.js', 'bijaks.js');
		$data['term'] 	= (!isset($_GET['term']) ? NULL : $_GET['term']);
		if($data['term'] )
		{
			$data['limit']	= 5;
			$offset 			= 0;

			$data['searchpolitisi'] = $this->search_lib->searchPolitisi($data['term'], $data['limit'], $offset);
			$data['politisi_next_offset'] = $data['limit'] + $offset;


			if( isset($data['member']) && ! empty($data['member']['account_id']) ) {
				$data['searchnews'] = $this->search_lib->searchNews($data['term'], $data['limit'], $offset, $data['member']['account_id']);
			} else {
				$data['searchnews'] = $this->search_lib->searchNews($data['term'], $data['limit'], $offset, "");
			}
			$data['news_next_offset'] = $data['limit'] + $offset;


			$data['searchmembers'] = $this->search_lib->searchMember($data['term'], $data['limit'], $offset);
			$data['member_next_offset'] = $data['limit'] + $offset;



			$data['searchsentimen'] = $this->search_lib->searchSentimen($data['term'], $data['limit'], $offset);
			$data['sentimen_next_offset'] = $data['limit'] + $offset;
		}
		
      $html['html']['content']  = $this->load->view('search/search_index', $data, true);
      $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
      $html['html']['footer_content'] =  $this->load->view('template/tpl_footer_content', $html, true);
      $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
      $this->load->view('template/tpl_one_column', $html, false);

	}

	public function ajax_politisi()
	{
      $data 			= $this->data;
		$data['term'] 	= (!isset($_GET['term'])) ? NULL : $_GET['term'];

		if($data['term'])
		{
			$data['limit']						= 10;
			$offset 								= (isset($_GET['offset']) ? intval($_GET['offset']) : 0);
			$data['searchpolitisi'] 		= $this->search_lib->searchPolitisi($data['term'], $data['limit'], $offset);
			$data['politisi_next_offset'] = $data['limit'] + $offset;
			$data['term'] 						= htmlspecialchars($data['term']);

			$this->load->view('search/search_list_politisi', $data);
		}
	}

	public function ajax_news()
	{
      $data 			= $this->data;
		$data['term'] 	= (!isset($_GET['term'])) ? NULL : $_GET['term'];

		if($data['term'])
		{
			$data['limit']						= 10;
			$offset 								= (isset($_GET['offset']) ? intval($_GET['offset']) : 0);
			$data['searchnews'] 				= $this->search_lib->searchNews($data['term'], $data['limit'], $offset, "");
			$data['news_next_offset'] 		= $data['limit'] + $offset;
			$data['term'] 						= htmlspecialchars($data['term']);

			$this->load->view('search/search_list_news', $data);
		}
	}


	public function ajax_member()
	{
      $data 			= $this->data;
		$data['term'] 	= (!isset($_GET['term'])) ? NULL : $_GET['term'];

		if($data['term'])
		{
			$data['limit']						= 10;
			$offset 								= (isset($_GET['offset']) ? intval($_GET['offset']) : 0);
			$data['searchmembers'] 			= $this->search_lib->searchMember($data['term'], $data['limit'], $offset, "");
			$data['member_next_offset'] 	= $data['limit'] + $offset;
			$data['term'] 						= htmlspecialchars($data['term']);

			$this->load->view('search/search_list_member', $data);
		}
	}


	public function ajax_sentimen()
	{
      $data 			= $this->data;
		$data['term'] 	= (!isset($_GET['term'])) ? NULL : $_GET['term'];

		if($data['term'])
		{
			$data['limit']						= 10;
			$offset 								= (isset($_GET['offset']) ? intval($_GET['offset']) : 0);
			$data['searchsentimen'] 			= $this->search_lib->searchSentimen($data['term'], $data['limit'], $offset, "");
			$data['sentimen_next_offset'] 	= $data['limit'] + $offset;
			$data['term'] 						= htmlspecialchars($data['term']);

			$this->load->view('search/search_list_sentimen', $data);
		}
	}

	public function ajaxsentimensearch()
	{
		$term = (!isset($_GET['term']) ? NULL : $_GET['term']);
		$mode = (!isset($_GET['mode']) ? 'all' : strtolower($_GET['mode']));
		if($term)
		{
			$limit = 10;
			$offset = (isset($_GET['offset']) ? intval($_GET['offset']) : 0);
			$query = $this->search_lib->searchSentimen($term, $limit, $offset);
			$data['searchsentimen'] = $query;
			$data['sentimen_next_offset'] = $limit + $offset;

			$data['term'] = htmlspecialchars($term);
			$data['mode'] = $mode;
			$member = $this->session->userdata('member');
			$data['member'] = $member;

			$this->load->view('include/search_sentimen_list', $data);

		}
	}



 	private function checkMember() {
 	   $this->load->model('member', 'member_model');
		if($this->member) {
			$this->member['current_city'] = $this->member_model->getUserCurrentCity($this->member['account_id']);
		} else {
			redirect($this->data['base_url'].'home/login/?redirect='.$this->current_url);
		}
	}

   private function checkUID() {
		$uID = $this->input->post('uID');
		if(isset($_GET['uid']) && ! empty($_GET['uid'])) {
			$this->uID = $_GET['uid'];
			$this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);

		} else if(! empty($uID)) {
			$this->uID = $uID;
			$this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);

		} else {
			$this->uID  = $this->member['account_id'];
			$this->isLogin = true;
		}
	}

	private function isLogin($str1, $str2) {
		if(! strcmp($str1, $str2)) {
			return true;
		} else {
			return false;
		}
	}
}
