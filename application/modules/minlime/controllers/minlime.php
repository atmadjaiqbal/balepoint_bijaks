<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Minlime extends Application {

	function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $data = $this->data;
        $this->load->library('minlime_lib');
        $this->load->library('aktor/aktor_lib');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->load->library('redis', array('connection_group' => 'master'), 'redis_master');
        $this->redis_slave = $this->redis_slave->connect();
        $this->redis_master = $this->redis_master->connect();
    }

    function profile_comment($page_id='', $limit=0, $offset=0)
    {
//        $limit = 9;
        if(empty($page_id)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'bad request';
            $this->message($data);
        }

//        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){
            $offset += 1; // (intval($page) - 1);
        }

        $result = array();
        $zrange = $limit + $offset;

        $list_key = 'profile:comment:list:';

        $list_len = $this->redis_slave->lLen($list_key.$page_id);
        if ($zrange > intval($list_len)){
            $zrange = $list_len;
        }

        if($offset > intval($list_len)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'bad request';
            $this->message($data);
        }

        $comment_list = $this->redis_slave->lRange($list_key.$page_id, $offset, $zrange);

        foreach($comment_list as $row=>$val){
            $comment_detail = $this->redis_slave->get('profile:comment:detail:'.$val);
            $comment_array = @json_decode($comment_detail, true);
            if(!empty($comment_array)){
                $comment_array['image_url'] = icon_url($comment_array['attachment_title']);
            }
            $result[$row] = $comment_array;
        }
        $dt['result'] = $result;
        $htm = $this->load->view('timeline_comment', $dt, true);

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }


    function post_status()
    {
        if($this->isLogin(true) == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('val', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user', '', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('userfile', '', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Title harus di isi';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $val = $this->input->post('val', true);
        $user = $this->input->post('user', true);

        $cid = sha1( uniqid().time() );

        // check if follow
        $this->is_follow($user);

        $data_content = array(
            'content_id' => $cid,
            'account_id' => $this->member['account_id'],
            'content_group_type' => 10,
            'page_id' => $user,
            'description' => trim($val),
            'content_type' => 'TEXT',
            'entry_date' => mysql_datetime()
        );

        $this->minlime_lib->insert_content($data_content);

        // get wall key redis
        $last_key = $this->redis_slave->get('wall:user:key:'.$this->member['account_id']);

        // insert content_id to own list
        $this->redis_master->lPush('wall:user:list:'.$last_key.':'.$this->member['account_id'], $cid);

        // get content_detail insert to redis
        $content_detail = $this->minlime_lib->get_content_by_id($cid);
        if($content_detail->num_rows() > 0){
            $cnt_detail = @json_encode($content_detail->row_array());
            $this->redis_master->set('wall:user:detail:'.$cid, $cnt_detail);
        }

        // get friend list
        $friend_list = $this->aktor_lib->get_friend_list($this->member['account_id'], $limit=0, $offset=0);
        foreach($friend_list->result_array() as $row=>$val)
        {
            $laskey = $this->redis_slave->get('wall:user:key:'.$val['friend_account_id']);
            $this->redis_master->lPush('wall:user:list:'.$laskey.':'.$val['friend_account_id'], $cid);
        }

        $dt['wall_list'] = $content_detail->result_array();

        $htm = $this->load->view('template/aktor/tpl_aktor_wall', $dt, true);

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    private function is_follow($user)
    {
        $select = 'count(*) as total';
        $where = array('account_id' => $this->member['account_id'], 'page_id' => $user);
        $is_follow = $this->aktor_lib->is_follow($select, $where)->row_array(0);
        if(intval($is_follow['total']) == 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus follow !';
            $this->message($data);
        }
    }

    function post_photo()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', '', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('userfile', '', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'title dan photo harus di isi';
            $this->message($data);
        }

        $id = $this->input->post('id', true);
        $user = $this->input->post('user', true);
        $title = $this->input->post('title', true);
        $desc = $this->input->post('description', true);

        $cid = sha1( uniqid().time() );

        // check if follow
        $this->is_follow($user);

        $config['upload_path'] = FCPATH.'public/upload/image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000'; // Kb
        $config['file_name'] = $cid;
        $config['max_width']  = '1024'; // in pixel
        $config['max_height']  = '800'; // in pixel

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('userfile') )
        {
            $data['rcode'] = 'bad';
            $data['msg'] = $this->upload->display_errors();
            $this->message($data);
        }

        $var_upload = $this->upload->data();

        // resize image
        $this->image_resize('600', '800', 'large', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], FALSE);

        $this->image_resize('120', '130', 'thumb', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('320', '240', 'thumb/portrait', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('180', '320', 'thumb/landscape', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('208', '208', 'badge', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('36', '36', 'icon', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $select = 'content_id';
        $where = array(
            'account_id' => $id,
            'content_group_type' => 20,
            'page_id' => $user,
            'content_type' => 'GROUP'
        );
        $group_content = $this->minlime_lib->get_content($select, $where);
        $group_content_id = null;
        if($group_content->num_rows() > 0){
            $group_content_array = $group_content->row_array(0);
            $group_content_id = $group_content_array['content_id'];
        }

        $data_content = array(
            'content_id' => $cid,
            'account_id' => $this->member['account_id'],
            'content_group_type' => 20,
            'group_content_id' => $group_content_id,
            'page_id' => $user,
            'title' => $title,
            'description' => $desc,
            'content_type' => 'IMAGE',
            'entry_date' => mysql_datetime()
        );

        $this->minlime_lib->insert_content($data_content);

        $data_content_attachment = array(
            'content_id' => $cid,
            'attachment_length' => $var_upload['file_size'],
            'attachment_type' => $var_upload['file_type'],
            'attachment_title' => $var_upload['file_name'],
            'attachment_path' => 'public/upload/image/',
            'attachment_width' => $var_upload['image_width'],
            'attachment_height' => $var_upload['image_height']
        );
        $this->minlime_lib->insert_content_attachment($data_content_attachment);

        // get wall key redis
        $last_key = $this->redis_slave->get('wall:user:key:'.$this->member['account_id']);

        // insert content_id to own list
        $this->redis_master->lPush('wall:user:list:'.$last_key.':'.$this->member['account_id'], $cid);

        // get content_detail insert to redis
        $content_detail = $this->minlime_lib->get_content_by_id($cid);
        if($content_detail->num_rows() > 0){
            $cnt_detail = @json_encode($content_detail->row_array());
            $this->redis_master->set('wall:user:detail:'.$cid, $cnt_detail);
        }

        // get friend list
        $friend_list = $this->aktor_lib->get_friend_list($this->member['account_id'], $limit=0, $offset=0);
        foreach($friend_list->result_array() as $row=>$val)
        {
            $last_key_user = $this->redis_slave->get('wall:user:key:'.$val['friend_account_id']);

            $this->redis_master->lPush('wall:user:list:'.$last_key_user.':'.$val['friend_account_id'], $cid);
        }

        // get html template
        $dt['wall_list'] = $content_detail->result_array();

        $htm = $this->load->view('template/komunitas/tpl_komunitas_wall', $dt, true);

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

//    function set_profile_picture()
//    {
//        if($this->isLogin() == false){
//            $data['rcode'] = 'bad';
//            $data['msg'] = 'Anda harus login !';
//            $this->message($data);
//        }
//    }

    function delete_content()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('content_id', 'cid', 'required|trim|xss_clean');
        $this->form_validation->set_rules('tipe', 'tipe post', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';

            $data['msg']['content_id'] = form_error('content_id');
            $data['msg']['report_message'] = form_error('report_message');
            $data['msg']['tipe'] = form_error('tipe');

            $this->message($data);
        }

        $cid = $this->input->post('content_id');
        $tipe = $this->input->post('tipe');

        // CHECK IF OWN CONTENT

        if($tipe == 'content'){
            $select = 'count(*) as total';
            $where = array(
                'content_id' => $cid,
                'account_id' => $this->member['account_id']
            );
            $content = $this->minlime_lib->get_content($select, $where)->row_array(0);
            if(intval($content['total']) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'content not found';
                $this->message($data);
            }

            $where = array('content_id' => $cid);
            $data_content = array('status' => 2);

            $this->minlime_lib->update_tcontent($where, $data_content);

            /*###### GET WALL LIST #########*/
            $wall_list = $this->aktor_lib->get_wall_list_content_id($this->member['account_id'], $this->member['user_id'], $limit=1000, $offset=0);

            $last_key = $this->redis_slave->get('wall:user:key:'. $this->member['account_id']);
            if(empty($last_key))
                $last_key = 0;
            $rkey = $this->redis_master->incr('wall:user:key:'. $this->member['account_id']);
            $pipe = $this->redis_master->multi();
            foreach($wall_list->result_array() as $row=>$val){
                $pipe->rPush('wall:user:list:'.$rkey.':'.$this->member['account_id'], $val['content_id']);
            }
            $pipe->exec();

//            $this->redis_master->delete('wall:user:detail:'.$cid);

            /*** INSERT LOG */
            $data_changed = array(
                'id' => $this->member['account_id'],
                'tipe' => 'wall',
                'post_type' => 3,
                'insert_date' => mysql_datetime(),
                'insert_by' => $this->member['account_id'],
                'status' => 1
            );

            $ids = array('id' => $this->member['account_id']);
            $this->minlime_lib->changed_redis($data_changed, $ids);

            $where = array('content_id' => $cid);
            $data_content = array('status' => 2);

            $this->minlime_lib->update_tcontent_comment($where, $data_content);

            /** delete COMMENT */
            $last_key = $this->redis_slave->get('comment:key:'.$cid);
//            if(empty($last_key))
//                $last_key = 0;

            $this->redis_master->delete('comment:list:'.$last_key.':'.$cid);

        }elseif($tipe == 'comment'){
            $select = '*';
            $where = array(
                'comment_id' => $cid,
                'account_id' => $this->member['account_id']
            );
            $content = $this->minlime_lib->get_tcontent_comment($select, $where);
            if(intval($content->num_rows()) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'content not found';
                $this->message($data);
            }

            $content_first = $content->row_array(0);
//            var_dump($content_first);

            $where = array('comment_id' => $cid);
            $data_content = array('status' => 2);

            $this->minlime_lib->update_tcontent_comment($where, $data_content);

            /** delete COMMENT */
            $last_key = $this->redis_slave->get('comment:key:'.$content_first['content_id']);
            if(empty($last_key))
                $last_key = 0;

            $rkey = $this->redis_slave->incr('comment:key:'.$content_first['content_id']);

            $coment_list = $this->minlime_lib->get_comment_by_content($content_first['content_id']);
            foreach($coment_list->result_array() as $row=>$val){
                $this->redis_master->rPush('comment:list:'.$rkey.':'.$content_first['content_id'], $val['comment_id']);
            }

            $this->redis_master->delete('comment:list:'.$last_key.':'.$content_first['content_id']);
            $this->redis_master->delete('comment:detail:'.$cid);

            $lst_score = $this->minlime_lib->get_content_score($content_first['content_id'])->result_array();

            $this->redis_master->set('content:last:score:'.$content_first['content_id'], json_encode($lst_score));

        }

        $data['rcode'] = 'ok';
        $data['msg'] = 'oke';
        $this->message($data);

    }

    function report_spam()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('content_id', 'cid', 'required|trim|xss_clean');
        $this->form_validation->set_rules('report_message', 'message', 'required|trim|xss_clean');
        $this->form_validation->set_rules('tipe', 'post tipe', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
//            $data['msg'] = 'data yang di input tidak valid';
            $data['msg']['content_id'] = form_error('content_id');
            $data['msg']['report_message'] = form_error('report_message');
            $data['msg']['tipe'] = form_error('tipe');

            $this->message($data);
        }
        $tipe = $this->input->post('tipe');
        $cid = $this->input->post('content_id');
        $message = $this->input->post('report_message');

        // CHECK IF CONTENT
        $content_result = array();
        if($tipe == 'content'){
            $select = '*';
            $where = array(
                'content_id' => $cid
            );
            $content = $this->minlime_lib->get_content($select, $where);
            if(intval($content->num_rows()) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'content not found';
                $this->message($data);
            }

            $content_result = $content->row_array(0);
        }elseif($tipe == 'comment'){
            $content = $this->minlime_lib->get_comment($cid);
            if(intval($content->num_rows()) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'content not found';
                $this->message($data);
            }
            $content_result = $content->row_array(0);
        }

        //check if friend
        /*$select = 'count(*) as total';
        $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$content_result['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$content_result['account_id']."')";
        $is_friend = $this->aktor_lib->get_friend($select, $where)->row_array();
        if(intval($is_friend['total']) == 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'this content is not your friend';
            $this->message($data);
        }*/

        // CHECK KALO BELOM SUBMIT
        $select = 'count(*) as total';
        $where = array(
            'content_id' => $cid,
            'report_from' => $this->member['account_id'],
        );

        $repot = $this->minlime_lib->get_report_spam($select, $where)->row_array(0);
        if(intval($repot['total']) > 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'already submit report';
            $this->message($data);
        }

        // submit
        $cid_spam = sha1( uniqid().time() );
        $data_report = array(
            'tcontent_report_spam_id' => $cid_spam,
            'content_id' => $cid,
            'content_type' => strtoupper($tipe),
            'account_id' => $content_result['account_id'],
            'report_from' => $this->member['account_id'],
            'message' => $message,
            'created_date' => mysql_datetime()
        );

        $this->minlime_lib->report_spam($data_report);
        $data['rcode'] = 'ok';
        $data['msg'] = 'success';
        $this->message($data);
    }

    function unfollow(){
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('tipe', '', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'bad';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $user = $this->input->post('user');
        $tipe = $this->input->post('tipe');

        if(intval($tipe) == 1){
            if($this->member['account_id'] != $id){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $where = array('account_id' => $id, 'page_id' => $user);

        }elseif(intval($tipe) == 2){
            if($this->member['user_id'] != $user){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $where = array('account_id' => $id, 'page_id' => $user);
        }elseif(intval($tipe) == 3){
            if($this->member['account_id'] != $id){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $where = array('account_id' => $id, 'page_id' => $user);
        }

        $this->minlime_lib->delete_follow($where);

        /*###### GET WALL LIST #########*/
        $wall_list = $this->aktor_lib->get_wall_list_content_id($this->member['account_id'], $this->member['user_id'], $limit=1000, $offset=0);

        $last_key = $this->redis_slave->get('wall:user:key:'. $this->member['account_id']);
        if(empty($last_key))
            $last_key = 0;

        $rkey = $this->redis_master->incr('wall:user:key:'. $this->member['account_id']);
        $pipe = $this->redis_master->multi();
        foreach($wall_list->result_array() as $row=>$val){
            $pipe->rPush('wall:user:list:'.$rkey.':'.$this->member['account_id'], $val['content_id']);
        }
        $pipe->exec();

//            $this->redis_master->delete('wall:user:detail:'.$cid);

        /*** INSERT LOG */
        $data_changed = array(
            'id' => $this->member['account_id'],
            'tipe' => 'wall',
            'post_type' => 3,
            'insert_date' => mysql_datetime(),
            'insert_by' => $this->member['account_id'],
            'status' => 1
        );

        $ids = array('id' => $this->member['account_id']);
        $this->minlime_lib->changed_redis($data_changed, $ids);

        $data['rcode'] = 'ok';
        $data['msg'] = 'oke';
        $this->message($data);
    }

    private function image_resize($height, $width, $path, $file_name, $image_width, $image_height, $crop)
    {
        if($height == 0 || $width == 0 || $image_width == 0 || $image_height == 0)
            return;


        $this->load->library('image_lib');
        // Resize image settings
        $config['image_library'] = 'gd2';
        $config['source_image'] = './public/upload/image/'.$file_name;
        $config['new_image'] = "./public/upload/image/".$path."/".$file_name;
        // $config['maintain_ratio'] = TRUE;
        // $config['create_thumb'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        if(!$crop)
        {
            if($height > $image_height && $width > $image_width)
            {
                @copy($config['source_image'],$config['new_image']);
            }
            else
            {
                $config['master_dim'] = 'width';
                $config['maintain_ratio'] = TRUE;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
            }

        }
        else
        {
            if($height > $image_height && $width > $image_width)
            {
                @copy($config['source_image'],$config['new_image']);
            }
            else
            {
                if($image_width/$image_height > $width/$height)
                {
                    $config['master_dim'] = 'height';
                    $config['x_axis'] = (($image_width*($height/$image_height)) - $width) / 2;
                    $config['y_axis'] = 0;
                }
                else
                {
                    $config['master_dim'] = 'width';
                    $config['x_axis'] = 0;
                    $config['y_axis'] = (($image_height*($width/$image_width))  - $height) / 2;
                }

                $config['maintain_ratio'] = TRUE;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $config['source_image'] = $config['new_image'];
                $config['maintain_ratio'] = FALSE;
                $this->image_lib->initialize($config);
                $this->image_lib->crop();
            }
        }

    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    private function isLogin($return=false)
    {
        $cr = current_url();
        $this->member = $this->session->userdata('member');
        if(!$this->member['logged_in']){
            if($this->input->is_ajax_request()){
                return false;
            }else{
                if($return){
                    return false;
                }else{
                    redirect(base_url().'home/login?frm='.urlencode($cr));
                }
            }

        }

        return true;
        //exit('please');
    }

    private function isBijaksUser($user_id)
    {
        $select = 'ta.`account_id`, ta.`user_id`, ta.`display_name`, ta.account_type, ta.`registration_date`, ta.`last_login_date`, ta.`email`, ta.`fname`, ta.`lname`,
ta.`gender`, ta.`birthday`, ta.`current_city`, tp.`profile_content_id`, tp.`about`, tp.`personal_message`, tat.`attachment_title`';
        $user_id = urldecode($user_id);
        $where = array('ta.user_id' => strval($user_id));

        $users = $this->aktor_lib->get_user($select, $where);

        if($users->num_rows() > 0)
        {
            $this->user = $users->row_array(0);
            if($this->user['account_type'] != 1){
                show_404();
            }

        }else{
            show_404();
        }
    }

}