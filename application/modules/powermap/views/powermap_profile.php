<!-- <link rel='stylesheet' href='<?php echo base_url() ?>public/style/powermap.css'> -->
<script src="<?php echo base_url(); ?>public/script/graph/d3.v2.js"></script>

<div class="" id="chart">
	<div class="detail hide" id='short_profile'>
		<div class="media">
			<a style="cursor: pointer;" class="pull-right" onclick="hideprofile()"><i class="icon-remove"></i></a>
          <a class="pull-left" href="#">
            <img class="media-object" style="width: 64px; height: 64px;" src="">
          </a>
          <div class="media-body">
            
            
          </div>
        </div>
	</div>

</div>

<script type="text/javascript">
	var send_graph;

	var dist = 100,
		ch = -5000;

	var width = <?php echo $width; ?>,
	    height = <?php echo $height; ?>,
	    root_node = [],
    	root_links = [];

	var fill = d3.scale.category20();

	var force = d3.layout.force()
	    .gravity(0.8)
	    .linkDistance(150)
	    .charge(-10000)
	    // .friction(0.5)
	    .linkStrength([1])
	    .size([width, height]);


	var dragGroup = d3.behavior.drag()
		.on('dragstart', function(d) {
				
		})
		.on('drag', function(d, i) {
			
			d.x += d3.event.dx;
			d.y += d3.event.dy;
			
			d3.select(this).attr("transform", "translate("+d.x+","+d.y+")");


		});

	var svg = d3.select("#chart").append("svg")
	    .attr("width", width)
	    .attr("height", height)
	    .data([{x: 0, y: 0}]);

	var g = svg.append("svg:g")
		.attr("transform", "translate(-20, 0)")
		.append("svg:g")
		.call(dragGroup);
    
    g.append('rect')
		.attr("width", width*2)
		.attr("height", height*2)
		.style("fill", "transparent")
		.style("stroke-width", "0")
		.style("stroke", "transparent")
		.style("cursor", "move")
		.data([{x: width / 2, y: height / 2}]);

    g.attr("opacity", 1e-6)
		.transition()
		.duration(4000)
		.attr("opacity", 1);

    console.debug('ok');
	
$.ajax({
    type:'post',
    url:"<?php echo base_url();?>powermap/powerprofile/<?php echo $id; ?>/3",
    async : false,
    success: function(graph){
     if(graph.msg == 'ok'){
        send_graph = JSON.stringify(graph);
        root_links = graph.links;
        root_node = graph.nodes;
        update(root_node, root_links);
      }else{
        $('#chart').append('<p>Powermap belum tersedia</p>');
      }
    }
});

function update(nodes, links){

  force
      .nodes(nodes)
      .links(links);


  var link = g.selectAll(".link")
      .data(links);

  var line = link.enter().insert("svg:line", ".node")
    .attr("class", "link")
    .style("stroke-width", function(d, i) { 
      var stro = Math.sqrt(parseInt(d.lebar) + parseInt(d.lebar));
      return Math.sqrt(stro); 
    })
    .attr("x1", function(d) { return d.source.x; })
    .attr("y1", function(d) { return d.source.y; })
    .attr("x2", function(d) { return d.target.x; })
    .attr("y2", function(d) { return d.target.y; });

  link.exit().remove();

  var node = g.selectAll(".node")
      .data(nodes, function(d) { return d.index; })
      
  // node.select("circle");
      
  var nodeEnter = node.enter().append("svg:g")
    .attr("class", "node")
    .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
    .call(force.drag);

  nodeEnter.append("svg:circle")
    .attr("class", "node")
    .attr("r", function(d) { 
    	var lebar = d.size;
        return lebar;
    })
    .style("fill", function(d) { 
      return '#'+d.color;
    })
    .on("click", function(d){ nextchild(d); })
    .on("mouseover", function(d,i){
      /*var dtext = d3.select(this)[0][0].nextSibling;
      var nm = d.name;
      if(d.size <= 8){
        $(dtext).text(nm);
      }*/
    })
    .on("mouseout", function(d,i){
      /*var dtext = d3.select(this)[0][0].nextSibling;
      if(d.size <= 8){
        $(dtext).text('');
      }*/
    });


  nodeEnter.append("svg:text")
    .attr("dx", function(d, i){ 
      var lebarnya = parseInt(d.size) + 5;
      return lebarnya; 
    })
    .attr("dy", ".35em")
    .style("cursor", "pointer")
    .attr("font-size", function(d){ 
    	var si = "14px";
      if(d.size <= 8){
        si = "11px";
      }
      return si;
		
	   })
    .attr("font-weight", function(d){ 
      var si = "bold";
      if(d.size <= 8){
        si = "plain";
      }
      return si;
    
    })
    .text(function(d) { 
       var nm = d.name;
      if(d.dampak){
        nm = d.dampak;
      }
      return nm;
      /*if(d.size > 8){
        return nm;
      }else{
        return "";
      }*/
    })
    .attr("visibility", function(d){
      /*if(d.size > 8){
        return "visible";
      }else{
        return "hidden";
      }*/
    })
    .on("click", rectclick)
    .on("mouseover", function(d,i){
        
        // d3.select(this).text(d.name);
      
    })
    .on("mouseout", function(d,i){
      
        // d3.select(this).text('');
      
    });
    

  node.exit().remove();

  force.on("tick", function() {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    /*nodeEnter.attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; });*/
    node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
  });

  force.start();

  
}

function nextchild(d){
  // var obj_graph = {'graph' : all_graph, 'id':d.page_id};
  // console.debug(send_graph);
  $.ajax({
    'url' : '<?php echo base_url();?>powermap/nextnode',
    'data' : {'graph' : send_graph, 'id':d.page_id},
    'dataType' : 'json',
    'type' : 'post',
    'async' : true,
    'success' : function(graph){
      // console.debug(graph);
      if(graph.msg == 'ok'){
        send_graph = JSON.stringify(graph);
        root_links = graph.links;
        root_node = graph.nodes;
        update(root_node, root_links);
      }
    }
  });
}

function relasi(nodes, links){
	var n_link = [];
	$.each(links, function(i, val){
	  $.each(nodes, function(j, row){
	      
	      if(parseInt(val.source) == parseInt(row.idx)){

	        val.source = j;
	        return false;
	      }
	  });
	  // console.debug(val.target);
	  $.each(nodes, function(k, mow){
	    
	    if(parseInt(val.target) == parseInt(mow.idx)){
	        // console.debug(i + ' ==> '+val.target + ' = '+ mow.idx);
	        val.target = k;
	        return false;
	      }
	  });
	 
	  n_link.push(val);
	});
	return n_link;

}

function rectclick(d){
   
		$('#short_profile .media-body').html('');
		$('#short_profile .media-object').attr('src', '<?php echo base_url("public/assets/img/ui/loading.gif"); ?>');
		view_profile();
  
	function view_profile(){

		var prof_uri = '<?php echo base_url(); ?>politisi/index/';

		var tex = d.name;

		/*if(tex.length > 18)
		tex = d.name.substring(0, 18)+ '...';*/

		var title = '<a class="heading-uri" href="'+prof_uri+d.page_id+'"><h4 class="media-heading">'+tex+'</h4></a>'
		$('#short_profile .media-body').html('');
		$('#short_profile .media-body').append(title);

		//$('#short_profile .heading-uri').attr('href', prof_uri+d.page_id);
		//$('#short_profile .media-heading').text(tex);
		//console.debug($('#short_profile .media-body'));
		var str = '<h5 style="margin-top: 2px;margin-bottom: 2px;">Skandal Terkait :</h5>';

		$.post('<?php echo base_url(); ?>powermap/profile', {'id':d.page_id}, function(data){
		$('#short_profile .media-object').attr('src', data.thumb);
    if(data.msg == 'ok'){
		    $.each(data.scandal, function(i, val){
			    str += '<div style="margin-top: 2px; margin-bottom: 4px;" class="media">';
			          str += '<a class="pull-left" href="#">';
			          str += '<img class="media-object" style="width: 34px; height: 34px;" src="'+val.foto_scandal+'">';
			          str += '</a><div class="media-body">';
			          str += '<a href="'+val.url+'"><h5 style="margin-bottom: -3px;" class="media-heading">'+val.title+'</h5></a>';
			          str += '<span><i class="icon-time"></i>&nbsp;'+val.date+'</span>&nbsp; &bull; &nbsp;';
			          str += '<span><i class="icon-flag"></i>&nbsp;'+val.status+' </span>';
			          //str += '<a href="'+val.url+'"><i class="icon-tasks"></i>&nbsp;Detail</a>';
			          str += '</div></div>';
			          //console.debug(val);
			          

			  });
			  $('#short_profile .media-body').append(str);
		  
		}else{
			  str += '<span>Tidak terlibat skandal</span>';
			  $('#short_profile .media-body').append(str);
		}
			if(!$('#short_profile').is(":visible")){
				$('#short_profile').show('blind');	
			}
			
		});
    };

    
}

function hideprofile()
{
	$('#short_profile').hide('blind', function(){
		$('#short_profile .media-body').html('');
		$('#short_profile .media-object').attr('src', '');
	});
}


</script>
