<?php
$this->load->view('include_me/header');
?>

<link rel='stylesheet' href='<?php echo base_url() ?>public/script/select2/select2.css'>
<script src="<?php echo base_url() ?>public/script/select2/select2.js" ></script>
<style type="text/css">
        .zebra { background-color: #efefef; }

        img.flag { height: 10px; width: 15px; padding-right: 10px; }

        .movie-result td {vertical-align: top }
        .movie-image { width: 60px; }
        .movie-image img { height: 80px; width: 60px;  }
        .movie-info { padding-left: 10px; vertical-align: top; }
        .movie-title { font-size: 1.2em; padding-bottom: 15px; }
        .movie-synopsis { font-size: .8em; color: #888; }
        .select2-highlighted .movie-synopsis { font-size: .8em; color: #eee; }
        .bigdrop.select2-container .select2-results {max-height: 300px;}
        .bigdrop .select2-results {max-height: 300px;}


</style>


<div id='body' class='page page-news page-skandal'>
   <div class='container'>
   	<div class='row row-col'><div class='col-border clearfix' style='border-left: none'>

         <div class='col col-3-4' id='page-news-list' style="width:704px;">
         <div class='block'>
         	<span style="cursor: pointer;" title="Lihat lebih luas" class="pull-right btn btn-mini" id="full_screen"><i class="icon-resize-full" ></i> </span>
         	<h2>Power Mapping</h2>
          		<hr />
         	<div id="mapContent">
         
         		<p>
         			Peta Kekuatan Kekuasaan
         			Setiap hari bahkan setiap saat, begitu banyak informasi dan data yang tersebar maupun berserakan, mengenai figur elit, tokoh maupun institusi yang bergelut di kancah politik Indonesia, dengan berbagai peristiwa serta berita yang menyertainya.
         		</p>
         		<p>
         			Padahal, tidak setiap waktu kita bisa mengikuti atau mendapati rangkaian peristiwa dan berita, apalagi menelusuri data-data terkait yang menunjukkan keterkaitan orang per orang dalam kejadian politik, skandal hukum atau keuangan maupun yang lain, begitu pula jika anda sekadar ingin mengetahui tingkat kekuatan atau kedekatan seseorang dengan poros alias sumbu kekuasaan.
         		</p>
         		<p>
         			Berdasarkan hal itulah, kami menyuguhkan gambaran integral yang konprehensif, namun disajikan dengan singkat, padat dan jelas tanpa banyak kata, melalui tampilan infografis yang menarik.
         		</p>
         		
         		<p>
         			Salam,
         			Bijaks.net
         		</p>
         
         		<span style="cursor: pointer;"  class="pull-left btn btn-mini btn-info" id="lihat_screen">Lihat Powermap</span><br>
         	</div>
         </div>
         </div>

      	<div class='col col-1-4 col-side' id='col-rite' style="width:239px;">
      		<!-- <pre> -->
      		<?php //var_dump($sidebar); ?>
      		<!-- </pre> -->
            <?php $this->load->view('core/sidebar_sentimen'); ?>
      	</div>   
   	</div></div>
	</div>
</div>


<style type="text/css">
#myModal{
	top: 30px;
	left: 30px;
	right: 30px;
	bottom: 30px;
	margin-left: 0px;
	padding-bottom: 30px;
}

#modal_tree{
	/*max-height: 100%;*/
}
</style>

<div id="myModal" class="modal hide fade" style="width: auto; height: auto;">
	<div class="modal-header">
		<button type="button" class="close" style="margin-left: 20px;" data-dismiss="modal" aria-hidden="true">&times;</button>
		
		<input class="span2 pull-right" id="e6" style="z-index: 1000; margin-right: 20px;" type="text">
		
		<h3>Powermap <img class="hide" src="<?php echo base_url();?>public/style/images/ajax-loader.gif" id="ajax_loader" /></h3>
	</div>
	<div id="modal_tree" class="">
		
	</div>
	<!--div class="modal-footer">
		<a href="#" class="btn">Close</a>
		<a href="#" class="btn btn-primary">Save changes</a>
	</div-->
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var width_base = $('#mapContent').width(),
			height1 = $('#col-left').height(),
			height2 = $('#col-rite').height();

		var height_base = height1 > height2 ? parseInt(height1) - 200 : parseInt(height2) - 200;
		// getpowermap('mapContent', width_base, height_base, 2);

		/*$.post('<?php echo base_url(); ?>powermap/tree', {'w': width , 'h': height, 'retas': 2}, function(data){
			$('#mapContent').append(data);
		});*/

		$('#full_screen').click(function(){
			$('#myModal').modal('show');
		});

		$('#lihat_screen').click(function(){
			$('#myModal').modal('show');
		});

		$('#myModal').on('shown', function(){
			// $('#mapContent').html('');
			
			$('#ajax_loader').show();
			var width = $('#myModal').width(),
				height = parseInt($('#myModal').height()) - 30;

			getpowermap('modal_tree', width, height, 2, 0);
			

			/*$.post('<?php echo base_url(); ?>powermap/tree', {'w': width , 'h': height, 'retas': 1}, function(data){
				console.debug(data);
				$('#myModal .modal-body').append(data);
				$('#ajax_loader').hide();
			});*/

		});

		$('#myModal').on('hidden', function(){
			$('#modal_tree').html('');
			// getpowermap('mapContent', width_base, height_base, 2);
		});

		e6show();
		
	});

	function e6show()
	{
		$("#e6").select2({
			id: function(e) { return e.name },
		    placeholder: "Cari node",
		    minimumInputLength: 3,
		    allowClear: true,
		    width: 250,
		    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
		        url: "<?php echo base_url(); ?>powermap/nodelist",
		        dataType: 'json',
		        data: function (term, page) {
		            return {
		                q: term, // search term
		                page_limit: 10
		                 // please do not use so this example keeps working
		            };
		        },
		        results: function (data, page) { // parse the results into the format expected by Select2.
		            // since we are using custom formatting functions we do not need to alter remote JSON data
		            return {results: data.politisi};
		        }
		    },
		    
		    formatResult: movieFormatResult, // omitted for brevity, see the source of this page
		    formatSelection: movieFormatSelection,  // omitted for brevity, see the source of this page
		    dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller
		    //escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
		});
	}

	function movieFormatResult(politisi) {
       
        var markup = "<div>";
        
        markup += "<img style='max-width: 40px; margin-right: 5px;' src='"+politisi.photo_profile+"' /><span>" + politisi.name + "</span>";
        
        markup += "</div>"

        return markup;
    }


    function movieFormatSelection(politisi) {
    	$('#ajax_loader').show();
    	$('#modal_tree').html('');
    	getpowermap('modal_tree', width, height, 2, politisi.page_id);
    	return politisi.name;
    }

	function getpowermap(id, width, height, type, idx)
	{
		$.post('<?php echo base_url(); ?>powermap/maps', {'w': width , 'h': height, 'retas': type, 'id':idx}, function(data){
			$('#'+id).append(data);
			$('#ajax_loader').hide();
		});
	};

</script>


<?php
$this->load->view('include_me/footer')
?>