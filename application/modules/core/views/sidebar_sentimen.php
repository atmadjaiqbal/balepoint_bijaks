<?php if(!empty($sidebar)):?>

<?php foreach ($sidebar as $section_key =>   $section) :?>
   <?php if(!empty($sidebar[$section_key])):?>
      <div class='block' style="padding-bottom:5px;">
         <h3 style="margin-bottom:5px;"><?php echo ucfirst($section_key);?> terkini</h3>

         <section class="feeds">
         <?php foreach ($sidebar[$section_key]as $key => $row) :?>
         <?php 
				$contentID 		= $row['content_id'];
				$contentDate 	= mysql_date("%d %M %Y - %h:%i", strtotime($row['updated_date']));
            $url 				= base_url(). $section_key .'/index/'. $row['id'] . "-" . urltitle($row['title']) . "/";
         ?>
            <article class="no-figure">
               <a href="<?php echo $url;?>" title="<?php echo $row['title']; ?>"><?php echo word_limiter($row['title'],6);?></a>
					<div class="more-space" >
						<small class='cid_<?php echo $contentID;?>' data-entrydate='<?php echo $contentDate; ?>' data-tipe='2' data-restype='2' style="">
							<span style="padding-right: 10px;"><?php echo $contentDate; ?></span>
							<span class="postmeta postmeta-comment">-</span>
							|
							<span class="like-content postmeta postmeta-like">-</span>
							<span class="dislike-content postmeta postmeta-dislike">-</span>
						</small>
					</div>
                
            </article>
         <?php endforeach;?>
         </section>   
      </div>

   <?php endif;?>
<?php endforeach;?>
<?php endif;?>
