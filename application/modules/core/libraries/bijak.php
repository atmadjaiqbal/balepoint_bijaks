<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Bijak {

	private $CI;

	public function __construct()
	{
		$this->CI =& get_instance();

		$this->db         = $this->CI->load->database('slave', true);
		$this->db_master  = $this->CI->load->database('master', true);
	}

	public function getContentCountMultiple($accountID, $contentID) {


		  if(is_array($contentID))
		  {
				$contentID = " IN ( '".implode("','",$contentID)."')";
		  } else {
				$contentID = " = '".$contentID."'";
		  }

			$sql = "SELECT tc.content_id as content_id,
							  tmcgt.content_group_name as content_group_name,
							  IFNULL(tct.content_date,tc.entry_date) as content_date,
							  tc.entry_date as entry_date,
							  get_count_comment_content(tc.content_id) as count_comment,
							get_count_like_content(tc.content_id) as count_like,
							get_count_dislike_content(tc.content_id) as count_dislike,
							get_count_like_content_by_account('".$accountID."', tc.content_id) as is_like,
							get_count_dislike_content_by_account('".$accountID."', tc.content_id) as is_dislike
				  FROM  tcontent tc
					 LEFT JOIN tcontent_text tct ON tc.content_id = tct.content_id
					 LEFT JOIN tm_content_group_type tmcgt ON tmcgt.content_group_type = tc.content_group_type
				  WHERE tc.content_id ".$contentID;

		$result = $this->db->query($sql)->result_array();

		return $result;

	}

	public function getContentCountAction($accountID, $contentID) {


		  if(is_array($contentID))
		  {
				$contentID = " IN ( '".implode("','",$contentID)."')";
		  } else {
				$contentID = " = '".$contentID."'";
		  }

			$sql = "SELECT tc.content_id as content_id,
							  tmcgt.content_group_name as content_group_name,
							  IFNULL(tct.content_date,tc.entry_date) as content_date,
							  tc.entry_date as entry_date,
							  tc.count_comment, tc.count_like, tc.count_dislike,
							  get_count_like_content_by_account('".$accountID."', tc.content_id) as is_like,
							  get_count_dislike_content_by_account('".$accountID."', tc.content_id) as is_dislike
				  FROM  tcontent tc
					 LEFT JOIN tcontent_text tct ON tc.content_id = tct.content_id
					 LEFT JOIN tm_content_group_type tmcgt ON tmcgt.content_group_type = tc.content_group_type
				  WHERE tc.content_id ".$contentID;

		$result = $this->db->query($sql)->result_array();

		return $result;

	}


	public function getContentCount($accountID, $contentID) {
			$sql = "SELECT get_count_comment_content(tc.content_id) as count_comment,
							get_count_like_content(tc.content_id) as count_like,
							get_count_dislike_content(tc.content_id) as count_dislike,
							get_count_like_content_by_account('".$accountID."', tc.content_id) as is_like,
							get_count_dislike_content_by_account('".$accountID."', tc.content_id) as is_dislike
				  FROM  tcontent tc
				  WHERE tc.content_id = '".$contentID."'";

		$result = $this->db->query($sql)->row_array();

		return $result;

	}

	public function getContent($select, $where)
	{
		$this->db->select($select);
		$this->db->where($where);
		$data = $this->db->get('tcontent');
		return $data;
	}

	public function getContentText($select, $where)
	{
		$this->db->select($select);
		$this->db->where($where);
		$data = $this->db->get('tcontent_text');
		return $data;
	}

	public function getContentComment($contentID)
	{
		$sql = "SELECT d1.*, d2.*, ta.attachment_title
					FROM tcontent_comment d1
					LEFT JOIN tusr_account d2 ON d1.account_id = d2.account_id
					LEFT JOIN tobject_page p ON p.page_id = d2.user_id
					LEFT JOIN tcontent_attachment ta ON ta.content_id = p.profile_content_id
					WHERE d1.content_id = '$contentID' and d1.status = 0
					ORDER BY entry_date";
		$result = $this->db->query($sql)->result_array();
		return $result;
	}

	public function getUserDetail($accountID)
	{
		$sql = "SELECT tp.profile_content_id content_id, tca.attachment_title
					FROM tusr_account ta
					LEFT JOIN tobject_page tp ON tp.page_id = ta.user_id
					LEFT JOIN tcontent_attachment tca ON tca.content_id = tp.profile_content_id
					WHERE ta.account_id = '$accountID' ";
		$result = $this->db->query($sql)->row_array();
		return $result;
	}

	public function getPolitisiTerkait($contentID, $type='cid', $limit=50, $display='image')
	{
		$this->CI->load->helper('imagefull');

		$str = "";
		$sql = "";

		if($type == 'cid'){
			$sql = "
			SELECT
				thp.*, ifnull(p.page_name, thp.page_id) AS page_name,
				(
					CASE
					WHEN ifnull(p.page_name, thp.page_id) = thp.page_id THEN
					'true'
					ELSE
					'false'
					END
				)AS equal_page_id_name,
				tca.attachment_title
			FROM
				tcontent_has_politic thp left
				JOIN tobject_page p ON thp.page_id = p.page_id LEFT
				JOIN tcontent_attachment tca ON(
					tca.content_id = p.profile_content_id
				)
			WHERE
				thp.content_id = '".$contentID."'
			LIMIT 0, $limit ";
		}

		$politisi = $this->db->query($sql)->result_array();
		sort($politisi);

		if( count($politisi) > 0 ) {
			foreach($politisi as $i => $poli) {
				$url = base_url().'politisi/index/'.$poli['page_id'];

				if ($display == 'image') {
					$str .= '<a class="tooltip-top block-user-friend-pic" href="'.base_url().'politisi/index/'.$poli['page_id'].'" title="'.$poli['page_name'].'">
									<img alt="'.$poli['page_name'].'" src="'. icon_url($poli['attachment_title'],'politisi/'.$poli['page_id']) .'">
								</a>';
				}

				if ($display == 'headline') {
					$str .= '<li><a class="headline-subtitle" href="'.$url.'" >'.$poli['page_name'].'</a></li>';
				}
			}
			if ($display == 'headline') {
				$str = '<ul>'.$str.'<ul>';
			}
		}
		return $str;
	}


	public function getSuksesi($type="latest", $offset=0, $limit=5)
	{
		$where      = " AND tr.publish = '1'";
		$order_by   = '';
		if ($type='latest') $order_by = ' tr.update_date DESC';

		$sql     = "SELECT tr.id_race id, tc.content_id, tr.race_name title , DATE_FORMAT(tr.update_date, '%d %M %Y - %h:%i') updated_date
						FROM trace tr
						LEFT JOIN tcontent tc on tr.id_race = tc.location AND tc.content_group_type = '56'
						WHERE 1=1 ".$where." ORDER BY ".$order_by ."  LIMIT ".$offset.", ".$limit;
		$result  = $this->db->query($sql)->result_array();

		/*
		if (!empty($result))
		{
			$cm  = $this->CI->load->module('ajax');
			foreach ($result as $key => $row) {
				$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['updated_date']));

				$result[$key]['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate);
			}
		}
		*/
		return $result;
	}

	public function getScandal($type="latest", $offset=0, $limit=5)
	{
		$where      = " AND ts.publish = '1'";
		$order_by   = '';
		if ($type='latest') $order_by = ' ts.updated_date DESC';

		$sql     = "SELECT ts.scandal_id id,tc.content_id,  ts.title, DATE_FORMAT(ts.updated_date, '%d %b %Y - %h:%i')  updated_date
						FROM tscandal ts
						LEFT JOIN tcontent tc on ts.scandal_id = tc.location AND tc.content_group_type = '50'
						WHERE 1=1 ".$where." ORDER BY ".$order_by ."  LIMIT ".$offset.", ".$limit;
		$result  = $this->db->query($sql)->result_array();
		/*
		if (!empty($result))
		{
			$cm  = $this->CI->load->module('ajax');
			foreach ($result as $key => $row) {
				$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['updated_date']));
				$result[$key]['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate);
			}
		}
		*/

		return $result;
	}


	public function getSurvey($type="latest", $offset=0, $limit=5)
	{
		$where      = " AND ts.publish = '1'";
		$order_by   = '';
		if ($type='latest') $order_by = ' tc.last_update_date  DESC';

		$sql     = "SELECT ts.survey_id id,tc.content_id,  ts.name title, DATE_FORMAT(tc.last_update_date, '%d %M %Y - %h:%i')  updated_date
						FROM survey ts
						LEFT JOIN tcontent tc on ts.survey_id = tc.location AND tc.content_group_type = '54'
						WHERE 1=1 ".$where." ORDER BY ".$order_by ."  LIMIT ".$offset.", ".$limit;
		$result  = $this->db->query($sql)->result_array();
		/*
		if (!empty($result))
		{
			$cm    = $this->CI->load->module('ajax');
			foreach ($result as $key => $row) {
				$contentDate                     = mdate("%d %M %Y - %h:%i", strtotime($row['updated_date']));
				$result[$key]['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate);
			}
		}
		*/


		return $result;
	}

	public function getSidebarSentimen()
	{
		$data['survey']    = $this->getSurvey();
		$data['suksesi']   = $this->getSuksesi();
		$data['scandal']   = $this->getScandal();
		return $data;
	}

	/**
		GET PAGE COUNT
	**/



	public function gePageCountMultiple($accountID, $contentID) {


		  if(is_array($contentID))
		  {
				$contentID = " IN ( '".implode("','",$contentID)."')";
		  } else {
				$contentID = " = '".$contentID."'";
		  }

			$sql = "
			SELECT
			all_rows.page_id,
			COUNT(DISTINCT all_rows.like_by) AS like_count,
			IF(SUM(all_rows.is_liked) > 0, 1, 0) AS is_liked,
			COUNT(DISTINCT all_rows.dislike_by) AS dislike_count,
			IF(SUM(all_rows.is_disliked) > 0, 1, 0) AS is_disliked,
			COUNT(DISTINCT all_rows.comment_by) AS comment_count,
			COUNT(DISTINCT all_rows.follow_by) AS follow_count,
			IF(SUM(all_rows.is_followed) > 0, 1, 0) AS is_followed
			FROM
			(
				SELECT
				top.page_id,
				pl.account_id AS like_by,
				IF(pl.account_id = '".$accountID."', 1, 0) AS is_liked,
				pdl.account_id AS dislike_by,
				IF(pdl.account_id = '".$accountID."', 1, 0) AS is_disliked,
				pc.account_id AS comment_by,
				tuf.account_id AS follow_by,
				IF(tuf.account_id = '".$accountID."', 1, 0) AS is_followed
				FROM
				tobject_page top
				LEFT JOIN page_like pl ON pl.page_id = top.page_id
				LEFT JOIN page_dislike pdl ON pdl.page_id = top.page_id
				LEFT JOIN page_comment pc ON pc.page_id = top.page_id
				LEFT JOIN tusr_follow tuf ON tuf.page_id = top.page_id
				WHERE top.page_id ".$contentID."
			) AS all_rows

		  GROUP BY
		  all_rows.page_id
		  ";

		$result = $this->db->query($sql)->result_array();

		return $result;

	}

	public function getpagelike($select, $where)
	{
		$this->db->select($select);
		$this->db->where($where);
		$data = $this->db->get('page_like');
		return $data;
	}

	public function getpagedislike($select, $where)
	{
		$this->db->select($select);
		$this->db->where($where);
		$data = $this->db->get('page_dislike');
		return $data;
	}

	public function getpagecomment($select, $where)
	{
		$this->db->select($select);
		$this->db->where($where);
		$data = $this->db->get('page_comment');
		return $data;
	}

	public function getpagefollow($select, $where)
	{
		$this->db->select($select);
		$this->db->where($where);
		$data = $this->db->get('tusr_follow');
		return $data;
	}

	public function insertpagefollow($data)
	{
		$this->db->insert('tusr_follow', $data);
	}

	public function deletepagefollow($where)
	{
		$this->db->where($where);
		$this->db->delete('tusr_follow');
	}

	public function insertpagelike($data)
	{
		$this->db->insert('page_like', $data);
	}

	public function deletepagelike($where)
	{
		$this->db->where($where);
		$this->db->delete('page_like');
	}

	public function insertpagedislike($data)
	{
		$this->db->insert('page_dislike', $data);
	}

	public function deletepagedislike($where)
	{
		$this->db->where($where);
		$this->db->delete('page_dislike');
	}

	 public function getCommentPage($where){
		  $this->db->select('d1.*, d3.page_id, d3.page_name, d3.profile_content_id, d4.attachment_title');
		  $this->db->from('page_comment d1');
		  $this->db->join('tusr_account d2', 'd2.account_id = d1.account_id', 'left');
		  $this->db->join('tobject_page d3', 'd3.page_id = d2.user_id', 'left');
		  $this->db->join('tcontent_attachment d4', 'd4.content_id = d3.profile_content_id', 'left');
		  $this->db->where($where);
		  $query = $this->db->get();
		  return $query;
	 }

	 public function insertCommentPage($data){
		  $this->db->insert('page_comment', $data);

		  return $this->db->insert_id();
	 }

	 public function deleteCommentPage($where)
	 {
		  $this->db->where($where);
		  $this->db->delete('page_comment');
	 }


	 public function isInFriendFollowFollower($home_account_id, $home_page_id, $guest_account_id)
	 {

		  $sql = "
				SELECT
				*
				FROM
				(SELECT
					*
				FROM
					(
						SELECT
							'follow' AS type,
							tua.account_id
						FROM
							tusr_follow tuf
						LEFT JOIN tusr_account tua ON tuf.page_id = tua.user_id
						LEFT JOIN tobject_page top ON top.page_id = tuf.page_id
						LEFT JOIN tcontent_attachment tca ON tca.content_id = top.profile_content_id
						WHERE
							tuf.account_id = ?
						AND tua.account_status = 1
					) AS list_relation
				UNION ALL
					(
						SELECT
							'friend' AS type,
							tua_fr.account_id
						FROM
							(
								(
									SELECT
										tufr.friend_account_id AS friend_account_id
									FROM
										tusr_friend tufr
									WHERE
										tufr.account_id = ?
								)
								UNION ALL
									(
										SELECT
											tufr.account_id AS friend_account_id
										FROM
											tusr_friend tufr
										WHERE
											tufr.friend_account_id = ?
									)
							) AS friends_list
						LEFT JOIN tusr_account tua_fr ON tua_fr.account_id = friends_list.friend_account_id
						LEFT JOIN tobject_page top_fr ON top_fr.page_id = tua_fr.user_id
						LEFT JOIN tcontent_attachment tca_fr ON tca_fr.content_id = top_fr.profile_content_id
						WHERE
							tua_fr.account_status = 1
					)
				UNION ALL
					SELECT
						'follower' AS type,
						tuax.account_id
					FROM
						tusr_follow tuf
					LEFT JOIN tusr_account tuax ON tuf.account_id = tuax.account_id
					LEFT JOIN tobject_page top ON top.page_id = tuf.page_id
					LEFT JOIN tcontent_attachment tca ON tca.content_id = top.profile_content_id
					WHERE
						tuf.page_id = ?
					AND tuax.account_status = 1
				) AS list_rel
				WHERE
				list_rel.account_id = ?
		  ";

		  $result  = $this->db->query($sql, array($home_account_id, $home_account_id, $home_account_id, $home_page_id, $guest_account_id))->result_array();

		  if(isset($result[0]))
		  {
				return $result;

		  } else {
				return array();
		  }
	 }

	 public function listFriendFollowFollower($account_id, $page_id)
	 {
		  $sql = "
				SELECT
				group_relation.account_id,
				GROUP_CONCAT(group_relation.type SEPARATOR '|') AS type_rel
				FROM
				(
				SELECT
					*
				FROM
					(
						SELECT
							'follow' AS TYPE,
							tua.account_id
						FROM
							tusr_follow tuf
						LEFT JOIN tusr_account tua ON tuf.page_id = tua.user_id
						LEFT JOIN tobject_page top ON top.page_id = tuf.page_id
						LEFT JOIN tcontent_attachment tca ON tca.content_id = top.profile_content_id
						WHERE
							tuf.account_id = ?
						AND tua.account_status = 1
					) AS list_relation
				UNION ALL
					(
						SELECT
							'friend' AS TYPE,
							tua_fr.account_id
						FROM
							(
								(
									SELECT
										tufr.friend_account_id AS friend_account_id
									FROM
										tusr_friend tufr
									WHERE
										tufr.account_id = ?
								)
								UNION ALL
									(
										SELECT
											tufr.account_id AS friend_account_id
										FROM
											tusr_friend tufr
										WHERE
											tufr.friend_account_id = ?
									)
							) AS friends_list
						LEFT JOIN tusr_account tua_fr ON tua_fr.account_id = friends_list.friend_account_id
						LEFT JOIN tobject_page top_fr ON top_fr.page_id = tua_fr.user_id
						LEFT JOIN tcontent_attachment tca_fr ON tca_fr.content_id = top_fr.profile_content_id
						WHERE
							tua_fr.account_status = 1
					)
				UNION ALL
					SELECT
						'follower' AS TYPE,
						tuax.account_id
					FROM
						tusr_follow tuf
					LEFT JOIN tusr_account tuax ON tuf.account_id = tuax.account_id
					LEFT JOIN tobject_page top ON top.page_id = tuf.page_id
					LEFT JOIN tcontent_attachment tca ON tca.content_id = top.profile_content_id
					WHERE
						tuf.page_id = ?
					AND tuax.account_status = 1
				) AS group_relation
				GROUP BY group_relation.account_id
		  ";

		  $result  = $this->db->query($sql, array($account_id, $account_id, $account_id, $page_id))->result_array();


		  if(isset($result[0]))
		  {
				return $result;

		  } else {
				return array();
		  }

	 }

	 public function isFriend($account_id, $friend_account_id)
	 {
		  $sql = "
			SELECT IF(count(*) > 0, TRUE, FALSE) as is_friend  FROM
		  (
			(
				SELECT
					tufr.friend_account_id AS friend_account_id
				FROM
					tusr_friend tufr
				WHERE
					tufr.account_id = ?
			)
			UNION ALL
			(
				SELECT
					tufr.account_id AS friend_account_id
				FROM
					tusr_friend tufr
				WHERE
					tufr.friend_account_id = ?
			)
		  ) AS list_friend
		  WHERE list_friend.friend_account_id = ?
		  ";

		  $result  = $this->db->query($sql, array($account_id, $account_id, $friend_account_id))->result_array();


		  if(isset($result[0]))
		  {

				return $result[0]['is_friend'];
		  } else {
				return false;
		  }

	 }

	 public function listFriend($account_id)
	 {
		  $sql = "
		  (
			SELECT
				tufr.friend_account_id AS friend_account_id
			FROM
				tusr_friend tufr
			WHERE
				tufr.account_id = ?
		)
		UNION ALL
		(
			SELECT
				tufr.account_id AS friend_account_id
			FROM
				tusr_friend tufr
			WHERE
				tufr.friend_account_id = ?
		)
		  ";

		  $result  = $this->db->query($sql, array($account_id, $account_id))->result_array();

		  return $result;
	 }

	 public function accountInfo($account_id)
	 {
		  $sql = "
				SELECT
				IF (
					tusr_account.account_type = 1,
					'politisi',
					'user'
				) AS type,
				 tusr_account.account_id,
				 tusr_account.user_id AS page_id,
				 tusr_account.display_name,
				 tcontent_attachment.attachment_title AS photo_profile
				FROM
					tusr_account
				INNER JOIN tobject_page ON tobject_page.page_id = tusr_account.user_id
				INNER JOIN tcontent_attachment ON tcontent_attachment.content_id = tobject_page.profile_content_id
				WHERE
					tusr_account.account_id = ?
		  ";

		  $result  = $this->db->query($sql, array($account_id))->result_array();

		  if(isset($result[0]))
		  {

				return $result[0];
		  } else {

				return array();
		  }
	 }


	public function addFriendRequest($account_id, $guest_account_id, $request_message)
	{
		$friend_request_id = NULL;
		$query = $this->db->query("SELECT friend_request_id FROM tusr_friend_request WHERE account_id = ? and to_account_id = ?", array($account_id, $guest_account_id));
		if($query->num_rows() == 0)
		{
			$friend_request_id = sha1( uniqid() );
			$query1 = $this->db->query("INSERT INTO tusr_friend_request SET friend_request_id = ?, flag = 0, account_id = ?, to_account_id = ?, request_msg = ?, request_date = NOW()",
										array($friend_request_id, $account_id, $guest_account_id, $request_message));
		}
		else
		{
			$friend_request_id = $query->row()->friend_request_id;
			$query1 = $this->db->query("UPDATE tusr_friend_request SET flag = 0, request_msg = ?, request_date = NOW() WHERE account_id = ? AND to_account_id = ?",
										array($request_message, $account_id, $guest_account_id));
		}
		return $friend_request_id;
	}

	public function unfriend($account_id, $friend_id)
	{
		$query = $this->db->query("DELETE FROM tusr_friend
				WHERE (account_id = ? AND friend_account_id = ?)
				OR (account_id = ? AND friend_account_id = ?)",
				array($account_id, $friend_id, $friend_id, $account_id));
	}



	public function removeFollowUser($account_id, $page_id)
	{
		$query1 = $this->db->query("DELETE FROM tusr_follow WHERE account_id = ? AND  page_id = ?", array($account_id, $page_id));
	}

	 function getDisplayNameFromUserId($user_id)
	 {
		  $sql = "
				SELECT
					tusr_account.display_name
				FROM
					tusr_account
				WHERE
					tusr_account.user_id = ?;
		  ";

		  $result  = $this->db->query($sql, array($user_id))->result_array();

		  if(isset($result[0]))
		  {

				return $result[0]['display_name'];
		  } else {
				return '';
		  }

	 }

	 function getAccountIdFromUserId($user_id)
	 {
		  $sql = "
				SELECT
					tusr_account.account_id
				FROM
					tusr_account
				WHERE
					tusr_account.user_id = ?;
		  ";

		  $result  = $this->db->query($sql, array($user_id))->result_array();

		  if(isset($result[0]))
		  {

				return $result[0]['account_id'];
		  } else {
				return '';
		  }

	 }

	 function getProfileFromPageId($page_id)
	 {
		  $sql = "
				SELECT
					ta.display_name AS username,
					ta.user_id AS user_id,
					ta.email AS email,
					ta.account_type AS account_type,
					ta.account_id AS account_id,
					p.profile_content_id AS profile_id,
					a.attachment_title AS xname
				FROM
					tusr_account ta
				LEFT JOIN tobject_page p ON p.page_id = ta.user_id
				LEFT JOIN tcontent_attachment a ON a.content_id = p.profile_content_id
				WHERE
					ta.user_id = ?
		  ";

		  $result  = $this->db->query($sql, array($page_id))->result_array();


		  if(isset($result[0]))
		  {
				$return['username'] = $result[0]['username'];
				$return['user_id'] = $result[0]['user_id'];
				$return['email'] = $result[0]['email'];
				$return['account_type'] = $result[0]['account_type'];
				$return['account_id'] = $result[0]['account_id'];
				$return['profile_id'] = $result[0]['profile_id'];
				$return['xname'] = $result[0]['xname'];
				$return['affiliasi'] = $this->getUserAffiliasiText($result[0]['user_id']);

				return $return;

		  } else {

				return array();
		  }
	 }


	 function getProfileFromAccountId($account_id)
	 {
		  $sql = "
				SELECT
					ta.display_name AS username,
					ta.user_id AS user_id,
					ta.email AS email,
					ta.account_type AS account_type,
					ta.account_id AS account_id,
					p.profile_content_id AS profile_id,
					a.attachment_title AS xname
				FROM
					tusr_account ta
				LEFT JOIN tobject_page p ON p.page_id = ta.user_id
				LEFT JOIN tcontent_attachment a ON a.content_id = p.profile_content_id
				WHERE
					ta.account_id = ?
		  ";

		  $result  = $this->db->query($sql, array($account_id))->result_array();


		  if(isset($result[0]))
		  {
				$return['username'] = $result[0]['username'];
				$return['user_id'] = $result[0]['user_id'];
				$return['email'] = $result[0]['email'];
				$return['account_type'] = $result[0]['account_type'];
				$return['account_id'] = $result[0]['account_id'];
				$return['profile_id'] = $result[0]['profile_id'];
				$return['xname'] = $result[0]['xname'];
				$return['affiliasi'] = $this->getUserAffiliasiText($result[0]['user_id']);

				return $return;

		  } else {

				return array();
		  }
	 }



	public function getUserAffiliasiText($user_id) {
		if(! empty($user_id)) {
			$rs = $this->getUserAffiliasi($user_id);
			$str = array();
			foreach($rs as $row) {
				if($row['page_name'] != $row['page_id']) {
					$str[] = "<a href='" . base_url() . "politisi/index/" . $row['page_id'] . "' title='" . $row['page_name'] . "'>" . $row['page_name'] . "</a>";
				} else {
					$str[] = "<a title='" . $row['page_name'] . "'>" . $row['page_name'] . "</a>";
				}
			}
			return implode(", ", $str);
		} else {
			return "";
		}
	}


	public function getUserAffiliasi($user_id) {
		if(! empty($user_id)) {
			// TO GET LIST OF POLITISI & PARTAI FROM AFFILIASI USER
			$sql = "SELECT politic_id as page_id FROM `tuser_affiliasi` WHERE user_id = ?";
			$query = $this->db->query($sql, array($user_id))->result_array();
			foreach($query as &$row) {
				$sql = "SELECT page_name, profile_content_id FROM `tobject_page` WHERE page_id = ?";
				$rs = $this->db->query($sql, array($row['page_id']))->result_array();
				if(! empty($rs)) {
					$row['page_name'] = $rs[0]['page_name'];
					$row['profile_content_id'] = $rs[0]['profile_content_id'];
				} else {
					$row['page_name'] = $row['page_id'];
					$row['profile_content_id'] = "";
				}
			}
			return $query;
		} else {
			return array();
		}
	}

	 public function count_profile_info($my_account_id, $profile_page_id)
	 {
		  $sql = "
				SELECT
				IF((SELECT count(*) AS count_request_friend_from FROM tusr_friend_request WHERE (to_account_id = tusr_account.account_id AND flag = 0 AND account_id = ? )) > 0,'yes','no') AS you_request,
				IF((SELECT count(*) AS count_request_friend_to FROM tusr_friend_request WHERE (account_id = tusr_account.account_id AND flag = 0 AND to_account_id = ? )) > 0,'yes','no') AS they_request,
				IF((SELECT count(*) AS count_follow FROM tusr_follow WHERE (account_id = ? AND page_id = tusr_account.user_id)) > 0,'yes','no') AS you_follow,
				IF(
						(
							SELECT
							count(*) AS is_friend
							FROM
							tusr_friend
							WHERE
							(
								( tusr_friend.account_id = tusr_account.account_id)
								AND
								( tusr_friend.friend_account_id = ? )
							)
							OR
							(
								( tusr_friend.account_id = ? )
								AND
								( tusr_friend.friend_account_id = tusr_account.account_id)
							)
						) > 0, 'yes','no') AS is_friend

				FROM
				tusr_account
				WHERE
				tusr_account.user_id = ?
		  ";


		  $result  = $this->db->query($sql, array($my_account_id, $my_account_id, $my_account_id, $my_account_id, $my_account_id, $profile_page_id))->result_array();

		  if(isset($result[0]))
		  {

				return $result[0];

		  } else {
				return array(
						'you_request' => 'no',
						'they_request' => 'no',
						'you_follow' => 'no',
						'is_friend' => 'no'
				);
		  }
	 }


	 public function getContentTypeName($content_id)
	 {
		  $sql = "
				SELECT
					tm_content_group_type.content_group_name AS group_name
				FROM
					tcontent
				LEFT JOIN tm_content_group_type ON tm_content_group_type.content_group_type = tcontent.content_group_type
				WHERE
					tcontent.content_id = ?
		  ";

		  $result  = $this->db->query($sql, array($content_id))->result_array();

		  if(isset($result[0]))
		  {
				return $result[0]['group_name'];

		  } else {
				return '';
		  }

	 }


	 public function checkIsContentCanComment($account_id, $page_id, $content_id)
	 {
		  $sql = "
				SELECT
					COUNT(tcontent.content_id) AS count
				FROM
				tcontent
				LEFT JOIN tm_content_group_type ON tm_content_group_type.content_group_type = tcontent.content_group_type
				INNER JOIN (
					SELECT
						*
					FROM
						(
							(
								SELECT
									'self' AS type,
									top_me.page_id,
									tua_me.account_id,
									IFNULL(tua_me.account_status, 1) AS account_status
								FROM
									tobject_page top_me
								LEFT JOIN tusr_account tua_me ON top_me.page_id = tua_me.user_id
								LEFT JOIN tcontent_attachment tca_me ON tca_me.content_id = top_me.profile_content_id
								WHERE
									(
										tua_me.account_id = ?
										OR top_me.page_id = ?
									)
							)
							UNION ALL
								(
									SELECT
										'friend' AS type,
										tua_fr.user_id AS page_id,
										tua_fr.account_id,
										IFNULL(tua_fr.account_status, 1) AS account_status
									FROM
										(
											(
												SELECT
													tufr.friend_account_id AS friend_account_id
												FROM
													tusr_friend tufr
												WHERE
													tufr.account_id = ?
											)
											UNION ALL
												(
													SELECT
														tufr.account_id AS friend_account_id
													FROM
														tusr_friend tufr
													WHERE
														tufr.friend_account_id = ?
												)
										) AS friends_list
									LEFT JOIN tusr_account tua_fr ON tua_fr.account_id = friends_list.friend_account_id
									LEFT JOIN tobject_page top_fr ON top_fr.page_id = tua_fr.user_id
									LEFT JOIN tcontent_attachment tca_fr ON tca_fr.content_id = top_fr.profile_content_id
									WHERE
										tua_fr.account_status = 1
								)
						) AS friend_list
					GROUP BY
						friend_list.page_id
				) AS final_friend_list ON ( final_friend_list.page_id = tcontent.page_id OR final_friend_list.account_id = tcontent.account_id)

				WHERE
				tcontent.content_id = ?
		  ";




		  $result  = $this->db->query($sql, array($account_id, $page_id, $account_id, $account_id,$content_id))->result_array();
		  // echo $this->db->last_query();
		  if(isset($result[0]))
		  {
				if($result[0]['count'] > 0)
				{
					 return TRUE;
				} else {
					 return FALSE;
				}


		  } else {
				return FALSE;
		  }


	 }

	 public function streamGuestWall($guestmember_account_id, $page='1', $limit='10', $old = FALSE)
	 {

			 $this->CI->load->library('session');
		  $this->CI->load->model('member');
		  $this->CI->load->model('Sentimen_model');
		  $this->CI->load->model('Suksesi_model');
		  $this->CI->load->model('content_stream');

		  $member = $this->CI->session->userdata('member');

		  $guestmember = $this->getProfileFromAccountId($guestmember_account_id);

		  $guestmember['current_city']    = $this->CI->member->getUserCurrentCity_cache_900_original($guestmember_account_id);

		  $offset = (intval($page) - 1) * intval($limit);

		  $stream = $this->CI->content_stream->getOwnWallPostsFinalVersion($guestmember['account_id'], $guestmember['user_id'], $limit, $offset)->result_array();


		  $contents = array();
		  $remove_post_url = base_url().'ajax/remove_post';

		  if(isset($_GET['skip']))
		  {
				var_dump($stream);
				exit();
		  }

		  $cm = $this->CI->load->module('ajax');



		  if(count($stream) > 0)
		  {

				$cant_delete_if_not_self_content = array(
					 'related_race' => true,
					 'related_scandal' => true,
					 'add_album_photos' => true,
					 'update_profile_picture' => true,
					 'related_news' => true,
				);

			foreach($stream as $key => $row)
			{
					 foreach($row as $k => $v)
					 {
						  $item[$k] = $v;
					 }




					 $content_date = mdate("%d %M %Y - %h:%i", strtotime($row['entry_date']));

					 //$item['count_page'] = '<div class="content_'.$row['content_id'].'"  style="display:inline; width:300px;">';
					 //$item['count_page'] .= $cm->count_content($row['content_id'], '1', '2', $member['account_id']);

					$item['count_page'] = '<div class="cid_'.$row['content_id'].'"  data-entrydate="'.$content_date.'" data-tipe="1" data-restype="2" style="font-family: sans-serif;font-size: 12px;width:300px;">';


					 $item['remove_content_link'] = '';

					 if($row['from_account_id'] == $member['account_id'])
					 {


						  if($row['to_account_id'] != $member['account_id'])
						  {
								$activity_status = $row['activity_status'];

								if(!isset($cant_delete_if_not_self_content[$activity_status]))
								{

								$item['remove_content_link'] .= '<div style="float:right; cursor:pointer; font-size:12px; display:inline" ><a class="makeright icon-close delete-content" id="delete_'.$row['content_id'].'" data-id="'.$row['content_id'].'" data-uri = "'.$remove_post_url.'" data-content_date="'.$content_date.'"  title="hapus"><i class="icon-remove half-transparent "></i></a></div>';
								}

								unset($activity_status);
						  } else {

								$item['remove_content_link'] .= '<div style="float:right; cursor:pointer; font-size:12px; display:inline" ><a class="makeright icon-close delete-content" id="delete_'.$row['content_id'].'" data-id="'.$row['content_id'].'" data-uri = "'.$remove_post_url.'" data-content_date="'.$content_date.'"  title="hapus"><i class="icon-remove half-transparent "></i></a></div>';
						  }
					 }

					 $item['count_page'] .= '</div>';

					 $item['content_date'] = $content_date;

					 unset($content_date);

					 switch($row['activity_status']){
						  case 'status_self':
								$item['activity_status_rename'] = 'Update status';
								break;
						  case 'status_to':
								$item['activity_status_rename'] = 'Mengirim wall';
								break;
						  case 'update_profile_picture':
								$item['activity_status_rename'] = 'Update photo profile';
								break;
						  case 'add_album_photos':
								$item['activity_status_rename'] = 'Menambah photo album';
								break;
						  case 'wall_photos_self':
								$item['activity_status_rename'] = 'Menambah wall photo';
								break;
						  case 'wall_photos_to':
								$item['activity_status_rename'] = 'Mengirim wall photo';
								break;
						  case 'related_news':
								$item['activity_status_rename'] = 'Terkait dengan berita';
								break;
						  case 'related_scandal':
								$item['activity_status_rename'] = 'Terkait dengan skandal';
								break;
						  case 'related_race':
								$item['activity_status_rename'] = 'Terkait dengan suksesi';
								break;
						  case 'add_blog':
								$item['activity_status_rename'] = 'Menulis blog';
								break;
						  default:
								$item['activity_status_rename'] = $row['activity_status'];
								break;
					 }



				if($row['activity_status'] == "related_news")
			{
				$textD = $this->CI->content_stream->getContentDetails_Text_cache_900_row($row['content_id']);
				if(count($textD) > 0)
				{
					$item['content_text']       = $textD->content_text;
					$item['news_id']       		= $textD->news_id;
					$item['content_id']       	= $textD->content_id;
					$item['content_date']       = $textD->content_date;
					$item['content_url']        = $textD->content_url;
					$item['content_source']     = $textD->content_source;
					$item['content_image_url']  = $textD->content_image_url;
				}

					 unset($textD);
			}

				if($row['content_type'] == "IMAGE"){

				$attachment = $this->CI->content_stream->getContentAttachment_cache_900_row($row['content_id']);
				if(count($attachment) > 0)
				{
					$item['attachment_path']  = $attachment->attachment_path;
					$item['attachment_title'] = $attachment->attachment_title;
					$item['attachment_type']  = $attachment->attachment_type;
				}

					 unset($attachment);
			}


				if($row['activity_status'] == "related_scandal")
			{
				$scandal        = $this->CI->Sentimen_model->getScandalDetailMini($row['location']);
				if(count($scandal) > 0)
				{
					$item['scandal_id']  = $scandal['scandal_id'];
					$item['title']       = $scandal['title'];
					$item['dampak']      = $scandal['uang'];
					$item['status']      = $scandal['status'];
					$item['short_desc']      = ($scandal['short_desc'] == '') ? $scandal['description'] : $scandal['short_desc'];
					$item['updated_date']= $scandal['updated_date'];
					$item['photo']       = $scandal['photo'];
					$item['scandal_url']       = base_url().'scandal/index/'.$scandal['scandal_id'].'-'.urltitle($scandal['title']);
				}

					 unset($scandal);
			}


				if($row['activity_status'] == "related_race")
			{
					$suksesi  = $this->CI->Suksesi_model->getSuksesiDetail($row['location']);
					$suksesi_photos   = $this->CI->Suksesi_model->getSuksesiPhoto($row['location']);

				if(count($suksesi) > 0)
				{
					$item['id_race']  = $suksesi['id_race'];
					$item['race_name']       = $suksesi['race_name'];
					$item['description']      = $suksesi['description'];
					$item['tgl_pelaksanaan']      = $suksesi['tgl_pelaksanaan'];
					$item['kdname']      = $suksesi['kdname'];
					$item['dname']      = $suksesi['dname'];
					$item['daftar_pemilih_tetap']= $suksesi['daftar_pemilih_tetap'];
					$item['status']       = $suksesi['status'];
					$item['periode']       = $suksesi['periode'];
					$item['status']       = $suksesi['status'];
					$item['photo']       = (count($suksesi_photos) > 0) ? $suksesi_photos[0]['attachment_title'] : '';
					$item['suksesi_url']       = base_url().'suksesi/index/'.$suksesi['id_race'].'-'.urltitle($suksesi['race_name']);
				}

					 unset($suksesi);
			}

				array_push($contents, $item);

					unset($item);
				}
		  }


		  $data['contents'] = $contents;
		  $data['guestmember'] = $guestmember;

		  $data['offset']   = $offset;
			$data['member'] = $member;
			$data['next_page'] = intval($page) + 1;

		  $isAFriend = $this->CI->member->isAFriend($member['account_id'],$guestmember['account_id']);
		  $data['isAFriend'] = $isAFriend;

		  $data['count_profile'] = $cm->count_profile($guestmember['user_id'], TRUE, TRUE);

		  return $data;
	 }

	 public function streamMyWall($page='1', $limit='10', $old = FALSE)
	 {

		  $this->CI->load->library('session');
		  $this->CI->load->model('member');
		  $this->CI->load->model('Sentimen_model');
		  $this->CI->load->model('Suksesi_model');
		  $this->CI->load->model('content_stream');

		  $offset = (intval($page) - 1) * intval($limit);

		  $member = $this->CI->session->userdata('member');


		$member['current_city']    = $this->CI->member->getUserCurrentCity_cache_900_original($member['account_id']);

		  $stream = ($old) ? $this->CI->content_stream->getOwnWallPostsFinalVersionOld($member['account_id'], $member['user_id'], $limit, $offset)->result_array() : $this->CI->content_stream->getOwnWallPostsFinalVersion($member['account_id'], $member['user_id'], $limit, $offset)->result_array();


		  $contents = array();
		  $remove_post_url = base_url().'ajax/remove_post';

		  if(isset($_GET['skip']))
		  {
                echo "<pre>";
				var_dump($stream);
                echo "</pre>";
				exit();
		  }

		  $cm = $this->CI->load->module('ajax');

		  $contents = $data = array();

		  if(count($stream) > 0)
		  {
				$cant_delete_if_not_self_content = array(
					 'related_race' => true,
					 'related_scandal' => true,
					 'add_album_photos' => true,
					 'update_profile_picture' => true,
					 'related_news' => true,
					 'add_group' => true,
				);


				foreach($stream as $key => $row)
				{
					 foreach($row as $k => $v)
					 {
					 $item[$k] = $v;
					 }

					 $content_date = mdate("%d %M %Y - %h:%i", strtotime($row['entry_date']));

					 //$item['count_page'] = '<div class="content_'.$row['content_id'].'"  style="display:inline; width:300px;">';
					 //$item['count_page'] .= $cm->count_content($row['content_id'], '1', '2', $member['account_id']);

					 $item['count_page'] = '<div class="cid_'.$row['content_id'].'"  data-entrydate="'.$content_date.'" data-tipe="1" data-restype="2" style="font-family: sans-serif;font-size: 12px;width:300px;">';

					 $item['remove_content_link'] = '';

					 if($row['from_account_id'] == $member['account_id'])
					 {


					 if($row['to_account_id'] != $member['account_id'])
					 {
						  $activity_status = $row['activity_status'];

						  if(!isset($cant_delete_if_not_self_content[$activity_status]))
						  {

						  $item['remove_content_link'] .= '<div style="float:right; cursor:pointer; font-size:12px; display:inline" ><a class="makeright icon-close delete-content" id="delete_'.$row['content_id'].'" data-id="'.$row['content_id'].'" data-uri = "'.$remove_post_url.'" data-content_date="'.$content_date.'"  title="hapus"><i class="icon-remove half-transparent "></i></a></div>';
						  }

								unset($activity_status);
						  } else {

								$item['remove_content_link'] .= '<div style="float:right; cursor:pointer; font-size:12px; display:inline" ><a class="makeright icon-close delete-content" id="delete_'.$row['content_id'].'" data-id="'.$row['content_id'].'" data-uri = "'.$remove_post_url.'" data-content_date="'.$content_date.'"  title="hapus"><i class="icon-remove half-transparent "></i></a></div>';
						  }
					 }

					 $item['count_page'] .= '</div>';

					 $item['content_date'] = $content_date;

					 unset($content_date);

					 switch($row['activity_status']){
					 case 'status_self':
						  $item['activity_status_rename'] = 'Update status';
						  break;
					 case 'status_to':
						  $item['activity_status_rename'] = 'Mengirim wall';
						  break;
					 case 'update_profile_picture':
						  $item['activity_status_rename'] = 'Update photo profile';
						  break;
					 case 'add_album_photos':
						  $item['activity_status_rename'] = 'Menambah photo album';
						  break;
					 case 'wall_photos_self':
						  $item['activity_status_rename'] = 'Menambah wall photo';
						  break;
					 case 'wall_photos_to':
						  $item['activity_status_rename'] = 'Mengirim wall photo';
						  break;
					 case 'related_news':
						  $item['activity_status_rename'] = 'Terkait dengan berita';
						  break;
					 case 'related_scandal':
						  $item['activity_status_rename'] = 'Terkait dengan skandal';
						  break;
					 case 'related_race':
						  $item['activity_status_rename'] = 'Terkait dengan suksesi';
						  break;
					 case 'add_blog':
						  $item['activity_status_rename'] = 'Menulis blog';
						  break;
					 case 'add_group':
						  $item['activity_status_rename'] = 'Membuat group';
						  break;						  
					 default:
						  $item['activity_status_rename'] = $row['activity_status'];
						  break;
					 }


					 if($row['activity_status'] == "related_news")
					 {
						  $textD = $this->CI->content_stream->getContentDetails_Text_cache_900_row($row['content_id']);
						  if(count($textD) > 0)
						  {
								$item['content_text']       = $textD->content_text;
								$item['news_id']       		= $textD->news_id;
								$item['content_id']       	= $textD->content_id;
								$item['content_date']       = $textD->content_date;
								$item['content_url']        = $textD->content_url;
								$item['content_source']     = $textD->content_source;
								$item['content_image_url']  = $textD->content_image_url;
						  }

						  unset($textD);

					 }

					 if($row['content_type'] == "IMAGE")
					 {
							$item['attachment_path']  = '';
							$item['attachment_title'] = '';
							$item['attachment_type']  = '';

						  $attachment = $this->CI->content_stream->getContentAttachment_cache_900_row($row['content_id']);

						  if(count($attachment) > 0)
						  {
								$item['attachment_path']  = $attachment->attachment_path;
								$item['attachment_title'] = $attachment->attachment_title;
								$item['attachment_type']  = $attachment->attachment_type;
						  }

						  unset($attachment);
					 }


					 if($row['activity_status'] == "related_scandal")
					 {
						  $scandal        = $this->Sentimen_model->getScandalDetailMini($row['location']);

						  if(count($scandal) > 0)
						  {
								$item['scandal_id']  = $scandal['scandal_id'];
								$item['title']       = $scandal['title'];
								$item['dampak']      = $scandal['uang'];
								$item['status']      = $scandal['status'];
								$item['short_desc']      = ($scandal['short_desc'] == '') ? $scandal['description'] : $scandal['short_desc'];
								$item['updated_date']= $scandal['updated_date'];
								$item['photo']       = $scandal['photo'];
								$item['scandal_url']       = base_url().'scandal/index/'.$scandal['scandal_id'].'-'.urltitle($scandal['title']);
						  }

						  unset($scandal);
					 }


					 if($row['activity_status'] == "related_race")
					 {
						  $suksesi  = $this->Suksesi_model->getSuksesiDetail($row['location']);
						  $suksesi_photos   = $this->Suksesi_model->getSuksesiPhoto($row['location']);

						  if(count($suksesi) > 0)
						  {
								$item['id_race']  = $suksesi['id_race'];
								$item['race_name']       = $suksesi['race_name'];
								$item['description']      = $suksesi['description'];
								$item['tgl_pelaksanaan']      = $suksesi['tgl_pelaksanaan'];
								$item['kdname']      = $suksesi['kdname'];
								$item['dname']      = $suksesi['dname'];
								$item['daftar_pemilih_tetap']= $suksesi['daftar_pemilih_tetap'];
								$item['status']       = $suksesi['status'];
								$item['periode']       = $suksesi['periode'];
								$item['status']       = $suksesi['status'];
								$item['photo']       = (count($suksesi_photos) > 0) ? $suksesi_photos[0]['attachment_title'] : '';
								$item['suksesi_url']       = base_url().'suksesi/index/'.$suksesi['id_race'].'-'.urltitle($suksesi['race_name']);
						  }

						  unset($suksesi);
					 }

				array_push($contents, $item);

				unset($item);
				}
		  }

		  $data['contents'] = $contents;
		  $data['old']   = $old;

		  $data['offset']   = $offset;
		$data['member'] = $member;
		$data['next_page'] = intval($page) + 1;

		  return $data;

	 }
}

/* End of file file.php */