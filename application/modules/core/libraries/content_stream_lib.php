<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_stream_lib {

    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->db = $this->CI->load->database('slave', true);
    }

    public function GetContentStreamByViewerPage($viewerPageID=null, $ContentID=null)
    {
        $contents = array();
        $contents = $data = array();

        $sql = "SELECT * FROM stream_content WHERE viewer_page_id = '".$viewerPageID."' AND content_id = '".$ContentID."' ";
        $query = $this->db->query($sql);
        $row = $query->row_array();

        $item = $row;

        $content_date = mdate("%d %M %Y - %h:%i", strtotime($row['entry_date']));
        $item['count_page'] = '<div class="cid_'.$row['content_id'].'"  data-entrydate="'.$content_date.'" data-tipe="1" data-restype="2" style="font-family: sans-serif;font-size: 12px;width:300px;">';
        $item['count_page'] .= '</div>';
        $item['content_date'] = $content_date;
        unset($content_date);
        $item['activity_status_rename'] = 'Menambah wall photo';

        if($row['content_type'] == "IMAGE")
        {
            $item['attachment_path']  = '';
            $item['attachment_title'] = '';
            $item['attachment_type']  = '';

            $attachment = $this->getContentAttachment($row['content_id']);

            if(count($attachment) > 0)
            {
                $item['attachment_path']  = $attachment['attachment_path'];
                $item['attachment_title'] = $attachment['attachment_title'];
                $item['attachment_type']  = $attachment['attachment_type'];
            }
            unset($attachment);
        }

        $data['contents'] = $item;

        return $data;
    }

    public function GetContentAttachment($contentID=null)
    {
        $sql = "SELECT * FROM tcontent_attachment WHERE content_id = '".$contentID."' LIMIT 1";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

}

?>