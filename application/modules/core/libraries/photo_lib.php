<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photo_Lib {

	private $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->db = $this->CI->load->database('slave', true);
	}

	public function getContentAttachment($id) {
		$this->db->where('content_id', $id);
		$query = $this->db->get('tcontent_attachment');
		return $query;
	}

	public function getProfilePicActive($account_id) {
		$sql = 'SELECT user_id FROM tusr_account WHERE account_id = ? LIMIT 1';
		$query = $this->db->query($sql, array($account_id));

		$page_id = '';
		if($query->num_rows() > 0) $page_id = $query->row()->user_id;

		$query = $this->db->query('SELECT `tcontent_attachment`.* FROM `tobject_page` LEFT JOIN `tcontent` ON (`tcontent`.`content_id` = `tobject_page`.`profile_content_id`) LEFT JOIN `tcontent_attachment` ON (`tcontent_attachment`.`content_id` = `tcontent`.`content_id`) WHERE `tobject_page`.`page_id` = ? AND `content_group_type` = 22', array($page_id));
		$rs = $query->result_array();
		if(count($rs) > 0) {
			return $rs[0];
		} else {
			return false;
		}
	}


}

?>