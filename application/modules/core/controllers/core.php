<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Core extends Application
{
	function __construct()
	{
		parent::__construct();
   }


	function index()
	{
		echo 'in core index';
   }

	function clear_cache($dir='')
	{
		$this->load->library('core/cache_lib');
		$this->cache_lib->clearCache($dir);

		echo 'cache cleared!!';
	}


   function populateStreamWall($page='0')
   {
   	$this->load->helper("url");

   	$limit 	= 5;
   	$offset 	= $page * $limit;
   	$next 	= (int)$limit + (int)$page;
   	$db 		= $this->load->database('slave', TRUE);

   	$sql 		= "Select * from tusr_account order by account_id limit ".$offset.",".$limit;
   	//$sql 		= "Select * from tusr_account where account_id = '17a9b5d3-0f03-483d-b139-fda1ed9faae2'";
   	$account = $db->query($sql)->result_array();
   	if (!empty($account)) {
   		foreach ($account as $row) {

		   	$sql = "Select count(stream_id) found_row from stream_wall where source_account_id = '".$row['account_id']."' ";
		   	$res = $db->query($sql)->row_array();

		   	if ($res['found_row']  == '0') {
		   		echo "<br><hr>POPUTATE <br>";

					$sp = "CALL sp_populate_stream_wall('".$row['account_id']."', '".$row['user_id']."', 0, 0)";
					$sp_res = $db->query($sp)->result_array();
					echo $sp ."<br>";

			   	$sql = "update stream_wall set source_account_id = '".$row['account_id']."' where source_account_id is null";
			   	$res = $db->query($sql);
					echo $sql."<br>";

					$sp = "CALL sp_populate_stream_viewer('".$row['account_id']."', '".$row['user_id']."', 0, 0)";
					$sp_res = $db->query($sp)->result_array();
					echo $sp ."<br>";

			   	$sql = "update stream_wall set source_account_id = '".$row['account_id']."' where source_account_id is null";
			   	$res = $db->query($sql);
					echo $sql."<br>";
	   		}
   		}
   	}

   	$url = base_url().'core/populateStreamWall/'.$next;
		die('<script>document.location="'.$url.'"</script>');
  	}


  	function testEmail($email='zola.octanoviar@gmail.com')
  	{
		$this->load->config();
		$mail_config = $this->config->load('mail'); ;

		$this->load->library('email', $mail_config);

		$this->email->set_newline("\r\n");
		$this->email->from('admin@bijaks.net', 'Bijak Admin');
		$this->email->to($email, 'email to');
		$this->email->subject('Core TestEmail Bijak Registrasi : Email Konfirmasi');

		$this->email->send();

		echo $this->email->print_debugger();
  	}
}