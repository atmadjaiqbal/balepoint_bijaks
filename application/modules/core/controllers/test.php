<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends Application
{
   protected $className;
   protected $cacheName;
   
	function __construct()
	{
		parent::__construct();
		
		$this->cacheName = 'smartcache_'. __CLASS__;      
	   $this->loadCache($this->cacheName);
		$this->setProfiler();
   }
	

	function index()
	{
	   
      $data = array_merge($this->data, array(
            'title'   => ' Berita'
      ));
      $y = $this->testCache_cache_120();
      
		$this->load->view('berita/berita_index', $data);
   }

   
	function cache()
	{
      
      $cacheKey   = __CLASS__.'::'.__METHOD__;
      $cacheData  = $this->getCache($cacheKey, $this->cacheName, $cacheData = $this->data);

      $data = array_merge($this->data, array(
            'title'   => ' Berita'
      ));

      echo "classname = " . $this->className;
      
		$this->load->view('berita_index', $data);
   }   
   
   function genPageWall()
   {
   	echo  'generate page wall';	
   	$db = $this->load->database('slave', TRUE); 
   	
  	}
}