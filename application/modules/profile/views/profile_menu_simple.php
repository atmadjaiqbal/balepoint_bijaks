        <?php
        foreach($submenu as $rwSubmenu)
        {
        ?>
            <script type="text/javascript">
                $(document).ready(function() {

                    $("#submenunext_<?php echo $rwSubmenu['id_structure_node']; ?>").hide();

                    $("#submenu_<?php echo $rwSubmenu['id_structure_node']; ?>").click(function() {

                        $("#submenunext_<?php echo $rwSubmenu['id_structure_node']; ?>").show();

                        var nodeID = <?php echo $rwSubmenu['id_structure_node']; ?>;
                        $.ajax({
                            url: '<?php echo base_url(); ?>profile/submenu?id=' +nodeID,
                            type: "GET",
                            success: function(data){
                                $('#submenunext_<?php echo $rwSubmenu['id_structure_node']; ?>').empty();
                                $('#submenunext_<?php echo $rwSubmenu['id_structure_node']; ?>').html('<span>'+data+'</span>');

                                console.log(data);
                            }
                        });
                    });

                    function submenuover(e)
                    {
                        $("#submenunext_<?php echo $rwSubmenu['id_structure_node']; ?>").show();

                        var nodeID = <?php echo $rwSubmenu['id_structure_node']; ?>;
                        $.ajax({
                            url: '<?php echo base_url(); ?>profile/submenu?id=' +nodeID,
                            type: "GET",
                            success: function(data){
                                $('#submenunext_<?php echo $rwSubmenu['id_structure_node']; ?>').empty();
                                $('#submenunext_<?php echo $rwSubmenu['id_structure_node']; ?>').html('<span>'+data+'</span>');

                                console.log(data);
                            }
                        });

                    };


                });
            </script>

        <li <?php echo ($rwSubmenu['node_count'] > 0) ? 'class="dropdown-submenu" data-toggle="dropdown-submenu"' : ''; ?>
             id="submenu_<?php echo $rwSubmenu['id_structure_node']; ?>">
            <a onmouseover="submenuover(this)" href="<?php echo ($rwSubmenu['node_count'] > 0) ? '#' : base_url().'profile/sublistprofile/'.$rwSubmenu['id_structure_node']; ?>?header=<?php echo $rwSubmenu['structure_name']; ?>">
                <?php echo $rwSubmenu['structure_name'];?>
            </a>
            <div id="submenunext_<?php echo $rwSubmenu['id_structure_node']; ?>"></div>
        </li>
        <?php
        }
        ?>
