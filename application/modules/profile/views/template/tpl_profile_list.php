<div style="font-family: helvetica; font-size: 20px; color: #000;margin-top:20px;">
    <?php echo $ProfileTitle; ?>
</div>
<div id='sub-profile'>
<?php
    foreach($rsProfile as $row)
    {
        if(!empty($row['profile_detail']))
        {
?>
            <div class="row-fluid">
                <div class="title"><?php echo $row['structure_name']; ?></div>
<?php
            foreach($row['profile_detail'] as $rwDetail)
            {
                $profileURL = base_url().'aktor/profile/'.$rwDetail['page_id'];
                if(!empty($rwDetail['page_name']))
                {
?>
                    <div id="politic-structure" class="span4">
                    <div class="profile">
                        <div class="profile-foto">
                            <a href="<?php echo $profileURL;?>">
                                <img alt="" title="<?php echo $rwDetail['page_name']; ?>"
                                     src='<?php echo $rwDetail['badge_url']; ?>' data-src="<?php echo $rwDetail['badge_url']; ?>"
                                     data-toggle='tooltip' class='tooltip-bottom' />
                            </a>
                        </div>
                        <div class="profile-detail">
                            <a href="<?php echo $profileURL;?>" title="<?php echo $rwDetail['page_name']; ?>">
                                <strong><?php echo (strlen($rwDetail['page_name']) > 28 ? substr($rwDetail['page_name'], 0, 28).' ...' : $rwDetail['page_name']); ?></strong>
                            </a>
                            <p><?php echo (strlen($row['structure_name']) > 28 ? substr($row['structure_name'], 0, 28).' ...' : $row['structure_name']); ?></p>
                            <p><span><?php
                                    if($rwDetail['icon_partai_url'] != 'None')
                                    {
                                        echo '<img src="'.$rwDetail['icon_partai_url'].'" width="25px" height="25px" ';
                                    } else {
                                        echo '';
                                    }
                                    ?></span> <?php
                                if($rwDetail['partai_name'] != 'None')
                                {
                                    if(strlen($rwDetail['partai_name']) > 25)
                                    {
                                        echo substr($rwDetail['partai_name'], 0, 25) . ' ...';
                                    } else {
                                        echo $rwDetail['partai_name'];
                                    }
                                }
                                ?></p>
                        </div>
                    </div>
                    </div>

                <?php

                }
            }
?>
            </div>
<?php
        }  // end if
    }
    ?>

</div>
