<style>
#navi{
    background-color: #822f12 !important;
    border-bottom: 2px none #000 !important;
    border-top: 2px none #000 !important;
    border-radius: 30px 30px 0 0;
    color: #ffffff !important;
    height: 40px !important;
    box-shadow: 4px 4px 4px #888888;
 }

#navi .navi-eksekutif a {
    color: #ffffff !important;
    /*width: 130px;*/
}

#navi .navi-eksekutif a:hover {
    background-color: #f8f4f4 !important;
    color: #a41c1c !important;
}


#navi .navi-legislatif a {
    color: #ffffff !important;
    /*width: 130px;*/
}

#navi .navi-legislatif a:hover {
    background-color: #f8f4f4 !important;
    color: #a41c1c !important;
}

#navi .navi-yudikatif a {
    color: #ffffff !important;
    /*width: 130px;*/
}

#navi .navi-yudikatif a:hover {
    background-color: #f8f4f4 !important;
    color: #a41c1c !important;
}

#navi .navi-partai a {
    color: #ffffff !important;
    /*width: 130px;*/
}

#navi .navi-partai a:hover {
    background-color: #f8f4f4 !important;
    color: #a41c1c !important;
}

#navi .navi-perusahaan a {
    color: #ffffff !important;
    /*width: 130px;*/
}

#navi .navi-perusahaan a:hover {
    background-color: #f8f4f4 !important;
    color: #a41c1c !important;
}

#navi .navi-player a {
    color: #ffffff !important;
    /*width: 130px;*/
}

#navi .navi-player a:hover {
    background-color: #f8f4f4 !important;
    color: #a41c1c !important;
}


</style>
<div class="container">
    <div class="sub-header-container">
        <ul id="navi">
            <li class=" navi-eksekutif dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>EKSEKUTIF</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $eksekutif;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-legislatif dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>LEGISLATIF</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $legislatif;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-yudikatif dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>YUDIKATIF</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $yudikatif;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-partai dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>PARTAI POLITIK</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $partai;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-perusahaan dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>PERUSAHAAN</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $perusahaan;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-player dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>POWER PLAYER</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $player;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

        </ul>

    </div>
</div>
