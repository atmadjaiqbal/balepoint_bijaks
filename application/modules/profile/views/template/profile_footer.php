<div class="sub-header-container" style="margin-bottom: 30px;">
    <img src="<?php echo base_url('assets/images/banner_indonesia_baik.png'); ?>">
</div>

<div class="container">
    <div class="sub-header-container" style="margin-bottom: 30px;">
        <div id="home-terkini">
            <div id="home-title-section" class="hp-label hp-label-hot-issues">
                <span class="hitam"><a href="<?php echo base_url(); ?>news">TERKINI</a></span>
                <span style="float: right;margin-right: 10px;"><a href="<?php echo base_url(); ?>news"><i class='icon-align-justify'></i></a></span>
            </div>
            <?php if (isset($latest_news) && !empty($latest_news)) :?>
                <?php
                $data['rsNews'] = $latest_news;
                $data['last_comunity'] = $last_comunity_news;
                $this->load->view('profile/news', $data);
                ?>
            <?php endif;?>
        </div>

        <div id="home-terkini" style="margin-left:30px;margin-right: 30px;">
            <div id="home-title-section" class="hp-label hp-label-pink">
                <span class="hitam"><a href="<?php echo base_url(); ?>news">RESES</a></span>
                <span style="float: right;margin-right: 10px;"><a href="<?php echo base_url(); ?>news"><i class='icon-align-justify'></i></a></span>
            </div>
            <?php if (isset($reses) && !empty($reses)) :?>
                <?php
                $data['rsNews'] = $reses;
                $data['last_comunity'] = $last_comunity_reses;
                $this->load->view('profile/news', $data);
                ?>
            <?php endif;?>
        </div>

        <div id="home-terkini">
            <div id="home-title-section" class="hp-label hp-label-biru">
                <span class="hitam"><a href="<?php echo base_url(); ?>news">HOT ISSUE</a></span>
                <span style="float: right;margin-right: 10px;"><a href="<?php echo base_url(); ?>news"><i class='icon-align-justify'></i></a></span>
            </div>
            <?php if (isset($hot) && !empty($hot)) :?>
                <?php
                $data['rsNews'] = $hot;
                $data['last_comunity'] = $last_comunity_hot;
                $this->load->view('profile/news', $data);
                ?>
            <?php endif;?>
        </div>

    </div>
    <br/>


</div>
<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">
                <div class="span4">
                    <div class="hp-place ">
                        <div class="hp-label hp-label-<?php echo category_color('ekonomi'); ?>">
                            <h4>EKONOMI</h4>
                        </div>
                        <div class="hp-place-ekonomi">
                            <?php
                            if (isset($ekonomi) && !empty($ekonomi))
                            {
                                $data['rsNews'] = $ekonomi;
                                $this->load->view('profile/news_toc', $data);
                            }
                            ?>
                        </div>
                        <br>
                        <div class="hp-footer last-coklat"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="hp-place ">
                        <div class="hp-label hp-label-<?php echo category_color('hukum'); ?>">
                            <h4>HUKUM</h4>
                        </div>
                        <div class="hp-place-hukum">
                            <?php
                            if (isset($hukum) && !empty($hukum))
                            {
                                $data['rsNews'] = $hukum;
                                $this->load->view('profile/news_toc', $data);
                            }
                            ?>
                        </div>
                        <br>
                        <div class="hp-footer last-coklat"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="hp-place ">
                        <div class="hp-label hp-label-<?php echo category_color('parlemen'); ?>">
                            <h4>PARLEMEN</h4>
                        </div>
                        <div class="hp-place-parlemen">
                            <?php
                            if (isset($parlemen) && !empty($parlemen))
                            {
                                $data['rsNews'] = $parlemen;
                                $this->load->view('profile/news_toc', $data);
                            }
                            ?>
                        </div>
                        <br>
                        <div class="hp-footer last-coklat"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">
                <div class="span4">
                    <div class="hp-place ">
                        <div class="hp-label hp-label-<?php echo category_color('nasional'); ?>">
                            <h4>NASIONAL</h4>
                        </div>
                        <div class="hp-place-nasional">
                            <?php
                            if (isset($nasional) && !empty($nasional))
                            {
                                $data['rsNews'] = $nasional;
                                $this->load->view('profile/news_toc', $data);
                            }
                            ?>
                        </div>
                        <br>
                        <div class="hp-footer last-hitam"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="hp-place ">
                        <div class="hp-label hp-label-<?php echo category_color('daerah'); ?>">
                            <h4>DAERAH</h4>
                        </div>
                        <div class="hp-place-daerah">
                            <?php
                            if (isset($daerah) && !empty($daerah))
                            {
                                $data['rsNews'] = $daerah;
                                $this->load->view('profile/news_toc', $data);
                            }
                            ?>
                        </div>
                        <br>
                        <div class="hp-footer last-hitam"></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="hp-place ">
                        <div class="hp-label hp-label-<?php echo category_color('internasional'); ?>">
                            <h4>INTERNASIONAL</h4>
                        </div>
                        <div class="hp-place-internasional">
                            <?php
                            if (isset($internasional) && !empty($internasional))
                            {
                                $data['rsNews'] = $internasional;
                                $this->load->view('profile/news_toc', $data);
                            }
                            ?>
                        </div>
                        <br>
                        <div class="hp-footer last-hitam"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <br/>
</div>
