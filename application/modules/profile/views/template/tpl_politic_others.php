<div style="font-family: helvetica; font-size: 20px; color: #000;margin-top:20px;">
    <?php echo $ProfileTitle; ?>
</div>
<div id='sub-profile'>
<?php
$_k = 0; $_p = 0;

$_count_data = isset($rsProfile['count_total']) ? $rsProfile['count_total'] : 0;
$_brs = ceil($_count_data/3);

if($_count_data > 10)
{
  $_col1_a = 0; $_col1_b = $_brs;
  $_col2_a = $_brs + 1; $_col2_b = $_brs * 2;
  $_col3_a = $_col2_b + 1; $_col3_b = $_count_data;
} else {
  $_col1_a = 0; $_col1_b = $_count_data;
  $_col2_a = 10 + 1; $_col2_b = 10 * 2;
  $_col3_a = $_col2_b + 1; $_col3_b = 30;
}

foreach($rsProfile as $row)
{
    if(!empty($row['profile_detail']))
    {
?>
      <div id="politic-structure" style="margin-left:10px;margin-right:10px;">
        <div class="title"><?php echo $row['structure_name']; ?> </div>
<?php
       foreach($row['profile_detail'] as $rwDetail)
       {
          $profileURL = base_url().'profile/detailprofile/'.$rwDetail['page_id'];

           if($_k == $_col1_a || $_k == $_col2_a || $_k == $_col3_a )
           {
               if($_k == $_col2_a || $_k == $_col3_a)
               {
                   $_style_col = 'style="margin-left:30px;"';
               } else {
                   $_style_col = '';
               }
               ?>
<?php
           }

          if(!empty($rwDetail['page_name']))
          {
?>
             <div class="profile">
                <div class="profile-foto">
                   <a href="<?php echo $profileURL;?>">
                      <img alt="" title="<?php echo $rwDetail['page_name']; ?>"
                           src='<?php echo $rwDetail['badge_url']; ?>' data-src="<?php echo $rwDetail['badge_url']; ?>"
                           data-toggle='tooltip' class='tooltip-bottom' />
                   </a>
                </div>
                <div class="profile-detail">
                   <a href="<?php echo $profileURL;?>" title="<?php echo $rwDetail['page_name']; ?>">
                      <strong><?php echo (strlen($rwDetail['page_name']) > 28 ? substr($rwDetail['page_name'], 0, 28).' ...' : $rwDetail['page_name']); ?></strong>
                   </a>
                   <p><?php echo (strlen($row['structure_name']) > 28 ? substr($row['structure_name'], 0, 28).' ...' : $row['structure_name']); ?></p>
                   <p><span><?php
                      if($rwDetail['icon_partai_url'] != 'None')
                      {
                         echo '<img src="'.$rwDetail['icon_partai_url'].'" width="25px" height="25px" ';
                      } else {
                         echo '';
                      }
                   ?></span> <?php
                      if($rwDetail['partai_name'] != 'None')
                      {
                         if(strlen($rwDetail['partai_name']) > 25)
                         {
                           echo substr($rwDetail['partai_name'], 0, 25) . ' ...';
                         } else {
                           echo $rwDetail['partai_name'];
                         }
                      }
                   ?></p>
                </div>
             </div>
<?php

         }


           if($_k == $_col1_b || $_k == $_col2_b || $_k == $_col3_b)
           {
?>

<?php
           }


         $_k++;

       }

    }  // end if
?>
   </div>
<?php
   $_p++;
}
?>

</div>
