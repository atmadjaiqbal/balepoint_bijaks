<div class="container">
    <div class="sub-header-container">
        <ul id="navi">
            <li class=" navi-eksekutif dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>EKSEKUTIF</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $eksekutif;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-legislatif dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>LEGISLATIF</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $legislatif;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-yudikatif dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>YUDIKATIF</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $yudikatif;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-partai dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>PARTAI POLITIK</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $partai;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-perusahaan dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>PERUSAHAAN</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $perusahaan;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

            <li class=" navi-player dropdown">
                <a title="" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>POWER PLAYER</strong></span></a>
                <ul class="dropdown-menu">
                    <?php
                    $data['struktur'] = $player;
                    $this->load->view('profile/profile_menu_strukture', $data);
                    ?>
                </ul>
            </li>

        </ul>

    </div>
</div>
