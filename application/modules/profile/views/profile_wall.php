<div class="container">
    <?php $this->load->view('template/tpl_header_profile'); ?>
</div>
<?php
$wall_title = ($user['user_id'] == $this->member['user_id']) ? 'My Wall' : 'Wall '.character_limiter(ucwords($user['display_name']), 20);
?>
<div class="container" id="profile-detail" style="margin-top:-130px;">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php echo $this->load->view('template/profile/tpl_profile_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span8"><h3><?=$wall_title;?></h3></div>
                    <!--                    <div class="span4 text-right">-->
                    <!--                        <a class="btn-flat btn-flat-large btn-flat-dark">FRIEND</a>-->
                    <!--                        <a class="btn-flat btn-flat-large btn-flat-dark">FOLLOW</a>-->
                    <!--                    </div>-->
                </div>

                <!-- FORM POST -->
                <div class="row-fluid komu-wall-post">
                    <div class="span12 komu-wall-tab">
                        <ul class="komu-wall-nav" id="komunitasTab">
                            <li class="active"><a href="#status" ><div class="komu-btn-icon-light komu-btn-icon-light-status"></div></a></li>
                            <li><a href="#photo"><div class="komu-btn-icon-light komu-btn-icon-light-photo"></div></a></li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active" id="status">
                                <div class="row-fluid">
                                    <input data-user="<?=$user['user_id'];?>" data-id='<?=$user['account_id'];?>' id="val" class="input-flat input-flat-large" type="text" placeholder="Apa pendapat anda tentang politisi ini ?" name='status'>

                                </div>
                                <div class="row-fluid">
                                    <div class="span12 text-right">
                                        <a id="send_status" class="btn-flat btn-flat-dark">kirim</a>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="photo">
                                <div class="row-fluid">
                                    <form id="addwallphotoform" name="addwallphotoform" method="post" enctype="multipart/form-data" action="<?php echo base_url().'timeline/post_photo';?>" target="wallphotouploadiframe">
                                        <div class="textarea_holder">
                                            <input name="user" type="hidden" value ="<?php echo $user['user_id'];?>" />
                                            <input name="id" type="hidden" value ="<?php echo $user['account_id'];?>" />
                                            <input class="input-flat input-flat-large" type="text" placeholder="photo's title..." name="title" value=""><br>
                                            <textarea class="input-flat input-flat-large" id="description" name="description" autocomplete="off" cols="30" rows="3" placeholder="Say something about this..."></textarea>
                                            <input type="file" name="userfile" id="userfile">
                                            <div class="pull-right">
                                                <input type="submit" class="btn-flat btn-flat-dark" id="wallphotobtnpost" value="Kirim">

                                            </div>
                                        </div>
                                    </form>
                                    <iframe name="wallphotouploadiframe" id="wallphotouploadiframe" style="display:none;"></iframe>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <br>
                <div class="div-line-black"></div>
                <br>
                <div class="komu-wall-container" id="wall_list" data-id="<?=$user['account_id'];?>" data-user="<?=$user['user_id'];?>" data-page="1" >
                    <?php
                    //$this->load->view('template/komunitas/tpl_komunitas_wall');
                    ?>
                </div>
                <br>
                <div class="row-fluid komu-wall-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a id="wall_load_more" class="komu-wall-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<br>