<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js"></script><style>

<?php /*
    #politik li img {
        border-radius: 10px 10px 10px 10px;
        margin-top: 3px;
        margin-left: 3px;
    }

    #politik li {
        cursor: pointer;
        float: left;
        margin: 0;
        vertical-align: top;
    }
    #politik li img {
        width: 378px;
        height: 378px;
    }
    #politik .small-politik {
        cursor: pointer;
        float: left;
        margin: 0;
        vertical-align: top;
    }
    #politik .small-politik img {
        width: 185px;
        height: 185px;
    }
    #politik .very-small-politik {
        cursor: pointer;
        float: left;
        margin: 0;
        vertical-align: top;
    }
    #politik .very-small-politik img {
        height: 112px;
        width: 112px;
    }
    #politik .terkait-small-politik {
        cursor: pointer;
        float: left;
        margin: 0;
        vertical-align: top;
    }
    #politik .terkait-small-politik img {
        height: 76px;
        width: 76px;
    }
    #politik .small-partai {
        cursor: pointer;
        float: left;
        margin: 0;
        vertical-align: top;
    }
    #politik .small-partai img {
        height: 88px;
        padding: 3px !important;
        width: 88px;
    }
    #politik .small-lembaga {
        cursor: pointer;
        float: left;
        margin: 0;
        vertical-align: top;
    }
    #politik .small-lembaga img {
        height: 88px;
        padding: 3px !important;
        width: 88px;
    }
*/?>
    #PPI_title{
        margin-left: 0;
        margin-top: 10px;
        margin-bottom: 10px;
        font-weight: bold;
        color: #822f12;
        font-size: 33px;
    }

    .politic_item{
        /*width: 185px;*/
        /*height: 185px;*/
        /*border: solid 1px red;*/
        margin-top: 5px;
    }

    .politic_item img{
        width: 185px;
        height: 185px;
        border-radius: 10px 10px 10px 10px !important;

    }

    .big_img{
        width: 378px !important;
        height: 378px !important;
    }

    .caption_big{
        position: absolute;
        background: rgba(164,127,36,0.8);
        margin-left: auto;
        margin-right: auto;
        margin-top: -50px;
        z-index: 99;
        width: 100%;
        height: 50px;
        border-radius: 0 0 10px 10px;
        text-align: center;
    }

    .caption_text{
        padding-top: 3px;
        font-weight: normal;
        font-size: 17px;
        color: #ffffff;

    }

    .caption_posisi{
        font-size: 15px;
        color: #ffffff;
    }

</style>
<div class="container">
    <?php $this->load->view('template/aktor/tpl_sub_header2',$hot_profile); ?>
</div>
<br>
<?php echo $profileheader; ?>
<br/>
<?php
?>
<div class="container">
    <div class="sub-header-container" style="margin-bottom: 30px;">
        <div id='politik_container'>
            <?php
            $i = 0;
            foreach($hot_profile as $row){
                $targetUrl = base_url('aktor/profile/'.$row['page_id']);
                $imgSrc = $row['badge_url'];
                $pageId = $row['page_id'];
                $pageName = $row['page_name'];
                $posisi = explode(',',$row['posisi']);

//                if($i >= 0 && $i <= 9) {
                if(in_array($pageId,$bigProfiles)){
                    ?>
                    <div class="politic_item">
                        <a href="<?php echo $targetUrl; ?>">
                            <img src="<?php echo $imgSrc; ?>" data-toggle="tooltip" data-original-title="<?php echo $pageName; ?>" class="tooltip-bottom big_img"/ >
                            <div class="caption_big">
                                <div class="caption_text">
                                    <?php echo $pageName; ?>
                                </div>
                                <div class="caption_posisi">
                                    <?php echo $posisi[0]; ?>
                                </div>
                            </div>
                        </a>

                    </div>

                <?php
                }else{
//                    if($i >= 10 && $i <= 50){
                ?>
                        <div class="politic_item">
                            <a href="<?php echo $targetUrl; ?>">
                                <img src="<?php echo $imgSrc; ?>" data-toggle="tooltip" data-original-title="<?php echo $pageName; ?>" class="tooltip-bottom"/ >
                                <div class="caption_big">
                                    <div class="caption_text">
                                        <?php echo $pageName; ?>
                                    </div>
                                </div>
                            </a>
                        </div>

                <?php
//                    }
                }
                $i++;
            }
            ?>

        </div>
    </div>
</div>

<?php /*
<div class="container">
 <div class="sub-header-container" style="margin-bottom: 30px;">
    <div id='politik'>
    <ul>
    <?php
      $i = 0;
      foreach ($hot_profile as $row){
          if($i >= 0 && $i <= 9){
    ?>
        <li>
          <a href="<?php echo base_url().'aktor/profile/'.$row['page_id']; ?>" data-id="<?php echo $row['page_id']; ?>">
            <img src='<?php echo $row['badge_url'] ; ?>'
                 data-src='<?php echo $row['badge_url'] ; ?>'
                 data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $row['page_name']; ?>"
                 alt=''/>
          </a>
        </li>
    <?php
          } else {

            if($i >= 10 && $i <= 50)
            {
    ?>
              <li class="small-politik">
                  <a href="<?php echo base_url().'aktor/profile/'.$row['page_id']; ?>" data-id="<?php echo $row['page_id']; ?>">
                      <img src='<?php echo $row['badge_url'] ; ?>'
                           data-src='<?php echo $row['badge_url'] ; ?>'
                           data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $row['page_name']; ?>"
                           alt=''/>
                  </a>
              </li>
    <?php
            }
          }

          $i++;
       }
    ?>
    </ul>
  </div>
 </div>
</div>
*/?>





<br>

<?php
if($hot_partai){
?>
<div class="container">
    <div id="PPI_title">PARTAI POLITIK DAN INSTITUSI</div>

    <div class="sub-header-container" style="margin-bottom: 30px;">
        <!--div class="home-title-section hp-label hp-label-hot-profile" style="background-color: #FF7E00;">
            <span class="hitam"></span>
        </div-->
        <div id='politik'>
            <ul>
                <?php
                $y = 0;
                foreach ($hot_partai as $rwPartai)
                {
                        ?>
                        <li class="small-partai">
                            <a href="<?php echo base_url().'aktor/profile/'.$rwPartai['page_id']; ?>" data-id="<?php echo $rwPartai['page_id']; ?>">
                                <img class="img-polaroid lazy" src='<?php echo $rwPartai['badge_url'] ; ?>'
                                     data-src='<?php echo $rwPartai['badge_url'] ; ?>'
                                     data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $rwPartai['page_name']; ?>"
                                     alt=''/>
                            </a>
                        </li>
                    <?php
                    $y++;
                }
                ?>
            </ul>
        </div>
    </div>
</div>
<br/>
<?php
}
?>

<?php
if($hot_lembaga)
{
?>
    <div class="container">
        <div class="sub-header-container" style="margin-bottom: 30px;">
            <!--div class="home-title-section hp-label hp-label-hot-profile" style="background-color: #FF7E00;">
                <span class="hitam"></span>
            </div-->
            <div id='politik'>
                <ul>
                    <?php
                    $y = 0;
                    foreach ($hot_lembaga as $rwLem)
                    {
                        ?>
                        <li class="small-lembaga">
                            <a href="<?php echo base_url().'aktor/profile/'.$rwLem['page_id']; ?>" data-id="<?php echo $rwLem['page_id']; ?>">
                                <img class="img-polaroid lazy" src='<?php echo $rwLem['badge_url'] ; ?>'
                                     data-src='<?php echo $rwLem['badge_url'] ; ?>'
                                     data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $rwLem['page_name']; ?>"
                                     alt=''/>
                            </a>
                        </li>
                        <?php
                        $y++;
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <br/>
<?php
}
?>
<br>
<script>
    var msnry = new Masonry( '#politik_container', {
        // options
        "columnWidth": 70,
        "itemSelector": '.politic_item',
        "gutter": 5,
        "isFitWidth": true,
        "isOriginLeft": false,
        "isOriginTop": true,





    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>