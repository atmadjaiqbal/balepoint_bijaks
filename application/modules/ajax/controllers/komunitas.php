<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Komunitas extends Application
{
   public $response;
   
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');  
		$this->load->helper('date');  
		$this->load->helper('m_date');  
		$this->load->helper('bijak');  
        
		$this->load->library('session');
		$this->load->library('core/bijak');
	}
	
	function index()
	{
      echo "Invalid Request";
	}
	
	function sidebar()
	{
		$this->load->library('core/cache_lib');

		echo $this->cache_lib->getSidebarKomunitasPolitisi();
	}

}