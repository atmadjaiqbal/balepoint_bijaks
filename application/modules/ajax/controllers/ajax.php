<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends Application
{
    protected  $CI;
	public $member;
	public $response;

	public function __construct()
	{
		parent::__construct();
        $this->CI =& get_instance();
        $this->CI->load->database('slave');

        $this->load->helper('url');
		$this->load->helper('imagefull');
		$this->load->helper('date');
		$this->load->helper('m_date');
		$this->load->helper('bijak');

		$this->load->library('session');
		$this->load->library('core/bijak');

		$this->member     = $this->session->userdata('member');
		$this->response   = array('code' => 'FAILED',  'message' => 'Failed! Unable to process request');
	}


	function index()
	{
		echo "Invalid Request";
	}

    public function GetLastSocialInfo($where=null, $groupType=12)
    {
        $sql = <<<QUERY
        SELECT * FROM (
        (
           SELECT tc.`account_id` AS 'account_id', tc.`content_id` AS 'content_id', ta.display_name AS 'display_name',
                  ta.user_id AS 'user_id', tt.news_id AS 'news_id', tp.`profile_content_id` AS 'profile_content_id',
                  tac.attachment_title AS 'attachment_title', t.title AS 'title', t.content_group_type AS 'content_group_type',
                  tht.topic_id AS 'topic_id', tc.`entry_date` AS 'entry_date', tt.content_url AS 'content_url',
                  tc.comment_id AS 'comment_id', tc.`text_comment` AS 'comment_text', '1' AS 'comunity_type'
           FROM tcontent_comment tc
           INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
           INNER JOIN `tusr_account` ta ON ta.`account_id`=tc.`account_id`
           INNER JOIN `tobject_page` tp ON tp.page_id = ta.`user_id`
           LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
           INNER JOIN tcontent t ON t.`content_id`=tc.`content_id`
           INNER JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
           WHERE t.`content_group_type` = {$groupType} AND tc.`status` = 0
           ORDER BY entry_date DESC LIMIT 5
        )

        UNION

        (
           SELECT tc.`account_id` AS 'account_id', tc.`content_id` AS 'content_id', ta.display_name AS 'display_name',
                  ta.user_id AS 'user_id', tt.news_id AS 'news_id', tp.`profile_content_id` AS 'profile_content_id',
                  tac.attachment_title AS 'attachment_title', t.title AS 'title', t.content_group_type AS 'content_group_type',
                  tht.topic_id AS 'topic_id', tc.`entry_date` AS 'entry_date', tt.content_url AS 'content_url',
                  'comment_id', 'comment_text', '2' AS 'comunity_type'
           FROM tcontent_like tc
           INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
           INNER JOIN `tusr_account` ta ON ta.`account_id`=tc.`account_id`
           INNER JOIN `tobject_page` tp ON tp.page_id = ta.`user_id`
           LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
           INNER JOIN tcontent t ON t.`content_id`=tc.`content_id`
           INNER JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
           WHERE t.`content_group_type` = {$groupType}
           ORDER BY entry_date DESC LIMIT 5
        )

        UNION

        (
           SELECT tc.`account_id` AS 'account_id', tc.`content_id` AS 'content_id', ta.display_name AS 'display_name',
                  ta.user_id AS 'user_id', tt.news_id AS 'news_id', tp.`profile_content_id` AS 'profile_content_id',
                  tac.attachment_title AS 'attachment_title', t.title AS 'title', t.content_group_type AS 'content_group_type',
                  tht.topic_id AS 'topic_id', tc.`entry_date` AS 'entry_date', tt.content_url AS 'content_url',
                  'comment_id', 'comment_text', '3' AS 'comunity_type'
           FROM tcontent_dislike tc
           INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
           INNER JOIN `tusr_account` ta ON ta.`account_id`= tc.`account_id`
           INNER JOIN `tobject_page` tp ON tp.page_id = ta.`user_id`
           LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
           INNER JOIN tcontent t ON t.`content_id`=tc.`content_id`
           INNER JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
           WHERE t.`content_group_type` = {$groupType}
           ORDER BY entry_date DESC LIMIT 5
        )
        ) AS comunity_update
        {$where}
        ORDER BY entry_date DESC LIMIT 1;
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function GetLastSocialInfoByTopicID($topicID)
    {
        $where = "WHERE topic_id = '".$topicID."' ";
        return $this->GetLastSocialInfo($where);
    }

    public function GetLastSocialInfoByContentID($contentID, $groupType=12)
    {
        $where = "WHERE content_id = '".$contentID."' ";
        return $this->GetLastSocialInfo($where, $groupType);
    }

    public function CountAllComment()
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'total_comment' FROM tcontent_comment;
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function CountAllLike()
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'total_like' FROM tcontent_like
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function CountAllUser()
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'total_user' FROM tusr_account
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function CountAllScandal()
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'total_scandal' FROM `tscandal`
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function CountAllSuksesi()
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'total_suksesi' FROM `trace`
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function CountAllTokoh()
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'total_tokoh' FROM v_usr_politisi;
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function GetComment($where=null)
    {
        $sql = <<<QUERY
        SELECT tc.`account_id` AS 'account_id', tc.`content_id` AS 'content_id', ta.display_name AS 'display_name',
               ta.user_id AS 'user_id', tt.news_id AS 'news_id', tp.`profile_content_id` AS 'profile_content_id',
               tac.attachment_title AS 'attachment_title', t.title AS 'title', t.content_group_type AS 'content_group_type',
               tht.topic_id AS 'topic_id', tc.`entry_date` AS 'entry_date', tt.content_url AS 'content_url',
               tc.comment_id AS 'comment_id', tc.`text_comment` AS 'comment_text', '1' AS 'comunity_type'
        FROM tcontent_comment tc
        INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
        INNER JOIN `tusr_account` ta ON ta.`account_id`=tc.`account_id`
        INNER JOIN `tobject_page` tp ON tp.page_id = ta.`user_id`
        LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
        INNER JOIN tcontent t ON t.`content_id`=tc.`content_id`
        INNER JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
		{$where}
        ORDER BY entry_date DESC LIMIT 1
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function GetCommentByContentGroupType($groupType)
    {
        $where = "WHERE t.`content_group_type` = ".$groupType." AND tc.`status` = 0";
        return $this->GetComment($where);
    }

    public function GetCommentByTopicId($topicid)
    {
        $where = "WHERE tht.topic_id = ".$topicid;
        return $this->GetComment($where);
    }

    public function GetLike($where=null)
    {
        $sql = <<<QUERY
        SELECT tc.`account_id` AS 'account_id', tc.`content_id` AS 'content_id', ta.display_name AS 'display_name',
               ta.user_id AS 'user_id', tt.news_id AS 'news_id', tp.`profile_content_id` AS 'profile_content_id',
               tac.attachment_title AS 'attachment_title', t.title AS 'title', t.content_group_type AS 'content_group_type',
               tht.topic_id AS 'topic_id', tc.`entry_date` AS 'entry_date', tt.content_url AS 'content_url',
               'comment_id', 'comment_text', '2' AS 'comunity_type'
        FROM tcontent_like tc
        INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
        INNER JOIN `tusr_account` ta ON ta.`account_id`=tc.`account_id`
        INNER JOIN `tobject_page` tp ON tp.page_id = ta.`user_id`
        LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
        INNER JOIN tcontent t ON t.`content_id`=tc.`content_id`
        INNER JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
        {$where}
        ORDER BY entry_date DESC LIMIT 1
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function GetLikeByContentGroupType($groupType)
    {
        $where = "WHERE t.`content_group_type` = ".$groupType;
        return $this->GetLike($where);
    }

    public function GetDislike($where=null)
    {
        $sql = <<<QUERY
        SELECT tc.`account_id` AS 'account_id', tc.`content_id` AS 'content_id', ta.display_name AS 'display_name',
               ta.user_id AS 'user_id', tt.news_id AS 'news_id', tp.`profile_content_id` AS 'profile_content_id',
               tac.attachment_title AS 'attachment_title', t.title AS 'title', t.content_group_type AS 'content_group_type',
               tht.topic_id AS 'topic_id', tc.`entry_date` AS 'entry_date', tt.content_url AS 'content_url',
               'comment_id', 'comment_text', '3' AS 'comunity_type'
        FROM tcontent_dislike tc
        INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
        INNER JOIN `tusr_account` ta ON ta.`account_id`= tc.`account_id`
        INNER JOIN `tobject_page` tp ON tp.page_id = ta.`user_id`
        LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
        INNER JOIN tcontent t ON t.`content_id`=tc.`content_id`
        INNER JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
        {$where}
        ORDER BY entry_date DESC LIMIT 1
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->row_array();
    }

    public function GetDisikeByContentGroupType($groupType)
    {
        $where = "WHERE t.`content_group_type` = ".$groupType;
        return $this->GetDislike($where);
    }

	public function count_content_result($var_contentID = NULL)
	{
		$bijak_lib = $this->load->library('core/bijak');
		$contentID = ($this->input->post('contentid')) ? $this->input->post('contentid') : $var_contentID;
		$accountID = ($this->member) ? $this->member['account_id'] : '';
		$result    = $bijak_lib->getContentCountMultiple($accountID, $contentID);

		$this->output->set_content_type('application/json');

		if(count($result) > 0)
		{
			$this->output->set_output(json_encode(array('msg' => 'ok', 'data' => $result)));
		} else {
			$this->output->set_output(json_encode(array('msg' => 'empty', 'data' => array())));
		}
	}


	public function count_content_action($var_contentID = NULL)
	{
		$bijak_lib 	 = $this->load->library('core/bijak');
		$contentID   = ($this->input->post('content_id')) ? $this->input->post('content_id') : $var_contentID;
		$accountID   = ($this->member) ? $this->member['account_id'] : '';
		$result      = $bijak_lib->getContentCountAction($accountID, $contentID);

		$this->output->set_content_type('application/json');

		if(count($result) > 0)
		{
			$this->output->set_output(json_encode(array('msg' => 'ok', 'data' => $result)));
		} else {
			$this->output->set_output(json_encode(array('msg' => 'empty', 'data' => array())));
		}
	}


	public function count_content($var_contentID = NULL, $var_tipe = NULL, $var_res_type = NULL, $var_accountID = NULL , $var_content_date = NULL )
	{
		if($this->input->post('id') || $this->input->post('tipe') || $this->input->post('res_type') || $this->input->post('content_date'))
		{
			$this->count_content_post();

			return;
		}

		$result['tipe']         = $var_tipe;
		$result['restype']      = $var_res_type;
		$result['content_id']   = $var_contentID;
		$datestring             = "%d %M %Y - %h:%i";
		$result['content_date'] = mdate($datestring, strtotime($var_content_date));

		if(intval($var_res_type) == 1){
			$this->load->view('ajax/count_content_rev', $result);
		} else {
			return $this->load->view('ajax/count_content_rev', $result, true);
		}
	}

	public function count_content_post($var_contentID = NULL, $var_tipe = NULL, $var_res_type = NULL, $var_accountID = NULL , $var_content_date = NULL )
	{
		$bijak_lib   = $this->load->library('core/bijak');
		$restype     = ($this->input->post('res_type')) ? $this->input->post('res_type') : $var_res_type;
		$tipe        = ($this->input->post('tipe')) ? $this->input->post('tipe') : $var_tipe;
		$contentID   = ($this->input->post('id')) ? $this->input->post('id') : $var_contentID;
		$contentDate = ($this->input->post('content_date')) ? $this->input->post('content_date') : $var_content_date;

		if($var_accountID != NULL || $var_accountID != '') {
			$accountID  = $var_accountID;
		} else {
			$accountID  = ($this->member) ? $this->member['account_id'] : '';
		}

		$result               = $bijak_lib->getContentCount($accountID, $contentID);
		$rand                 = '';
		$result['rand']       = $rand;
		$result['account_id'] = $accountID;
		$result['tipe']       = $tipe;
		$result['content_id'] = $contentID;
		$result['islike']     = '';
		$result['isdislike']  = '';
		$result['is_like']    = (isset($result['is_like'])) ? $result['is_like'] : '0';
		$result['is_dislike'] = (isset($result['is_dislike'])) ? $result['is_dislike'] : '0';

		if(!empty($accountID)){
			$result['like_uri']    = ($result['is_like'] == '0') ? base_url('ajax/like') : base_url('ajax/unlike') ;
			$result['dislike_uri'] = ($result['is_dislike'] == '0') ? base_url('ajax/dislike') : base_url('ajax/undislike');
		}else{
			$result['like_uri']    = '';
			$result['dislike_uri'] = '';
		}

		$result['callback'] = base_url('ajax/count_content');
		$result['tipe']     = $tipe;
		$result['restype']  = $restype;

		if (!$contentDate) {
			$dt = array();
			if($tipe == 1){
				$select = 'entry_date as content_date';
				$where  = array('content_id' => $contentID);
				$dt     = $bijak_lib->getContent($select, $where)->row_array();
			}else{
				$select = 'content_date';
				$where  = array('content_id' => $contentID);
				$dt     = $bijak_lib->getContentText($select, $where)->row_array();
			}
		}

		$datestring = "%d %M %Y - %h:%i";

		if($contentDate != NULL)
		{
			$result['content_date'] = $contentDate;
		} else {

			$result['content_date'] = (!empty($dt)) ? mdate($datestring, strtotime($dt['content_date'])) : '';
		}

		if(intval($restype) == 1 || $this->input->post('res_type')){
			$this->load->view('ajax/count_content', $result);
		} else {
			return $this->load->view('ajax/count_content', $result, true);
		}
	}

	public function count_page($id='', $res_type=2)
	{
		if($this->input->post('id') || $this->input->post('res_type'))
		{
			$this->count_page_post();

			return;
		}

		$result['page_id'] = $id;

		if(intval($res_type) == 1){
			$this->load->view('ajax/count_page_rev', $result);
		} else {
			return $this->load->view('ajax/count_page_rev', $result, true);
		}
	 }

	public function count_page_result($pageId = NULL)
	{
		$bijak_lib = $this->load->library('core/bijak');
		$pageId    = ($this->input->post('page_id')) ? $this->input->post('page_id') : $pageId;
		$accountID = ($this->member) ? $this->member['account_id'] : '';
		$result    = $bijak_lib->gePageCountMultiple($accountID, $pageId);

		$this->output->set_content_type('application/json');

		if(count($result) > 0)
		{
			$this->output->set_output(json_encode(array('msg' => 'ok', 'data' => $result)));

		} else {
			$this->output->set_output(json_encode(array('msg' => 'empty', 'data' => array())));

		}
	}

	public function count_page_post($id='', $res_type=2)
	{
		$page_id   = ($this->input->post('id'))? $this->input->post('id'):$id;
		$accountID = ($this->member) ? $this->member['account_id'] : '';
		$r_type    = ($this->input->post('res_type')) ? $this->input->post('res_type'): $res_type;

		if( ! empty($page_id) ) {
			$bijak_lib     = $this->load->library('core/bijak');
			$where         = array('page_id' => $page_id);
			$select        = 'count(*) as count';
			$count_like    = $bijak_lib->getpagelike($select, $where);
			$count_dislike = $bijak_lib->getpagedislike($select, $where);
			$count_comment = $bijak_lib->getpagecomment($select, $where);
			$count_follow  = $bijak_lib->getpagefollow($select, $where);
			$where         = array('account_id' => $accountID, 'page_id' => $page_id);
			$is_like       = $bijak_lib->getpagelike($select, $where)->row_array();
			$is_dislike    = $bijak_lib->getpagedislike($select, $where)->row_array();
			$is_follow     = $bijak_lib->getpagefollow($select, $where)->row_array();

			$data['page_id']       = $page_id;
			$data['callback']      = base_url('ajax/count_page');
			$data['count_like']    = $count_like->row_array();
			$data['count_dislike'] = $count_dislike->row_array();
			$data['count_comment'] = $count_comment->row_array();
			$data['count_follow']  = $count_follow->row_array();
			$data['is_like']       = (isset($is_like['count'])) ? $is_like['count'] : '0';
			$data['is_dislike']    = (isset($is_dislike['count'])) ? $is_dislike['count'] : '0';
			$data['is_follow']     = (isset($is_follow['count'])) ? $is_follow['count'] : '0';
			$data['like_uri']      = '';
			$data['dislike_uri']   = '';
			$data['follow_uri']    = '';

			if(!empty($accountID)) {
				$data['like_uri']    = base_url('ajax/like_page');
				$data['dislike_uri'] = base_url('ajax/dislike_page') ;
				$data['follow_uri']  = base_url('ajax/follow_page') ;
			}

			if(intval($r_type) == 1){
				$this->load->view('ajax/count_page', $data);
			}else{
				return $this->load->view('ajax/count_page', $data, true);
			}
		}
	}

	public function count_profile($id = '', $skip_follow_redirect = FALSE, $return_only = FALSE)
	{
		$page_id   = ($this->input->post('id'))? $this->input->post('id'):$id;
		$accountID = ($this->member) ? $this->member['account_id'] : '';

		if( ! empty($page_id) )
		{
			$bijak_lib        = $this->load->library('core/bijak');
			$check_friendship = $bijak_lib->count_profile_info($accountID, $page_id);

			$data['callback']                  = base_url('ajax/count_profile');
			$data['username']                  = $this->member['username'];
			$data['show_request_friend_modal'] = FALSE;
			$data['profile_name']              = $bijak_lib->getDisplayNameFromUserId($page_id);

			if(!empty($accountID)){
				$data['page_id']    = $page_id;
				$data['follow_uri'] = base_url('ajax/follow_profile');

				if($check_friendship['is_friend'] == 'yes')
				{
					$data['friend_status']       = 'Berteman';
					$data['friend_uri']          = base_url('ajax/friend_profile');
					$data['friend_uri_redirect'] = '';
					$data['is_friend']           = 'yes';
				}

				 if($check_friendship['is_friend'] == 'no')
				 {
					  $data['friend_status'] = 'Friend';
					  $data['friend_uri'] = base_url('ajax/friend_profile');
					  $data['friend_uri_redirect'] = '';
					  $data['is_friend'] = 'no';

					  $data['show_request_friend_modal'] = TRUE;


					  if($check_friendship['you_request'] == 'yes')
					  {
							$data['friend_status'] = 'Menunggu Konfirmasi';
							$data['friend_uri'] = '';
							$data['friend_uri_redirect'] = '';

							$data['show_request_friend_modal'] = FALSE;
					  }

					  if($check_friendship['they_request'] == 'yes')
					  {
							$data['friend_status'] = 'Konfirmasi Pertemanan!';
							$data['friend_uri'] = '';
							$data['friend_uri_redirect'] = base_url('komunitas/friendrequestlist');

							$data['show_request_friend_modal'] = FALSE;
					  }


					  if($skip_follow_redirect === FALSE)
					  {
							if($check_friendship['you_follow'] == 'yes')
							{
								 $data['friend_uri_redirect'] = base_url('komunitas');
							}
					  }
				 }
				 $data['follow_status'] = ($check_friendship['you_follow'] == 'yes') ? 'Unfollow' : 'Follow';
			} else {
				$data['friend_uri'] = '';
				$data['follow_uri'] = '' ;
				$data['friend_uri_redirect'] = '';

				$data['friend_status'] = '';
				$data['follow_status'] = '';
			}


			if($return_only)
			{
				 return $this->load->view('ajax/count_profile', $data, TRUE);
			} else {
				 $this->load->view('ajax/count_profile', $data);
			}
		}
	}

	 public function friend_profile()
	 {
		$page_id =  ($this->input->post('id'))? $this->input->post('id'):$id;
		$accountID  = ($this->member) ? $this->member['account_id'] : '';

		if( ! empty($page_id) && $accountID != '') {
			$bijak_lib = $this->load->library('core/bijak');

				$check_friendship = $bijak_lib->count_profile_info($accountID, $page_id);


				if($check_friendship['you_request'] == 'yes' || $check_friendship['they_request'] == 'yes')
				{

				$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode(array('msg' => 'bad')));

					 return;
				}

				$friend_account_id = $bijak_lib->getAccountIdFromUserId($page_id);

				if($check_friendship['is_friend'] == 'no')
				{
				$message = $this->input->post('message');

				$bijak_lib->addFriendRequest($accountID, $friend_account_id, $message);

				$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode(array('msg' => 'ok')));

				} else {
				$bijak_lib->unfriend($accountID, $friend_account_id);
				$bijak_lib->removeFollowUser($accountID, $page_id);
				// $bijak_lib->removeFollowUser($page_id, $accountID);

				$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode(array('msg' => 'ok')));

				}



		  } else {

			$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('msg' => 'bad')));
		  }
	 }

	 public function follow_profile()
	 {
		  $this->follow_page();
	 }

	public function follow_page()
	{
		$page_id =  $this->input->post('id');
		$accountID  = ($this->member) ? $this->member['account_id'] : '';
		if(!empty($accountID)){
			$bijak_lib = $this->load->library('core/bijak');

			$select = 'count(*) as count';
			$where = array('account_id' => $accountID, 'page_id' => $page_id);
			$is_follow = $bijak_lib->getpagefollow($select, $where)->row_array();

			if(intval($is_follow['count']) > 0){
				$where = array('account_id' => $accountID, 'page_id' => $page_id);
				$bijak_lib->deletepagefollow($where);
			}else{
				$data = array('account_id' => $accountID, 'page_id' => $page_id, 'entry_date' => mysql_datetime());
				$bijak_lib->insertpagefollow($data);
			}

			$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('msg' => 'ok')));
				return;
		}
		$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'bad')));
	}

	public function like_page()
	{
		$page_id =  $this->input->post('id');
		$accountID  = ($this->member) ? $this->member['account_id'] : '';
		if(!empty($accountID)){
			$bijak_lib = $this->load->library('core/bijak');

			$select = 'count(*) as count';

			$where = array('account_id' => $accountID, 'page_id' => $page_id);
			$is_like = $bijak_lib->getpagelike($select, $where)->row_array();

			if(intval($is_like['count']) > 0){
				$where = array('account_id' => $accountID, 'page_id' => $page_id);
				$bijak_lib->deletepagelike($where);
			}else{
				$data = array('account_id' => $accountID, 'page_id' => $page_id, 'entry_date' => mysql_datetime());
				$bijak_lib->insertpagelike($data);

				$where = array('account_id' => $accountID, 'page_id' => $page_id);
				$bijak_lib->deletepagedislike($where);
			}

			$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('msg' => 'ok')));
				return;
		}
		$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'bad')));
	}

	public function dislike_page()
	{
		$page_id =  $this->input->post('id');
		$accountID  = ($this->member) ? $this->member['account_id'] : '';

		if(!empty($accountID)){
			$bijak_lib = $this->load->library('core/bijak');

			$select = 'count(*) as count';

			$where = array('account_id' => $accountID, 'page_id' => $page_id);
			$is_dislike = $bijak_lib->getpagedislike($select, $where)->row_array();

			if(intval($is_dislike['count']) > 0){
				$where = array('account_id' => $accountID, 'page_id' => $page_id);
				$bijak_lib->deletepagedislike($where);
			}else{
				$data = array('account_id' => $accountID, 'page_id' => $page_id, 'entry_date' => mysql_datetime());
				$bijak_lib->insertpagedislike($data);

				$where = array('account_id' => $accountID, 'page_id' => $page_id);
				$bijak_lib->deletepagelike($where);
			}

			$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('msg' => 'ok')));
				return;
		}
		$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'bad')));

	}

	 public function comment_page($id='', $account_id='', $res_type='1', $n=50)
	 {
		  $account_id = ($this->member) ? $this->member['account_id'] : $account_id;
		  $id = ($this->input->post('id')) ? $this->input->post('id'):$id;
		  $bijak_lib = $this->load->library('core/bijak');

		  $where = "d1.page_id = '".$id."'";
		  $list_comment = $bijak_lib->getCommentPage($where);
		  $data['comments']          = $list_comment->result_array();

		  $data['can_comment']       = ($this->member)  ? TRUE : FALSE;
		  $data['member']            = $this->member;
		  $data['page_id']        = $id;
		 $data['preshown_coments'] = $n;

		  if($res_type == '1'){
				$this->load->view('ajax/comment_page', $data);
		  }else{
				return $this->load->view('ajax/comment_page', $data, true);
		  }
	 }

	 public function comment_page_add()
	 {
		  $comment = $this->input->post('comment');
		  $id = $this->input->post('id');
		  $account_id = ($this->member['account_id']) ? $this->member['account_id'] : '';
		  if(empty($account_id)){
			  return;
		  }
		  $bijak_lib = $this->load->library('core/bijak');

		  $dat = array('account_id' => $account_id, 'page_id' => $id, 'comment_text' => $comment, 'entry_date' => mysql_datetime());

		  $insert_id = $bijak_lib->insertCommentPage($dat);

		  $where = "d1.page_comment_id = ".$insert_id;
		  $list_comment = $bijak_lib->getCommentPage($where);

		  $data['cmnt'] = $list_comment->row_array();
		  $data['page_id'] = $id;
		  $data['member'] = $this->member;

		  echo $this->load->view('ajax/comment_page_last', $data);
	 }

	 public function comment_page_delete()
	 {
		  $account_id = ($this->member['account_id']) ? $this->member['account_id'] : '';

		  if(empty($account_id)){
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('msg' => 'bad')));
				return;
		  }

		  $cid = ($this->input->post('cid')) ? $this->input->post('cid') : '';
		  if(empty($cid)){
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('msg' => 'bad')));
				return;
		  }

		  $bijak_lib = $this->load->library('core/bijak');

		  $where = "d1.page_comment_id = ".$cid;
		  $list_comment = $bijak_lib->getCommentPage($where)->row_array();

		  if($list_comment['account_id'] != $account_id){
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode(array('msg' => 'bad')));
				return;
		  }

		  $where = "page_comment_id = ".$cid;
		  $bijak_lib->deleteCommentPage($where);

		  $this->output->set_content_type('application/json');
		  $this->output->set_output(json_encode(array('msg' => 'bad')));

	 }

	function _generateSalt($max)
	{
			$characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		  $i = 0;
		  $salt = "";
		  while ($i < $max) {
				$salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
				$i++;
		  }

		  return $salt;
	}


	public function comment_load() {
		$data['content_id']  = $this->input->post('id');
		$data['walluser']    = ($this->input->post('walluser')) ? $this->input->post('walluser') : 0;
		$data['wallpolitisi']  = ($this->input->post('wallpolitisi')) ? $this->input->post('wallpolitisi') : 0;
		$data['preshown_coments']    = ($this->input->post('n')) ? $this->input->post('n') : 50;
		  $data['content_date']    = ($this->input->post('content_date')) ? $this->input->post('content_date') : NULL;

		  $this->load->view('ajax/comment_load', $data);
	}

	 protected function getContentComment($contentID)
	 {
		  $bijak_lib = $this->load->library('core/bijak');
		  return $bijak_lib->getContentComment($contentID);
	 }

	public function comment_content()
	{
		if(isset($_GET['id']))
		{
			$bijak_lib       = $this->load->library('core/bijak');
			$contentID       = $_GET['id'];
			$ContentTypeName = $bijak_lib->getContentTypeName($contentID);

			$array_always_can_comment = array (
				 // 'BLOG',
				 'NEWS',
				 'SCANDAL',
				 'SURVEY',
				 'RACE',
				 'KEBIJAKAN'
			);

			if(in_array($ContentTypeName, $array_always_can_comment))
			{
				$data['can_comment_by_content'] = TRUE;
			} else {
				 $ContentTypeName                = $bijak_lib->checkIsContentCanComment($this->member['account_id'], $this->member['user_id'], $contentID);
				 $data['can_comment_by_content'] = $ContentTypeName;
			}

			$comments                  = $this->getContentComment($contentID);
			$data['comments']          = $comments;
			$data['wallpolitisi']      = ( isset($_GET['wallpolitisi']) ) ? intval($_GET['wallpolitisi']) : 0;
			$data['walluser']          = ( isset($_GET['walluser']) ) ? intval($_GET['walluser']) : 0;
			$data['can_comment']       = ($this->member)  ? TRUE : FALSE;
			$data['member']            = $this->member;
			$data['content_id']        = $contentID;
			$data['preshown_coments']  = isset($_GET['n']) ? intval($_GET['n']) : 50; // prev default = 2

			if(isset($_GET['content_date']))
			{
				 $data['content_date'] = $_GET['content_date'];
			}

			if($data['can_comment_by_content'])
			{
			  $this->load->view('ajax/comment_content', $data);
			} else {

			  $this->load->view('ajax/comment_content_view_only', $data);
			}
		}
	}



	public function comment_add()
	{
		if($this->member){
			$this->load->model('content_stream');
			$this->load->model('Politik_model');
			$this->load->helper('bijak');

			$comment_id       = sha1( uniqid() );
			$account_id       = $this->member['account_id'];
			$text_comment		= $this->input->post('comment', true);
			$content_id			= $this->input->post('cid',true);
			$content_query    = $this->content_stream->getSingleContentDetails($content_id);
			if($content_query->num_rows() > 0)
			{
				$content_data = array(
										'comment_id' => $comment_id,
										'account_id' => $account_id,
										'content_id' => $content_id,
										'text_comment' => $text_comment,
										'entry_date' => mysql_datetime());
				$this->content_stream->addComment($content_data);

				$cache_job = $this->load->module('job');
				$cache_job->cache_home_comment();

				$content_data['display_name'] = $this->member['username'];
				$data['content_id']           = $content_id;
				$data['member']               = $this->member;
				$data['cmnt']                 = $content_data;

					 if($this->input->post('content_date'))
					 {
						  $data['content_date']                 = $this->input->post('content_date');
					 }

					 /**
					  * Clean cache
					  **/


						  /*
						  $key_cache = $this->count_content_cache_120_original_key($content_id, 1,1, $account_id);

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 1, 1, NULL);

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 1, 1, '');

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 1,2, $account_id);

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 1, 2, NULL);

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 1, 2, '');

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);


						  $key_cache = $this->count_content_cache_120_original_key($content_id, 2,2, $account_id);

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 2, 2, NULL);

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 2, 2, '');

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);


						  $key_cache = $this->count_content_cache_120_original_key($content_id, 2,1, $account_id);

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 2, 1, NULL);

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);

						  $key_cache = $this->count_content_cache_120_original_key($content_id, 2, 1, '');

						  $this->count_content_cache_120_original_delete($key_cache);

						  unset($key_cache);
						  */

					 /**
					  * Clear Cache
					  **/

					 /*
					 $key_cache = $this->getContentComment_cache_900_original_key($content_id);

					 $this->getContentComment_cache_900_original_delete($key_cache);

					 unset($key_cache);

					 $key_cache = $this->Politik_model->getContentTextByID_cache_900_array_key($account_id, $content_id);

					 $this->Politik_model->getContentTextByID_cache_900_array_delete($key_cache);

					 unset($key_cache);

					 $key_cache = $this->Politik_model->getContentTextByID_cache_900_array_key(NULL, $content_id);

					 $this->Politik_model->getContentTextByID_cache_900_array_delete($key_cache);

					 unset($key_cache);
					 */

				$this->load->view('ajax/comment_item', $data);
			} else {
				$data['errorMessage'] = 'posting telah dihapus';
				$this->load->view('komunitas/error_page', $data);
			}

		}
	}


	public function comment_remove()
	{
		$this->load->model('content_stream');
		$this->load->model('Politik_model');

		$member = $this->session->userdata('member');
		if($member){
			$comment_id = $_POST['id'];
			$query = $this->content_stream->getCommentDetails($comment_id);
			$row = $query->row();
			if($row)
			{
				if($member['account_id'] == $row->account_id)
					 {
					$query = $this->content_stream->deleteComment($comment_id);

					 }
			}
		}
	}

	function getContentCount($content_id)
	{
		$this->load->model('content_stream');
		$return	= array(
			'message'			=> 'error',
			'count_comment' 	=> '0',
			'count_like' 		=> '0',
			'count_dislike' 	=> '0'
		);

		$content_count = $this->content_stream->getContentCount($content_id);
		if (!empty($content_count)) {
			$return['message']			= 'ok';
			$return['count_comment']	= $content_count['count_comment'];
			$return['count_like']		= $content_count['count_like'];
			$return['count_dislike']	= $content_count['count_dislike'];

			if ($content_count['content_group_name'] == 'BLOG') {
				$cache_job = $this->load->module('job');
				$cache_job->cache_home_blog_favorit();
			}
		}

		return $return;

	}
	public function like()
	{
		$this->load->model('content_stream');

		$member 		= $this->session->userdata('member');
		$response	= array(
			'message'			=> 'error',
			'count_comment' 	=> '0',
			'count_like' 		=> '0',
			'count_dislike' 	=> '0'
		);

		if($member){
			$content_id 	= $_POST['id'];
			if($content_id) {
				$content_data 	= array(
					'account_id'=> $member['account_id'],
					'content_id' 		=> $content_id,
					'entry_date' 		=> mysql_datetime()
				);

				$this->content_stream->removeLike($member['account_id'], $content_id);
				$this->content_stream->removeDislike($member['account_id'], $content_id);
				$this->content_stream->addLike($content_data);
				$response = $this->getContentCount($content_id);
			} else {
				$response['message']	= 'invalid_content_id';
			}
		} else {
			$response['message']	= 'not_authorized';
		}

		echo json_encode($response);
	}

	public function unlike()
	{
		$this->load->model('content_stream');

		$member 		= $this->session->userdata('member');
		$response	= array(
			'message'			=> 'error',
			'count_comment' 	=> '0',
			'count_like' 		=> '0',
			'count_dislike' 	=> '0'
		);

		if($member){
			$content_id = $_POST['id'];
			if($content_id) {
				$this->content_stream->removeLike($member['account_id'], $content_id);
				$response = $this->getContentCount($content_id);
			} else {
				$response['message']	= 'invalid_content_id';
			}
		} else {
			$response['message']	= 'not_authorized';
		}

		echo json_encode($response);
	}


	public function dislike()
	{
		$this->load->model('content_stream');

		$member 		= $this->session->userdata('member');
		$response	= array(
			'message'			=> 'error',
			'count_comment' 	=> '0',
			'count_like' 		=> '0',
			'count_dislike' 	=> '0'
		);

		if($member){
			$content_id 	= $_POST['id'];
			if ($content_id) {
				$content_data 	= array(	'account_id' => $member['account_id'],
					'content_id' => $content_id,
					'entry_date' => mysql_datetime()
				);

				$this->content_stream->removeLike($member['account_id'], $content_id);
				$this->content_stream->removeDislike($member['account_id'], $content_id);
				$this->content_stream->addDislike($content_data);
				$response = $this->getContentCount($content_id);
			} else {
				$response['message']	= 'invalid_content_id';
			}
		} else {
			$response['message']	= 'not_authorized';
		}

		echo json_encode($response);
	}

	public function undislike()
	{
		$this->load->model('content_stream');

		$member 		= $this->session->userdata('member');
		$response	= array(
			'message'			=> 'error',
			'count_comment' 	=> '0',
			'count_like' 		=> '0',
			'count_dislike' 	=> '0'
		);
		if($member){
			$content_id 	= $_POST['id'];

			if($content_id)
			{
				$this->content_stream->removeDislike($member['account_id'], $content_id);
				$response = $this->getContentCount($content_id);
			} else {
				$response['message']	= 'invalid_content_id';
			}
		} else {
			$response['message']	= 'not_authorized';
		}

		echo json_encode($response);
	}

	public function friend()
	{
		$member     = $this->session->userdata('member');
		$response   = 'error|error';

		if($member && !empty($_POST)){

			$accountID  = $member['account_id'];
			$friendID   = $_POST['fuid'];
			$status     = $_POST['status'];

			$this->load->model('member', 'member_lib');
			if ($status == 'add')   {
				$friend_request_id = $this->member_lib->addFriendRequest($accountID, $friendID, '');
				$response   = 'remove|UnFriend';
			}

			if ($status == 'remove')   {
				$friend_request_id = $this->member_lib->unfriend($accountID, $friendID);
				$response   = 'add|Friend';
			}
		}
		echo $response;
	}


	public function remove_post()
	{

		$member = $this->session->userdata('member');
		if($member){

				$this->load->model('content_stream');
				// $this->load->model('Content_mongo_stream');

			$content_id = $_POST['id'];
			$query = $this->content_stream->getSingleContentDetails($content_id);
			$row = $query->row();
			if($row)
			{
				if($member['account_id'] == $row->account_id || // poster
					$member['user_id'] == $row->page_id)         // wall-post receiver
				{
					// check if photo
					if($row->content_group_type == 20 || $row->content_group_type == 40 || $row->content_group_type == 22 || $row->content_group_type == 30) // WALL_PHOTO or ALBUM_PHOTO or VIDEO
					{
						$attch = $this->content_stream->getContentAttachment($content_id)->row();
						if($attch)
						{
							try{
								$filepath = '.'.$attch->attachment_path.'large/'.$attch->attachment_title;
								unlink($filepath);
								$filepath = '.'.$attch->attachment_path.'thumb/'.$attch->attachment_title;
								unlink($filepath);
							} catch (Exception $e) {
							}
							if($row->content_group_type == 22)
							{
								try{
									$filepath = '.'.$attch->attachment_path.'badge/'.$attch->attachment_title;
									unlink($filepath);
									$filepath = '.'.$attch->attachment_path.'icon/'.$attch->attachment_title;
									unlink($filepath);
								} catch (Exception $e) {
								}
							}
							if($row->content_group_type == 30)
							{
								try{
									$filepath = '.'.$attch->attachment_path.$attch->attachment_title; // video
									unlink($filepath);
									$filepath = '.'.$attch->attachment_path.'poster/'.$attch->attachment_title.'.jpg'; // vid poster
									unlink($filepath);
								} catch (Exception $e) {
								}

							}

						}

					}

					$query = $this->content_stream->deleteContent($content_id);
					// $this->Content_mongo_stream->deleteWallByContentId($content_id);

					 echo 'ok';

				} else {
					 echo 'error';
				}
			} else {
				  echo 'error';
			}
		} else {
		  echo 'error';
		}
	}
}