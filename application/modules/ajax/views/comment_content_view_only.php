
	<?php $counter = 0; ?>
	<?php foreach($comments as $cmnt): ?>

			<div class="comment row-fluid<?php if($counter >= $preshown_coments) echo ' hide'; ?>" id="cmntitem_<?php echo $cmnt['comment_id'];?>">
				<div class="comment-user span1"><img src="<?php echo icon_url($cmnt['attachment_title'],'user/'.$cmnt['user_id']);?>" alt=""></div>
				<div class="comment-content span11">
					<p class="comment-info">
					<span style="font-weight:bold;"><?php echo $cmnt['display_name']; ?></span>
					@ <?php echo  mysql_date("%d %M %Y - %H:%i", strtotime($cmnt['entry_date'])); ?></p>
					<p>
					<?php echo  $cmnt['text_comment'];?>
					</p>
				</div>
			</div>
	<?php $counter++; ?>
	<?php endforeach;?>

	<?php if($counter >= $preshown_coments): ?>
		<div class="more" id="morecmn_<?php echo $content_id?>"><div class="btn btn-mini show-all">show all comments</div></div>
		<script>
		<?php if($walluser > 0) { ?>
		$("*[id=morecmn_<?php echo $content_id?>]").click(function() {
			$("*[id=morecmn_<?php echo $content_id?>]").hide();
			$("*[id=comment_<?php echo $content_id?>] .hide").fadeIn('slow');
			$("#cmnt_<?php echo $content_id?> .hide").fadeIn('slow');
		});
		<?php } else { ?>
		$("#morecmn_<?php echo $content_id?>").click(function() {
  				$("#morecmn_<?php echo $content_id?>").hide();
  				$("#comment_<?php echo $content_id?> .hide").fadeIn('slow');
				$("#loadingImgLogin").hide();
		});
		<?php } ?>
		</script>
		<br />
	<?php endif;?>