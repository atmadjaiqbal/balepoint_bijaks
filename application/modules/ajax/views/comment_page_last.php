<div class="comment row-fluid" id="cmntitem_<?php echo $cmnt['page_comment_id'];?>">
    <div class="comment-user span1"><img src="<?php echo icon_url($cmnt['attachment_title'],'user/'.$cmnt['page_id']);?>" alt=""></div>
    <div class="comment-content span11">
        <?php if($cmnt['account_id'] == $member['account_id']){ ?>
            <div class="comment-content-remove">
                <a class="makeright icon-close" name="cmdDelComment" id="cmdDelComment_<?php echo $cmnt['page_comment_id'];?>" title="hapus komentar">
                    <i class="icon-remove half-transparent"></i>
                </a>
            </div>
        <?php } ?>
        <p class="comment-info">
            <a href="<?php echo base_url().'komunitas/wall?uid='.$cmnt['account_id'];?>"><?php echo $cmnt['page_name']; ?></a>
            @ <?php echo date('F j, Y - h:m', strtotime($cmnt['entry_date'])); ?>
        </p>
        <p>
            <?php echo $cmnt['comment_text'];?>
        </p>
    </div>
</div>

<script>
    $("#cmdDelComment_<?php echo $cmnt['page_comment_id'];?>").click(function(){
        if(confirm('Yakin ingin menghapus komentar ini ?')){
            $.ajax({
                url: "<?php echo base_url().'ajax/comment_page_delete'?>",
                type: "post",
                data: {cid: "<?php echo $cmnt['page_comment_id']?>"},
                success: function(response, textStatus, jqXHR){

                    $("#cmntitem_<?php echo $cmnt['page_comment_id'];?>").slideUp('slow', function() { $(this).remove(); } );

                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert(
                            "The following error occured: "+
                                    textStatus, errorThrown
                    );
                },
                complete: function(){

                    $('#count_<?php echo $page_id; ?>').load('<?php echo base_url("ajax/count_page"); ?>', {'id':'<?php echo $page_id; ?>', 'res_type':'1'});

                }
            });
        }
    });
</script>