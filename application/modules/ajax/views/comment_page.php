<div id="cmnt_place_<?php echo $page_id; ?>">
<?php $counter = 0; ?>
<?php foreach($comments as $cmnt): ?>

    <div class="comment row-fluid<?php if($counter >= $preshown_coments) echo ' hide'; ?>" id="cmntitem_<?php echo $cmnt['page_comment_id'];?>">
        <div class="comment-user span1"><img src="<?php echo icon_url($cmnt['attachment_title'],'user/'.$cmnt['page_id']);?>" alt=""></div>
        <div class="comment-content span11">
            <?php if($cmnt['account_id'] == $member['account_id']):?>
                <div class="comment-content-remove">
                    <a class="makeright icon-close" name="cmdDelComment" id="cmdDelComment_<?php echo $cmnt['page_comment_id'];?>" title="hapus komentar">
                        <i class="icon-remove half-transparent"></i>
                    </a>
                </div>
            <?php endif;?>
            <p class="comment-info">
                <?php if($can_comment) { ?><a href="<?php echo base_url().'komunitas/wall?uid='.$cmnt['account_id'];?>"><?php echo $cmnt['page_name']; ?></a><?php
            } else { ?>
                <span style="font-weight:bold;"><?php echo $cmnt['page_name']; ?></span>
                <?php } ?> @ <?php echo date('F j, Y - h:m', strtotime($cmnt['entry_date'])); ?></p>
            <p>
                <?php echo  $cmnt['comment_text'];?>
            </p>
        </div>
    </div>

<script>

     $("#cmdDelComment_<?php echo $cmnt['page_comment_id'];?>").click(function(){
        if(confirm('Yakin ingin menghapus komentar ini ?')){
            $.ajax({
                url: "<?php echo base_url().'ajax/comment_page_delete'?>",
                type: "post",
                data: {cid: "<?php echo $cmnt['page_comment_id']?>"},
                success: function(response, textStatus, jqXHR){

                        $("#cmntitem_<?php echo $cmnt['page_comment_id'];?>").slideUp('slow', function() { $(this).remove(); } );

                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert(
                            "The following error occured: "+
                                    textStatus, errorThrown
                    );
                },
                complete: function(){

                    $('#count_<?php echo $page_id; ?>').load('<?php echo base_url("ajax/count_page"); ?>', {'id':'<?php echo $page_id; ?>', 'res_type':'1'});

                }
            });
        }
    });
</script>

<?php $counter++; ?>
<?php endforeach;?>
</div>

<?php if($can_comment): ?>
<div class="comment show row-fluid">
    <div class="comment-user span1"><img alt="<?php echo $member['username']; ?>" src="<?php echo icon_url($member['xname'], 'user/'.$member['user_id']);?>" /></div>
    <div class="comment-content span11">
        <input type="text" id="commentinput_<?php echo $page_id;?>" class="input-block-level" placeholder="beri komentar...">
        <script>
            $('#commentinput_<?php echo $page_id?>').keyup(function(e) {
                if(e.keyCode == 13) {
                    var inputStr = $("#commentinput_<?php echo $page_id;?>").val();

                    if($.trim(inputStr) != ""){
                       $("#morecmn_<?php echo $page_id?>").hide();
                        $("#comment_<?php echo $page_id?> .hide").show();
                        $("#commentinput_<?php echo $page_id;?>").val("");

                        $.ajax({
                            url: "<?php echo base_url().'ajax/comment_page_add'?>",
                            type: "post",
                            data: {comment: $.trim(inputStr), id: "<?php echo $page_id?>"},
                            success: function(response, textStatus, jqXHR){
                                $('#cmnt_place_<?php echo $page_id; ?>').append(response)

                            },
//                            error: function(jqXHR, textStatus, errorThrown){
//                                alert(
//                                        "The following error occured: "+
//                                                textStatus, errorThrown
//                                );
//                            },
                            complete: function(){
                                $('#count_<?php echo $page_id; ?>').load('<?php echo base_url("ajax/count_page"); ?>', {'id':'<?php echo $page_id; ?>', 'res_type':'1'});
                            }
                        });
                    } else {
                        alert('cannot post empty comment');
                    }
                }
            });
        </script>
    </div>
</div>
<?php else: ?>
<div id="need_login">
    <span>Silahkan <a id="to_login">login</a> atau <a href="<?php echo base_url(); ?>home/signup">daftar</a> untuk memberi komentar</span>
    <div id="form_login" class="well hide" style="padding:5px; margin-top:10px">
        <span id="loadingImgLogin" class="hide"><img src="<?php echo base_url(); ?>public/image/ajax-loader.gif" /></span>
        <span class="message"><b></b></span>
        <form id="formlogin" name="formlogin" method="POST" action="<?php echo base_url(); ?>home/loginaction">
            <table>
                <tr><td>Email:</td><td width="10">&nbsp;</td><td>Password:</td></tr>
                <tr><td><input type="text" id="email" name="email" size="35" autocomplete="off" tabindex="1" /></td>
                    <td></td><td><input type="password" id="pass" name="pass" autocomplete="off"  tabindex="2"/></td></tr>
            </table>
            <input type="submit" class="btn" value="Sign In" />
            <input type="button" class="btn" onclick="$('#form_login').hide();" value="Cancel" />
            &nbsp; &nbsp;
            <a href="<?php echo base_url(); ?>home/forgotpass/">Lupa kata sandi?</a>
        </form>


    </div>
</div>
<script type="text/javascript">

    $('#to_login').click(function(){
        $('#form_login').show();
        $('#form_login #email').focus();
    });
    //console.debug($("#cmnt_<?php //echo $content_id?>"));
    $('#formlogin').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            pass: {
                required: true
            }

        },
        messages: {
            email: {
                required: "Please enter a valid email address",
                minlength: "Please enter a valid email address"
            },
            pass: {
                required: "Provide a password"
            }
        },
        errorPlacement: function(error, element) {
            error.css('color', 'red');
            //$('.msg').append(error);
            offset = element.offset();
            //error.insertBefore(element);
            error.css('top', offset.top);
            error.css('position', 'absolute');
            error.css('left', offset.left + element.outerWidth()+ 10);
            error.css('width','250px');
            error.insertAfter(element);
            //error.addClass('message');  // add a class to the wrapper

        },
        submitHandler: function(form) {
            $("#formlogin").hide();
            $("#loadingImgLogin").show('slow');
            var options = {
                method: 'POST', dataType: 'json',
                success: function(data) {
                    $('.message b').text('');
                    if(data.message != 'OK'){
                        $('.message b').append(data.message);
                        $("#loadingImgLogin").hide();
                        $("#formlogin").show('slow');

                        $('.message').show();
                    }else{
                        //location.href = '<?php echo base_url() ?>politik';
                        $('#ref').val('1');
                        $('*[id=ref2]').val('1');
                        $("#comment_<?php echo $content_id?>").load('<?php echo base_url(); ?>ajax/comment_content?id=<?php echo $content_id; ?>');
                        //alert('OK');
                    }

                    return false;
                }
            };
            $(form).ajaxSubmit(options);
            return false;
        }
    });
</script>
<?php endif;?>
<?php if($counter >= $preshown_coments): ?>
<div class="more" id="morecmn_<?php echo $page_id?>"><div class="btn btn-mini show-all">show all comments</div></div>
<script>

    $("#morecmn_<?php echo $page_id?>").click(function() {
        $("#morecmn_<?php echo $page_id?>").hide();
        $("#comment_<?php echo $page_id?> .hide").fadeIn('slow');
        $("#loadingImgLogin").hide();
    });

</script>
<br />
<?php endif;?>