<div class="conid_<?php echo $content_id; ?> content-action" data-entrydate="<?php echo $content_date; ?>"  data-tipe="<?php echo $tipe; ?>" data-restype="<?php echo $restype; ?>">
	<div class="content-action-comment" title="Jumlah Komentar Konten ini"><i class="icon-comments"></i>
			<span class="count-action"><?php echo (isset($count_comment)) ? $count_comment : 0; ?></span>
	</div>

	<div title="Suka Konten ini" class="content-action-like<?php echo ($is_like == '1') ? ' voted' : ''?>"
			data-cdate="<?php echo $content_date;?>"
			data-cb="<?php echo $callback; ?>"
			data-id="<?php echo $content_id; ?>"
			data-uri="<?php echo $like_uri; ?>"
			data-tipe="<?php echo $tipe; ?>"
			data-restype="<?php echo $restype; ?>">
			<i class="icon-thumbs-up"></i>
			<span class="count-action"><?php echo (isset($count_like)) ? $count_like : 0; ?></span>
	</div>

	<div title="Tidak Suka Konten ini" class="content-action-dislike<?php echo  ($is_dislike == '1') ? ' voted' : ''?>"
			data-cdate="<?php echo $content_date;?>"
			data-cb="<?php echo $callback; ?>"
			data-id="<?php echo $content_id; ?>"
			data-uri="<?php echo $dislike_uri; ?>"
			data-tipe="<?php echo $tipe; ?>"
			data-restype="<?php echo $restype; ?>">

			<i class="icon-thumbs-down"></i>
			<span class="count-action"><?php echo (isset($count_dislike)) ? $count_dislike : 0; ?></span>
	</div>
</div>
