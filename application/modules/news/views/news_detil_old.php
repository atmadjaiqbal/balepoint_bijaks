<?php 
$temp = !empty($news['modified_date']) ? $news['modified_date'] : $news['date'];
$date2Show = mdate('%d %M %Y - %h:%i %A', strtotime($temp));
?>

<div class="container">
	<?php //$this->load->view('template/tpl_sub_header'); ?>
    <?php $this->load->view('template/tpl_sub_header2'); ?>
</div>

<div class="container">
	<div class="sub-header-container">
		<div class="row-fluid">
			<!-- CONTENT DETAIL -->
            <div class="span8">
                <?php //echo '<pre>'; var_dump($news['type_news']); echo '</pre>';?>
                <?php if($news['type_news'] == 3){ ?>

                <div class="news-detail">
                    <h2><?php echo $news['title']; ?></h2>
                    <h4><?php echo $date2Show; ?></h4>
                    <div style="float:right;margin-top:-30px;">
                        <!-- a href="https://www.facebook.com/sharer/sharer.php?u=< ?php echo $og_slug; ?>&t=<?php echo $news['title']; ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                           target="_blank" title="Share on Facebook"><img style="width: 16px;height: 16px;" src="<?php echo base_url();?>assets/images/fb_bw.png"></a -->
                        <div class="fb-share-button" data-href="<?php echo $og_slug; ?>" data-layout="button"></div>                           
                        <a href="https://twitter.com/share?url=<?php echo $og_slug; ?>&via=bijaksdotnet&text=<?php echo $news['title']; ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                           target="_blank" title="Share on Twitter">
                            <img style="width: 16px;height: 16px;" src="<?php echo base_url();?>assets/images/tw_bw.png">
                        </a>
                        <a href="https://plus.google.com/share?url=<?php echo $og_slug; ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');return false;"
                           target="_blank" title="Share on Google+">
                            <img style="width: 16px;height: 16px;" src="<?php echo base_url();?>assets/images/gplus_bw.png">
                        </a>
                        <?php /*
                        <a href="http://pinterest.com/pin/create/button/?url={<?php echo $og_slug; ?>}&media={<?php echo $og_image; ?>}" class="pin-it-button" count-layout="horizontal">
                            <img style="width: 16px;height: 16px;" src="<?php echo base_url();?>assets/images/pin_bw.png">
                        </a>
                        */?>
                    </div>

                    <img class="news-img" alt="<?php echo $news['title']; ?>" src="<?php echo $news['image_large']; ?>">
                    <div class="score-place score-place-overlay score" data-id="<?php echo $news['content_id']; ?>" ></div>
                    <br>
                    <div class="row-fluid">
                        <span class="pull-left">Politisi Terkait</span>
                        <em class="pull-right"><?php echo $news['caption']; ?></em>
                    </div>
                    <div class="row-fluid news-profile-holder">
                        <div class="span6">
                            <div class="row-fluid ">
                                <div class="span12">
                                <?php 
                                if (count($news['news_profile']) > 0){ 
                                    foreach ($news['news_profile'] as $key => $pro) { 
                                        $img_pola = key_exists('score', $pro) ? 'img-btm-border-'.$pro['score'] : ''; 
                                ?>
                                    <div class="content-politisi-terkait">
                                        <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                            <img class="img-btm-border <?php echo $img_pola;?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                                        </a>
                                    </div>
                                <?php 
                                    } 
                                }else{ 
                                ?>
                                    <div class="span10"><span>Tidak ada politisi terkait</span></div>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="time-line-content pull-right" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo $news['content_id']; ?>">
                                <?php //echo $news['timeline']; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $conten = strip_tags($news['content'], '<p>');
                    $conten = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $conten);
                    $conten = str_replace('<p>', '<p class="news-wp">' , $conten);
                    ?>
                    <p><?php echo strip_tags($conten, '<p>'); ?></p>
                </div> 
                <?php }else{ ?>
                <div class="news-detail">
                    <h2><?php echo $news['title']; ?></h2>
                    <h4><?php echo mdate('%d %M %Y %h:%i %A', strtotime($news['date'])); ?></h4>
                    <div style="float:right;margin-top:-30px;">
                        <!-- a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $og_slug; ?>&t=<?php echo $news['title']; ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                           target="_blank" title="Share on Facebook"><img style="width: 16px;height: 16px;" src="<?php echo base_url();?>assets/images/fb_bw.png"></a -->
                        <div class="fb-share-button" data-href="<?php echo $og_slug; ?>" data-layout="button"></div>                              
                        <a href="https://twitter.com/share?url=<?php echo $og_slug; ?>&via=bijaksdotnet&text=<?php echo $news['title']; ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                           target="_blank" title="Share on Twitter">
                            <img style="width: 16px;height: 16px;" src="<?php echo base_url();?>assets/images/tw_bw.png">
                        </a>
                        <a href="https://plus.google.com/share?url=<?php echo $og_slug; ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');return false;"
                           target="_blank" title="Share on Google+">
                            <img style="width: 16px;height: 16px;" src="<?php echo base_url();?>assets/images/gplus_bw.png">
                        </a>
                        <?php /*
                        <a href="http://pinterest.com/pin/create/button/?url={< ?php echo $og_slug; ?>}&media={< ?php echo $og_image; ?>}" class="pin-it-button" count-layout="horizontal">
                            <img style="width: 16px;height: 16px;" src="< ?php echo base_url();?>assets/images/pin_bw.png">
                        </a>
                        */?>
                    </div>
                    <div class="news-detail-small text-center">
                        <img class="news-img" alt="<?php echo $news['title']; ?>" src="<?php echo $news['image_large']; ?>">
                        <div class="score-place score-place-overlay score" data-id="<?php echo $news['content_id']; ?>" >
                        </div>
                        <div class="row-fluid text-right" style="margin-top: 10px;">
                            <em class=""><?php echo $news['caption']; ?></em>
                        </div>
                        <div class="row-fluid ">
                            <div class="span2"><span>Politisi Terkait</span></div>
                            <div class="span10">
                            <?php 
                            if (count($news['news_profile']) > 0){ 
                                foreach ($news['news_profile'] as $key => $pro) { 
                                    $img_pola = key_exists('score', $pro) ? 'img-btm-border-'.$pro['score'] : '';
                            ?>
                                <div class="content-politisi-terkait">
                                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                        <img class="img-btm-border <?php echo $img_pola;?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                                    </a>
                                </div>
                            <?php } 
                            }else{ ?>
                                <span>Tidak ada politisi terkait</span>
                            <?php } ?>
                            </div>
                        </div>
                        <p></p>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="time-line-content pull-right" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo $news['content_id']; ?>">
                                    <?php //echo $news['timeline']; ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php
                    $conten = strip_tags($news['content'], '<p>');
                    $conten = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $conten);
                    $conten = str_replace('<p>', '<p class="news-wp">' , $conten);
                    ?>
                    <p><?php echo strip_tags($conten, '<p>'); ?></p>
                </div> <!-- News Detail -->

                <?php } ?>
                <div class="div-line"></div>
                <div class="row-fluid">

                    <h5>BERI KOMENTAR</h5>
                    <div class="row-fluid">
                        <div class="comment-side-left">
                            <div style="background:
                                    url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                            </div>
                        </div>

                        <div class="comment-side-right">
                            <div class="row-fluid">
                                <?php /*<input id='comment_type' data-id="<?=$news['content_id'];?>" type="text" name="comment" class="media-input">*/?>
                                <textarea id='comment_type' data-id="<?=$news['content_id'];?>" name="comment" class="media-input" rows="4"></textarea>
                            </div>
                            <div class="row-fluid">
                                <div class="span12 text-right">
                                    <?php if(!is_array($this->member)){?>
                                    <span>Login untuk komentar</span>&nbsp;
                                    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
                                    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
                                    <?php }elseif(is_array($this->member)){?>
                                    <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="comment" data-page="1" data-id="<?=$news['content_id'];?>" class="row-fluid comment-container"></div>
                <div id="load_more_place" class="row-fluid komu-follow-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a data-page="1" data-id="<?=$news['content_id'];?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>
                <div id="div_line_bottom" class="div-line"></div>

			</div>

			<div class="span4 right-side">
                <div class="home-title-section hp-label hp-label-hitam" style="background-color: #bbbbbb;margin-top:-25px;">
                    <?php
                    $_style = '';$_category = '';
                    switch(strtoupper($category))
                    {
                        case 'PARLEMEN' :
                            $_category =  'POLITIK PARLEMEN'; break;
                        case 'EKONOMI' :
                            $_category =  'POLITIK EKONOMI'; break;
                        case 'HUKUM' :
                            $_category =  'POLITIK HUKUM'; break;
                        case 'NASIONAL' :
                            $_category =  'POLITIK NASIONAL'; break;
                        case 'DAERAH' :
                            $_category =  'POLITIK DAERAH'; break;
                        case 'INTERNASIONAL' :
                            $_category =  'POLITIK INTERNASIONAL'; break;
                        case 'LINGKUNGAN' :
                            $_category =  'POLITIK LINGKUNGAN'; break;
                        case 'KESENJANGAN' :
                            $_category =  'POLITIK KESENJANGAN'; break;
                        case 'SARA' :
                            $_category =  'POLITIK SARA'; break;
                        case 'KOMUNITAS' :
                            $_category =  'POLITIK SOSIAL MEDIA'; break;
                        case 'GAYAHIDUP' :
                            $_category =  'POLITIK GAYA HIDUP'; break;
                        case 'RESES' :
                            $_category =  'POLITIK SENGGANG'; break;
                        case 'INDOBARAT' :
                            $_category =  'POLITIK INDONESIA BARAT';
                            $_style = 'style="font-size: 14px;'; break;
                        case 'INDOTIM' :
                            $_category =  'POLITIK INDONESIA TIMUR';
                            $_style = 'style="font-size: 14px;'; break;
                        case 'INDOTENG' :
                            $_category =  'POLITIK INDONESIA TENGAH';
                            $_style = 'style="font-size: 14px;';break;
                    }

                    if($category == 'terkini')
                    {
                        $_category = 'TAJUK UTAMA LAINNYA';
                    } else {
                        $_category = $_category . ' LAINNYA';
                    }

                    ?>
                    <span class="hitam" <?php echo $_style; ?>><a style="cursor:default;"><?php echo $_category; ?></a></span>
                </div>
                <p></p>
                <div class="">
                <?php
                    $dt['val'] = $news_list;
                    $this->load->view('home/news_headline_terkait', $dt);
                ?>
                </div>
                <?php /*
<!--				--><?php //foreach ($news_list as $key => $row) { ?>
<!--					<div class="row-fluid">-->
<!--						<div class="span12">-->
<!--						--><?php //
//							$dt['news_detail'] = $row;
//							$dt['val'] = $row;
////							$this->load->view('template/content/tpl_news_index', $dt);
//							$this->load->view('home/news_headline_list', $dt);
//
//						?>
<!--						</div>-->
<!--					</div>-->
<!--				--><?php //} ?>
                */?>
			</div>
		</div>
	</div>
</div>
<br>
<?php $this->load->view('template/modal/report_spam_modal'); ?>
