<style>
.content_box{
    width: 1200px;
    
    margin-left: auto;
    margin-right: auto;
    padding-top: 0;
}
.spanbox{
    float: left;
    min-height: 197px;
    width: 460px;
    border: solid 1px #c4c4c4;
    margin-left: 0;
    margin-right: 2em;
    margin-top: 1em;
    padding: 3px;
}

.img-btm-border {
    height: 30px;
    width: 30px;
}

.next_button{
    margin-top: 1em;
    margin-left: 16em;
}

.news_image{
    float: left;
    width: 25%;
}

.news_details{
    float: left;
    margin-left: .5em;
    width: 27.5em;
    text-align: justify;
}

.news_title3{
    font-weight: bold;
    margin-top: 0;
    font-size: 1.2em;
    line-height: 1em;
}

.news_description{
    line-height: 1.3em;
}

.news_politisi_terkait{
    min-height: 4em;
    margin-top: .4em;
}

.polter{
    float: left;
    margin-right: .3em;
}

.news-index-date {
    margin-top: -5px;
}

.news_last_activity{
    margin-top: 1em;
}

.social-small {
    height: 36px;
    padding-bottom: 2px;
    padding-top: 2px;
}

.news-image {
    height: 70px;
    width: 100%;
}

.news-index-date {
    margin-top: 0;
}

.morenote{
    margin-top: .3em;
    margin-left: .3em;
    font-size: .9em;
}

.score-container{
    max-width: 114px !important;
    height: 29px !important;
    padding-top: 1px;
}

/*.score-container{*/
    /*width: 225px;*/
    /*max-width: 225px;*/
    /*text-align: center;*/
    /*background: rgba(0, 0, 0, 0.6);*/
    /*margin: 0 auto;*/

/*}*/

.content-score{
    /*list-style: none;*/
    /*display: inline-block;*/
    /*margin: 0 auto;*/
    /*list-style-position:inside;*/
    padding: 1px !important;
    /*cursor: pointer;*/
}

.content-score li{
    margin-right: 2px !important;
    min-width: 32px !important;
    max-height: 28px !important;
}

.content-score span{
    /*color: white;*/
    /*font-style: italic;*/
    line-height: 20px !important;
    /*float: left;*/
}

.score-place-overlay{
    /*position: relative;*/
    margin-top: 1px !important;
}

#circular_progress_indicator_div{
    position: relative;
    margin-left: 30px;
    margin-right: auto;
    margin-top: 30px;
}

#circular_progress_indicator{
    width: 32px;
    height: 32px;
}
</style>
<div class="container">
    <?php $this->load->view('template/tpl_sub_header2'); ?>
</div>
<div class="container">
    <div class="content_box">
        <?php
        foreach ($news as $key => $row){
            $dt['news_detail'] = $row;

        ?>
        <div class="spanbox">
            <?php $this->load->view('template/content/tpl_news_index2', $dt); ?>
        </div>
        <?php
        }
        ?>
    </div>
</div>
<?php //echo $pagi; ?>
<div class="next_button">
    <input type="button" id="next_news" value="Berita Selanjutnya">&nbsp;&nbsp;
    <img id="circular_progress_indicator" src="<?php echo base_url('assets/images/image_825740.gif');?>" style="display:none;"/>
</div>

<div id="page_spot">
    <input type="hidden" id="hidden_page" value="2"/>
</div>




<script>
    var limit = 20;
    var page = $('#hidden_page').val();

    var category = '<?php echo $category;?>';
    $(document).ready(function(){
        $('#next_news').click(function(){
            $.ajax({
                type: 'post',
                datatype: 'json',
                url : Settings.base_url+'news/news_list',
                data:{'limit':limit,'page':page,'category':category},
                beforeSend: function(){
                    $('#circular_progress_indicator').css('display','block');
                },
                success: function(response){
                    $('.content_box').append(response);
                    page = Number(page) + 1;
                    $('#page_spot').html('<input type="hidden" id="hidden_page" value="'+page+'"/>'); // change page value 

                    if(response != ''){
                        $('.time-line-content').each( function(a, b){
                            var uri = $(b).data('uri');
                            var cat = $(b).data('cat');
                            // console.debug(uri);
                            $(b).load(Settings.base_url+'timeline/last/content/'+uri+'/false/'+cat);
                        });

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });
                    }

                },
                complete: function(){
                    $('#circular_progress_indicator').css('display','none');
                },
                error: function(){
                }


            });
        });

        

    });

</script>