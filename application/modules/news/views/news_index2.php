<style>
.content_box{
    width: 1200px;
    
    margin-left: auto;
    margin-right: auto;
    padding-top: 0;
}
.spanbox{
    float: left;
    min-height: 197px;
    width: 460px;
    border: solid 1px #c4c4c4;
    margin-left: 0;
    margin-right: 2em;
    margin-top: 1em;
    padding: 3px;
}

.img-btm-border {
    height: 30px;
    width: 30px;
}

.next_button{
    margin-top: 1em;
    margin-left: 16em;
}

.news_image{
    float: left;
    width: 25%;
}

.news_details{
    float: left;
    margin-left: .5em;
    width: 27.5em;
    text-align: justify;
}

.news_title3{
    font-weight: bold;
    margin-top: 0;
    font-size: 1.2em;
    line-height: 1em;
}

.news_description{
    line-height: 1.3em;
}

.news_politisi_terkait{
    min-height: 4em;
    margin-top: .4em;
}

.polter{
    float: left;
    margin-right: .3em;
}

.news-index-date {
    margin-top: -5px;
}

.news_last_activity{
    margin-top: 1em;
}

.social-small {
    height: 36px;
    padding-bottom: 2px;
    padding-top: 2px;
}

.news-image {
    height: 70px;
    width: 100%;
}

.news-index-date {
    margin-top: 0;
}

.morenote{
    margin-top: .3em;
    margin-left: .3em;
    font-size: .9em;
}

.score-container{
    max-width: 114px !important;
    height: 29px !important;
    padding-top: 1px;
}

/*.score-container{*/
    /*width: 225px;*/
    /*max-width: 225px;*/
    /*text-align: center;*/
    /*background: rgba(0, 0, 0, 0.6);*/
    /*margin: 0 auto;*/

/*}*/

.content-score{
    /*list-style: none;*/
    /*display: inline-block;*/
    /*margin: 0 auto;*/
    /*list-style-position:inside;*/
    padding: 1px !important;
    /*cursor: pointer;*/
}

.content-score li{
    margin-right: 2px !important;
    min-width: 32px !important;
    max-height: 28px !important;
}

.content-score span{
    /*color: white;*/
    /*font-style: italic;*/
    line-height: 20px !important;
    /*float: left;*/
}

.score-place-overlay{
    /*position: relative;*/
    margin-top: 1px !important;
}
</style>

<div class="container">
    <?php $this->load->view('template/tpl_sub_header2'); ?>
</div>
<div class="container">
    <div class="content_box">
        <?php
        foreach ($news as $key => $row){
            $dt['news_detail'] = $row;

        ?>
        <div class="spanbox">
            <?php $this->load->view('template/content/tpl_news_index2', $dt); ?>
        </div>
        <?php
        }
        ?>
    </div>
</div>
<?php echo $pagi; ?>

