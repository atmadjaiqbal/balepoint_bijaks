<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends Application {

	var $arrNews = array(
				1 => "hukum",
				3 => "kesenjangan",
				7 => "parlemen",
				9 => "nasional",
				4 => "ekonomi",
				10 => "teknologi",
				12 => "hankam",
				17 => "daerah",
				18 => "reses",
				19 => "internasional",
//				8 => "hot-issues"
        8 => "headline",
        1150 => 'gayahidup',
        1151 => 'komunitas',
        1152 => 'lingkungan',
        347 => 'sara',
        1221 => 'indobarat',
        1222 => 'indoteng',
        1223 => 'indotim'
			);
    var $member = array();
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $data = $this->data;
        $this->load->library('news_lib');
        $this->load->library('politik/politik_lib');
        $this->load->library('scandal/scandal_lib');
        $this->load->module('timeline');
        $this->load->module('scandal');
        $this->load->module('profile');
        $this->load->module('survey');
        $this->load->module('headline');


        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->member = $this->session->userdata('member');

        $this->member = $this->session->userdata('member');

//        var_dump($this->member);
        // $redis = new Redis();
		// echo '<pre>';
		// var_dump($this->redis_slave->hget('news', '13042'));
		// echo '</pre>';
    }

    /*function get_version() {

	    echo CI_VERSION; // echoes something like 1.7.1

	}*/

    public function category($category='') {

		if( ! empty($category) ) {
			$this->index($category);
		}
	}

    /*
    function index2($category="")
    {
        $data['title'] = "Bijaks | Total Politik";
		$data['scripts'] = array('bijaks.js');
    	// print $category;
    	$topic_id = array_search($category, $this->arrNews);

    	$data['category'] = (empty($category)) ? 'terkini' : $category;

    	$id = 0;
    	$ct = 'index';
		if(!empty($category)){
			$id = $topic_id;
			$ct = 'category/' . $category;
		}

    	$limit = 29;
		$page = 1;
		$per_page = $this->input->get('per_page');
		if(!is_bool($per_page)){
			if(!empty($per_page)){
				$page = $per_page;	
			}
		}

		$offset = ($limit*$page) - $limit;
		// echo $offset;

		$qr = $_SERVER['QUERY_STRING'];

		
		//TRY USE REDIS
		
//        $last_key = $this->redis_slave->lRange('news:list:rkey', 0, 0);
        $last_key = $this->redis_slave->get('news:key');
        $rkey = 'news:topic:list:'.$last_key.':'.$id;

		$user_count = $this->redis_slave->lLen($rkey);
        
// kalo udah 1000 tergantung dari news list categories dari redis

       if($offset > $user_count)
       {
           $limit = $limit + 1;
           $result = $this->getNewsMysql($id, $limit, $offset, $result=1);           
        } else {
	       $result = $this->getNewsCache($id, $limit, $offset, $result=1);
        }

		$this->load->library('pagination');

		$config['base_url'] = base_url().'news/' . $ct . '?'; //.$qr;
		$config['total_rows'] = $user_count;
		$config['per_page'] = $limit; 
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string'] = TRUE;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2; // round($choice);
		$config['full_tag_open'] = '<div class="pagi pagi-centered"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="disabled"><div><a>';
		$config['cur_tag_close'] = '</a></div></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '...';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '...';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$this->pagination->initialize($config); 

		$data['pagi'] =  $this->pagination->create_links();
		$data['news'] = $result;
		$data['offset'] = $offset;
		$data['total']	= $user_count;
		$data['topic_last_activity'] = $id; // $this->timeline->topic($id);

        $html['html']['content']  = $this->load->view('news/news_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }
    */

    function index3($category="")
    {
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts'] = array('bijaks.js');
        // print $category;
        $topic_id = array_search($category, $this->arrNews);

        $data['category'] = (empty($category)) ? 'terkini' : $category;
        $data['ajax']['category'] = $data['category'];

        $id = 0;
        $ct = 'index';
        if(!empty($category)){
            $id = $topic_id;
            $ct = 'category/' . $category;
        }

        $limit = 29;
        $data['ajax']['limit'] = $limit;
        $page = 1;
        $per_page = $this->input->get('per_page');
        if(!is_bool($per_page)){
            if(!empty($per_page)){
                $page = $per_page;
            }
        }
        $data['ajax']['page'] = $page;

        $offset = ($limit*$page) - $limit;
        // echo $offset;

        $qr = $_SERVER['QUERY_STRING'];


        /**
        TRY USE REDIS
         */
//        $last_key = $this->redis_slave->lRange('news:list:rkey', 0, 0);
        $last_key = $this->redis_slave->get('news:key');
        $rkey = 'news:topic:list:'.$last_key.':'.$id;

        $user_count = $this->redis_slave->lLen($rkey);

// kalo udah 1000 tergantung dari news list categories dari redis

        if($offset > $user_count)
        {
            $limit = $limit + 1;
            $result = $this->getNewsMysql($id, $limit, $offset, $result=1);
        } else {
            $result = $this->getNewsCache($id, $limit, $offset, $result=1);
        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'news/' . $ct . '?'; //.$qr;
        $config['total_rows'] = $user_count;
        $config['per_page'] = $limit;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2; // round($choice);
        $config['full_tag_open'] = '<div class="pagi pagi-centered"><ul>';
        $config['full_tag_close'] = '</ul></div>';
        $config['cur_tag_open'] = '<li class="disabled"><div><a>';
        $config['cur_tag_close'] = '</a></div></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '...';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '...';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;

        $this->pagination->initialize($config);

        $data['pagi'] =  $this->pagination->create_links();
        $data['news'] = $result;
        $data['offset'] = $offset;
        $data['total']	= $user_count;
        $data['topic_last_activity'] = $id; // $this->timeline->topic($id);

        $html['html']['content']  = $this->load->view('news/news_index2', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    function index($category="")
    {
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts'] = array('bijaks.js');
        // print $category;
        $topic_id = array_search($category, $this->arrNews);

        $data['category'] = (empty($category)) ? 'terkini' : $category;
        $data['ajax']['category'] = $data['category'];

        $id = 0;
        $ct = 'index';
        if(!empty($category)){
            $id = $topic_id;
            $ct = 'category/' . $category;
        }

        $limit = 29;
        $data['ajax']['limit'] = $limit;
        $page = 1;
        $per_page = $this->input->get('per_page');
        if(!is_bool($per_page)){
            if(!empty($per_page)){
                $page = $per_page;
            }
        }
        $data['ajax']['page'] = $page;

        $offset = ($limit*$page) - $limit;
        // echo $offset;

        $qr = $_SERVER['QUERY_STRING'];


        //TRY USE REDIS
        $last_key = $this->redis_slave->get('news:key');
        $rkey = 'news:topic:list:'.$last_key.':'.$id;

        $user_count = $this->redis_slave->lLen($rkey);

// kalo udah 1000 tergantung dari news list categories dari redis

        if($offset > $user_count)
        {
            $limit = $limit + 1;
            $result = $this->getNewsMysql($id, $limit, $offset, $result=1);
        } else {
            $result = $this->getNewsCache($id, $limit, $offset, $result=1);
        }


        $data['ajax']['page'] = $page;
        $data['ajax']['limit'] = $limit;
        $data['ajax']['offset'] = $offset;
        $data['ajax']['user_count'] = $user_count;
        $data['ajax']['id'] = $id;
        $data['ajax']['category'] = (empty($category)) ? 'terkini' : $category;


        // $this->load->library('pagination');

        // $config['base_url'] = base_url().'news/' . $ct . '?'; //.$qr;
        // $config['total_rows'] = $user_count;
        // $config['per_page'] = $limit;
        // $config['use_page_numbers'] = TRUE;
        // $config['page_query_string'] = TRUE;
        // $choice = $config["total_rows"] / $config["per_page"];
        // $config["num_links"] = 2; // round($choice);
        // $config['full_tag_open'] = '<div class="pagi pagi-centered"><ul>';
        // $config['full_tag_close'] = '</ul></div>';
        // $config['cur_tag_open'] = '<li class="disabled"><div><a>';
        // $config['cur_tag_close'] = '</a></div></li>';
        // $config['num_tag_open'] = '<li>';
        // $config['num_tag_close'] = '</li>';
        // $config['next_link'] = '...';
        // $config['next_tag_open'] = '<li>';
        // $config['next_tag_close'] = '</li>';
        // $config['prev_link'] = '...';
        // $config['prev_tag_open'] = '<li>';
        // $config['prev_tag_close'] = '</li>';
        // $config['first_link'] = FALSE;
        // $config['last_link'] = FALSE;

        // $this->pagination->initialize($config);

        // $data['pagi'] =  $this->pagination->create_links();
        $data['news'] = $result;
        $data['offset'] = $offset;
        $data['total']  = $user_count;
        $data['topic_last_activity'] = $id; // $this->timeline->topic($id);

        $html['html']['content']  = $this->load->view('news/news_index3', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }
    function article($ID="0-0", $slug="")
    {
		    $data['scripts'] = array('bijaks.js', 'bijaks.spam.js');
    	  $IDs = explode("-", $ID);
    	  $id = intval($IDs[0]);
		    $news_id = intval(@$IDs[1]);

		    $data['category'] = ($id === 0) ? 'terkini' : $this->arrNews[$id];

		    $data['topic_last_activity'] = $id; // $this->timeline->topic($id);
   		  $data['news'] = $this->getNewsCache($news_id, $limit=0, $offset=0, $result=2);   
		
        $terkait = array(); $_t = 0; $flag_terkait = true;
        if(isset($data['news']['news_terkait']))
        {
            foreach($data['news']['news_terkait'] as $rk)
            {
                $_terkait = $rk['terkait_news_id'];
                $terkait[$_t] = $this->getNewsCache($_terkait, $limit=0, $offset=0, $result=2);
                if($terkait[$_t] == '' || empty($terkait[$_t])) {
                    $terkait[$_t] = $this->getNewsMysql($_terkait, $limit=0, $offset=0, $result=2);
                }
                $_t++;
            }
        } else {
            $terkait = $this->news_lib->getNewsTerkait($data['news']['content_id']);
            if($terkait) {
                foreach($terkait as $rk) {
                    $_terkait = $rk['terkait_news_id'];
                    $terkait[$_t] = $this->getNewsCache($_terkait, $limit=0, $offset=0, $result=2);
                    if($terkait[$_t] == '' || empty($terkait[$_t])) {
                        $terkait[$_t] = $this->getNewsMysql($_terkait, $limit=0, $offset=0, $result=2);
                    }
                    $_t++;
                }
            }
        }

        if($data['news'] == '')
        {
            $data['news'] = $this->getNewsMysql($news_id, $limit=0, $offset=0, $result=2);
            $terkait = $this->news_lib->getNewsTerkait($data['news']['content_id']);
            if($terkait) {
                foreach($terkait as $rk) {
                    $_terkait = $rk['terkait_news_id'];
                    $terkait[$_t] = $this->getNewsCache($_terkait, $limit=0, $offset=0, $result=2);
                    if($terkait[$_t] == '' || empty($terkait[$_t])) {
                        $terkait[$_t] = $this->getNewsMysql($_terkait, $limit=0, $offset=0, $result=2);
                    }
                    $_t++;
                }
            }
        }

        $data['news_terkait'] = $terkait;

        $_id_catnews = isset($data['news']['categories'][0]['id']) ? $data['news']['categories'][0]['id'] : 0;
        $og_slug = "http://www.bijaks.net/news/article/".$_id_catnews.'-'.$data['news']['id'].'/'.$data['news']['slug'];
        $polterkait = array(); foreach($data['news']['news_profile'] as $rwpol) {$polterkait[] = $rwpol['page_name']; }
        $_artdesc = strip_tags($data['news']['content'], '<p>');
        $_artdesc = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $_artdesc);
        $_artdesc = strip_tags($_artdesc);
        $_artdesc = str_replace('"','',$_artdesc);
        $_artdesc = str_replace("'","",$_artdesc);
        //$_artdesc = str_replace('&#x2013;','',$_artdesc);

        $data['title'] = $data['news']['title']; // "Bijaks | Total Politik";
        $data['articledesc'] = substr($_artdesc, 0, 160);
        $data['polterkait'] = $polterkait;
        $data['topnews'] = array($data['news']['title']);
        $data['og_image'] = $data['news']['image_large'];
        $data['og_slug'] = $og_slug;

// var_dump($data['news']['content_id']);
//		$data['list_comments'] = $this->timeline->comment('content', $data['news']['content_id'], $result='true');

		$data['news_list']  = $this->getNewsCache($id, $limit=17, $offset=0, 1, $news_id);

		$html['html']['content']  = $this->load->view('news/news_detil', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    private function getNewsMysql($id=0, $limit=10, $offset=0, $result=1, $key_exist=0)
    {
        if($result === 1)
        {
             $news = $this->news_lib->getBeritaList($id, $offset, $limit);            
			 $result = array();
             $i = 0;
             if(count($news) > 0)
             {
                 foreach($news as $key => $value)
                 {                     
                       $result[$i] = $this->news_lib->getNews($value['news_id']);
                       $result[$i]['topic_id'] = $result[$i]['topic_id'];
                       $result[$i]['image_thumbnail'] = 'http://news.bijaks.net/uploads/'.$result[$i]['content_image_url'];
                       $i++;
                 }
            }
            return $result;
        } else {
            $news_array = $this->news_lib->getNews($id);
            $news_array['topic_id'] = $news_array['topic_id'];
            $news_array['image_thumbnail'] = 'http://news.bijaks.net/uploads/'.$news_array['content_image_url'];
            
            $news_url = 'http://news.bijaks.net/mobile/get_post/?dev=1&id='.$id;
			$ch = curl_init($news_url);				
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $body  = substr($response, $header_size);
            $json_output = json_decode($body);
            $conten  = $json_output->post->content;

            $conten = strip_tags($conten, '<p>'); 
            $conten = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $conten); 
		    $attachments 	= $json_output->post->attachments;
		    $image_url		= '';
		    $image_title 	= '';
		    $image_caption = '';
		    $image_height	= '';
		    if (!empty($attachments)) {
			    $image_url		= $attachments[0]->url;
			    $image_title 	= $attachments[0]->title;
			     $image_caption = $attachments[0]->caption;
			     $image_height 	= $attachments[0]->images->full->height;
		     }
		
            $news_array = array_merge($news_array, 
		     array(
                'date' => $news_array['entry_date'],
			    'message'			=> 'ok',
			    'content' 			=> $conten, 
			    'image_large'			=> $image_url,
			    'image_title' 		=> $image_title,
			    'caption' 	=> $image_caption,
			    'image_height' 	=> $image_height,
			    'category' 			=> $json_output->post->categories[0]->title
		     ));

			return $news_array;
       }
    }

    private function getNewsCache($id=0, $limit=9, $offset=0, $result=1, $key_exist=0)
    {
    	if($result === 1){
//            $last_key = $this->redis_slave->lRange('news:list:rkey', 0, 0);

            if($id == 8)
            {
                $rkey = "news:list:headline";
            } else {
                $last_key = $this->redis_slave->get('news:key');
                $rkey = 'news:topic:list:'.$last_key.':'.$id;
            }

			$rrange = $offset + $limit;

			$news = $this->redis_slave->lRange($rkey, $offset, $rrange);

            if($key_exist != 0)
                if(@in_array($key_exist, $news) ){
                    $news = $this->redis_slave->lRange($rkey, $offset, intval($rrange) + 1);
                }

            $news = array_unique($news);

			$result = array();
			$i = 0;
            if(count($news) > 0){
                foreach ($news as $key => $value) {
                    // echo $key;
                    if($key_exist == intval($value))
                        continue;
//                    if($key == $limit)break;
    //				$news_array = array();
                    $news_detail = $this->redis_slave->get('news:detail:'.$value);
//                    echo $key. ' => ' . $news_detail . '<br>';
                    if($news_detail != 'None'){
                        $news_array = @json_decode($news_detail, true);
                        $result[$i] = $news_array;
                        $result[$i]['topic_id'] = $news_array['categories'][0]['id'];

                        $i++;
                   } else {
                        $news = $this->news_lib->getBeritaList($id, $offset, $limit);
                        if(count($news) > 0)
                        {
                            $i = 0;
                            foreach($news as $key => $value)
                            {
                                $result[$i] = $this->news_lib->getNews($value['news_id']);
                                $result[$i]['topic_id'] = $result[$i]['topic_id'];
                                $result[$i]['image_thumbnail'] = 'http://news.bijaks.net/uploads/'.$result[$i]['content_image_url'];
                                $i++;
                            }
                        }
                   }
                }
            }
			return $result;
		}else{
			// $result = array();
			$news_detail = $this->redis_slave->get('news:detail:'.$id);
//            $news_detail = $this->redis_slave->get('news:'.$id);
			$news_array = @json_decode($news_detail, true);
			return $news_array;

		}
    }

    function sesi($category='0', $limit=0, $return='false' ){

        $data['scripts'] = array('bijaks.js');
        // print $category;
        $topic_id = (array_search($category, $this->arrNews)) ? array_search($category, $this->arrNews) : 0;
        $data['category'] = 'terkini';
        $where = array();
        if($category != '0'){
            $where = array('tht.topic_id' => $topic_id);
            $data['category'] = $category;
        }

        $result = $this->getNewsCache($topic_id, $limit, 0, 1);
        $data['result'] = $result;
        if($return === 'true'){

            $tpl = $this->load->view('template/content/tpl_news_category', $data, True);
            if(intval($limit) > 0 )
                $tpl = $this->load->view('template/content/tpl_news_category_multi', $data, True);
            return $tpl;
        }

        if(intval($limit) === 0 ){
            $this->load->view('template/content/tpl_news_category', $data);
        }else{
            $this->load->view('template/content/tpl_news_category_multi', $data);
        }


    }

    function newsloader(){
        $data['scripts'] = array('bijaks.js');

        $category = $this->uri->segment(3); $page = $this->uri->segment(4); $limit = 4; if($page == 2) $limit = $limit + 1;
        $offset = ($limit*intval($page)) - $limit; $rrange = $offset;
        $topic_id = (array_search($category, $this->arrNews)) ? array_search($category, $this->arrNews) : 0;
        $data['category'] = 'terkini'; $where = array(); if($category != '0'){ $where = array('tht.topic_id' => $topic_id); $data['category'] = $category; }
        $res = $this->getNewsCache($topic_id, $limit, $rrange , 1); $i = 0;
        if($res)
        {
          foreach($res as $key => $value)
          {
            $content_id = (isset($value['content_id']) || !empty($value['content_id'])) ? $value['content_id'] : 0;
            $result[$i] = $value;
            $result[$i]['komu'] = $this->timeline->last_score($content_id, true, '1');
            $i++;
          }
        } else {
            $result = array();
        }

        $data['result'] = $result;
        return $this->load->view('template/content/tpl_news_content', $data);
    }

    function home_hotlist($catnews)
    {
        $topic_id = 0;
        $page = $this->uri->segment(4);
        $limit = 4; if($page == 2) $limit = $limit;
        $offset = (($limit*intval($page)) + 1) - $limit;
        if($page == 2) $offset = (($limit*intval($page)) + 2) - $limit;
        $rrange = $offset + $limit;
        $last_key = $this->redis_slave->get('news:key');
        $rkey = 'news:topic:list:'.$last_key.':' . $catnews;
        $resnews = array(); $morenews = array(); $i = 0;
        if($page == 1)
        {
           $news = $this->redis_slave->lrange($rkey, 0, 1);
           $rowNews = $this->redis_slave->get('news:detail:'.$news[0]);
        } else {
           $news = $this->redis_slave->lrange($rkey, 5, 6);
           $rowNews = $this->redis_slave->get('news:detail:'.$news[0]);
        }
        $news_array = json_decode($rowNews, true);
        $resnews = $news_array; $resnews['topic_id'] = $news_array['categories'][0]['id'];
        $resnews['komu'] = $this->timeline->last_score($news_array['content_id'], true, '0');
        $newsmore = $this->redis_slave->lrange($rkey, $offset, $rrange);
        foreach($newsmore as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_more = json_decode($rowNews, true);
            $morenews[$i] = $news_more;
            $morenews[$i]['topic_id'] = $news_more['categories'][0]['id'];
            $i++;
        }
        $data['topnews'] = $resnews; $data['morenews'] = $morenews;
        return $this->load->view('template/content/tpl_news_home_hotlist', $data);
    }

    function home_beritapilihan()
    {
        $topic_id = 0;
        $last_key = $this->redis_slave->get('news:key'); $rkey = 'news:topic:list:'.$last_key.':1151';
        $selectnews = array(); $i = 0; $_news = $this->redis_slave->lrange($rkey, 0, 15);
        foreach($_news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_select = json_decode($rowNews, true);
            $selectnews[$i] = $news_select;
            $selectnews[$i]['topic_id'] = $news_select['categories'][0]['id'];
            $i++;
        }
        $data['result'] = $selectnews;
        return $this->load->view('template/content/tpl_news_home_pilihan', $data, true);
    }

    function news_list(){
        $category = !empty($_POST['category']) ? $_POST['category'] : 'terkini';
        $limit = !empty($_POST['limit']) ? $_POST['limit'] : 10;
        $page = !empty($_POST['page']) ? $_POST['page'] : 1;
        $topic_id = array_search($category, $this->arrNews);
        $limit = 10;
        $id = 0;
        $ct = 'index';
        if(!empty($category)){
            $id = $topic_id;
            $ct = 'category/' . $category;
        }

        // $page = 2;
        $offset = ($limit*$page) - $limit;
        // if($offset > $user_count){
        //     $limit = $limit + 1;
        //     $result = $this->getNewsMysql($id, $limit, $offset, $result=1);
        // } else {
        //     $result = $this->getNewsCache($id, $limit, $offset, $result=1);
        // }
        $result = $this->getNewsMysql($id, $limit, $offset, $result=1);

        $data['news'] = $result;
        $data['category'] = $category;
        $this->load->view('ajax_index',$data);
    }

}
