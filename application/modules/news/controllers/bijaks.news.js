$(function(){
    //COMMENT ON HOVER
    $('#comment').on('mouseenter', '.comment-row', function(e){
        e.preventDefault();
        $(this).find('.flag').show();

    }).on('mouseleave', '.comment-row' ,function(e){
            $(this).find('.flag').hide();
            $(this).find('.dropdown').removeClass('open');
        });

    $('#comment').on('click', '.report-spam', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var tipe = $(this).data('tipe');
        $('#submit_report>#tipe').val(tipe);
        $('#submit_report>#cid').val(id);
        $('#modal_report').modal('show');
    });
});