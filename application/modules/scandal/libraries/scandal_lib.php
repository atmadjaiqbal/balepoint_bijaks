<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scandal_Lib {

	protected  $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database('slave');
	}

	public function getScandalList($tipe='', $page='0', $limit = '10',$categoryId = 0, $scandalsToExclude = array()) {
		$this->CI->load->helper('bijak');
		$member = $this->CI->session->userdata('member');
		$member_account_id = (isset($member['account_id'])) ? $member['account_id'] : NULL;

        switch ($tipe) {
            case 'hotlist':
                $where = " AND ts.hotlist = '1' ";
                $order_by = ' ts.hot_date DESC ';
            break;
            case 'headline':
                $where = " AND ts.headline = '1' ";
                $order_by = ' ts.head_date DESC ';
            break;
            default:
                $where = '';
                $order_by = ' date DESC ';
        }

        $exclusion = '';
        if(count($scandalsToExclude)>0){
            $exclusion = implode(',', $scandalsToExclude);
            $exclusion = ' AND ts.scandal_id NOT IN('.$exclusion.') ';
        }

        $addJoin = $categoryId != 0 ? 'LEFT JOIN tscandal_category_detail scd ON(scd.scandal_id = ts.`scandal_id`)' : '';
        $addCondition = $categoryId != 0 ? 'AND scd.`scandal_category_id`="'.$categoryId.'"' : '';

		$sql     = "
        SELECT ts.scandal_id,tc.content_id,ts.title, ts.status, ts.date, ts.updated_date, ts.uang, 
        ts.publish, ts.short_desc, left(ts.description,250) description,tc.count_comment, 
        tc.count_like, tc.count_dislike,
        get_count_like_content_by_account('".$member_account_id."', tc.content_id) as is_like,
		get_count_dislike_content_by_account('".$member_account_id."', tc.content_id) as is_dislike
        FROM tscandal ts
		LEFT JOIN tcontent tc on ts.scandal_id = tc.location AND tc.content_group_type = '50'
        ".$addJoin."
		WHERE main_scandal = '0' and publish = 1 ".$where." ".$addCondition.$exclusion." 
		ORDER BY date DESC LIMIT ".$page.", ".$limit;
		$result  = $this->CI->db->query($sql)->result_array();
		foreach ($result as $key => $row) {
            $photos = $this->getScandalPhoto($row['scandal_id']);
			$result[$key]['url'] = base_url() . "scandal/index/" . $row['scandal_id'] . "-" . urltitle($row['title']) . "/";
			$result[$key]['photo'] = (isset($photos[0])) ? $photos[0]['attachment_title'] : 0;
			$result[$key]['photos'] = (!empty($photos)) ? $photos : array() ;
			$result[$key]['players'] = $this->getScandalPlayer($row['scandal_id']);
			$contentDate = mysql_date("%d %M %Y - %h:%i", strtotime($row['updated_date']));
			$result[$key]['content_date'] =  $contentDate;
            //$result[$key]['content_count'] =  $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate);
            $result[$key]['content_count'] = '';
		}
		return $result;
	}

	public function getScandalListCount($mainScandal=0,$publish=1) {
        $mainScandalCondition = $mainScandal != '999999' ? 'main_scandal = "'.$mainScandal.'"' : '1';
		$sql     = "SELECT count(scandal_id) total_rows FROM tscandal WHERE ".$mainScandalCondition." AND publish = '".$publish."'";
		$result  = $this->CI->db->query($sql)->row_array();
		$return  = '0';
		if (isset($result['total_rows'])) $return = $result['total_rows'];
		return $return;
	}

	public function getScandalDetail($scandalID) {
        if(is_array($scandalID)) return null;
		$sql     = "SELECT ts.*, tc.content_id,
							tc.count_comment, tc.count_like, tc.count_dislike
		            FROM tscandal ts
		            LEFT JOIN tcontent tc on ts.scandal_id = tc.location AND tc.content_group_type = '50'
		            WHERE ts.scandal_id = '$scandalID' ";
		/*
		SELECT ts.*, tc.content_id
            FROM tscandal ts
            LEFT JOIN tcontent tc ON tc.`location` = ts.`scandal_id` AND tc.content_group_type = 50
            WHERE %s and ts.main_scandal = %s
            ORDER BY `date` DESC
		 */
      	$result  = $this->CI->db->query($sql)->row_array();
      	if (!empty($result)) {
         	$cm = $this->CI->load->module('ajax');
		   	$photos = $this->getScandalPhoto($result['scandal_id']);
			$result['photo']     = (isset($photos[0])) ? $photos[0]['attachment_title'] : 0;
			$result['photos']    = (!empty($photos)) ? $photos : array() ;
         	$result['players']   = $this->getScandalPlayer($result['scandal_id']);

         	$contentDate         		= mysql_date("%d %M %Y - %h:%i", strtotime($result['updated_date']));
         	$result['content_date']   	= $contentDate ;
         	$result['content_count']   = $cm->count_content($result['content_id'], '2', '2', NULL, $contentDate);
			$result['kronologi'] = $this->getScandalKronologi($result['scandal_id']);
			$result['relations'] = $this->getScandalAliran($result['scandal_id']);

      	}
		return $result;
	}

	public function getScandalPhoto($scandalID) {
//		$sql     = "SELECT *  FROM tcontent_attachment
//		            WHERE content_id in (SELECT content_id FROM tscandal_photo Where scandal_id = '$scandalID' ORDER BY created_date DESC)";
		$sql = "SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('public/upload/image/skandal/', ta.attachment_title) AS original_url,
            CONCAT('public/upload/image/skandal/large/', ta.attachment_title) AS large_url,
            CONCAT('public/upload/image/skandal/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('public/upload/image/skandal/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url
            FROM tscandal_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            where tp.scandal_id = '".$scandalID."' ORDER BY ta.`order` ASC";
      $result  = $this->CI->db->query($sql)->result_array();
		return $result;
	}


	public function getScandalPlayer($scandalID, $offset = 0, $limit = 5) {
		$LIMIT = "LIMIT $offset, $limit ";
		if (!$offset)  $LIMIT = '';

//		$sql = "SELECT tp.page_id as pg_id, tp.title, tp.description, top.page_id, top.profile_content_id, top.page_name , ta.attachment_title
//					  FROM tscandal_player tp
//					  LEFT JOIN tobject_page top ON top.page_id = tp.page_id
//					  LEFT JOIN tcontent_attachment ta ON ta.content_id = top.profile_content_id
//					  WHERE scandal_id = '$scandalID'
//					  ORDER BY tp.nomor_urut, tp.player_id DESC    $LIMIT";
		$sql = "SELECT sp.date, sp.title, sp.`description`, sp.`short_desc`, sp.`sumber_data`, sp.`main_player`, sp.`scandal_id`, sp.`nomor_urut`,
            sp.`pengaruh`, tp.page_id, tp.`page_name`,
            CONCAT('politisi/index/', tp.page_id) AS profile_url,
            ta.`attachment_title`,
            CONCAT('public/upload/image/politisi/', tp.`page_id`, '/icon/', ta.attachment_title) AS profile_icon_url,
            CONCAT('public/upload/image/politisi/', tp.`page_id`, '/thumb/', ta.attachment_title) AS profile_thumb_url
            FROM tscandal_player sp
            LEFT JOIN tobject_page tp ON tp.`page_id`=sp.`page_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id`=tp.`profile_content_id`
            where sp.scandal_id = '".$scandalID."' ORDER BY nomor_urut ASC ".$LIMIT;


		$result = $this->CI->db->query($sql)->result_array();
		return $result;
	}

	public function getScandalKronologi($scandalID) {
		$sql     = "SELECT scandal_id, title, date, short_desc FROM tscandal WHERE main_scandal = '$scandalID' order by date ";
        $result  = $this->CI->db->query($sql)->result_array();
		return $result;
	}

    public function GetScandalKronologies($scandalID, $limit=0, $offset=0)
    {
        $lm = '';
        if (intval($limit) != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }

        $sql     = "SELECT scandal_id, title, date, short_desc FROM tscandal WHERE main_scandal = '$scandalID' order by date $lm";
        $result  = $this->CI->db->query($sql)->result_array();
        return $result;
    }

	public function getScandalAliran($scandalID) {
		$sql     = "SELECT tr.*, ta.attachment_title source_attachment,  tat.attachment_title target_attachment
						FROM trelations_map tr
					   LEFT JOIN tobject_page top ON top.page_id = tr.source_id
					   LEFT JOIN tcontent_attachment ta ON ta.content_id = top.profile_content_id

					   LEFT JOIN tobject_page topt ON topt.page_id = tr.target_id
					   LEFT JOIN tcontent_attachment tat ON tat.content_id = topt.profile_content_id
						WHERE tr.scandal_id = '$scandalID' order by tanggal ";
      $result  = $this->CI->db->query($sql)->result_array();
		return $result;
	}

	public function getScandalByPlayer($page){
		$sql = "SELECT tp.date, tp.`title` AS title_player, tp.`description` AS player_desc, tp.`short_desc`, tp.`sumber_data`, tp.`nomor_urut`, tp.`pengaruh`,
            ts.`scandal_id`, ts.`title` AS scandal_title, ts.`description` AS scandal_desc
            FROM tscandal_player tp
            LEFT JOIN tscandal ts ON ts.`scandal_id`=tp.`scandal_id`
            WHERE tp.page_id  = '".$page."'
            ORDER BY tp.date desc";
		$result  = $this->CI->db->query($sql)->result_array();
		return $result;
	}

	public function getScandalPhotoHeadline($scandalId){
		$sql = "SELECT tp.content_id, tp.type, t.`content_group_type`, t.`title`, ta.`attachment_title`,
            ta.hotlist AS 'hotlist', ta.headline AS 'headline', ta.`order` AS 'set_order',
            CONCAT('{0}/public/upload/image/skandal/', ta.attachment_title) AS original_url,
            CONCAT('{0}/public/upload/image/skandal/large/', ta.attachment_title) AS large_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/', ta.attachment_title) AS thumb_url,
            CONCAT('{0}/public/upload/image/skandal/thumb/landscape/', ta.attachment_title) AS thumb_landscape_url
            FROM tscandal_photo tp
            LEFT JOIN tcontent t ON t.`content_id` = tp.`content_id`
            LEFT JOIN tcontent_attachment ta ON ta.`content_id` = t.`content_id`
            where tp.scandal_id = '".$scandalId."' AND ta.headline = 1";
		$result  = $this->CI->db->query($sql)->result_array();
		return $result;
	}

    public function getCategoryDetails($categoryId){
        $this->CI->db->select('scandal_category_id,scandal_category_name,scandal_category_description,sort_number');
        $this->CI->db->where('scandal_category_id = "'.$categoryId.'"');
        $result = $this->CI->db->get('tscandal_category');
        return $result->result_array()[0];
    }

}

/* End of file file.php */