<style>
.scandal_box{
    float: left;
    width: 960px;
    margin-left: .5em;
    margin-right: .5em;
    margin-top: .5em;
    /*border: none 1px black;*/
    height: auto;
    /*max-height: 557px;*/
    border: solid 1px #c4c4c4;
    padding-top: 7px;
    padding-right: 5px;
    padding-left: 7px;
    padding-bottom: 0;

}    

.left_section{
    float: left;
    width: 320px;
}

.right_section{
    float: left;
    width: 625px;
    padding: 7px;
    
}

.time-line-content {
    width: 630px;
}

#index_scandal_container{
    width: 984px;
}

.large-carousel-title{
    font-weight: bold;
    font-size: 1.5em;
    margin-left: 1px;
    margin-top: -9px;
    height: 24px;
}

.info1{
    font-weight: normal !important;
    font-size: 16px !important;
    line-height: 16px !important;
}

.row-fluid [class*="span"] {
    min-height: 20px !important;
 }

 .next_button{
    margin-top: 1em;
    margin-left: 16em;
}

 #circular_progress_indicator{
    width: 32px;
    height: 32px;
}

.img-polaroid-scandal{
    width: 45px !important;
    height: 45px !important;
}

.large-control-container {
    height: 55px !important;
    margin-top: 3px !important;
}
.large-carousel-control {
    /*background-color: #bbbbbb;*/
    height: 55px !important;
    /*overflow: hidden;*/
    /*text-align: left;*/
}

.info2{
    float: left;
    width: 72px;
    line-height: 16px;
}

.content_info2{
    float: left;
    line-height: 16px;
    width: 550px;
    text-align: justify;
    word-wrap: break-word;
}

.score-container {
    /*background: none repeat scroll 0 0 rgba(0, 0, 0, 0.6);*/
    /*height: 38px;*/
    /*margin: 0 auto;*/
    max-width: 320px !important;
    /*text-align: center;*/
    width: 320px !important;
}
.tab-content {
    overflow: hidden !important;
}
</style>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <?php $this->load->view('template/skandal/tpl_scandal_sub_header'); ?>
        </div>
        <div class="span12">
            &nbsp;
        </div>
    </div>
</div>

<!-- Banner Ads begin  -->
<!-- Date : 30 June 2015   -->
<!-- Firstly Ibolz Ads for apps -->

<style type="text/css">
    .text_adds {
        font-family: "Courier New", Courier, monospace !important;
        font-size: 18px !important;
        font-weight: bold !important;
        color: black !important;
        text-align: center !important;
    }
</style>

<?php
$link_ibolz = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
$link_ntmc = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc";

$text1 = "100% Dangdut, 100% Goyang, 100% Gratis!!! klik disini";
$text2 = "Ribuan Film Horor dan Action, Gratis!!! UNTUK ANDA YANG BERANI !!! KLIK DI SINI";
$text3 = "Kini anda bisa menonton Film berkualitas HD langsung dari smartphone anda, klik di sini";
$text4 = "Pantau Sekarang Ratusan CCTV yg tersebar di seluruh Indonesia, LIVE !!!";
$text5 = "Pantau perayaan Idul Adha lewat CCTV yang tersebar di titik penting di kotamu";
$text7 = "Musik Dangdut, Pop, New Hits, Semuanya ada di sini !!!";
$text8 = "takut terjebak macet saat perayaan idul adha dan liburan panjang? cari tahu dulu situasinya disini";

$ads_banner1 = '<a href="http://id.ibolz.tv/home/channel_detail/19055066575480387f8490e/19055066575480387f8490e/channel" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092801.jpg'.'"/></a>
                <a href="'.$link_ibolz.'" target="_blank"><div class="text_adds">'.$text1.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner2 = '<a href="http://id.ibolz.tv/home/channel_detail/1371339731548545414abe7/1371339731548545414abe7/channel" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092802.jpg'.'"/></a>
                <a href="'.$link_ibolz.'" target="_blank"><div class="text_adds">'.$text3.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner3 = '<a href="http://id.ibolz.tv/home/channel_detail/54275805254854568af8bc/54275805254854568af8bc/channel" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092803.jpg'.'"/></a>
                <a href="'.$link_ibolz.'" target="_blank"><div class="text_adds">'.$text2.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner4 = '<a href="http://ntmc.ibolz.tv" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092804.jpg'.'"/></a>
                <a href="'.$link_ntmc.'" target="_blank"><div class="text_adds">'.$text4.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner5 = '<a href="http://ntmc.ibolz.tv" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092805.jpg'.'"/></a>
                <a href="'.$link_ntmc.'" target="_blank"><div class="text_adds">'.$text5.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner6 = '<a href="http://ntmc.ibolz.tv" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092806.jpg'.'"/>
                <a href="'.$link_ntmc.'" target="_blank"><div class="text_adds">'.$text8.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner7 = '<a href="'.$link_ibolz.'" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz20150904_07.jpg'.'"/>
                <div class="text_adds">'.$text7.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner8 = '<a href="'.$link_ibolz.'" target="_blank">
                <img src="'.base_url() . 'assets/images/adds8.jpg'.'"/>
                <div class="text_adds">'.$text4.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
                
$ads_banner = array($ads_banner1, $ads_banner6, $ads_banner2, $ads_banner8, $ads_banner3, $ads_banner7, $ads_banner4, $ads_banner5);
shuffle($ads_banner);

// $FireFox = preg_match('/Firefox/i', $_SERVER['HTTP_USER_AGENT']);
// $InterEx = preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT']);
// $Opera   = preg_match('/Opera/i', $_SERVER['HTTP_USER_AGENT']);
// $Chrome  = strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome');
// $Safari  = strpos($_SERVER['HTTP_USER_AGENT'], 'Safari');

// //do something with this information
// if( $FireFox ){
//     $_link_download = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
// }else if($InterEx){
//     $_link_download = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
// }else if($Opera){
//     $_link_download = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
// }else if($Chrome){
//     $_link_download = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
// }else if($Safari){
//     $_link_download = "http://id.ibolz.tv";
// }

?>
<div class="container-fluid">
    <div class="sub-header-container">
        <div class="row-fluid">
             <?php echo $ads_banner[0]; ?>
        </div>
    </div>
</div>
<!-- End Of Ads Banner -->


<div>
    <div class="container " id="index_scandal_container">
        <?php 
        foreach($skandal as $key => $val):
            $dt['val'] = $val;
        ?>
        <div class="scandal_box">
            <?php $this->load->view('template/skandal/tpl_skandal_index_large3', $dt); ?>
        </div>
        <?php 
        endforeach;
        ?>
        
    </div>
</div>
<?php  //echo $pagi; ?>
<div class="next_button">
    <input type="button" id="next_scandals" value="Skandal Selanjutnya">&nbsp;&nbsp;
    <img id="circular_progress_indicator" src="<?php echo base_url('assets/images/image_825740.gif');?>" style="display:none;"/>
</div>

<div id="page_spot">
    <input type="hidden" id="hidden_page" value="2"/>
</div>
<?php /*
<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>

<?php foreach($skandal as $key => $val) { ?>
    <?php if($key % 2 == 0){ ?>
        <div class="container">
            <div class="sub-header-container">
                <div class="row-fluid">
     <?php } ?>
        <div class="span6">
            <?php $dt['val'] = $val;
                //echo $val['title'];
            ?>
            <?php $this->load->view('template/skandal/tpl_skandal_index_large', $dt); ?>

        </div>
    <?php if($key % 2 == 1 || $key == count($skandal) - 1){  ?>
                </div>
            </div>
        </div>
        <br>
    <?php } ?>
<?php } ?>
<?php  echo $pagi; ?>
*/?>
<br>
<script>
    var limit = 10;
    var page = $('#hidden_page').val();

    var category = '';
    $(document).ready(function(){
        $('#next_scandals').click(function(){
            $.ajax({
                type: 'post',
                datatype: 'json',
                url : Settings.base_url+'scandal/scandals_list',
                data:{'limit':limit,'page':page,'category':category},
                beforeSend: function(){
                    $('#circular_progress_indicator').css('display','block');
                },
                success: function(response){
                    $('#index_scandal_container').append(response);
                    page = Number(page) + 1;
                    $('#page_spot').html('<input type="hidden" id="hidden_page" value="'+page+'"/>'); // change page value 
                    if(response != ''){
                        $('.time-line-content').each( function(a, b){
                            var uri = $(b).data('uri');
                            var cat = $(b).data('cat');
                            var size = $(b).data('size');
                            // console.debug(uri);
                            $(b).load(Settings.base_url+'timeline/last/content/'+uri+'/false/'+cat+'/'+size);
                        });

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                        $('.carousel').carousel({interval: 3000});
                    }

                },
                complete: function(){
                    $('#circular_progress_indicator').css('display','none');
                },
                error: function(){
                }


            });
        });

        

    });

</script>