<?php 
if(!empty($scandal)){
    foreach ($scandal as $key => $v) { 
    $tmp = new DateTime($v['date']);
    $kejadian = $tmp->format('d M Y');
    $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
    $skandal_photos = (count($v['photos']) > 0) ? $v['photos'][count($v['photos']) - 1]['large_url'] : '';
    $imgAUrl = $v['photos'][0]['large_url'];

    $forArray = array('!', '#', '$','"',',');
    $_judul = str_replace($forArray, '', $v['title']);
    $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);
    ?>
<div class="scandals_a_box">
    <div class="scandal_image" id="scandal_image_<?php echo $v['scandal_id'];?>">
        <?php 
        echo anchor($skandal_url,'<img class="scandals_a_image lazy" src="'.$imgAUrl.'" />','');
        ?>
    </div>
    <div class="scandals_a_info">
        <div class="scandals_a_title">
            <?php echo anchor($skandal_url,$v['title'],'');?>
        </div>
        <div class="scandals_a_description">
            <?php echo $v['short_desc'];?>
        </div>
        <div class="scandals_a_smallinfo">
            <div style="line-height: 1em;">status<span style="margin-left: 35px;"><?php echo ':&nbsp;'.$v['status'];?></span></div>
            <div style="line-height: 1em;">kejadian<span style="margin-left: 20px;"><?php echo ':&nbsp;'.$kejadian;?></span></div>
            <div style="line-height: 1em;">dampak<span style="margin-left: 23px;line-height: 1em;"><?php echo ':&nbsp;'.$dampak;?></span></div>
        </div>
    </div>
    <div class="scandals_a_players">
        <div style="font-weight: bold;">Para Pemain</div>
        <div class="" style="min-height: 48px;">
            <?php if(count($v['players'])>0) {
                ?>
                <ul class="ul-img-hr">
                    <?php foreach ($v['players'] as $key => $pro) { ?>
                        <?php 
                        if ($key === 5) {
                            echo 'more';
                            break;
                        };

                        if ($pro['profile_icon_url'] != 'None') {
                            $img_politik = $pro['profile_icon_url'];
                            if (!file_exists($img_politik)) {
                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                            }
                        } else {
                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                        }
                        ?>
                        <li class="">
                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                <img
                                    class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                    title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                    src="<?php echo base_url().$img_politik; ?>">
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
            <?php } ?>
        </div>
    </div>
</div>
<?php 
};
};
?>