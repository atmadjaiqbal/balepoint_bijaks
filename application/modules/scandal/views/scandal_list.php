﻿<?php if(!empty($scandals)):?>
	<?php foreach ($scandals as $rid => $row) :?>
		<?php
		$rid = $offset + $rid;
		$countPolitisi = count($row['players']); $i = 1; $politisi = '';
		foreach($row['players'] as $player):
			$coma = ($i < $countPolitisi) ? ', ' : '';
			if (!empty($player['page_id']) && !empty($player['page_name'])) {
				$politisi .= '<a href="'.base_url() . 'politisi/index/' . $player['page_id'].'">'.$player['page_name'] . '</a>' . $coma;
			} else {
				if (!empty($player['pg_id'] )) $politisi .= $player['pg_id'] . $coma;
			}
			$i++;
		endforeach;
		?><div class="content-place">
				<h4 style="padding:0;"><a href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?></a></h4>
				<div class="more-space"><?php echo $row['content_count'];?></div>

				<div class="row-fluid">
				<div class="span7 pull-left">
					<?php if (!empty($row['photos'])) :?>
						<div id='carousel-scandal-<?=$rid?>' class='carousel-scandal span12' style="height:100%">
							<div class='carousel slide' style="margin-bottom:0px;">
								<div class='carousel-inner'>
									<?php foreach ($row['photos'] as $i => $photo):?>
									<div class="item<?=$i==0?' active':''?>" data-slide-number="<?php echo $i;?>" style="height:260px;line-height:260px;vertical-align: middle;" >
										<img class="carousel-center" src="<?=$photo['large_url']; ?>" style="max-height:260px; margin: 0 auto;">
									</div>
									<?php endforeach;?>
								</div>
								<?php if (count($row['photos']) > 1) :?>
									<a class='carousel-control left' href='#carousel-scandal-<?=$rid?>' data-slide='prev'>&lsaquo;</a>
									<a class='carousel-control right' href='#carousel-scandal-<?=$rid?>' data-slide='next'>&rsaquo;</a>
								<?php endif;?>
							</div>
						</div>

						<div class="row-fluid slider-thumbs">
							  <div class="span12" style="margin-top:5px;">
									<ul class="thumbnails">
										<?php foreach ($row['photos'] as $i => $photo):?>
										 <li style="margin-bottom:3px;width:10%;">
											  <a class="thumbnail" id="carousel-thumb-<?php echo $i;?>" data-carousel="carousel-scandal-<?=$rid?>">
													<img src="<?php echo thumb_url($photo['attachment_title'], 'skandal'); ?>" />
											  </a>
										 </li>
										 <?php endforeach;?>
									</ul>
							  </div>
						 </div>
					<?php endif;?>
				</div>

				<div class="span5 pull-right" style="margin-top:-10px;">
					<table class='table-h table-skandal' style="margin:0;">
						<tr><th>Status:</th><td><?php echo $row['status']; ?></td></tr>
						<tr><th>Dampak:</th><td><?php echo $row['uang']; ?></td></tr>
						<tr><th>Politisi terkait:</th><td><?php echo $politisi;?></td></tr>
						<tr><td colspan='2'><?php echo $row['description']; ?>...</td></tr>

					</table>
				</div>

				<p class="moar readmore"><a href="<?php echo base_url() . "scandal/index/" . $row['scandal_id'] . "-" . urltitle($row['title']) . "/" ;?>">Lihat selengkapnya</a></p>
				</div>
		</div> <!-- .content-place -->


		<div style="clear:both"></div><hr/>
	<?php endforeach;?>
<?php endif;?>