<div class="scandals_b_box">
<?php 
if(!empty($scandal)){
    foreach ($scandal as $key => $v) {
        $forArray = array('!', '#', '$','"',',');
        $_judul = str_replace($forArray, '', $v['title']);
        $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);

        $tmp = new DateTime($v['date']);
        $kejadian = $tmp->format('d M Y');
        $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
        // $imgBUrl = @base_url('public/upload/image/skandal/large/'.$v['photo']);
        // $imgTUrl = @base_url('public/upload/image/skandal/large/'.$v['photo']);
        $imgBUrl = $v['photos'][0]['large_url'];
        $linkedImage = anchor($skandal_url,'<img class="scandals_a_image lazy" src="'.$imgBUrl.'" style="width: 100%;max-height: 151px;"/>','');
        $linkedTImage = anchor($skandal_url,'<img class="scandals_a_image lazy belseimage" src="'.$imgBUrl.'" />','');

        $dotUrl = anchor($skandal_url,'...');
        $linkedTitle = anchor($skandal_url,$v['title'],'');
        $SLinkedTitle = anchor($skandal_url,substr($v['title'],0,40).$dotUrl,'');
        $shortDescription = $v['short_desc'];
        $status = $v['status'];
        if($key == 0){
        ?>
        <div class="scandals0">
            <div class="scandal0Image">
                <?php echo $linkedImage;?>
            </div>
            <div class="scandal0Info" style="line-height: 18px;">
                <div class="scandal0Title"><?php echo $linkedTitle;?></div>
                <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">status</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                <div class="clear"></div>
                <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">kejadian</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                <div class="clear"></div>
                <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">dampak</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;word-wrap: break-word;width: 226px;"><?php echo $dampak;?></div>
                <div class="clear"></div>
                <div class="playersthumb" style="margin-left: 9px;">
                    <div style="font-weight: bold;">Para Pemain</div>
                    <div class="" style="min-height: 48px;">
                        <?php if(count($v['players'])>0) {
                            ?>
                            <ul class="ul-img-hr">
                                <?php foreach ($v['players'] as $key => $pro) { ?>
                                    <?php 
                                    if ($key === 5) {
                                        echo 'more';
                                        break;
                                    };

                                    if ($pro['profile_icon_url'] != 'None') {
                                        $img_politik = $pro['profile_icon_url'];
                                        if (!file_exists($img_politik)) {
                                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                                        }
                                    } else {
                                        $img_politik = 'assets/images/icon/no-image-politisi.png';
                                    }
                                    ?>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                            <img
                                                class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                src="<?php echo base_url().$img_politik; ?>" >
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php    
        }else{
        ?>
        <div class="scandalsBElse">
            <?php echo $linkedTImage;?> 
            <div style="font-weight: bold;"><?php echo $SLinkedTitle;?></div>
            <div class="clear"></div>
            <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">status</div>
            <div class="smallfont" style="float: left;width: 4px;">:</div>
            <div class="smallfont" style="float: left;"><?php echo $status;?></div>
            <div class="clear"></div>
            <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">kejadian</div>
            <div class="smallfont" style="float: left;width: 4px;">:</div>
            <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
            <div class="clear"></div>
            <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">dampak</div>
            <div class="smallfont" style="float: left;width: 4px;">:</div>
            <div class="smallfont" style="float: left;word-wrap: break-word;width: 88px;"><?php echo substr($dampak,0,50).$dotUrl;?></div>
            <div class="clear"></div>
        </div>
        <?php    
        }


    } 
};?>
</div>
<div class="clear"></div>