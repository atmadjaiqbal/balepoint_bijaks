<?php 
$categoryD = [
                    'categoryId' => 1,
                    'categoryName' => 'seksual',
                    'image' => 'seksual-logo1.png'
                ];
if(!empty($scandal)){
    foreach ($scandal as $key => $v) {
        $forArray = array('!', '#', '$','"',',');
        $_judul = str_replace($forArray, '', $v['title']);
        $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);

        $tmp = new DateTime($v['date']);
        $kejadian = $tmp->format('d M Y');
        $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
        $imgDUrl = $v['photos'][0]['large_url'];
        $imgTUrl = $v['photos'][0]['thumb_landscape_url'];
        $linkedImage = anchor($skandal_url,'<img class="scandals_d_image lazy" src="'.$imgDUrl.'" />','');
        $linkedTImage = anchor($skandal_url,'<img class="scandals_d_image lazy belseimage" src="'.$imgDUrl.'" />','');

        $dotUrl = anchor($skandal_url,'...');
        $linkedTitle = anchor($skandal_url,$v['title'],'');
        $SLinkedTitle = anchor($skandal_url,substr($v['title'],0,40).$dotUrl,'');
        $shortDescription = $v['short_desc'];
        $status = $v['status'];
        if($key == 0){
        ?>
        <div class="first-scandal">
            <a href="<?php echo $skandal_url;?>">
                <img id="first-scandal-image" src="<?php echo $imgDUrl;?>"/>
            </a>
            <div id="first-scandal-overlay"><?php echo $v['title'];?></div>
            <div style="font-weight: bold;">Para Pemain</div>
            <div class="" style="min-height: 48px;">
                <?php if(count($v['players'])>0) {
                    ?>
                    <ul class="ul-img-hr">
                        <?php foreach ($v['players'] as $key => $pro) { ?>
                            <?php 
                            if ($key === 5) {
                                echo 'more';
                                break;
                            };

                            if ($pro['profile_icon_url'] != 'None') {
                                $img_politik = $pro['profile_icon_url'];
                                if (!file_exists($img_politik)) {
                                    $img_politik = 'assets/images/icon/no-image-politisi.png';
                                }
                            } else {
                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                            }
                            ?>
                            <li class="">
                                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                    <img
                                        class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                        title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                        src="<?php echo base_url().$img_politik; ?>">
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } else { ?>
                    <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                <?php } ?>
            </div> 

            <div id="first-scandal-info">
                <div class="smallfont" style="float: left;width: 60px;margin-left: 0;">status</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                <div class="clear"></div>
                <div class="smallfont" style="float: left;width: 60px;margin-left: 0;">kejadian</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                <div class="clear"></div>
                <div class="smallfont" style="float: left;width: 60px;margin-left: 0;">dampak</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;word-wrap: break-word;width: 207px;"><?php echo $dampak;?></div>
                <div class="clear"></div>
            </div>
    </div>
    <img src="<?php echo @base_url('assets/images/'.$categoryD['image']);?>"/>
        <?php
        }else{
        ?>
    <div class="scandals-d-box">
        <div class="scandals-d-title">
            <?php echo $linkedTitle;?>
        </div>
        <div class="scandals-d-image-div">
            <?php echo $linkedImage;?>
        </div>
        <div class="scandals-d-desc-div smallfont">
            <?php echo $shortDescription;?>
        </div>

        <div class="smallfont">Para Pemain:</div>  
        <div class="" style="min-height: 48px;">
            <?php if(count($v['players'])>0) {
                ?>
                <ul class="ul-img-hr">
                    <?php foreach ($v['players'] as $key => $pro) { ?>
                        <?php 
                        if ($key === 5) {
                            echo 'more';
                            break;
                        };

                        if ($pro['profile_icon_url'] != 'None') {
                            $img_politik = $pro['profile_icon_url'];
                            if (!file_exists($img_politik)) {
                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                            }
                        } else {
                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                        }
                        ?>
                        <li class="">
                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                <img
                                    class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                    title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                    src="<?php echo base_url().$img_politik; ?>">
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
            <?php } ?>
        </div> 

        <div class="clear"></div>
        <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">status</div>
        <div class="smallfont" style="float: left;width: 10px;">:</div>
        <div class="smallfont" style="float: left;"><?php echo $status;?></div>
        <div class="clear"></div>
        <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">kejadian</div>
        <div class="smallfont" style="float: left;width: 10px;">:</div>
        <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
        <div class="clear"></div>
        <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">dampak</div>
        <div class="smallfont" style="float: left;width: 10px;">:</div>
        <div class="smallfont" style="float: left;word-wrap: break-word;width: 215px;"><?php echo $dampak;?></div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
        <?php
        }
    }
}
?>