<?php
$loopPhotos = $skandal['photos'];
$skandal_url = base_url() . 'scandal/index/'.$skandal['scandal_id'].'-'.urltitle($skandal['title']);
$skandal_photos = (count($loopPhotos) > 0) ? $loopPhotos[0]['large_url'] : base_url('assets/images/img-default-scandal.png');
$skandal_photos = base_url('assets/images/img-default-scandal.jpg');
$headx_photos = null; // (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
$headm_photos = null; //(count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';
$status = ucwords($skandal['status']);
$dampak = $skandal['uang'];
$scandalTitle = $skandal['title'];;
$scandalDescription = nl2br($skandal['description']);
$bg1 = 'rgba(244,240,239,1.0)';
$bg2 = 'rgba(229,225,224,1.0)';
foreach($loopPhotos as $kes => $item){
    if(key_exists('type', $item)){
        if(intval($item['type']) == 1){
            $headx_photos = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                $headx_photos = $item['original_url'];
            }

            $headm_photos = $item['thumb_url'];
            break;
        }
    }
}

?>

<div class="container">
    <div class="row">
        <?php 
        // $this->load->view('template/tpl_sub_header'); // old look
        $this->load->view('template/skandal/tpl_scandal_sub_header'); // new look
        ?>
    </div>    
    <div class="row" id="scandal_row">
        <div id="left_content">
            <?php // LEFT CONTENT HERE ?>
            <div id="scandal_title"><?php echo $scandalTitle;?></div>

            <div id="scandal_photo_carousel">
                <div id="myCarousel" data-interval="3000" data-pause="hover" class="carousel-head carousel slide">

                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <?php
                        $_active = ''; $k = 0;
                        foreach($loopPhotos as $kes => $item){
                            if($k == 0 ) { $_active = 'active'; } else { $_active = ''; }
                        ?>
                        <div class="<?php echo $_active; ?> item skandal-carousel-image">
                            <?php
                            // echo $item['original_url'];
                            $img_uri_car = $item['large_url'];
                            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                                $img_uri_car = $item['original_url'];
                            };?>
                            <img src="<?php echo base_url($img_uri_car);?>">
                        </div>
                        <?php $k++; } ?>
                    </div>
                    <div class="score-place score-place-overlay score" data-id="<?php echo $skandal['content_id']; ?>" ></div>
                    <div class="row-fluid skandal-carousel-control-container" style="">
                        <div class="skandal-carousel-control-thumbs">
                            <!-- Carousel thumb -->
                            <ol class="skandal-carousel-indicators">
                                <?php
                                $set_active =''; 
                                $i= 0; 
                                foreach($loopPhotos as $kem => $item){
                                    $set_active = $i == 0 ? 'active' : '';
                                ?>
                                <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $set_active; ?>">
                                    <img class="img-polaroid img-polaroid-scandal" src="<?php echo base_url().$item['thumb_url'] ;?>">
                                </li>
                                <?php 
                                    $i++; 
                                } ?>
                            </ol>
                        </div>
                    </div>
                    <div class="skandal-detail-under-slider">
                        <hr class="hr-white">
                        <span><strong>&nbsp; Politisi Terkait</strong></span>
                        <div class="row-fluid">
                            <div class="span12">
                            <?php
                            if (count($skandal['players']) > 0){
                                echo '<ul class="ul-img-hr">';
                                foreach ($skandal['players'] as $key => $pro){
                                    if( $pro['profile_icon_url'] != 'None'){
                                        $img_politik = $pro['profile_icon_url'];
                                        if(!file_exists($img_politik)){
                                                        $img_politik = 'assets/images/icon/no-image-politisi.png';
                                        }
                                    }
                            ?>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                        <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="" src="<?php echo base_url().$img_politik; ?>">
                                    </a>
                                </li>
                                <?php
                                }
                                echo '</ul>';
                            } else{
                                echo '<span>Tidak ada politisi terkait</span>';
                            };
                            ?>
                            </div>
                        </div>
                        <hr class="hr-white">
                        <div id="info_scandal">
                            <div class="row-fluid">
                                <div class="span2 span2-pd-left">
                                    <strong>Status</strong>
                                </div>
                                <div class="span10 span10-pd-right">
                                    <?php echo ucwords($status); ?>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2 span2-pd-left">
                                    <strong>Kejadian</strong>
                                </div>
                                <div class="span10 span10-pd-right">
                                    <?php echo mdate('%d %M %Y', strtotime($skandal['date'])); ?>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2 span2-pd-left">
                                    <strong>Dampak</strong>
                                </div>
                                <div class="span10 span10-pd-right">
                                    <?php echo $dampak; ?>
                                </div>

                            </div>
                        </div>                            

                    </div>
                            
                </div>
            </div>

            <div style="clear: both;"></div>
            <div id="scandal_accordion">

                <div class="accordion" id="accordion2">
                    <div class="accordion-group">
                        <div class="accordion-heading odd1">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
                            DETIL
                            </a>
                        </div>
                        <div id="collapse1" class="accordion-body collapse in">
                            <div class="accordion-inner">
                                <?php echo $scandalDescription;?>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-group">
                        <div class="accordion-heading even1">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2">
                                KRONOLOGIS
                            </a>
                        </div>
                        <div id="collapse2" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <div class="skandal-kronologi skandal-slim">
                                <?php
                                $dt_sebelum = '';
                                $kronologi_id = '';
                                foreach($skandal['kronologi'] as $indeks => $krono){
                                if($dt_sebelum != $krono['date']){
                                    $kronologi_id = $krono['scandal_id'];
                                }
                                ?>
                                <?php if($dt_sebelum == $krono['date']){ ?>
                                    <!--                                <div id="" class="--><?php //echo $kronologi_id; ?><!----><?php //echo ($dt_sebelum == $krono['date']) ? ' collapse' : '';?><!--">-->
                                    <?php } ?>
                                <div class="row-fluid ">
                                    <div class="span3">
                                        <?php
                                        if($dt_sebelum != $krono['date']){

                                            ?>
                                            <h4><?php echo mdate('%d %M %Y', strtotime($krono['date'])); ?> </h4>
                                            <?php } ?>
                                    </div>
                                    <div class="span9">
                                        <?php //if($dt_sebelum != $krono['date']){ ?>
                                        <i class="icon-plus pull-right mycolaps" data-toggle="collapse" data-target="#<?php echo $krono['scandal_id']; ?>"></i>
                                        <?php //} ?>
                                        <div class="">
                                            <p><strong><?php echo $krono['title']; ?></strong></p>
                                            <p id="<?php echo $krono['scandal_id']; ?>" class="collapse"><?php echo nl2br($krono['short_desc']); ?></p>
                                            <div class="div-line-small"></div>
                                        </div>


                                    </div>

                                </div>
                                <?php if($dt_sebelum == $krono['date']){ ?>
                                    <!--                                </div>-->
                                    <?php } ?>
                                <?php $dt_sebelum = $krono['date']; ?>
                                <?php } ?>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="accordion-group">
                        <div class="accordion-heading odd1">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
                                PARA PEMAIN
                            </a>
                        </div>
                        <div id="collapse3" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <div class='skandal-players '> <!-- skandal-slim -->

                                <?php if (count($skandal['players']) > 0){ ?>
                                <?php foreach ($skandal['players'] as $key => $pro) { ?>
                                    <?php if($key != 0){ ?>
                                    <div class="div-line-small"></div>
                                    <?php } ?>
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <a class="" href="<?php echo base_url().'aktor/profile/'.$pro['page_id']; ?>"><h3><?php echo $pro['page_name']; ?></h3></a>
                                            <p><?php echo nl2br($pro['description']); ?></p>
                                        </div>
                                        <div class="span3 skandal-player-detail">
                                            <?php
                                                if( $pro['profile_icon_url'] != 'None'){
                                                    $img_politik = $pro['profile_thumb_url'];
                                                } else {
                                                    $img_politik = 'assets/images/badge/no-image-politisi.png';
                                                }
                                            ?>
                                            <img class="thumbnail" alt="<?php echo $pro['page_name']; ?>" src="<?php echo base_url().$img_politik; ?>">
                                        </div>

                                    </div>

                                    <?php } ?>

                                <?php }else{ ?>
                                <p class="alert alert-info">Data belum tersedia</p>
                                <?php } ?>
                            </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="accordion-group">
                        <div class="accordion-heading even1">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4">
                                KONEKSI & ALIRAN DANA
                            </a>
                        </div>
                        <div id="collapse4" class="accordion-body collapse">
                            <div class="accordion-inner">
                                <div class='skandal-relations skandal-slim'>
                                <?php if (count($skandal['relations']) > 0){ ?>
                                <?php foreach ($skandal['relations'] as $key => $pro) { ?>
                                    <div class="div-line-small"></div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <a class="" href="<?php echo !empty($pro['profile_source_url']) ? $pro['profile_source_url'] : ''; ?>">
                                                <h3><?php echo !empty($pro['page_name_source']) ? $pro['page_name_source'] : ''; ?></h3></a>
                                            <p><strong><?php echo !empty($pro['keterangan']) ? $pro['keterangan'] : '' . ' : ' .$pro['relasi'] . ' kepada' ; ?></strong></p>
                                            <a class="" href="<?php echo !empty($pro['profile_target_url']) ? $pro['profile_target_url'] : ''; ?>">
                                                <h3><?php echo !empty($pro['page_name_target']) ? $pro['page_name_target'] : ''; ?></h3>
                                            </a>
                                            <p><?php echo nl2br($pro['deskripsi']); ?></p>
                                        </div>
                                        <div class="span3 skandal-player-detail">
                                            <img title="<?php echo !empty($pro['page_name_source']) ? $pro['page_name_source'] : '';?>" 
                                            alt="<?php echo !empty($pro['page_name_source']) ? $pro['page_name_source'] : ''; ?>" 
                                            src="<?php echo !empty($pro['profile_thumb_url_source']) ? $pro['profile_thumb_url_source'] : ''; ?>">
                                            <a class="" href="<?php echo !empty($pro['profile_source_url']) ? $pro['profile_source_url'] : '#'; ?>">
                                                <?php echo !empty($pro['page_name_source']) ? $pro['page_name_source'] : ''; ?>
                                            </a>
                                        </div>
                                        <div class="span3 skandal-player-detail">
                                            <img title="<?php echo !empty($pro['page_name_target']) ? $pro['page_name_target'] : ''; ?>" 
                                            alt="<?php echo !empty($pro['page_name_target']) ? $pro['page_name_target'] : ''; ?>" 
                                            src="<?php echo !empty($pro['profile_thumb_url_target']) ? $pro['profile_thumb_url_target'] : ''; ?>">
                                            <a class="" href="<?php echo !empty($pro['profile_target_url']) ? $pro['profile_target_url'] : '#'; ?>">
                                                <?php echo !empty($pro['page_name_target']) ? $pro['page_name_target'] : ''; ?>
                                            </a>
                                        </div>

                                    </div>

                                    <?php } ?>

                                <?php }else{ ?>
                                <p class="alert alert-info">Data belum tersedia</p>
                                <?php } ?>

                            </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="accordion-group">
                        <div class="accordion-heading odd1">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5">
                                SKANDAL POWERMAP
                            </a>
                        </div>
                        <div id="collapse5" class="accordion-body collapse">
                            <div class="accordion-inner">
                                 <img src="<?=base_url();?>assets/images/powermap-under-b.gif">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row-fluid">

                    <h5>BERI KOMENTAR</h5>
                    <div class="row-fluid">
                        <div class="comment-side-left">
                            <div style="background:
                                    url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                            </div>
                        </div>

                        <div class="comment-side-right">
                            <div class="row-fluid">
                                <input id='comment_type' data-id="<?=$skandal['content_id'];?>" type="text" name="comment" class="media-input" >
                            </div>
                            <div class="row-fluid">
                                <div class="span12 text-right">
                                    <?php if(!is_array($this->member)){?>
                                    <span>Login untuk komentar</span>&nbsp;
                                    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
                                    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
                                    <?php }elseif(is_array($this->member)){?>
                                    <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="comment" data-page="1" data-id="<?=$skandal['content_id'];?>" class="row-fluid comment-container">

                </div>
                <div id="load_more_place" class="row-fluid komu-follow-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a data-page="1" data-id="<?=$skandal['content_id'];?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>
                <div id="div_line_bottom" class="div-line"></div>



            <?php // /LEFT CONTENT HERE ?>
        </div>

        <div id="right_content">
            <?php // RIGHT CONTENT HERE ?>
            <div id="scandal_terkait">
                <div class="home-title-section">
                    <div style="float: left;width: 240px;">
                        <div style="font-weight: 1px;font-size: 24px;font-weight: bold;margin-top: 10px;">SKANDAL LAINNYA</div>
                    </div>
                    <div style="float: left;margin-top: 19px;">
                        <div class="arrow-down"></div>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <?php foreach($skandal_terkait as $num=>$row){ ?>
                <?php
                    $dt['val'] = $row;
                    $dt['val']['tipe'] = 'small';
                ?>
                <?php $this->load->view('template/skandal/tpl_skandal_terkait', $dt); ?>
                <?php } ?>



            </div>

            <?php // /RIGHT CONTENT HERE ?>
        </div>
        
    </div>
</div>




<script>
    // $(function () {
    //     $('#myTab a').click(function (e) {
    //         e.preventDefault();
    //         $(this).tab('show');
    //     });

    //     $('.mycolaps').on('click', function(e){
    //         e.preventDefault();
    //         var dt_target = $(this).data('target');
    //         var cl = $(dt_target).attr('class');
    //         if(cl == 'collapse'){
    //             $(this).attr('class', 'icon-minus pull-right mycolaps collapsed');
    //         }else{
    //             $(this).attr('class', 'icon-plus pull-right mycolaps collapsed');
    //         }
    //     });

    // })

    // $(document).ready(function() {
    //   /*
    //   $('.carousel-control').live('click', function(e) {
    //       $(this).siblings('.carousel-inner').css( "overflow", "hidden" );
    //   });
    //   */

    //   $('.carousel-head').carousel({interval:false})

    // });
</script>

<?php $this->load->view('template/modal/report_spam_modal'); ?>