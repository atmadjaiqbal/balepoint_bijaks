<?php 
if(!empty($scandal)){
    foreach ($scandal as $key => $v) {
        $forArray = array('!', '#', '$','"',',');
        $_judul = str_replace($forArray, '', $v['title']);
        $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);

        $tmp = new DateTime($v['date']);
        $kejadian = $tmp->format('d M Y');
        $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
        $imgFUrl = $v['photos'][0]['large_url'];
        $imgTUrl = $v['photos'][0]['thumb_url'];
        $linkedImage = anchor($skandal_url,'<img class="scandals_a_image lazy" src="'.$imgFUrl.'" style="width: 100%;max-height: 151px;"/>','');
        $linkedTImage = anchor($skandal_url,'<img class="scandals_a_image lazy belseimage" src="'.$imgTUrl.'" style="width: 70px;height: 40px;" />','');

        $dotUrl = anchor($skandal_url,'...');
        $linkedTitle = anchor($skandal_url,$v['title'],'');
        $SLinkedTitle = anchor($skandal_url,substr($v['title'],0,40).$dotUrl,'');
        $shortDescription = $v['short_desc'];
        $status = $v['status'];
        if($key == 0){
    ?>
    <div class="scandal_f0_title" >
    <?php echo $linkedTitle;?>
    </div>
    <div class="scandal_f0_image" >
    <?php echo $linkedImage;?>
    </div>
    <div class="scandal_f0_players" style="margin-left: 13px;">
        <div class="smallfont">Para Pemain:</div>  
            <div class="" style="min-height: 48px;">
                <?php if(count($v['players'])>0) {
                    ?>
                <ul class="ul-img-hr">
                    <?php foreach ($v['players'] as $key => $pro) { ?>
                        <?php 
                        if ($key === 5) {
                            echo 'more';
                            break;
                        };

                        if ($pro['profile_icon_url'] != 'None') {
                            $img_politik = $pro['profile_icon_url'];
                            if (!file_exists($img_politik)) {
                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                            }
                        } else {
                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                        }
                        ?>
                        <li class="">
                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                <img
                                    class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                    title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                    src="<?php echo base_url().$img_politik; ?>">
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            <?php } else { ?>
                <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
            <?php } ?>
        </div> 
    </div>
    <div class="scandal-f0-stats" style="margin-left: 13px;">
    <div class="clear"></div>
    <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">status</div>
    <div class="smallfont" style="float: left;width: 10px;">:</div>
    <div class="smallfont" style="float: left;"><?php echo $status;?></div>
    <div class="clear"></div>
    <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">kejadian</div>
    <div class="smallfont" style="float: left;width: 10px;">:</div>
    <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
    <div class="clear"></div>
    <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">dampak</div>
    <div class="smallfont" style="float: left;width: 10px;">:</div>
    <div class="smallfont" style="float: left;word-wrap: break-word;width: 215px;"><?php echo $dampak;?></div>
    <div class="clear"></div>    
    </div>
    <div class="scandals-e-other">

    <?php
        }else{
    ?>  
    <div class="scandals-f-other-image">
        <?php echo $linkedTImage;?>
    </div>
    <div class="scandals-f-other-title">
        <?php echo $linkedTitle;?>
    </div>
    <div class="clear"></div>
    <?php
        }
    }
    echo '<div class="clear"></div>';
    echo '</div>';
}
?>