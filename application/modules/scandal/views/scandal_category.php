<?php 
//echo '<pre>';print_r($scandals_top4[0]);echo '</pre>';
$forbiddenChars = array(' ','#',',','.','$','"',"'");
$titleScandal0 = !empty($scandals_top4[0]['title']) ? html_entity_decode(strtoupper($scandals_top4[0]['title'])) : '';
$photoScandal0 = !empty($scandals_top4[0]['photos'][0]['large_url']) ? $scandals_top4[0]['photos'][0]['large_url'] : '';
if($photoScandal0 != ''){
    $photoScandal0 = '<img src="'.$photoScandal0.'"/>';
}else{
    $photoScandal0 = '';
}

$descriptionScandal0 = !empty($scandals_top4[0]['description']) ? substr($scandals_top4[0]['description'],0,350).'...' : '';
$playersScandal0 = !empty($scandals_top4[0]['players']) ? $scandals_top4[0]['players'] : array();
$statusScandal0 = !empty($scandals_top4[0]['status']) ? $scandals_top4[0]['status'] : '';
$dampakScandal0 = !empty($scandals_top4[0]['uang']) ? $scandals_top4[0]['uang'] : '';
if(!empty($scandals_top4[0]['date'])){
    $tmp = new DateTime($scandals_top4[0]['date']);
    $kejadianScandal0 = $tmp->format('d M Y');
}else{
    $kejadianScandal0 = '';
}
$urlScandal0 = base_url('scandal/index/'.$scandals_top4[0]['scandal_id'].'-'.strtolower(str_replace($forbiddenChars, '_', $titleScandal0)));
?>

<div class="container">
    <?php $this->load->view('template/skandal/tpl_scandal_sub_header_category',['featuredCategory'=>$featuredCategory]); ?>
</div>
<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid" id="row_first">
            <div class="span4">
                <div id="first-scandal">
                    <div id="first-scandal-content">
                        <div class="scandal-title1"><?php echo anchor($urlScandal0,$titleScandal0);?></div>
                        <div class="scandal-image1">
                            <a href="<?php echo $urlScandal0;?>">
                                <?php echo $photoScandal0;?>
                            <a>
                        </div>
                        <div class="scandal-description1">
                            <?php echo $descriptionScandal0;?>
                        </div>
                        <div class="scandal-players1">
                            <div style="font-weight: bold;color: rgba(255,0,9,1.0);">Para Pemain</div>
                            <div class="" style="min-height: 48px;">
                                <?php if(count($playersScandal0)>0) {
                                    ?>
                                    <ul class="ul-img-hr">
                                        <?php foreach ($playersScandal0 as $key => $pro) { ?>
                                            <?php 
                                            if ($key === 5) {
                                                echo 'more';
                                                break;
                                            };

                                            if ($pro['profile_icon_url'] != 'None') {
                                                $img_politik = $pro['profile_icon_url'];
                                                if (!file_exists($img_politik)) {
                                                    $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                }
                                            } else {
                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                            }
                                            ?>
                                            <li class="">
                                                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                    <img
                                                        class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                        title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                        src="<?php echo base_url().$img_politik; ?>">
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="scandal-status1">
                            <div class="row-fluid">
                                <div class="span3 lh1">Status</div>
                                <div class="span1 lh1">:</div>
                                <div class="span8 lh1"><?php echo $statusScandal0;?></div>
                            </div>    
                            <div class="row-fluid">
                                <div class="span3 lh1">Kejadian</div>
                                <div class="span1 lh1">:</div>
                                <div class="span8 lh1"><?php echo $kejadianScandal0;?></div>
                            </div>    
                            <div class="row-fluid">
                                <div class="span3 lh1">Dampak</div>
                                <div class="span1 lh1">:</div>
                                <div class="span8 lh1"><?php echo $dampakScandal0;?></div>
                            </div>

                        </div>
                        <div class="scandal-selengkapnya1">
                            <a href="<?php echo $urlScandal0;?>">
                                <img src="<?php echo base_url('assets/images/scandal/selengkapnya.png')?>"/>
                            </a>
                        </div>

                    </div>

                </div>
            </div>
            <div class="span8">
                <img src="<?php echo base_url($categoryDetails[$categoryId]['img']);?>" id="category-image"/>
            </div>
        </div>
        <div class="row-fluid" id="row-second">
        <?php 
        for($i=1;$i<=3;$i++){
            if(empty($scandals_top4[$i])) continue;

            $titleScandal0 = !empty($scandals_top4[$i]['title']) ? html_entity_decode(strtoupper($scandals_top4[$i]['title'])) : '';
            $photoScandal0 = !empty($scandals_top4[$i]['photos'][0]['large_url']) ? $scandals_top4[$i]['photos'][0]['large_url'] : '';
            if($photoScandal0 != ''){
                $photoScandal0 = '<img class="scandal-pic2" src="'.$photoScandal0.'"/>';
            }else{
                $photoScandal0 = '';
            }

            $descriptionScandal0 = !empty($scandals_top4[$i]['description']) ? substr($scandals_top4[$i]['description'],0,350).'...' : '';
            $playersScandal0 = !empty($scandals_top4[$i]['players']) ? $scandals_top4[$i]['players'] : array();
            $statusScandal0 = !empty($scandals_top4[$i]['status']) ? $scandals_top4[$i]['status'] : '';
            $dampakScandal0 = !empty($scandals_top4[$i]['uang']) ? $scandals_top4[$i]['uang'] : '';
            if(!empty($scandals_top4[$i]['date'])){
                $tmp = new DateTime($scandals_top4[$i]['date']);
                $kejadianScandal0 = $tmp->format('d M Y');
            }else{
                $kejadianScandal0 = '';
            }           
            $urlScandal0 = base_url('scandal/index/'.$scandals_top4[$i]['scandal_id'].'-'.strtolower(str_replace($forbiddenChars, '_', $titleScandal0)));

            $addedClass1 = ($i == 1 || $i == 2) ? 'bordered1' : '';
        ?>    

            <div class="span4">
                <div class="n-scandal">
                    <div class="n-scandal-content ">
                        <div class="scandal-title2"><?php echo anchor($urlScandal0,$titleScandal0);?></div>
                        <div class="scandal-image2">
                            <a href="<?php echo $urlScandal0;?>">
                                <?php echo $photoScandal0;?>
                            </a>
                        </div>
                        <div class="scandal-description2">
                            <?php echo $descriptionScandal0;?>
                        </div>
                        <div class="scandal-players1">
                            <div style="font-weight: bold;color: rgba(255,0,9,1.0);">Para Pemain</div>
                            <div class="" style="min-height: 48px;">
                                <?php if(count($playersScandal0)>0) {
                                    ?>
                                    <ul class="ul-img-hr">
                                        <?php foreach ($playersScandal0 as $key => $pro) { ?>
                                            <?php 
                                            if ($key === 5) {
                                                echo 'more';
                                                break;
                                            };

                                            if ($pro['profile_icon_url'] != 'None') {
                                                $img_politik = $pro['profile_icon_url'];
                                                if (!file_exists($img_politik)) {
                                                    $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                }
                                            } else {
                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                            }
                                            ?>
                                            <li class="">
                                                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                    <img
                                                        class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                        title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                        src="<?php echo base_url().$img_politik; ?>">
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="scandal-status2">
                            <div class="row-fluid">
                                <div class="span3 lh1">Status</div>
                                <div class="span1 lh1">:</div>
                                <div class="span8 lh1"><?php echo $statusScandal0;?></div>
                            </div>    
                            <div class="row-fluid">
                                <div class="span3 lh1">Kejadian</div>
                                <div class="span1 lh1">:</div>
                                <div class="span8 lh1"><?php echo $kejadianScandal0;?></div>
                            </div>    
                            <div class="row-fluid">
                                <div class="span3 lh1">Dampak</div>
                                <div class="span1 lh1">:</div>
                                <div class="span8 lh1"><?php echo $dampakScandal0;?></div>
                            </div>

                        </div>
                        <div class="scandal-selengkapnya1">
                            <a href="<?php echo $urlScandal0;?>">
                                <img src="<?php echo base_url('assets/images/scandal/selengkapnya.png')?>"/>
                            </a>
                        </div>

                    </div>

                </div>            
            </div>


        <?php
        }
        ?>
        </div>

        <div id="scandal-category-n">
            <div class="row-fluid" id="row-third">
                <div class="span8">
                    <?php 
                    if(!empty($scandals_rest_left)){
                        foreach($scandals_rest_left as $k=>$v){
                            $titleScandal = !empty($v['title']) ? html_entity_decode($v['title']) : '';
                            $photoScandal = !empty($v['photos'][0]['large_url']) ? $v['photos'][0]['large_url'] : '';
                            if($photoScandal != ''){
                                $photoScandal = '<img class="scandal-pic3" src="'.$photoScandal.'"/>';
                            }else{
                                $photoScandal = '';
                            }

                            $descriptionScandal = !empty($v['description']) ? substr($v['description'],0,350).'...' : '';
                            $playersScandal = !empty($v['players']) ? $v['players'] : array();
                            $statusScandal = !empty($v['status']) ? $v['status'] : '';
                            $dampakScandal = !empty($v['uang']) ? $v['uang'] : '';
                            if(!empty($v['date'])){
                                $tmp = new DateTime($v['date']);
                                $kejadianScandal = $tmp->format('d M Y');
                            }else{
                                $kejadianScandal = '';
                            }   
                            $urlScandal = base_url('scandal/index/'.$v['scandal_id'].'-'.strtolower(str_replace($forbiddenChars, '_', $titleScandal)));
                    ?>        
                    <div class="row-fluid bottombordered">
                        <div class="span6">
                            <div class="scandal-image3 text-center">
                                <?php echo anchor($urlScandal,$photoScandal);?>
                            </div>
                            <div class="scandal-status3">
                                <div class="row-fluid">
                                    <div class="span3 lh1">
                                        status
                                    </div>
                                    <div class="span1 lh1">
                                        :
                                    </div>
                                    <div class="span8 lh1">
                                        <?php echo $statusScandal;?>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3 lh1">
                                        kejadian
                                    </div>
                                    <div class="span1 lh1">
                                        :
                                    </div>
                                    <div class="span8 lh1">
                                        <?php echo $kejadianScandal;?>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span3 lh1">
                                        dampak
                                    </div>
                                    <div class="span1 lh1">
                                        :
                                    </div>
                                    <div class="span8 lh1">
                                        <?php echo $dampakScandal;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="scandal-title3">
                                <?php echo anchor($urlScandal,$titleScandal);?>
                            </div>
                            <div class="scandal-description3">
                                <?php echo $descriptionScandal;?>
                            </div>
                            <div class="scandal-players3">
                                <div style="font-weight: bold;color: rgba(0,0,0,1.0);">Para Pemain</div>
                                <div class="" style="min-height: 48px;">
                                    <?php if(count($playersScandal)>0) {
                                        ?>
                                        <ul class="ul-img-hr">
                                            <?php foreach ($playersScandal as $key => $pro) { ?>
                                                <?php 
                                                if ($key === 5) {
                                                    echo 'more';
                                                    break;
                                                };

                                                if ($pro['profile_icon_url'] != 'None') {
                                                    $img_politik = $pro['profile_icon_url'];
                                                    if (!file_exists($img_politik)) {
                                                        $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                    }
                                                } else {
                                                    $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                }
                                                ?>
                                                <li class="">
                                                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                        <img
                                                            class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                            title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                            src="<?php echo base_url().$img_politik; ?>">
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } else { ?>
                                        <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    }
                    ?>
                </div>
                <div class="span4">
                    <?php 
                    if(!empty($scandals_rest_right)){
                        foreach($scandals_rest_right as $k=>$v){
                            $titleScandal = !empty($v['title']) ? html_entity_decode($v['title']) : '';
                            $photoScandal = !empty($v['photos'][0]['large_url']) ? $v['photos'][0]['large_url'] : '';
                            if($photoScandal != ''){
                                $photoScandal = '<img class="scandal-pic4" src="'.$photoScandal.'"/>';
                            }else{
                                $photoScandal = '';
                            }

                            $descriptionScandal = !empty($v['description']) ? substr($v['description'],0,350).'...' : '';
                            $playersScandal = !empty($v['players']) ? $v['players'] : array();
                            $statusScandal = !empty($v['status']) ? $v['status'] : '';
                            $dampakScandal = !empty($v['uang']) ? $v['uang'] : '';
                            if(!empty($v['date'])){
                                $tmp = new DateTime($v['date']);
                                $kejadianScandal = $tmp->format('d M Y');
                            }else{
                                $kejadianScandal = '';
                            } 
                            $urlScandal = base_url('scandal/index/'.$v['scandal_id'].'-'.strtolower(str_replace($forbiddenChars, '_', $titleScandal)));
                    ?>
                    <div class="bottombordered" style="padding-bottom: 10px;">
                        <div class="scandal-title4">
                            <?php echo anchor($urlScandal,$titleScandal);?>
                        </div>
                        <div class="scandal-image4">
                            <?php echo anchor($urlScandal,$photoScandal);?>
                        </div>
                        <div class="scandal-status4">
                            <div class="row-fluid">
                                <div class="span3 lh1">
                                    status
                                </div>
                                <div class="span1 lh1">
                                    :
                                </div>
                                <div class="span8 lh1">
                                    <?php echo $statusScandal;?>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3 lh1">
                                    kejadian
                                </div>
                                <div class="span1 lh1">
                                    :
                                </div>
                                <div class="span8 lh1">
                                    <?php echo $kejadianScandal;?>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span3 lh1">
                                    dampak
                                </div>
                                <div class="span1 lh1">
                                    :
                                </div>
                                <div class="span8 lh1">
                                    <?php echo $dampakScandal;?>
                                </div>
                            </div>
                        </div>
                        <div class="scandal-players4">
                            <div style="font-weight: bold;color: rgba(0,0,0,1.0);">Para Pemain</div>
                            <div class="" style="min-height: 48px;">
                                <?php if(count($playersScandal)>0) {
                                    ?>
                                    <ul class="ul-img-hr">
                                        <?php foreach ($playersScandal as $key => $pro) { ?>
                                            <?php 
                                            if ($key === 5) {
                                                echo 'more';
                                                break;
                                            };

                                            if ($pro['profile_icon_url'] != 'None') {
                                                $img_politik = $pro['profile_icon_url'];
                                                if (!file_exists($img_politik)) {
                                                    $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                }
                                            } else {
                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                            }
                                            ?>
                                            <li class="">
                                                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                    <img
                                                        class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                        title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                        src="<?php echo base_url().$img_politik; ?>">
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    }
                    ?>

                </div>
            </div>
        </div>

        <div class="row-fluid" style="">
            <div class="span12 text-center">
                <a class="btn btn-large scandal-category-n-next" data-page="2" data-category-id="<?php echo $categoryId;?>" data-holder="scandal-category-n">LOAD MORE</a>
            </div>
        </div>


    </div>
</div>
<?php $this->load->view('template/modal/report_spam_modal'); ?>