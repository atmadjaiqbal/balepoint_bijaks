<?php 
$categoryId = $scandal['details']['id'];
?>

<?php 
$forArray = array('!', '#', '$','"',',');
$catScandals = $scandal['list'];	
if(count($catScandals)>0){
	// Process First Scandal
	$title = !empty($catScandals[0]['title']) ? $catScandals[0]['title'] : '';
	$scandalId0 = !empty($catScandals[0]['scandal_id']) ? $catScandals[0]['scandal_id'] : '';
	$slug = str_replace($forArray, '', $title);
	$scandalUrl = base_url() . 'scandal/index/' . $scandalId0 . '-' . urltitle($slug);
	$tmp = new DateTime($catScandals[0]['date']);
 	$status = !empty($catScandals[0]['status']) ? $catScandals[0]['status'] : '';
	$kejadian = $tmp->format('d M Y');
	$dampak = strlen($catScandals[0]['uang']) > 100 ? substr($catScandals[0]['uang'], 0, 97).'...' : $catScandals[0]['uang'];
	$largeImageUrl = !empty($catScandals[0]['photos'][0]['large_url']) ? $catScandals[0]['photos'][0]['large_url'] : '';
	$thumbImageUrl = !empty($catScandals[0]['photos'][0]['thumb_url']) ? $catScandals[0]['photos'][0]['thumb_url'] : '';
	$thumbLandscapeImageUrl = !empty($catScandals[0]['photos'][0]['thumb_landscape_url']) ? $catScandals[0]['photos'][0]['thumb_landscape_url'] : '';
	$players0 = $catScandals[0]['players'];
 //    $shortDescription = $v['short_desc'];
?>
	<div class="scandals0-title-div">
		<a href="<?php echo $scandalUrl;?>" class="scandals0-title">
			<?php echo $title;?>
		</a>
	</div>
	<div class="scandals0-image-div">
		<img src="<?php echo $largeImageUrl;?>" class="scandals0-image"/>
	</div>
	<div class="scandals0-players-div">
		<div style="font-weight: bold;">Para Pemain</div>
	    <div class="" style="min-height: 48px;">
	        <?php 
	        if(count($players0)>0) {
	        ?>
	        <ul class="ul-img-hr">
	            <?php foreach ($players0 as $key => $pro) { ?>
	                <?php 
	                if ($key === 5) {
	                    echo 'more';
	                    break;
	                };
	                if ($pro['profile_icon_url'] != 'None') {
	                    $img_politik = $pro['profile_icon_url'];
	                    if (!file_exists($img_politik)) {
	                        $img_politik = 'assets/images/icon/no-image-politisi.png';
	                    }
	                } else {
	                    $img_politik = 'assets/images/icon/no-image-politisi.png';
	                }
	                ?>
	                <li class="">
	                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
	                        <img
	                            class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
	                            title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
	                            src="<?php echo base_url().$img_politik; ?>" >
	                    </a>
	                </li>
	            <?php } ?>
	        </ul>
	        <?php } else { ?>
	            <div style="font-weight: bold;">Tidak ada politisi terkait</div>
	        <?php } ?>
	    </div>  
	</div>
	<div class="scandals0-status-div">
        <div class="clear"></div>
		<div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">status</div>
        <div class="smallfont" style="float: left;width: 10px;">:</div>
        <div class="smallfont" style="float: left;"><?php echo $status;?></div>
        <div class="clear"></div>
        <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">kejadian</div>
        <div class="smallfont" style="float: left;width: 10px;">:</div>
        <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
        <div class="clear"></div>
        <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">dampak</div>
        <div class="smallfont" style="float: left;width: 10px;">:</div>
        <div class="smallfont" style="float: left;word-wrap: break-word;width: 200px;"><?php echo $dampak;?></div>
        <div class="clear"></div>
	</div>
<?php	
};
?>

	<div class="scandalsn">
		<div class="scandalsn-inside">
<?php
if(count($catScandals)>1){
	foreach($catScandals as $ks=>$vs){
		$scandalId = !empty($vs['scandal_id']) ? $vs['scandal_id'] : '';
		if($scandalId == $scandalId0) continue;
		$title = !empty($vs['title']) ? $vs['title'] : '';
		$slug = str_replace($forArray, '', $title);
		$scandalUrl = base_url() . 'scandal/index/' . $scandalId . '-' . urltitle($slug);
		$tmp = new DateTime($vs['date']);
	 	$kejadian = $tmp->format('d M Y');
		$dampak = strlen($vs['uang']) > 100 ? substr($vs['uang'], 0, 97).'...' : $vs['uang'];
		$status = !empty($vs['status']) ? $vs['status'] : '';
		$largeImageUrl = !empty($vs['photos'][0]['large_url']) ? $vs['photos'][0]['large_url'] : '';
		$thumbImageUrl = !empty($vs['photos'][0]['thumb_url']) ? $vs['photos'][0]['thumb_url'] : '';
		$thumbLandscapeImageUrl = !empty($vs['photos'][0]['thumb_landscape_url']) ? $vs['photos'][0]['thumb_landscape_url'] : '';
		$players = $vs['players'];
?>
		<div class="clear"></div>
		<div class="icons-left-div">
			<img src="<?php echo $thumbImageUrl;?>" class="icons-left"/>
		</div>
		<div class="titles-right-div">
			<a href="<?php echo $scandalUrl;?>" class="titles-right"><?php echo $title;?></a>
		</div>
		<div class="clear"></div>
<?php
	}
}
?>		</div>
	</div>
  