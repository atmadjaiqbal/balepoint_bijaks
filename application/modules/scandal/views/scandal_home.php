<?php 
$mainScandalUrl = base_url('scandal/index/'.$mainScandal['scandal_id'].'-'.urltitle($mainScandal['title']));
$mainScandalTitle = !empty($mainScandal['title']) ? strtoupper($mainScandal['title']) : 'Untitled';
$arrowLeft = '<div class="arrow-left" style="float: left; width: 18px;"></div>';
if(!empty($mainScandal['uang'])){
    if(strlen($mainScandal['uang'])>54){
        $mainScandalSubtitle = $arrowLeft.strtoupper(substr($mainScandal['uang'], 0,51)).'...';        
    }else{
        $mainScandalSubtitle = $arrowLeft.strtoupper($mainScandal['uang']);
    }
}else{
    $mainScandalSubtitle = '';
}

if(!empty($mainScandal['description'])){
    if(strlen($mainScandal['description'])>500){
        $mainScandalDescription = substr($mainScandal['description'], 0, 497).'...';
    }else{
        $mainScandalDescription = $mainScandal['description'];
    }
}else{
    $mainScandalDescription = '';   
}

$mainScandalCategory = !empty($mainScandalCategory['scandal_category_name']) ? strtoupper('skandal '.$mainScandalCategory['scandal_category_name']) : '';


?>

<style type="text/css">
.scandals_a_box{
    /*border: solid 1px red;*/
    border-radius: 10px 10px 10px 10px;
    background-image: url('<?php echo base_url("assets/images/back-sby.png");?>');
    width: 593px;
    height: 188px;
    margin-bottom: 1px;
    margin-left: 27px;
    margin-top: 10px;
    padding: 10px;
}

#right-content-box-top{
    margin-left: 10px;
    background-image: url('<?php echo base_url('assets/images/bg-seksual.png');?>');
    background-repeat: repeat-y;
    border-bottom: solid 3px #faaecc;
    padding-left: 20px;
    min-height: 100px;
    /*border: solid 1px #cecece;*/
}



#scandals_e_title{
    background: url('<?php echo base_url('assets/images/'.$categoryE['image']);?>');
    height: 62px;
    margin-top: 10px;
}

#scandals_f_title{
    background: url('<?php echo base_url('assets/images/'.$categoryF['image']);?>');
    height: 62px;
    margin-top: 10px;
}


</style>

<div class="container">
    <div class="row">
        <?php 
        $this->load->view('template/skandal/tpl_scandal_sub_header'); // new look
        ?>
    </div>    
    <div class="row" id="scandal_row">
        <div id="left_content">
            <div id="left_content_title"><?php echo $mainScandalCategory;?></div>
            <?php // LEFT CONTENT HERE ?>
            <img src="<?php echo base_url('assets/images/sby.png');?>" id="image_main_scandal"/>

            <div id="main_scandal_box" style="position: relative;">
                <div id="main_scandal_title">
                    <?php echo $mainScandalTitle;?>
                </div>
                <div id="main_scandal_subtitle">
                    <?php echo $mainScandalSubtitle;?>
                </div>            
                <div id="main_scandal_description">
                    <?php echo $mainScandalDescription;?>
                </div>
                <div id="selengkapnya1">
                    <?php echo anchor($mainScandalUrl,'Selengkapnya','');?>
                </div>        
                <div id="like_box"> 

                </div>    
            </div>

            <?php // SCANDALS A?>
            <div id="scandals_a_spot">
                <?php 
                if(!empty($scandalsA)){
                    foreach ($scandalsA as $key => $v) { 
                    if($v['scandal_id'] == $mainScandalId) continue;                        
                    $tmp = new DateTime($v['date']);
                    $kejadian = $tmp->format('d M Y');
                    $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
                    $skandal_photos = (count($v['photos']) > 0) ? $v['photos'][count($v['photos']) - 1]['large_url'] : '';
                    $imgAUrl = !empty($v['photos'][0]['large_url']) ? $v['photos'][0]['large_url'] : '';

                    $forArray = array('!', '#', '$','"',',');
                    $_judul = str_replace($forArray, '', $v['title']);
                    $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);
                    ?>
                <div class="scandals_a_box">
                    <div class="scandal_image" id="scandal_image_<?php echo $v['scandal_id'];?>">
                        <?php 
                        echo anchor($skandal_url,'<img class="scandals_a_image lazy" src="'.$imgAUrl.'" />','');
                        ?>
                    </div>
                    <div class="scandals_a_info">
                        <div class="scandals_a_title">
                            <?php echo anchor($skandal_url,$v['title'],'');?>
                        </div>
                        <div class="scandals_a_description">
                            <?php echo $v['short_desc'];?>
                        </div>
                        <div class="scandals_a_smallinfo">
                            <div style="line-height: 1em;">status<span style="margin-left: 35px;"><?php echo ':&nbsp;'.$v['status'];?></span></div>
                            <div style="line-height: 1em;">kejadian<span style="margin-left: 20px;"><?php echo ':&nbsp;'.$kejadian;?></span></div>
                            <div style="line-height: 1em;">dampak<span style="margin-left: 23px;line-height: 1em;"><?php echo ':&nbsp;'.$dampak;?></span></div>
                        </div>
                    </div>
                    <div class="scandals_a_players">
                        <div style="font-weight: bold;">Para Pemain</div>
                        <div class="" style="min-height: 48px;">
                            <?php if(count($v['players'])>0) {
                                ?>
                                <ul class="ul-img-hr">
                                    <?php foreach ($v['players'] as $key => $pro) { ?>
                                        <?php 
                                        if ($key === 5) {
                                            echo 'more';
                                            break;
                                        };

                                        if ($pro['profile_icon_url'] != 'None') {
                                            $img_politik = $pro['profile_icon_url'];
                                            if (!file_exists($img_politik)) {
                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                            }
                                        } else {
                                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                                        }
                                        ?>
                                        <li class="">
                                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                <img
                                                    class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                    title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                    src="<?php echo base_url().$img_politik; ?>">
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } else { ?>
                                <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php 
                };
            };
            ?>
            </div>

            <div class="btn btn-mini" id="next_a">5 Berikutnya</div>
            <div>
                <img id="circular_progress_indicator2" src="<?php echo base_url('assets/images/image_825740.gif');?>" style="display:none;width: 32px;height: 32px;"/>
            </div>
            <div class="btn btn-mini" id="selengkapnya_a"><a href="<?php echo $allALink;?>" style="color: #000;">Selengkapnya</a></div>
            <div class="clear"></div>
            <div id="page_a_spot">
                <input type="hidden" id="hidden_a_page" value="2"/>
            </div>
            <?php // /SCANDALS A?>

            <div class="line1"></div>
            <?php // SCANDALS B?>
            <div id="scandals_b_area">
                <div id="scandals_b_cat_title">
                    <span class="scandal_title1">SKANDAL</span> <span class="scandal_name1"><?php echo strtoupper($categoryB['categoryName']);?></span>
                </div>
                <div id="scandals_b_spot">
                     <div class="scandals_b_box">
                    <?php 
                    $x = 1;
                    if(!empty($scandalsB) && $x){
                        foreach ($scandalsB as $key => $v) {
                            $forArray = array('!', '#', '$','"',',');
                            $_judul = str_replace($forArray, '', $v['title']);
                            $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);
                    
                            $tmp = new DateTime($v['date']);
                            $kejadian = $tmp->format('d M Y');
                            $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
                            // $imgBUrl = @base_url('public/upload/image/skandal/large/'.$v['photo']);
                            // $imgTUrl = @base_url('public/upload/image/skandal/large/'.$v['photo']);
                            $imgBUrl = $v['photos'][0]['large_url'];
                            $linkedImage = anchor($skandal_url,'<img class="scandals_a_image lazy" src="'.$imgBUrl.'" style="width: 100%;max-height: 151px;"/>','');
                            $linkedTImage = anchor($skandal_url,'<img class="scandals_a_image lazy belseimage" src="'.$imgBUrl.'" />','');

                            $dotUrl = anchor($skandal_url,'...');
                            $linkedTitle = anchor($skandal_url,$v['title'],'');
                            $SLinkedTitle = anchor($skandal_url,substr($v['title'],0,40).$dotUrl,'');
                            $shortDescription = $v['short_desc'];
                            $status = $v['status'];
                            if($key == 0){
                            ?>
                            <div class="scandals0">
                                <div class="scandal0Image">
                                    <?php echo $linkedImage;?>
                                </div>
                                <div class="scandal0Info" style="line-height: 18px;">
                                    <div class="scandal0Title"><?php echo $linkedTitle;?></div>
                                    <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">status</div>
                                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                                    <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                                    <div class="clear"></div>
                                    <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">kejadian</div>
                                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                                    <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                                    <div class="clear"></div>
                                    <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">dampak</div>
                                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                                    <div class="smallfont" style="float: left;word-wrap: break-word;width: 226px;"><?php echo $dampak;?></div>
                                    <div class="clear"></div>
                                    <div class="playersthumb" style="margin-left: 9px;">
                                        <div style="font-weight: bold;">Para Pemain</div>
                                        <div class="" style="min-height: 48px;">
                                            <?php if(count($v['players'])>0) {
                                                ?>
                                                <ul class="ul-img-hr">
                                                    <?php foreach ($v['players'] as $key => $pro) { ?>
                                                        <?php 
                                                        if ($key === 5) {
                                                            echo 'more';
                                                            break;
                                                        };

                                                        if ($pro['profile_icon_url'] != 'None') {
                                                            $img_politik = $pro['profile_icon_url'];
                                                            if (!file_exists($img_politik)) {
                                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                            }
                                                        } else {
                                                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                        }
                                                        ?>
                                                        <li class="">
                                                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                                <img
                                                                    class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                                    title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                                    src="<?php echo base_url().$img_politik; ?>" >
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } else { ?>
                                                <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php    
                            }else{
                            ?>
                            <div class="scandalsBElse">
                                <?php echo $linkedTImage;?> 
                                <div style="font-weight: bold;"><?php echo $SLinkedTitle;?></div>
                                <div class="clear"></div>
                                <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">status</div>
                                <div class="smallfont" style="float: left;width: 4px;">:</div>
                                <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                                <div class="clear"></div>
                                <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">kejadian</div>
                                <div class="smallfont" style="float: left;width: 4px;">:</div>
                                <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                                <div class="clear"></div>
                                <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">dampak</div>
                                <div class="smallfont" style="float: left;width: 4px;">:</div>
                                <div class="smallfont" style="float: left;word-wrap: break-word;width: 88px;"><?php echo substr($dampak,0,50).$dotUrl;?></div>
                                <div class="clear"></div>
                            </div>
                            <?php    
                            }
            

                        } 
                    };?>
                    </div>
                    <div class="clear"></div>
                </div>  

                <div class="btn btn-mini" id="next_b">5 Berikutnya</div>
                <div>
                    <img id="circular_progress_indicator5" src="<?php echo base_url('assets/images/image_825740.gif');?>" style="display:none;width: 32px;height: 32px;"/>
                </div>
                <div class="btn btn-mini" id="selengkapnya_b"><a href="<?php echo $allBLink;?>" style="color: #000;">Selengkapnya</a></div>
                <div class="clear"></div>
                <div id="page_b_spot">
                    <input type="hidden" id="hidden_b_page" value="2"/>
                </div>                  
            </div>
            
            <?php // /SCANDALS B?>
            
            <div class="line1"></div>
            <?php // SCANDALS C?>
            <div id="scandals_c_area">
                <div id="scandals_c_cat_title">
                    <span class="scandal_title1">SKANDAL</span> <span class="scandal_name1"><?php echo strtoupper($categoryC['categoryName']);?></span>
                </div>
                <div id="scandals_c_spot">
                    <div class="scandals_c_box">
                <?php 
                    if(!empty($scandalsC) && $x){
                        foreach ($scandalsC as $key => $v) {
                            $forArray = array('!', '#', '$','"',',');
                            $_judul = str_replace($forArray, '', $v['title']);
                            $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);
                    
                            $tmp = new DateTime($v['date']);
                            $kejadian = $tmp->format('d M Y');
                            $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
                            // $imgBUrl = @base_url('public/upload/image/skandal/large/'.$v['photo']);
                            // $imgTUrl = @base_url('public/upload/image/skandal/large/'.$v['photo']);
                            $imgBUrl = $v['photos'][0]['large_url'];
                            $linkedImage = anchor($skandal_url,'<img class="scandals_a_image lazy" src="'.$imgBUrl.'" style="width: 100%;max-height: 151px;"/>','');
                            $linkedTImage = anchor($skandal_url,'<img class="scandals_a_image lazy belseimage" src="'.$imgBUrl.'" />','');

                            $dotUrl = anchor($skandal_url,'...');
                            $linkedTitle = anchor($skandal_url,$v['title'],'');
                            $SLinkedTitle = anchor($skandal_url,substr($v['title'],0,40).$dotUrl,'');
                            $shortDescription = $v['short_desc'];
                            $status = $v['status'];
                            if($key == 0){
                            ?>
                            <div class="scandals0">
                                <div class="scandal0Image">
                                    <?php echo $linkedImage;?>
                                </div>
                                <div class="scandal0Info">
                                    <div class="scandal0Title"><?php echo $linkedTitle;?></div>
                                    <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">status</div>
                                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                                    <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                                    <div class="clear"></div>
                                    <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">kejadian</div>
                                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                                    <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                                    <div class="clear"></div>
                                    <div class="smallfont" style="float: left;width: 60px;margin-left: 10px;">dampak</div>
                                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                                    <div class="smallfont" style="float: left;word-wrap: break-word;width: 226px;"><?php echo $dampak;?></div>
                                    <div class="clear"></div>
                                    <div class="playersthumb" style="margin-left: 9px;">
                                        <div style="font-weight: bold;">Para Pemain</div>
                                        <div class="" style="min-height: 48px;">
                                            <?php if(count($v['players'])>0) {
                                                ?>
                                                <ul class="ul-img-hr">
                                                    <?php foreach ($v['players'] as $key => $pro) { ?>
                                                        <?php 
                                                        if ($key === 5) {
                                                            echo 'more';
                                                            break;
                                                        };

                                                        if ($pro['profile_icon_url'] != 'None') {
                                                            $img_politik = $pro['profile_icon_url'];
                                                            if (!file_exists($img_politik)) {
                                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                            }
                                                        } else {
                                                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                        }
                                                        ?>
                                                        <li class="">
                                                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                                <img
                                                                    class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                                    title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                                    src="<?php echo base_url().$img_politik; ?>" >
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } else { ?>
                                                <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                                            <?php } ?>
                                        </div>                                        
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <?php    
                            }else{
                            ?>    
                            <div class="scandalsCElse">
                                <?php echo $linkedTImage;?> 
                                <div style="font-weight: bold;"><?php echo $SLinkedTitle;?></div>
                                <div class="clear"></div>
                                <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">status</div>
                                <div class="smallfont" style="float: left;width: 4px;">:</div>
                                <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                                <div class="clear"></div>
                                <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">kejadian</div>
                                <div class="smallfont" style="float: left;width: 4px;">:</div>
                                <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                                <div class="clear"></div>
                                <div class="smallfont" style="float: left;width: 40px;margin-left: 0;">dampak</div>
                                <div class="smallfont" style="float: left;width: 4px;">:</div>
                                <div class="smallfont" style="float: left;word-wrap: break-word;width: 88px;"><?php echo substr($dampak,0,50).$dotUrl;?></div>
                                <div class="clear"></div>
                            </div>
                            <?php 
                            }
                        }
                    }    
                ?>            
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="btn btn-mini" id="next_c">5 Berikutnya</div>
                <div>
                    <img id="circular_progress_indicator6" src="<?php echo base_url('assets/images/image_825740.gif');?>" style="display:none;width: 32px;height: 32px;"/>
                </div>
                <div class="btn btn-mini" id="selengkapnya_c"><a href="<?php echo $allCLink;?>" style="color: #000;">Selengkapnya</a></div>
                <div class="clear"></div>
                <div id="page_c_spot">
                    <input type="hidden" id="hidden_c_page" value="2"/>
                </div>
    
            </div>    
            
            <?php // /SCANDALS C?>

            <?php // /LEFT CONTENT HERE ?>
        </div>

        <div id="right_content">
            <?php // RIGHT CONTENT HERE ?>
            <div id="right-content-box-top">
                <div id="scandals_d_spot">
                <?php 
                if(!empty($scandalsD)){
                    foreach ($scandalsD as $key => $v) {
                        $forArray = array('!', '#', '$','"',',');
                        $_judul = str_replace($forArray, '', $v['title']);
                        $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);
                
                        $tmp = new DateTime($v['date']);
                        $kejadian = $tmp->format('d M Y');
                        $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
                        $imgDUrl = !empty($v['photos'][0]['large_url']) ? $v['photos'][0]['large_url'] : '';
                        $imgTUrl = !empty($v['photos'][0]['thumb_landscape_url']) ? $v['photos'][0]['thumb_landscape_url'] : '';
                        $linkedImage = anchor($skandal_url,'<img class="scandals_d_image lazy" src="'.$imgDUrl.'" />','');
                        $linkedTImage = anchor($skandal_url,'<img class="scandals_d_image lazy belseimage" src="'.$imgDUrl.'" />','');

                        $dotUrl = anchor($skandal_url,'...');
                        $linkedTitle = anchor($skandal_url,$v['title'],'');
                        $SLinkedTitle = anchor($skandal_url,substr($v['title'],0,40).$dotUrl,'');
                        $shortDescription = $v['short_desc'];
                        $status = $v['status'];
                        if($key == 0){
                        ?>
                        <div class="first-scandal">
                            <a href="<?php echo $skandal_url;?>">
                                <?php if($imgDUrl != ''){?>
                                <img id="first-scandal-image" src="<?php echo $imgDUrl;?>"/>
                                <?php };?>
                            </a>
                            <div id="first-scandal-overlay"><?php echo $v['title'];?></div>
                            <div style="font-weight: bold;">Para Pemain</div>
                            <div class="" style="min-height: 48px;">
                                <?php if(count($v['players'])>0) {
                                    ?>
                                    <ul class="ul-img-hr">
                                        <?php foreach ($v['players'] as $key => $pro) { ?>
                                            <?php 
                                            if ($key === 5) {
                                                echo 'more';
                                                break;
                                            };

                                            if ($pro['profile_icon_url'] != 'None') {
                                                $img_politik = $pro['profile_icon_url'];
                                                if (!file_exists($img_politik)) {
                                                    $img_politik = 'assets/images/icon/no-image-politisi.png';
                                                }
                                            } else {
                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                            }
                                            ?>
                                            <li class="">
                                                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                    <img
                                                        class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                        title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                        src="<?php echo base_url().$img_politik; ?>">
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                                <?php } ?>
                            </div> 

                            <div id="first-scandal-info">
                                <div class="smallfont" style="float: left;width: 60px;margin-left: 0;">status</div>
                                <div class="smallfont" style="float: left;width: 10px;">:</div>
                                <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                                <div class="clear"></div>
                                <div class="smallfont" style="float: left;width: 60px;margin-left: 0;">kejadian</div>
                                <div class="smallfont" style="float: left;width: 10px;">:</div>
                                <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                                <div class="clear"></div>
                                <div class="smallfont" style="float: left;width: 60px;margin-left: 0;">dampak</div>
                                <div class="smallfont" style="float: left;width: 10px;">:</div>
                                <div class="smallfont" style="float: left;word-wrap: break-word;width: 207px;"><?php echo $dampak;?></div>
                                <div class="clear"></div>
                            </div>
                    </div>
                    <img src="<?php echo base_url('assets/images/'.$categoryD['image']);?>"/>
                        <?php
                        }else{
                        ?>
                    <div class="scandals-d-box">
                        <div class="scandals-d-title">
                            <?php echo $linkedTitle;?>
                        </div>
                        <div class="scandals-d-image-div">
                            <?php echo $linkedImage;?>
                        </div>
                        <div class="scandals-d-desc-div smallfont">
                            <?php echo $shortDescription;?>
                        </div>

                        <div class="smallfont">Para Pemain:</div>  
                        <div class="" style="min-height: 48px;">
                            <?php if(count($v['players'])>0) {
                                ?>
                                <ul class="ul-img-hr">
                                    <?php foreach ($v['players'] as $key => $pro) { ?>
                                        <?php 
                                        if ($key === 5) {
                                            echo 'more';
                                            break;
                                        };

                                        if ($pro['profile_icon_url'] != 'None') {
                                            $img_politik = $pro['profile_icon_url'];
                                            if (!file_exists($img_politik)) {
                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                            }
                                        } else {
                                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                                        }
                                        ?>
                                        <li class="">
                                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                <img
                                                    class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                    title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                    src="<?php echo base_url().$img_politik; ?>">
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } else { ?>
                                <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                            <?php } ?>
                        </div> 

                        <div class="clear"></div>
                        <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">status</div>
                        <div class="smallfont" style="float: left;width: 10px;">:</div>
                        <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                        <div class="clear"></div>
                        <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">kejadian</div>
                        <div class="smallfont" style="float: left;width: 10px;">:</div>
                        <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                        <div class="clear"></div>
                        <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">dampak</div>
                        <div class="smallfont" style="float: left;width: 10px;">:</div>
                        <div class="smallfont" style="float: left;word-wrap: break-word;width: 215px;"><?php echo $dampak;?></div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                        <?php
                        }
                    }
                }
                ?>
                </div>
            </div>
            <div class="btn btn-mini" id="next_d">5 Berikutnya</div>
            <div>
                <img id="circular_progress_indicator7" src="<?php echo base_url('assets/images/image_825740.gif');?>" style="display:none;width: 32px;height: 32px;"/>
            </div>
            <div class="btn btn-mini" id="selengkapnya_d"><a href="<?php echo $allDLink;?>" style="color: #000;">Selengkapnya</a></div>
            <div class="clear"></div>
            <div id="page_d_spot">
                <input type="hidden" id="hidden_d_page" value="2"/>
            </div>

            <div id="scandals_e_title"></div>
            <div class="arrow-down" style="margin-top: -27px;margin-left: 244px;"></div>
            <div id="scandals_e_spot">
                <?php 
                if(!empty($scandalsE)){
                    foreach ($scandalsE as $key => $v) {
                        $forArray = array('!', '#', '$','"',',');
                        $_judul = str_replace($forArray, '', $v['title']);
                        $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);
                
                        $tmp = new DateTime($v['date']);
                        $kejadian = $tmp->format('d M Y');
                        $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
                        $imgDUrl = $v['photos'][0]['large_url'];
                        $imgTUrl = $v['photos'][0]['thumb_url'];
                        $linkedImage = anchor($skandal_url,'<img class="scandals_a_image lazy" src="'.$imgDUrl.'" style="width: 100%;max-height: 151px;"/>','');
                        $linkedTImage = anchor($skandal_url,'<img class="scandals_a_image lazy belseimage" src="'.$imgTUrl.'" style="width: 70px;height: 40px;" />','');

                        $dotUrl = anchor($skandal_url,'...');
                        $linkedTitle = anchor($skandal_url,$v['title'],'');
                        $SLinkedTitle = anchor($skandal_url,substr($v['title'],0,40).$dotUrl,'');
                        $shortDescription = $v['short_desc'];
                        $status = $v['status'];
                        if($key == 0){
                    ?>
                    <div class="scandal_e0_title" >
                    <?php echo $linkedTitle;?>
                    </div>
                    <div class="scandal_e0_image" >
                    <?php echo $linkedImage;?>
                    </div>
                    <div class="scandal_e0_players" style="margin-left: 13px;">
                        <div class="smallfont">Para Pemain:</div>  
                            <div class="" style="min-height: 48px;">
                                <?php if(count($v['players'])>0) {
                                    ?>
                                <ul class="ul-img-hr">
                                    <?php foreach ($v['players'] as $key => $pro) { ?>
                                        <?php 
                                        if ($key === 5) {
                                            echo 'more';
                                            break;
                                        };

                                        if ($pro['profile_icon_url'] != 'None') {
                                            $img_politik = $pro['profile_icon_url'];
                                            if (!file_exists($img_politik)) {
                                                $img_politik = 'assets/images/icon/no-image-politisi.png';
                                            }
                                        } else {
                                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                                        }
                                        ?>
                                        <li class="">
                                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                <img
                                                    class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                    title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                    src="<?php echo base_url().$img_politik; ?>">
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } else { ?>
                                <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                            <?php } ?>
                        </div> 
                    </div>
                    <div class="scandal-e0-stats" style="margin-left: 13px;">
                    <div class="clear"></div>
                    <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">status</div>
                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                    <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                    <div class="clear"></div>
                    <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">kejadian</div>
                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                    <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                    <div class="clear"></div>
                    <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">dampak</div>
                    <div class="smallfont" style="float: left;width: 10px;">:</div>
                    <div class="smallfont" style="float: left;word-wrap: break-word;width: 215px;"><?php echo $dampak;?></div>
                    <div class="clear"></div>    
                    </div>
                    <div class="scandals-e-other">

                    <?php
                        }else{
                    ?>  
                    <div class="scandals-e-other-image">
                        <?php echo $linkedTImage;?>
                    </div>
                    <div class="scandals-e-other-title">
                        <?php echo $linkedTitle;?>
                    </div>
                    <div class="clear"></div>
                    <?php
                        }
                    }
                    echo '<div class="clear"></div>';
                    echo '</div>';
                }
                ?>
                
            </div>
            <div class="btn btn-mini" id="next_e">5 Berikutnya</div>
            <div>
                <img id="circular_progress_indicator8" src="<?php echo base_url('assets/images/image_825740.gif');?>" style="display:none;width: 32px;height: 32px;"/>
            </div>
            <div class="btn btn-mini" id="selengkapnya_e"><a href="<?php echo $allELink;?>" style="color: #000;">Selengkapnya</a></div>
            <div class="clear"></div>
            <div id="page_e_spot">
                <input type="hidden" id="hidden_e_page" value="2"/>
            </div>

            <div class="line2"></div>

            <div id="scandals_f_title"></div>
            <div class="arrow-down" style="margin-top: -19px;margin-left: 244px;"></div>
            <div id="scandals_f_spot">
            
            <?php 
            if(!empty($scandalsF)){
                foreach ($scandalsF as $key => $v) {
                    $forArray = array('!', '#', '$','"',',');
                    $_judul = str_replace($forArray, '', $v['title']);
                    $skandal_url = base_url() . 'scandal/index/' . $v['scandal_id'] . '-' . urltitle($_judul);

                    $tmp = new DateTime($v['date']);
                    $kejadian = $tmp->format('d M Y');
                    $dampak = strlen($v['uang']) > 140 ? substr($v['uang'], 0, 137).'...' : $v['uang'];
                    $imgFUrl = $v['photos'][0]['large_url'];
                    $imgTUrl = $v['photos'][0]['thumb_url'];
                    $linkedImage = anchor($skandal_url,'<img class="scandals_a_image lazy" src="'.$imgFUrl.'" style="width: 100%;max-height: 151px;"/>','');
                    $linkedTImage = anchor($skandal_url,'<img class="scandals_a_image lazy belseimage" src="'.$imgTUrl.'" style="width: 70px;height: 40px;" />','');

                    $dotUrl = anchor($skandal_url,'...');
                    $linkedTitle = anchor($skandal_url,$v['title'],'');
                    $SLinkedTitle = anchor($skandal_url,substr($v['title'],0,40).$dotUrl,'');
                    $shortDescription = $v['short_desc'];
                    $status = $v['status'];
                    if($key == 0){
                ?>
                <div class="scandal_f0_title" >
                <?php echo $linkedTitle;?>
                </div>
                <div class="scandal_f0_image" >
                <?php echo $linkedImage;?>
                </div>
                <div class="scandal_f0_players" style="margin-left: 13px;">
                    <div class="smallfont">Para Pemain:</div>  
                        <div class="" style="min-height: 48px;">
                            <?php if(count($v['players'])>0) {
                                ?>
                            <ul class="ul-img-hr">
                                <?php foreach ($v['players'] as $key => $pro) { ?>
                                    <?php 
                                    if ($key === 5) {
                                        echo 'more';
                                        break;
                                    };

                                    if ($pro['profile_icon_url'] != 'None') {
                                        $img_politik = $pro['profile_icon_url'];
                                        if (!file_exists($img_politik)) {
                                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                                        }
                                    } else {
                                        $img_politik = 'assets/images/icon/no-image-politisi.png';
                                    }
                                    ?>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                            <img
                                                class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                src="<?php echo base_url().$img_politik; ?>">
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                        <?php } ?>
                    </div> 
                </div>
                <div class="scandal-f0-stats" style="margin-left: 13px;">
                <div class="clear"></div>
                <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">status</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;"><?php echo $status;?></div>
                <div class="clear"></div>
                <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">kejadian</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;"><?php echo $kejadian;?></div>
                <div class="clear"></div>
                <div class="smallfont" style="float: left;width: 60px;margin-left: 1px;">dampak</div>
                <div class="smallfont" style="float: left;width: 10px;">:</div>
                <div class="smallfont" style="float: left;word-wrap: break-word;width: 215px;"><?php echo $dampak;?></div>
                <div class="clear"></div>    
                </div>
                <div class="scandals-e-other">

                <?php
                    }else{
                ?>  
                <div class="scandals-f-other-image">
                    <?php echo $linkedTImage;?>
                </div>
                <div class="scandals-f-other-title">
                    <?php echo $linkedTitle;?>
                </div>
                <div class="clear"></div>
                <?php
                    }
                }
                echo '<div class="clear"></div>';
                echo '</div>';
            }
            ?>



            </div>
            <div class="btn btn-mini" id="next_f">5 Berikutnya</div>
            <div>
                <img id="circular_progress_indicator9" src="<?php echo base_url('assets/images/image_825740.gif');?>" style="display:none;width: 32px;height: 32px;"/>
            </div>
            <div class="btn btn-mini" id="selengkapnya_f"><a href="<?php echo $allFLink;?>" style="color: #000;">Selengkapnya</a></div>
            <div class="clear"></div>
            <div id="page_f_spot">
                <input type="hidden" id="hidden_f_page" value="2"/>
            </div>


            <?php // /RIGHT CONTENT HERE ?>
        </div>
        
    </div>
</div>

<script type="text/javascript">
    var page = $('#hidden_a_page').val();
    var pageb = $('#hidden_b_page').val();
    var pagec = $('#hidden_c_page').val();
    var paged = $('#hidden_d_page').val();
    var pagee = $('#hidden_e_page').val();
    var pagef = $('#hidden_f_page').val();
    var limit = 5;
    var exclude = <?php echo $mainScandalId;?>;
    $(document).ready(function(){
    })

    $('#next_a').click(function(){
        $.ajax({
            async: true,
            cache: false,
            type: 'post',
            datatype: 'json',
            url: Settings.base_url+'scandal/scandals_list2',
            data: {'limit':limit,'page':page,'categoryId':'<?php echo $categoryA['categoryId']?>','viewType':'a','exclude':exclude},
            beforeSend: function(){
                $('#circular_progress_indicator2').css('display','block');
            },
            success: function(response){
                $('#scandals_a_spot').html(response);
                page = Number(page) + 1;
                $('#page_a_spot').html('<input type="hidden" id="hidden_a_page" value="'+page+'"/>');
            },
            complete: function(){
                $('#circular_progress_indicator2').css('display','none');
            },


        })        
    })

    $('#next_b').click(function(){
        $.ajax({
            async: true,
            cache: false,
            type: 'post',
            datatype: 'json',
            url: Settings.base_url+'scandal/scandals_list2',
            data: {'limit':limit,'page':pageb,'categoryId':'<?php echo $categoryB['categoryId']?>','viewType':'b','exclude':exclude},
            beforeSend: function(){
                $('#circular_progress_indicator5').css('display','block');
            },
            success: function(response){
                $('#scandals_b_spot').html(response);
                pageb = Number(pageb) + 1;
                $('#page_b_spot').html('<input type="hidden" id="hidden_b_page" value="'+pageb+'"/>');
            },
            complete: function(){
                $('#circular_progress_indicator5').css('display','none');
            },


        })        
    })    

    $('#next_c').click(function(){
        $.ajax({
            async: true,
            cache: false,
            type: 'post',
            datatype: 'json',
            url: Settings.base_url+'scandal/scandals_list2',
            data: {'limit':limit,'page':pagec,'categoryId':'<?php echo $categoryC['categoryId']?>','viewType':'c','exclude':exclude},
            beforeSend: function(){
                $('#circular_progress_indicator6').css('display','block');
            },
            success: function(response){
                $('#scandals_c_spot').html(response);
                pagec = Number(pagec) + 1;
                $('#page_c_spot').html('<input type="hidden" id="hidden_c_page" value="'+pagec+'"/>');
            },
            complete: function(){
                $('#circular_progress_indicator6').css('display','none');
            },


        })        
    })

    $('#next_d').click(function(){
        $.ajax({
            async: true,
            cache: false,
            type: 'post',
            datatype: 'json',
            url: Settings.base_url+'scandal/scandals_list2',
            data: {'limit':limit,'page':paged,'categoryId':'<?php echo $categoryD['categoryId']?>','viewType':'d','exclude':exclude},
            beforeSend: function(){
                $('#circular_progress_indicator7').css('display','block');
            },
            success: function(response){
                $('#scandals_d_spot').html(response);
                paged = Number(paged) + 1;
                $('#page_d_spot').html('<input type="hidden" id="hidden_d_page" value="'+paged+'"/>');
            },
            complete: function(){
                $('#circular_progress_indicator7').css('display','none');
            },


        })        
    })        

    $('#next_e').click(function(){
        $.ajax({
            async: true,
            cache: false,
            type: 'post',
            datatype: 'json',
            url: Settings.base_url+'scandal/scandals_list2',
            data: {'limit':limit,'page':pagee,'categoryId':'<?php echo $categoryE['categoryId']?>','viewType':'e','exclude':exclude},
            beforeSend: function(){
                $('#circular_progress_indicator8').css('display','block');
            },
            success: function(response){
                $('#scandals_e_spot').html(response);
                pagee = Number(pagee) + 1;
                $('#page_e_spot').html('<input type="hidden" id="hidden_e_page" value="'+pagee+'"/>');
            },
            complete: function(){
                $('#circular_progress_indicator8').css('display','none');
            },


        })        
    })        

    $('#next_f').click(function(){
        $.ajax({
            async: true,
            cache: false,
            type: 'post',
            datatype: 'json',
            url: Settings.base_url+'scandal/scandals_list2',
            data: {'limit':limit,'page':pagef,'categoryId':'<?php echo $categoryF['categoryId']?>','viewType':'f','exclude':exclude},
            beforeSend: function(){
                $('#circular_progress_indicator9').css('display','block');
            },
            success: function(response){
                $('#scandals_f_spot').html(response);
                pagef = Number(pagef) + 1;
                $('#page_f_spot').html('<input type="hidden" id="hidden_f_page" value="'+pagef+'"/>');
            },
            complete: function(){
                $('#circular_progress_indicator9').css('display','none');
            },


        })        
    })        

</script>