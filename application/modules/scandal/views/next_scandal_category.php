<?php 
$forbiddenChars = array(' ','#',',','.','$','"',"'");
?>
<div class="row-fluid">
    <div class="span8">
        <?php 
        if(!empty($scandals_rest_left)){
            foreach($scandals_rest_left as $k=>$v){
                $titleScandal = !empty($v['title']) ? html_entity_decode($v['title']) : '';
                $photoScandal = !empty($v['photos'][0]['large_url']) ? $v['photos'][0]['large_url'] : '';
                if($photoScandal != ''){
                    $photoScandal = '<img class="scandal-pic3" src="'.$photoScandal.'"/>';
                }else{
                    $photoScandal = '';
                }

                $descriptionScandal = !empty($v['description']) ? substr($v['description'],0,350).'...' : '';
                $playersScandal = !empty($v['players']) ? $v['players'] : array();
                $statusScandal = !empty($v['status']) ? $v['status'] : '';
                $dampakScandal = !empty($v['uang']) ? $v['uang'] : '';
                if(!empty($v['date'])){
                    $tmp = new DateTime($v['date']);
                    $kejadianScandal = $tmp->format('d M Y');
                }else{
                    $kejadianScandal = '';
                }   
                $urlScandal = base_url('scandal/index/'.$v['scandal_id'].'-'.strtolower(str_replace($forbiddenChars, '_', $titleScandal)));
        ?>        
        <div class="row-fluid bottombordered">
            <div class="span6">
                <div class="scandal-image3 text-center">
                    <?php echo anchor($urlScandal,$photoScandal);?>
                </div>
                <div class="scandal-status3">
                    <div class="row-fluid">
                        <div class="span3 lh1">
                            status
                        </div>
                        <div class="span1 lh1">
                            :
                        </div>
                        <div class="span8 lh1">
                            <?php echo $statusScandal;?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3 lh1">
                            kejadian
                        </div>
                        <div class="span1 lh1">
                            :
                        </div>
                        <div class="span8 lh1">
                            <?php echo $kejadianScandal;?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3 lh1">
                            dampak
                        </div>
                        <div class="span1 lh1">
                            :
                        </div>
                        <div class="span8 lh1">
                            <?php echo $dampakScandal;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="scandal-title3">
                    <?php echo anchor($urlScandal,$titleScandal);?>
                </div>
                <div class="scandal-description3">
                    <?php echo $descriptionScandal;?>
                </div>
                <div class="scandal-players3">
                    <div style="font-weight: bold;color: rgba(0,0,0,1.0);">Para Pemain</div>
                    <div class="" style="min-height: 48px;">
                        <?php if(count($playersScandal)>0) {
                            ?>
                            <ul class="ul-img-hr">
                                <?php foreach ($playersScandal as $key => $pro) { ?>
                                    <?php 
                                    if ($key === 5) {
                                        echo 'more';
                                        break;
                                    };

                                    if ($pro['profile_icon_url'] != 'None') {
                                        $img_politik = $pro['profile_icon_url'];
                                        if (!file_exists($img_politik)) {
                                            $img_politik = 'assets/images/icon/no-image-politisi.png';
                                        }
                                    } else {
                                        $img_politik = 'assets/images/icon/no-image-politisi.png';
                                    }
                                    ?>
                                    <li class="">
                                        <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                            <img
                                                class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                                title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                                src="<?php echo base_url().$img_politik; ?>">
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
        }
        ?>
    </div>
    <div class="span4">
        <?php 
        if(!empty($scandals_rest_right)){
            foreach($scandals_rest_right as $k=>$v){
                $titleScandal = !empty($v['title']) ? html_entity_decode($v['title']) : '';
                $photoScandal = !empty($v['photos'][0]['large_url']) ? $v['photos'][0]['large_url'] : '';
                if($photoScandal != ''){
                    $photoScandal = '<img class="scandal-pic4" src="'.$photoScandal.'"/>';
                }else{
                    $photoScandal = '';
                }

                $descriptionScandal = !empty($v['description']) ? substr($v['description'],0,350).'...' : '';
                $playersScandal = !empty($v['players']) ? $v['players'] : array();
                $statusScandal = !empty($v['status']) ? $v['status'] : '';
                $dampakScandal = !empty($v['uang']) ? $v['uang'] : '';
                if(!empty($v['date'])){
                    $tmp = new DateTime($v['date']);
                    $kejadianScandal = $tmp->format('d M Y');
                }else{
                    $kejadianScandal = '';
                } 
                $urlScandal = base_url('scandal/index/'.$v['scandal_id'].'-'.strtolower(str_replace($forbiddenChars, '_', $titleScandal)));
        ?>
        <div class="bottombordered" style="padding-bottom: 10px;">
            <div class="scandal-title4">
                <?php echo anchor($urlScandal,$titleScandal);?>
            </div>
            <div class="scandal-image4">
                <?php echo anchor($urlScandal,$photoScandal);?>
            </div>
            <div class="scandal-status4">
                <div class="row-fluid">
                    <div class="span3 lh1">
                        status
                    </div>
                    <div class="span1 lh1">
                        :
                    </div>
                    <div class="span8 lh1">
                        <?php echo $statusScandal;?>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 lh1">
                        kejadian
                    </div>
                    <div class="span1 lh1">
                        :
                    </div>
                    <div class="span8 lh1">
                        <?php echo $kejadianScandal;?>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span3 lh1">
                        dampak
                    </div>
                    <div class="span1 lh1">
                        :
                    </div>
                    <div class="span8 lh1">
                        <?php echo $dampakScandal;?>
                    </div>
                </div>
            </div>
            <div class="scandal-players4">
                <div style="font-weight: bold;color: rgba(0,0,0,1.0);">Para Pemain</div>
                <div class="" style="min-height: 48px;">
                    <?php if(count($playersScandal)>0) {
                        ?>
                        <ul class="ul-img-hr">
                            <?php foreach ($playersScandal as $key => $pro) { ?>
                                <?php 
                                if ($key === 5) {
                                    echo 'more';
                                    break;
                                };

                                if ($pro['profile_icon_url'] != 'None') {
                                    $img_politik = $pro['profile_icon_url'];
                                    if (!file_exists($img_politik)) {
                                        $img_politik = 'assets/images/icon/no-image-politisi.png';
                                    }
                                } else {
                                    $img_politik = 'assets/images/icon/no-image-politisi.png';
                                }
                                ?>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                        <img
                                            class="iconsize1 img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                            title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                            src="<?php echo base_url().$img_politik; ?>">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <div style="font-weight: bold;padding-top: 0;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
            }
        }
        ?>

    </div>
</div>