<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comunity extends Application
{
	var $member;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('date');
		$this->load->helper('m_date');
		
		$this->load->helper('text');
		$this->load->helper('seourl');

		$this->load->library('core/member_lib');
		$this->load->library('core/photo_lib');
        $this->load->library('core/content_stream_lib');
		$this->load->library('groups/groups_lib');

		$this->load->model('content_stream');

     // $this->checkMember();
     // $this->checkUID();
	}

	public function index()
	{

    }
  
	public function friendrequest()
	{
		$data = $this->data;
		$member = $this->session->userdata('member');
		if($member)
		{

      $data['usermember'] = $this->member_lib->GetUserAccountByAccountID($member['account_id']);

			$data['member'] = $member;
			$data['guestmember'] = $member;
			
			$FriendRequest = $this->member_lib->GetFriendRequest($member['account_id']);		
			$foto_sirequest = array();
			for($i = 0; $i < count($FriendRequest); $i++)
			{
				$FriendRequest[$i]['display_name'] = $this->member_lib->GetUserDisplayName($FriendRequest[$i]['account_id']);
				$foto_sirequest[$FriendRequest[$i]['account_id']] = $this->FotoTitleByAccount($FriendRequest[$i]['account_id']);
			}
			
		  $FriendRequestFrom = $this->member_lib->GetFriendRequestFrom($member['account_id']);
			$foto_sentrequest = array();
			for($i = 0; $i < count($FriendRequestFrom); $i++)
			{
				$FriendRequestFrom[$i]['to_display_name'] = $this->member_lib->GetUserDisplayName($FriendRequestFrom[$i]['to_account_id']);
				$foto_sentrequest[$FriendRequestFrom[$i]['to_account_id']] = $this->FotoTitleByAccount($FriendRequestFrom[$i]['to_account_id']);
			}

			$data['requests'] = $FriendRequest;
			$data['foto_request'] = $foto_sirequest;
			$data['sentrequests'] = $FriendRequestFrom;
			$data['foto_sentrequest'] = $foto_sentrequest;
			$this->load->view('comunity/friend_request_list_view', $data);			
		} else {	
			redirect(base_url().'home/login/?redirect='.$this->current_url);
		}
	}
	
  public function follow($tab="") 
  {
  	$data = $this->data;
    $user_id = (isset($_GET['uid'])) ? $_GET['uid'] : false;
	  $member = $this->session->userdata('member');
	  if ($member) 
	  {
		  if (!$user_id)  $user_id = $member['account_id'];
		  $row = $this->member_lib->GetMemberByAccountID($user_id);
		  if ($row) 
		  {
			  $member2 = array(
					'username'  => $row['display_name'],
				  'user_id' => $row['page_id'],
					'email' => $row['email'],
					'account_id' => $row['account_id'],
					'profile_id' => $row['profile_content_id'],
					'xname' => $this->FotoTitle($row['profile_content_id']),
					'gender' => $row['gender'],
					'birthday' => $row['birthday'],
					'affiliasi' => $this->member_lib->GetUserAffiliasiText($row['page_id']),
				  'current_city' => $this->member_lib->GetUserCurrentCity($user_id)
			  );
                                        
			  $data['member'] = $member;
			  $data['guestmember'] = $member2;
			  $rowFollow = $this->member_lib->GetMemberFollowAnFollowerList($member2['account_id'], $member2['user_id']);
			  $xdata['follow_or_follower'] = $rowFollow;
			  $xdata['limit'] = 1000;
			  $xdata['offset'] = 0;
			  $xdata['tab'] = $tab;
			  $data['follow_list'] = $this->load->view('comunity/microview/followee_view', $xdata, true);
			  $this->load->view('comunity/follow_view', $data);
		  } else {
				redirect(base_url().'comunity/follow');
		  }
		} else {
			redirect(base_url().'home/login/?redirect='.$this->current_url);
		}
	}

  public function userlike($url='')
  {
    $this->checkMember();
    $this->checkUID();     
  	$data = $this->data;
    
    $data['title']       = 'User Like';
    $data['UserLike']    = $this->GetUserLikeDetail($this->member['account_id']);
	$data['load_more']   = true;
    $data['page']        = '1';
    $data['offset']      = '0';  
    $data['member_data'] = $this->GetMemberData($this->member['account_id']);
    $data['usermember']  = $data['member_data']['usermember'];
    $this->load->view('comunity/like_user', $data);
  }
  
  public function userlikemore($page='1', $limit='50')
  {
    $this->checkMember();
    $this->checkUID();     
   	
      // first page use limit 10 so next offset is 11
    $offset           = ($page =="1") ? '11' :  $page*$limit;
    $data['UserLike'] = $this->GetUserLikeDetail($this->member['account_id'], $offset, $limit);       	
    $data['offset']   = $offset;
    $totalPage        = ceil($data['UserLike']['total_rows']/$limit);
 	$nextPage         = (int)$page+1;
    $load_more        = ($page < $totalPage) ? '1': '0';               
    $html = $this->load->view('comunity/like_user_list', $data, true);      
    // HTML Response, Flag Load More, Next Page
    echo $html .'||'. $load_more .'||'. $nextPage;      
   }   

   private function GetUserLikeDetail($AccountID)
   {
     $data['ResLike'] = $this->member_lib->GetUserLikeMember($AccountID, 0, 20);
     $data['total_rows'] = $this->member_lib->NewCountUserLikeMember($AccountID);
     return $data;
   }

  public function userunlike()
  {
    $this->checkMember();
    $this->checkUID();     
  	$data = $this->data;
    
    $data['title']       = 'User Unlike';
    $data['Dislike']    = $this->GetUserUnlikeDetail($this->member['account_id']);   	
	$data['load_more']   = true;
    $data['page']        = '1';
    $data['offset']      = '0';  
    $data['member_data'] = $this->GetMemberData($this->member['account_id']);  	
    $data['usermember']  = $data['member_data']['usermember'];    
    $this->load->view('comunity/unlike_user', $data);  	
  }

  public function userunlikemore($page='1', $limit='50')
  {
    $this->checkMember();
    $this->checkUID();     
   	
      // first page use limit 10 so next offset is 11
    $offset           = ($page =="1") ? '11' :  $page*$limit;
    $data['Dislike'] = $this->GetUserUnikeDetail($this->member['account_id'], $offset, $limit);       	
    $data['offset']   = $offset;
    $totalPage        = ceil($data['Dislike']['total_rows']/$limit);
 	$nextPage         = (int)$page+1;
    $load_more        = ($page < $totalPage) ? '1': '0';               
    $html = $this->load->view('comunity/unlike_user_list', $data, true);      
    // HTML Response, Flag Load More, Next Page
    echo $html .'||'. $load_more .'||'. $nextPage;      
  }   

  private function GetUserUnlikeDetail($AccountID)
  {
  	$data['ResLike'] = $this->member_lib->GetUserDislikeMember($AccountID, 0, 20); 
  	$data['total_rows'] = $this->member_lib->NewCountUserDislikeMember($AccountID);
    return $data;
  }

  public function usercomment()
  {
    $this->checkMember();
    $this->checkUID();     
  	$data = $this->data;
    
    $data['title']       = 'User Comment';
    $data['Comment']     = $this->GetUserCommentDetail($this->member['account_id']);   	
	$data['load_more']   = true;
    $data['page']        = '1';
    $data['offset']      = '0';  
    $data['member_data'] = $this->GetMemberData($this->member['account_id']);  	
    $data['usermember']  = $data['member_data']['usermember'];
    $this->load->view('comunity/user_comment', $data);
  } 

  private function GetUserCommentDetail($AccountID, $page='0', $limit='20')
  {
  	$data['ResComment'] = $this->member_lib->GetUserCommentMember($AccountID, $page, $limit);
  	$data['total_rows'] = $this->member_lib->NewCountUserCommentMember($AccountID);
    return $data;
  }

  public function usercommentmore($page='1', $limit='50')
  {
    $this->checkMember();
    $this->checkUID();     
   	
      // first page use limit 10 so next offset is 11
    $offset           = ($page =="1") ? '11' :  $page*$limit;
    $data['Comment'] = $this->GetUserCommentDetail($this->member['account_id'], $offset, $limit);       	
    $data['offset']   = $offset;
    $totalPage        = ceil($data['Comment']['total_rows']/$limit);
 	$nextPage         = (int)$page+1;
    $load_more        = ($page < $totalPage) ? '1': '0';               
    $html = $this->load->view('comunity/comment_user_list', $data, true);      
    // HTML Response, Flag Load More, Next Page
    echo $html .'||'. $load_more .'||'. $nextPage;      
  }   

  public function viewContent($userAccount, $content_id, $groupType)
  {

      $this->checkMember();
      $this->checkUID();
      $data = $this->data;

      $data['title']  = 'User Comment';
      $data['titlecontent'] = $groupType;
      $data['member_data'] = $this->GetMemberData($this->member['account_id']);
      $data['usermember']  = $data['member_data']['usermember'];
      $data['GroupName'] = $this->member_lib->GetContentGroupTypeName($groupType);
      $data['Content'] = $this->member_lib->ViewUserCommentOnMember($content_id, $userAccount);
      $this->load->view('comunity/content_user_index', $data);
  }

	public function Profile()
	{
		$data = $this->data;		
		$member = $this->session->userdata('member');
		if($member)
		{
			$user_id = isset($_GET['uid']) ? $_GET['uid'] : NULL;
			$isEdit  = isset($_GET['edit']) ? $_GET['edit'] : 0;
			if(!$user_id) $user_id = $member['account_id'];			
			$row = $this->member_lib->GetMemberByAccountID($user_id);
			$page = $this->content_stream->getUserPage($user_id)->row();
			if($row && $page)
			{
					$member2 = array(
								'username'  => ucwords(strtolower($row['display_name'])),
								'user_id' => $row['page_id'],
								'email'     => $row['email'],
								'account_id' => $row['account_id'],
								'profile_id' => $row['profile_content_id'],
								'xname' => $this->FotoTitle($row['profile_content_id']),
								'gender' => $row['gender'],
								'birthday' => $row['birthday'],
								'about' => $page->about,
								'current_city' => $this->member_lib->GetUserCurrentCity($user_id),
								'fname' => $row['fname'],
								'lname' => $row['lname'],
								'affiliasi' => $this->member_lib->GetUserAffiliasiText($row['page_id']),
								'arrAffiliasi' => $this->member_lib->GetUserAffiliasi($row['page_id']),
					);
								
					$isAFriend = $this->member_lib->isAFriend($member['account_id'],$user_id);
					$isFolowing = $this->member_lib->isFolowing($member['account_id'],$user_id); 
					$isFriendRequested = $this->member_lib->isFriendRequested($member['account_id'],$user_id); 
					$RequestedToBeYourFriend = $this->member_lib->isFriendRequested($user_id,$member['account_id']); 

          $data['isEdit']                  = $isEdit;	
					$data['isAFriend']               = $isAFriend;	
					$data['isFriendRequested']       = $isFriendRequested;	
					$data['RequestedToBeYourFriend'] = $RequestedToBeYourFriend;	
					$data['isFolowing']              = $isFolowing;			
					$data['member']                  = $member;
					$data['guestmember']             = $member2;
            
          $cm = $this->load->module('ajax');
          $data['count_profile'] = $cm->count_profile($data['guestmember']['user_id'], TRUE, TRUE);
					$this->load->view('comunity/profile_basic_info_view', $data);
			} else {
			   echo 'invalid profile';
				//redirect(base_url()."komunitas/profile");
			}
		} else {
			redirect(base_url().'home/login/?redirect='.$this->current_url);
		}
	}

	public function contactinfo() {
		$data = $this->data;
		$user_id = isset($_GET['uid']) ? $_GET['uid'] : NULL;
		$member = $this->session->userdata('member');
		if ($member) {
			if (!$user_id)  $user_id = $member['account_id'];
			$row = $this->member_lib->GetMemberByAccountID($user_id);
			if ($row) {
					$member2 = array(
								'username'  => $row['display_name'],
								'user_id' => $row['page_id'],
								'email'     => $row['email'],
								'account_id' => $row['account_id'],
								'profile_id' => $row['profile_content_id'],
								'xname' => $this->FotoTitle($row['profile_content_id']),
								'gender' => $row['gender'],
								'birthday' => $row['birthday'],
								'affiliasi' => $this->member_lib->GetUserAffiliasiText($row['page_id']),
								'current_city' => $this->member_lib->GetUserCurrentCity($user_id)
					);
					$address_info = (array)$this->member_lib->GetObjectInfoAddressByAccountId($user_id);
					if (!$address_info)  $address_info = array(
							'page_id' => $this->content_stream->getUserPageId($user_id),
							'address_id' => NULL,
							'address' => NULL,
							'city_state' => NULL,
							'country' => NULL,
							'postalcode' => NULL,
							'phone' => NULL,
							'location' => NULL,
							'main_address' => NULL,
							'province_state' => NULL
					);			
					$data['member'] = $member;
					$data['address_info'] = $address_info;
					$data['guestmember'] = $member2;
					$this->load->view('comunity/profile_contact_info_view', $data);
			}
		}	
	}
					
    private function FotoTitleByAccount($accountid, $expired = 300)
	{
        $query = call_user_func_array(array($this->photo_lib,"getProfilePicActive"), array($accountid));
		$title = 'noimage.gif';
		if($query) { $title = $query['attachment_title'];}		
		return $title;		
	}

	private function FotoTitle($contentID, $expired = 300) 
	{		
		$query = $this->photo_lib->getContentAttachment($contentID)->row_array();	
		if(count($query) > 0)
		{
			return $query['attachment_title'];			
		} else {
			return 'noimage.gif';
		}		
	}

  public function TotalNotify()
  {
      $this->checkMember();
      $this->checkUID();
      $data = $this->data;

      $NumberOfFriend = 0;
      $NumberOfFollow = 0;
      $NumberOfLike = 0;
      $NumberOfDislike = 0;
      $NumberOfComment = 0;
      $TotalNotify = 0;

 	  $rsMember = $this->member_lib->GetMemberByAccountID($this->member['account_id']);
			
	  $query = $this->member_lib->GetFriendRequest($this->member['account_id']);
	  $NumberOfFriend = count($query);

      $user_detail = $this->member_lib->GetMemberByAccountID($this->member['account_id']);
	  $count = $this->member_lib->GetMemberFollowsCountRev($user_detail['account_id'], $user_detail['user_id']);
	  $NumberOfFollow = $count[0]['count'];
	  if($rsMember['show_notice_like'] == 'y')
	  {
        $user_detail = $this->member_lib->GetMemberByAccountID($this->member['account_id']);
        $count = $this->member_lib->GetUserLikeMember($this->member['account_id']);
        $NumberOfLike = count($count);
      } else { $NumberOfLike = 0; }   

	  if($rsMember['show_notice_unlike'] == 'y')
      {
        $user_detail = $this->member_lib->GetMemberByAccountID($this->member['account_id']);
        $count = $this->member_lib->GetUserDislikeMember($user_detail['account_id']);
        $NumberOfDislike = count($count);					
      } else { $NumberOfDislike = 0; }
      
      if($rsMember['show_notice_comment'] == 'y')
      {	
        $user_detail = $this->member_lib->GetMemberByAccountID($this->member['account_id']);
        $count = $this->member_lib->CountUserComment($user_detail['account_id']);
        $NumberOfComment = $count['comment_count'];
      } else { $NumberOfComment = 0; }
      
      $TotalNotify = $NumberOfFriend + $NumberOfFollow + $NumberOfLike + $NumberOfDislike + $NumberOfComment;
      echo $TotalNotify;
  }

  public function NumberOfFriendRequest()
  {
      $this->checkMember();
      $this->checkUID();
	  $query = $this->member_lib->GetFriendRequest($this->member['account_id']);
	  echo count($query);
  }

	public function NumberOfFollow()
	{
        $this->checkMember();
        $this->checkUID();
        $user_detail = $this->member_lib->GetMemberByAccountID($this->member['account_id']);
		$count = $this->member_lib->GetMemberFollowsCountRev($user_detail['account_id'], $user_detail['user_id']);
		echo $count[0]['count'];
	}
	
	public function NumberOfLike()
	{
        $this->checkMember();
        $this->checkUID();
        $user_id = $this->member['account_id'];
        $user_detail = $this->member_lib->GetMemberByAccountID($user_id);
        $count = $this->member_lib->CountUserLike($this->member['account_id']);
        echo $count['like_account'];
	}
	
	public function NumberOfUnlike()
	{
        $this->checkMember();
        $this->checkUID();
        $user_id = isset($_GET['uid']) ? $_GET['uid'] : $this->member['account_id'];
        $user_detail = $this->member_lib->GetMemberByAccountID($user_id);
        $count = $this->member_lib->CountUserUnLike($user_detail['account_id']);
        echo $count['dislike_account'];
	}

	public function NumberOfComment()
	{
        $this->checkMember();
        $this->checkUID();
        $user_id = isset($_GET['uid']) ? $_GET['uid'] : $this->member['account_id'];
        $user_detail = $this->member_lib->GetMemberByAccountID($user_id);
        $count = $this->member_lib->CountUserComment($user_detail['account_id']);
        echo $count['comment_count'];
	}
	
	public function NumberOfGroups()
	{
        $this->checkMember();
        $this->checkUID();
        $user_detail = $this->member_lib->GetMemberByAccountID($this->member['account_id']);
        $count = $this->member_lib->CountMemberGroup($this->member['account_id']);
        echo $count['total_group'];
	}

    public function likehasread()
    {
  	    $_accountID = $_GET['accountid'];
  	    $_entrydate = $_GET['entrydate'];
        $count = $this->member_lib->likehasread($_accountID, $_entrydate);
		echo $_accountID;
    }

    public function unlikehasread()
    {
  	    $_accountID = $_GET['accountid'];
  	    $_entrydate = $_GET['entrydate'];
        $count = $this->member_lib->unlikehasread($_accountID, $_entrydate);
		echo $_accountID;
    }
	
    public function commenthasread()
    {
     	$_accountID = $_GET['accountid'];
  	    $_entrydate = $_GET['entrydate'];
        $count = $this->member_lib->commenthasread($_accountID, $_entrydate);
	    echo $_accountID;
    }

    public function followhasread()
    {
        $_accountid = $_GET['accountid'];
        $_pagename = $_GET['pagename'];
        $count = $this->member_lib->followhasread($_accountid, $_pagename);
        echo $count;
    }

    public function statusdetail($to_page_id, $content_id)
    {
        $this->checkMember();
        $this->checkUID();
        $data = $this->data;

        $member = $this->session->userdata('member');
        if($member)
        {
            $data['member_data'] = $this->GetMemberData($this->member['account_id']);
            $data['usermember']  = $data['member_data']['usermember'];
            $data['guestmember'] = $data['member_data']['usermember'];
            $data['content'] = $this->content_stream_lib->GetContentStreamByViewerPage($to_page_id, $content_id);

            $this->load->view('comunity/status_detail_view', $data);
        } else {
            redirect(base_url().'home/login/?redirect='.$this->current_url);
        }
    }

	public function groupwall($groupID)
	{
		$data = $this->data;
		$member = $this->session->userdata('member');
		if($member)
		{
			$row = $this->member_lib->GetMemberByAccountID($member['account_id']);
			$data['mgroup'] = $this->comgroup_lib->GetGroupByGroupID($groupID);
            $data['usermember'] = $this->member_lib->GetUserAccountByAccountID($member['account_id']);
			$page = $this->content_stream->getUserPage($member['account_id'])->row();
		    if ($row)
		    {
			   $member = array(
					'username'  => $row['display_name'],
			  	    'user_id' => $row['page_id'],
					'email' => $row['email'],
					'account_id' => $row['account_id'],
					'profile_id' => $row['profile_content_id'],
					'foto_name' => $this->FotoTitle($row['profile_content_id']),
					'gender' => $row['gender'],
					'birthday' => $row['birthday'],					
			   );
               $data['groupphoto'] = $this->FotoTitleByAccount($member['account_id']);
			   $this->load->view('comunity/group/group_postwall', $data);
			}  
		} else {
			redirect(base_url().'home/login/?redirect='.$this->current_url);			
		}

	}
	
 	private function checkMember() {
 	   $this->load->model('member', 'member_model');
		if($this->member) {
			$this->member['current_city'] = $this->member_model->getUserCurrentCity($this->member['account_id']); 
		} else {
			redirect($this->data['base_url'].'home/login/?redirect='.$this->current_url);
		}
	}

   private function checkUID() {
		$uID = $this->input->post('uID');
		if(isset($_GET['uid']) && ! empty($_GET['uid'])) {
			$this->uID = $_GET['uid'];
			$this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);
			
		} else if(! empty($uID)) {
			$this->uID = $uID;
			$this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);
			
		} else {
			$this->uID  = $this->member['account_id'];
			$this->isLogin = true;
		}
	}
	
	private function isLogin($str1, $str2) {
		if(! strcmp($str1, $str2)) {
			return true;
		} else {
			return false;
		}
	}
	
	private function GetMemberData($AccountID)
	{
      $row = $this->member_lib->GetMemberByAccountID($AccountID);
      $data['usermember'] = $this->member_lib->GetUserAccountByAccountID($AccountID);
	  if ($row)
	  {
	     $data['member2'] = array(
			'username'  => $row['display_name'],
		    'user_id' => $row['page_id'],
			'email' => $row['email'],
			'account_id' => $row['account_id'],
			'profile_id' => $row['profile_content_id'],
			'xname' => $this->FotoTitle($row['profile_content_id']),
			'gender' => $row['gender'],
			'birthday' => $row['birthday'],
			'affiliasi' => $this->member_lib->GetUserAffiliasiText($row['page_id']),
			'current_city' => $this->member_lib->GetUserCurrentCity($AccountID)
		 );
	  }
	  return $data;
	}
		  	
}
?>
