<?php

//echo "<pre>";
//print_r($Content);
//echo "</pre>";

$_photo = isset($Content['content_photo']['attachment_title']) ? $Content['content_photo']['attachment_title'] : '';

?>
<div class="post post-listing-item row-fluid">
    <div class="post-user span1">
        <img src="<?php echo icon_url($_photo, 'user/'.$Content['content']['account_id']); ?>" alt="<?php echo $Content['content']['display_name']; ?>"" />
    </div>
    <div class="post-content span11">
        <a href="<?php echo base_url().'komunitas/wall?uid='.$Content['content']['account_id']; ?>"><b><?php echo $Content['content']['display_name']; ?></b></a>
        <h5><a href="<?php echo base_url().'groups/detail?id='.$Content['content']['content_id'].'&uid='.$Content['content']['account_id'];?>"  target="_blank">
            <?php echo textwrap($Content['content']['title']); ?>
        </a></h5>

        <div class="row-bor">
            <div class="cnt-text-fix"><div><?php echo $Content['content']['description']; ?></div></div>
        </div>

        <div class="comments" id="cmnt_<?php echo $Content['content']['content_id']; ?>">
          <script>
            $("#cmnt_<?php echo $Content['content']['content_id']; ?>").load('<?php echo base_url(); ?>ajax/comment_load/', { 'id': '<?php echo $Content['content']['content_id']; ?>','n': '10','content_date': '<?php echo $Content['content']['entry_date']; ?>'  });
          </script>
        </div>
    </div>
</div>
