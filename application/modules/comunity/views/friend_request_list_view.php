<?php
$isSentimen = FALSE;
$this->load->view($template_path .'tpl_header');
?>
<div id='main'><div id='body' class='page'><div class='container'>

<div class='row row-col'><div class='col-border clearfix'>
	<div class='col col-1-4 col-side' id='col-left'>
		<?php
		$this->load->view('comunity/member/include_ajax');
		$this->load->view('comunity/member/user_badge');
		$data['activePage'] = 'friend_request';
		$this->load->view('comunity/member/komunitas_menu', $data);
		$this->load->view('komunitas/include_member/mini_friend_list');
		?>
	</div>
	<div class='col col-1-2' id='col-main'>
		<div class='block'>

			<h2>Friend Requests</h2>
			<?php if(count($requests) == 0):?><p><i>Belum ada friend requests</i></p><?php endif;?>
			<?php foreach($requests as $req){?>
				<div class="row-fluid">
					<div class='span1'>
						<a href="<?php echo base_url().'/comunity/profile?uid='.$req['account_id'];?>">
							<img style="float:left; padding-right:2px;" src="<?php echo icon_url($foto_request[$req['account_id']]);?>">
						</a>
					</div>
					<div class='span11'>
						<b><a href="<?php echo base_url().'/comunity/profile?uid='.$req['account_id'];?>"><?php echo $req['display_name'];?></b></a> meminta ijin untuk menjadi teman anda, dengan pesan:
						<i>"<?php echo $req['request_msg'];?>"</i>';
						<div class="more" id="acceptdeny_<?php echo $req['friend_request_id'];?>">
							<a class="btn btn-mini" id="accept_<?php echo $req['friend_request_id'];?>">terima</a>
							<a class="btn btn-mini" id="deny_<?php echo $req['friend_request_id'];?>">tolak</a>
						</div>
					</div>
					<script>
						$("#accept_<?php echo $req['friend_request_id'];?>").click(function(){
							$.ajax({
								url: "<?php echo base_url().'komunitas/ajaxconfirmfriendrequest'?>",
								type: "post",
								data: {id: "<?php echo $req['account_id']?>"},
								success: function(response, textStatus, jqXHR){
									$("#acceptdeny_<?php echo $req['friend_request_id'];?>").html('<i>pertemanan diterima</i>');
								},
								error: function(jqXHR, textStatus, errorThrown){
									alert(
										"The following error occured: "+
										textStatus, errorThrown
									);
									$("#ajax_loader").hide();
								},
								complete: function(){
								}
							});
						});
						$("#deny_<?php echo $req['friend_request_id'];?>").click(function(){
							$.ajax({
								url: "<?php echo base_url().'komunitas/ajaxdenyfriendrequest'?>",
								type: "post",
								data: {id: "<?php echo $req['account_id']?>"},
								success: function(response, textStatus, jqXHR){
									$("#acceptdeny_<?php echo $req['friend_request_id'];?>").html('<i>pertemanan ditolak</i>');
								},
								error: function(jqXHR, textStatus, errorThrown){
									alert(
										"The following error occured: "+
										textStatus, errorThrown
									);
									$("#ajax_loader").hide();
								},
								complete: function(){
								}
							});
						});
					</script>
				</div>

			<?php }?>

            <hr />
			<h2 style="padding-bottom: 10px;">Pending Friend Requests</h2>
            
			<?php if(count($sentrequests) == 0):?><p><i>belum ada friend requests yg anda kirim</i></p><?php endif;?>
			<?php foreach($sentrequests as $req){?>
            <div class="row-fluid">
				<div class='span1'>
					<a href="<?php echo base_url().'/comunity/profile?uid='.$req['to_account_id'];?>"><img style="float:left; padding-right:2px;" src="<?php echo icon_url($foto_sentrequest[$req['to_account_id']]);?>" /></a>
				</div>
				<div class='span11'>
					<a href="<?php echo base_url().'/comunity/profile?uid='.$req['to_account_id'];?>"><b><?php echo $req['to_display_name'];?></b></a>,
					dengan pesan: <i>"<?php echo $req['request_msg'];?>"</i>';
				</div>
            </div>
            <hr />
			<?php }?>

		</div>
	</div>
	<div class='col col-1-4 col-side' id='col-rite'>
		<div class='block'>
			<?php
			include('member/favourite_politician_view.php');
			?>
		</div>
	</div>
</div></div>

</div></div></div><!-- #main -->

<?php $this->load->view($template_path .'tpl_footer');?>
