<div class="block block-user-menu">
<?php $txtCSS = "class=\"active\"";?>
<ul class="nav nav-tabs nav-stacked">
	<li <?php if( $activePage == "wall") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "komunitas/wall"; ?>">Wall</a>
    </li>
    <li <?php if( $activePage == "photo") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "album/photo/"; ?>">Photos <span class="numberofphotoalbums pull-right">*</span></a>
    </li>
    <?php if(false) { ?>
    <li <?php if( $activePage == "video") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "uservideos"; ?>">Videos <span class="numberofvideos pull-right">*</span></a>
    </li>
    <?php } ?>
    <li <?php if( $activePage == "blogs") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "blog/"; ?>">Blogs <span class="numberofblogs pull-right">*</span></a>
    </li>
    <li <?php if( $activePage == "follow") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "comunity/follow/"; ?>">Follow <span class="numberoffollow pull-right">*</span></a>
    </li>

<?php if(!isset($guestmember['account_id']) || $member['account_id'] == $guestmember['account_id']) { ?>
    <li <?php if( $activePage == "friend_request") { echo $txtCSS; } ?>>
		  <a rel="nofollow" href="<?php echo base_url();?>comunity/friendrequest">Friend requests <span class="numberofnotification pull-right">*</span></a>
	  </li>
<!--		<li <?php if( $activePage == "member_group") { echo $txtCSS; } ?>>
		  <a rel="nofollow" href="<?php echo base_url();?>groups/LoadGroup">Groups <span class="numberofgroups pull-right">*</span></a>
		</li> -->
<?php }?>
    
<?php    
    if(false)
    {
?>
	<li <?php if( $activePage == "invite") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "komunitas/invite/"; ?>">Invite</a>
    </li>
<?php }?>
</ul>
<?php if(false) { ?>
<ul class="nav nav-tabs nav-stacked box-lite">
        <li <?php if( $activePage == "basicInfo") { echo $txtCSS; } ?>>
    	<a href="<?php echo base_url(). "comunity/profile";?>">Basic Information</a></li>
        <li <?php if( $activePage == "contactInfo") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "komunitas/contactinfo"; ?>">Contact Information</a></li>            
        
        <li <?php if( $activePage == "profilePicture") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "komunitas/profilepicture";?>">Profile picture</a></li>            

</ul><?php } ?>
</div>