<div class="block block-user-menu">
<?php $txtCSS = "class=\"active\""; ?>
<ul class="nav nav-tabs nav-stacked">
    	<li <?php if( $activePage == "wall") { echo $txtCSS; } ?>>
    	<a href="<?php echo base_url(). "komunitas/wall?uid=". $guestmember['account_id']; ?>">Wall</a></li>
    			
        <li <?php if( $activePage == "photos") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "album/photo?uid=".$guestmember['account_id']; ?>">Photos <span class="numberofphotoalbums pull-right">*</span></a></li>
        <?php if(false) { ?>
        <li <?php if( $activePage == "videos") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "uservideos?uid=".$guestmember['account_id']; ?>">Videos <span class="numberofvideos pull-right">*</span></a></li>
        <?php } ?>   
        <li <?php if( $activePage == "blogs") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "blog?uid=".$guestmember['account_id'];; ?>">Blogs <span class="numberofblogs pull-right">*</span></a></li>
        <li <?php if( $activePage == "follow") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "comunity/follow?uid=".$guestmember['account_id']; ?>">Follow <span class="numberoffollow pull-right">*</span></a></li>

	<?php if($member['account_id'] == $guestmember['account_id']) { ?>
	    <li <?php if( $activePage == "friend_request") { echo $txtCSS; } ?>>
			<a rel="nofollow" href="<?php echo base_url();?>comunity/friendrequest">Friend requests <span class="numberofnotification pull-right">*</span></a>
		</li>
<!--	    <li <?php if( $activePage == "member_group") { echo $txtCSS; } ?>>
			<a rel="nofollow" href="<?php echo base_url();?>groups/LoadGroup">Groups <span class="numberofgroups pull-right">*</span></a>
		</li> -->
	<?php } ?>

</ul>
<?php if(FALSE) { ?>
<ul class="nav nav-tabs nav-stacked box-lite">
        <li <?php if( $activePage == "basicInfo") { echo $txtCSS; } ?>>
    	<a href="<?php echo base_url(). "comunity/profile?uid=". $guestmember['account_id']; ?>">Basic Information</a></li>
        <li <?php if( $activePage == "contactInfo") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "komunitas/contactinfo?uid=". $guestmember['account_id']; ?>">Contact Information</a></li>            
        
		<?php if($member['account_id'] == $guestmember['account_id']): ?>
        <li <?php if( $activePage == "profilePicture") { echo $txtCSS; } ?>>
        <a href="<?php echo base_url(). "komunitas/profilepicture";?>">Profile picture</a></li>            
		<?php endif;?>

</ul><?php } ?>
</div>