<?php
$isSentimen = FALSE;
$this->load->view($template_path .'tpl_header');
?>
<div id='main'><div id='body' class='page'><div class='container'>

<div class='row row-col'><div class='col-border clearfix'>
	<div class='col col-1-4 col-side' id='col-left'>
		<?php
		$this->load->view('comunity/member/include_ajax');
		$this->load->view('comunity/member/guest_user_badge');
		$data['activePage'] = 'follow';
		$this->load->view('comunity/member/profile_menu', $data);
		if($member['account_id'] == $guestmember['account_id'])
			$this->load->view('komunitas/include_member/mini_friend_list');
		?>
	</div>
	<div class='col col-1-2' id='col-main'>
		<div class='block'>
        	<h2>Follows</h2>
            <hr>
        	<div id="follow_list">
            	<?php echo $follow_list; ?>
        	</div>
		</div>
	</div>
	<div class='col col-1-4 col-side' id='col-rite'>
		<div class='block'>
		<?php
		include('member/favourite_politician_view.php');
		?>
		</div>
	</div>
</div></div>

</div></div></div><!-- #main -->

<?php $this->load->view($template_path .'tpl_footer');?>
