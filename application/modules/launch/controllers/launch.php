<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Launch extends Application {


    public function __construct()
    {
        parent::__construct();

        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);

        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
    }


    public function index()
    {
        $data = $this->data;

        $this->load->view('bijaks_launch',$data);
    }

}

?>