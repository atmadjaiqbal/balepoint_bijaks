<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Topten extends Application
{
    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->helper('m_date');

        $this->load->helper('text');
        $this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');
        $this->load->module('survey');

        $this->load->library('aktor/aktor_lib');
        $this->load->library('caleg/caleg_lib');
        $this->load->library('topten_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->library('suksesi/suksesi_lib');
    }

    public function index($page=1)
    {
        redirect();

//         $data['category'] = 'topten';
//         $data['scripts']     = array('bijaks.js', 'bijaks.survey.js');
//         $data['title'] = "Bijaks | Total Politik";
//         $limit = 6; $page = 1;
//         $per_page = $this->input->get('per_page');
//         if(!is_bool($per_page)){ if(!empty($per_page)){ $page = $per_page; } }
//         $offset = ($limit*intval($page)) - $limit;
//         $qr = $_SERVER['QUERY_STRING'];

//         $countCat = $this->topten_lib->get_topten_category('count(*) as count');
//         $ResCategories = $this->topten_lib->get_topten_category('*', $where=array(), $limit, $offset);

//         if($ResCategories->num_rows == 0){
//             show_404();
//         }

//         $ResTopten = array();$i = 0;
//         foreach($ResCategories->result_array() as $rwCat)
//         {
//             $id_category = $rwCat['id_topten_category'];
//             $ResTopten[$i]['topten_id'] = $rwCat['id_topten_category'];
//             $ResTopten[$i]['categories_name'] = $rwCat['category_name'];

//             $where = array('tpt.id_topten_category' => $rwCat['id_topten_category']);
//             $select = 'tpt.*, tpc.category_name';
//             $politisi = $this->topten_lib->get_topten($select, $where, $group_by='', $order_by='tpt.no_urut');
//             $result = $politisi->result_array(); $_polisiti = array();
//             foreach($result as $key=>$value){
//                 $where = array('page_id' => $value['page_id']);
//                 $users = $this->redis_slave->get('profile:detail:'.$value['page_id']);
//                 $user_array = json_decode($users, true);
//                 $_polisiti = $user_array['page_name'];
//                 $user_array['deskripsi'] = $value['deskripsi'];
//                 $user_array['list_photo'] = $this->aktor_lib->get_foto_profile_album(40, $value['page_id'], 8, 0);
//                 $user_array['scandal_count'] = count($user_array['scandal']);
//                 $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
//                 $user_array['news_count'] = $news_terkait['cou'];
//                 $foll = $this->aktor_lib->get_follower($value['page_id']);
//                 $user_array['follower_count'] = $foll->num_rows();
//                 $ResTopten[$i]['politisi'][$key] = $user_array;
//             }
//             $i++;
//         }

//         $data['top_category'] = $ResCategories->result_array();
//         $data['politisi'] = $ResTopten;
// //        header("Access-Control-Allow-Origin: *");
// //        $this->load->view('index_view', $data);
//         $data['topic_last_activity'] =  '';

//         /*---- Metatag ----- */
//         $data['articledesc'] = '';
//         $data['polterkait'] = '';
//         $data['topnews'] = array();
//         /*---- End Metatag --- */

//         $this->load->library('pagination');
//         $config['base_url'] = base_url().'topten/index/?'; //.$qr;
//         $config['total_rows'] = intval($countCat->num_rows());
//         $config['per_page'] = $limit;
//         $config['use_page_numbers'] = TRUE;
//         $config['page_query_string'] = TRUE;
//         $choice = $config["total_rows"] / $config["per_page"];
//         $config["num_links"] = 2; // round($choice);
//         $config['full_tag_open'] = '<div class="pagi pagi-centered"><ul>';
//         $config['full_tag_close'] = '</ul></div>';
//         $config['cur_tag_open'] = '<li class="disabled"><div><a>';
//         $config['cur_tag_close'] = '</a></div></li>';
//         $config['num_tag_open'] = '<li>';
//         $config['num_tag_close'] = '</li>';
//         $config['next_link'] = '...';
//         $config['next_tag_open'] = '<li>';
//         $config['next_tag_close'] = '</li>';
//         $config['prev_link'] = '...';
//         $config['prev_tag_open'] = '<li>';
//         $config['prev_tag_close'] = '</li>';
//         $config['first_link'] = FALSE;
//         $config['last_link'] = FALSE;
//         $this->pagination->initialize($config);
//         $data['pagi'] =  $this->pagination->create_links();
//         $data['offset'] = $offset;
//         $data['total']	= intval($countCat->num_rows());
//         $html['html']['content']  = $this->load->view('index_view', $data, true);
//         $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
//         $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);;
//         $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
//         $this->load->view('template/tpl_one_column', $html);
    }

    public function toptens($id=1)
    {
        $data['scripts'] = array('bijaks.js', 'bijaks.survey.js');
        $data['title'] = "Bijaks | Total Politik";
        $data['id'] = !empty($id) ? $id : 1;

        # Fetch topten with defined id_topten_category 
        // $where = array('id_topten_category' => $id);
        // $topten = $this->topten_lib->get_topten_category($select='*', $where);
        // if($topten->num_rows == 0){ 
        //     show_404(); 
        // }

        // $topten_array = $topten->row_array(); 

        $key = 'topten:detail:'.$id;
        $topten_array = (array)json_decode($this->redis_slave->get($key));


        // $where = array('tpt.id_topten_category' => $topten_array['id_topten_category']);
        // $select = 'tpt.*, tpc.category_name';
        // $politisi = $this->topten_lib->get_topten($select, $where, $group_by='', $order_by='tpt.no_urut');
       
        // $result = array();
        // $youtube = $this->topten_lib->get_topten_youtube($select, $where, $group_by='', $order_by='tpt.no_urut');

        // $wheren = array('tn.id_topten_category' => $topten_array['id_topten_category']);
        // $topnews = $this->topten_lib->get_topten_news('tn.*, tpc.category_name', $wheren, $group_by='', $order_by='tn.no_urut');


        $params = ['toptenType'=>$topten_array['topten_type'],'toptenId'=>$topten_array['id_topten_category']];
        $keyContent = 'topten:content:'.$id;
        $tempContent = json_decode($this->redis_slave->get($keyContent));
            
        switch($topten_array['topten_type']){
            case '1' :
                $politisi = $tempContent;
                $ket = 'Keterangan : ';
                $data['ket'] = $ket; 
                $_polisiti = array();

                foreach($politisi as $key=>$value){
                    //$user_array['topten_type'] = $value['topten_type'];
                    $where = array('page_id' => $value->page_id);
                    $users = $this->redis_slave->get('profile:detail:'.$value->page_id);
                    $user_array = json_decode($users, true);
                    $_polisiti[] = $user_array['page_name'];
                    $user_array['deskripsi'] = $value->deskripsi;
                    $user_array['deskripsi_lain'] = $value->deskripsi_lain;
                    //   $where = array('page_id' => $val['caleg_id']);
                    //   $skandal_terkait = $this->caleg_lib->get_scandal_player($select='count(*) as cou', $where)->row_array(0);

                    $user_array['list_photo'] = $this->aktor_lib->get_foto_profile_album(40, $value->page_id, 8, 0);
                    $user_array['scandal_count'] = @count($user_array['scandal']);
                    $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                    $user_array['news_count'] = $news_terkait['cou'];
                    $foll = $this->aktor_lib->get_follower($value->page_id);
                    $user_array['follower_count'] = $foll->num_rows();
                    $user_array['topten_type'] = $value->topten_type;
                    $user_array['topten_id'] = $value->topten_id;


                    $rkey_min = 'news:detail:'; 
                    $berita = array();
                    $nw = 0;
                    $have_redis_empty = false; 
                    $news = array();
                    if(!empty($value->news_list)){
                        $newslist = explode(",", $value->news_list);
                        foreach($newslist as $val){
                            $berita = $this->redis_slave->get($rkey_min.$val);
                            $beritason = json_decode($berita, true);
//                       if(empty($beritason)){ $have_redis_empty = true; continue;}
                            $user_array['news'][$nw] = $beritason;
                            $nw++;
                        }
                    } else {
                        $user_array['news'] = '';
                    }

                    $result[$key] = $user_array;
                } break;

            case '2' :
                $youtube = $tempContent;
                $ket = 'Keterangan : ';
                $data['ket'] = $ket; 
                $_polisiti = array();
                foreach($youtube as $key=>$value){
                    $result[$key] = $value;
                }; 
                break;

            case '3' :
                $topnews = $tempContent;
                $ket = 'Kekayaan : ';
                $data['ket'] = $ket; 
                $_polisiti = array();
                foreach($topnews as $key => $value){
                    $rkey_min = 'news:detail:'; 
                    $berita = array();
                    $nw = 0;
                    $have_redis_empty = false; 
                    $news = array();
                    if(!empty($value->news_list)){
                      $newslist = explode(",", $value->news_list);
                      foreach($newslist as $val){
                         $berita = $this->redis_slave->get($rkey_min.$val);
                         $beritason = json_decode($berita, true);
//                       if(empty($beritason)){ $have_redis_empty = true; continue;}
                         $value->news[$nw] = $beritason;
                         $nw++;
                      }
                    } else {
                      $value->news = '';
                    }                

                    $result[$key] = $value;
                }
            break;
        }
        $this->topten_lib->updateviewer($id);
        # /Fetch topten with defined id_topten_category 

        /*
        *    Topten Lainnya
        */
        $rkey = 'topten:list';
        $toptenlist = $this->redis_slave->lrange($rkey, 0, 300);
        $restopten = array();
        foreach($toptenlist as $key => $value)
        {
            if($value == $id) continue;
            $rowTopten = $this->redis_slave->get('topten:detail:'. $value);
            $jsontopten = @json_decode($rowTopten, true);
            $restopten[$key] = $jsontopten;
        }

        //echo '<pre>';print_r($restopten);echo '</pre>';
        $data['toptenlain'] = $restopten;

        //$data['toptenlain'] = array();

        // /Fetch right side Category titles


        
        // All set and GO!
        $data['top_category'] = $topten_array;
        $data['politisi'] = $result;
        $data['topic_last_activity'] =  '';

        /*---- Metatag ----- */
        $data['articledesc'] = '';
        $data['polterkait'] = $_polisiti;
        $data['topnews'] = array($topten_array['category_name']);
        /*---- End Metatag --- */

        switch($topten_array['topten_type'])
        {
            case '1' :
                $viewName = 'topten_view';
                break;

            case '2' :
                $viewName = 'topten_youtube';
                break;

            case '3' :
                $viewName = 'topten_news';
                break;
            // case '4' :
            //     $viewName = 'topten_view';
            //     break;
            // case '5' :
            //     $viewName = 'topten_youtube2';
            //     break;
            // case '6' :
            //     $viewName = 'topten_news';
            //     break;

            default:
                $viewName = 'topten_view';    
        }

        $data['og_slug'] = "http://www.bijaks.net/topten/toptens/".$id;
        $html['html']['content'] = $this->load->view($viewName, $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);;
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html);
    }

    public function UpdateViewer()
    {
        $idCat = $this->input->post('catid');
        if(!empty($idCat))
        {
            $this->topten_lib->updateviewer($idCat);
        }
    }

}
