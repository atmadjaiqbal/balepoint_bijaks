<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>
<style type="text/css">
#topside_header{
    background-image: url('<?php echo base_url('assets/images/headers/HEADER-17.jpg');?>');
    background-repeat: no-repeat;
    height: 150px;
}
#myModal .modal-content {
    max-height: 600px;
    overflow: auto;
}

.top-image {
    width: 180px !important;
    height: 155px !important;
    margin-left: -25px !important;
} 

.thumb1{
    width: 80px;
    height: 80px;
    float: left;
    margin-right: 5px;
}
.gallery {
    list-style: none outside none;
    padding-left: 0;
    margin-left: 0px;
}
.gallery li {
    display: block;
    float: left;
    height: 80px;
    margin-bottom: 7px;
    margin-right: 0px;
    width: 88px;
}
.gallery li a {
    height: 100px;
    width: 100px;
}
.gallery li a img {
    max-width: 82px;
}
#lg-outer {
    background: rgba(0,0,0,0.6) !important;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
    $('.thumb2').click(function(){
        var target = $(this).data('homeid');
        var src = $(this).attr('src');
        $('#'+target).attr('src',src);
    });
    $(".gallery").lightGallery();
    $(".gallery2").lightGallery();
})
</script>
<div class="container" style="margin-top: 30px;">
    <div class="sub-header-container">

        <div id="topside_header">
        <a href="<?php echo base_url();?>">
            <img src="<?php echo base_url('assets/images/bijaks_logo.png'); ?>" style="z-index: 99;margin-left: 33px;margin-top:23px;width: 158px;opacity: 0.8;"/>        
        </a>
        </div>

        <div style="clear: both"></div>

    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>
</div>
<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <div class="span8">
                <h2 style="margin-bottom: 0px;"><?php echo strtoupper($top_category['category_name']); ?></h2>
                <div class="text-left" style="margin-left:0px;margin-top:0px;"><h4><?php echo date('d M Y', strtotime($top_category['created_date'])); ?></h4></div><!--div class="span6 text-right"><h4>< ?php echo $top_category['viewer']; ?> viewer</h4></div-->

                <?php 
                if(!empty($top_category['description'])){
                    echo '<div>'.$top_category['description'].'</div>';
                }
                ?>

                <br/>

                <?php
                if($politisi){
                    foreach($politisi as $key => $val)
                    {
                        ?>

                        <div class="row-fluid row-fluid-250 carousel-inner">
                            <div class="politic-section-left" id="myModal">
                                <div class="div-line-index div-line-header-1"># <?php echo $val->no_urut;?></div>
                                <?php
                                if ($id == 48 || $id == 50 || $id == 56) {
                                    ?>
                                    <ul id="light-gallery" class="gallery2">
                                        <li data-src="<?php echo $val->top_img;?>" style="width: 200px;">
                                            <a href="#">
                                                <img src="<?php echo $val->top_img;?>" class="top-image" />
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- <img class="top-image-large" src="<?php //echo $val['top_img'];?>" id="image-large-<?php //echo $val['topten_news_id'];?>"> -->
                                    <?php
                                }
                                else {
                                    ?>                                    
                                    <a href="#" data-toggle="modal" data-target="#fotomeme-<?php echo $val->no_urut;?>">
                                        <img class="top-image-large" src="<?php echo $val->top_img;?>" id="image-large-<?php echo $val->topten_news_id;?>">
                                    </a>
                                    <?php
                                }
                                ?>
                                <div class="modal fade hide" id="fotomeme-<?php echo $val->no_urut;?>" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="margin-top: -50px;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title" id="myModalLabel"><?php echo $val->top_title; ?></h4>
                                            </div>
                                            <div class="modal-body" style="text-align: center;">
                                                <img src="<?php echo $val->top_img;?>" style="height: auto;width: auto;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="politic-section-right">
                               <a style="cursor: default;">
                                 <h4 class="name-politer"><?php echo $val->top_title; ?></h4>
                               </a>
                               <div><?php echo $val->top_description; ?></div>
                               <?php if(!empty($val->news)) { ?><p>Baca disini beritanya : </p> <?php } ?>
                               <div id="topnews">
                               <?php

                               if($val->news)
                               {
                                  $dt['result'] = $val->news; $this->load->view('tpl_topten_news', $dt);
                               }
                               ?>
                               </div>

                               <div class="clearfix"></div>
                            </div>
                        </div>
                        <hr class="hr-black__">

                    <?php
                    }
                }
                ?>

            </div>

            <div class="span4">
                <div class="row-fluid" style="margin-bottom: 5px;">
                    <div class="span12">
                        <div  class="home-title-section hp-label hp-label-hitam">
                            <span class="hitam"><a style="cursor: default;">TOP 10 Lainnya</a></span>
                        </div>

                    </div>
                </div>
                <div id="topten-terkait">

                    <?php
                    if(!empty($toptenlain))
                    {
                        foreach($toptenlain as $rwlain)
                        {
                            $_bakcolor = 'style="background-color:#cecece;"';
                            switch ($rwlain['set_color'])
                            {
                                case '1' : $_bakcolor = 'style="background-color:#F2DADA;"';break;
                                case '2' : $_bakcolor = 'style="background-color:#96D3CC;"';break;
                                case '3' : $_bakcolor = 'style="background-color:#193791;"';break;
                                case '4' : $_bakcolor = 'style="background-color:#8d601f;"';break;
                                case '5' : $_bakcolor = 'style="background-color:#cecef4;"';break;
                            }
                            ?>
                            <div <?php echo $_bakcolor; ?> class="topten">
                                <a href="<?php echo base_url('topten/toptens') .'/'. $rwlain['id_topten_category'];?>">
                                    <img src="<?php echo base_url().'assets/images/topten_red.png';?>">&nbsp;<div><?php echo $rwlain['category_name']; ?></div>
                                    <p class="text-left"><?php echo date('d M Y', strtotime($rwlain['created_date'])); ?></p>
                                    <!-- p class="text-right" style="color: #bcbcbc;"><?php echo $rwlain['viewer'];?> viewers</p -->
                                </a>
                            </div>
                            <?php // $this->load->view('tpl_topten_terkait', $dt); ?>
                        <?php
                        }
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>





