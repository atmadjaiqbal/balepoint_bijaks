<style type="text/css">
#topside_header{
    background-image: url('<?php echo base_url('assets/images/headers/HEADER-17.jpg');?>');
    background-repeat: no-repeat;
    height: 150px;
}    
</style>
<div class="container" style="margin-top: 30px;">
    <div class="sub-header-container">
        <div id="topside_header">
        <a href="<?php echo base_url();?>">
            <img src="<?php echo base_url('assets/images/bijaks_logo.png'); ?>" style="z-index: 99;margin-left: 33px;margin-top:23px;width: 158px;opacity: 0.8;"/>        
        </a>
        </div>

        <div style="clear: both"></div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>
</div>
<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <div class="span8">
                <h2 style="margin-bottom: 0px;"><?php echo strtoupper($top_category['category_name']); ?></h2>
                <div class="span6 text-left" style="margin-left:0px;margin-top:0px;"><h4><?php echo date('d M Y', strtotime($top_category['created_date'])); ?></h4></div><!-- div class="span6 text-right"><h4>< ?php echo $top_category['viewer']; ?> viewer</h4></div-->
                <br/>

                <?php
                if($politisi){
                   foreach($politisi as $key => $val){
                ?>

                <div class="row-fluid row-fluid-250 carousel-inner">
                    <div class="politic-section-left">
                        <div class="div-line-index div-line-header-1"># <?php echo $val['no_urut'];?></div>
                        <img class="top-image-large" src='<?php echo $val['img'];?>'>
                    </div>
                    <div class="politic-section-right">
                        <?php
                        $a = $val['topten_link'] == 0  ? '<a style="cursor: default;">' : '<a href="'.$val['topten_link'].'">';
                        echo $a.'<h4 class="name-politer">'.$val['top_title'].'</h4></a>';
                        echo $val['description'];
                        if(!empty($val['media_url'])) {
                        ?>
                        <div style="width: 440px;height: auto;margin-bottom: 5px;">
                           <iframe width="440" height="230" src="<?php echo $val['media_url']; ?>" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <?php };
                        ?>

                        <div class="clearfix"></div>
                       
                    </div>
                </div>
                <hr class="hr-black">

                <?php
                    }
                }
                ?>

            </div>

            <div class="span4">
                <div class="row-fluid" style="margin-bottom: 5px;">
                    <div class="span12">
                        <div  class="home-title-section hp-label hp-label-hitam">
                            <span class="hitam"><a style="cursor: default;">TOP 10 Lainnya</a></span>
                        </div>

                    </div>
                </div>
                <div id="topten-terkait">

                    <?php
                    if(!empty($toptenlain)){
                        foreach($toptenlain as $rwlain){
                            $_bakcolor = 'style="background-color:#cecece;"';
                            switch ($rwlain['set_color']){
                                case '1' : $_bakcolor = 'style="background-color:#F2DADA;"';break;
                                case '2' : $_bakcolor = 'style="background-color:#96D3CC;"';break;
                                case '3' : $_bakcolor = 'style="background-color:#193791;"';break;
                                case '4' : $_bakcolor = 'style="background-color:#8d601f;"';break;
                                case '5' : $_bakcolor = 'style="background-color:#cecef4;"';break;
                            }
                            ?>
                            <div <?php echo $_bakcolor; ?> class="topten">
                                <a href="<?php echo base_url('topten/toptens/') .'/'. $rwlain['topten_id'];?>">
                                    <img src="<?php echo base_url().'assets/images/topten_red.png';?>">&nbsp;<div><?php echo $rwlain['categories_name']; ?></div>
                                    <p class="text-left"><?php echo date('d M Y', strtotime($rwlain['created_date'])); ?></p>
                                    <!-- p class="text-right" style="color: #bcbcbc;"><?php echo $rwlain['viewer'];?> viewer</p -->
                                </a>
                            </div>
                            <?php // $this->load->view('tpl_topten_terkait', $dt); ?>
                        <?php
                        }
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>





