<script>
    $(document).ready(function(){
        $('.galery-img').click(function(ev){
            ev.preventDefault();
            var uri = $(this).data('uri');
            var pg = $(this).data('pg');
            $('#main-image-' + pg).attr('src', uri);
        })

        $('.modal-body').slimscroll({
            height : '400px',
            alwaysVisible: true,
            allowPageScroll : true,
            color: '#B8B8B8',
            opacity : .7
        })

        $('#myModal').on('hidden', function(){
            $('.modal-header h3').text('');
            $('.modal-body').html('');
        })

    });

</script>
<style type="text/css">
#topside_header{
    background-image: url('<?php echo base_url('assets/images/headers/HEADER-17.jpg');?>');
    background-repeat: no-repeat;
    height: 150px;
}    
</style>
<div class="container" style="margin-top: 30px;">
    <div class="sub-header-container">
        <div id="topside_header">
        <a href="<?php echo base_url();?>">
            <img src="<?php echo base_url('assets/images/bijaks_logo.png'); ?>" style="z-index: 99;margin-left: 33px;margin-top:23px;width: 158px;opacity: 0.8;"/>        
        </a>
        </div>

        <div style="clear: both"></div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>
</div>
<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <div class="span8">
                <h2 style="margin-bottom: 0px;"><?php echo strtoupper($top_category['category_name']); ?></h2>
                <div class="span6 text-left" style="margin-left:0px;margin-top:0px;">
                    <h4>
                        <?php echo date('d M Y', strtotime($top_category['created_date'])); ?>
                    </h4>
                </div>
                <!--div class="span6 text-right"><h4>< ?php echo $top_category['viewer']; ?> viewer</h4></div-->
                <!-- div style="font-size: 12px;text-align: right;">Tanggal Tayang : < ?php echo date('d M Y', strtotime($top_category['created_date'])); ?> | < ?php echo $top_category['viewer']; ?> viewer</div-->
                <div style="clear: both;"></div>
                <?php if(!empty($top_category['description'])) { ?>
                <p style="margin-top:10px;"><?php echo $top_category['description']; ?></p>
                <?php } ?>
                <?php
                if($politisi ){
                    foreach($politisi as $key=>$value){
                        $pp = (count(@$value['profile_photo']) > 0) ? @$value['profile_photo'][0]['badge_url'] : '';
                ?>
                <div class="row-fluid row-fluid-250 carousel-inner">
                    <div class="politic-section-left">
                        <div class="div-line-index div-line-header-<?php echo $top_category['id_topten_category'] <= 4 ? $top_category['id_topten_category'] : '1';?>">
                            # <?php echo $key + 1;?>
                        </div>
                        <img id="main-image-<?php echo @$value['page_id'];?>" class="top-image-large" src="<?php echo $pp;?>">
                        <!--div class="score-place score-place-overlay score" data-id="<?php echo $value['page_id']; ?>" ></div-->
                        <?php if($value['topten_type'] == '0') { ?>
                        <a target="__blank" class="a-block a-block-<?php echo (key_exists('id_topten_category', $top_category)) ? $top_category['id_topten_category'] : '1';?>" href="<?php echo base_url();?>aktor/scandals/<?php echo @$value['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $value['scandal_count'];?></span></a>
                        <a target="__blank" class="a-block" href="<?php echo base_url();?>aktor/news/<?php echo @$value['page_id'];?>">BERITA <span class="clearfix"><?php echo $value['news_count'];?></span></a>
                        <a target="__blank" class="a-block a-block-<?php echo (key_exists('id_topten_category', $top_category)) ? $top_category['id_topten_category'] : '1';?>" href="<?php echo base_url();?>aktor/follow/<?php echo @$value['page_id'];?>">FOLLOWER <span class="clearfix"><?php echo $value['follower_count'];?></span></a>
                        <?php } ?>
                    </div>
                    <div class="politic-section-right">
                        <?php
                        $nm_arry = explode(' ', strtoupper(@$value['page_name']));
                        $first_nm = $nm_arry[0];
                        $nm_arry[0] = '';

                        $next_nm_array = implode(" ", $nm_arry);
                        if(@$value['badge_partai_url'] != 'None'){
                            ?>
                            <img class="top-image-partai" src="<?php echo @$value['badge_partai_url']; ?>">
                            <h3 class="name-politer">
                            <?php if(@$value['topten_type'] == '0') { ?>
                                <a href="<?php echo base_url('aktor/profile/') .'/'. @$value['page_id'];?>"><?php echo strtoupper(@$value['page_name']); // trim($first_nm);?></a>
                            <?php } else { ?>
                                <?php echo strtoupper(@$value['page_name']); // trim($first_nm);?>
                            <?php } ?>
                            </h3>
                            <?php if(!empty(@$value['deskripsi'])){ ?>
                                <h4 class="name-politer-desc"><?php echo $ket .' : '. @$value['deskripsi'];?></h4>
                            <?php } ?>
                            <!--                --><?php //if(count($nm_arry) > 1){ ?>
                            <!--                <h3 class="name-politer"><a target="__blank" href="--><?php //echo base_url('aktor/profile/') .'/'. $value['page_id'];?><!--">--><?php //echo trim($next_nm_array);?><!--</a></h3>-->
                            <!--               --><?php //} ?>
                            <div class="clearfix"></div>
                        <?php }else{ ?>
                            <?php if($value['topten_type'] == '0') { ?>
                            <h3 class="name-politer"><a href="<?php echo base_url('aktor/profile/') .'/'. $value['page_id'];?>"><?php echo strtoupper($value['page_name']);?></a></h3>
                            <?php } else { ?>
                            <h3 class="name-politer"><a href="#" style="cursor:default;"><?php echo strtoupper($value['page_name']);?></a></h3>
                            <?php } ?>
                            <?php if(!empty($value['deskripsi'])){ ?>
                                <h4 class="name-politer-desc"><?php echo $ket . $value['deskripsi'];?></h4>
                            <?php } ?>
                        <?php } ?>
                        <p></p>
                        <div class="politer-galery " >
                            <div class="politer-galery-container" data-zount="<?php echo count($value['list_photo']) - 1;?>">
                                <?php $_i = 1;foreach ($value['list_photo'] as $kes => $vas) {
                                    if($_i > 5) break;
                                    ?>
                                    <img data-uri='<?php echo 'http://www.bijaks.net/public/upload/image/politisi/'.$vas['page_id'].'/badge/'.$vas['attachment_title'];?>' data-pg="<?php echo $vas['page_id'];?>" class="img-politer-galery galery-img" src="<?php echo 'http://www.bijaks.net/public/upload/image/politisi/'.$vas['page_id'].'/thumb/'.$vas['attachment_title'];?>">
                                    <?php $_i++;} ?>
                            </div>
                        </div>
                        <?php if(!empty($value['deskripsi_lain'])){ ?>
                            <p><?php echo $value['deskripsi_lain'];?></p>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <p><?php echo character_limiter(nl2br(@$value['about']), 240);?></p>

                        <?php
    /*                    $data['topten_type'] = $value['topten_type'];
                        $data['topten_id'] = $value['topten_id'];

                        if($value['topten_type'] == '2')
                        {
                            $this->load->view('tpl_topten_info',$data);
                        } */
                        ?>


                        <?php if(!empty($value['news'])) { ?><p>Baca disini beritanya : </p> <?php } ?>
                        <div id="topnews">
                        <?php
                            if($value['news']){ $dt['result'] = $value['news']; $this->load->view('tpl_topten_news', $dt); }
                        ?>
                        </div>

                     </div>
                </div>

                        <hr class="hr-black">

                    <?php
                    }
                }
    ?>
       </div>

          <div class="span4">
              <div class="row-fluid" style="margin-bottom: 5px;">
                  <div class="span12">
                      <div  class="home-title-section hp-label hp-label-hitam">
                          <span class="hitam"><a style="cursor: default;">TOP 10 Lainnya</a></span>
                      </div>

                  </div>
              </div>
              <div id="topten-terkait">

          <?php
            if(!empty($toptenlain))
            {
                foreach($toptenlain as $rwlain)
                {
                    $_bakcolor = 'style="background-color:#cecece;"';
                    switch ($rwlain['set_color'])
                    {
                        case '1' : $_bakcolor = 'style="background-color:#F2DADA;"';break;
                        case '2' : $_bakcolor = 'style="background-color:#96D3CC;"';break;
                        case '3' : $_bakcolor = 'style="background-color:#193791;"';break;
                        case '4' : $_bakcolor = 'style="background-color:#8d601f;"';break;
                        case '5' : $_bakcolor = 'style="background-color:#cecef4;"';break;
                    }
          ?>
              <div <?php echo $_bakcolor; ?> class="topten">
                <a href="<?php echo base_url('topten/toptens') .'/'. $rwlain['id_topten_category'];?>">
                  <img src="<?php echo base_url().'assets/images/topten_red.png';?>">&nbsp;<div><?php echo $rwlain['category_name']; ?></div>
                  <p class="text-left"><?php echo date('d M Y', strtotime($rwlain['created_date'])); ?></p>
                  <!-- p class="text-right" style="color:#bcbcbc;">< ?php echo $rwlain['viewer'];?> viewer</p -->
                </a>
              </div>
                    <?php // $this->load->view('tpl_topten_terkait', $dt); ?>
          <?php
                }
            }
          ?>

              </div>
          </div>

     </div>


   </div>





</div>
<br/>
