<!DOCTYPE HTML>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <meta charset="utf-8">
    <title>Bijaks - <?php echo (key_exists('category_name', $top_category)) ? ucwords(strtolower($top_category['category_name'])) : '';?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.politik.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.aktor.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.profile.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.suksesi.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.komunitas.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>

    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscrollHorizontal.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/lazyload/jquery.lazyload.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/lazyload/jquery.scrollstop.js"></script>

    <script type="text/javascript">
        var Settings = <?php
	     $settings = array(
	             'base_url' => base_url()
	     );
	     echo json_encode($settings);
	     ?>;

    </script>

    <?php if (!empty($scripts)) : ?>
        <?php foreach ($scripts as $script) : ?>
            <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })
                (window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-40169954-1', 'http://www.bijaks.net');
        ga('send', 'pageview');
    </script>

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>

    <link href="<?php echo base_url(); ?>assets/css/bijaks.politer.css" rel="stylesheet">
    <!--    <link href="--><?php //echo base_url(); ?><!--assets/css/bootstrap-responsive.css" rel="stylesheet">-->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
    <![endif]-->

    <script>
        $(document).ready(function(){
            $('.galery-img').click(function(ev){
                ev.preventDefault();
                var uri = $(this).data('uri');
                var pg = $(this).data('pg');
                $('#main-image-' + pg).attr('src', uri);
            })
            var img_size = 130;
/*
            $(".politer-galery-container").each(function(i, x){
//            e.preventDefault();
                var co = $(x).data('zount');
                var width = parseInt(co) * img_size;
                console.debug(width);
                $(x).width(width );
            });
*/
//        console.debug($(".politer-galery-container").width());
/*
            $('.politer-galery').slimScrollH({
                height : '134px',
                width: '520px',
                alwaysVisible: true,
                allowPageScroll : true,
                railColor : '#333',
                color: '#B8B8B8',
                opacity : 1,
                railOpacity : 1,
                distance : '3px'
            });
*/
            $('.modal-body').slimscroll({
                height : '400px',
                alwaysVisible: true,
                allowPageScroll : true,
                color: '#B8B8B8',
                opacity : .7
            })

            $('#tentang').click(function(e){
                e.preventDefault();
                $('.modal-header h3').text('TENTANG BIJAKS');
                $.get('<?php echo base_url('page/about');?>', function(dt){
                    $('.modal-body').html(dt);
                })
                $('#myModal').modal('show');
            });

            $('#kontak').click(function(e){
                e.preventDefault();
                $('.modal-header h3').text('KONTAK BIJAKS');
                $.get('<?php echo base_url('page/contact');?>', function(dt){
                    $('.modal-body').html(dt);
                })
                $('#myModal').modal('show');
            });

            $('#privasi').click(function(e){
                e.preventDefault();
                $('.modal-header h3').text('KEBIJAKAN PRIVASI');
                $.get('<?php echo base_url('page/privacy');?>', function(dt){
                    $('.modal-body').html(dt);
                })
                $('#myModal').modal('show');
            });
            $('#ketentuan').click(function(e){
                e.preventDefault();
                $('.modal-header h3').text('SYARAT DAN KETENTUAN');
                $.get('<?php echo base_url('page/term_condition');?>', function(dt){
                    $('.modal-body').html(dt);
                })
                $('#myModal').modal('show');
            });

            $('#myModal').on('hidden', function(){
                $('.modal-header h3').text('');
                $('.modal-body').html('');
            })

        });

    </script>

</head>
<body>
<div class="container" style="margin-top: -120px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right category-news-title-merah">
                    <h1><?php echo strtoupper($top_category['category_name']); ?></h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>
</div>
