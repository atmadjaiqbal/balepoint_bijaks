<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span5"><h3>Profile</h3></div>
                    <div class="span7 text-right">
                        <?php if(is_array($this->member)){?>
                        <?php if($user['user_id'] != $this->member['user_id']){ ?>
                        <a data-tipe="<?=($is_teman) ? '1' : '2';?>" data-id="<?=$this->member['account_id'];?>" data-user="<?=$user['account_id'];?>" id="teman" href="#" class="btn-flat btn-flat-large btn-flat-dark"> <?=(!$is_teman) ? 'TAMBAH TEMAN <i class="icon-ok icon-white"></i>' : 'HAPUS TEMAN <i class="icon-remove icon-white"></i>';?></a>
                        <a data-tipe="<?=($is_follow) ? '1' : '2';?>" data-id="<?=$user['user_id'];?>" id="follow_user" href="#" class="btn-flat btn-flat-large btn-flat-dark"><?=(!$is_follow) ? 'FOLLOW <i class="icon-ok icon-white"></i>' : 'UNFOLLOW <i class="icon-remove icon-white"></i>';?></a>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <br>
                <?php if($this->session->flashdata('msg')){ ?>
                <div class="alert alert-info fade in">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?=$this->session->flashdata('msg');?>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('rcode')){?>
                <div class="alert alert-error fade in">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?=$this->session->flashdata('msg');?>
                    <?=$this->session->flashdata('current_password');?>
                    <?=$this->session->flashdata('new_password');?>
                    <?=$this->session->flashdata('confirm_password');?>
                </div>
                <?php } ?>
                <?php $abot = trim($user['about']); if(!empty($abot)){?>
                <p class=""><em><?=$abot;?></em></p>
                <?php } ?>
                <div class="row-fluid" id="info">
                    <div class="span6">
                        <h4>Informasi Dasar</h4>
                        <div id="info_dasar" class="">
                        <?php if(!empty($user['fname'])){?>
                            <p class="komu-profile-info"><?=$user['fname'];?></p>
                        <?php }else{ ?>
                            <p class="komu-profile-info-null"><em>First Name</em></p>
                        <?php } ?>

                        <?php if(!empty($user['lname'])){?>
                            <p class="komu-profile-info"><?=$user['lname'];?></p>
                        <?php }else{ ?>
                            <p class="komu-profile-info-null"><em>Last Name</em></p>
                        <?php } ?>

                        <?php if(!empty($user['gender'])){?>
                            <p class="komu-profile-info"><?=($user['gender'] == 'M') ? 'Laki Laki' : 'Perempuan';?></p>
                        <?php }else{ ?>
                            <p class="komu-profile-info-null"><em>Jenis Kelamin</em></p>
                        <?php } ?>

                        <?php if(!empty($user['birthday'])){?>
                            <p class="komu-profile-info"><?=mdate('%d %M %Y', strtotime($user['birthday']));?></p>
                        <?php }else{ ?>
                            <p class="komu-profile-info-null"><em>Tanggal Lahir</em></p>
                        <?php } ?>

                        <?php if(!empty($user['current_city'])){?>
                            <p class="komu-profile-info"><?=$user['current_city'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Lokasi</em></p>
                        <?php } ?>

<!--                        --><?php //$abot = trim($user['about']); if(!empty($abot)){?>
<!--                        <p class="komu-profile-info">--><?//=$user['about'];?><!--</p>-->
<!--                        --><?php //}else{ ?>
<!--                        <p class="komu-profile-info-null"><em>Tentang</em></p>-->
<!--                        --><?php //} ?>

                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <a id="edit_dasar" class="btn-flat btn-flat-large btn-flat-dark pull-right">EDIT</a>
                        <?php } ?>
                        </div>

                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <div id="edit_info_dasar" class="hide">
                            <form id="edit_form_dasar" action="">
                                <input type="hidden" name='id' value="<?=$user['user_id'];?>">
                                <input placeholder="nama depan" name="fname" type="text" class="span12 komu-profile-info" value="<?=$user['fname'];?>">
                                <input placeholder="nama belakang" name="lname" type="text" class="span12 komu-profile-info" value="<?=$user['lname'];?>">
                                <select name="gender" class="span12 komu-profile-info-select">
                                    <option value="M" <?=($user['gender'] == 'M') ? 'checked':'';?>>Laki Laki</option>
                                    <option value="F" <?=($user['gender'] == 'F') ? 'checked':'';?>>Perempuan</option>
                                </select>
                                <input placeholder="tanggal lahir" id="dob" name="birthday" type="text" class="span12 komu-profile-info" value="<?=$user['birthday'];?>">
                                <input placeholder="lokasi" name="current_city" type="text" class="span12 komu-profile-info" value="<?=$user['current_city'];?>">
                                <textarea placeholder="about" rows="3" class="span12 komu-profile-info" name="about"><?=$user['about'];?></textarea>

                                <div class="text-right">
                                <img id="dasar_loading" class="hide" src="<?=base_url('assets/images/loading.gif');?>">
                                <input id="batal_dasar" type="reset" value="BATAL" class="btn-flat btn-flat-large btn-flat-dark">
                                <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-dark">
                                </div>
                            </form>
                        </div>
                        <?php } ?>

                    </div>

                    <div class="span6">
                        <h4>Informasi Kontak</h4>
                        <div id="info_kontak">
                        <?php if(!empty($address['address'])){?>
                        <p class="komu-profile-info"><?=$address['address'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Alamat</em></p>
                        <?php } ?>

                        <?php if(!empty($address['city_state'])){?>
                        <p class="komu-profile-info"><?=$address['city_state'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Kota</em></p>
                        <?php } ?>

                        <?php if(!empty($address['province_state'])){?>
                        <p class="komu-profile-info"><?=$address['province_state'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Propinsi</em></p>
                        <?php } ?>

                        <?php if(!empty($address['country'])){?>
                        <p class="komu-profile-info"><?=$address['country'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Negara</em></p>
                        <?php } ?>

                        <?php if(!empty($address['postalcode'])){?>
                        <p class="komu-profile-info"><?=$address['postalcode'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Kode Pos</em></p>
                        <?php } ?>

                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <a id="edit_kontak" class="btn-flat btn-flat-large btn-flat-dark pull-right">EDIT</a>
                        <?php } ?>
                        </div>

                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <div id="edit_info_kontak" class="hide">
                            <form id="edit_form_kontak" action="">
                                <input type="hidden" name='id' value="<?=(!empty($address['user_id']))? $user['user_id']:'0';?>">
                                <input placeholder="alamat" name="address" type="text" class="span12 komu-profile-info" value="<?=(!empty($address['address']))? $address['address'] : '';?>">

                                <input placeholder="kota" name="city_state" type="text" class="span12 komu-profile-info" value="<?=(!empty($address['address']))? $address['city_state'] : '';?>">
                                <input placeholder="propinsi" name="province_state" type="text" class="span12 komu-profile-info" value="<?=(!empty($address['address']))? $address['province_state'] : '';?>">
                                <input placeholder="negara" name="country" type="text" class="span12 komu-profile-info" value="<?=(!empty($address['address']))? $address['country'] : '';?>">
                                <input placeholder="kode pos" name="postalcode" type="text" class="span4 komu-profile-info" value="<?=(!empty($address['address']))? $address['postalcode'] : '';?>">


                                <div class="text-right">
                                    <img id="kontak_loading" class="hide" src="<?=base_url('assets/images/loading.gif');?>">
                                    <input id="batal_kontak" type="reset" value="BATAL" class="btn-flat btn-flat-large btn-flat-dark">
                                    <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-dark">
                                </div>
                            </form>
                        </div>
                        <?php } ?>

                    </div>
                </div>

                <?php if($user['user_id'] == $this->member['user_id']){ ?>
                <div class="row-fluid" id="">
                    <div class="span6">
                        <h4>Ganti Password</h4>
                        <form id="change_password" action="<?=base_url('komunitas/change_password');?>" method="post">
                            <input type="hidden" name='id' value="<?=$user['user_id'];?>">
                            <input placeholder="current password" name="current_password" type="password" class="span12 komu-profile-info" value="">
                            <input placeholder="new password (min 6 karakter)" name="new_password" type="password" class="span12 komu-profile-info" value="">
                            <input placeholder="confirm password (min 6 karakter)" name="confirm_password" type="password" class="span12 komu-profile-info" value="">
                            <div class="text-right">
                                <input id="" type="reset" value="BATAL" class="btn-flat btn-flat-large btn-flat-dark">
                                <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-dark">
                            </div>
                        </form>
                    </div>
                </div>
                <?php } ?>


            </div>
        </div>
    </div>
</div>


<!--- ###### MODAL #######-->
<div id="modal_teman" class="modal hide fade">
    <div class="modal-body">
        <div class="row-fluid">
            <h4>Kirim pesan sebagai teman.</h4>
            <form id="submit_friend">
                <input name="id" type="hidden" value="" id="fid">
                <input id="report_message" type="text" name="message" class="media-input comment_type span12">

                <div class="pull-right">
                    <input type="reset" id="reste" value="Batal" class="btn-flat btn-flat-gray">
                    <input type="submit" id="report" value="Kirim" class="btn-flat btn-flat-gray">
                </div>

            </form>
        </div>
    </div>
</div>