<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span8"><h3>Blog</h3></div>
                    <div class="span4 text-right">
                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <a href="<?=base_url().'komunitas/blog/'.$user['user_id'].'/tambah/'.$val['content_id'].'/'.url_title($val['title']);?>" class="btn-flat btn-flat-large btn-flat-dark">EDIT BLOG</a>
                        <?php } ?>
                    </div>
                </div>
                <br>
                <div class="row-fluid">
                    <div class="row-fluid">
                        <h4 class="komu-blog-title"><?=$val['title'];?></h4>
                    </div>

                    <div class="content-info">
                        <p style="float: left" class="komu-blog-date"><?=mdate('%M %d, %Y - %h:%s', strtotime($val['entry_date']));?></p>
                        <div  class="score-place-min score" data-tipe="1" data-id="<?php echo $val['content_id']; ?>" >
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <p class="komu-blog-content">
                                <?=$val['content_blog'];?>
                            </p>
                        </div>
                    </div>

                </div>
                <!-- KOMENTAR -->
                <div class="div-line"></div>
                <div class="row-fluid">

                    <h5>BERI KOMENTAR</h5>
                    <div class="row-fluid">
                        <div class="comment-side-left">
                            <div style="background:
                                    url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                            </div>
                        </div>

                        <div class="comment-side-right">
                            <div class="row-fluid">
                                <input id='comment_type' data-id="<?=$val['content_id'];?>" type="text" name="comment" class="media-input" >
                            </div>
                            <div class="row-fluid">
                                <div class="span12 text-right">
<?php if(!is_array($this->member)){?>
    <span>Login untuk komentar</span>&nbsp;
    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
    <?php }elseif(is_array($this->member)){?>
                                    <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="comment" data-page="1" data-id="<?=$val['content_id'];?>" class="row-fluid comment-container">

                </div>
                <div class="row-fluid komu-follow-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a data-page="1" data-id="<?=$val['content_id'];?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>
                <div class="div-line"></div>

            </div>
        </div>
    </div>
</div>