<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span12"><h3>Friends</h3></div>
                </div>
                <br>

                <ul class="nav nav-tabs" id="friendTab">
                    <li class="active"><a data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="1" href="#friend">FRIEND</a></li>
                    <?php if($this->member['user_id'] == $user['user_id']){?>
                    <li><a data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="2" href="#request">FRIEND REQUEST</a></li>
                    <li><a data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="3" href="#pending">PENDING FRIEND REQUEST</a></li>
                    <? } ?>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="friend">
                        <div data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="1" id="friend_list"></div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a data-page="2" data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="1" id="friend_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                    </div>
<?php if($this->member['user_id'] == $user['user_id']){?>
                    <div class="tab-pane" id="request">
                        <div data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="2" id="request_list"></div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a data-page="2" data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="2" id="request_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="pending">
                        <div data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="3" id="pending_list"></div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a data-page="2" data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="3" id="pending_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                    </div>
<?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>