<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>

<!-- BLOG -->
<script type="text/javascript" src="<?=base_url();?>assets/js/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#blog_area",
        menubar:false,
        statusbar: false,
        height: 250,
        relative_urls : false,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste jbimages"
        ],
        toolbar: "insertfile undo redo | paste styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"

    });





</script>

<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span12"><h3>Buat Blog</h3></div>
                </div>
                <br>

                <div class="row-fluid">
                    <div class="span12 komu-blog-post">
                        <form id="add_blog_form" name="add_blog_form" method="post" action="">
                            <input name="cid" type="hidden" value ="<?=(!empty($blog['content_id'])) ? $blog['content_id'] : '0';?>" />
                            <input name="user" type="hidden" value ="<?php echo $user['user_id'];?>" />
                            <input name="id" type="hidden" value ="<?php echo $user['account_id'];?>" />
                             <input class="input-flat input-flat-large" type="text" placeholder="Blog title" name="title" value="<?=(!empty($blog['title'])) ? $blog['title'] : '';?>"><br>
                            <textarea id="blog_area" name="content_blog" style="width:100%"><?=(!empty($blog['content_blog'])) ? $blog['content_blog'] : '';?></textarea>
                            <div class="pull-right">
                                <input type="submit" class="btn-flat btn-flat-dark" id="wall_post_blog" value="Kirim">
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

    <script>
        $(function(){
//            $('#wall_post_blog').click(function(){

            $('#add_blog_form').submit(function(event){

                event.preventDefault();

                var please = '<div id="comment_loader" class="loader"></div>';
                $(this).prepend(please);
                $('#comment_loader').css('visibility', 'visible');

                tinyMCE.triggerSave();
                var ts = $('#blog_area').html( tinymce.get('blog_area').getContent() );
                var htm_tini = tinymce.get('blog_area').getContent();
                var found = $('.blg-img', htm_tini);
                var dt = $(this).serialize();
                found.each(function(e, b){
                    dt += "&nm[]="+$(b).data('nm');
                })

                $.post('<?php echo base_url().'timeline/post_blog';?>', dt, function(data){
                    if(data.rcode == 'ok'){
                        window.location = data.msg;
                    }else{
                        $('#comment_loader').remove();
                        $('#add_blog_form').prepend(data.msg);
                    }
                })

                return false;

//                event.preventDefault();
            });

        });
    </script>