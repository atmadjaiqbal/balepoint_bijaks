<?php $this->load->view($template_path .'tpl_header');?>

<div id='body' class='page page-news page-skandal'>
   <div class='container'>
      <div class='row row-col'><div class='col-border clearfix' style='border-left: none'>
      	<div class='col col-3-4' id='page-news-list' style="width:700px;">
      		<div class='block'>
      		   <h2>Survey</h2><hr />
      		   <div class="scandal-content">
      		   <?php $this->load->view('survey/survey_list'); ?>
      		   
      			<?php if ($survey['total_rows'] > 10 && $load_more): ?>
      				<div id="loadmore" class=""></div>
      				<div class="row-fluid load-more-loader"></div>
      				<div class="row-fluid load-more-wrapper">
      				   <a id="read-loadmore" data-url="<?php echo base_url() .'survey/load_more/';?>" data-page="<?php echo $page;?>">Load More</a>
      				</div>
      			<?php endif; ?>
      			</div> <!-- #scandal-content -->
      		</div>
      	</div>	
      	<div class='col col-1-4 col-side' id='col-rite' style="width:239px;">
      		  <?php $this->load->view('core/sidebar_sentimen'); ?>
      	</div>
      </div></div>
   </div>
</div>

<script src="<?php echo base_url(); ?>public/script/jquery/highcharts.js"></script>
<script src="<?php echo base_url(); ?>public/script/jquery/modules/exporting.js"></script>

<?php $this->load->view($template_path .'tpl_footer');?>