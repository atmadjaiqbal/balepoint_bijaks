<?php $this->load->view($template_path .'tpl_header');?>

<div id='body' class='page page-news page-skandal'>
   <div class='container'>
      <div class='row row-col'><div class='col-border clearfix' style='border-left: none'>
      	<div class='col col-3-4' id='page-news-list' style="width:704px;">
      		<div class='block'>
      		   
      		   <div class="scandal-content">


               <?php if(!empty($survey)):?>
                  <?php foreach ($survey['list'] as $key => $row) :?>
                  <?php $url = base_url() .'survey/index/'. $row['survey_id'] . "-" . urltitle($row['name']) ;?>
               
                     <div class="content-place">
                        <div class="row-fluid">
                           <h4><a href="<?php echo $url;?>"><?php echo $row['name']; ?></a> </h4>
                           
                           <div class="more-space content_<?php echo $row['content_id'];?>"><?php echo $row['content_count'];?></div>
                           
                           <div style="padding:10px 0px 5px 0px;">
                           	<?php echo textWrap(nl2br($row['desc'])); ?>
                           </div>
               		   </div>
                     </div>
                     <div style="clear:both"></div>

      			
                     <?php if(!empty($row['question'])):?>
                        <?php foreach ($row['question'] as $keyq => $q) :?>
                           <div class="content-place" style="padding-bottom:10px;">
                           <?php echo $q['question'];?>
                           
                           <?php if(!empty($q['option'])):?>
		                           <?php 
		                           $is_answer = '0';
		                           $answer_val = '';
		                           $answer_date = '';
											foreach($answer as $qa) {
												if ($qa['question_id'] == $q['question_id'] && $qa['is_answer']  == '1') {
													$is_answer = '1';
				                           $answer_val = $qa['answer'];
				                           $answer_date = $qa['answer_date'];
												}
					                  }                            
		                           
		                           ?>
                                 <?php if ($is_answer =='0'): ;?>
                                 <?php if($memberlogin): ;?>
                                    <form name="myform" id="myform" action="<?php echo $url;?>" method="post">
                                    <input type="hidden" name="question_id" value="<?php echo $q['question_id']; ?>" />
                                    <input type="hidden" name="survey_id" value="<?php echo $q['survey_id']; ?>" />
                                    <div class="controls">
                                    <?php foreach ($q['option'] as $keya => $a) :?>
                                       <input type="hidden" name="answer_<?php echo $a['question_option_id'];?>" value="<?php echo $a['name']; ?>" />
                                       <label class="radio">
                                          <input type="radio" value="<?php echo $a['question_option_id'];?>" name="question_option_id"><?php echo $a['name'];?>
                                       </label>
                                    <?php endforeach;?>

                                    </div>
                                    <div><input type="submit" value="Jawab" class="btn"></div>           
                                    </form>
                                 <?php else:?>
                                    <div style="margin-top:20px;font-style: italic;font-weight: bold">
                                        Silakan Login Terlebih dahulu untuk melakukan Vote.</div>
                                 <?php endif;?>
                                 <?php else:?>
                                    <div class="controls">
                                    <?php foreach ($q['option'] as $keya => $a) :?>
                                       <label class="radio">
                                          -  <?php echo $a['name'];?>
                                       </label>
                                    <?php endforeach;?>   
                                    </div>   
                                    <?php $answer_date = mdate("%d %M %Y - %h:%i", strtotime($answer_date));?>
                                    Anda telah memilih : <b><?php echo $answer_val;?></b> pada  <b><?php echo $answer_date;?></b>.
                                 <?php endif;?>
                           <?php endif;?>
			                  <?php         
									$values = $legends = array(); $i = 0;
				               $chartData = array();
			
			                  $chartData['title'] = $q['question'];		
			                  
			                  foreach($q['option'] as $opt) {
			                  	$legends[] = ucwords($opt['name']);
			                     //if(isset($opt['votes'])) { $persen = $opt['votes']; } else { $persen = 0; }
			                     //$values[] = $persen;
			                     $values[] = (isset($opt['votes'])) ? $opt['votes'] : 0;
			                  }  
			
				               if (!empty($values)) {
				                  $chartData['width']        = 222; 
				                  $chartData['height']       = 350;
				                  $chartData['values']       = $values;
				                  $chartData['legends']      = $legends;
				                  $chartData['question_id']  = $q['question_id'];
				                  $this->load->view('survey/survey_chart', $chartData); 
				               }
				               ?>
 									</div>
                        <?php endforeach;?>
                     <?php endif;?>
                     <div style="clear:both"></div><hr/>
<!--
                     <div class="content-place"> 
                     <?php            
                        
                        foreach($row['question'] as $key => $val) {
	                        $values = $legends = array(); $i = 0;
	                        $chartData = array();

                           $chartData['title'] = $val['question'];		
                           
                           foreach($val['option'] as $opt) {
                           	$legends[] = ucwords($opt['name']);
                              if(isset($opt['votes'])) { $persen = $opt['votes']; } else { $persen = 0; }
                              $values[] = $persen;
                           }  

	                        if (!empty($values)) {
	                           $chartData['width']        = 222; 
	                           $chartData['height']       = 350;
	                           $chartData['values']       = $values;
	                           $chartData['legends']      = $legends;
	                           $chartData['question_id']  = $row['content_id'];
	                           $this->load->view('survey/survey_chart', $chartData); 
	                        }

                        }  
                     ?>
                     </div>
-->                     
                     <div style="clear:both"></div><hr/>
                     
                     <div class="content-place">
   					      <div class="more-space">Beri komentar:</div>
                  			<input type="hidden" id="ref2" value="0" />
                  			<div id="conten-comment-wrapper"></div>
   					   </div>
   					   <script>
   	                  $('#conten-comment-wrapper').load('<?php echo base_url(); ?>ajax/comment_load/', { 'id':'<?php echo $row['content_id']; ?>', 'content_date':'<?php echo $row['content_date']; ?>' });
   	               </script>         			                     

                  <?php endforeach;?>
               <?php endif;?>



      		   
      			</div> <!-- #scandal-content -->
      		</div>
      	</div>	
      	<div class='col col-1-4 col-side' id='col-rite' style="width:235px;">
      		  <?php $this->load->view('core/sidebar_sentimen'); ?>
      	</div>
      </div></div>
   </div>
</div>

<script src="<?php echo base_url(); ?>public/script/jquery/highcharts.js"></script>
<script src="<?php echo base_url(); ?>public/script/jquery/modules/exporting.js"></script>

<?php $this->load->view($template_path .'tpl_footer');?>