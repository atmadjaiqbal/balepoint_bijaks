<?php if(!empty($survey)):?>
   <?php foreach ($survey['list'] as $key => $row) :?>
   <?php $url = base_url() .'survey/index/'. $row['survey_id'] . "-" . urltitle($row['name']) ;?>

      <div class="content-place">
         <div class="row-fluid">
            <h4><a href="<?php echo $url;?>"><?php echo $row['name']; ?></a> </h4>

            <div class="row-fluid">
               <div class="pull-left more-space"><?php echo $row['content_count'];?></div>
               <div class="pull-right">
                   <a class="btn " href="<?php
                    if($memberlogin) {
                        echo $url;
                    } else {
                       echo $this->data['base_url'].'home/login/?redirect='.$this->current_url;
                    } ?>/">Vote</a>
               </div>
            </div>

            <div style="padding:2px 0px 5px 0px;"><?php echo $row['desc']; ?></div>

            <?php

               foreach($row['question'] as $key => $val) {
                  $values = $legends = $option_id = $option_value = $series_data = array(); $i = 0;
                  $chartData = array();

                  $chartData['title'] = $val['question'];

                  foreach($val['option'] as $opt) {
                     $legends[]        = ucwords($opt['name']);
                     $values[]         = (isset($opt['votes'])) ? $opt['votes'] : 0;
                     $option_id[]      = (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0;
                     $option_value[]   = (isset($opt['name'])) ? $opt['name'] : '';
                     $series_data[]    = array(
                        'name'=> ucwords($opt['name']),
                        'id' => (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0,
                        'voted' => (isset($opt['votes'])) ? $opt['votes'] : 0
                     );
                  }
                  if (!empty($values)) {
                     $chartData['width']        = 222;
                     $chartData['height']       = 350;
                     $chartData['values']       = $values;
                     $chartData['legends']      = $legends;
                     $chartData['show_legend']  = 'true';
                     $chartData['question_id']  = $val['question_id'];
                     $chartData['survey_id']    = $row['survey_id'];
                     $chartData['option_id']    = $option_id;
                     $chartData['option_value'] = $option_value;
                     $chartData['series_data']  = $series_data;
                     $chartData['url']          = $url .'?source=survey_list';
                     $chartData['memberlogin']  = $memberlogin;
                     $this->load->view('survey/survey_chart', $chartData);
                  }

               }
            ?>
         </div>
      </div>
      <div style="clear:both"></div><hr/>
   <?php endforeach;?>
<?php endif;?>
