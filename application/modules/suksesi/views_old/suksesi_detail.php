<?php $this->load->view($template_path .'tpl_header');?>
<div id='body' class='page page-news page-suksesi'>

	<div class='container'>
		<div class='row row-col'><div class='col-border clearfix' style='border-left: none'>
			<div class='col col-3-4' id='page-news-list' style="width:704px;">
				<div class='block'>
					<?php if(!empty($suksesi['list'])):?>
						<div class="scandal-content">
						<?php foreach ($suksesi['list'] as $key => $row) :?>
							<?php $rid = $key;?>
							<?php $url = base_url().'suksesi/index/'. $row['id_race'] . "-" . urltitle($row['race_name']) . "/";?>
							<div class="content-place">

								<h4 style="padding:0;"><a href="<?php echo $url; ?>"><?php echo $row['race_name']; ?></a></h4>
								<p class="content_<?php echo $row['content_id'];?>"><?php echo $row['content_count'];?></p>

								<div class="row-fluid">
									<div class="span7 pull-left">
										<?php if (!empty($row['photos'])) :?>
											<div id='carousel-scandal-<?=$rid?>' class='carousel-scandal span12' style="height:100%">
												<div class='carousel slide' style="margin-bottom:0px;">
													<div class='carousel-inner'>
														<?php foreach ($row['photos'] as $i => $photo):?>
															<?php if ($photo['type'] == '0') :?>
																<div class="item<?=$i==0?' active':''?>" data-slide-number="<?php echo $i;?>" style="height:260px;line-height:260px;vertical-align: middle;" >
																	<img class="carousel-center" src="<?php echo large_url($photo['attachment_title'], 'suksesi'); ?>" style="max-height:260px; margin: 0 auto;">
																</div>
															<?php endif;?>
														<?php endforeach;?>
													</div>
													<!--
													<?php if (count($row['photos']) > 1) :?>
														<a class='carousel-control left' href='#carousel-scandal-<?=$rid?>' data-slide='prev'>&lsaquo;</a>
														<a class='carousel-control right' href='#carousel-scandal-<?=$rid?>' data-slide='next'>&rsaquo;</a>
													<?php endif;?>
													-->
												</div>
											</div>

											<div class="row-fluid slider-thumbs">
												<div class="span12" style="margin-top:5px;">
													<ul class="thumbnails">
														<?php foreach ($row['photos'] as $i => $photo):?>
															<?php if ($photo['type'] == '0') :?>
																<li style="margin-bottom:3px;width:10%;">
																	<a class="thumbnail" id="carousel-thumb-<?php echo $i;?>" data-carousel="carousel-scandal-<?=$rid?>">
																		<img src="<?php echo thumb_url($photo['attachment_title'], 'suksesi'); ?>" />
																	</a>
																</li>
														<?php endif;?>
														<?php endforeach;?>
													</ul>
												</div>
											</div>
										<?php endif;?>
									</div>

									<div class="span5 pull-right" style="margin-top:-10px;">
										<table class='table-h table-skandal' style="margin:0;">
											<tr><th>Tanggal Pelaksanaan</th><td>:<?php echo $row['tgl_pelaksanaan']; ?></td></tr>
											<tr><th>Suksesi</th><td>:<?php echo (!empty($row['kdname'])) ? $row['kdname']: '-' ; ?></td></tr>
											<tr><th>Daerah Pemilihan</th><td>:<?php echo (!empty($row['dname'])) ? $row['dname'] : '-'; ?></td></tr>
											<tr><th>Daftar Pemilih Tetap</th><td>:<?php echo (!empty($row['daftar_pemilih_tetap'])) ? $row['daftar_pemilih_tetap']: '-' ; ?></td></tr>
											<tr><th>Status</th><td>:<?php echo (!empty($row['status'])) ? $row['status']: '-' ; ?></td></tr>
											<tr><th>Jumlah Putaran</th><td>:<?php echo (!empty($row['score_count'])) ? $row['score_count']: '-' ; ?></td></tr>
										</table>
									</div>
								</div>

								<div class="row-fluid">
									<?php echo textWrap(nl2br($row['description'])); ?>
								</div>

								<div class="row-fluid">
								<div class="more-space">Beri komentar:</div>
									<input type="hidden" id="ref2" value="0" />
									<div id="conten-comment-wrapper"></div>
								</div>


								<?php if(!empty($row['score'])) :?>
									<div class="row-fluid">
										<div  class="list-content-suksesi tabbable">
											<ul class="nav nav-tabs">
												<?php
												$t=0;
												foreach($row['score'] as $score):
													$tabSection = "section-tab-".$score['id_trace_status'] ;
													if(!empty($score['lembaga'])) : ?>
														<li id="list-<?php echo $tabSection;?>" <?php if ($t==0) echo ' class="active"' ;?>>
															<a id="<?php echo $tabSection; ?>" href="#"><?php echo $score['score_name'];?></a>
														</li>
													<?php
													$t++;
													endif;
												endforeach;?>
											</ul>
											<div class="tab-content">
												<?php
												$t=0;
												foreach ($row['score'] as $score) :
													$tabSection = "section-tab-".$score['id_trace_status'] ;
													if (!empty($score['lembaga'])) :
												?>
														<div class="tab-pane" id="conten-<?php echo $tabSection;?>" <?php echo ($t==0) ? ' style="display: block;"' : ' style="display: none;"';?> >
															<?php if (!empty($score['lembaga'])) :?>
																<?php if (!empty($score['kandidat'])) :?>
																	<div class="row-fluid">
																		<h4>Calon dan Pasangan Calon</h5>
																		<?php
																		foreach ($score['kandidat'] as $k => $kandidat) :
																			$no_urut				= $kandidat['nomor_urut'];
																			$alias				= $kandidat['alias'];
																			$candidate			= $kandidat['kandidat_name'];
																			$candidate_link	= base_url() . 'politisi/index/' . $kandidat['page_id'];
																			$candidate_pic		= badge_url($kandidat['kandidat_attachment'], 'politisi/'.$kandidat['page_id']);
																			$pasangan			= $kandidat['pasangan_name'];
																			$pasangan_link		= base_url() . 'politisi/index/' . $kandidat['page_id_pasangan'];
																			$pasangan_pic		= badge_url($kandidat['pasangan_attachment'], 'politisi/'.$kandidat['page_id_pasangan']);
																			$pull 				= " pull-right";
																			if($k % 2 == 0) {
																				$pull = " pull-left";
																				echo '<div class="row-fluid">';
																			}
																		?>
																			<div class="race-detail-candidate span6 <?=$pull;?>">
																				<div class="row-fluid">
																					<h5><span class='race-detail-candidate-num' title='<?=$alias?>'><?=$no_urut?></span>. <?=$alias?></h5>
																					<span class="span5 pull-left race-detail-candidate">
																						<a href="<?=$candidate_link?>"><img src="<?=$candidate_pic?>" alt="<?=$candidate?>"/></a>
																					</span>
																					<span class="span5 pull-left race-detail-candidate">
																						<a  href="<?=$pasangan_link?>"><img src="<?=$pasangan_pic?>" alt="<?=$pasangan?>"/></a>
																					</span>
																				</div>
																			</div>
																		<?php
																			if($k % 2 !=0) echo '</div><!-- /row-fluid -->';
																		endforeach;
																		if($k % 2 ==0) echo '</div><!-- /row-fluid -->';
																		?>
																	</div>
																<?php endif;?>
																<hr/>
																<h4>Rekapitulasi</h4>
																<table id='tabel-2' class='suksesi-tabel-rekapitulasi'>
																	<tr class='suksesi-tabel-rekap-head'>
																		<th>Sumber</th>
																		<?php if(!empty($score['kandidat'])):?>
																			<?php foreach($score['kandidat'] as $kandidat):?>
																				<td><span data-toggle='tooltip' class='tooltip-top race-detail-candidates-num' title="<?=$kandidat['alias']?>"><?=$kandidat['nomor_urut']?></span></td>
																			<?php endforeach;?>
																		<?php endif;?>
																	</tr>
																	<?php foreach($score['lembaga'] as $lembaga):?>
																		<tr class='suksesi-tabel-rekap-row'>
																			<?php $lembaga_name = (strpos($lembaga['page_name'], ' ') === FALSE) ? $lembaga['page_id'] : $lembaga['page_name'];?>
																			<th><?=$lembaga_name;?></th>

																			<?php
																			foreach ($score['kandidat'] as $kandidat):
																				$total_score = 0;
																				foreach($lembaga['kandidat'] as $lg) {
																					if ($lg['score_type'] == '1') $total_score+=$lg['score'];
																				}

																				$kandidat_score = '-';
																				foreach($lembaga['kandidat'] as $lg) {
																					if($lg['page_id'] == $kandidat['page_id']) {
																						if ($lg['score_type'] == '0') {
																							$kandidat_score = number_format($lg['score'], 2);
																						} else {
																							$kandidat_score = number_format(($lg['score']/$total_score)*100, 2);
																						}
																					}
																				}
																				?>
																				<td><?=$kandidat_score?> %</td>
																			<?php endforeach;?>
																		</tr>
																	<?php endforeach;?>
																</table>
																<ul class='suksesi-tabel-rekapitulasi-legenda'>
																<?php
																foreach($score['kandidat'] as $kandidat) {
																	$num = $kandidat['nomor_urut'];
																	$alias = $kandidat['alias'];
																	echo "<li><span class='race-detail-candidates-num'>$num</span> $alias</li>";
																	}
																?>
																</ul>
															<?php endif;?>
														</div>
												 <?php
												 	$t++;
												 	endif;
												 endforeach;
												 ?>
											</div>
										</div>
									</div>
								<?php endif;?>

							</div> <!-- .content-place -->

							<div style="clear:both"></div><hr/>
						<?php endforeach;?>
						</div> <!-- #scandal-content -->
					<?php endif;?>
				</div>
			</div>

			<div class='col col-1-4 col-side' id='col-rite' style="width:235px;">
				<?php $this->load->view('core/sidebar_sentimen'); ?>
			</div>

		</div></div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#conten-comment-wrapper').load('<?php echo base_url(); ?>ajax/comment_load/', { 'id':'<?php echo $row['content_id']; ?>', 'content_date':'<?php echo $row['content_date']; ?>' });

	 //Suksesi Tabs When page loads...
	 $('.list-content-suksesi').each(function() {
		$(this).find(".nav-tabs > li:first").addClass("active"); //Activate first tab
		$(this).find(".tab-pane:first").addClass("active"); //Show first tab content
		$("#content-"+this.id).slimScroll({
			height: '300px',
			railDraggable : true,
			railVisible: true,
			alwaysVisible: true
			});
	});

	 //Suksesi Tabs On Click Event
	 $(".list-content-suksesi > ul.nav-tabs > li > a").click(function(e) {
		$(this).parents('.list-content-suksesi').find(".tab-pane").removeClass("active").hide(); //Remove any "active" & "hide" tab content
		$(this).parents('.list-content-suksesi').find(".nav-tabs > li").removeClass("active"); //Remove any "active" tab pange
		var tabID = this.id;
		$(".list-content-suksesi #conten-"+tabID).addClass("active").show();
		$(".list-content-suksesi #list-"+tabID).addClass("active");
		e.preventDefault();
		});
	});

</script>
<?php $this->load->view($template_path .'tpl_footer');?>