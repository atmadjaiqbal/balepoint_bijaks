<?php $this->load->view($template_path .'tpl_header');?>
<div id='body' class='page page-news page-suksesi'>
	<div class='container'>
		<div class='row row-col'><div class='col-border clearfix' style='border-left: none'>
			<div class='col col-3-4' id='page-news-list' style="width:700px;">
				<div class='block'>
					<h2>Suksesi</h2><hr />
					<div class="scandal-content">
					<?php $this->load->view('suksesi/suksesi_list'); ?>
					<?php if ($suksesi['total_rows'] > 10 && $load_more): ?>
						<div id="loadmore" class=""></div>
						<div class="row-fluid load-more-loader"></div>
						<div class="row-fluid load-more-wrapper">
							<a id="read-loadmore" data-url="<?php echo base_url() .'suksesi/load_more/';?>" data-page="<?php echo $page;?>">Load More</a>
						</div>
					<?php endif; ?>
					</div> <!-- #scandal-content -->
				</div>
			</div>
			<div class='col col-1-4 col-side' id='col-rite' style="width:239px;">
				  <?php $this->load->view('core/sidebar_sentimen'); ?>
			</div>
		</div></div>
	</div>
</div>

<script>
$(document).ready(function() {
	//Suksesi Tabs When page loads...
	$('.list-content-suksesi').each(function() {
		$(this).find(".nav-tabs > li:first").addClass("active"); //Activate first tab
		$(this).find(".tab-pane:first").addClass("active"); //Show first tab content

		$("#content-"+this.id).slimScroll({
			//height: '300px',
			railDraggable : true,
			railVisible: true,
			alwaysVisible: true
		});
	});
	//Suksesi Tabs On Click Event
	$(".list-content-suksesi > ul.nav-tabs > li > a").click(function(e) {
		$(this).parents('.list-content-suksesi').find(".tab-pane").removeClass("active").hide(); //Remove any "active" & "hide" tab content
		$(this).parents('.list-content-suksesi').find(".nav-tabs > li").removeClass("active"); //Remove any "active" tab pange
		var tabID = this.id;
		$(".list-content-suksesi #conten-"+tabID).addClass("active").show();
		$(".list-content-suksesi #list-"+tabID).addClass("active");
		e.preventDefault();
	});
});
</script>
<?php $this->load->view($template_path .'tpl_footer');?>