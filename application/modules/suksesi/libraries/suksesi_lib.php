<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suksesi_Lib {

  protected $CI;

  public function __construct()
  {
    $this->CI =& get_instance();
    $this->CI->load->database('slave');
  }

    public function getSuksesiCount()
    {
        $where = array('trace.publish' => '1');
        $this->CI->db->where($where);
        return $this->CI->db->count_all_results('trace');
    }

   public function getSuksesiList($offset=0, $limit=10, $var_where=array())
    {
    $member           = $this->CI->session->userdata('member');
    $member_account_id  = (isset($member['account_id'])) ? $member['account_id'] : NULL;


      $where      = array('trace.publish' => '1');
      if (!empty($var_where)) $where = array_merge($where, $var_where);

      $this->CI->db->select("tc.content_id, tc.last_update_date, trace.*, trace_kepala_daerah.kdname, trace_daerah.dname,
                             tc.count_comment, tc.count_like, tc.count_dislike,
                             get_count_like_content_by_account('".$member_account_id."', tc.content_id) as is_like,
                             get_count_dislike_content_by_account('".$member_account_id."', tc.content_id) as is_dislike ", FALSE);
    $this->CI->db->from('trace');
    $this->CI->db->join("tcontent tc", "trace.id_race = tc.location AND tc.content_group_type = '56'", "left" );
    $this->CI->db->join('trace_kepala_daerah', 'trace.id_kepala_daerah = trace_kepala_daerah.id_kepala_daerah', "left");
    $this->CI->db->join('trace_daerah', 'trace.id_daerah = trace_daerah.id_daerah', "left");

    $this->CI->db->where($where);
      $this->CI->db->limit($limit, $offset);
      $this->CI->db->order_by('trace.update_date', 'DESC');

    $query = $this->CI->db->get()->result_array();
    return $query;
    }

   public function getSuksesiPhoto($raceID, $limit='0')
    {
      $limit   = ($limit == "0") ? "" : " LIMIT 0, $limit ";
      $sql     = "SELECT ta.attachment_title, tp.type FROM trace_photo tp
                  LEFT JOIN tcontent_attachment ta ON ta.content_id = tp.content_id
                  WHERE tp.id_race='".$raceID."'
                  ORDER BY insert_date DESC $limit";
    $result = $this->CI->db->query($sql)->result_array();
    return $result;
    }

   public function getSuksesiStatus($where, $order_by='status asc')
    {
      $this->CI->db->where($where);
      $this->CI->db->order_by($order_by);
      $data = $this->CI->db->get('trace_status');
      return $data;
    }

    public function getLembaga($id)
    {
      $sql = 'select tl.*, tp.page_name from trace_lembaga tl left join tobject_page tp on tp.page_id = tl.page_id where id_trace_status = ? order by created_date desc ';
      $data = $this->CI->db->query($sql, $id);

      return $data;
    }

   public function getLembagaScoreList($id)
    {

      $sql = "select tl.*, tp.page_name
               from trace_lembaga tl
               left join tobject_page tp on tp.page_id = tl.page_id
               where tl.id_trace_status = '".$id."' order by created_date desc ";
      $data = $this->CI->db->query($sql)->result_array();

      if (!empty($data))
      {
         foreach($data as $key => $row ){
                  $sql = "select ts.*, tk.nomor_urut, tk.page_id, tp.page_name , tc1.attachment_title  kandidat_attachment,
                               tpp.page_id page_id_pasangan, tpp.page_name page_name_pasangan , tc2.attachment_title pasangan_attachment
                          from trace_kandidat tk
                          left join   trace_score ts on ts.idtrace_kandidat = tk.idtrace_kandidat
                          left join tobject_page  tp on tp.page_id = tk.page_id
                          left join tcontent_attachment tc1 on tp.profile_content_id = tc1.content_id
                          left join tobject_page  tpp on tpp.page_id = tk.page_id_pasangan
                          left join tcontent_attachment tc2 on tpp.profile_content_id = tc2.content_id
                          where  ts.id_trace_lembaga = '".$row['id_trace_lembaga']."'
                          order by tk.nomor_urut, tk.idtrace_kandidat";

                  $kandidat = $this->CI->db->query($sql)->result_array();
                  /*
                  foreach($kandidat as $keyk => $rowk ){
                    $sql = "select tkp.*,tp.page_id, tp.page_name
                    from trace_kandidat_pendukung  tkp
                    left join tobject_page  tp on tkp.page_id = tp.page_id
                    where idtrace_kandidat = '".$rowk['idtrace_kandidat']."' order by created_date";
                    $pendukung = $this->CI->db->query($sql)->result_array();

                     $kandidat[$keyk]['pendukung'] = $pendukung;
                  }
                  */

            $data[$key]['kandidat']       =  $kandidat;
         }
      }

      return $data;
    }


    public function getKandidatList($suksesiID)
    {
        $sql = "select distinct tk.nomor_urut, tk.alias, tk.page_id,
                    tp1.page_name AS kandidat_name,
                    tc1.attachment_title kandidat_attachment,
                    tk.page_id_pasangan,
                    tp2.page_name AS pasangan_name,
                    tc2.attachment_title pasangan_attachment
                from trace_kandidat tk
                left join tobject_page tp1 on tp1.page_id = tk.page_id
                left join tcontent_attachment tc1 on tp1.profile_content_id = tc1.content_id

                left join tobject_page tp2 on tp2.page_id = tk.page_id_pasangan
                left join tcontent_attachment tc2 on tp2.profile_content_id = tc2.content_id
                where id_trace_status in (select id_trace_status from trace_status where  id_race = '".$suksesiID."' and draft='1')
                order by tk.nomor_urut ";
        $data = $this->CI->db->query($sql)->result_array();
        return $data;
    }

    public function getKandidatScoreList($statusID)
    {
        $sql = "select distinct tk.nomor_urut, tk.alias, tk.page_id,
                    tp1.page_name AS kandidat_name,
                    tc1.attachment_title kandidat_attachment,
                    tk.page_id_pasangan,
                    tp2.page_name AS pasangan_name,
                    tc2.attachment_title pasangan_attachment
                from trace_kandidat tk
                left join tobject_page tp1 on tp1.page_id = tk.page_id
                left join tcontent_attachment tc1 on tp1.profile_content_id = tc1.content_id

                left join tobject_page tp2 on tp2.page_id = tk.page_id_pasangan
                left join tcontent_attachment tc2 on tp2.profile_content_id = tc2.content_id
                where id_trace_status = '".$statusID."'
                order by tk.nomor_urut ";
        $data = $this->CI->db->query($sql)->result_array();
        return $data;
    }


    public function getKandidatListCount($suksesiID)
    {
        $return = 0;
        $sql = "select count(distinct tk.page_id) count
                from trace_kandidat tk
                where id_trace_status in (select id_trace_status from trace_status where  id_race = '".$suksesiID."' and draft='1')" ;
        $data = $this->CI->db->query($sql)->row_array();
        if (!empty($data)) $return = $data['count'];
        return $return;
    }


    public function getKandidat($where)
    {
        $sql = 'select tk.*, tp1.page_name as kandidat_name, tc1.attachment_title  kandidat_attachment,
                  tp2.page_name as pasangan_name, tc2.attachment_title pasangan_attachment
                from trace_kandidat tk
                left join tobject_page tp1 on tp1.page_id = tk.page_id
                left join tcontent_attachment tc1 on tp1.profile_content_id = tc1.content_id

                left join tobject_page tp2 on tp2.page_id = tk.page_id_pasangan
                left join tcontent_attachment tc2 on tp2.profile_content_id = tc2.content_id
                where id_trace_status = ?
                order by tk.nomor_urut';
        $data = $this->CI->db->query($sql, $where);
        return $data;
    }



   public function getSuksesiDetail($raceID)
    {
      $sql = 'SELECT trace.*, trace_kepala_daerah.kdname, trace_daerah.dname  FROM trace
               LEFT JOIN trace_kepala_daerah ON trace.id_kepala_daerah = trace_kepala_daerah.id_kepala_daerah
               LEFT JOIN trace_daerah ON trace.id_daerah = trace_daerah.id_daerah
               WHERE id_race="'.$raceID.'"';
    $result = $this->CI->db->query($sql)->row_array();
    return $result;
    }


   function getSuksesiData($page='0', $limit='10', $where=array())
   {
      $arr        = array(intval($limit), $page);
      $count      = $this->getSuksesiCount();
      $race       = $this->getSuksesiList($page, $limit,$where);
      $race_arr   = array();
      $i = 0;

      foreach ($race as $row) {
        $contentDate = mysql_date("%d %M %Y - %h:%i", strtotime($row['update_date']));
        $row['content_date']    = $contentDate;
        //$row['content_count'] = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate);
        $row['content_count']   = '';

        $where = array('id_race' => $row['id_race']);
        $row['photos']    = $this->getSuksesiPhoto($row['id_race']);
        if(!empty($row['photos'])){
          $row['thumb'] = thumb_url($row['photos'][0]['attachment_title']);
        }

        $order_by = 'status asc';
        $status = $this->getSuksesiStatus($where, $order_by);

        $status_arr = array();
            $score      = array();
            $score_count= 0;

        $status_aktif = '';
        $j = 0;
        foreach($status->result_array() as $rr){
          $con_status = '';
          $st = $rr['status'];

          switch ($st) {
            case 1:
              $con_status = 'Survey / Prediksi';
              break;
            case 2:
              $con_status = 'Putaran Pertama';
              break;
            case 3:
              $con_status = 'Putaran Kedua';
              break;

          }

          if($rr['draft'] == '1'){
            $status_aktif = $con_status;
          }

          $rr['draft'] = (intval($rr['draft']) == 1) ? 'Publish' : 'Draft';
          $rr['status'] = $con_status;

          $rr['lembaga'] = $this->getLembagaScoreList($rr['id_trace_status']);

          $rr['kandidat']   = $this->getKandidatScoreList($rr['id_trace_status']);
          $status_arr[$j] = $rr;

               // get latest publis scrore
          if($rr['draft'] == 'Publish') $score = $rr;

          if($rr['draft'] == 'Publish' && $st <> '1') $score_count++;


          $j++;
        }
        $row['status'] = $status_aktif;
         // no need to add detail data tuk list, just to get status
        $row['kandidat_count'] = $this->getKandidatListCount($row['id_race']);

        $row['score'] = $score;
            $row['score_count'] = $score_count;

        $race_arr[$i] = $row;
        $i++;
      }

      $data['total_rows']  = $count;
      $data['list']     = $race_arr;
      return $data;
   }

}

/* End of file file.php */