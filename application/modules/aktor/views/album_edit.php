<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span12"><h3>Buat Album</h3></div>
                </div>

                <div class="row-fluid">
                    <div class="span12 komu-blog-post">
                        <form id="add_album" name="add_album_form" method="post" action="<?php echo base_url().'timeline/post_album';?>">
                            <input name="cid" type="hidden" value ="<?=(!empty($val['content_id'])) ? $val['content_id'] : '0';?>" />
                            <input class="input-flat input-flat-large" type="text" placeholder="Album title" name="title" value="<?=(!empty($val['title'])) ? $val['title'] : '';?>"><br>
                            <textarea class="span12 input-flat input-flat-large" id="" rows="4" cols="4" name="desc" ><?=(!empty($val['description'])) ? $val['description'] : '';?></textarea>
                            <div class="pull-right">
                                <input type="submit" class="btn-flat btn-flat-dark" id="album_post" value="Kirim">
                            </div>
                        </form>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

