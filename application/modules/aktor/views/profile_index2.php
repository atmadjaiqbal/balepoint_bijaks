<style>
#main-profile-box{
    margin-top: -20px;
    background: url("<?php echo base_url('assets/images/background-tag.png');?>") no-repeat;
}

#profile-content{
    margin-top: 234px;;
}

p{
    font-size: 14px !important;
}

#profile-name{
    text-shadow: 2px 2px 3px rgba(0, 0, 0, 1);
    font-size: 30px;
    color: #ffffff;
    word-wrap: break-word;
    width: 350px;
    font-weight: bold;
    line-height: 48px;
    margin-top: -160px;
    margin-left: 55px;
    margin-bottom: 200px;

    -webkit-transform: rotate(-8deg);
    -moz-transform: rotate(-8deg);
    -ms-transform: rotate(-8deg);
    -o-transform: rotate(-8deg);
    transform: rotate(-8deg);
}

.description_box{
    margin-top: -62px;
}

#follow_button{
    background-color: #c4c4c4;
    left: 675px;
    padding-left: 10px;
    position: absolute;
    font-weight: normal;
    width: 10em;

}
</style>

<div class="container">
    <?php $this->load->view('template/aktor/tpl_sub_header2',$hot_profile); ?>
</div>
<br>
<div class="container" id="profile-detail">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- KOMUNITAS KONTENT -->
            <div class="span12">
                <div class="row-fluid" id="main-profile-box">
                    <div class="span8" id="profile-content">
                        <!-- addition buttons -->

                        <?php if($this->member['user_id'] && $user['page_id'] != $this->member['user_id']){?>
                            <div id="follow_button">
                                <a data-id="<?=$user['page_id'];?>" data-follow="<?php echo $is_follow; ?>" id="follow" href="#" style="color: #ffffff;" ><?=($is_follow == 0) ? 'FOLLOW <i class="icon-ok icon-white"></i>' : 'UNFOLLOW <i class="icon-remove icon-white"></i>';?></a>
                            </div>
                            <div style="clear: both;"></div>
                        <?php };?>

                        <div id="profile-name">
                            <?php echo strtoupper($user['page_name']);?>
                        </div>

                        <div class="description_box">
                            <?php $abot = trim($user['about']); if(!empty($abot)){?>
                                <p class=""><?php echo nl2br($abot);?></p>
                            <?php } ?>
                            <?php $pm = trim($user['personal_message']); if(!empty($user['personal_message'])){?>
                                <p class=""><?php echo nl2br($pm);?></p>
                            <?php } ?>
                        </div>

                        <div class="div-line"></div>
                        <div class="row-fluid">

                            <h5>BERI KOMENTAR</h5>
                            <div class="media media-comment">
                                <div class="pull-left media-side-left">
                                    <div style="background:
                                        url('<?php if ($this->member['account_type'] == 0 ||$this->member['account_type'] == 2) { echo icon_url($this->member['xname'],'user/'.$this->member['user_id']) ; } else {  echo icon_url($this->member['xname'],'politisi/'.$this->member['user_id']) ; } ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                                    </div>
                                </div>

                                <div class="media-body">
                                    <div class="row-fluid">
                                        <input id='comment_type' data-id="<?=$user['page_id'];?>" type="text" name="comment" class="media-input" <?php echo ($this->member ? '' : 'disabled="disabled"'); ?>>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12 text-right">
                                            <?php
                                            if(!$this->member)
                                            {
                                                ?>
                                                <span>Login untuk komentar</span>&nbsp;
                                                <a class="btn-flat btn-flat-dark" href="/home/login">Register</a>
                                                <a class="btn-flat btn-flat-dark" href="/home/login">Login</a>
                                            <?php
                                            } else {
                                                ?>
                                                <a id="send_comment" class="btn-flat btn-flat-gray">Send</a>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="politisi_comment" data-page="1" data-limit="10" data-offset="0" data-id="<?=$user['page_id'];?>" class="row-fluid comment-container">

                        </div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a data-page="1" data-id="<?=$user['page_id'];?>" id="politisi_comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                        <div class="div-line"></div>



                    </div>

                    <!-- SMALL SIDE BAR -->
                    <div class="span4" style="margin-top: 400px;">
                        <?php $this->load->view('template/aktor/tpl_aktor_side2'); ?>
                    </div>





                </div>
            </div>




        </div>
    </div>
</div>
<br>
