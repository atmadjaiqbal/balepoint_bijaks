<?php foreach($result as $row=>$val){ ?>

<?php
    $cat_id = (count($val['categories']) > 0) ? $val['categories'][0]['id'] : '0';
    $news_url = base_url() . 'news/article/'.$cat_id.'-'.$val['id'].'/'.urltitle($val['title']);
?>

<div class="row-fluid aktor-skandal-list">
    <div class="span4 aktor-skandal-left">
        <img class="aktor-skandal-photo" src="<?=$val['image_thumbnail'];?>">
        <div class="score-place score-place-overlay score" data-tipe="0" data-id="<?=$val['content_id'];?>"></div>
    </div>
    <div class="span8 aktor-skandal-right">
        <a title="<?=$val['title'];?>" href="<?php echo $news_url; ?>"><h4><?=( strlen($val['title']) > 40) ? substr($val['title'], 0, 40).'...' : $val['title'];?></h4></a>
        <em><small><?=mdate('%d %M %Y', strtotime($val['date']));?></small></em>

        <div class="row-fluid ">
            <div class="span2"><span>Politisi Terkait</span></div>
            <?php if (count($val['news_profile']) > 0){ ?>
            <?php foreach ($val['news_profile'] as $key => $pro) { ?>
                <?php if($key === 7) break; ?>
                <div class="content-politisi-terkait">
                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                        <img title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                    </a>
                </div>
                <?php } ?>

            <?php }else{ ?>
            <div class="span10"><span>Tidak ada politisi terkait</span></div>
            <?php } ?>
        </div>

    </div>
</div>
<div class="div-line-small"></div>
<?php } ?>

<script>
    if(<?=$count_result;?> < 10 && <?=$count_result;?> != -1){
        $('#load_more_place').remove();
        $('#div_line_bottom').remove();
    }
</script>