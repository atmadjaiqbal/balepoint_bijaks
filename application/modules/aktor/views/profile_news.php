<div class="container">
    <!--?php $this->load->view('template/tpl_header_profile'); ?-->
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<div class="container" id="profile-detail">
    <div class="sub-header-container">
        <div class="badge-info">    
         <?php echo $this->load->view('template/aktor/tpl_aktor_side'); ?>
        </div>   
        <div class="profile-info">
            <div class="profile-name">
                <p>Berita <?php echo $user['page_name']; ?></p>
            </div>
            <div id="profile_news_container" data-user="<?=$user['page_id'];?>" data-tipe="1" data-page='1'>

            </div>
            <br>
            <div id="load_more_place" class="row-fluid komu-wall-list komu-wall-list-bg">
                <div class="span12 text-center">
                    <a id="profile_news_load_more" class="komu-wall-list-more" href="#">LOAD MORE</a>
                </div>
            </div>

        </div>
    </div>
</div>
<br/>

