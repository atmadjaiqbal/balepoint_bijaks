<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aktor extends Application
{
    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->helper('m_date');

        $this->load->helper('text');
        $this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');        
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');

        $this->load->module('survey');
        $this->load->module('headline');

        $this->load->library('aktor/aktor_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->library('suksesi/suksesi_lib');
    }

    public function _remap($method, $params=array())
    {
    	
        if($method == 'index'){
//            show_404();
            redirect(base_url());
        }
        if(method_exists($this, $method))
        {
            $this->isBijaksProfile($params[0]);
            $this->$method($params);
        }else{

            $this->isBijaksProfile($method);
            $this->index($method);
        }

    }

    private function isBijaksProfile($user_id)
    {

        $users = $this->redis_slave->get('profile:detail:'.$user_id);
        $user_array = json_decode($users, true);
//        echo '<pre>';
//        var_dump($user_array);
//        echo '</pre>';
//        exit;
        if(count($user_array) > 0)
        {
            $this->user = $user_array;
        }else{
            show_404();
        }
    }

    function index($id='')
    {
        if(empty($id))
            show_404();

        $this->isLogin();
//        if($this->isLogin(true) == false){
////            $data['rcode'] = 'bad';
////            $data['msg'] = 'Anda harus login !';
////            $this->message($data);
//            show_404();
//        }
        $user = $this->user;
        $user_id = $id;

        $data['title'] = "Bijaks | Total Politik";
        $data['scripts'] = array('bijaks.js', 'bijaks.aktor.js');
        $data['category'] = 'profile';
        $data['topic_last_activity'] = 'bijak';

//        $detailProfile = $profile_detail = $this->redis_slave->get('profile:detail:'.$id);
//        $data['detailProf'] = @json_decode($detailProfile, true);
//        $porfile_news = $this->politik_lib->GetProfileBerita($id);
//        $data['count_news'] = count($porfile_news);
//        $data['count_scandal'] = count($data['detailProf']['scandal']);


        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }

        $data['user'] = $user;

        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list = array();
        if($arr_profile['album_photo'])
        {
            foreach($arr_profile['album_photo'] as $rwPhoto)
            {
                $_photo_list = $rwPhoto['photo'];
            }
        }

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;
        $data['follow'] = $this->aktor_lib->get_follower($user['page_id'])->result_array();

        $count_post = $this->aktor_lib->get_count_post(null, $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }

        $data['user'] = $user;
        $limit = 9; $page = 6;
        $offset = ($limit*$page) - $limit;
        if($offset != 0) $offset += ($page - 1);
//        $data['wall_list'] = $this->get_wall_list($user['account_id'], $user['user_id'], $limit, $offset);

        $html['html']['content']  = $this->load->view('aktor_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);
    }

    public function wall($page_id='')
    {
        if($this->isLogin(true) == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $limit = 9;
        $account_id = $this->input->post('id');
        $page = $this->input->post('page');
        if(empty($account_id) || is_bool($account_id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){
            $offset += (intval($page) - 1);
        }
        $this->isBijaksProfile($page_id[0]);
        $data['user'] = $this->user;
        $data['wall_list'] = $this->get_wall_list($account_id, $page_id[0], $limit, $offset);
        $data['list_photo'] = $this->aktor_lib->get_foto_profile($page_id[0], 10, 0);
        $data['follow'] = $this->aktor_lib->get_follower($page_id[0])->result_array();

        $this->load->view('template/aktor/tpl_aktor_wall', $data);

    }

    private function get_wall_list($account_id, $page_id, $limit, $offset)
    {
//        if($offset < 1000){
            $limit += 1;
            $data = $this->aktor_lib->get_wall_list($account_id, $page_id, $limit, $offset);
            $result = array();
            foreach($data->result_array() as $row=>$val){
                if(intval($val['content_group_type']) == 40){
                    $foto_list = $this->aktor_lib->get_foto_album($val['content_id']);
                    $val['photo'] = $foto_list->result_array();
                }
                $result[$row] = $val;
            }
            return $result;
//        }else{
//            $result = array();
//            $last_key = $this->redis_slave->get('wall:user:key:'.$account_id);
//
//            $zrange = $limit + $offset;
//            $list_key = 'wall:user:list:'.$last_key.':'. $account_id;
//            $list_len = $this->redis_slave->lLen($list_key);
//            if ($zrange > intval($list_len)){
//                $zrange = $list_len;
//            }
//
//            if($offset > intval($list_len))
//                return $result;
//
//            $list_wall = $this->redis_slave->lRange($list_key, $offset, $zrange);
//
//            $content_key = 'wall:user:detail:';
//            foreach($list_wall as $row=>$val)
//            {
//                $content = $this->redis_slave->get($content_key.$val);
//                $result[$row] = json_decode($content, true);
//            }
//            return $result;
//
//        }
    }

    function profile2($id)
    {
//        $this->isLogin();
        $user_id = $id[0];
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.profile.js');
        $data['category'] = 'profile';

//        $detailProfile = $profile_detail = $this->redis_slave->get('profile:detail:'.$id[0]);
//        $data['detailProf'] = @json_decode($detailProfile, true);
//        $porfile_news = $this->politik_lib->GetProfileBerita($id[0]);
//        $data['count_news'] = count($porfile_news);
//        $data['count_scandal'] = count($data['detailProf']['scandal']);

        $data['topic_last_activity'] = 'bijak';

        $this->isBijaksProfile($user_id);
        $user = $this->user;

        // check if friend
//        $data['is_teman'] = false;
//        if($id != $this->member['user_id']){
//            $select = 'count(*) as total';
//            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
//            $is_friend = $this->aktor_lib->get_friend($select, $where)->row_array();
//            if($is_friend['total'] > 0) $data['is_teman'] = true;
//        }

        //check if follow
        $data['is_follow'] = false;
        if($id != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = array('account_id' => $this->member['account_id'], 'page_id' => $user['page_id']);
            $dt = $this->aktor_lib->is_follow($select, $where)->row_array();
            if($dt['total'] > 0) $data['is_follow'] = true;
        }

        $data['follow'] = $this->aktor_lib->get_follower($user['page_id'])->result_array();
        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }


        $where = array('page_id' => $user['page_id']);
        $info_address = $this->aktor_lib->get_info_address('*', $where);
        $data['address'] = array();
        if($info_address->num_rows() > 0){
            $data['address'] = $info_address->row_array(0);
        }
/*
        $data['hot_profile'] = $this->profile->hot('true');
        $data['skandal'] = $this->scandal->sesi('true');
        $data['survey'] = $this->survey->mini_survey();
        $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');
        $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 9, 'true');
*/
        $data['user'] = $user;
        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list = array();
        if($arr_profile['album_photo'])
        {
            foreach($arr_profile['album_photo'] as $rwPhoto)
            {
                $_photo_list = $rwPhoto['photo'];
            }
        }

        /*---- Metatag ----- */
        $_artdesc = strip_tags($arr_profile['about'], '<p>');
        $_artdesc = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $_artdesc);
        $_artdesc = strip_tags($_artdesc);
        $_artdesc = str_replace('"','',$_artdesc);
        $_artdesc = str_replace("'","",$_artdesc);
        $data['articledesc'] = substr($_artdesc, 0, 160);
        $data['polterkait'] = array($arr_profile['page_name']);
        $data['topnews'] = '';
        /*---- End Metatag --- */

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;

        $html['html']['content']  = $this->load->view('profile_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);
    }

    function follow($id)
    {
        //$this->isLogin();

        $user_id = $id[0];
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts'] = array('bijaks.js',  'bijaks.aktor.follow.js');
        $data['category'] = 'follow';
        $data['topic_last_activity'] = 'bijak';

        $this->isBijaksProfile($user_id);
        $user = $this->user;

        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }

        $data['user'] = $user;

        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list = array();
        if($arr_profile['album_photo'])
        {
          foreach($arr_profile['album_photo'] as $rwPhoto)
          {
             $_photo_list = $rwPhoto['photo'];
          }
        }

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;
        $data['follow'] = $this->aktor_lib->get_follower($user['page_id'])->result_array();

        $html['html']['content']  = $this->load->view('follow_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);

    }

    function photo($dt)
    {
//        $this->isLogin();
//        if(count($dt) == 1){

            $user_id = $dt[0];

            $data['title'] = "Bijaks | Total Politik";
            $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.profile.photo.js');
            $data['category'] = 'album';
            $data['topic_last_activity'] = 'bijak';

            $this->isBijaksProfile($user_id);
            $data['detailProf'] = $this->user;
            $user = $this->user;

            $count_post = $this->aktor_lib->get_count_post(null, $user['page_id']);
            foreach($count_post->result_array() as $val){
                $user['count_'.$val['tipe']] = $val['total'];
            }

            $rowProf = $this->redis_slave->get('profile:detail:'.$dt[0]);
            $arr_profile = @json_decode($rowProf, true);
            $_photo_list = array();
            if($arr_profile['album_photo']) {
               foreach($arr_profile['album_photo'] as $rwPhoto)
               {
                 $_photo_list = $rwPhoto['photo'];
               }
            }

            /*---- Metatag ----- */
            $_artdesc = strip_tags($arr_profile['about'], '<p>');
            $_artdesc = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $_artdesc);
            $_artdesc = strip_tags($_artdesc);
            $_artdesc = str_replace('"','',$_artdesc);
            $_artdesc = str_replace("'","",$_artdesc);
            $data['articledesc'] = substr($_artdesc, 0, 160);
            $data['polterkait'] = array($arr_profile['page_name']);
            $data['topnews'] = '';
            /*---- End Metatag --- */

            $data['user'] = $user;
            $data['list_photo'] = $_photo_list;
            $data['follow'] = $this->aktor_lib->get_follower($dt[0])->result_array();

            $html['html']['content']  = $this->load->view('album_index', $data, true);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
            $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
            $this->load->view('template/tpl_one_column', $html);


    }

    public function photo_album_list()
    {
        $tipe = $this->uri->segment(5);
        $page_id = $this->uri->segment(3);
        $page = $this->uri->segment(4);
        $user = $this->user;
        $limit = 12;
        $offset = ($limit*intval($page)) - $limit;
        $list_album = $this->aktor_lib->get_foto_profile_album($tipe, $page_id, $limit, $offset);
        $data['user'] = $user;
        $data['result'] = $list_album;
        $data['count_result'] = count($list_album);
        $this->load->view('photo_album_list', $data);
    }


    function news($url='')
    {
    	$page_id = $this->uri->segment(3);
    	$page = $this->uri->segment(4);

        $data = $this->data;
        $data['topic_last_activity'] = 'profile';
        $data['scripts']     = array('bijaks.js', 'bijaks.profile.js');

        $user = $this->user;
        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $data['user'] = $user;

        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list = array();
        if($arr_profile['album_photo'])
        {
            foreach($arr_profile['album_photo'] as $rwPhoto)
            {
                $_photo_list = $rwPhoto['photo'];
            }
        }
        /*---- Metatag ----- */
        $_artdesc = strip_tags($arr_profile['about'], '<p>');
        $_artdesc = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $_artdesc);
        $_artdesc = strip_tags($_artdesc);
        $_artdesc = str_replace('"','',$_artdesc);
        $_artdesc = str_replace("'","",$_artdesc);
        $data['articledesc'] = substr($_artdesc, 0, 160);
        $data['polterkait'] = array($arr_profile['page_name']);
        $data['topnews'] = '';
        /*---- End Metatag --- */

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;
        $data['follow'] = $this->aktor_lib->get_follower($page_id)->result_array();
        $data['category'] = 'profile';

        $html['html']['content']  = $this->load->view('profile_news', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);        	
    }

    function news_list()
    {
        $page_id = $this->uri->segment(3);
        $page = $this->uri->segment(4);

        $user = $this->user;

        $limit = 10;

        $offset = ($limit*intval($page)) - $limit;
//        if($offset != 0){
//            $offset += (intval($page) - 1);
//        }

        $news_list = $this->aktor_lib->get_news_by_aktor($page_id, $limit, $offset);
        $rkey_min = 'news:detail:';
        $result = array();

        $have_redis_empty = false;

        foreach($news_list->result_array() as $row=>$val)
        {
            $news = $this->redis_slave->get($rkey_min.$val['news_id']);
            $news_json = json_decode($news, true);
            if(empty($news_json)){
                $have_redis_empty = true;
                continue;
            }
            $news_json['entry_date'] = $val['entry_date'];
            $result[$row] = $news_json;

        }

        $data['user'] = $user;
        $data['result'] = $result;
        $data['count_result'] = count($result);
        if($have_redis_empty) $data['count_result'] = -1;

        $this->load->view('aktor_news_list', $data);
    }


    
    function news_detail($page_id=null, $offset=0, $limit=5)
    {
    	$data['ResultBerita'] = $this->politik_lib->GetProfileBerita($page_id, null, $limit, $offset);
    	$data['total_rows'] = $this->politik_lib->CountProfileBerita($page_id);
      return $data;    	
    }

    public function newsmore($page_id=null, $page=1, $limit=10)
    {
       $data['category'] = 'profile';
    	 $pro_news = array();
       $offset = ($page =="1") ? '11' :  $page*$limit;
       $NewsDetails = $this->news_detail($page_id[0], $offset, $limit);
       $_i = 0;

       foreach($NewsDetails['ResultBerita']  as $row)
       {
            $content_id = $row['content_id'];
            $news_id = $row['news_id'];
            $topic_id = $row['topic_id'];
            $content_url = $row['content_url'];

            $news_detail = $this->redis_slave->get('news:detail:'.$news_id);
            $news_array = @json_decode($news_detail, true);
            
            $pro_news[$_i] = $news_array;
            $pro_news[$_i]['news_id'] = $news_id;

            if(!empty($pro_news))
            {
                $pro_news['topic_id'] = $news_array['categories'][0]['id'];
            }
            $_i++;
       }

       $data['news'] = $pro_news;
              
       $data['offset']   = $offset;
       $totalPage        = ceil($NewsDetails['total_rows']['count_news']/$limit);
 		 $nextPage         = (int)$page+1;
       $load_more        = ($page < $totalPage) ? '1': '0';               
       $html = $this->load->view('template/profile/tpl_profile_news', $data, true);      
    // HTML Response, Flag Load More, Next Page
       echo $html .'||'. $load_more .'||'. $nextPage;          	
    }

    function scandals($url=null)
    {
    	$page_id = $this->uri->segment(3);

        $data = $this->data;
        $data['scripts']     = array('bijaks.js', 'bijaks.profile.js');

        $data['topic_last_activity'] = 'profile';

        $user = $this->user;
        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }

        $data['user'] = $user;

        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list  = array();
        if($arr_profile['album_photo'])
        {
            foreach($arr_profile['album_photo'] as $rwPhoto)
            {
                $_photo_list = $rwPhoto['photo'];
            }
        }

        /*---- Metatag ----- */
        $topnews = array();
        $_artdesc = strip_tags($arr_profile['about'], '<p>');
        $_artdesc = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $_artdesc);
        $_artdesc = strip_tags($_artdesc);
        $_artdesc = str_replace('"','',$_artdesc);
        $_artdesc = str_replace("'","",$_artdesc);
        $data['articledesc'] = substr($_artdesc, 0, 160);
        $data['polterkait'] = array($arr_profile['page_name']);
        $_y = 0; foreach($arr_profile['scandal'] as $rwscan) {if($_y > 5) break; $topnews[$_y] = $rwscan['scandal_title']; $_y++;}
        $data['topnews'] = $topnews;
        /*---- End Metatag --- */

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;
        $data['follow'] = $this->aktor_lib->get_follower($page_id)->result_array();
        $data['category'] = 'profile';
        $html['html']['content']  = $this->load->view('profile_scandals', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);    	    	    	
    	
    }

    function scandal_list($dt=null)
    {
        $page_id = $this->uri->segment(3);
        $page = $this->uri->segment(4);

        $user = $this->user;

        $limit = 10;

        $offset = ($limit*intval($page)) - $limit;
//        if($offset != 0){
//            $offset += (intval($page) - 1);
//        }

        $scandal_list = $this->aktor_lib->get_scandal_by_aktor($page_id, $limit, $offset);
        $rkey_min = 'scandal:min:';
        $rkey_player = 'scandal:players:';
        $rkey_photo = 'scandal:photo:';
        $result = array();

        foreach($scandal_list->result_array() as $row=>$val)
        {
            $skandal_min = $this->redis_slave->get($rkey_min.$val['scandal_id']);
            $skandal_player = $this->redis_slave->get($rkey_player.$val['scandal_id']);
            $skandal_photo = $this->redis_slave->get($rkey_photo.$val['scandal_id']);

            $skandal_json = json_decode($skandal_min, true);
            $skandal_json['player'] = json_decode($skandal_player, true);
            $skandal_json['photo'] = json_decode($skandal_photo, true);

            $result[$row] = $skandal_json;

        }

        $data['user'] = $user;
        $data['result'] = $result;
        $data['count_result'] = count($result);

        $this->load->view('aktor_skandal_list', $data);

    }

    function scandal_detail($page_id=null, $offset=null, $limit=null)
    {
      $detailProfile = $profile_detail = $this->redis_slave->get('profile:detail:'.$page_id);
      $data['detailProf'] = @json_decode($detailProfile, true);
      foreach($data['detailProf']['scandal'] as $key => $value)
      {
        $skandalProfile[$key] = $value;
        $scanPhoto = $this->redis_slave->get('scandal:photo:'.$value['scandal_id']);
        $skandalProfile[$key]['photos'] = @json_decode($scanPhoto, true);
        $skandal_players = $this->redis_slave->get('scandal:players:'.$value['scandal_id']);
        $skandalProfile[$key]['players'] = @json_decode($skandal_players, true);
        $skandal_detail = $this->redis_slave->get('scandal:min:'.$value['scandal_id']);
        $skandalProfile[$key]['details'] = @json_decode($skandal_detail, true);                    
      }
            
    	$data['ResultScandal'] = $skandalProfile;
    	$data['total_rows'] = count($data['detailProf']['scandal']);
      return $data;    	
    }

    function scandalmore()
    {
       $offset = ($page =="1") ? '11' :  $page*$limit;
       $data['NewsScandal'] = $this->scandal_detail($page_id);              
       $data['offset']   = $offset;
       $totalPage        = ceil($data['NewsScandal']['total_rows']/$limit);
 		 $nextPage         = (int)$page+1;
       $load_more        = ($page < $totalPage) ? '1': '0';               
       $html = $this->load->view('template/profile/tpl_profile_scandals', $data, true);      
    // HTML Response, Flag Load More, Next Page
       echo $html .'||'. $load_more .'||'. $nextPage;          	    	
    }

    function album_list($dt)
    {
        if(count($dt) != 3){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = $dt[0];
        $tipe = $dt[1];
        $page = $dt[2];

        $this->isBijaksProfile($id);

        $user = $this->user;
        $data['user'] = $user;
        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        $photo = $this->get_album_list($id, $user['account_id'], $limit, $offset);

        $data['photo'] = $photo;
        $this->load->view('template/aktor/tpl_komunitas_album', $data);
    }

    private function get_album_list($id, $account_id, $limit=1, $offset, $tipe=0)
    {
        $result = array();
        $album = $this->aktor_lib->get_foto_group($account_id, $id, $limit, $offset);
        foreach($album->result_array() as $row=>$val){
            $photo = $this->aktor_lib->get_foto_album($val['content_id']);
            $val['count'] = $photo->num_rows();
            $val['photo'] = $photo->result_array();
            $result[$row] = $val;
        }
        return $result;
    }

    function foto_list($dt)
    {
        if(count($dt) != 2){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = $dt[0];
        $page = $dt[1];

        $this->isBijaksProfile($id);

        $user = $this->user;
        $data['user'] = $user;
        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;

        $photo = $this->aktor_lib->get_foto_profile($id, $limit, $offset);
        $data['photo'] = $photo->result_array();
        $this->load->view('template/aktor/tpl_aktor_foto', $data);

    }

    function foto_modal($dt)
    {
        $id = $dt[0];
        $cid = $dt[1];

        $this->isBijaksProfile($id);
        $user = $this->user;
        $photo = $this->aktor_lib->get_foto_list($cid);
        if($photo->num_rows == 0){
            $dat['rcode'] = 'bad';
            $dat['message'] = 'content not found';
            header('application/json');
            echo json_encode($dat);
            exit;
        }
        $dat['rcode'] = 'ok';
        $dat['message'] = $photo->row_array(0);
        $dat['message']['image_uri'] = large_url($dat['message']['attachment_title']);
        header('application/json');
        echo json_encode($dat);

    }

    public function blog_list($dt)
    {
        if(count($dt) != 3){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = $dt[0];
        $tipe = $dt[1];
        $page = $dt[2];

        $this->isBijaksProfile($id);

        $user = $this->user;
        $data['user'] = $user;
        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        $blog = $this->get_blog_list($id, $user['account_id'], $limit, $offset);

        $data['blogs'] = $blog;
        $this->load->view('template/aktor/tpl_komunitas_blog', $data);

    }


    function follower_list($dt)
    {
        if($this->isLogin(true) == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        if(count($dt) != 4){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = $dt[0];
        $acc_id = $dt[1];
        $tipe = $dt[2];
        $page = $dt[3];

        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $this->isBijaksProfile($id);
        $data['user'] = $this->user;

        $offset = ($limit*intval($page)) - $limit;
        $fl_list = array();
        if(intval($tipe) == 1){
            $follow_lembaga = $this->aktor_lib->get_follow_politisi($acc_id, $limit, $offset);
            $fl_list = $follow_lembaga->result_array();
        }elseif(intval($tipe) == 2){
            $follow_lembaga = $this->aktor_lib->get_follower($id, $limit, $offset);
            $fl_list = $follow_lembaga->result_array();
        }elseif(intval($tipe) == 3){
            $follow_lembaga = $this->aktor_lib->get_follow($acc_id, $limit, $offset);
            $fl_list = $follow_lembaga->result_array();
        }
        $data['tipe'] = $tipe;
        $data['follow'] = $fl_list;
        $this->load->view('template/aktor/tpl_aktor_follow', $data);

    }


    function isLogin($return=false)
    {
        $cr = current_url();
        $this->member = $this->session->userdata('member');
        if(!$this->member['logged_in']){
            if($this->input->is_ajax_request()){
                return false;
            }else{
                if($return){
                    return false;
                }else{
                    redirect(base_url().'home/login?frm='.urlencode($cr));
                }
            }

        }

        return true;
        //exit('please');
    }


    function profile($id)
    {
        $user_id = $id[0];
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.profile.js');
        $data['category'] = 'profile';

//        $detailProfile = $profile_detail = $this->redis_slave->get('profile:detail:'.$id[0]);
//        $data['detailProf'] = @json_decode($detailProfile, true);
//        $porfile_news = $this->politik_lib->GetProfileBerita($id[0]);
//        $data['count_news'] = count($porfile_news);
//        $data['count_scandal'] = count($data['detailProf']['scandal']);

        $data['topic_last_activity'] = 'bijak';

        $this->isBijaksProfile($user_id);
        $user = $this->user;

        // check if friend
//        $data['is_teman'] = false;
//        if($id != $this->member['user_id']){
//            $select = 'count(*) as total';
//            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
//            $is_friend = $this->aktor_lib->get_friend($select, $where)->row_array();
//            if($is_friend['total'] > 0) $data['is_teman'] = true;
//        }

        //check if follow
        $data['is_follow'] = false;
        if($id != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = array('account_id' => $this->member['account_id'], 'page_id' => $user['page_id']);
            $dt = $this->aktor_lib->is_follow($select, $where)->row_array();
            if($dt['total'] > 0) $data['is_follow'] = true;
        }

        $data['follow'] = $this->aktor_lib->get_follower($user['page_id'])->result_array();
        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }


        $where = array('page_id' => $user['page_id']);
        $info_address = $this->aktor_lib->get_info_address('*', $where);
        $data['address'] = array();
        if($info_address->num_rows() > 0){
            $data['address'] = $info_address->row_array(0);
        }
        /*
                $data['hot_profile'] = $this->profile->hot('true');
                $data['skandal'] = $this->scandal->sesi('true');
                $data['survey'] = $this->survey->mini_survey();
                $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
                $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
                $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');
                $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
                $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
                $data['internasional'] = $this->news->sesi('internasional', 9, 'true');
        */
        $data['user'] = $user;
        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list = array();
        if($arr_profile['album_photo'])
        {
            foreach($arr_profile['album_photo'] as $rwPhoto)
            {
                $_photo_list = $rwPhoto['photo'];
            }
        }

        // Hot Profiles
        $i = 1;
        $temp = new Politik_Lib();
        $list_profile = $temp->getHotList(15);

        if(!empty($list_profile))
        {
            foreach ($list_profile as $key => $value)
            {
                $profile_detail = $this->redis_slave->get('profile:detail:'.$value['page_id']);
                $arr_profile = @json_decode($profile_detail, true);
                $result[$i] = $arr_profile;
                $i++;
            }
        }
        $data['hot_profile'] = $result;

        $_artdesc = strip_tags($arr_profile['about'], '<p>');
        $_artdesc = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $_artdesc);
        $_artdesc = strip_tags($_artdesc);
        $_artdesc = str_replace('"','',$_artdesc);
        $_artdesc = str_replace("'","",$_artdesc);
        $data['articledesc'] = substr($_artdesc, 0, 160);
        $data['polterkait'] = array($arr_profile['page_name']);
        $data['topnews'] = '';
        /*---- End Metatag --- */

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;

        $html['html']['content']  = $this->load->view('profile_index2', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);
    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }


}
