<script>
$(function() {
    $('#<?php echo $category;?>_Carousel').carousel({
        interval: false,
        wrap: true
    })

});​
</script>

<?php
if(!empty($row['topic_id']) && !empty($row['id']) && !empty($row['slug']))
{
    $news_url = base_url() . "news/article/" . $row['topic_id'] . "-" . $row['id'] . "/" . trim( str_replace('/', '', $row['slug']));
} else {
    $news_url = '';
}

if(!empty($row['title']))
{
    $short_title = (strlen ($row['title']) > 26) ? substr($row['title'], 0, 26). '...' : $row['title'];
} else {
    $short_title = '';
}

$_title = !empty($row['title']) ? $row['title'] : '';

if($category == 'reses')
{
    $_backstyle = 'style="background-color: #0EEA75;"';
} else {
    $_backstyle = 'style="background-color: #9E5B3E;"';
}
?>

<div class="hp-label hp-label-<?php echo category_color($category); ?>" <?php echo $_backstyle; ?>>
    <a class="pull-right hp-label-index" href="<?php echo base_url().'news/index/'.$category; ?>"><img src="<?php echo base_url(); ?>/assets/images/list_icon.png"></a>
    <h4><?php echo strtoupper($category); ?></h4>
</div>
<?php
  if(!empty($row))
  {
?>
<div class="headline">
    <div class="headline-title clearfix" style="margin-top: -5px;">
        <h4><a href="<?php echo $news_url; ?>" title="<?php echo $_title; ?>"><?php echo $_title; ?></a>
        <span style="float:right;font-size:11px;font-style:italic;"><?php echo (!empty($row['date']) ? mdate('%d %M %Y - %h:%i', strtotime($row['date'])) : ''); ?></span>
        </h4>
    </div>
    <div class="headline-detail clearfix">
        <div class='post-pic' style="margin-top:-5px;">
            <a href="<?php echo $news_url; ?>">
<?php
  if(!empty($row['image_thumbnail']))
  {
?>
     <img class="lazy" data-original="<?php echo $row['image_thumbnail']; ?>" alt="<?php echo $short_title; ?>" style="width: 300px; height: 160px;"/>
<?php
  } else {
?>
     <img src="<?php echo base_url().'assets/images/noimage.jpg' ; ?>" style="width: 300px; height: 160px;"/>
<?php
  }
?>
            </a>
            <div class="score-place score-place-overlay score" data-id="<?php echo !empty($row['content_id']) ? $row['content_id'] : ''; ?>" ></div>
        </div>
        <div class="related-politicians clearfix">
            <span class="pull-left related-politician-label">Politisi Terkait :</span>
            <?php if(!empty($row['news_profile'])) { ?>
            <?php if (count($row['news_profile']) > 0){ ?>
                <?php foreach ($row['news_profile'] as $key => $pro) { ?>
                    <?php if($key === 4) break; ?>
                    <span class="pull-left related-politician-img">
                        <img title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                    </span>
                <?php } ?>
                <?php if (count($row['news_profile']) > 4){ ?>
                    <div class="span2">
                        <span>More</span>
                    </div>
                <?php } ?>
            <?php }else{ ?>
                <span class="pull-left related-politician-img">Tidak ada politisi terkait</span>
            <?php } ?>
            <?php } else { ?>
                <span class="pull-left related-politician-img">Tidak ada politisi terkait</span>
            <?php } ?>
        </div>
        <div class="time-line-content" data-cat="<?php echo $category; ?>" data-uri="<?php echo $row['content_id']; ?>"></div>
    </div>
<?php
  if(!empty($row['content_id']))
  {
?>
<?php
  }
?>
    <?php
    if(isset($nextresult) || !empty($nextresult))
    {
?>
        <a id="<?php echo $category;?>-berikut" class="left carousel-control" href="#<?php echo $category;?>_Carousel" data-slide="next" style="top:450px;margin-left:-15px;">8 berikutnya</a>
<!--        <a class="right carousel-control" href="#<?php echo $category;?>_Carousel" data-slide="next" style="top:450px;margin-left:150px;">Next News</a> -->
        <a class="right carousel-control" href="<?php echo base_url().'news/index/'.$category; ?>" style="top:450px;margin-left:150px;">Selengkapnya</a>

        <div id="<?php echo $category;?>_Carousel" class="slide vertical">
           <div class="carousel-inner">
    <?php

        $_n = 1;
        $newscontent_01 = '';$newscontent_02 = '';$newscontent_03 = '';$newscontent_04 = '';$newscontent_05 = '';
        $newscontent_06 = '';$newscontent_07 = '';$newscontent_08 = '';$newscontent_09 = '';$newscontent_10 = '';
        $newscontent_11 = '';$newscontent_12 = '';$newscontent_13 = '';$newscontent_14 = '';$newscontent_15 = '';
        $newscontent_16 = '';$newscontent_17 = '';$newscontent_18 = '';$newscontent_19 = '';$newscontent_20 = '';

        foreach($nextresult as $key => $value)
        {

            if(!empty($value['topic_id']) && !empty($value['id']) && !empty($value['slug']))
            {
                $news_url = base_url() . "news/article/" . $value['topic_id'] . "-" . $value['id'] . "/" . trim( str_replace('/', '', $value['slug']));
            } else {
                $news_url = '';
            }

            if(!empty($value['title']))
            {
                $short_title = (strlen ($value['title']) > 23) ? substr($value['title'], 0, 23). '...' : $value['title'];
            } else {
                $short_title = '';
            }

            if(!empty($value['image_thumbnail']))
            {
                $_imgNews = $value['image_thumbnail'];
            } else {
                $_imgNews = base_url().'assets/images/noimage.jpg';
            }

            $_title = !empty($value['title']) ? $value['title'] : '';


            if(!empty($row['date']))
            {
                $newsDate = mdate('%d %M %Y - %h:%i', strtotime($row['date']));
            } else {
                $newsDate = '';
            }

            $_content_id = !empty($row['content_id']) ? $row['content_id'] : 0;

            $newsItem = '
               <div class="subheadline ">
                 <div class="img-sec">
                     <img src="'.$_imgNews.'" style="width:100px;height: 60px;">
                 </div>
                 <div class="title-sec">
                    <p><a title="'.$_title.'" href="'.$news_url.'">'.$short_title.'</a></p>
                    <span style="font-style:italic;">'.$newsDate.'</span>
                    <div class="score-place score" data-tipe="1" data-id="'.$_content_id.'" style="margin-top:-10px;"></div>
                 </div>
               </div>';

            if($_n == 1) $newscontent_01 = $newsItem;
            if($_n == 2) $newscontent_02 = $newsItem;
            if($_n == 3) $newscontent_03 = $newsItem;
            if($_n == 4) $newscontent_04 = $newsItem;
            if($_n == 5) $newscontent_05 = $newsItem;
            if($_n == 6) $newscontent_06 = $newsItem;
            if($_n == 7) $newscontent_07 = $newsItem;
            if($_n == 8) $newscontent_08 = $newsItem;
            if($_n == 9) $newscontent_09 = $newsItem;
            if($_n == 10) $newscontent_10 = $newsItem;
            if($_n == 11) $newscontent_11 = $newsItem;
            if($_n == 12) $newscontent_12 = $newsItem;
            if($_n == 13) $newscontent_13 = $newsItem;
            if($_n == 14) $newscontent_14 = $newsItem;
            if($_n == 15) $newscontent_15 = $newsItem;
            if($_n == 16) $newscontent_16 = $newsItem;
            if($_n == 17) $newscontent_17 = $newsItem;
            if($_n == 18) $newscontent_18 = $newsItem;
            if($_n == 19) $newscontent_19 = $newsItem;
            if($_n == 20) $newscontent_20 = $newsItem;

            $_n++;
        }
    ?>
               <div class="item active">
                   <div><?php echo $newscontent_01; ?></div>
                   <div><?php echo $newscontent_02; ?></div>
                   <div><?php echo $newscontent_03; ?></div>
                   <div><?php echo $newscontent_04; ?></div>
                   <div><?php echo $newscontent_05; ?></div>
                   <div><?php echo $newscontent_06; ?></div>
                   <div><?php echo $newscontent_07; ?></div>
               </div>
               <div class="item">
                   <div><?php echo $newscontent_08; ?></div>
                   <div><?php echo $newscontent_09; ?></div>
                   <div><?php echo $newscontent_10; ?></div>
                   <div><?php echo $newscontent_11; ?></div>
                   <div><?php echo $newscontent_12; ?></div>
                   <div><?php echo $newscontent_13; ?></div>
                   <div><?php echo $newscontent_14; ?></div>
               </div>
           </div>
        </div>
    <?php
    }
    ?>

</div>
<?php
  }
?>
