<style type="text/css">
	#container_block{
		margin-top: 3em;
		margin-left: 5em;
	};

	.titlead{
		font-weight: bold;
	}
</style>
<div id="container_block">
	<h1>Banner Management</h1>
	<div>
		<table style="background-color: #ffffff;">
			<?php 
			if(!empty($banners)){
				foreach($banners as $v){
			?>
			<tr>	
				<td><b>Image Url</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['image_url'];?></td>
			</tr>
			<tr>	
				<td><b>Image Link</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['image_link'];?></td>
			</tr>
			<tr>	
				<td><b>Text Shown</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['text_shown'];?></td>
			</tr>
			<tr>	
				<td><b>Text Link</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['text_link'];?></td>
			</tr>
			<tr>	
				<td><b>Ordering</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['ordering'];?></td>
			</tr>
			<tr>	
				<td><b>NB Clicks</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['nb_clicks'];?></td>
			</tr>
			<tr>	
				<td><b>Active</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['is_active'] == 1 ? 'Yes' : 'No';?></td>
			</tr>
			<tr>	
				<td><b>Active</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['is_active'] == 1 ? 'Yes' : 'No';?></td>
			</tr>
			<tr>	
				<td><b>Show Image</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['is_image_shown'] == 1 ? 'Yes' : 'No';?></td>
			</tr>
			<tr>	
				<td><b>Show Text</b></td>
				<td>&nbsp;</td>
				<td><?php echo $v['is_text_shown'] == 1 ? 'Yes' : 'No';?></td>
			</tr>
			<tr>
				<td colspan="3">
					<a href="<?php echo $v['image_link'];?>" target="_blank">
					<img src="<?php echo base_url($v['image_url']);?>" style="display: block !important;"/>
					</a><br/>
					<a href="<?php echo $v['text_link'];?>" target="_blank">
						<?php echo $v['text_shown'];?>
					</a>
				</td>
			</tr>

			<tr><td colspan="3"><hr style="" /></td></tr>
			<?php			
				}
			}
			?>
		</table>
	</div>
</div>