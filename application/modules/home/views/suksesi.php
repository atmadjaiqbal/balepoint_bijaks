<div class="row-fluid">
<?php
  foreach($suksesi as $key => $val)
  {
?>
     <div class="span12">
        <?php $dt['val'] = $val; ?>
        <?php //if(isset($_GET["mk"])){ ?>
        <?php $this->load->view('pilpres_template', $dt); ?>
        <?php //}else{ ?>
        <?php //$this->load->view('template/suksesi/tpl_suksesi_home_capres', $dt); ?>
        <?php //} ?>
     </div>
<?php
  }
?>
</div><!-- end row-fluid -->

<div class=" div-line-small"></div>
<div class="row" id="home-suksesi" style="margin-top:20px; display:none;">

  <div id="suksesiCarousel" class="slide">
    <div class="carousel-inner">
<?php
if(!empty($moresuksesi))
{
    $y = 0;
    foreach($moresuksesi as $key => $val)
    {
        foreach($val['status'] as $idx => $status){
            if(intval($status['draft']) == 1){
                if(intval($status['status']) == 1)
                {
                    $status_suksesi = 'Survey / Prediksi';
                } else {
                    if(intval($status['status']) == 2)
                    {
                        $status_suksesi = 'Putaran Pertama';
                    } else {
                        $status_suksesi = 'Putaran Kedua';
                    }
                }
                $index_trace_status = $idx;
            }
        }

        $suksesi_url = base_url() . 'suksesi/index/'.$val['id_race'].'-'.urltitle($val['race_name']);
        $status_pilkada = (!empty($val['status_akhir_suksesi']) && $val['status_akhir_suksesi'] <> 'None' ? 'Status : '.$val['status_akhir_suksesi'] : '');

        $map_photo = '';
        $logo_photo = '';
        foreach($val['photos'] as $foto){
            if(intval($foto['type']) === 2){
                $map_photo = $foto['thumb_url'];
            }elseif(intval($foto['type']) === 1){
                $logo_photo = $foto['thumb_url'];
            }
        }

        $suksesi_item = '<a href="'.$suksesi_url.'">
                           <div class="content-logo">
                             <div><img class="lazy" src="'.$logo_photo.'"  style="width: 70px; height: 70px;"></div>
                             <div class="content-text">'.$val['race_name'].'</div>
                             <div class="content-text">'.date('d F Y',strtotime($val['tgl_pelaksanaan'])).'</div>
                             <div class="content-text"><strong><p style="font-size:10px;">'.$status_pilkada.'</p></strong></div>
                           </div>
                          </a>';
        if($val['id_race'] != '274' && $val['id_race'] != '275')
        {
          if($y == 1) $suksesi_img1 = $suksesi_item; if($y == 2) $suksesi_img2 = $suksesi_item;
          if($y == 3) $suksesi_img3 = $suksesi_item; if($y == 4) $suksesi_img4 = $suksesi_item;
          if($y == 5) $suksesi_img5 = $suksesi_item; if($y == 6) $suksesi_img6 = $suksesi_item;
          if($y == 7) $suksesi_img7 = $suksesi_item;
          $y++;
        }
    }
?>
    <div class="item active">
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img1; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img2; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img3; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img4; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img5; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img6; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img7; ?></div>
        </div>
    </div>

<?php
}
?>
    </div>
      <!--a class="left carousel-control" href="#suksesiCarousel" data-slide="prev">‹</a>
      <a class="right carousel-control" href="#suksesiCarousel" data-slide="next">›</a-->
  </div>

</div>
<br/>

<div id="slngkp" class="row-fluid" style="display:none;">
    <div class="span6 text-left"></div>
    <div class="span6 text-right">
        <a href="<?php echo base_url(); ?>suksesi" class="btn btn-mini" >Suksesi Dan Survey Selengkapnya</a>
    </div>
</div>

<div class="row-fluid">
  <div class="span12">
    <a id="show_hide_qc" class="btn btn-small btn-default">Lihat Selengkapnya</a>
  </div>
</div>

<script type="text/javascript">
  $(function(){
      $('#show_hide_qc').click(function(){
        $('.tocollapse').fadeIn( 800, function(){
            $('.tocollapse').css('display', '');
            $('#home-suksesi').fadeIn( 800, function(){
                $('#home-suksesi').css('display', '');
                $('#slngkp').css('display', '');
            });
        });

        $(this).remove();
      })
  })
</script>
