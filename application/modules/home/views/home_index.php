<!--
<script type="text/javascript">
    $(document).ready(function() {

        var id = '#dialog';

        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        //Set heigth and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});

        //transition effect
        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow",0.8);

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);

        //transition effect
        $(id).fadeIn(2000);

        //if close button is clicked
        $('.window .close').click(function (e) {
            //Cancel the link behavior
            e.preventDefault();

            $('#mask').hide();
            $('.window').hide();
        });

        //if mask is clicked
        $('#mask').click(function () {
            $(this).hide();
            $('.window').hide();
        });

    });

</script>

<style type="text/css">
    #mask {position:absolute;left:0;top:0;z-index:9000;background-color:#000;display:none;}
    #boxes .window {position:absolute;left:0;top:0;width:960px;height:700px;display:none; z-index:9999;padding:5px;}
    #boxes #dialog {width:960px;height:720px;padding:5px;background-color:#ffffff;}
    #boxes .close-overlay {color: #000000;font-family:helvetica, "Times New Roman", arial;position: absolute;top:0px;}
</style>

-->
<script type="text/javascript">
    $(function() {
        $(window).scroll( function(){
            $('.fadeInBlock').each( function(i){
                var top_of_object = $(this).position().top - 600;
                var top_of_window = $(window).scrollTop();
                if( top_of_object <= top_of_window ){
                    $(this).animate({'opacity':'1'},250);
                }
            });       
        });
    });
</script>
<style type="text/css">
    .fadeInBlock {
        opacity:0;
    }
    .text_adds {
        font-family: "Courier New", Courier, monospace !important;
        font-size: 18px !important;
        font-weight: bold !important;
        color: black !important;
        text-align: center !important;
    }
</style>

<div id="boxes">
    <div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window">
        <img src="<?php echo base_url('assets/images/launch-bijak-fullpage.jpg'); ?>" style="background: rgba(0, 0, 0, 0);" >
        <a href="#" class="close">x</a>
    </div>
    <!-- Mask to cover the whole screen -->
    <div style="width: 1478px; height: 900px; display: none; opacity: 0.8;" id="mask"></div>
</div>

<!-- ######### headline ####### -->
<div class="container" <?php echo isset($_GET['style']) ? "margin-top:29px;" : ""; ?>>
    <div class="sub-header-container" >
        <?php if (isset($headline) && !empty($headline)) echo $headline; ?>

        <?php $this->timeline->sub_header('bijak'); ?>
        
        <!-- <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div> -->

        <!-- div id='disclaimer' class="sub-header-container"></div -->
    </div>

    <div style="width: 960px;margin-left: auto;margin-right: auto;margin-top: 0;height: 86px;"> 
        <div style="float: left;width: 320px;">
            <a href="<?php echo base_url('hotpages/badairestorasi');?>">
                <img src="<?php echo base_url('assets/images/hotpages/badairestorasi/banner.jpg');?>" style="width: 100%;height:85px;"/>
            </a>
        </div>      
        <div style="float: left;width: 320px;">
            <a href="<?php echo base_url('hotpages/petakatambang');?>">
                <img src="<?php echo base_url('assets/images/hotpages/tambang/salim-kancil-banner.jpg');?>" style="width: 100%;height:85px;"/>
            </a>
        </div>      

        <div style="float: left;width: 320px;">
            <a href="<?php echo base_url('hotpages/kampus_abal2');?>">
                <img src="<?php echo base_url('assets/images/hotpages/banner19.jpg');?>" style="width: 100%;height:85px;"/>
            </a>
        </div>    	
        <?php /*
        <div style="float: left;width: 320px;">
            <a href="<?php echo base_url('hotpages/daruratasap');?>">
                <img src="<?php echo base_url('assets/images/hotpages/asap/kabut-asap-banner.jpg');?>" style="width: 100%;height:85px;"/>
            </a>
        </div>
        */?>

    </div>


</div>


<?php
if(isset($bijaks_ticker))
{
    ?>
    <div class="container">
        <?php echo $bijaks_ticker; ?>
        <p id="quickcount"></p>
    </div>
<?php
}
?>


<!-- Banner Ads begin  -->
<!-- Date : 30 June 2015   -->
<!-- Firstly Ibolz Ads for apps -->
<?php
/*
$link_ibolz = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
$link_ntmc = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.ntmc";

$text1 = "100% Dangdut, 100% Goyang, 100% Gratis!!! klik disini";
$text2 = "Ribuan Film Horor dan Action, Gratis!!! UNTUK ANDA YANG BERANI !!! KLIK DI SINI";
$text3 = "Kini anda bisa menonton Film berkualitas HD langsung dari smartphone anda, klik di sini";
$text4 = "Pantau Sekarang Ratusan CCTV yg tersebar di seluruh Indonesia, LIVE !!!";
$text5 = "Pantau perayaan Idul Adha lewat CCTV yang tersebar di titik penting di kotamu";
$text7 = "Musik Dangdut, Pop, New Hits, Semuanya ada di sini !!!";
$text8 = "takut terjebak macet saat perayaan idul adha dan liburan panjang? cari tahu dulu situasinya disini";

$ads_banner1 = '<a href="http://id.ibolz.tv/home/channel_detail/19055066575480387f8490e/19055066575480387f8490e/channel" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092801.jpg'.'"/></a>
                <a href="'.$link_ibolz.'" target="_blank"><div class="text_adds">'.$text1.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner2 = '<a href="http://id.ibolz.tv/home/channel_detail/1371339731548545414abe7/1371339731548545414abe7/channel" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092802.jpg'.'"/></a>
                <a href="'.$link_ibolz.'" target="_blank"><div class="text_adds">'.$text3.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner3 = '<a href="http://id.ibolz.tv/home/channel_detail/54275805254854568af8bc/54275805254854568af8bc/channel" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092803.jpg'.'"/></a>
                <a href="'.$link_ibolz.'" target="_blank"><div class="text_adds">'.$text2.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner4 = '<a href="http://ntmc.ibolz.tv" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092804.jpg'.'"/></a>
                <a href="'.$link_ntmc.'" target="_blank"><div class="text_adds">'.$text4.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner5 = '<a href="http://ntmc.ibolz.tv" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092805.jpg'.'"/></a>
                <a href="'.$link_ntmc.'" target="_blank"><div class="text_adds">'.$text5.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner6 = '<a href="http://ntmc.ibolz.tv" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz2015092806.jpg'.'"/>
                <a href="'.$link_ntmc.'" target="_blank"><div class="text_adds">'.$text8.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner7 = '<a href="'.$link_ibolz.'" target="_blank">
                <img src="'.base_url() . 'assets/images/adsbanner/ibolz20150904_07.jpg'.'"/>
                <div class="text_adds">'.$text7.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner8 = '<a href="'.$link_ibolz.'" target="_blank">
                <img src="'.base_url() . 'assets/images/adds8.jpg'.'"/>
                <div class="text_adds">'.$text4.'<span style="margin-left: 40px;color:red;font-size: 18px;font-weight: bold;font-family : Courier New, Courier, monospace;">(download here)</span></div></a><br>';
$ads_banner = array($ads_banner1, $ads_banner6, $ads_banner2, $ads_banner8, $ads_banner3, $ads_banner7, $ads_banner4, $ads_banner5);
shuffle($ads_banner);
*/
// $FireFox = preg_match('/Firefox/i', $_SERVER['HTTP_USER_AGENT']);
// $InterEx = preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT']);
// $Opera   = preg_match('/Opera/i', $_SERVER['HTTP_USER_AGENT']);
// $Chrome  = strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome');
// $Safari  = strpos($_SERVER['HTTP_USER_AGENT'], 'Safari');

// //do something with this information
// if( $FireFox ){
//     $_link_download = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
// }else if($InterEx){
//     $_link_download = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
// }else if($Opera){
//     $_link_download = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
// }else if($Chrome){
//     $_link_download = "https://play.google.com/store/apps/details?id=air.com.balepoint.ibolz.id.prod";
// }else if($Safari){
//     $_link_download = "http://id.ibolz.tv";
// }

?>
<div class="container fadeInBlock">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- <a href="<?php echo $_link_download;?>"> -->
                <?php echo $ads_banner[0]; ?>
            <!-- </a> -->
        </div>
    </div>
</div>
<!-- End Of Ads Banner -->

<!-- row for event and caleg 2014 -->

<!-- div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <div class="span4">

                < ?php if(isset($sekilas)) { ?>
                    < ?php echo $sekilas; ?>
                < ?php } ?>

            </div>

            <div class="span8">

                < ?php if(isset($banner_pilkada)) { ?>
                    < ?php echo $banner_pilkada; ?>
                < ?php } ?>

                < ?php if(isset($banner_isis)) { ?>
                    < ?php echo $banner_isis; ?>
                < ?php } ?>

                < ?php if(isset($banner_gaza)) { ?>
                    < ?php echo $banner_gaza; ?>
                < ?php } ?>

            </div>

        </div>
    </div>
</div>
<br/ -->

<!-- News Content for bijaks Ver.2.6 -->
<div class="container">
<div class="sub-header-container">
<div class="row-fluid">
    <div class="span8">

        <div class="row-fluid" style="height: 390px;">
            <div class="home-title-section hp-label hp-label-hitam" style="background-color:#ffffff;">
                <span class="hitam"><a href="<?php echo base_url().'news/index/nasional'; ?>">POLITIK NASIONAL</a></span>
            </div>
            <div id="parlehot_container" data-newstype="9" data-page='1' class="newshot-container komunitas-scoring"></div>
            <div class="row-fluid">
                <div class="span6 text-left">
                    <a id="parlehot_loadmore" data-tipe="1" class="btn btn-mini" >10 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url().'news/index/nasional'; ?>" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
        </div>

        <div class="row-fluid" style="height: 390px;">
            <div class="home-title-section hp-label hp-label-hitam" style="background-color:#ffffff;">
                <span class="hitam"><a href="<?php echo base_url().'news/index/hukum'; ?>">POLITIK HUKUM</a></span>
            </div>
            <div id="newshot_container" data-newstype="1" data-page='1' class="newshot-container komunitas-scoring"></div>
            <div class="row-fluid">
                <div class="span6 text-left">
                    <a id="newshot_loadmore" data-tipe="1" class="btn btn-mini" >10 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url().'news/index/hukum'; ?>" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
        </div>
    </div>

    <div class="span4">
        <!-- HOT ISSUE -->
        <div class="row-fluid">
            <div class="span12">
                <div  class="home-title-section hp-label hp-label-coklat"  style="background-color: #D8BABA;">
                    <span class="hitam"><a href="<?php echo base_url().'news/index/headline'; ?>">TAJUK UTAMA</a></span>
                </div>
                <div id="headline_container" data-tipe="1" data-page='1' class="home-issue-container komunitas-scoring"></div>
                <div class="row-fluid" style="margin-bottom: 2px;">
                    <div class="span6 text-left">
                        <a id="headline_loadmore" data-tipe="1" class="btn btn-mini" >10 Berikutnya</a>
                    </div>
                    <div class="span6 text-right">
                        <a href="<?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container fadeInBlock">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- <a href="<?php echo $_link_download;?>"> -->
                <?php echo $ads_banner[1]; ?>
            <!-- </a> -->
        </div>
    </div>
</div>

<div class="row-fluid fadeInBlock">
    <div class="span12">

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/ekonomi">POLITIK EKONOMI</a></span>
                </div>
                <div id="ekonomi_container" data-tipe="1" data-category="ekonomi" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="ekonomi_loadmore" data-holder="hs-place-ekonomi" data-category="ekonomi" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/ekonomi" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-ekonomi"></div>
        </div>

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/parlemen">POLITIK PARLEMEN</a></span>
                </div>
                <div id="nasional_container" data-tipe="1" data-category="parlemen" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="nasional_loadmore" data-category="parlemen" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/parlemen" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-nasional"></div>
        </div>

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/internasional">POLITIK INTERNASIONAL</a></span>
                </div>
                <div id="internasional_container" data-tipe="1" data-category="internasional" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="inter_loadmore" data-category="internasional" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/internasional" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-internasional"></div>
        </div>
    </div>
</div>

<div class="row-fluid fadeInBlock">
    <div class="span12">
        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/indobarat">POLITIK DAERAH BARAT</a></span>
                </div>
                <div id="indobar_container" data-tipe="1" data-category="indobarat" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="indobar_loadmore" data-category="indobarat" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/indobarat" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-indobarat"></div>
        </div>

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/indoteng">POLITIK DAERAH TENGAH</a></span>
                </div>
                <div id="indoteng_container" data-tipe="1" data-category="indoteng" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="indoteng_loadmore" data-category="indoteng" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/indoteng" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-indoteng"></div>
        </div>

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/indotim">POLITIK DAERAH TIMUR</a></span>
                </div>
                <div id="indotim_container" data-tipe="1" data-category="indotim" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="indotim_loadmore" data-category="indotim" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/indotim" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-indotim"></div>
        </div>
    </div>
</div>

<div class="row-fluid fadeInBlock">
    <div class="span12">

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/lingkungan">POLITIK LINGKUNGAN</a></span>
                </div>
                <div id="lingkungan_container" data-tipe="1" data-category="lingkungan" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="lingkungan_loadmore" data-category="lingkungan" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/lingkungan" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-lingkungan"></div>
        </div>

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/kesenjangan">POLITIK KESENJANGAN</a></span>
                </div>
                <div id="kesenjangan_container" data-tipe="1" data-category="kesenjangan" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="kesenjangan_loadmore" data-category="kesenjangan" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/kesenjangan" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-kesenjangan"></div>
        </div>

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/sara">POLITIK SARA</a></span>
                </div>
                <div id="sara_container" data-tipe="1" data-category="sara" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="sara_loadmore" data-category="sara" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/sara" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-sara"></div>
        </div>
    </div>
</div>

<div class="row-fluid fadeInBlock">
    <div class="span12">

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/komunitas">POLITIK SOSIAL MEDIA</a></span>
                </div>
                <div id="sosmed_container" data-tipe="1" data-category="komunitas" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="sosmed_loadmore" data-category="komunitas" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/komunitas" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-komunitas"></div>
        </div>

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/gayahidup">POLITIK GAYA HIDUP</a></span>
                </div>
                <div id="gayahidup_container" data-tipe="1" data-category="gayahidup" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="gayahidup_loadmore" data-category="gayahidup" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/gayahidup" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-gayahidup"></div>
        </div>

        <div class="span4">
            <div class="hp-place ">
                <div class="home-title-section hp-label hp-label-hitam">
                    <span class="hitam"><a href="<?php echo base_url(); ?>news/index/reses">POLITIK SENGGANG</a></span>
                </div>
                <div id="reses_container" data-tipe="1" data-category="reses" data-page='1' class="komunitas-scoring"></div>
            </div>

            <div id="" class="row-fluid">
                <div class="span6 text-left">
                    <a id="reses_loadmore" data-category="reses" data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
                <div class="span6 text-right">
                    <a href="<?php echo base_url(); ?>news/index/reses" class="btn btn-mini" >Selengkapnya</a>
                </div>
            </div>
            <p></p>
            <div class="hp-footer last-reses"></div>
        </div>

    </div>
</div>
</div>
</div>
<!-- End News Content -->

<!-- <div class="container fadeInBlock">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>"> -->
            <!--img src="< ?php echo base_url() . 'assets/images/banner-agustusan-bijaks.jpg'; ?>" -->
<!--         </div>
    </div>
</div> -->

<!-- Banner Ads -->
<div class="container fadeInBlock">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- <a href="<?php echo $_link_download;?>"> -->
                <?php echo $ads_banner[2]; ?>
            <!-- </a> -->
        </div>
    </div>
</div>

<!-- ROW 3 scandal, survey, hot issue -->
<div class="container fadeInBlock">
    <div class="sub-header-container">
        <div class="row-fluid">
            <div class="span12">
                <!-- SCANDAL -->
                <div class="row-fluid">
                    <div class="span12">
                        <div id="home-skandal" style="margin-bottom: 150px;">
                            <div class="home-title-section hp-label hp-label-skandal">
                                <span class="hitam"><a href="<?=base_url();?>scandal">SKANDAL</a></span>
                            </div>
                            <?php echo $scandal;?>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row-fluid text-right">
                    <a href="<?php echo base_url(); ?>scandal" class="">[Simak <?php echo $formattedNbScandals; ?> ribu Skandal lainnya]</a>
                </div>
            </div>

            <!-- div class="span4">
                <!-- SURVEY >
                <div class="row-fluid">
                    <div class="span12 ">
                        <div  class="home-title-section hp-label hp-label-biru">
                            <span class="hitam"><a href="<?=base_url();?>survey">POLLING KOMUNITAS</a></span>
                        </div>
                        <img src="< ? =base_url();?>assets/images/polling.jpg" >
                        < ?php echo $survey;?>
                    </div>
                </div>
                <br/>
                <div class="row-fluid text-right">
                    <a href="< ?php echo base_url(); ?>survey" class="btn btn-small btn-default">Lihat <?php echo $surpey; ?> survey lainnya</a>
                </div>
            </div -->
        </div>
    </div>
</div>

<div class="container fadeInBlock">
    <div class="sub-header-container">
        <div class="row-fluid">
          <?php echo $ads_banner[3]; ?>
        </div>
    </div>
</div>

<!--   row 2 hot profile -->
<div class="container fadeInBlock">
    <div class="sub-header-container">
        <?php if (isset($hot_profile) && !empty($hot_profile)) echo $hot_profile; ?>
    </div>
    <div class="row-fluid text-right">
        <a href="<?php echo base_url(); ?>politik" class="">[Simak <?php echo number_format(($tokoh+4840)/1000,1,",","."); ?> ribu tokoh lainnya]</a>
    </div>
    <!-- div class=" div-line-small"></div-->
    <br/>
</div>

 <br/>
<!-- ######### SUKSESI ####### -->
<!-- div class="container">
    <div class="sub-header-container" style="margin-bottom: 0px;">
        <div id="" class="home-title-section hp-label hp-label-kuning" style="background-color: #FFE930;margin-bottom: 10px;">
            <span class="hitam">
                <a href="< ?php echo base_url(); ?>suksesi">SUKSESI DAN SURVEY</a>
            </span>
        </div>

        < ? php if (isset($suksesi) && !empty($suksesi)) :?>
            < ?php //echo $suksesi; ?>
        < ?php endif;? >
    </div>
</div>

< ?php if(isset($banner_pilpres)) { ?>
    <div class="container" style="margin-top:5px;">
        <div class="sub-header-container">
            < ?php echo $banner_pilpres; ?>
        </div>
    </div>
< ? php } ? >
<br/ -->

<!-- remove date 15 Agustus 2014 -->
<!--div  class="container">
    <div class="sub-header-container" style="margin-bottom: 0px;">
        <div id="" class="home-title-section hp-label hp-label-kuning" style="margin-bottom: 10px;background-color: #FFE930;">
            <span class="hitam">
                <a href="< ?php echo base_url(); ?>suksesi"><strong><span style="font-size: 24px;margin-left:4px;">QUICK COUNT&nbsp;&nbsp;&nbsp;&nbsp;VS&nbsp;&nbsp;&nbsp;&nbsp;SURVEY SEBELUMNYA PILPRES 2014</span></strong></a>
            </span>
           
        </div>
        < ?php if (isset($suksesi) && !empty($suksesi)) :?>
            < ?php echo $suksesi; ?>
        < ?php endif;?>
    </div>
</div>
<br/ -->

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid fadeInBlock" style="margin-top:-20px;margin-bottom: 10px;">
            <img src="<?php echo base_url('assets/images/yuk-gabung.gif'); ?>">
            <!--img src="< ? php echo base_url('assets/images/banner-agustusan-bijaks.jpg'); ?>" -->
        </div>
        <div class="row-fluid fadeInBlock">
            <div class="span8">
                <div class="row-fluid" id="home-opini">
                    <div class="home-title-section hp-label hp-label-coklat" style="background-color: #9c6851;margin-top:0px;">
                        <span class="hitam"><a style="cursor:default;color:#ffffff;">KOMENTAR ANDA</a></span>
                    </div>
                    <div id="komentar_container" data-tipe="1" data-page='1'></div>
                </div>
                <div class="row-fluid">
                    <div class="span12 text-right">
                        <a id="komentar_loadmore"  data-tipe="1" class="btn btn-mini" >20 Berikutnya</a>
                    </div>
                </div>
            </div>

            <div class="span4">
                <div class="row-fluid" id="home-blog">
                    <div class="home-title-section hp-label hp-label-coklat" style="background-color: #9c6851;margin-top:0px;">
                        <span class="hitam"><a style="cursor:default;color:#ffffff;">BLOG FAVORIT ANDA</a></span>
                    </div>
                    <div id="blog_container" data-tipe="1" data-page='1'></div>
                </div>
                <div class="row-fluid">
                    <div class="span12 text-right">
                        <a id="blog_loadmore"  data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                    </div>
                </div>
            </div>
        </div>

		    <div class="sub-header-container">
		        <div class="row-fluid">
		            <!-- <a href="<?php echo $_link_download;?>"> -->
		                <?php echo $ads_banner[4]; ?>
		            <!-- </a> -->
		        </div>
		    </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="row-fluid" id="home-latest">
                    <div class="home-title-section hp-label hp-label-coklat" style="background-color: #9c6851;margin-top:0px;">
                        <span class="hitam"><a style="cursor:default;color:#ffffff;">LATEST BLOG</a></span>
                    </div>
                    <div class="blog-col effect-1">
                        <div id="bloglatest_container" data-tipe="1" data-page='1'></div>
                    </div>
                    <?php for($l=2;$l<=16;$l++){?>
                    <div class="blog-col effect-1">
                        <div id="bloglatest_container<?php echo $l;?>" data-tipe="1" data-page='<?php echo $l;?>'></div>
                    </div>
                    <?php };?>
                </div>

                <div class="row-fluid">
                    <div class="span12 text-center">
                    <?php for($r=2;$r<=17;$r++){?>
                        <div id="more<?php echo $r;?>" class="loadmoreblog loadmoreblog-bg" style="display: none;">
                            <a id="bloglatest_loadmore<?php echo $r;?>"  data-tipe="1" class="loadmoreblog-text">Berikutnya</a>
                        </div>
                    <?php };?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br/>

<script src="<?php echo base_url(); ?>public/script/jquery/highcharts.js"></script>
<script src="<?php echo base_url(); ?>public/script/jquery/modules/exporting.js"></script>
<script>
    $(document).ready(function() {
        $('#show_hide_map').click(function(){
            $('.mapcollapse').fadeIn( 800, function(){
                $('.mapcollapse').css('display', '');
            });
            $(this).remove();
        })

        /*
         $(window).scroll(function(){
         if($(window).scrollTop() == $(document).height() - $(window).height()){
         bloglatest_onscroll('#bloglatest_container');
         }
         });
         */

        $(window).scroll(function() {
            $(".pin").each( function() {
                if( $(window).scrollTop() > $(this).offset().top - 500 ) {
                    $(this).css('opacity',1);
                }
            });
        });
    });
</script>

