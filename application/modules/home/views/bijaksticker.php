<?php 

if(!empty($rsevent))
{
?>
<div id="bijaksticker" class="sub-header-container">
    <div class="row-fluid">
        <div class="section-event">
            <?php

            $_event_date = array(); $_event_name = array(); $_date_count = array();
            foreach($rsevent as $row)
            {
                $_event_date[] = date('d F Y',strtotime($row['event_date']));
                $_event_name[] = $row['event_name'];
                $_event_alias[] = $row['event_alias'];
                $_date_count[] = date('d F Y',strtotime($row['event_date']));

                $_event_day = strtotime($row['event_date']);
                $_event_now = time();
                $_event_diff = ($_event_now - $_event_day);
                $_event_left = floor($_event_diff/(60*60*24));
                $_event_left = str_replace('-', '', $_event_left);

                switch(strlen($_event_left))
                {
                    case '3' :$_Digit01[] = substr($_event_left, 0, 1);$_Digit02[] = substr($_event_left, 1, 1);$_Digit03[] = substr($_event_left, 2, 1);break;
                    case '2' :$_Digit01[] = '0';$_Digit02[] = substr($_event_left, 0, 1);$_Digit03[] = substr($_event_left, 1, 1);break;
                    case '1' :$_Digit01[] = '0';$_Digit02[] = '0';$_Digit03[] = substr($_event_left, 0, 1);break;
                }
            }

            ?>
            <a href="<?php echo base_url('page/jadwalpemilu');?>">
            <div class="event-03">
                <div class="countdown1">
                    <div class="digit-container">

                   <?php if(isset($_Digit01[0])) { ?> <div class="digit"><p><?php echo $_Digit01[0]; ?></p></div> <?php } ?>
                   <?php if(isset($_Digit02[0])) { ?> <div class="digit"><p><?php echo $_Digit02[0]; ?></p></div> <?php } ?>
                   <?php if(isset($_Digit03[0])) { ?> <div class="digit"><p><?php echo $_Digit03[0]; ?></p></div> <?php } ?>

                    </div>
                    <div class="day-gain pull-right">
                        <em class="day-ev">hari lagi</em>
                    </div>
                </div>

                <div class="event">
                    <p class="event-name"><?php echo strtoupper($_event_alias[0]);?></p>
                    <p class="event-date"><em><?php echo $_event_date[0];?></em></p>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="event-03">
                <div class="countdown1">
                    <div class="digit-container">
                        <?php if(isset($_Digit01[1])) { ?> <div class="digit"><p><?php echo $_Digit01[1]; ?></p></div> <?php } ?>
                        <?php if(isset($_Digit02[1])) { ?> <div class="digit"><p><?php echo $_Digit02[1]; ?></p></div> <?php } ?>
                        <?php if(isset($_Digit03[1])) { ?> <div class="digit"><p><?php echo $_Digit03[1]; ?></p></div> <?php } ?>
                    </div>
                    <div class="day-gain pull-right">
                        <em class="day-ev">hari lagi</em>
                    </div>
                </div>

                <div class="event">
                    <p class="event-name"><?php echo $_event_alias[1];?></p>
                    <p class="event-date"><em><?php echo $_event_date[1];?></em></p>
                </div>
            </div>
            </a>
            <!--<div class="event-03">
                <div class="countdown1">
                    <?php /*if(isset($_Digit01[1])) { */?> <div class="digit"><?php /*echo $_Digit01[1]; */?></div> <?php /*} */?>
                    <?php /*if(isset($_Digit02[1])) { */?> <div class="digit"><?php /*echo $_Digit02[1]; */?></div> <?php /*} */?>
                    <?php /*if(isset($_Digit03[1])) { */?> <div class="digit"><?php /*echo $_Digit03[1]; */?></div> <?php /*} */?>
                    <h4 class="day-ev">hari lagi</h4>
                </div>


                <div class="event">
                    <p class="event-name"><?php /*echo strtoupper($_event_alias[1]);*/?></p>
                    <p class="event-date"><em>Tanggal :<?php /*echo $_event_date[1];*/?></em></p>
                </div>
            </div>

            <div class="event-selengkapnya">
              <a href="<?php /*echo base_url();*/?>page/jadwalpemilu" class="btn btn-mini" >Jadwal Lainnya</a>
            </div>-->

        </div>

    <div class="section-top">
        <?php
        if($rstopten)
        {
            $D = 0;
            foreach($rstopten as $key => $row)
            {
                if($row['set_headline'] != '0')
                {
                  $topten_title[$D] = $row['alias_name'];
                  $topten_id[$D] = $row['id_topten_category'];
                  $D++;
                }
            }
        }


        ?>

      <div style="float: left;width: 230px;">
       <ul class="topten">
          <li class=""><a style="color: #383838;" href="<?php echo base_url('topten/toptens/'.(isset($topten_id[0]) ? $topten_id[0] : ''));?>"><?php echo (isset($topten_title[0]) ? $topten_title[0] : '');?></a></li>
          <li class=""><a style="color: #383838;" href="<?php echo base_url('topten/toptens/'.(isset($topten_id[1]) ? $topten_id[1] : ''));?>"><?php echo (isset($topten_title[1]) ? $topten_title[1] : '');?></a></li>
       </ul>
      </div>

      <div style="float: left;margin-left:4px;width: 230px;">
       <ul class="topten">
          <li class=""><a style="color: #383838;" href="<?php echo base_url('topten/toptens/'.(isset($topten_id[2]) ? $topten_id[2] : ''));?>"><?php echo (isset($topten_title[2]) ? $topten_title[2] : '');?></a></li>
          <li class=""><a style="color: #383838;" href="<?php echo base_url('topten/toptens/'.(isset($topten_id[3]) ? $topten_id[3] : ''));?>"><?php echo (isset($topten_title[3]) ? $topten_title[3] : '');?></a></li>
       </ul>
      </div>

    </div>

    </div>
</div>


<?php
}
?>
