<?php

switch($rsSocial['comunity_type'])
{
    case '1' :
        $_text = $rsSocial['comment_text'];
        $comunity_icon = 'comment_icon_large.png';
        break;
    case '2' :
        $_text = $rsSocial['title'];
        $comunity_icon = 'like_icon_large.png';
        break;
    case '3' :
        $_text = $rsSocial['title'];
        $comunity_icon = 'dislike_icon_large.png';
        break;
}
?>

<div class="comunity" style="width: <?php echo $width;?>; background-color: <?php echo $BackColor; ?>;">
    <div class="comunity-foto">
        <a href="<?php echo base_url() . "komunitas/wall?uid=".$rsSocial['content_url'];?>">
            <img title="<?php echo $rsSocial['display_name']; ?>"
                 src='<?php echo badge_url($rsSocial['attachment_title'],'user/'.$rsSocial['user_id']) ; ?>'
                 data-src="<?php echo badge_url($rsSocial['attachment_title'],'user/'.$rsSocial['user_id']) ; ?>"
                 data-toggle='tooltip' class='tooltip-bottom' />
        </a>
    </div>
    <div class="comunity-name">
        <a href="<?php echo base_url() . "komunitas/wall?uid=".$rsSocial['content_url'];?>" title="<?php echo $rsSocial['display_name']; ?>">
            <?php echo substr(strtoupper($rsSocial['display_name']),0, 18); ?>
            <?php if (strlen($rsSocial['display_name']) > 19) { echo '...';} ?>
        </a>
        <p><a href="" title="<?php echo $rsSocial['account_id']; ?>"><strong>
            <?php echo substr($_text,0, 32); ?>
            <?php if (strlen($_text) > 33) { echo '...';} ?>
        </strong></a></p>
    </div>
    <div style="background: url('assets/images/<?php echo $comunity_icon; ?>');" class="comunity-icon"></div>


</div>
