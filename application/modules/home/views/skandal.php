<div style="background-color: #BBBBBB;width: 958px;">
    <div class="row-fluid">
<?php foreach($skandal as $key => $val) { ?>
    <div class="span4" style="margin-right: -8px;">
        <?php $dt['val'] = $val; ?>
        <?php $this->load->view('template/skandal/tpl_skandal_home', $dt); ?>
    </div>
<?php } ?>
    </div>
</div>

    <div class="headline-index" style="margin-top: 10px;margin-botom:10px;width: 958px;">
        <div class="title">
            <span class="index-title" style="float: left;">SKANDAL LAINNYA</span>
            <div style="float: right;">
                <!-- <a id="scandal_caro_left" class="left carousel-control" href="#scandalCarousel" data-slide="prev" style="font-size:45px;position: relative;float: left;width: 25px;height: 25px;">‹</a>
                <a id="scandal_caro_right" class="right carousel-control" href="#scandalCarousel" data-slide="next" style="margin-left:40px;margin-right:-10px;font-size:45px;position: relative;float: right;width: 25px;height: 25px;">›</a> -->
            </div>
        </div>
        <div id="scandalCarousel" class="lide">
            <div class="carousel-inner">
                <?php
                if (!empty($scandal_more)) {
                    $y = 1;
                    $carousel_img1 = '';
                    $carousel_img2 = '';
                    $carousel_img3 = '';
                    $carousel_img4 = '';
                    $carousel_img5 = '';
                    $carousel_img6 = '';
                    $carousel_img7 = '';
                    $carousel_img8 = '';
                    $carousel_img9 = '';
                    $carousel_img10 = '';
                    foreach($scandal_more as $i =>  $row){
                        $morePhotos = '';
                        $moreURL = base_url() . 'scandal/index/'.$row['scandal_id'].'-'.urltitle($row['title']);
                        if(count($row['photos']) > 0){
                            foreach($row['photos'] as $pot => $pitem){
                                if(isset($pitem['type'])){
                                    if($pitem['type'] == '1'){
                                        $morePhotos = $pitem['large_url'];
                                        if( file_exists('public/upload/image/skandal/'.$pitem['attachment_title'])){
                                            $morePhotos = $pitem['original_url'];
                                        }
                                    }
                                }
                            }

                            if(empty($morePhotos)){
                                $morePhotos = $row['photos'][0]['large_url'];
                                if( file_exists('public/upload/image/skandal/'.$row['photos'][0]['attachment_title'])){
                                    $morePhotos = $row['photos'][0]['original_url'];
                                }
                            }
                        } else {
                            $morePhotos = 'assets/images/thumb/noimage.jpg';
                        }

                        $contentID = $row['content_id'];
                        $carousel_item = '<a href="'.$moreURL.'">
                            <img class="tooltip-bottom" title="'.$row['title'].'" src="'.$morePhotos.'"  style="width: 200px; height: 150px;">
                            <div class="score-place score-place-overlay score" data-id="'.$contentID.'" ></div>
                            <div class="scandal-overlay" style="width: 100% !important;">
                                <p class="overlay-text"><strong>'.substr($row['title'],0 ,20).' ...</strong></p>
                            </div>
                          </a>';

                        if($y == 1) $carousel_img1 = $carousel_item;
                        if($y == 2) $carousel_img2 = $carousel_item;
                        if($y == 3) $carousel_img3 = $carousel_item;
                        if($y == 4) $carousel_img4 = $carousel_item;
                        if($y == 5) $carousel_img5 = $carousel_item;
                        if($y == 6) $carousel_img6 = $carousel_item;
                        if($y == 7) $carousel_img7 = $carousel_item;
                        if($y == 8) $carousel_img8 = $carousel_item;
                        if($y == 9) $carousel_img9 = $carousel_item;
                        if($y == 10) $carousel_img10 = $carousel_item;

                        $y++;
                    }
                    ?>
                    <!-- <div class="item active"> -->
                    <div>
                        <div style="float:left;margin-right: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img1; ?></div>
                        </div>
                        <div style="float:left;margin-right: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img2; ?></div>
                        </div>
                        <div style="float:left;margin-right: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img3; ?></div>
                        </div>
                        <div style="float:left;margin-right: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img4; ?></div>
                        </div>
                        <div style="float:left;">
                            <div class="news-img-item"><?php echo $carousel_img5; ?></div>
                        </div>
                    </div>
                    <!-- <div class="item"> -->
                    <div>
                        <div style="float:left;margin-right: 2px;margin-top: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img6; ?></div>
                        </div>
                        <div style="float:left;margin-right: 2px;margin-top: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img7; ?></div>
                        </div>
                        <div style="float:left;margin-right: 2px;margin-top: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img8; ?></div>
                        </div>
                        <div style="float:left;margin-right: 2px;margin-top: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img9; ?></div>
                        </div>
                        <div style="float:left;margin-top: 2px;">
                            <div class="news-img-item"><?php echo $carousel_img10; ?></div>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            var imgCount = 2;
            $('#scandalCarousel').carousel({
                interval:false
            });

            $('#scandalCarousel').bind('slide',function(en){
                console.debug(en);
                if(en.direction == 'left'){
                    $('#scandal_caro_left').data('slide', 'prev')
                    imgCount--;
                    if(imgCount == 0){
                        $('#scandal_caro_right').data('slide', '');
                        $('#scandal_caro_left').data('slide', 'prev');
                    }
                }else{
                    $('#scandal_caro_right').data('slide', 'next');
                    imgCount++;
                    if(imgCount == 2){
                        $('#scandal_caro_left').data('slide', '');
                        $('#scandal_caro_right').data('slide', 'next');
                    }

                }

            });

            $('.carousel-head').carousel({interval:false});

            var uri = "<?php echo base_url();?>powermap/skandal/";
            var width = '285';
            var height = '200';
            var el = $('.home-skandal-detail-map');

            var id = $(el[0]).data('id');
            var id2 = $(el[1]).data('id');

        });
    </script>
