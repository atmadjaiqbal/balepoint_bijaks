<?php

if(!empty($rsevent))
{
    ?>
    <div id="bijaksticker" class="sub-header-container">

        <div class="section-caleg">
            <a href="http://caleg2014.bijaks.net" target="_blank">
                <img class="caleg-image" src="<?php echo base_url();?>assets/images/caleg-image-header.png">
                <div class="ticker-overlay">
                    <h4 class="text-red">DAFTAR CALON TETAP</h4>
                    <h4 class="text-white">ANGGOTA DPR<br/>PEMILU 2014</h4>
                </div>
            </a>
        </div>

        <a href="<?php echo base_url();?>page/jadwalpemilu">
            <div class="section-event">
                <?php

                $_event_date = array(); $_event_name = array(); $_date_count = array();
                foreach($rsevent as $row)
                {
                    $_event_date[] = date('d F Y',strtotime($row['event_date']));
                    $_event_name[] = $row['event_name'];
                    $_date_count[] = date('d F Y',strtotime($row['event_date']));
                }

                ?>
                <div id="event-01">
                    <div class="countdown1">
                        <p class="days">00</p>
                    </div>
                    <h4 class="day-ev">HARI<br/>LAGI</h4>
                    <div class="event">
                        <p class="event-date"><?php echo $_event_date[0];?></p>
                        <p class="event-name"><?php echo $_event_name[0];?></p>
                    </div>
                </div>

                <div id="event-02">
                    <div class="countdown2">
                        <p class="days">00</p>
                    </div>
                    <h4 class="day-ev">HARI<br/>LAGI</h4>
                    <div class="event">
                        <p class="event-date"><?php echo $_event_date[1];?></p>
                        <p class="event-name"><?php echo $_event_name[1];?></p>
                    </div>
                </div>

            </div>
        </a>

        <div class="section-top">
            <ul class="topten">
                <li class=""><a href="">Top 10 Calon Presiden</a></li>
                <li class=""><a href="">Top 10 Calon Legislatif</a></li>
                <li class=""><a href="">Top 10 Tokoh Nasional</a></li>
                <li class=""><a href="">Top 10 Profile</a></li>
            </ul>


        </div>


    </div>

    <script>
        (function (e) {
            e.fn.countdown = function (t, n) {
                function i() {
                    eventDate = Date.parse(r.date) / 1e3;
                    currentDate = Math.floor(e.now() / 1e3);
                    if (eventDate <= currentDate) {
                        n.call(this);
                        clearInterval(interval)
                    }
                    seconds = eventDate - currentDate;
                    days = Math.floor(seconds / 86400);
                    seconds -= days * 60 * 60 * 24;
                    hours = Math.floor(seconds / 3600);
                    seconds -= hours * 60 * 60;
                    minutes = Math.floor(seconds / 60);
                    seconds -= minutes * 60;
                    days == 1 ? thisEl.find(".timeRefDays").text("day") : thisEl.find(".timeRefDays").text("days");
                    hours == 1 ? thisEl.find(".timeRefHours").text("hour") : thisEl.find(".timeRefHours").text("hours");
                    minutes == 1 ? thisEl.find(".timeRefMinutes").text("minute") : thisEl.find(".timeRefMinutes").text("minutes");
                    seconds == 1 ? thisEl.find(".timeRefSeconds").text("second") : thisEl.find(".timeRefSeconds").text("seconds");
                    if (r["format"] == "on") {
                        days = String(days).length >= 2 ? days : "0" + days;
                        hours = String(hours).length >= 2 ? hours : "0" + hours;
                        minutes = String(minutes).length >= 2 ? minutes : "0" + minutes;
                        seconds = String(seconds).length >= 2 ? seconds : "0" + seconds
                    }
                    if (!isNaN(eventDate)) {
                        thisEl.find(".days").text(days);
                        thisEl.find(".hours").text(hours);
                        thisEl.find(".minutes").text(minutes);
                        thisEl.find(".seconds").text(seconds)
                    } else {
                        alert("Invalid date. Example: 30 Tuesday 2013 15:50:00");
                        clearInterval(interval)
                    }
                }
                thisEl = e(this);
                var r = {
                    date: null,
                    format: null
                };
                t && e.extend(r, t);
                i();
                interval = setInterval(i, 1e3)
            }
        })(jQuery);


        $(document).ready(function () {
            function e() {
                var e = new Date;
                e.setDate(e.getDate() + 60);
                dd = e.getDate();
                mm = e.getMonth() + 1;
                y = e.getFullYear();
                futureFormattedDate = mm + "/" + dd + "/" + y;
                return futureFormattedDate
            }

            $(".countdown1").countdown({
                date: "<?php echo $_date_count[0]; ?>", // Change this to your desired date to countdown to
                format: "on"
            });

            $(".countdown2").countdown({
                date: "<?php echo $_date_count[1]; ?>", // Change this to your desired date to countdown to
                format: "on"
            });

        });
    </script>

<?php
}
?>