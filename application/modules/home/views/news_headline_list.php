<?php
foreach($val as $key=>$result){
    //echo '<pre>';print_r($result);exit;
    if($key == 10) exit;
    $_topic_id = !empty($result['topic_id']) ? $result['topic_id'] : '';
    $_id = !empty($result['id']) ? $result['id'] : '';
    $_slug = !empty($result['slug']) ? $result['slug'] : '';
    $_title = !empty($result['title']) ? $result['title'] : '';
    $modified_date = !empty($result['modified_date']) ? $result['modified_date'] : $result['date'];
    $news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
    $short_title = (strlen ($_title) > 47) ? substr($_title, 0, 44). ' ...' : $_title;
    if(!empty($result['image_thumbnail']))
    {
        $_image_thumb = $result['image_thumbnail'];
    } else {
        $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
    }

    $date2Show = mdate('%d %M %Y - %h:%i %A', strtotime($modified_date));
    ?>

    <div id="media-headline">
        <a title="<?php echo !empty($result['title']) ? $result['title'] : ''; ?>" href="<?php echo $news_url; ?>">
            <div class="img-place">
                <!--img class="lazy" data-original="<php echo $_image_thumb; ?>" -->
                <img src="<?php echo $_image_thumb; ?>">

            </div>
            <div class="ctn-place">
                <h5><?php echo $short_title; ?></h5>
                <p class="news-date"><?php echo $date2Show; ?></p>
                <!--div class="score-place score-place-left score" data-tipe="1" data-id="<?php //echo !empty($result['content_id']) ? $result['content_id'] : 0;?>" style="margin-top:-17px;"><?php // echo $result['komu']; ?></div-->
            </div>
        </a>

    </div>
    <!--div class="div-line-small"></div-->
<?php } ?>