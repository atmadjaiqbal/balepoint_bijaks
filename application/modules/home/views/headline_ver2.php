<style>
.smaller{
    font-weight: normal;
    font-size: .7em;
    color: #dadada;
    margin-top: 2px;
}
</style>
<?php

$data['line'] = '';
if (!empty($headline)) :

    if(!empty($like)){
        $showLike = '<li><span><strong>People Likes</strong></span><span>'.number_format(($like*2)).'</span>';
    }else{
        $like = 1;
        $showLike = '<li><span><strong>People Likes</strong></span><span>'.number_format(($like*2)).'</span>';
    }

    if(!empty($comment)){
        $showComment = '<li><span><strong>People Comment</strong></span><span>'.number_format(($comment * 1.5)).'</span>';
    }else{
        $comment = 1;
        $showComment = '<li><span><strong>People Comment</strong></span><span>'.number_format(($comment * 1.5)).'</span>';
    }

    if(!empty($news)){
        $showNews = '<strong>'.number_format(intval($news) + 179904).'</strong>';
    }else{
        $news = 1;
        $showNews = '<strong>'.number_format(intval($news) + 179904).'</strong>';
    }

    if(!empty($skandal)){
        $showSkandal = '<strong>'.number_format(intval($skandal) + 3833).'</strong>';
    }else{
        $skandal = 1;
        $showSkandal = '<strong>'.number_format(intval($skandal) + 3833).'</strong>';
    }

    if(!empty($tokoh)){
        $showTokoh = '<strong>'.number_format(intval($tokoh)+4840).'</strong>';
    }else{
        $tokoh = 1;
        $showTokoh = '<strong>'.number_format(intval($tokoh)+4840).'</strong>';
    }

    ?>


    <div id="slideheadline" class="carousel slide">

        <div id="slideouter">
            <div class="headlogo">
                <img src="<?php echo base_url('assets/images/bijaks_logo.png'); ?>" style="background: rgba(0, 0, 0, 0);" >
            </div>
            <div style="margin-top:20px;margin-left:20px;">
                <ul class=leaders>
                    <li><span><strong>Total News per day</strong></span><span>>&nbsp;200 !</span>
                    <?php echo $showLike;?>
                    <?php echo $showComment;?>
                    <li><span><strong>Total Page Views</strong></span><span>>&nbsp;5.000.000</span>
                </ul>
            </div>
            <div style="margin-top: 10px;margin-left: 30px;">
                <div class="ic_scandal" style="float:left;">
                    <div class="ic_scandal_center"><br/>
                        <div class="ic_scandal_total"><?php echo $showNews;?></div>
                        <div class="ic_scandal_news">Berita</div>
                        <div class="smaller">[sejak 2014]</div>
                    </div>
                </div>
                <div class="ic_scandal" style="float:left;">
                    <div class="ic_scandal_center"><br/>
                        <!--                        <div class="ic_scandal_year">2010 - Now</div>-->
                        <div class="ic_scandal_total"><?php echo $showSkandal;?></div>
                        <div class="ic_scandal_news">Scandal</div>
                        <div class="smaller">[sejak 2013]</div>
                    </div>
                </div>
                <div class="ic_scandal" style="float:left;">
                    <div class="ic_scandal_center"><br/>
                        <!--                        <div class="ic_scandal_year">2010 - Now</div>-->
                        <div class="ic_scandal_total"><?php echo $showTokoh;?></div>
                        <div class="ic_scandal_news">Tokoh</div>
                        <div class="smaller">[sejak 2014]</div>
                    </div>
                </div>
                <!--
                    <div class="ic_suksesi" style="float:left;">
                        <span><strong><?php //echo number_format($suksesi);?></strong></span>
                    </div>
    -->
                <!--                <div class="ic_tokoh" style="float:left;">-->
                <!--                    <span>-->
                <!--                        <strong>-->
                <!--                        --< ? php //echo number_format($tokoh); ?>
                <!--                        </strong>-->
                <!--                    </span></div>-->
            </div>
            <!-- <div class="disclaimer-head" style="margin-top: 89px;float: none;width: auto;text-align: center;">
                <p>Disclaimer: data terkumpul sejak 2010 - sekarang</p>
            </div> -->


        </div>
        <div class="carousel-inner">

            <!-- SLIDER QUICK COUNT -->
            <div class="item active slideimg">
               <img src="<?php echo base_url().'assets/images/spalsh-presiden.jpg';?>" alt="" style="width: 960px; height: auto;">
               <!-- img src="<?php echo base_url().'assets/images/splash-idul-adha-bijaks.jpg';?>" alt="" style="width: 960px; height: auto;" -->
            </div>

            <!-- <div class="item active slideimg">
                 <img src="<?php echo base_url().'assets/images/KAA.jpg';?>" alt="" style="width: 960px; height: auto;">
                 
                 <div class="carousel-caption" id="slidecaption">
                   <div class="sectioncategories" style="background-color:#4DAD40;opacity:0.8;">
                     <p>PERISTIWA</p>
                   </div>
                   <div class="sectiontitle">
                     <div class="headline-date-view">21 - 27 April 2015</div>
                     <div class="headline-title">
                        <a href="http://www.bijaks.net/hotpages/kaa" title="Peristiwa Konferensi Asia Afrika">Konferensi Asia Afrika 2015</a>
                     </div>
                   </div>
                 </div>                 
            </div> -->


            <?php
            $i=1;
            $content_callback 	= base_url('ajax/count_content');
            $content_like_uri 	= '';
            $content_dislike_uri = '';
            $_color = "#9E5B3E;";
            $content_date = '';
            $url = '';

            foreach($headline as $row){
                $categoryId = !empty($row['result_row']['categories'][0]['id']) ? $row['result_row']['categories'][0]['id'] : 0;
        $content_id		= $row['content_id'];
        if (isset($row['content_type']) && $row['content_type'] == 'TEXT')  {

            if(!empty($row['result_row']['title']))
            {
                $title = (strlen($row['result_row']['title']) > 45 ? substr($row['result_row']['title'], 0, 45) .' ...' : $row['result_row']['title']);   // "NEWS";
                $title = strip_tags($title);
                $title = str_replace("&#", '', $title);
                $title = str_replace("8220;", '', $title);
                $title = str_replace("8217;", '', $title);
                $title = str_replace("8216;", '', $title);
                $title = str_replace("8221;", '', $title);
                $title = str_replace("82", '', $title);
                $title = str_replace('"', '', $title);
                $_textTransform = "BERITA";
                $_color = "#9E5B3E;";
                $url = base_url() . "news/article/".$categoryId."-" . $row['result_row']['id'] . "/" . $row['result_row']['slug'];
                $image	= $row['result_row']['image_full'];
                $content_date 	= date('d M Y - H:i A', strtotime($row['result_row']['date']));
                $hover_title = $row['result_row']['title'];
            }
        }

        if (isset($row['content_type']) && $row['content_type'] == 'HOT_PROFILE')  {
            $title	= $row['result_row']['page_name'];
            $_textTransform = "PROFILE";
            $hover_title = $row['result_row']['page_name'];
            $_color = "#FF7E00;";
            $_jabatan = $row['result_row']['posisi'];
            $_partai_img = $row['result_row']['thumb_partai_url'];
            $_pr = 1;
            if(!empty($row['result_row']['scandal']))
            {
                foreach($row['result_row']['scandal'] as $rowScan)
                {
                    if($_pr > 7) break;
                    $skandal_uri = base_url() . 'scandal/index/'.$rowScan['scandal_id'].'-'.urltitle($rowScan['scandal_title']);
                    $badge_color = "#666666";
                    if ($rowScan['pengaruh'] == '1')	$badge_color = "#953B39";
                    if ($rowScan['pengaruh'] == '2')	$badge_color = "#E2C402";
                    if ($rowScan['pengaruh'] == '3')	$badge_color = "#0ACC27";

                    $listScan[] = '<a class="badge " style="float:left; background-color:'.$badge_color.';" href="'.$skandal_uri.'">
                                <small>'.(strlen($rowScan['scandal_title']) > 20 ? substr($rowScan['scandal_title'], 0, 20) . '...' : $rowScan['scandal_title']).'</small>
                            </a>';
                    $_pr++;
                }
            }
            $url 		= base_url('aktor/profile/'.$row['result_row']['page_id']);

            if(!empty($row['result_row']['headline_photo']))
            {
                $image = ($row['result_row']['headline_photo'][0]['headline_url'] != 'None' ? $row['result_row']['headline_photo'][0]['headline_url'] : base_url().'public/img/no-image-politisi.png' );
            } else {
                $image = ($row['result_row']['large_url'] != 'None' ? $row['result_row']['large_url'] : base_url().'public/img/no-image-politisi.png' );
            }

            //$content_date 	= mysql_date("%d %M %Y - %h:%i", strtotime($row['entry_date']));
            $content_date = '';
        }

//        if(!empty($_GET['debug']) && $_GET['debug'] == 1){
            if (isset($row['content_type']) && $row['content_type'] == 'SKANDAL')  {
                $title 	= (strlen($row['result_row']['title']) > 45 ? substr($row['result_row']['title'], 0, 45) .' ...' : $row['result_row']['title']);
                $hover_title = $row['result_row']['title'];
                $_textTransform = "SKANDAL";
                $_color = "#DC0000;";
                $url = base_url() . 'scandal/index/'.$row['result_row']['scandal_id'].'-'.urltitle($row['result_row']['title']);
                $idx_img = count($row['scandal_photo']) - 1;

                $image = (!empty($row['scandal_photo'][$idx_img]['large_url']) ? $row['scandal_photo'][$idx_img]['large_url'] : base_url().'public/img/no-image-politisi.png');

                foreach($row['scandal_photo'] as $rw=>$ph){

                    if(!empty($ph['headline']))
                    {
                        if(intval($ph['headline']) == 1){
                            $image = base_url().$ph['large_url'];
                            break;
                        }
                    }
                }

                //$content_date 	= mysql_date("%d %M %Y - %h:%i", strtotime($row['updated_date']));
                $content_date = '';
            }
//        }


    /*
        if (isset($row['content_type']) && $row['content_type'] == 'SUKSESI')  {

            if(!empty($row['result_row']['status'][0]['lembaga']))
            {
                $resSuksesi = $row['result_row']['status'][0]['lembaga'];
            } else {
                $resSuksesi = $row['result_row']['status'][1]['lembaga'];
            }



            foreach($resSuksesi as $key => $rwSuksesi)
            {
                if($key > (count($rwSuksesi['kandidat']) - 1)){
                    continue;
                }


                $kandidat_name	= $rwSuksesi['kandidat'][$key]['kandidat']['page_name'];

                $kandidat_link	= base_url() . 'aktor/profile/' . $rwSuksesi['kandidat'][$key]['kandidat']['page_id'];
                $kandidat_pic	= $rwSuksesi['kandidat'][$key]['kandidat']['profile_badge_url'];

                $pasangan_name	= $rwSuksesi['kandidat'][$key]['kandidat']['page_name_pasangan'];;
                $pasangan_link	= base_url() . 'aktor/profile/' . $rwSuksesi['kandidat'][$key]['kandidat']['page_id_pasangan'];
                $pasangan_pic	= $rwSuksesi['kandidat'][$key]['kandidat']['profile_badge_pasangan_url'];

                if($row['result_row']['id_race'] == '275')
                {
                    $badge_suksesi[] = '<div class="home-race-candidate" style="float:left;"><a href="'.$kandidat_link.'">
                                            <div  title="'.$kandidat_name.'"
                                             style="background: url('.$kandidat_pic.') no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi">
                                            </div>
                                            </a>
                                          </div>';
                } else {
                    $badge_suksesi[] = '<div class="home-race-candidate" style="float:left;"><a href="'.$kandidat_link.'">
                                            <div  title="'.$kandidat_name.'"
                                             style="background: url('.$kandidat_pic.') no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi">
                                            </div>
                                            </a>
                                          </div>
                                          <div class="home-race-candidate" style="float:left;">
                                          <a href="'.$pasangan_link.'">
                                            <div  title="'.$pasangan_name.'"
                                             style="background: url('.$pasangan_pic.') no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi">
                                            </div>
                                          </a></div>';
                }
            }

            $title 	= (strlen($row['result_row']['race_name']) > 45 ? substr($row['result_row']['race_name'], 0, 45) .' ...' : $row['result_row']['race_name']);
            $_textTransform = "SUKSESI";
            $hover_title = $row['result_row']['race_name'];
            $_color = "#FFE930;";
            $url 		= base_url('suksesi/index/'.$row['result_row']['id_race']. '-' . urltitle($row['result_row']['race_name']) . '/');
            if(!empty($row['result_row']['photo_headline']))
            {
                $image = (!empty($row['result_row']['photo_headline'][0]['large_url']) ? $row['result_row']['photo_headline'][0]['large_url'] : base_url().'public/img/large/noimage.jpg');
            } else {
                foreach($row['result_row']['photos'] as $rw=>$ph){
                    if(intval($ph['headline']) == 1){
                        $image = $ph['large_url'];
                        break;
                    }
                }
            }

            // $content_date = date("d M Y - H:i", strtotime($row['result_row']['head_date']));
            $content_date = '';
        }
        */

        if(!empty($account_id)){
            $content_like_uri 	= ($row['is_like'] == '0') ? base_url('ajax/like') : base_url('ajax/unlike') ;
            $content_dislike_uri = ($row['is_dislike'] == '0') ? base_url('ajax/dislike') : base_url('ajax/undislike');
        }
        $content_data	= array(
            'content_id' 		=> $content_id,
            'content_date' 	=> $content_date,
            'count_comment' 	=> (isset($row['count_comment']) ? $row['count_comment'] : '0'),
            'count_like' 		=> (isset($row['count_like']) ? $row['count_like'] : '0'),
            'count_dislike' 	=> (isset($row['count_dislike']) ? $row['count_dislike'] : '0'),
            'is_like' 			=> 0 /*$row['is_like']*/,
            'is_dislike' 		=> 0 /*$row['is_dislike']*/,
            'tipe' 				=> '2',
            'restype' 			=> '2',
            'callback'			=> $content_callback,
            'like_uri'			=> $content_like_uri,
            'dislike_uri'		=> $content_dislike_uri
        );

        ?>

        <!-- div class="item <?php if($i==0) echo " active";?> slideimg" -->
        <div class="item slideimg">
        <!-- div class="item slideimg" -->
            <!--
                        <div class="headline-subtitle-wrapper">
                            <?php  /* echo $row['politisi']; */ ?>
                        </div>
                        -->
            <!--                <a href="<?php echo $url; ?>" title="<?php echo $hover_title; ?>"> -->
            <img src="<?php echo $image;?>" alt="" style="width: 960px; height: auto;">
            <!--                </a>  -->
            <?php
            if ($row['content_type'] == 'HOT_PROFILE')
            {
                ?>
                <div class="carousel-caption" id="slideinformation">
                    <div class="informasi-judul">Posisi : </div><div class="informasi-jabatan"><?php echo $_jabatan; ?></div>
                    <?php
                    if(!empty($listScan))
                    {
                        ?>
                        <div class="informasi-scandal">
                            <div class="skandal-judul">Scandal : </div>
                            <?php
                            foreach($listScan as $listSkandal)
                            {
                                echo $listSkandal;
                            }
                            ?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            <?php
            }
            ?>

            <?php
            /*
            if ($row['content_type'] == 'SUKSESI')  {
                echo '<div class="carousel-caption" id="slidesuksesi">';
                foreach($badge_suksesi as $bgSuk)
                {
                    echo $bgSuk;
                }
                echo '</div>';
            }
            */
            ?>
            <div class="carousel-caption" id="slidecaption">
                <div class="sectioncategories" style="background-color:<?php echo $_color;?>opacity:0.8;"">
                <p><?php echo $_textTransform; ?></p>
            </div>
            <div class="sectiontitle">
                <div class="headline-date-view"><?php echo (isset($content_date)) ? $content_date : ''; ?></div>
                <div class="headline-title"><a href="<?php echo $url; ?>" title="<?php echo $hover_title; ?>"><?php echo $title; ?></a></div>
            </div>
            <?php
            if($row['content_type'] == 'TEXT')
            {
                ?>
                <div class="comunity-icon">
                    <img src="<?php echo base_url('assets/images/comunity_icon_head.png'); ?>">
                </div>
                <div class="comunity-total">
                    <p style="font-family: helvetica;font-size: 15px;line-height: 30px;margin-left: 10px;margin-top: 10px;"><?php echo $content_data['count_comment'];?></p>
                    <p style="font-family: helvetica;font-size: 15px;line-height: 30px;margin-left: 10px;"><?php echo $content_data['count_like'];?></p>
                    <p style="font-family: helvetica;font-size: 15px;line-height: 30px;margin-left: 10px;"><?php echo $content_data['count_dislike'];?></p>
                </div>
            <?php
            }

            ?>
        </div>
        </div>
    <?php
    $i++;

            }

    ?>
    </div>
    <!--
        <div style="float:right;">
            <a class="left carousel-control" href="#slideheadline" data-slide="prev" style="font-size:35px;position: relative;float: left;width: 20px;height: 20px;">‹</a>
            <a class="right carousel-control" href="#slideheadline" data-slide="next" style="margin-left:35px;font-size:35px;position: relative;float: right;width: 20px;height: 20px;">›</a>
        </div>
    -->

    <ol class="carousel-indicators">
        <li data-target="#slideheadline" data-slide-to="0" class="active"></li>
        <li data-target="#slideheadline" data-slide-to="1"></li>
        <li data-target="#slideheadline" data-slide-to="2"></li>
        <li data-target="#slideheadline" data-slide-to="3"></li>
        <li data-target="#slideheadline" data-slide-to="4"></li>
        <li data-target="#slideheadline" data-slide-to="5"></li>
        <li data-target="#slideheadline" data-slide-to="6"></li>
        <?php //if(!empty($_GET['debug']) && $_GET['debug'] == 1){?>
        <li data-target="#slideheadline" data-slide-to="7"></li>
        <?php //};?>
        <!-- li data-target="#slideheadline" data-slide-to="8"></li -->
    </ol>

    </div>

<?php endif;?>
