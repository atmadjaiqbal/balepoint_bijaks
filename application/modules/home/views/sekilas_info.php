<div style="background-color: #F8F8F8;border-radius: 10px;padding-left: 10px;padding-top:10px; padding-right:10px;box-shadow:2px 2px 2px #878787;border:1px solid #aaaaaa">
    <div style="background-color: #880F04;margin-top:-15px;border-radius: 10px 10px 0 0;margin-left: -11px;margin-top: -20px;width: 307px">
        <h4 style="font-size: 14.5px; color: #ffffff;padding:5px;font-weight: bold;">&nbsp;S&nbsp;E&nbsp;K&nbsp;I&nbsp;L&nbsp;A&nbsp;S</h4>
    </div>
    <div style="width: 290px;">
        <h4 style="font-size: 11px;font-weight: bold;line-height: 12px;color: #880F04;">GAGASAN MEMILIH KEPALA DAERAH TIDAK LANGSUNG OLEH RAKYAT, TAPI OLEH DPRD</h4>
        <table border="0" style="margin-top:-5px;">
            <tr style="line-height: 12px;">
                <td style="font-size: 11px;font-weight: bold;">PENGAGAS:</td><td style="font-size: 10px;">GERINDRA, PKS, GOLKAR, PPP, PAN, Demokrat (<span style="font-size: 9px;">berubah haluan 19/Sep/2014</span>)</td>
            </tr>
            <tr style="line-height: 12px;">
                <td style="font-size: 11px;font-weight: bold">ALASANNYA:</td><td style="font-size: 10px;">(1) akan hemat biaya, (2) organisasi akan lebih baik, (3) akan kurangi konflik</td>
            </tr>
            <tr><td colspan="2" style="height: 10px;"></td></tr>
            <tr style="line-height: 12px;">
                <td style="font-size: 11px;font-weight: bold">SAAT INI:</td><td style="font-size: 10px;">(1) Rakyat memilih langsung, (2) Kedaulatan ada di rakyat, (3) Pemimpin yg bersih & berprestasi, seperti Jokowi, Ahok, Rismawati, Ridwan Kamil, Ganjar Pranowo bisa terpilih, (4) Politik uang PILKADA di DPRD di hilangkan</td>
            </tr>
            <tr><td colspan="2" style="height: 10px;"></td></tr>
            <tr style="line-height: 12px;">
                <td style="font-size: 11px;" colspan="2"><b>ANALISA</b> kenapa gagasan ini di cetuskan:</td>
            </tr>
            <tr style="line-height: 12px;">
                <td style="font-size: 11px;" colspan="2">Kecenderungan kader Demokrat, PKS, Gerindra, dkk untuk tidak dapat bersaing di mata rakyat</td>
            </tr>
            <tr style="line-height: 12px;">
                <td style="font-size: 11px;" colspan="2">
                    <ul style="list-style: disc;margin-top:0px;margin-left:10px;">
                        <li style="font-size: 10px;line-height: 12px;">Demokrat, PKS, PPP citranya terpuruk karena kasus korupsi</li>
                        <li style="font-size: 10px;line-height: 12px;">Minim nya koalisi merah putih (Gerindra, dkk) akan tokoh-tokoh yg berprestasi di mata rakyat</li>
                        <br/>
                        <li style="font-size: 10px;line-height: 12px;">Demokrat berubah haluan pro pilkada langsung, demi pencitraan ketua umumnya.</li>
                    </ul>
                </td>
            </tr>

        </table>
    </div>
</div>