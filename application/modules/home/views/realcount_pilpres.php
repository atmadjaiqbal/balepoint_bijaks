<div class="row-fluid">
  <div class="span12">
     <table summary='' class='home-race-pilpres table' >
        <tr>
           <td></td>
           <td class="well well-small" class='pilpres-race-kandidat' style="text-align: center !important;width: 126px;">
              <img class="real-image" src="http://www.bijaks.net/assets/images/pilpres-2014/prabowo-hatta.png">
           </td>
           <td class="well well-small" class='pilpres-race-kandidat' style="text-align: center !important;width: 126px;">
              <img class="real-image" src="http://www.bijaks.net/assets/images/pilpres-2014/jokowi-jk.png">
           </td>
           <td style="vertical-align: bottom !important;"><span>INFORMASI</td>
        </tr>

        <tr>
           <td class="pilpres-race-title" style="width: 240px;">
              <strong title="Pol-Tracking Institute"><a style="cursor: default;font-size: 14px;color:#4A3ED6;">KPU</a></strong>
           </td>
           <td style="text-align:center;width: 126px;" class='pilpres-race-percentage well well-small'>
              <div class='pilpres-percentage-box'>
                 <div class='pilpres-percentage-bar' style='height:46.85px;background-color:#EC282A;'></div><div class="pilpres-bar-2">46.85 %<div>
              </div>
           </td>
           <td style="text-align:center;width: 126px;" class='pilpres-race-percentage well well-small'>
              <div class='pilpres-percentage-box'>
                 <div class='pilpres-percentage-bar ' style='height:53.15px;background-color:#4A3ED6;'></div><div class="pilpres-bar-2">53.15 %<div>
              </div>
           </td>
           <td style="vertical-align: bottom;">
               <p class="informasi-suara"><!-- Data masuk: 75.05% <br/ -->Last update: 22-07-2014 19:21:33<br/>Data yang di hasilkan dari rekapitulasi lembaga KPU</p>
           </td>
        </tr>

         <tr>
             <td class="pilpres-race-title" style="width: 240px;">
                 <strong title="Pol-Tracking Institute"><a style="cursor: default;font-size: 14px;color:#4A3ED6;">KAWAL PEMILU</a></strong>
             </td>
             <td style="text-align:center;width: 126px;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-box'>
                     <div class='pilpres-percentage-bar' style='height:46.84px;background-color:#EC282A;'></div><div class="pilpres-bar-2">46.84 %<div>
                         </div>
             </td>
             <td style="text-align:center;width: 126px;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-box'>
                     <div class='pilpres-percentage-bar ' style='height:53.15px;background-color:#4A3ED6;;'></div><div class="pilpres-bar-2">53.15 %<div>
                         </div>
             </td>
             <td style="vertical-align: bottom;">
                 <p class="informasi-suara">Data masuk: 98.02%<br/>Last update: 22-07-2014 14:49:00</p>
             </td>
         </tr>

         <tr>
             <td class="pilpres-race-title" style="width: 240px;">
                 <strong title="Pol-Tracking Institute"><a style="cursor: default;font-size: 14px;color:#4A3ED6;">TIM SUKSES No 1<br>PRABOWO-HATTA</a></strong>
             </td>
             <td style="text-align:center;width: 126px;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-box'>
                     <div class='pilpres-percentage-bar' style='height:53.52px;background-color:#4A3ED6;'></div><div class="pilpres-bar-2">53.52 %<div>
                         </div>
             </td>
             <td style="text-align:center;width: 126px;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-box'>
                     <div class='pilpres-percentage-bar ' style='height:46.48px;background-color:#EC282A;'></div><div class="pilpres-bar-2">46.48 %<div>
                         </div>
             </td>
             <td style="vertical-align: bottom;">
                 <p class="informasi-suara">Data masuk: 99.97%<br/>Last update: 15-07-2014</p>
             </td>
         </tr>

         <tr>
             <td class="pilpres-race-title" style="width: 240px;">
                 <strong title="Pol-Tracking Institute"><a style="cursor: default;font-size: 14px;color:#4A3ED6;">TIM SUKSES No 2<br>JOKOWI-JK</a></strong>
             </td>
             <td style="text-align:center;width: 126px;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-box'>
                     <div class='pilpres-percentage-bar' style='height:46.54px;background-color:#EC282A;'></div><div class="pilpres-bar-2">46.54 %<div>
                         </div>
             </td>
             <td style="text-align:center;width: 126px;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-box'>
                     <div class='pilpres-percentage-bar ' style='height:53.46px;background-color:#4A3ED6;;'></div><div class="pilpres-bar-2">53.46 %<div>
                         </div>
             </td>
             <td style="vertical-align: bottom;">
                 <p class="informasi-suara">Data masuk: 90%<br/>Last update: 15-07-2014 20:39:00</p>
             </td>
         </tr>


     </table>
  </div>
</div>
