<?php
$_name_acc = ''; $i = 0;
if (!empty($comments))
{
    $total_comment = count($comments);

	foreach($comments as $row) {
		$date = date('d M Y', strtotime(str_replace('-', '/', $row['entry_date'])));
		if ($row['account_type'] === '1') {
			$url = base_url() . "politisi/index/" . $row['page_id'] ;
			$prefix_img = "politisi/";
		} else {
			$url = base_url() . "komunitas/profile/".$row['page_id'];
			$prefix_img = "user/";
		}

        //if($row['page_name'] <> $_name_acc)
        //{
            $_name_acc = $row['page_name'];
            if($i > 19) break;

            $comment_on ='';
            if ($row['content_group_name'] == 'STATUS') {
                $comment_on_url = $url;
                $comment_on = "Status ". $row['page_name'];

            } else if ($row['content_group_name'] == 'BLOG') {
                $comment_on_url 	= base_url() . "komunitas/profile/".$row['page_id'];
                $comment_on 		= "Blog : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";

            } else if ($row['content_group_name'] == 'NEWS') {
                $comment_on_url 	= base_url().'news/article/0-'.$row['news_id'].'/'.$row['content_url'];
                $comment_on 		= "Berita : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";

            } else if ($row['content_group_name'] == 'SCANDAL') {
                $comment_on_url 	= base_url().'scandal/index/'.$row['location'].'/'.  urltitle($row['title']) ;
                $comment_on 		= "Skandal : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";

            } else if ($row['content_group_name'] == 'RACE') {
                $comment_on_url 	= base_url().'suksesi/index/'.$row['location'].'/'.  urltitle($row['title']) ;
                $comment_on 		= "Suksesi : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";

            } else if ($row['content_group_name'] == 'SURVEY') {
                $comment_on_url 	= base_url().'survey/index/'.$row['location'].'/'.  urltitle($row['title']) ;
                $comment_on 		= "Suksesi : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";
            }

            if($i == 0) $_backcolor = 'style="background-color:#E6C05A;"';
            if($i == 2) $_backcolor = 'style="background-color:#F7D788;"';
            if($i == 4) $_backcolor = 'style="background-color:#FCE9BA;"';
            if($i >= 5 || $i == 1 || $i == 3) $_backcolor = '';

            $_tplcomment = '
            <div class="blog-list" '.$_backcolor.'>
              <div class="blog-list-foto">
                <a href="'.$url.'">
                  <img alt="'.$row['page_name'].'" title="'.$row['page_name'].'"
                     src="'.badge_url($row['attachment_title'],"user/".$row['user_id']).'"
                     data-src="'.badge_url($row['attachment_title'],"user/".$row['user_id']).'"
                     data-toggle="tooltip" class="tooltip-bottom"/>
                </a>
              </div>
              <div class="blog-list-name">
                 <span class="pull-right comment-date" style="margin-top:-5px;">'.time_passed($row['entry_date']).'</span>
                 <a href="'.$url.'" title="'.$row['page_name'].'">'.substr(strtoupper($row['page_name']),0, 18).(strlen($row['page_name']) > 19 ? '...' : '').'</a>
                 <p><a href="'.$url.'" title="'.$row['text_comment'].'">
                    <strong>'.substr($row['text_comment'],0, 30).(strlen($row['text_comment']) > 29 ? '...' : '').'</strong>
                    </a>
                 </p>
                 <div class="comment-on">di '.$comment_on.'</div>
              </div>
            </div>';

?>

<?php
if($i % 2 == 0){ $_margin = 'margin-right:3px;margin-top:0px';} else { $_margin = ''; }
?>
<div style="float:left;<?php echo $_margin; ?>">
    <div class="blog-section">
       <?php echo $_tplcomment; ?>

    </div>
</div>
<?php
        $i++;
      //  }
    }
}
?>


