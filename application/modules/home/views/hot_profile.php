<div>
    <div  class="home-title-section hp-label hp-label-hot-profile">
        <span class="hot">HOT PROFILE</span>
    </div>

    <div class='col-4' id='home-hot-profile-menu'>
        <ul>
<?php
            foreach ($hot_profile as $row)
            {
                $_profile_url = base_url().'aktor/profile/'.$row['page_id'];
?>
                <li>
                 <span data-id="<?php echo $row['page_id']; ?>">

                   <img src='<?php echo $row['badge_url']; ?>' data-src='<?php echo $row['badge_url']; ?>'
                        title="<?php echo $row['page_name']; ?>" alt=""/>
                 </span>
                </li>
<?php
            }
?>
        </ul>
    </div>

    <div class='col-5' id='home-hot-profile-details'>
<?php
        foreach ($hot_profile as $row)
        {
            $_profile_url = base_url().'aktor/profile/'.$row['page_id'];
            $content_id = $row['profile_content_id'];
?>
            <div class='home-hot-profile-detail'>
                <div class='clearfix'>
                    <div class='home-hot-profile-detail-pic'>
                        <img src='<?php echo $row['badge_url']; ?>' data-src='<?php echo $row['badge_url']; ?>' alt=''/>
                    </div>
                    <div class='home-hot-profile-detail-text'>
                        <a href="<?php echo $_profile_url; ?>">
                            <h4><?php echo (strlen($row['page_name'])> 25 ? substr($row['page_name'], 0 ,25) . '...' : $row['page_name']); ?></h4>
                        </a>
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Asosiasi</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <span class=""><?php echo $row['partai_name'];?></span>
                            </div>
                        </div>
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Posisi</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <span class=""><?php echo substr($row['posisi'],0,30) .'...';?></span>
                            </div>
                        </div>
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Prestasi</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <?php
                                $prestasi = character_limiter($row['prestasi'], 27);                           ?>
                                <span class=""><?php echo $prestasi;?></span>
                            </div>
                        </div>
                        <hr class="hr-black">
                    </div>

                </div>


                <div style="height: 175px;">
                  <div class="row-fluid">
                    <div class="span2 span2-pd-left">
                        <span><strong>Skandal</strong></span>
                    </div>
                    <div class="span10 span10-pd-right">
                        <?php $p = 0; ?>
                        <?php foreach ($row['scandal'] as $item){ ?>
                            <?php if($p <6){ ?>
                                <?php
                                $skandal_uri = base_url() . 'scandal/index/'.$item['scandal_id'].'-'.urltitle($item['scandal_title']);
                                $badge_color = "#666666";
                                if ($item['pengaruh'] == '1')	$badge_color = "#953B39";
                                if ($item['pengaruh'] == '2')	$badge_color = "#E2C402";
                                if ($item['pengaruh'] == '3')	$badge_color = "#0ACC27";
                                ?>
                                <a class="badge " style="background-color:<?php echo $badge_color;?>;" href="<?php echo $skandal_uri; ?>">
                                    <small><?php
                                        if(strlen($item['scandal_title']) > 8 )
                                        {
                                            $skandaljudul = substr($item['scandal_title'], 0, 10);
                                        } else {
                                            $skandaljudul = $item['scandal_title'];
                                        }
                                        echo $skandaljudul;
                                        ?></small>
                                </a>
                            <?php } ?>
                            <?php $p++; ?>
                        <?php }?>

                    </div>
                  </div>
                  <p style="margin-left:5px;margin-top: 5px;margin-right: 5px;">
                      <?php echo (strlen($row['about']) > 375 ? substr($row['about'], 0, 380) . ' ... ' : $row['about']); ?><a href="<?php echo $_profile_url; ?>">lihat selengkapnya</a>
                  </p>
                </div>

            </div>
<?php
        }
?>

    </div>

    <div class='col-4' id='home-hot-lembaga-menu'>
        <ul>
            <?php
            foreach ($hot_lembaga as $rwLem)
            {
                $_lembaga_url = base_url().'aktor/profile/'.$rwLem['page_id'];
                ?>
                <li>
                 <span data-id="<?php echo $rwLem['page_id']; ?>">
                   <img src='<?php echo $rwLem['badge_url']; ?>' data-src='<?php echo $rwLem['badge_url']; ?>'
                        title="<?php echo $rwLem['page_name']; ?>" alt=""/>
                 </span>
                </li>
            <?php
            }
            ?>
        </ul>
    </div>

    <div class='col-5' id='home-hot-lembaga-details'>
        <?php
        foreach ($hot_lembaga as $rwLem)
        {
            $_lembaga_url = base_url().'aktor/profile/'.$rwLem['page_id'];
            $content_id = $rwLem['profile_content_id'];
            ?>
            <div class='home-hot-lembaga-detail'>
                <div class='clearfix'>
                    <div class='home-hot-lembaga-detail-pic'>
                        <img src='<?php echo $rwLem['badge_url']; ?>' data-src='<?php echo $rwLem['badge_url']; ?>' alt=''/>
                    </div>
                    <div class='home-hot-lembaga-detail-text'>
                        <div style="height: 40px;">
                          <a href="<?php echo $_lembaga_url; ?>">
                            <h4><?php echo $rwLem['page_name']; ?></h4>
                          </a>
                        </div>
                        <hr class="hr-black">
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Posisi</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <span class=""><?php echo $rwLem['posisi'];?></span>
                            </div>
                        </div>
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Skandal</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <?php $p = 0; ?>
                                <?php if(isset($rwLem['scandal'])) { ?>
                                    <?php foreach ($rwLem['scandal'] as $item){ ?>
                                        <?php if($p < 5){ ?>
                                            <?php
                                            $skandal_uri = base_url() . 'scandal/index/'.$item['scandal_id'].'-'.urltitle($item['scandal_title']);
                                            $badge_color = "#666666";
                                            if ($item['pengaruh'] == '1')	$badge_color = "#953B39";
                                            if ($item['pengaruh'] == '2')	$badge_color = "#E2C402";
                                            if ($item['pengaruh'] == '3')	$badge_color = "#0ACC27";
                                            ?>
                                            <a class="badge " style="background-color:<?php echo $badge_color;?>;" href="<?php echo $skandal_uri; ?>">
                                                <small><?php
                                                    if(strlen($item['scandal_title']) > 8 )
                                                    {
                                                        $skandaljudul = substr($item['scandal_title'], 0, 10);
                                                    } else {
                                                        $skandaljudul = $item['scandal_title'];
                                                    }
                                                    echo $skandaljudul;
                                                    ?></small>
                                            </a>
                                        <?php } ?>
                                        <?php $p++; ?>
                                    <?php }?>
                                <?php } ?>

                            </div>
                        </div>
                        <hr class="hr-black">
                    </div>

                </div>

                <div style="height: 77px;">
                    <p style="margin-left:5px;margin-top: 5px;margin-right: 5px;">
                        <?php echo (strlen($rwLem['about']) > 147 ? substr($rwLem['about'], 0, 150) . ' ... ' : $rwLem['about']); ?><a href="<?php echo $_lembaga_url; ?>">lihat selengkapnya</a>
                    </p>
                </div>

            </div>
        <?php
        }
        ?>

    </div>

</div>