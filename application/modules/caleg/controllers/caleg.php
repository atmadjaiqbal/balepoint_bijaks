<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Caleg extends Application
{
    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->helper('m_date');

        $this->load->helper('text');
        $this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');

        $this->load->library('aktor/aktor_lib');
        $this->load->library('caleg_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->library('suksesi/suksesi_lib');
    }

    public function index()
    {
        $data['title'] = "Bijaks | Caleg 2014";
        $data['category'] = "Caleg 2014";

        $province_list = $this->caleg_lib->get_province();
        
        $result = array();
        foreach ($province_list->result_array() as $key => $value) {
            $where = array("province_id" => $value['province_id']);
            $select = "count(*) cou";
            $clg = $this->caleg_lib->get_structure_caleg($select, $where)->row_array();
            $value['total_caleg'] = $clg['cou'];

            // $pro_issue = $this->caleg_lib->get_issue('title', $where, $limit=0, $offset=0);
            // $issu = array_values($pro_issue->result_array());
            // $value['issue'] = array();
            // foreach ($issu as $key => $value) { 
            //     $value['issue'] = $value['title'];
            // }
             

            $result[$key] = $value;
        }

        $data['province'] = $result;


        $html['html']['content']  = $this->load->view('index_view', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = '';
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);

    }

    public function detail($province='', $province_id='0', $dapil='', $dapil_id='0')
    {
        $province = urldecode($province);
        $dapil = urldecode($dapil);
        $where = array('province_id' => intval($province_id), 'dapil_id' => intval($dapil_id));
        $dapil_area = $this->caleg_lib->get_master_dapil($select='*', $where);

        $data['area'] = '';
        if($dapil_area->num_rows() > 0){
            $dapil_area_row = $dapil_area->row_array(0);
            $data['area'] = $dapil_area_row['area'];
        }

        $where = array('province_id' => intval($province_id));
        $pro_issue = $this->caleg_lib->get_issue($select='*', $where, $limit=0, $offset=0);
        $data['issue'] = $pro_issue->result_array();

        $dt = $this->caleg_lib->get_partai_byid($province_id);
        $wheres=array('province_id' => $province_id);
        $pro = $this->caleg_lib->get_province($select='*', $wheres, 1, 0);
        $data['kursi'] = '0';
        if($pro->num_rows() > 0){
            $data_kursi = $pro->row_array();
            $data['kursi'] = $data_kursi['total_kursi'];
        }

        $result = array();
        foreach($dt->result_array() as $row=>$val){
            $dt = $this->caleg_lib->get_calegs($province, $dapil, $val['partai_id']);
            $val['kandidat'] = $dt->num_rows();
            $val['img_url'] = badge_url($val['attachment_title'], 'politisi/'.$val['partai_id']);
            $result[$row] = $val;
        }
        $data['result'] = $result;
        $data['province'] = urldecode($province);
        $data['province_id'] = $province_id;
        $data['dapil'] = urldecode($dapil);

        $this->load->view('detail', $data);

    }

    public function get_dapil()
    {
        $province_id = $this->input->post('province_id', true);
        $province = $this->input->post('province', true);

        $where = array('province_id' => intval($province_id));
        $dt = $this->caleg_lib->get_master_dapil('*', $where);

//        $data['rcode'] = 'ok';
//        if($dt->num_rows() == 0){
//            $data['rcode'] = 'bad';
//        }
        $data['province'] = $province;
        $data['province_id'] = $province_id;
        $data['result'] = $dt->result_array();
        
        header("Access-Control-Allow-Origin: *");
        
        $this->load->view('dapil_list', $data);

//        $this->output->set_content_type('application/json');
//        $this->output->set_output(json_encode($data));

    }

    public function get_partai()
    {
        $province_id = $this->input->post('province_id', true);
        $province = $this->input->post('province', true);
        $dapil = $this->input->post('dapil', true);
        $dapil_id = $this->input->post('dapil_id', true);

        $dt = $this->caleg_lib->get_partai_byid();

        $data['rcode'] = 'ok';
        if($dt->num_rows() == 0){
            $data['rcode'] = 'bad';
        }
        $result = array();
        foreach($dt->result_array() as $row=>$val){
            
            $val['img_url'] = thumb_url($val['attachment_title'], 'politisi/'.$val['page_id']);
            
            $where = array('page_id' => $val['page_id']);
            $skandal_terkait = $this->caleg_lib->get_scandal_player($select='count(*) as cou', $where)->row_array(0);
            $val['scandal_count'] = $skandal_terkait['cou'];

            $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
            $val['news_count'] = $news_terkait['cou'];

            $foll = $this->aktor_lib->get_follower($val['page_id']);
            $val['follower_count'] = $foll->num_rows();

            $clg_co = $this->caleg_lib->get_caleg_count($province_id, $dapil_id, $val['page_id'])->row_array(0);
            $val['caleg_count'] = $clg_co['co'];

            $result[$row] = $val;

        }
        $data['result'] = $result;

        // $this->output->set_content_type('application/json');
        // $this->output->set_output(json_encode($data));

        $data['province'] = $province;
        $data['province_id'] = $province_id;
        $data['dapil'] = $dapil;
        $data['dapil_id'] = $dapil_id;
        
        
        header("Access-Control-Allow-Origin: *");
        
        $this->load->view('partai_list', $data);

    }

    public function get_caleg()
    {
        $province = $this->input->post('province_id', true);
        $partai_id = $this->input->post('partai_id', true);
        $dapil = $this->input->post('dapil_id', true);

        $caleg_redis = $this->redis_slave->get('caleg:'.$province.':'.$dapil.':'.$partai_id);
        $caleg = @json_decode($caleg_redis, true);

        $result = array();
        foreach($caleg as $row=>$val){
            $val['caleg_img_url'] = badge_url($val['attachment_title'], 'politisi/'.$val['caleg_id']);
            // $val['partai_img_url'] = badge_url($val['partai_attachment_title'], 'politisi/'.$val['partai_id']);

            /** get profile */
            $profile = $this->redis_slave->get('profile:detail:'.$val['caleg_id']);
            $profiles = @json_decode($profile, true);
            $val['profile'] = $profiles;

            // $where = array('page_id' => $val['caleg_id']);
            // $skandal_terkait = $this->caleg_lib->get_scandal_player($select='count(*) as cou', $where)->row_array(0);
            // $val['scandal_count'] = $skandal_terkait['cou'];

            // $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
            // $val['news_count'] = $news_terkait['cou'];

            $foll = $this->aktor_lib->get_follower($val['caleg_id']);
            $val['follower_count'] = $foll->num_rows();

            // $object_page = $this->caleg_lib->get_object_page('tp.prestasi, ta.birthday', $where);
            // $val['prestasi'] = '';
            // $val['birthday'] = '';
            // if($object_page->num_rows() > 0){
            //     $prestasi = $object_page->row_array(0);
            //     $val['prestasi'] = $prestasi['prestasi'];
            //     $val['birthday'] = ($prestasi['birthday'] == '0000-00-00') ? '' : $prestasi['birthday'];
            // }

            $result[$row] = $val;
        }
        $data['result'] = $result;

        header("Access-Control-Allow-Origin: *");
        // $this->load->view('caleg_index', $data);
        $this->load->view('caleg_list', $data);

    }
}