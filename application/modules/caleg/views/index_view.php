<div class="container">
	<?php $this->load->view('template/tpl_sub_header'); ?>		
</div>
<br>
<div class="container">
		<div class="sub-header-container">
			<div class="row-fluid">
				<div class="span12 well well-small">
					<div class="accordion" id="accordion2">

		<?php foreach ($province as $key => $value) { ?>
			<div class="accordion-group">
			    <div class="accordion-heading">
			    	<h4 class="media-heading-nomargin"><a class="accordion-toggle" data-provinceid="<?php echo $value['province_id'];?>" data-province="<?php echo $value['province'];?>" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $value['province_id'];?>">
			        <?php echo $value['province'];?> <span>( <?php echo $value['total_kursi'];?> Kursi - <?php echo $value['total_caleg'];?> Caleg )</span>
			      	<i class="icon-chevron-right pull-right"></i></a></h4>

			    </div>
			    <div id="collapse<?php echo $value['province_id'];?>" class="accordion-body collapse">
			      <div class="accordion-inner">
			        	<div class="loader"></div>
			        	<div class="issue-place">
			        		<p>
			        			<?php //var_dump($value['issue']); // $issue = implode($value['issue'], ', ');
			        			// echo $issue; ?>
			        		</p>
			        	</div>
			        	<div class="dapil-place"></div>
			      </div>
			    </div>
			  </div>
		<?php } ?>
  
				</div>
			</div>
		</div>
	</div>
</div>	

<script type="text/javascript">
$(document).ready(function(){
	$('#accordion2').on('show', function (ev) {
		$(ev.target).prev().children().children().children('i').removeClass('icon-chevron-right').addClass('icon-chevron-down');
  		
	})

	$('#accordion2').on('shown', function (ev) {
		
		var dapil_place = $(ev.target).children().children('.dapil-place');
		var this_collapse = $(ev.target).prev().children().children();
		var province = this_collapse.data('province');
		var province_id = this_collapse.data('provinceid');
		if(!dapil_place.html().trim()){
			$(ev.target).children().children('.loader').css('visibility', 'visible');
			$.post('<?php echo base_url('caleg/get_dapil');?>', {'province':province, 'province_id':province_id}, function(dt){
				dapil_place.html(dt);	
				$(ev.target).children().children('.loader').css('visibility', 'hidden');
			})
			
		}
		
	})

	$('#accordion2').on('hide', function (ev) {
		$(ev.target).children().children('.loader').css('visibility', 'hidden');
		$(ev.target).prev().children().children().children('i').removeClass('icon-chevron-down').addClass('icon-chevron-right');
	})
	//clglst
	$('.dapil-place').on('click', '.dpl', function(ev){
		ev.preventDefault();
		var this_dapil = $(this);
		var dapil_id = this_dapil.data('id');
		var dapil = this_dapil.data('dapil');
		var province = this_dapil.data('province');
		var province_id = this_dapil.data('provid');

		var caleg_place = $('#caleg_place_' + dapil_id);
		if(!caleg_place.html().trim()){
			$('#loader_'+dapil_id).css('visibility', 'visible');
			$.post('<?php echo base_url('caleg/get_partai');?>', {'province':province, 'province_id':province_id, 'dapil_id':dapil_id, 'dapil':dapil}, function(dt){
				$('#caleg_place_' + dapil_id).html(dt);	
				$('#loader_'+dapil_id).css('visibility', 'hidden');
			})
		}

	})

	$('.dapil-place').on('click', '.clglst', function(ev){
		ev.preventDefault();
		var this_dapil = $(this);
		var dapil_id = this_dapil.data('id');
		var dapil = this_dapil.data('dapil');
		var province = this_dapil.data('province');
		var province_id = this_dapil.data('provid');
		var partai = this_dapil.data('partaiid');

		var caleg_place = $('#caleg_list_' + province_id + '_'+ dapil_id + '_' + partai);
		if(!caleg_place.html().trim()){
			$('#loader_' + province_id + '_'+ dapil_id + '_' + partai).css('visibility', 'visible');
			$.post('<?php echo base_url('caleg/get_caleg');?>', {'partai_id':partai, 'province_id':province_id, 'dapil_id':dapil_id}, function(dt){
				caleg_place.html(dt);	
				$('#loader_' + province_id + '_'+ dapil_id + '_' + partai).css('visibility', 'hidden');
			})
		}

	})

})
</script>