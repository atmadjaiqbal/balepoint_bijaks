<?php foreach ($result as $key => $value) { ?>

	<?php if($key % 3 == 0){ ?>
		<div class="div-line div-line-small"></div>
		<p>
		<div class="row-fluid">

	<?php } ?>
			<div class="span4">
				<img class="media-object img-polaroid" src="<?php echo $value['caleg_img_url'];?>">	
				
				<h4 class=""><a target="__blank" href="<?php echo base_url('aktor/profile/').'/'. $value['caleg_id'];?>"><?php echo $value['caleg'];?></a></h4>
				<h5 class="media-heading pull-left"><a target="__blank" href="<?php echo base_url('aktor/scandals').'/'. $value['caleg_id'];?>"><?php echo ($value['scandal_count'] == 0) ? '<small>'.$value['scandal_count'].' Scandal</small>' : '<strong>'.$value['scandal_count'].' Scandal</strong>';?></a></h5> <h4 class="media-heading pull-left">&nbsp;|&nbsp;</h4>
				<h5 class="media-heading pull-left"><a target="__blank" href="<?php echo base_url('aktor/news').'/'. $value['caleg_id'];?>"><?php echo ($value['news_count'] == 0) ? '<small>'.$value['news_count'].' Berita</small>' : '<strong>'.$value['news_count'].' Berita</strong>';?></a></h5> <h4 class="media-heading pull-left">&nbsp;|&nbsp;</h4>
				<h5 class="media-heading pull-left"><a target="__blank" href="<?php echo base_url('aktor/follow').'/'. $value['caleg_id'];?>"><?php echo ($value['follower_count'] == 0) ? '<small>'.$value['follower_count'].' Follower</small>' : '<strong>'.$value['follower_count'].' Follower</strong>';?></a></h5>
				<p class="clearfix"></p>
			
				
			</div>

	<?php if($key % 3 == 2 || $key == count($result) - 1){  ?>
		</div>
		
	<?php } ?>
<?php } ?>