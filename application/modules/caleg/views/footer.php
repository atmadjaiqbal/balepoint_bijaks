<!-- footer -->
<div id="footer">
    <div class="container text-right">
        <p>Copyright &copy; 2014, Bijaks.net. All Right Reserved.</p>
    </div>
</div>

    <!-- popover -->
<div data-id="" id="pop_over" style="z-index: 9990; absolute; top:0px; left: 0px" class="popover top">
    <div class="arrow"></div>
    <h3 class="popover-title">Popover top</h3>
    <div class="popover-content">
        <img src="<?=base_url('assets/images/loading.gif');?>" class="hide" id="loader_dapil">
    </div>
</div>

</body>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

    <script>
        $(document).ready(function(){
            $("#map_idn").mousemove(function(e){
//                console.debug(e.pageX + ", "+e.pageY);
//                $('#imgFollow').offset({left:e.pageX,top:e.pageY+20});
            });
            $('#map_idn').on("click", ".province", function(e){
                e.preventDefault();

                console.debug($(this));

                e = e || window.event;
                var ids = $(this).data('id');
                var title = $(this).data('title');
                var kursi = ' ( ' + $(this).data('kursi') + ' Kursi )';

                var content = '';

                var le = e.clientX;
                var tps = e.clientY;
                var parentOffset = $(this).parent().offset();

                var exis_id = $('.popover-title').data('id');
                if(exis_id != ids){
                    $('#dapil-list').remove();
                    $('.popover-title').data('id', '');
                    $('.popover-title').text('');
                    $('#pop_over').css('top', 0);
                    $('#pop_over').css('left', 0);
                    $('#pop_over').hide();
                }

                if($('#pop_over').is(':visible')){
                    $('#dapil-list').remove();
                    $('.popover-title').text('');
                    $('#pop_over').css('top', 0);
                    $('#pop_over').css('left', 0);
                    $('#pop_over').hide();
                }else{

                    $('.popover-title').text(title + kursi);
                    $('.popover-title').data('id', ids);

                    var pop_height = $('#pop_over').height();
                    var pop_width = ($('#pop_over').width() / 2);

//                    var x = (e.clientX - parentOffset.left)  + 95;
//                    var y = (e.clientY - parentOffset.top) + 40 - (pop_height - 35);

                    var x = e.clientX - pop_width; // - parentOffset.left;
                    var y = e.clientY - pop_height; // - parentOffset.top;

                    $('#pop_over').css('top', y);
                    $('#pop_over').css('left', x);
                    $('#pop_over').show();

                    $('#loader_dapil').show();

                    $.post('<?=base_url('caleg/get_dapil');?>', {province_id:ids, province:title}, function(dt){
                        $('.popover-content').append(dt);
                        $('#loader_dapil').hide();
                        pop_height = $('#pop_over').height();
                        y = e.clientY - pop_height;
                        $('#pop_over').css('top', y);
//                        if(dt.rcode == 'bad'){
//
//                        }else{
//                            var lst_dpl = '';
//                            lst_dpl = '<ul id="dapil-list" class="nav nav-list">';
//                            $(dt.result).each(function(a, b){
//                                lst_dpl += '<li><a class="dpl" data-provid="'+ids+'" data-dapil="'+ b.dapil+'" data-id="'+ b.dapil_id +'" data-province="'+title+'" href="#">'+ b.dapil+'</a></li>';
//                            })
//                            lst_dpl += '</ul>';
//                            $('.popover-content').append(lst_dpl);
//                            $('#loader_dapil').hide();
//
//                            pop_height = $('#pop_over').height();
//                            y = e.clientY - pop_height;
//                            $('#pop_over').css('top', y);
//                        }
                    })

                }

            });

            $('#pop_over').on('click', '.dpl', function(e){
                e.preventDefault();
                pop_hide();
                $('#caleg_loader').show();
                var prov = $(this).data('province');
                var prov_id = $(this).data('provid');
                var dapil = $(this).data('dapil');
                var dapil_id = $(this).data('id');
                $('.caleg-header-title').text(prov +', '+ dapil);

                $('.container-overlay').show('blind');
                var uri = '<?php base_url();?>caleg/detail/'+prov+'/'+prov_id+'/'+dapil+'/'+dapil_id;
                $('#graph_pie').attr('src', uri);
               // var ifra = '<iframe id="graph_pie" width="100%" scrolling="no" frameborder="no" height="100%" name="graph_pie" src="<?php //base_url();?>caleg/detail/'+prov+'/'+prov_id+'/'+dapil+'/'+dapil_id+'"></iframe> '
//                $('.container-overlay').html(ifra);
                /*
                $.post('<?php //base_url('caleg/get_partai');?>', {province:prov}, function(dt){
//                    console.debug(dt)
                    if(dt.rcode == 'bad'){

                    }else{


                        var apn = '';
                        $(dt.result).each(function(a, b){
                             apn += '<li><a class="partai-ref" data-dapil="'+dapil+'" data-prov="'+prov+'" data-id="'+ b.partai_id+'"><img title="'+b.partai+'" src="'+b.img_url+'"></a></li>';

                        })
                        $('#partai_list').html(apn);

                        // load caleg from first partai
                        $('#caleg_list').html('');
                        var partai = dt.result[0].partai_id;
                        $.post('<?php //echo base_url('caleg/get_caleg');?>', {province:prov, partai_id:partai, dapil:dapil}, function(dt){
                            $('#caleg_list').html(dt);
                            $('#caleg_loader').hide();

                        })

                        // loader hide

                    }
                })
                */

            });

            $('#graph_pie').load(function(){
                var bodys = this.contentWindow.document.body;
                var close_overlay = $(bodys).find('#close_overlay');

                $(close_overlay).click(function(){
                    $('.container-overlay').hide('toggle');
                    $('#graph_pie').attr('src', '');
                })
                $('#caleg_loader').hide();
            });

            $('#close_overlay').click(function(){
                $('.container-overlay').hide('blind');
                $('#caleg_list').html('');
            });

<!--            $('#partai_list').on('click', '.partai-ref', function(){-->
<!--                $('#caleg_loader').show();-->
<!--                $('#caleg_list').html('');-->
<!--                var partai = $(this).data('id');-->
<!--                var province = $(this).data('prov');-->
<!--                var dapil = $(this).data('dapil');-->
<!--                $.post('--><?//=base_url('caleg/get_caleg');?><!--', {province:province, partai_id:partai, dapil:dapil}, function(dt){-->
<!--                    $('#caleg_list').html(dt);-->
<!--                    $('#caleg_loader').hide();-->
<!--                })-->
<!--            });-->

        });

        function pop_hide()
        {
            $('#dapil-list').remove();
            $('.popover-title').text('');
            $('#pop_over').css('top', 0);
            $('#pop_over').css('left', 0);
            $('#pop_over').hide();
        }

    </script>

</html>