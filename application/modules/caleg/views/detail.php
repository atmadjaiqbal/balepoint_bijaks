<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <style type='text/css'>
        body{
            background: transparent;
        }
    </style>
    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/plugins/wookmark/css/reset.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/plugins/wookmark/css/main.css" media="screen"/>

    <link href="<?php echo base_url(); ?>assets/css/bijaks.caleg.css" rel="stylesheet">
    <!--    <link href="--><?php //echo base_url(); ?><!--assets/css/bootstrap-responsive.css" rel="stylesheet">-->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
    <![endif]-->
</head>
<body>

    <div class="caleg-header">
        <div class="row-fluid">
            <div class="span8">
                <h4 class="caleg-header-title"><?php echo $province;?>, <?php echo $dapil;?> [ <?php echo $kursi; ?> Kursi ]</h4>
            </div>
            <div class="span4 text-right">
<!--                <a class="btn-flat btn-flat-red">BANDINGKAN</a>-->
                <a id="close_overlay" class="btn-flat btn-flat-gray">CLOSE</a>
            </div>
        </div>
    </div>
    <div class="caleg-body">
        <div class="caleg-body-desc">
            <h5 class="">Area : <span><?php echo $area;?></span></h5>
            <!--p><?php //echo $area;?></p-->
            <h5 class="">Issue Daerah <i class="icon-chevron-down mycolaps" data-toggle="collapse" data-target="#issuedar" id="collapsedar"></i></h5>
                <div id="issuedar" class="collapse">
            <?php foreach($issue as $row=>$value){ ?>
                <p><?php echo $value['title'];?></p>
            <?php } ?>

<!--
            <ol>
            <?php //foreach($issue as $row=>$value){ ?>
                <li><p><?php //echo $value['title'];?></p></li>
            <?php// } ?>
            </ol>
-->         </div>
        </div>
        <div class="caleg-body-partai">
            <ul id="partai_list" class="partai-list">
                <?php
                    $partai_first = '';
                    foreach($result as $row=>$val){
                        if($row == 0){
                            $partai_first = $val['partai_id'];
                        }
                ?>
                <li>
                    <a class="partai-ref thumbnail" data-dapil="<?php echo $dapil;?>" data-prov="<?php echo $province;?>" data-provid="<?php echo $province_id;?>" data-id="<?php echo $val['partai_id'];?>"><img class="" title="<?php echo $val['partai'];?>" src="<?php echo $val['img_url'];?>">
                        <p class="partai-alias"><strong><?php echo $val['alias'];?></strong></p>
                        <p class="partai-caleg"><strong>(<?php echo $val['kandidat'];?> caleg)</strong></p>
                    </a>
                </li>
                <?php } ?>
            </ul>


        </div>
        <div id="caleg_loader" class="loader hide"></div>
        <div id="example" role="example">
            <ul class="unstyled" id="caleg_wook">

            </ul>
        </div>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/wookmark/jquery.wookmark.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/wookmark/jquery.imagesloaded.js"></script>


<script>
    (function ($) {
        $('#partai_list').on('click', '.partai-ref', function(){
            var partai = $(this).data('id');
            var province = $(this).data('prov');
            var dapil = $(this).data('dapil');
            var dt = {province:province, partai_id:partai, dapil:dapil};
            loadData(dt);
        });

        //* wook */
        var handler = null,
                apiURL = '<?=base_url('caleg/get_caleg');?>';

        // Prepare layout options.
        var options = {
            autoResize: true, // This will auto-update the layout when the browser window is resized.
            container: $('#caleg_wook'), // Optional, used for some extra CSS styling
            offset: 15, // Optional, the distance between grid items
            flexibleWidth: true,
            align : 'center',
            itemWidth: 200 // Optional, the width of a grid item
        };

        /**
         * Refreshes the layout.
         */
        function applyLayout() {
            options.container.imagesLoaded(function() {
                // Create a new layout handler when images have loaded.
                handler = $('#caleg_wook li');
                handler.wookmark(options);
            });
        };

        /**
         * Loads data from the API.
         */
        function loadData(dt) {
            $('#caleg_loader').show();
            $('#caleg_wook').html('');
            console.debug('load data');
//            $.post(apiURL, dt, onLoadData);
            $.ajax({
                type : 'post',
                url: apiURL,
                data: dt,
                success: onLoadData
            });
        };

        /**
         * Receives data from the API, creates HTML for images and updates the layout
         */
        function onLoadData(data) {
            $('#caleg_loader').hide();
            // Add image HTML to the page.
            $('#caleg_wook').append(data);

            // Apply layout.
            applyLayout();
        };

        var dt = {province:'<?php echo $province;?>', partai_id:'<?php echo $partai_first;?>', dapil:'<?php echo $dapil;?>'};

        loadData(dt);
    })(jQuery);
</script>

<script>
    $(document).ready(function() {
        $('#issuedar').on('hide show', function (e) {
            //if($(e.target).attr("id") != "collapsedar")return;
            $('#collapsedar').toggleClass('icon-chevron-down icon-chevron-right', 200);
        });
    });
</script>

    </body>
</html>
