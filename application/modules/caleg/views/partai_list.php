<?php foreach ($result as $key => $value) { ?>
	<div class="div-line div-line-small"></div>

	<div class="media">
		<a data-provid="<?php echo $province_id;?>" data-partaiid="<?php echo $value['page_id'];?>" data-id="<?php echo $dapil_id;?>" href="#" class="pull-left clglst" href="#">
			<img class="media-object" width="60" src="<?php echo $value['img_url'];?>">
		</a>
		<div class="media-body">
			<h4 class="media-heading">
				<a data-provid="<?php echo $province_id;?>" data-partaiid="<?php echo $value['page_id'];?>" data-id="<?php echo $dapil_id;?>" href="#" class="clglst"><?php echo $value['page_name'];?> <span>(<?php echo $value['caleg_count'];?> Caleg)<span></a>
			</h4>
			<h5 class="media-heading pull-left"><a target="__blank" href="<?php echo base_url('aktor/scandals').'/'. $value['page_id'];?>"><?php echo ($value['scandal_count'] == 0) ? '<small>'.$value['scandal_count'].' Scandal</small>' : '<strong>'.$value['scandal_count'].' Scandal</strong>';?></a></h5> <h4 class="media-heading pull-left">&nbsp;|&nbsp;</h4>
			<h5 class="media-heading pull-left"><a target="__blank" href="<?php echo base_url('aktor/news').'/'. $value['page_id'];?>"><?php echo ($value['news_count'] == 0) ? '<small>'.$value['news_count'].' Berita</small>' : '<strong>'.$value['news_count'].' Berita</strong>';?></a></h5> <h4 class="media-heading pull-left">&nbsp;|&nbsp;</h4>
			<h5 class="media-heading pull-left"><a target="__blank" href="<?php echo base_url('aktor/follow').'/'. $value['page_id'];?>"><?php echo ($value['follower_count'] == 0) ? '<small>'.$value['follower_count'].' Follower</small>' : '<strong>'.$value['follower_count'].' Follower</strong>';?></a></h5>
			<p class="clearfix"></p>
			<div class="loader" id="loader_<?php echo $province_id;?>_<?php echo $dapil_id;?>_<?php echo $value['page_id'];?>"></div>
			<!-- CALEG LIST -->
			<div id="caleg_list_<?php echo $province_id;?>_<?php echo $dapil_id;?>_<?php echo $value['page_id'];?>" class="caleg-list">
				
			</div>
			
		</div>
    </div>
    
<?php } ?>
