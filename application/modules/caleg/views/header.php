<!DOCTYPE HTML>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <meta charset="utf-8">
    <title>Caleg 2014</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })
            (window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-40169954-1', 'bijaks.net');
        ga('send', 'pageview');
    </script>

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>

    <link href="<?php echo base_url(); ?>assets/css/bijaks.caleg.css" rel="stylesheet">
<!--    <link href="--><?php //echo base_url(); ?><!--assets/css/bootstrap-responsive.css" rel="stylesheet">-->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
    <![endif]-->

</head>
<body>
<div id="header" class=" navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="row-fluid">
                <div class="span2">
                    <img class="caleg-image-header" src="<?php echo base_url();?>assets/images/caleg-image-header.png">
                    <a class="brand" href="#">
                        <img class="bijaks-logo" src="<?php echo base_url();?>assets/images/bijaks_logo.png">
                    </a>
                </div>
                <div class="span10">
                    <div class="text-right header-text">
                        <h4 class="h2-red">DAFTAR CALON TETAP</h4>
                        <h3 class="h2-white">ANGGOTA DPR PEMILU 2014</h3>
                    </div>

                </div>
            </div><!--/.nav-collapse -->
        </div>
    </div>

</div>
<div class="div-line-header">

</div>

