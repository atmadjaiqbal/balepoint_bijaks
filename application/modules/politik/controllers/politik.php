<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Politik extends Application {

    private $user = array();
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $data = $this->data;

        $this->load->library('cache');
        $this->load->library('typography');
        $this->load->library('politik/politik_lib');
        $this->load->library('timeline/timeline_lib');
        $this->load->library('profile/profile_lib');
        $this->load->library('news/news_lib');
        $this->load->module('profile/profile');
        $this->load->module('timeline/timeline');
        $this->load->module('news/news');
        $this->load->module('survey/survey');
        $this->load->module('headline/headline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->load->library('redis', array('connection_group' => 'master'), 'redis_master');
        $this->redis_slave = $this->redis_slave->connect();
        $this->redis_master = $this->redis_master->connect();
        $this->member = $this->session->userdata('member');
    }

    function index()
    {
        $data = $this->data;
        $data['scripts'] = array('bijaks.js', 'bijaks.struktur.js');
        $data['category'] = 'hot-profile';
        $data['topic_last_activity'] =  'profile';
        $arr_profile = array(); $arr_partai = array(); $arr_lembaga = array();
        $result = array(); $respart = array(); $reslem = array();
       // $headline = $this->redis_slave->lrange("profile:list:headline", 0, 0);
       /* if(count($headline))
        {
            foreach($headline as $key => $headval)
            {
                $profileHead = $this->redis_slave->get('profile:detail:'.$headval);
                $arrprofHead = @json_decode($profileHead, true);
                $result[0] = $arrprofHead;
            }
        }*/

        $i = 1; $profile_key = 'profile:list:hot';
        //$list_profile = $this->redis_slave->lrange($profile_key, 0, 48);
        $list_profile = $this->politik_lib->getHotlist();

        if(!empty($list_profile))
        {
            foreach ($list_profile as $key => $value)
            {
                $profile_detail = $this->redis_slave->get('profile:detail:'.$value['page_id']);
                $arr_profile = @json_decode($profile_detail, true);
                $result[$i] = $arr_profile;
                $i++;
            }
        }
        $data['hot_profile'] = $result;

        $y = 0; $partai = $this->politik_lib->getPartaiPolitic();
        if(!empty($partai))
        {
            foreach($partai as $rwpartai)
            {
                $partai_detail = $this->redis_slave->get('profile:detail:'.$rwpartai['page_id']);
                $arr_partai = @json_decode($partai_detail, true);
                $respart[$y] = $arr_partai;
                $y++;
            }
        }
        $data['hot_partai'] = $respart;

        $s = 0; $lembaga = $this->politik_lib->getLembagaNegara();
        if(!empty($lembaga))
        {
            foreach($lembaga as $rwLm)
            {
                $lembaga_detail = $this->redis_slave->get('profile:detail:'.$rwLm['page_id']);
                $arr_lembaga = @json_decode($lembaga_detail, true);
                $reslem[$s] = $arr_lembaga;
                $s++;
            }
        }
        $data['hot_lembaga'] = $reslem;

        /*---- Metatag ----- */
        $data['articledesc'] = '';
        $_p = 0;foreach( $data['hot_profile'] as $rwpoli) { if($_p>10) break;$poliname[$_p] = $rwpoli['page_name']; $_p++;}
        $data['polterkait'] = $poliname;
        $data['topnews'] = '';
        /*---- End Metatag --- */

        $data['profileheader'] = $this->profile->getHeaderMenu2();
        $html['html']['content']  = $this->load->view('profile/profile_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    function index2()
    {
        $data = $this->data;
        $data['scripts'] = array('bijaks.js', 'bijaks.struktur.js');
        $data['category'] = 'hot-profile';
        $data['topic_last_activity'] =  'profile';
        $arr_profile = array(); $arr_partai = array(); $arr_lembaga = array();
        $result = array(); $respart = array(); $reslem = array();

        $i = 1; $profile_key = 'profile:list:hot';
        //$list_profile = $this->redis_slave->lrange($profile_key, 0, 48);
        $list_profile = $this->politik_lib->getHotlist();

        if(!empty($list_profile))
        {
            foreach ($list_profile as $key => $value)
            {
                $profile_detail = $this->redis_slave->get('profile:detail:'.$value['page_id']);
                $arr_profile = @json_decode($profile_detail, true);
                $result[$i] = $arr_profile;
                $i++;
            }
        }
        $data['hot_profile'] = $result;

        $y = 0; $partai = $this->politik_lib->getPartaiPolitic();
        if(!empty($partai))
        {
            foreach($partai as $rwpartai)
            {
                $partai_detail = $this->redis_slave->get('profile:detail:'.$rwpartai['page_id']);
                $arr_partai = @json_decode($partai_detail, true);
                $respart[$y] = $arr_partai;
                $y++;
            }
        }
        $data['hot_partai'] = $respart;

        $s = 0; $lembaga = $this->politik_lib->getLembagaNegara();
        if(!empty($lembaga))
        {
            foreach($lembaga as $rwLm)
            {
                $lembaga_detail = $this->redis_slave->get('profile:detail:'.$rwLm['page_id']);
                $arr_lembaga = @json_decode($lembaga_detail, true);
                $reslem[$s] = $arr_lembaga;
                $s++;
            }
        }
        $data['hot_lembaga'] = $reslem;

        /*---- Metatag ----- */
        $data['articledesc'] = '';
        $_p = 0;foreach( $data['hot_profile'] as $rwpoli) { if($_p>10) break;$poliname[$_p] = $rwpoli['page_name']; $_p++;}
        $data['polterkait'] = $poliname;
        $data['topnews'] = '';
        /*---- End Metatag --- */

        // --Bigprofiles
        $data['bigProfiles'] = array(
            'irjokowidodo50ee1dee5bf19',
            'basukitjahajapurnama50f600df48ac5',
            'drshmuhammadjusufkalla50ee870b99cc9',
            'prabowosubiyantodjojohadikusumo50c1598f86d91',
            'megawatisoekarnoputri50ee62bce591e',
            'aburizalbakrie514662d3c6827',
        );

        $data['profileheader'] = $this->profile->getHeaderMenu2();
        $html['html']['content']  = $this->load->view('profile/profile_index2', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer_content'] = $this->load->view('template/tpl_footer_content', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function followaction()
    {
        $member = $this->session->userdata('member');
        if($member)
        {
            $account_id = $member['account_id'];
            if(isset($_POST['pid']) and isset($_POST['follow']))
            {
                $pid = $_POST['pid']; $follow = $_POST['follow'];
                if(intval($follow) == 0)
                {
                    $data = array('account_id' => $account_id, 'page_id' => $pid, 'entry_date' => mysql_datetime());
                    $this->politik_lib->InsertFollow($data);
                }else{
                    $this->politik_lib->DeleteFollow($account_id, $pid);
                }
                $this->output->set_content_type('application/json');
                $msg = array('message' => 'OK');
                $this->output->set_output(json_encode($msg));
            }else{
                $this->output->set_content_type('application/json');
                $msg = array('message' => 'error message');
                $this->output->set_output(json_encode($msg));
            }
        }
    }


    function user_politisi_comment($page_id='', $page=1)
    {
        $limit = 9;
        if(empty($page_id)){echo ''; return;        }
        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){$offset += (intval($page) - 1);}
        $result = array();
        $zrange = $limit + $offset;

        $list_key = 'profile:comment:list:';

        $list_len = $this->redis_slave->lLen($list_key.$page_id);
        if($zrange > intval($list_len)){ $zrange = $list_len;}
        if($offset > intval($list_len)){ echo ''; return;}

        $comment_list = $this->redis_slave->lRange($list_key.$page_id, $offset, $zrange);

        foreach($comment_list as $row=>$val){
            $comment_detail = $this->redis_slave->get('profile:comment:detail:'.$val);
            $comment_array = @json_decode($comment_detail, true);
            if(!empty($comment_array)){
                $comment_array['image_url'] = icon_url($comment_array['attachment_title']);
            }
            $result[$row] = $comment_array;
        }
       // var_dump($result);
        $data['result'] = $result;
        $this->load->view('timeline/timeline_profile_comment', $data);
    }

    function user_post_comment()
    {
        if(!$this->isLogin()){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login!';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('val', '', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Komentar harus di isi';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $val = $this->input->post('val', true);

        if(is_bool($id) || is_bool($val)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        // check if content exist
        $select = 'count(*) as count';
        $where = array('page_id' => trim($id));
        $cnt = $this->timeline_lib->get_content($select, $where)->row_array(0);

        if(intval($cnt['count']) == 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $cid = sha1( uniqid().time() );

        $data_post = array(
            'page_comment_id' => $cid,
            'account_id' => $this->member['account_id'],
            'page_id' => $id,
            'comment_text' => trim($val),
            'entry_date' => mysql_datetime()
        );

        $this->timeline_lib->insert_comment_profile($data_post);

        $result = array();
        // insert to redis
        $last_comment = $this->timeline_lib->get_comment_politisi($cid);
        //var_dump($last_comment->result_array());
        foreach($last_comment->result_array() as $row=>$value){
            $this->redis_master->lPush('profile:comment:list:'. $id, $value['page_comment_id']);
            $this->redis_master->set('profile:comment:detail:' . $value['page_comment_id'], json_encode($value));
            $value['image_url'] = icon_url($value['attachment_title']);
            $result[$row] = $value;
        }

        $dt['result'] = $result;
        $htm = $this->load->view('timeline/timeline_profile_comment', $dt, true);

        $lst_score = $this->timeline_lib->get_content_profile_score($id)->result_array();

        $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    function isLogin($return=false)
    {
        $cr = current_url();
        $this->member = $this->session->userdata('member');
        if(!$this->member){
            if($this->input->is_ajax_request()){
                return false;
            }else{
                if($return){
                    return false;
                }else{
                    redirect(base_url().'home/login?frm='.urlencode($cr));
                }
            }

        }

        return true;
        //exit('please');
    }

}