<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

define('SMART_CACHE', true);
define('SMART_CACHE_VERSION', 0.1);
define('DEFAULT_BASEPATH', str_replace(pathinfo(__FILE__, PATHINFO_BASENAME), '', realpath(__FILE__)));

/**
 * SmartCache
 *
 * Based on KhCache by : David Cole <neophyte@sourcetutor.com>
 *
 * @author      Eko Fedriyanto <eko@firstlabstudio.com>
 * @version     0.1
 * @copyright   2013
 */
class Smartcache
{
    /**
     * Smartcache Container Instance
     *
     * @var object
     * @access private
     */
    private $_Container;

    /**
     * Default KhCache Settings
     *
     * @var array
     * @access private
     */
    private $_Config = array('container' => 'File',
                         'ttl'       => 3600);

    /**
     * Cache Hits
     *
     * @var int
     * @access private
     */
    private $_Hits = 0;

    /**
     * Cache Misses
     *
     * @var int
     * @access private
     */
    private $_Misses = 0;

    /**
     * Constructor
     *
     * @return Smartcache
     */
    function Smartcache($config = array())
    {

        /*
         * Grab any user set Smartcache config options and
         * where applicable override the defaults.
         */

        $options = $config;

        if (isset($options['container']))
            $this->_Config['container'] = $options['container'];

        if (isset($options['ttl']))
            $this->_Config['ttl'] = $options['ttl'];

        /*
         * Try and instanciate the specified container ready to
         * handle cache requests.
         */

        if (!class_exists($class = 'Smart_Cache_'.$this->_Config['container']))
            $this->_show_error('Smart Cache :: Container \''.$class.'\' does not appear to exist.');
        else
        {
            $container_options = array();

            if (isset($options[$this->_Config['container']]) && is_array($options[$this->_Config['container']]))
                $container_options = $options[$this->_Config['container']];

            $this->_Container = new $class($container_options);
        }
    }
    
     
    /**
     * Show Error if debug config is true
     *
     * @param string   $message
     * @param string   $status_code
     * @param string   $heading
     * 
     * @return bool
     * @access private
     */
    
    
    private function _show_error($message, $status_code = 500, $heading = 'An Error Was Encountered')
    {
        if($this->_Config['debug'] == true)
        {
            show_error($message, $status_code = 500, $heading = 'An Error Was Encountered');
        }
    }
    
    

    /**
     * Delete Cache Item
     *
     * @param string $key
     * 
     * @return bool
     * @access public
     */
    function delete($key)
    {
        return $this->_Container->Delete($key);
    }
    
    /**
     * Delete All Cache Items
     *
     * @return bool
     * @access public
     */
    function delete_all()
    {
        return $this->_Container->DeleteAll();
    }
    
    /**
     * Fetch Cache Item
     *
     * @param string $key
     * 
     * @return mixed
     * @access public
     */
    function fetch($key, $force = true)
    {
        if (($ret = $this->_Container->Fetch($key,$force)) === false)
        {
            $this->_Misses++;
            return false;
        }
        else 
        {
            $this->_Hits++;
            return $ret;
        }      
    }
    
    /**
     * Store Cache Item
     *
     * @param string  $key
     * @param mixed   $data  All data will be serialized by the container
     * @param int     $ttl
     * 
     * @return bool
     * @access public
     */
    function store($key, $data, $ttl = false, $force = true)
    {
        if (!$ttl) // Use the global TTL if no TTL is specified
            $ttl = $this->_Config['ttl'];
        
        return $this->_Container->Store($key, $data, $ttl, $force);     
    }
    
    /**
     * Cache Function Response
     *
     * @param string $func
     * @param array  $args
     * @param int    $ttl
     * 
     * @return mixed
     * @access public
     */
    function call($func, $args = array(), $ttl = false, $force = true)
    {
        $key = $this->generatekey($func, $args);
        
        if (($cache = $this->fetch($key, $force)) !== false)
            return $cache;
        else 
        {
            if (!is_callable($func))
                return false;
                
            $data = call_user_func_array($func, $args);
            
            if (!$this->store($key, $data, $ttl, $force))
                return false;
                
            return $data;
        }
    }
    
    /**
     * Generate Key
     *
     * @return string
     * @access public
     */
    function generatekey()
    {
        return md5(serialize($data = func_get_args()));
    }
    
    /**
     * Get Number of Cache Hits
     *
     * @return int
     * @access public
     */
    function get_hits()
    {
        return $this->_Hits;
    }
    
    /**
     * Get Number of Cache Misses
     *
     * @return int
     * @access public
     */
    function get_misses()
    {
        return $this->_Misses;
    }
    
    /**
     * Get Number of Cache Misses
     *
     * @return int
     * @access public
     */
    function get_status($key)
    {
        return $this->_Container->Status($key); 
    }
}




/**
 * SmartCache :: File
 * 
 * A file based container for KhCache
 *
 */
class Smart_Cache_File
{
    
    /**
     * File Container Options
     *
     * @var array
     * @access private
     */
     
    private $_Config = array(
                         'store'           => '',
                         'auto_clean'      => 10,      // Default : random count 10 
                         'auto_clean_life' => 3600,    // Default : 3600 seconds  
                         'auto_clean_all'  => false,   // Default : false                      
                         'max_estimate_process'  => 0 // Default : 0 second. Use 0 if execution below 1 second
                         );
    
    /**
     * Cache Status
     *
     * @var array
     * @access private
     */
     
    private $_Cache_status = NULL;
                         

    /**
     * Constructor
     *
     * @param array $options  Container Options
     * 
     * @return Smart_Cache_File
     */
    function Smart_Cache_File($options)
    {    
        
        // Set user config options
        $this->_Config = array_merge($this->_Config, $options);   
        
        // Set default cache location if not specified  
        if (!isset($options['store']))
        {
            $store_cache_path = DEFAULT_BASEPATH.'cache/';
            
        } else {
            $store_cache_path = $this->_Config['store'];
        }
                 
        $this->_Config['store'] = $this->_createDirResults($store_cache_path).DIRECTORY_SEPARATOR;
        
        // Ensure cache store is writable                    
        if (!is_writable($this->_Config['store']))
            $this->_show_error('Smartcache :: File - Store \''.$this->_Config['store'].'\' is not writable');

        // Perform some house cleaning of the cache directory
        if (($this->_Config['auto_clean'] !== false) && (rand(1, $this->_Config['auto_clean']) === 1))
        {
            $this->_Clean();
        }
            
    }
    
    /**
     * Create Directory
     *
     * @param string $logDirectory
     * 
     * @return string
     * @access private
     */
    
    private function _createDirResults($logDirectory = '')
    {
        
        if($logDirectory == '')
        {
            $logDirectory = dirname(__FILE__);
            
            $dir_name = 'cache';
        
            $logDirectory = rtrim($logDirectory, '\\/');
    
    
            $dir_path = $logDirectory
                . DIRECTORY_SEPARATOR
                . $dir_name;
            
        } else {
            
 
        
            $logDirectory = rtrim($logDirectory, '\\/');
    
    
            $dir_path = $logDirectory;
            
        }
        
		if (!is_dir($dir_path)) {
		  
    		$old = umask(0);
    
    		@mkdir($dir_path, 0777, true);
    
    		umask($old);
		}
        
        return $dir_path;    
        
    }
    
    /**
     * Get Cache Status Item
     *
     * @param string $key
     * @param bool  $status default NULL
     * 
     * File cache status : $status
     *  processing = true
     *  no processing = false
     * 
     * @return bool
     * @access public
     */
     
     function Get_Cache_Status($key)
     {
        $file_cache_status  = $this->_Config['store'].'smartcache_'.md5($key).'_status';
        
        if (file_exists($file_cache_status))
        {
            // Try and open the cache file status    
            if (!($fp_status = @fopen($file_cache_status, 'r')))
            {
                $this->_show_error('Smart Cache :: Get_Cache_Status :: Set_Cache_Status :: '.__LINE__.' File - Error Opening File \''.$file_cache_status.'\'');
                
                return false;
            }
            
                
            // Lock file for reading
            flock($fp_status, LOCK_SH);
    
            // Retrieve the cache status contents
            $data_status = unserialize(file_get_contents($file_cache_status));
            fclose($fp_status); 
            
            if(isset($data_status['status_process']) && isset($data_status['max_estimate_process']) && isset($data_status['expire_estimate_process']))
            {
                return $data_status;
            
            } else {
                
                return false;
                
            }
            
            
        } else {
            return false;
        }
     }
    
    
    
    /**
     * Store Cache Status Item
     *
     * @param string $key
     * @param bool  $status default NULL
     * 
     * File cache status : $status
     *  processing = true
     *  no processing = false
     * 
     * @return bool
     * @access public
     */
     
     function Set_Cache_Status($key, $status = NULL, $max_estimate_process = NULL, $expire_estimate_process = NULL)
     {
        
        $file_cache_status  = $this->_Config['store'].'smartcache_'.md5($key).'_status';
        
        if($max_estimate_process != NULL)
        {
            $max_estimate_process = $this->_Config['max_estimate_process'];
        }
        
        
        // Do we have a cache status for this key?
        if (!file_exists($file_cache_status))
        {
            $status = true;
            
            $data_status  = serialize(
                array(
                    'status_process' => $status, 
                    'max_estimate_process' => $max_estimate_process,
                    'expire_estimate_process' => ($expire_estimate_process != NULL)? $expire_estimate_process : $max_estimate_process + time()
                    )
            );
            
            if (!($fp_status = @fopen($file_cache_status, 'a+')))
            {
                $this->_show_error('Smart Cache :: Set_Cache_Status :: '.__LINE__.' File - Error opening file \''.$file_cache_status.'\'.');  
                
                return false;
            }
            
            flock($fp_status, LOCK_EX); // Lock file for writing
            fseek($fp_status, 0);       // Seek to start of file
            ftruncate($fp_status, 0);   // Ensure the file is empty
        
            if (fwrite($fp_status, $data_status) === false)
            {
                $this->_show_error('Smart Cache :: Set_Cache_Status :: '.__LINE__.' File - Error writing to file \''.$file_cache_status.'\'.');
            
                fclose($fp_status); 
                
                return false;
                
            } 
            
            fclose($fp_status);  
                
            return true;
            
        } else {
            
            if($this->_Cache_status == NULL && $this->_Cache_status == false) // Recheck cache status
            {
               $this->_Cache_status = $this->Get_Cache_Status($key);
            }
            
            if($this->_Cache_status == false)
            {
                return false;
            }
            
            $status_temp = $this->_Cache_status['status_process'];
            
            
            if($max_estimate_process != NULL)
            {
                $max_estimate_process_temp = $max_estimate_process;
            } else {
                $max_estimate_process_temp = $this->_Cache_status['max_estimate_process'];
            }
            
            // $expire_estimate_process_temp = $data_status['expire_estimate_process'];
            
            // if($status_temp != $status)
            // {
                
                $data_status  = serialize(
                    array(
                        'status_process' => $status, 
                        'max_estimate_process' => $max_estimate_process_temp,
                        'expire_estimate_process' => ($expire_estimate_process != NULL)? $expire_estimate_process : $max_estimate_process_temp + time()
                        )
                );
                
                if (!($fp_status = @fopen($file_cache_status, 'a+')))
                {
                    $this->_show_error('Smart Cache :: Set_Cache_Status :: '.__LINE__.' File - Error opening file \''.$file_cache_status.'\'.');  
                    
                    return false;
                }
                
                flock($fp_status, LOCK_EX); // Lock file for writing
                fseek($fp_status, 0);       // Seek to start of file
                ftruncate($fp_status, 0);   // Ensure the file is empty
            
                if (fwrite($fp_status, $data_status) === false) 
                {
                    $this->_show_error('Smart Cache :: Set_Cache_Status :: '.__LINE__.' File - Error writing to file \''.$file_cache_status.'\'.');
                
                    fclose($fp_status);
                    
                    return false;
                    
                }
                
                fclose($fp_status);
                    
                return true;
                
            // } else {
            //    return true;
            // }    
        } 
     }
     
    /**
     * Store Cache Item
     *
     * @param string $key
     * @param mixed  $data
     * @param int    $ttl
     * @param bool   $force
     * 
     * @return bool
     * @access public
     */
    function Store($key, $data, $ttl, $force = false, $max_estimate_process = NULL, $expire_estimate_process = NULL)
    {
        $file_cache  = $this->_Config['store'].'smartcache_'.md5($key);
        $file_cache_status  = $this->_Config['store'].'smartcache_'.md5($key).'_status';
        
        if($force == true) // Will force write cache without check status of current cache
        {
            @unlink($file_cache);
            @unlink($file_cache_status);
            
            //$t1 = new MY_Timer();
            
            /*
            
            if($max_estimate_process == NULL)
            {
                if($this->_Cache_status != NULL && $this->_Cache_status != false)
                {
                    
                    $max_estimate_process = $this->_Cache_status['max_estimate_process'];
                    
                } else {
                    
                    $max_estimate_process = $this->_Config['max_estimate_process'];
                    
                }
                
            }
            */
            
/**
 *             if($expire_estimate_process == NULL)
 *             {
 *                 $expire_estimate_process = $this->_Cache_status['max_estimate_process'] + time();
 *             }
 */
            
            // $t1->start(); // Start timer
            
            // $this->Set_Cache_Status($key, true, $max_estimate_process); // Set status cache to "in process"
            
            if($this->Store_Real($key, $data, $ttl) == false ) { return false; } // Store cache
            
            // $t1->stop(); // End timer
            
            // $done_time = ceil($t1->get(MY_Timer::SECONDS));
            
            // $this->Set_Cache_Status($key, false, $done_time, time()); // Set status cache to "not in process / done"
            
            return true;
            
        } else { // Will write cache with check of current cache status
                
//            if($this->_Cache_status == NULL || $this->_Cache_status == false)
//            {
//               $this->_Cache_status = $this->Get_Cache_Status($key);
//            }

            $this->_Cache_status = $this->Get_Cache_Status($key);
            
                    
            if($this->_Cache_status != false) // If we have file cache status with valid data format
            {
                
                if($this->_Cache_status['status_process'] == false) // If not processing cache do store
                {
            
                    $t1 = new MY_Timer();
                    
                    if($max_estimate_process == NULL)
                    {
                        $max_estimate_process = $this->_Cache_status['max_estimate_process'];
                    }
                    
                    $t1->start(); // Start timer
                    
                    $this->Set_Cache_Status($key, true, $max_estimate_process); // Set status cache to "in process"
                    
                    if($this->Store_Real($key, $data, $ttl) == false ) { return false; } // Store cache
                    
                    $t1->stop(); // End timer
                    
                    $done_time = ceil($t1->get(MY_Timer::SECONDS));
                    
                    $this->Set_Cache_Status($key, false, $done_time, time()); // Set status cache to "not in process / done"
                    
                    return true;
                    
                } else { // If in processing cache do check if estimate of process already expired
                
                    if(time() < $this->_Cache_status['expire_estimate_process'] ) // In normal Process (not expired from estimate time)
                    {
                        return false;   
                        
                    } else { // expired Process (expired from estimate time)
                        
                        $t1 = new MY_Timer();
                        
                        if($max_estimate_process == NULL)
                        {
                            $max_estimate_process = $this->_Cache_status['max_estimate_process'];
                        }
                        
                        $t1->start(); // Start timer
                        
                        $this->Set_Cache_Status($key, true, $max_estimate_process); // Set status cache to "in process"
                        
                        if($this->Store_Real($key, $data, $ttl) == false ) { return false; } // Store cache
                        
                        $t1->stop(); // End timer
                        
                        $done_time = ceil($t1->get(MY_Timer::SECONDS));
                        
                        $this->Set_Cache_Status($key, false, $done_time, time()); // Set status cache to "not in process / done"
                        
                        return true;
                    }
                    
                }
                
            } else {
                
                @unlink($file_cache);
                @unlink($file_cache_status);
                
                $t1 = new MY_Timer();
                
                if($max_estimate_process == NULL)
                {
                    $max_estimate_process = $this->_Config['max_estimate_process'];
                    
                }
                
    /**
     *             if($expire_estimate_process == NULL)
     *             {
     *                 $expire_estimate_process = $this->_Cache_status['max_estimate_process'] + time();
     *             }
     */
                
                $t1->start(); // Start timer
                
                $this->Set_Cache_Status($key, true, $max_estimate_process); // Set status cache to "in process"
                
                if($this->Store_Real($key, $data, $ttl) == false ) { return false; } // Store cache
                
                $t1->stop(); // End timer
                
                $done_time = ceil($t1->get(MY_Timer::SECONDS));
                
                $this->Set_Cache_Status($key, false, $done_time, time()); // Set status cache to "not in process / done"
                
                return true;
                
            }
        }
        
    }
     
    /**
     * Store Real Cache Item
     *
     * @param string $key
     * @param mixed  $data
     * @param int    $ttl
     * 
     * @return bool
     * @access public
     */
    function Store_Real($key, $data, $ttl)
    {
        $file  = $this->_Config['store'].'smartcache_'.md5($key);
        $data  = serialize(array('expire' => ($ttl + time()), 'cache' => $data));
        
        if (!($fp = @fopen($file, 'a+')))
        {
            $this->_show_error('Smart Cache :: Store_Real :: '.__LINE__.' File - Error opening file \''.$file.'\'.');
            
            return false;
        }
         
        flock($fp, LOCK_EX); // Lock file for writing
        fseek($fp, 0);       // Seek to start of file
        ftruncate($fp, 0);   // Ensure the file is empty
    
        if (fwrite($fp, $data) === false) 
        {
            $this->_show_error('Smart Cache :: Store_Real :: '.__LINE__.' File - Error writing to file \''.$file.'\'.');
        
            fclose($fp);  
            return false;   
            
        }    
        
        fclose($fp);
        return true;
        
    }
     
    
    /**
     * Fetch Cache Item
     *
     * @param string $key
     * 
     * @return mixed
     * @access public
     */
    function Fetch($key, $force = false)
    {
        $file  = $this->_Config['store'].'smartcache_'.md5($key);

//        if($this->_Cache_status == NULL || $this->_Cache_status == false)
//        {
//           $this->_Cache_status = $this->Get_Cache_Status($key);
//        }

        if($force == true)
        {
          
                        
            // Do we have a cache for this key?
            if (!file_exists($file))
                return false;       
                
            // Try and open the cache file    
            if (!($fp = @fopen($file, 'r')))
            { 
                $this->_show_error('Smart Cache :: Fetch :: '.__LINE__.' File - Error opening file \''.$file.'\'.');
                
                return false;
            }
                
            // Lock file for reading
            flock($fp, LOCK_SH);
    
            // Retrieve the cache contents
            $data = unserialize(file_get_contents($file));
            fclose($fp); 
            
            // Has the cache item expired ?
            if (time() < $data['expire'])
            	return $data['cache'];
            else 
                return false; 
                
        } else {

            $this->_Cache_status = $this->Get_Cache_Status($key);
            
            if($this->_Cache_status == false)
            {
                return false;
            }
                        
            // Do we have a cache for this key?
            if (!file_exists($file))
                return false;       
                
            // Try and open the cache file    
            if (!($fp = @fopen($file, 'r')))
            { 
                $this->_show_error('Smart Cache :: Fetch :: '.__LINE__.' File - Error opening file \''.$file.'\'.');
                
                return false;
            }
                
            // Lock file for reading
            flock($fp, LOCK_SH);
    
            // Retrieve the cache contents
            $data = unserialize(file_get_contents($file));
            fclose($fp); 
            
            // $this->_Cache_status['status_process'] 
            
            if($this->_Cache_status['status_process'] == true)
            {
            
                if(time() > $this->_Cache_status['expire_estimate_process']) // Expired process time                
                {
                    
                    return false;
                    
                } else {
                    
                    return $data['cache'];
                    
                }
            
                
                
            } else {
                // Has the cache item expired ?
                if (time() < $data['expire'])
                	return $data['cache'];
                else 
                    return false; 
            } 
        }
        
    }
    
    /**
     * Delete Cache Item
     *
     * @param string $key
     * 
     * @return bool
     * @access public
     */
    function Delete($key)
    {
        
        $file_cache  = $this->_Config['store'].'smartcache_'.md5($key);
        $file_cache_status  = $this->_Config['store'].'smartcache_'.md5($key).'_status';
        
        if (file_exists($file_cache))
        {
            @unlink($file_cache);
            @unlink($file_cache_status);
            
            return true;
            
        } else {
            
            return false; 
        }       
    }      
    
    /**
     * Delete All Cache Files
     *
     * @return bool
     * @access public
     */
    function DeleteAll()
    {
        // Ensure we can read the cache directory 
        if (!($dh = opendir($this->_Config['store'])))
        {
            
            $this->_show_error('Smart Cache :: Fetch :: '.__LINE__.' File - Error opening Store \''.$this->_Config['store'].'\'.');
            
            return false;
        }
  
        // Remove any expired cache items
        while ($file = readdir($dh))
            if (($file != '.') && ($file != '..') && ($file != 'index.html') && is_file($cache_file = $this->_Config['store'].$file))
                if (($this->_Config['auto_clean_all'] == true) || (substr($file, 0, 11) == 'smartcache_'))
                    @unlink($cache_file);
                    
        return true;
    }
    
    /**
     * Clean Cache Directory
     *
     * @return void
     * @access private
     */
    function _Clean()
    {     
        // Ensure we can read the cache directory 
        if (!($dh = opendir($this->_Config['store'])))
        {
            $this->_show_error('Smart Cache :: Fetch :: File - Error Opening Store \''.$this->_Config['store'].'\'');
            return;
        }
  
        // Remove any expired cache items
        while ($file = readdir($dh))
            if (($file != '.') && ($file != '..') && ($file != 'index.html') && is_file($cache_file = $this->_Config['store'].$file))
                if (($this->_Config['auto_clean_all'] == true) || (substr($file, 0, 11) == 'smartcache_'))
                    if ((time() - @filemtime($cache_file)) > $this->_Config['auto_clean_life'])
                        @unlink($cache_file);
    }
    
    /**
     * Show Error if debug config is true
     *
     * @param string   $message
     * @param string   $status_code
     * @param string   $heading
     * 
     * @return bool
     * @access private
     */
    
    private function _show_error($message, $status_code = 500, $heading = 'An Error Was Encountered')
    {
        if($this->_Config['debug'] == true)
        {
            show_error($message, $status_code = 500, $heading = 'An Error Was Encountered');
        }
    }
}





/**
 * 
 * Timer Class 
 * Got from : http://codeaid.net/php/calculate-script-execution-time-(php-class)
 * 
 **/
 
 class MY_Timer
{
 
    // command constants
    const CMD_START = 'start';
    const CMD_STOP = 'end';
 
    // return format constants
    const SECONDS = 0;
    const MILLISECONDS = 1;
    const MICROSECONDS = 2;
 
    // number of microseconds in a second
    const USECDIV = 1000000;
 
 
    /**
     * Stores current state of the timer
     *
     * @var boolean
     */
    private $_running = false;
 
    /**
     * Contains the queue of times
     *
     * @var array
     */
    private $_queue = array();
 
 
    /**
     * Start the timer
     *
     * @return void
     */
    public function start()
    {
        // push current time
        $this->_pushTime(self::CMD_START);
    }
 
 
    /**
     * Stop the timer
     *
     * @return void
     */
    public function stop()
    {
        // push current time
        $this->_pushTime(self::CMD_STOP);
    }
 
 
    /**
     * Reset contents of the queue
     *
     * @return void
     */
    public function reset()
    {
        // reset the queue
        $this->_queue = array();
    }
 
 
    /**
     * Add a time entry to the queue
     *
     * @param string $cmd Command to push
     * @return void
     */
    private function _pushTime($cmd)
    {
        // capture the time as early in the function as possible
        $mt = microtime();
 
        // set current running state depending on the command
        if ($cmd == self::CMD_START) {
            // check if the timer has already been started
            if ($this->_running === true) {
                trigger_error('Timer has already been started', E_USER_NOTICE);
                return;
            }
 
            // set current state
            $this->_running = true;
 
        } else if ($cmd == self::CMD_STOP) {
            // check if the timer is already stopped
            if ($this->_running === false) {
                trigger_error('Timer has already been stopped/paused or has not yet been started', E_USER_NOTICE);
                return;
            }
 
            // set current state
            $this->_running = false;
 
        } else {
            // fail execution of the script
            trigger_error('Invalid command specified', E_USER_ERROR);
            return;
        }
       
        // recapture the time as close to the end of the function as possible
        if ($cmd === self::CMD_START) {
            $mt = microtime();
        }
       
        // split the time into components
        list($usec, $sec) = explode(' ', $mt);
 
        // typecast them to the required types
        $sec = (int) $sec;
        $usec = (float) $usec;
        $usec = (int) ($usec * self::USECDIV);
 
        // create the array
        $time = array(
            $cmd => array(
                'sec'   => $sec,
                'usec'  => $usec,
            ),
        );
 
        // add a time entry depending on the command
        if ($cmd == self::CMD_START) {
            array_push($this->_queue, $time);
 
        } else if ($cmd == self::CMD_STOP) {
            $count = count($this->_queue);
            $array =& $this->_queue[$count - 1];
            $array = array_merge($array, $time);
        }
    }
 
 
    /**
     * Get time of execution from all queue entries
     *
     * @param int $format Format of the returned data
     * @return int|float
     */
    public function get($format = self::SECONDS)
    {
        // stop timer if it is still running
        if ($this->_running === true) {
            trigger_error('Forcing timer to stop', E_USER_NOTICE);
            $this->stop();
        }
 
        // reset all values
        $sec = 0;
        $usec = 0;
 
        // loop through each time entry
        foreach ($this->_queue as $time) {
            // start and end times
            $start = $time[self::CMD_START];
            $end = $time[self::CMD_STOP];
 
            // calculate difference between start and end seconds
            $sec_diff = $end['sec'] - $start['sec'];
 
            // if starting and finishing seconds are the same
            if ($sec_diff === 0) {
                // only add the microseconds difference
                $usec += ($end['usec'] - $start['usec']);
 
            } else {
                // add the difference in seconds (compensate for microseconds)
                $sec += $sec_diff - 1;
 
                // add the difference time between start and end microseconds
                $usec += (self::USECDIV - $start['usec']) + $end['usec'];
            }
        }
 
        if ($usec > self::USECDIV) {
            // move the full second microseconds to the seconds' part
            $sec += (int) floor($usec / self::USECDIV);
 
            // keep only the microseconds that are over the self::USECDIV
            $usec = $usec % self::USECDIV;
        }
 
        switch ($format) {
            case self::MICROSECONDS:
                return ($sec * self::USECDIV) + $usec;
 
            case self::MILLISECONDS:
                return ($sec * 1000) + (int) round($usec / 1000, 0);
 
            case self::SECONDS:
            default:
                return (float) $sec + (float) ($usec / self::USECDIV);
        }
    }
   
   
    /**
     * Get the average time of execution from all queue entries
     *
     * @param int $format Format of the returned data
     * @return float
     */
    public static function getAverage($format = self::SECONDS)
    {
        $count = count($this->_queue);
        $sec = 0;
        $usec = $this->get(self::MICROSECONDS);
 
        if ($usec > self::USECDIV) {
            // move the full second microseconds to the seconds' part
            $sec += (int) floor($usec / self::USECDIV);
 
            // keep only the microseconds that are over the self::USECDIV
            $usec = $usec % self::USECDIV;
        }
 
        switch ($format) {
            case self::MICROSECONDS:
                $value = ($sec * self::USECDIV) + $usec;
                return round($value / $count, 2);
 
            case self::MILLISECONDS:
                $value = ($sec * 1000) + (int) round($usec / 1000, 0);
                return round($value / $count, 2);
 
            case self::SECONDS:
            default:
                $value = (float) $sec + (float) ($usec / self::USECDIV);
                return round($value / $count, 2);
        }
    }
 
}


/**
 * Set HTTP Status Header
 *
 * @access	public
 * @param	int		the status code
 * @param	string
 * @return	void
 */
 
if ( ! function_exists('my_set_status_header'))
{
	function my_set_status_header($code = 200, $text = '')
	{
		$stati = array(
							200	=> 'OK',
							201	=> 'Created',
							202	=> 'Accepted',
							203	=> 'Non-Authoritative Information',
							204	=> 'No Content',
							205	=> 'Reset Content',
							206	=> 'Partial Content',

							300	=> 'Multiple Choices',
							301	=> 'Moved Permanently',
							302	=> 'Found',
							304	=> 'Not Modified',
							305	=> 'Use Proxy',
							307	=> 'Temporary Redirect',

							400	=> 'Bad Request',
							401	=> 'Unauthorized',
							403	=> 'Forbidden',
							404	=> 'Not Found',
							405	=> 'Method Not Allowed',
							406	=> 'Not Acceptable',
							407	=> 'Proxy Authentication Required',
							408	=> 'Request Timeout',
							409	=> 'Conflict',
							410	=> 'Gone',
							411	=> 'Length Required',
							412	=> 'Precondition Failed',
							413	=> 'Request Entity Too Large',
							414	=> 'Request-URI Too Long',
							415	=> 'Unsupported Media Type',
							416	=> 'Requested Range Not Satisfiable',
							417	=> 'Expectation Failed',

							500	=> 'Internal Server Error',
							501	=> 'Not Implemented',
							502	=> 'Bad Gateway',
							503	=> 'Service Unavailable',
							504	=> 'Gateway Timeout',
							505	=> 'HTTP Version Not Supported'
						);

		if ($code == '' OR ! is_numeric($code))
		{
			exit('Status codes must be numeric');
		}

		if (isset($stati[$code]) AND $text == '')
		{
			$text = $stati[$code];
		}

		if ($text == '')
		{
			exit('No status text available.  Please check your status code number or supply your own message text.');
		}

		$server_protocol = (isset($_SERVER['SERVER_PROTOCOL'])) ? $_SERVER['SERVER_PROTOCOL'] : FALSE;

		if (substr(php_sapi_name(), 0, 3) == 'cgi')
		{
			header("Status: {$code} {$text}", TRUE);
		}
		elseif ($server_protocol == 'HTTP/1.1' OR $server_protocol == 'HTTP/1.0')
		{
			header($server_protocol." {$code} {$text}", TRUE, $code);
		}
		else
		{
			header("HTTP/1.1 {$code} {$text}", TRUE, $code);
		}
	}
}


if ( ! function_exists('show_error'))
{
    function show_error($message, $status_code = 500, $heading = 'An Error Was Encountered')
    {
        my_set_status_header($status_code);
        
        $message = '<p>'.implode('</p><p>', ( ! is_array($message)) ? array($message) : $message).'</p>';

        echo $message;

        exit();
    }
}

?>