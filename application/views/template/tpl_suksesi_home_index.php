<?php
$suksesi_url = base_url() . 'suksesi/index/'.$val['id_race'].'-'.urltitle($val['race_name']);
$map_photo = '';
$logo_photo = '';

// echo '<pre>';
// var_dump($val['kandidat']);
// echo '</pre>';

foreach($val['photos'] as $n=>$foto){
    if($n === 0){
        $map_photo =  $foto['large_url'];
    }

    if(intval($foto['type']) === 1){
        $logo_photo = $foto['large_url'];
    }
}
$status_suksesi = 'Survey / Prediksi';
$index_trace_status = 0;
foreach($val['status'] as $idx => $status){
    if(intval($status['draft']) == 1){
        if(intval($status['status']) == 1)
        {
            $status_suksesi = 'Survey / Prediksi';
        } else {
            if(intval($status['status']) == 2)
            {
                $status_suksesi = 'Putaran Pertama';
            } else {
                $status_suksesi = 'Putaran Kedua';
            }
        }
//        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
        $index_trace_status = $idx;
    }
}


$total_kandidat = count( $val['status'][$index_trace_status]['kandidat'] );
$tabSection = "section-tab-".$val['id_race'];
?>
<h4 title="<?php echo $val['race_name']; ?>"><a href="<?php echo $suksesi_url; ?>"><?php echo $val['race_name']; ?></a></h4>
<div class="row-fluid">
    <div class="span7">
        <div class="row-fluid">
            <img class="lazy" data-original="<?php echo $map_photo; ?>">
            <div class="score-place score-place-overlay score" data-id="<?php echo $val['content_id']; ?>" >
            </div>
            
        </div>
        
    </div>
    <div class="span5">
        <table class="table table-nomargin">
            <tr>
                <td class="suksesi-ket-place">
                    <span>Daftar pemilih tetap</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo number_format($val['daftar_pemilih_tetap'],0,'','.'); ?> Jiwa</span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Jumlah kandidat</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo $total_kandidat; ?></span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Tanggal pelaksanaan</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo mdate('%d %M %Y', strtotime($val['tgl_pelaksanaan'])); ?></span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Status</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <strong><?php echo (isset($val['status_akhir_suksesi']) && $val['status_akhir_suksesi'] ? $val['status_akhir_suksesi'] : $status_suksesi);?></strong></span>
                </td>
            </tr>
        </table>
        <?php foreach ($val['status'][$index_trace_status]['kandidat'] as $kes => $vs) { 
                $kandidat_page_id = $vs['page_id'];
                $kandidat_name  = $vs['page_name'];
                $kandidat_link  = base_url() . 'aktor/profile/' . $vs['page_id'];
                if($vs['profile_badge_url'] <> 'None')
                {
                    $kandidat_pic = $vs['profile_badge_url'];
                } else {
                    $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                }
            ?>
            <a class="pull-left" href='<?=$kandidat_link?>'>
                <div  title="<?=$kandidat_name?>" style="background:
                    url(<?=$kandidat_pic?>) no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi-mini">

                </div>
            </a>
        <?php } ?>
        
        
    </div>
</div>
<div class=" div-line-small"></div>
<!-- OPEN TAB -->
<?php if(!empty($val['status'][$index_trace_status]['lembaga'])):?>
    <?php
            $t = 0;
            $limit_lembaga = 5;
            $limit_candidat = 10;

            ?>
<div class="row-fluid row-fluid-extra">
    <div class="span1">

        <table class='home-race-candidates-left'>
        <?php foreach ($val['status'][$index_trace_status]['kandidat'] as $kes => $vs) { 
                $kandidat_page_id = $vs['page_id'];
                $kandidat_name  = $vs['page_name'];
                $kandidat_link  = base_url() . 'aktor/profile/' . $vs['page_id'];
                if($vs['profile_badge_url'] <> 'None')
                {
                    $kandidat_pic = $vs['profile_badge_url'];
                } else {
                    $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                }
            ?>
            <tr>
                <td valign="center">
                    <p class="kandidat-title"><?php echo $kes + 1;?></p>
                </td>
                <td class='home-race-percentage'>
                <a class="" href='<?=$kandidat_link?>'>
                    <div  title="<?=$kandidat_name?>" style="background:
                        url(<?=$kandidat_pic?>) no-repeat; background-position: center; background-size:40px 40px;" class="circular-suksesi-mini-super">

                    </div>
                </a>
                </td>
            </tr>   
        <?php } ?>
        </table>
    </div>
    <div class="span11">
        <div class="suksesi-hasil-place">
            
            <ul class="inline ul-inline">

                <?php
                $t = 0;
                $limit_lembaga = 5;
                $limit_candidat = 10;

                ?>
                
                    <?php
                    $limit_lembaga = 7;
                    $limit_candidat = 10;
                    $t=0; $_lembaga = array();$sort_name = array();
                    foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $sortlembaga){$sort_name[]  = $sortlembaga['lembaga_name'];}
                    if(in_array('Rekapitulasi KPU', $sort_name)){krsort($val['status'][$index_trace_status]['lembaga']);}

                    foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $lembaga):


                        if(!empty($lembaga['start_date']) && $lembaga['start_date'] <> 'None')
                        {
                            $lembaga_date = date('d-m-Y', strtotime($lembaga['start_date']));
                        } else {
                            $lembaga_date = '';
                        }

                        if(!empty($lembaga['kandidat'][$racekey]['score']['created_date']) && $lembaga['kandidat'][$racekey]['score']['created_date'] <> 'None')
                        {
                            $score_date = $lembaga['kandidat'][$racekey]['score']['created_date'];
                        } else {
                            $score_date = '';
                        }

                        // if($racekey >= $limit_lembaga){ break; }
                        $lembaga_name  = $lembaga['lembaga_name'];
                        if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];



                        $id_lembaga = $lembaga['id_trace_lembaga'];
                        $words  = preg_split("/\s+/", $lembaga_name);
                        $acronym = $lembaga['lembaga_alias']; // (count($word)s == 1) ? substr($words[0], 0,3) : '';
                        $tabName = 'tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                        $target = 'conten-tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                        // if(count($words) > 1) {
                        //     foreach($words as $w) {
                        //         if (isset($w[0])) {
                        //             if(ctype_alnum($w[0]))  $acronym .= $w[0];
                        //         }
                        //     }
                        // } else {
                        //     $acronym = substr($lembaga_name, 0,3);
                        // }
                        // $acronym =  strtoupper($acronym);
                        // if($acronym == '') $acronym = '&hellip;' ;
                        if($acronym == 'RK') { 
                            $backtab = 'style="background-color:#cecece;"'; 
                        } else { 
                            $backtab = '';
                        }

                        // $_lembaga[$t] = $acronym;

                        $is_quick = strpos($lembaga_name, "Quick Count");
                        // var_dump($is_quick);
                        if(is_bool($is_quick)) continue;

                        ?>
                        <li>
                            <strong title="<?php echo $lembaga_name .' '. $lembaga_date;?>"><a  id="" href="#"><?=$acronym;?></a></strong>
                            <div style="font-size: 9px;font-style: italic;line-height:12px;"><?php echo date('d/m/Y H:i:s', strtotime($score_date));?></div>
                            <div class="div-line-small"></div>
                            <!-- <p class="lembaga-title"><?=$lembaga_name;?></p> -->
                            <table summary='' class='home-race-candidates-small'>
                                    <?php
                                    $total_score    = 0;
                                    $winner         = '';
                                    $winner_score   = 0;
                                    $pct = 0;
                                    foreach($lembaga['kandidat'] as $kandidat) {
                                        if(count($kandidat['score']) > 0){
                                            $score = $kandidat['score'];
                                            $kandidat = $kandidat['kandidat'];
                                            if ($score['score_type'] == '1') {
                                                $total_score += (int) $score['score'];
                                            }
                                            if ($score['score'] > $winner_score) {
                                                $winner             = $kandidat['page_id'];
                                                $winner_score   = $score['score'];
                                            }
                                        }
                                    }
                                    foreach($lembaga['kandidat'] as $kandidatkey => $kandidat) :

                                        $score = $kandidat['score'];
                                        $kandidat = $kandidat['kandidat'];
                                       // if($kandidatkey >= $limit_candidat){break;}

                                        $kandidat_page_id = $kandidat['page_id'];
                                        $kandidat_name  = $kandidat['page_name'];
                                        $kandidat_link  = base_url() . 'aktor/profile/' . $kandidat['page_id'];

                                        $pasangan_page_id = $kandidat['page_id_pasangan'];

                                        if($kandidat['page_name_pasangan'] != 'None' && $kandidat['page_name_pasangan'] != 'None' &&
                                           $kandidat['page_name_pasangan'] != 'Tidak Ada Pasangan ' &&  $kandidat['page_name_pasangan'] != 'Pasangan Politisi' &&
                                           ! empty($kandidat['page_name_pasangan'])
                                        )
                                        {
                                            $pasangan_name  = trim($kandidat['page_name_pasangan']);
                                        } else {
                                            $pasangan_name  = '';
                                        }

                                        $pasangan_link  = base_url() . 'aktor/profile/' . $kandidat['page_id_pasangan'];

                                        $profile_kandidat = $this->redis_slave->get('profile:detail:'.$kandidat_page_id);
                                        $arrProfKandidat = @json_decode($profile_kandidat, true);

                                        $pasangan_profile_kandidat = $this->redis_slave->get('profile:detail:'.$pasangan_page_id);
                                        $arrPasanganProfKandidat = @json_decode($pasangan_profile_kandidat, true);

                                        $partai_kandidat_id = $arrProfKandidat['partai_id'];
                                        $partai_kandidat_pasangan_id = $arrPasanganProfKandidat['partai_id'];

                                        $prof_partai_kandidat = $this->redis_slave->get('profile:detail:'.$partai_kandidat_id);
                                        $arrProfpartaiKandidat = @json_decode($prof_partai_kandidat, true);

                                        $prof_partai_paskandidat = $this->redis_slave->get('profile:detail:'.$partai_kandidat_pasangan_id);
                                        $arrProfpartaiPasKandidat = @json_decode($prof_partai_paskandidat, true);

                                        if($val['id_race'] != '275')
                                        {
                                           if(isset($arrProfpartaiKandidat['alias']))
                                           {
                                              $kandidat_partai = ($arrProfpartaiKandidat['alias'] <> 'None' ? '('.$arrProfpartaiKandidat['alias'].')' : '');
                                           } else { $kandidat_partai = ''; }
                                        } else {
                                            if(isset($arrProfKandidat['alias']))
                                            {
                                                $kandidat_partai = ($arrProfKandidat['alias'] <> 'None' ? '('.$arrProfKandidat['alias'].')' : '');
                                            } else { $kandidat_partai = ''; }
                                        }

                                        if(isset($arrProfpartaiPasKandidat['alias']))
                                        {
                                            $pasangan_partai = ($arrProfpartaiPasKandidat['alias'] <> 'None' ? '('.$arrProfpartaiPasKandidat['alias'].')' : '');
                                        } else { $pasangan_partai = ''; }

                                        $kandidat_logo_partai = ($arrProfKandidat['icon_partai_url'] <> 'None' ? '<img src="'.$arrProfKandidat['icon_partai_url'].'" width="25px" height="25px" >' : '');
                                        $pasangan_logo_partai = ($arrPasanganProfKandidat['icon_partai_url'] <> 'None' ? '<img src="'.$arrPasanganProfKandidat['icon_partai_url'].'" width="25px" height="25px" >' : '');

                                        if($kandidat['profile_badge_url'] <> 'None')
                                        {
                                            $kandidat_pic = $kandidat['profile_badge_url'];
                                        } else {
                                            $kandidat_pic = $arrProfKandidat['icon_partai_url'];
                                            if($kandidat_pic == 'None')
                                            {
                                                $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                                            }
                                        }

                                        if($kandidat['profile_badge_pasangan_url'] <> 'None')
                                        {
                                            $pasangan_pic = $kandidat['profile_badge_pasangan_url'];
                                        } else {

                                            $pasangan_pic = $arrPasanganProfKandidat['icon_partai_url'];
                                            if($pasangan_pic == 'None')
                                            {
                                                $pasangan_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                                            }
                                        }

                                        if(!empty($score)){

                                            if ($score['score_type'] == '0') {
                                                $pct            = number_format($score['score'],2);
                                            } else {
                                                $pct            = number_format(($score['score'] / $total_score)*100,2);
                                            }
                                        }

                                        if($lembaga_name == 'Rekapitulasi KPU')
                                        {
                                            $winner_bar     = ($winner == $kandidat['page_id'] ) ? 'background-color:#128405;' : '';
                                            $winner_text = ($winner == $kandidat['page_id'] ) ? '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-weight:bold;">Pemenang  '.$pct.' </div>' : '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-weight:bold;">'.$pct.' %</div>';
                                        } else {
                                            $winner_bar     = ($winner == $kandidat['page_id'] ) ? 'background-color:#11097A;' : '';
                                            $winner_text = ($winner == $kandidat['page_id'] ) ? '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-size:9px; font-weight:bold;">'.$pct.' </div>' : '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-size:9px;font-weight:bold;">'.$pct.' </div>';
                                        }

                                        ?>
                                        <tr>
                                            <td class='home-race-percentage'>
                                                <div class='home-race-percentage-box-small'><div class='home-race-percentage-bar' style='width:<?php echo $pct * 4.5; ?>%;<?=$winner_bar;?>'></div><?=$winner_text;?></div>
                                                <!-- <p style="margin-top:-7px;font-size: 9px;font-weight: normal;"> -->
                                               <!-- <small><?php //echo $kandidat_name;?></small> -->
                                                <!-- </p> -->
                                                <!-- <p style="font-size: 9px;font-weight: normal;">
                                                    <?php //echo $pasangan_name.' '.$pasangan_partai; ?>
                                                </p> -->
                                            </td>
                                        </tr>

                                    <?php endforeach;?>
                                </table>
                        </li>

                        <?php
                        $t++;
                    endforeach;

                    if(in_array('KPU', $_lembaga))
                    {

                    } else {
                    ?>
                        <li>
                            <a style="display:block;background-color:#cecece;line-height: 45px;" href="#">KPU</a>
                            <div class="div-line-small"></div>
                            <!-- <p class="lembaga-title"><?=$lembaga_name;?></p> -->
                            <table summary='' class='home-race-candidates-small'>
                                <?php for ($i=0; $i < 12; $i++) { ?>
                                <tr>
                                    <td class='home-race-percentage'>
                                        <div class='home-race-percentage-box-small'>
                                            <div class='home-race-percentage-bar' style='width:0%;'></div>
                                            <div style="color:#ffffff;margin-top:-38px;padding:10px;font-size:9px;font-weight:bold;">0</div>
                                        </div>
                                    </td>
                                </tr>
                                <?php } ?>
                            </table>
                        </li>
                         
                    <?php
                    }

                    ?>
            </ul>
        </div>
    </div>
</div>
<?php endif; ?>


<script>
    $(document).ready(function() {
        var height_suksesi = $('#conten-<?=$tabName;?>').height();
        $('#conten-tab-<?php echo $index_trace_status;?>-9999-<?php echo $id_lembaga;?>').css({'height':+height_suksesi+'px','background-color':'#fff'});
    });
</script>
