<?php
    foreach($wall_list as $row=>$wall){

?>

<?php
    $post_at = '';
    $text = '';
    $desc = '';
    $icon = 'status';
    $uri = '';
    $flag = '<span class="dropdown">';
    $flag .= '&nbsp; <a data-toggle="dropdown" data-id="'. $wall['content_id'] .'" class="flag hide dropdown-toggle"><i class="icon-flag " ></i></a>';

    $flag_dropdown = '<ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">';
    $flag_dropdown .= '<li role="presentation"><a class="report-spam" data-tipe="content" data-id="'. $wall['content_id'] .'" role="menuitem" tabindex="-1" href="#">report spam</a></li>';

    if(($this->member['user_id'] == $wall['page_id']) || ($this->member['account_id'] == $wall['account_id'])){
        $flag_dropdown .= '<li role="presentation"><a class="remove-status" data-tipe="content" data-id="'. $wall['content_id'] .'" role="menuitem" tabindex="-1" href="#">remove</a></li>';
    }
    $flag_dropdown .= '</ul>';

    $flag .= $flag_dropdown;
    $flag .= '</span>';

    if($wall['content_group_type'] == 10){
        $post_at = '<small><em>- post status</em></small>';
        $text = $wall['description'];
        $icon = 'status';
    }elseif($wall['content_group_type'] == 11){
        $post_at = '<small><em>- post blog</em></small>';
        $uri = base_url().'komunitas/blog/'.$wall['page_id'].'/'.$wall['content_id'].'/'.urltitle($wall['title']);
        $icon = 'status';
        $text = '<a href="'.$uri.'">'.ucwords($wall['title']).'</a>';
        $desc = (!empty($wall['description'])) ? '<p class="komu-wall-list-status">'.$wall['description'].'</p>' : '';
    }elseif($wall['content_group_type'] == 40){
        $post_at = '<small><em>- post album</em></small>';
        $uri = base_url().'komunitas/photo/'.$wall['page_id'].'/'.$wall['content_id'].'/'.urltitle($wall['title']);
        $text = '<a href="'.$uri.'">'.ucwords($wall['title']).'</a>';
        $desc = (!empty($wall['description'])) ? '<p class="komu-wall-list-status">'.$wall['description'].'</p>' : '';
        $icon = 'photo';
    }elseif($wall['content_group_type'] == 22){
        $post_at = '<small><em>- post foto profile</em></small>';
        $text = 'Merubah foto profile';
        $desc = (!empty($wall['description'])) ? '<p class="komu-wall-list-status">'.$wall['description'].'</p>' : '';
        $icon = 'photo';
    }elseif($wall['content_group_type'] == 20){
        $post_at = '<small><em>- post foto</em></small>';
        $text = $wall['title'];
        $desc = (!empty($wall['description'])) ? '<p class="komu-wall-list-status">'.$wall['description'].'</p>' : '';
        $icon = 'photo';
    }

    $img_url = badge_url($wall['from_attachment_title']);
    $use_bg = ($row % 2 == 1 ) ? 'komu-wall-list-bg' : '';

    $mention = ($wall['from_page_name'] != $wall['to_page_name']) ? '<a href="'.base_url('aktor/profile/'.$wall['to_page_id']).'">'.strtoupper('@'.$wall['to_page_name']).'</a>' : '';

?>

<div data-id='<?=$wall['content_id'];?>' class="row-fluid komu-wall-list <?=$use_bg;?>">
    <div class="row-fluid komu-wall-header">
        <div class="span2">
            <a href="#">
                <div data-id='<?=$wall['content_id'];?>' title="<?=$wall['from_page_name'];?>" style="background:
                                                        url(<?=$img_url;?>) no-repeat; background-position: center; background-size:65px 65px;" class="circular-wall">

                </div>
            </a>
        </div>
        <div class="span9 komu-wall-text-main">
            <h5 class="komu-wall-list-uname"><a href="<?=base_url('komunitas/'.$wall['from_page_id']) ;?>"><?=strtoupper($wall['from_page_name']);?></a> <?=$mention; ?> &nbsp; <?=$post_at;?> <?=$flag;?></h5>
            <div id="score_<?=$wall['content_id'];?>" data-id="<?=$wall['content_id'];?>" data-tipe="1" class="score"></div>
            <p class="komu-wall-list-status"><?=$text;?></p>

        </div>
        <div class="span1">
            <div class="pull-right komu-wall-icon-big komu-wall-icon-big-<?=$icon;?>">

            </div>
        </div>
    </div>
            <?php
            if(intval($wall['content_group_type']) == 40){
                $st['id'] = $wall['content_id'];

            ?>
    <div class="row-fluid">

        <div class="span10 offset2 komu-wall-list-foto-container">
            <div class="row-fluid">
                <?php if(!empty($wall['photo'])){ ?>
                    <?php foreach($wall['photo'] as $rw=>$vl){ ?>
                    <?php if($rw == 4) break; ?>
                    <img class="span3 komu-wall-list-foto" src="<?=thumb_url(($vl['attachment_title']));?>">
                    <?php } ?>
                <?php } ?>
            </div>
            <?php //$this->load->view('template/komunitas/tpl_komunitas_wall_photo', $st);?>
            <?=$desc;?>
        </div>
    </div>
            <?php }elseif(intval($wall['content_group_type']) == 22 || intval($wall['content_group_type']) == 20){ ?>
            <div class="row-fluid">
                <div class="span10 offset2 komu-wall-list-foto-container">
                    <img class="komu-wall-list-foto" src="<?=large_url($wall['attachment_title']);?>">
                    <?=$desc;?>
                </div>
            </div>
            <?php }elseif(intval($wall['content_group_type']) == 11) { ?>
                <div class="row-fluid">
                    <div class="span10 offset2 komu-wall-list-foto-container">
                        <?=$desc;?>
                    </div>
                </div>

            <?php } ?>


    <div class="row-fluid">
        <div class="span10 offset2 comment-row">
            <div class="div-line-small"></div>
            <div class="row-fluid">
                <div class="comment-side-left">
                    <div style="background:
                            url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                    </div>
                </div>

                <div class="span10">
                    <div class="row-fluid">
                        <input id="inpt_<?=$wall['content_id'];?>" data-id="<?=$wall['content_id'];?>" type="text" name="comment" class="span12 media-input comment_type" >
                    </div>
                    <div class="row-fluid">
                        <div class="span12 text-right">
                            <?php if(!is_array($this->member)){ ?>
                            <span>Login untuk komentar</span>&nbsp;
                            <a class="btn-flat btn-flat-dark">Register</a>
                            <a class="btn-flat btn-flat-dark">Login</a>
                            <?php } ?>
                            <a data-id="<?=$wall['content_id'];?>" class="btn-flat btn-flat-gray send_coment">Send</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- #### COMMENT LIST #### -->
    <div  class="row-fluid ">
        <div id="cmnt_<?=$wall['content_id'];?>" data-uri="<?=$uri;?>" data-limit="3" data-offset="0" data-id="<?=$wall['content_id'];?>" class="span10 offset2 comment-container-wall hide">
<!--            <div class="div-line-small"></div>-->

        </div>
    </div>

</div>
<?php } ?>