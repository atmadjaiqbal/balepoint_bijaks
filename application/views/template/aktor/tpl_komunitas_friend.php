<?php foreach($follow as $row=>$val){ ?>
<?php
    $text = character_limiter($val['about'], 100);
    $btn_text = 'UNFRIEND';
    if(intval($tipe) != 1){
        $text = $val['request_msg'];

    }
    if(intval($tipe) == 1){
        $fid = '';
        $btn_text = 'UNFRIEND';
        $to_id = $val['friend_account_id'];
    }elseif(intval($tipe) == 2){
        $fid = $val['friend_request_id'];
        $to_id = $val['account_id'];
        $btn_text = 'TERIMA';
    }if(intval($tipe) == 3){
        $fid = $val['friend_request_id'];
        $to_id = $val['to_account_id'];
        $btn_text = 'BATALKAN';
    }
    $city = empty($val['current_city']) ? '' : $val['current_city'].', ';
    ?>
<div class="row-fluid">
    <div class="span1">
        <div style="background:
                url('<?=icon_url($val['attachment_title']);?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

        </div>
    </div>
    <div class="span7 komu-follow-list-title" >
        <a href="<?=base_url('komunitas/'.$val['page_id']);?>"><h5><?=strtoupper($val['page_name']);?></h5></a>
        <p><?=$city;?> <?=($val['gender'] == 'M') ? 'Laki Laki' : 'Perempuan';?></p>
        <p><?=$text;?></p>
    </div>
    <div class="span4 text-right">
<?php if($user['user_id'] == $this->member['user_id']){ ?>
        <a href="#" data-tipe="<?=$tipe;?>" data-id="<?=$user['account_id'];?>" data-user="<?=$to_id;?>" data-fid="<?=$fid;?>" class="btn-flat btn-flat-red pull-right unfriend"><?=$btn_text;?></a>
        <?php if(intval($tipe) == 2){ ?>
            <a href="#" data-tipe="4" data-id="<?=$user['account_id'];?>" data-user="<?=$to_id;?>" data-fid="<?=$fid;?>" class="btn-flat btn-flat-gray pull-right tolak">TOLAK</a>
        <?php } ?>
<?php } ?>
    </div>
</div>
<div class="div-line-small"></div>
<?php } ?>