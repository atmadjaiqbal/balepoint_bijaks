
<div id="myCarousel_<?=$id;?>" data-interval="false" data-pause="hover" class="carousel slide">

    <!-- Carousel items -->
    <div class="carousel-inner">
        <?php foreach($photos as $kes => $item){ ?>
        <div class="<?php echo ($kes == 0) ? 'active' : ''; ?> item komu-carousel-image">
            <img class="komu-wall-list-foto" src="<?=large_url($item['attachment_title']);?>">
        </div>
        <?php } ?>

    </div>
<!--    <div class="score-place score-place-overlay score" data-id="--><?php //echo $wall['content_id']; ?><!--" >-->
<!--    </div>-->
    <div class="row-fluid komu-carousel-control-container">
        <div class="span1">
            <!-- Carousel nav -->
            <a class="komu-carousel-control pull-left left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        </div>
        <div class="span10 komu-carousel-control-thumbs">
            <!-- Carousel thumb -->
            <ol class="komu-carousel-indicators">
                <?php foreach($photos as $kem => $item){ ?>
                <li data-target="#myCarousel_<?=$id;?>" data-slide-to="<?php echo $kem; ?>" class="<?php echo ($kem == 0) ? 'active' : ''; ?>">
                    <img class="komu-wall-list-foto" src="<?=thumb_url(($item['attachment_title']));?>">
                </li>
                <?php } ?>

            </ol>
        </div>
        <div class="span1">
            <a class="komu-carousel-control pull-right right" href="#myCarousel_<?=$id;?>" data-slide="next">&rsaquo;</a>
        </div>
    </div>
</div>
