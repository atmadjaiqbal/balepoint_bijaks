<style type="text/css">
#topside_header{
    background-image: url('<?php echo base_url('assets/images/headers/HEADER-18.jpg');?>');
    background-repeat: no-repeat;
    height: 150px;
}    
#heading_carousel {
    /*height: 150px;*/
    /*margin-left: -1px;*/
    margin-top: -3px !important;
    /*width: 960px;*/
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.carousel.css')?>" />
<script type="text/javascript" language="javascript" src="<?php echo base_url('assets/js/owl.carousel.min.js')?>" ></script>
<div class="sub-header-container" style="margin-top: 28px;">

    <div id="topside_header">
        <div id="topside_header">
        <a href="<?php echo base_url();?>">
            <img src="<?php echo base_url('assets/images/bijaks_logo.png'); ?>" style="z-index: 99;margin-left: 33px;margin-top:23px;width: 158px;opacity: 0.8;"/>        
        </a>
        </div>

        <div style="clear: both"></div>
    </div>

    <div style="clear: both"></div>
    <div id="heading_carousel">
        <div id="hot_profiles_carousel" class="owl-carousel">
            <?php
            foreach($hot_profile as $k=>$v){
                $thumbUrl = $v['profile_photo'][0]['thumb_url'];
                $pageName = $v['page_name'];
                $pageId = $v['page_id'];
            ?>
                <div class="hot_profile">
                    <div class="imageHolder">
                        <a href="<?php echo base_url('aktor/profile/'.$pageId); ?>">
                            <img src="<?php echo $thumbUrl;?>" class="lazyOwl">
                        </a>
                        <div class="caption"><br><?php echo $pageName;?></div>
                    </div>

                </div>

            <?php
            }
            ?>
        </div>
    </div>

</div>
<!-- <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div> -->

<div class="sub-header-container">
    <?php $this->timeline->sub_header('bijak'); ?>
</div>

<!--<div id='disclaimer' class="sub-header-container"></div>-->
<script type="text/javascript">
    $("#hot_profiles_carousel").owlCarousel({

        autoPlay: 1000, //Set AutoPlay to 3 seconds
        navigation: true,
        items : 5,
        itemsDesktop : [1199,5],
        itemsDesktopSmall : [979,5],
        navigationText: [
            "<i >XXXXXXXXXXXXXX</i>",
            "<i >YYYYYYYYYYYYY</i>"
        ],

    });

    $(document).ready(function(){
        $("#hot_profiles_carousel .owl-controls .owl-nav .owl-prev").html('<img src="<?php echo base_url('assets/images/left_transparent_control.png');?>"/>')
        $("#hot_profiles_carousel .owl-controls .owl-nav .owl-next").html('<img src="<?php echo base_url('assets/images/right_transparent_control.png');?>"/>')


    })
</script>
