<style>
.rotated-images{
    margin-top: 10px;
    margin-left: 10px;
    /*-webkit-transform: rotate(-8deg);*/
    /*-moz-transform: rotate(-8deg);*/
    /*-ms-transform: rotate(-8deg);*/
    /*-o-transform: rotate(-8deg);*/
    /*transform: rotate(-8deg);*/
}

.frame_miring{
    -webkit-transform: rotate(-7deg);
    -moz-transform: rotate(-7deg);
    -ms-transform: rotate(-7deg);
    -o-transform: rotate(-7deg);
    transform: rotate(-7deg);

    margin-top: -334px;
    margin-left: -43px;

}

.img-polaroid {
    /*background-color: #fff;*/
    border: 1px none rgba(0, 0, 0, 0.2) !important;
    /*box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1) ;*/
    /*padding: 4px;*/
}

.politisi-carousel-control-container {
    background-color: #ffffff !important;
    border: 2px solid #c4c4c4;
    padding-top: 4px !important;
    padding-left: 4px !important;
    padding-right: 4px !important;
    padding-bottom: 4px !important;
    width: 308px;
    border-radius: 3px 3px 3px 3px;

}

.box-biodata{
    margin-top: 10px;
    border: 2px solid #c4c4c4;
    padding-top: 4px !important;
    padding-left: 4px !important;
    padding-right: 4px !important;
    padding-bottom: 4px !important;
    width: 308px;
    height: auto;
    border-radius: 3px 3px 3px 3px;


}

.row-fluid [class*="span"] {
    min-height: 13px !important;
}

.bdata{
    font-size: 14px;
    font-weight: bold;
}

.politisi-carousel-logo-partai {
    left: 8px;
    /*height: 40px;*/
    /*position: relative;*/
    /*top: -306px;*/
    /*width: 40px;*/
}

#cengkunek{
    position: absolute;
    margin-top: -239px;
    margin-left: -31px;
    z-index: 99;
}

.dotted_bottom{
    border-bottom: 3px dotted #c4c4c4;
    width: 319px;
    margin-left: 0;
}

.wall_class{
    width: 319;
}

.pterkait{
    float: left;
    height: 60px;
    width: 60px;
    margin-bottom: 5px;
    margin-right: 5px;
    border: solid 2px #c4c4c4;
}

.icon_profile{
    width: 60px;
    height: 60px;
}
</style>
<div id="politisi-carousel" data-interval="3000" class="carousel-politisi slide">
    <div class="carousel-inner frame_miring">
        <?php
        $k=1;
        foreach($list_photo as $kes => $item){
            $_actisty = $k == 1 ? 'active' : '';

            if($k > 5) break;
            if(file_exists('public/upload/image/politisi/'.$item['page_id'].'/badge/'.$item['attachment_title'])){
                $_badgefoto = $item['badge_url'];
            } else {
                if(file_exists('public/upload/image/politisi/'.$item['page_id'].'/thumb/'.$item['attachment_title'])){
                    $_badgefoto = $item['thumb_url'];
                } else {
                    $_badgefoto = base_url(). 'assets/images/thumb/noimage.jpg';
                }
            }
            ?>

            <div class="item <?php echo $_actisty;?> politisi-carousel-image rotated-images">
                <img class="komu-side-img lazy" src="<?php echo $_badgefoto;?>">
            </div>
            <?php $k++; } ?>
        <?php
        if(!empty($user['thumb_partai_url']) && $user['thumb_partai_url'] != 'None')
        {
        ?>
           <div class="politisi-carousel-logo-partai">
               <img class="lazy" src="<?php echo $user['thumb_partai_url']; ?>">
           </div>

        <?php
        }
        ?>


    </div>

</div>
<div id="cengkunek"><img class="lazy" src="<?php echo base_url('assets/images/cengkunek.png')?>"/></div>


<div class="politisi-carousel-control-container" style="margin-top: 110px;">
    <div class="politisi-carousel-control-thumbs">
        <!-- Carousel thumb --->
        <ol class="politisi-carousel-indicators">
            <?php
            $i=0;
            foreach($list_photo as $kem => $itema){
                if($i > 4) break;
                ?>
                <li data-target="#politisi-carousel" data-slide-to="<?php echo $i; ?>">
                    <img class="img-polaroid lazy" src="<?php echo $itema['thumb_url'];?>">
                </li>
                <?php $i++; } ?>

            <?php
            if( $i < 2 || $i < 3 || $i < 4)
            {
                for ($o = 1; $o < (5 - $i); $o++) {
                    ?>
                    <li class="politisi-detail-noimage"></li>
                <?php
                }
            }
            ?>

        </ol>
    </div>
</div>


<div class="row-fluid box-biodata">
    <div class="span12" style="text-align: center;">
        <a href="<?php echo base_url('aktor/profile/'.$user['page_id']);?>" class="aktor-side-nav-link-user"><h4><?php echo strtoupper($user['page_name']);?></h4></a>
    </div>

    <?php if($this->member['user_id'] == $user['page_id'] && $this->member['account_id'] == $user['account_id']){ ?>
    <div class="span12 score-place-overlay">
        <a id="change_pp" class="btn-flat btn-flat-red pull-right"><i class="icon-camera"></i></a>
    </div>
    <?php } ?>

    <?php if($this->member['user_id'] == $user['page_id'] && $this->member['account_id'] == $user['account_id']){ ?>
    <div class="span12">
        <a href="<?=base_url('aktor/profile/'.$user['page_id']);?>" class="btn-flat btn-flat-gray pull-right">Edit</a>
    </div>
    <?php } ?>

    <?php if(isset($user['birthday']) && !empty($user['birthday']) && $user['birthday'] != 'None'){?>
    <div class="row-fluid bdata">
        <div class="span4">
            Tanggal Lahir
        </div>
        <div class="span1">
            :
        </div>
        <div class="span7">
            <?php echo date('d F Y', strtotime($user['birthday'])); ?>
        </div>
    </div>
    <?php };?>

    <?php if(isset($user['partai_name']) && !empty($user['partai_name']) && $user['partai_name'] != 'None'){?>
    <div class="row-fluid bdata">
        <div class="span4">
            Partai
        </div>
        <div class="span1">
            :
        </div>
        <div class="span7">
            <?php echo $user['partai_name']; ?>
        </div>
    </div>
    <?php };?>

    <?php if(isset($user['posisi']) && !empty($user['posisi']) && $user['posisi'] != 'None'){?>
        <div class="row-fluid bdata">
            <div class="span4">
                Posisi
            </div>
            <div class="span1">
                :
            </div>
            <div class="span7">
                <?php echo $user['posisi']; ?>
            </div>
        </div>

    <?php };?>

</div>

<div class="row-fluid" style="margin-top: 20px;width: 319px;">
    <div class="wall_class" style="background-color: #c4c4c4;">
        <a href="<?=base_url('aktor/'.$user['page_id']);?>" class="aktor-side-nav-link" ><span>Wall</span></a>
    </div>
    <div class="dotted_bottom">
        <a href="<?=base_url('aktor/scandals/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span>Scandal</span>
            <span class="pull-right"><?=$user['count_scandal'];?></span>
        </a>
    </div>
    <div class="dotted_bottom">
        <a href="<?=base_url('aktor/news/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span>Berita</span>
            <span class="pull-right"><?=$user['count_news'];?></span>
        </a>
    </div>
    <div class="dotted_bottom">
        <a href="<?=base_url('aktor/photo/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span>Photos</span>
            <span class="pull-right"><?=$user['count_photo'];?></span>
        </a>
    </div>
    <div class="dotted_bottom">
        <a href="<?=base_url('aktor/follow/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span>Follower</span>
            <span class="pull-right"><?=$user['count_follower'];?></span>
        </a>
    </div>
    <div style="margin-top: 30px;">
        <a href="<?=base_url('aktor/follow/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span style="font-size: 16px;">Profil Terkait</span>
        </a>
        <div class="row-fluid">
        <?php
        foreach($follow as $index=>$val){
            $fUrl = base_url('komunitas/'.$val['page_id_fl']);
            $imgUrl = badge_url($val['attachment_title']);
        ?>
            <div class="pterkait">
                <a href="<?php echo $fUrl;?>">
                    <img src="<?php echo $imgUrl;?>" class="icon_profile lazy"/>
                </a>
            </div>
        <?php
        };
        ?>
        </div>
    </div>

</div>



<script>
    $(document).ready(function() {
        $('.carousel-politisi').carousel({interval:false})
    });
</script>
