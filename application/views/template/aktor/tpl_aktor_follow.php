<?php foreach($follow as $row=>$val){ ?>
    <?php
            $city = empty($val['current_city']) ? '' : $val['current_city'].', ';

        ?>
<div class="row-fluid">
    <div class="span1">
        <div style="background:
                url('<?=icon_url($val['attachment_title']);?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

        </div>
    </div>
    <div class="span9 komu-follow-list-title" >
        <a href="<?=base_url('komunitas/'.$val['page_id_fl']);?>"><h5><?=strtoupper($val['page_name']);?></h5></a>
        <p><?=$city;?> <?=($val['gender'] == 'M') ? 'Laki Laki' : 'Perempuan';?></p>
        <p><?=character_limiter($val['about'], 100);?></p>
    </div>
    <div class="span2 text-right">
        <?php if($user['page_id'] == $this->member['user_id']){ ?>
        <a data-tipe="<?=$tipe; ?>" data-id="<?=$val['account_id'];?>" data-user="<?=$val['page_id'];?>" href="#" class="btn-flat btn-flat-red pull-right unfollow">UNFOLLOW</a>
            <?php } ?>
    </div>
</div>
<div class="div-line-small"></div>
<?php } ?>