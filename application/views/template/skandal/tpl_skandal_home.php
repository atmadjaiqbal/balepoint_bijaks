<div class="headline" style="width: 310px;">

<?php
if(!empty($val))
{
    $skandal_url = base_url() . 'scandal/index/'.$val['scandal_id'].'-'.urltitle($val['title']);
    $skandal_photos = (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
    $css_image = 'news-image-skandal';
    $limit_terkait = 6;
    if(isset($val['tipe'])){
        $limit_terkait = 2;
        $css_image .= '-'.$val['tipe'];
        $skandal_photos = (count($val['photos']) > 0) ? $val['photos'][0]['thumb_landscape_url'] : '';
    }

    $headx_photos = (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
    $headm_photos = (count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';

    foreach($val['photos'] as $kes => $item){
        if(intval($item['type']) == 1){
            $headx_photos = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                $headx_photos = $item['original_url'];
            }

            $headm_photos = $item['thumb_url'];
            break;
        }
    }

    ?>


    <!-- SCANDAL -->

    <div class="headline-title">
        <h4 style="margin-left: 5px;height: 65px;"><a href="<?php echo $skandal_url; ?>"><?php echo $val['title']; // (strlen($val['title']) > 25) ? substr($val['title'], 0, 25).'...' : $val['title']; ?></a></h4>
    </div>
    <div class="headline-detail clearfix">

        <div id="myCarousel-<?php echo $val['scandal_id'];?>" data-interval="false" data-pause="hover" class="carousel-head slide post-pic" style="float:none;">

            <div class="carousel-inner">
                <div class="active item photo-carousel">
                    <img src="<?=$headx_photos;?>">
                </div>

                <?php $k=1; $max = 6; foreach($val['photos'] as $kes => $item){ ?>
                    <?php
                    if($k >= $max) break;

                    if(isset($item['type']))
                    {
                        if($item['type'] == 1)
                        {
                            $max += 1;
                            continue;
                            $_x_active = 'active';
                        } else {
                            if($k == 0)
                            {
                                $_x_active = 'active';
                            } else {
                                $_x_active = '';
                            }
                        }
                    } else {
                        //if($k == 1) { $_x_active = 'active'; } else { $_x_active = ''; }
                    }
                    ?>
                    <div class="<?php echo $_x_active; ?> item photo-carousel">
                        <?php
                        $img_uri_car = $item['large_url'];
                        if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){

                            $img_uri_car = $item['original_url'];
                        }
                        ?>
                        <img src="<?=$img_uri_car;?>">
                    </div>
                    <?php $k++; } ?>

            </div>
            <div class="score-place score-place-overlay score" data-id="<?php echo $val['content_id']; ?>" ></div>

            <div class="photo-control-container">
                <!--          <div class="photo-button">
                             <a class="pull-left left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
          </div>-->
                <div class="photo-carousel-control">
                    <!-- Carousel thumb -->
                    <ol class="photo-carousel-indicators">
                        <li data-target="#myCarousel-<?php echo $val['scandal_id'];?>" data-slide-to="0" class="active" style="margin-right:0px;">
                            <img src="<?=$headm_photos;?>">
                        </li>
                        <?php $i=1; $maxm = 5; foreach($val['photos'] as $kem => $item){ ?>
                            <?php
                            if($i >= 6) break;
                            if(isset($item['type']))
                            {
                                if($item['type'] == 1)
                                {
                                    $maxm += 1;
                                    continue;
                                    $set_active = 'active';
                                } else {
                                    $set_active = '';
                                }
                            } else {
                                if($i == 1)
                                {
                                    //$set_active = 'active';
                                } else {
                                    $set_active = '';
                                }
                            }
                            ?>
                            <li data-target="#myCarousel-<?php echo $val['scandal_id'];?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $set_active; ?>" style="margin-right:0px;">
                                <img src="<?php echo $item['thumb_url'] ;?>">
                            </li>

                            <?php $i++; } ?>

                        <?php
                        if($i < 6 || $i < 5 || $i < 4 || $i < 3 || $i < 2)
                        {
                            for ($o = 1; $o <= (6 - $i); $o++) {
                                ?>
                                <li style="margin-right:0px;">
                                    <div style="width: 50px;height: 46px;border: solid 1px #ffffff;border-right: none;"></div>
                                </li>
                            <?php

                            }
                        }
                        ?>

                    </ol>
                </div>
                <!--          <div class="photo-button">
                            <a class="pull-right right" href="#myCarousel" data-slide="next">&rsaquo;</a>
          </div> -->
            </div>
            <div class="row-fluid">
                <div class="">
                    <span><strong>&nbsp;Politisi Terkait</strong></span>
                    <?php if (count($val['players']) > 6){ ?>
                        <a class="pull-right" href="<?php echo $skandal_url; ?>">More &nbsp;</a>
                    <?php } ?>
                </div>

                <div class="">
                    <?php
                    if (count($val['players']) > 0)
                    {
                        echo '<ul class="ul-img-hr">';
                        foreach ($val['players'] as $key => $pro)
                        {
                            if($key === $limit_terkait) break;
                            if($pro['profile_icon_url'] == 'None')
                            {
                                $_src = base_url(). 'assets/images/no-image-politisi.png';
                            } else {
                                $_src = $pro['profile_icon_url'];
                            }
                            ?>
                            <li class="">
                                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                    <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="" src="<?php echo $_src; ?>">
                                </a>
                            </li>
                        <?php
                        }
                        echo '</ul>';
                    } else{
                        ?>
                        <span>Tidak ada politisi terkait</span>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <hr class="hr-white">
            <div class="row-fluid">
                <div class="span2 span2-pd-left">
                    <span><strong>Status</strong></span>
                </div>
                <div class="span10 span10-pd-right">
                    <span class=""><?php echo ucwords($val['status']); ?></span>
                </div>
            </div>
            <hr class="hr-white">
            <div class="row-fluid">
                <div class="span2 span2-pd-left">
                    <span><strong>Kejadian</strong></span>
                </div>
                <div class="span10 span10-pd-right">
                    <span class=""><?php echo mdate('%d %M %Y', strtotime($val['date'])); ?></p></span>
                </div>
            </div>
            <hr class="hr-white">
            <div class="row-fluid">
                <div class="span2 span2-pd-left">
                    <span class=""><strong>Dampak</strong></span>
                </div>
                <div class="span10 span10-pd-right">
                    <span class=""><?php echo (strlen($val['uang']) > 100 ? substr($val['uang'], 0, 100) .'...' : $val['uang']); ?></span>
                </div>
            </div>
        </div>

    </div>
<?php
}
?>
</div>
