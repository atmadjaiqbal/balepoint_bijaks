<?php 
$scandalUrl = base_url() . 'scandal/index/'.$val['scandal_id'].'-'.urltitle($val['title']);
$title = !empty($val['title']) ? $val['title'] : 'Untitled';
$scandalPhotos = (count($val['photos']) > 0) ? $val['photos'][count($val['photos']) - 1]['large_url'] : '';
$limitTerkait = 4;
if(isset($val['tipe'])){
    $limitTerkait = 4;
    $scandalPhotos = (count($val['photos']) > 0) ? $val['photos'][count($val['photos'])-1]['large_url'] : '';
}
$shortDescription = substr($val['description'], 0, 73).'...';
$headxPhotos = null; // (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
$headmPhotos = null; //(count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';

// foreach ($val['photos'] as $kes => $item) {
//     if (key_exists('type', $item)) {
//         if (intval($item['type']) == 1) {
//             $headxPhotos = $item['large_url'];
//             if (file_exists('public/upload/image/skandal/' . $item['attachment_title'])) {
//                 $headxPhotos = $item['original_url'];
//             }

//             $headmPhotos = $item['thumb_url'];
//             break;
//         }
//     }
// }
?>


<div class="side_scandal_container">
    <div class="side_scandal_container_top">    
        <div class="left_part1">
            <div class="image_container1">
                <img class="scanimg" src="<?php echo base_url($scandalPhotos);?>">
            </div>
        </div>
        <div class="right_part1">
            <div class="scandal_title">
                <a href="<?php echo $scandalUrl;?>"><?php echo $title;?></a>
            </div>
            <div class="scandal_short_description">
                <?php echo $shortDescription;?>
            </div>
        </div>
    </div>
    <div class="side_scandal_container_bottom">    
        <div class="left_part2">
            <div class="caption1">Politisi Terkait :</div>
        </div>
        <div class="right_part2">
            <div class="thumb_list1"> 
            <?php 
            if (count($val['players']) > 0){
                echo '<ul class="ul-img-hr">';
                foreach ($val['players'] as $key => $pro){
                    if($key === $limitTerkait) break; // To limit shown "Politisi Terkait"
                    if( $pro['profile_icon_url'] != 'None'){
                        $imgPolitik = base_url($pro['profile_icon_url']);
                        if(!file_exists($imgPolitik)){
                            $imgPolitik = base_url('assets/images/icon/no-image-politisi.png');
                        }
                    }else{
                        $imgPolitik = base_url('assets/images/icon/no-image-politisi.png');
                    }

                    $temp = (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');
                    echo '<li>';
                    echo '<a href="'.base_url('aktor/profile/'.$pro['page_id']).'">';
                    echo '<img class="img-btm-border img-btm-border-'.$temp.'" title="'.$pro['page_name'].'" alt="'.$pro['page_name'].'" src="'.$imgPolitik.'"/>';
                    echo '</a>';
                    echo '</li>';
                }
                echo '</ul>';
            }else{
                echo '<span>Tidak ada politisi terkait</span>';
            }
            ?>
            </div>
        </div>
        
    </div>


</div>