<?php
$forArray = array('!', '#', '$');
$_judul = str_replace($forArray, '', $val['title']);
$skandal_url = base_url() . 'scandal/index/' . $val['scandal_id'] . '-' . urltitle($_judul);

$skandal_photos = (count($val['photos']) > 0) ? $val['photos'][count($val['photos']) - 1]['large_url'] : '';
$css_image = 'news-image-skandal';
$limit_terkait = 11;
if (isset($val['tipe'])) {
    $limit_terkait = 4;
    $css_image .= '-' . $val['tipe'];
    $skandal_photos = (count($val['photos']) > 0) ? $val['photos'][count($val['photos']) - 1]['large_url'] : '';
}

$headx_photos = null; // (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
$headm_photos = null; //(count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';

if (count($val['photos']) > 0) {
    foreach ($val['photos'] as $poto => $poto_item) {
        if (isset($poto_item['headline'])) {
            if (intval($poto_item['headline']) == 1) {
                $headx_photos = $poto_item['large_url'];
                $headm_photos = $poto_item['thumb_url'];
                break;
            }
        }
    }

    if (empty($headx_photos)) {
        foreach ($val['photos'] as $kes => $item) {
            if (key_exists('type', $item)) {
                if (intval($item['type']) == 1) {
                    $headx_photos = $item['large_url'];
                    if (file_exists('public/upload/image/skandal/' . $item['attachment_title'])) {
                        $headx_photos = $item['original_url'];
                    }

                    $headm_photos = $item['thumb_url'];
                    break;
                }
            }
        }
    }
}

?>
<div class="left_section">
    <div id="large-carousel<?php echo $val['content_id']; ?>" data-interval="3000" data-pause="hover"
         class="carousel-large carousel slide">
        <div class="carousel-inner">
            <?php $_active = '';
            // $k= ($headx_photos != null) ? 1 : 0;
            $k = 0;
            $max = 5;
            foreach ($val['photos'] as $kes => $item) {
                if ($k >= $max) break;
                $_active = $k == 0 ? 'active' : '';
                ?>
                <div class="<?php echo $_active; ?> item photo-carousel-large">
                    <?php
                    $img_uri_car = $item['large_url'];
                    if (file_exists('public/upload/image/skandal/' . $item['attachment_title'])) {
                        $img_uri_car = $item['original_url'];
                    }
                    ?>
                    <img class="lazy" alt='<?php echo $val['title']; ?>' src="<?php echo $img_uri_car;?>" class="carousel_style" style="width: 320px;height: auto;">
                </div>
                <?php $k++;
            } ?>
        </div>
        <div class="score-place score-place-overlay score" data-id="<?php echo $val['content_id']; ?>"></div>
        <div class="large-control-container">
            <div class="large-carousel-control">
                <!-- Carousel thumb -->
                <ol class="large-carousel-indicators">
                    <?php
                    $set_active = '';
                    $i = 0;
                    $maxm = 5;
                    foreach ($val['photos'] as $kem => $item) { ?>
                        <?php
                        if ($i >= $maxm) break;
                        $set_active = $i == 0 ? 'active' : '';
                        ?>
                        <li data-target="#large-carousel<?php echo $val['content_id']; ?>" data-slide-to="<?php echo $i; ?>"
                            class="<?php echo $set_active; ?>">
                            <img class="img-polaroid img-polaroid-scandal" src="<?php echo $item['thumb_url'];?>">
                        </li>
                        <?php $i++;
                    } ?>
                </ol>
            </div>
        </div>

        <div class="skandal-detail-under-slider">
            
            


        </div>

    </div>



</div>
<div class="right_section">
    <div class="row-fluid" style="margin-bottom: 10px;">
        <div class="large-carousel-title_" style="font-size: 1.5em;font-weight: bold;">
            <a href="<?php echo $skandal_url; ?>">
                <?php echo $val['title'];?>
            </a>
        </div>
    </div>
    <div class="row-fluid info1">
        <div class="info2">Status</div>
        <div class="content_info2"><?php echo ucwords($val['status']); ?></div>
    </div>
    <div class="clear"></div>
    <div class="row-fluid info1">
        <div class="info2">Kejadian</div>
        <div class="content_info2"><?php echo mdate('%d %M %Y', strtotime($val['date'])); ?></div>
    </div>
    <div class="clear"></div>
    <div class="row-fluid info1">
        <div class="info2">Dampak</div>
        <div class="content_info2"><?php echo $val['uang']; ?></div>
    </div>
    <div class="clear"></div>
    <div class="row-fluid info1">
        <div class="info2">Deskripsi</div>
        <div class="content_info2"><?php echo substr($val['description'], 0, 150).'...'; ?></div>
    </div>
    <div class="clear"></div>
    <div>
        <div class="row-fluid">
                <?php if (count($val['players']) > 0){?>
                <div class="">
                    <span><strong>&nbsp;Politisi Terkait</strong></span>
                    <?php if (count($val['players']) > 11) { ?>
                        <a class="pull-right" href="<?php echo $skandal_url; ?>">More &nbsp;</a>
                    <?php } ?>
                </div>
                <?php };?>
                <div class="" style="min-height: 48px;">
                    <?php if (count($val['players']) > 0) {
                        ?>
                        <ul class="ul-img-hr">
                            <?php foreach ($val['players'] as $key => $pro) { ?>
                                <?php if ($key === $limit_terkait) break; ?>
                                <?php
                                if ($pro['profile_icon_url'] != 'None') {
                                    $img_politik = $pro['profile_icon_url'];
                                    if (!file_exists($img_politik)) {
                                        $img_politik = 'assets/images/icon/no-image-politisi.png';
                                    }
                                } else {
                                    $img_politik = 'assets/images/icon/no-image-politisi.png';
                                }
                                ?>
                                <li class="">
                                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                        <img
                                            class="img-btm-border img-btm-border-<?php echo(key_exists('pengaruh', $pro) ? $pro['pengaruh'] : '0'); ?>"
                                            title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>"
                                            src="<?php echo base_url().$img_politik; ?>">
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } else { ?>
                        <div style="font-weight: bold;padding-top: 25px;padding-bottom: 35px;">Tidak ada politisi terkait</div>
                    <?php } ?>
                </div>

            </div>

    </div>
    <div class="time-line-content" data-cat="skandal" data-uri="<?php echo $val['content_id']; ?>" data-size="2">
        <?php //$this->load->view('template/tpl_sub_comunity_small'); ?>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.carousel-large').carousel({interval: false})
    });

</script>