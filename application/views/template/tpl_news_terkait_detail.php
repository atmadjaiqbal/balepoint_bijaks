<?php
if(!empty($result))
{
    if(!empty($result['topic_id']) && !empty($result['id']) && !empty($result['slug']))
    {
        $news_url = base_url() . "news/article/" . $result['topic_id'] . "-" . $result['id'] . "/" . trim( str_replace('/', '', $result['slug']));
    } else {
        $news_url = "";
    }

    if(!empty($result['title']))
    {
        $short_title = (strlen ($result['title']) > 23) ? substr($result['title'], 0, 23). '...' : $result['title'];
        $short_title = $result['title'];
    } else {
        $short_title = "";
    }

    if(!empty($result['image_thumbnail']))
    {
        $_image_thumb = $result['image_thumbnail'];
    } else {
        $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
    }
?>
    <div id="media-news"  class="media" style="width: 315px !important;"> <!--id="media-footer"-->
        <div class="media-news-image pull-left" style="background:
            url('<?php echo $_image_thumb; ?>') no-repeat; background-position : center; background-size:auto 55px;">

        </div>
        <div class="media-body">
            <h5><a title="<?php echo !empty($result['title']) ? $result['title'] : ''; ?>" href="<?php echo $news_url; ?>"><?php echo $short_title; ?></a></h5>
            <p class="news-date" style="margin-top:-2px;"><?php echo !empty($result['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($result['date'])) : ''; ?></p>
            <!-- div class="score-place score-place-left score" data-tipe="1" data-id="< ?php echo !empty($value['content_id']) ? $value['content_id'] : 0;?>"  style="margin-top:-17px;">< ?php echo $value['komu']; ?></div-->
        </div>
    </div>
<?php
}
?>