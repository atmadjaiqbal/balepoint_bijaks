<div class="span4">
	<div class="hp-place-news">
        <?php
        switch($category)
        {
            case 'hukum' : $_berita = 'POLITIK HUKUM'; break;
            case 'parlemen' : $_berita = 'POLITIK PARLEMEN'; break;
            case 'nasional' : $_berita = 'POLITIK NASIONAL'; break;
            case 'ekonomi' : $_berita = 'POLITIK EKONOMI'; break;
            case 'teknologi' : $_berita = 'POLITIK TEKNOLOGI'; break;
            case 'hankam' : $_berita = 'POLITIK HANKAM'; break;
            case 'daerah' : $_berita = 'POLITIK DAERAH'; break;
            case 'reses' : $_berita = 'POLITIK SENGGANG'; break;
            case 'internasional' : $_berita = 'POLITIK INTERNASIONAL'; break;
            case 'gayahidup' : $_berita = 'POLITIK GAYA HIDUP'; break;
            case 'komunitas' : $_berita = 'POLITIK SOSIAL MEDIA'; break;
            case 'lingkungan' : $_berita = 'POLITIK LINGKUNGAN'; break;
            case 'sara' : $_berita = 'POLITIK SARA'; break;
            case 'kesenjangan' : $_berita = 'POLITIK KESENJANGAN'; break;
        }
        ?>

        <div class="home-title-section hp-label hp-label-hitam">
            <span class="hitam"><a href="<?php echo base_url(); ?>news/index/<?=$category;?>"><?php echo strtoupper($_berita); ?></a></span>
        </div>

		<div <!-- class="hp-place-<--?php echo $category; ?> -->">
            <div class="hp-place-rela-con-news">
            <div class="hp-place-rela hs-place-<?php echo $category; ?>">
		<?php 
			// echo '<pre>';
			// var_dump($result);
			// echo '</pre>';
           if(!empty($result)) 
           { 
		?>
		<?php foreach ($result as $key => $value) { ?>
			<?php
                   if(!empty($value['topic_id']) && !empty($value['id']) && !empty($value['slug']))
                   {
					   $news_url = base_url() . "news/article/" . $value['topic_id'] . "-" . $value['id'] . "/" . trim( str_replace('/', '', $value['slug']));
                   } else {
                        $news_url = "";
                   }

                   if(!empty($value['title']))
                   {
					    $short_title = (strlen ($value['title']) > 23) ? substr($value['title'], 0, 23). '...' : $value['title'];
                       $short_title = $value['title'];
                   } else {
                        $short_title = ""; 
                   }

                   if(!empty($value['image_thumbnail']))
                   {
                     $_image_thumb = $value['image_thumbnail'];
                   } else {
                     $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
                   }

			 ?>
			<div id="media-news" class="media"> <!-- media-headline -->
				<div class="media-news-image pull-left" style="background:url('<?php echo $_image_thumb; ?>') no-repeat; background-position : center; background-size:auto 55px; /* 80px */"></div>
			  	<div class="media-body">
			    	<h5><a title="<?php echo !empty($value['title']) ? $value['title'] : ''; ?>" href="<?php echo $news_url; ?>"><?php echo $short_title; ?></a></h5>
                    <p class="news-date" style="margin-top:-2px;"><?php echo !empty($value['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($value['date'])) : ''; ?></p>
			  	    <!--div class="score-place score-place-left score" data-tipe="1" data-id="<?php //echo !empty($value['content_id']) ? $value['content_id'] : 0;?>"  style="margin-top:-17px;"></div-->
                 </div>
			</div>
            <!--div class="div-line-small-2"></div-->
           <?php } ?>
        
		<?php } ?>
            </div>
            </div>
        </div>
        


    </div>
    <div class="row-fluid">
        <div class="span6 text-left">
            <a data-holder="hs-place-<?php echo $category; ?>" data-tipe="1" class="btn btn-mini news-next-list">5 Berikutnya</a>
        </div>
        <div class="span6 text-right">
            <a href="<?php echo base_url(); ?>news/index/<?php echo $category; ?>" class="btn btn-mini">Selengkapnya</a>
        </div>
    </div>
    <p></p>
    <!-- div class="hp-footer last-hitam"></div-->
</div>
