<div class="hp-place-news"> <!-- class="hp-place-< ?php echo $category; ?>"-->
    <div class="hp-place-rela-con-news">
        <div class="hp-place-rela hs-place-<?php echo $category; ?>">
            <?php
            // echo '<pre>';
            // var_dump($result);
            // echo '</pre>';
            if(!empty($result))
            {
                ?>
                <?php foreach ($result as $key => $value) { ?>
                <?php
                if(!empty($value['topic_id']) && !empty($value['id']) && !empty($value['slug']))
                {
                    $news_url = base_url() . "news/article/" . $value['topic_id'] . "-" . $value['id'] . "/" . trim( str_replace('/', '', $value['slug']));
                } else {
                    $news_url = "";
                }

                if(!empty($value['title']))
                {
                    $short_title = (strlen ($value['title']) > 23) ? substr($value['title'], 0, 23). '...' : $value['title'];
                    $short_title = $value['title'];
                } else {
                    $short_title = "";
                }

                if(!empty($value['image_thumbnail']))
                {
                    $_image_thumb = $value['image_thumbnail'];
                } else {
                    $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
                }

                ?>
                <div id="media-news"  class="media"> <!--id="media-footer"-->
                    <div class="media-news-image pull-left" style="background:
                        url('<?php echo $_image_thumb; ?>') no-repeat; background-position : center; background-size:auto 55px;">

                    </div>
                    <div class="media-body">
                        <h5><a title="<?php echo !empty($value['title']) ? $value['title'] : ''; ?>" href="<?php echo $news_url; ?>"><?php echo $short_title; ?></a></h5>
                        <p class="news-date" style="margin-top:-2px;"><?php echo !empty($value['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($value['date'])) : ''; ?></p>
                        <!-- div class="score-place score-place-left score" data-tipe="1" data-id="< ?php echo !empty($value['content_id']) ? $value['content_id'] : 0;?>"  style="margin-top:-17px;">< ?php echo $value['komu']; ?></div-->
                    </div>
                </div>
                <!-- div class="div-line-small-2"></div -->
            <?php } ?>

            <?php } ?>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {

    });

</script>

