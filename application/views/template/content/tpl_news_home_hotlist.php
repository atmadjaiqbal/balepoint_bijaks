<div style="margin-top:10px;">
<?php
if(!empty($topnews))
{
    $_topic_id = !empty($topnews['topic_id']) ? $topnews['topic_id'] : '';
    $_id = !empty($topnews['id']) ? $topnews['id'] : '';
    $_slug = !empty($topnews['slug']) ? $topnews['slug'] : '';
    $_title = !empty($topnews['title']) ? $topnews['title'] : '';

    $news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
    $short_title = (strlen ($_title) > 38) ? substr($_title, 0, 38). '...' : $_title;
    if(!empty($topnews['image_thumbnail']))
    {
        $_image_thumb = $topnews['image_thumbnail'];
    } else {
        $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
    }
?>
    <div class="row-fluid" style="height: 190px;">
        <div style="float:left;width:300px;height: 170px;">
            <a href="<?php echo $news_url;?>"><img style="width:300px;height: 168px;" src="<?php echo $_image_thumb; ?>" class="lazy"></a>
            <div class="score-place score-place-overlay score" data-id="<?php echo !empty($topnews['content_id']) ? $topnews['content_id'] : 0;?>"><?php echo $topnews['komu']; ?></div>
        </div>
        <div style="float:left;width: 320px;height: 170px;margin-left: 10px;">
            <h5 class="media-heading"><a href="<?php echo $news_url;?>"><?php echo $_title; ?></a></h5>
            <p class="news-date"><?php echo !empty($topnews['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($topnews['date'])) : ''; ?></p>
            <p style="line-height: 18px;margin-top: -10px;"><?php echo substr(!empty($topnews['excerpt']) ? $topnews['excerpt'] : '',0, 240) . ' ... ';?><a href="<?php echo $news_url;?>">selengkapnya</a></p>
        </div>

    </div>
<?php
}
?>
    <div class="row-fluid" style="margin-top:-15px;">
<?php
        $news_arr = array();
        if(!empty($morenews))
        {
          $_i = 1;
          $_news_pilihan01 = ''; $_news_pilihan02 = ''; $_news_pilihan03 = ''; $_news_pilihan04 = '';
          $_news_pilihan05 = ''; $_news_pilihan06 = ''; $_news_pilihan07 = ''; $_news_pilihan08 = '';
          $_news_pilihan09 = ''; $_news_pilihan10 = ''; $_news_pilihan11 = ''; $_news_pilihan12 = '';
          foreach($morenews as $key=>$result)
          {
             $more_topic_id = !empty($result['topic_id']) ? $result['topic_id'] : '';
             $more_id = !empty($result['id']) ? $result['id'] : '';
             $more_slug = !empty($result['slug']) ? $result['slug'] : '';
             $more_title = !empty($result['title']) ? $result['title'] : '';

             $more_news_url = base_url() . "news/article/" . $more_topic_id . "-" . $more_id . "/" . trim( str_replace('/', '', $more_slug));
             $more_short_title = (strlen ($more_title) > 38) ? substr($more_title, 0, 38). '...' : $more_title;
             if(!empty($result['image_thumbnail']))
             {
                $more_image_thumb = $result['image_thumbnail'];
             } else {
                $more_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
             }

             $carousel_news_item = '
                   <a title="'.(!empty($result['title']) ? $result['title'] : '').'" href="'.$more_news_url.'">
                     <div class="row-fluid" style="text-align:center;">
                       <img src="'.$more_image_thumb.'" class="lazy">
                     </div>
                     <div class="row-fluid" style="margin-top: 15px;">
                        <h5 style="margin-left:5px;">'.$more_short_title.'</h5>
                        <p class="news-date" style="margin-left:5px;line-height: 14px;">'.(!empty($result['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($result['date'])) : '').'</p>
                     </div>
                   </a>';

             if($_i == 1) $_news_pilihan01 = $carousel_news_item; if($_i == 2) $_news_pilihan02 = $carousel_news_item; if($_i == 3) $_news_pilihan03 = $carousel_news_item;
             if($_i == 4) $_news_pilihan04 = $carousel_news_item; if($_i == 5) $_news_pilihan05 = $carousel_news_item;
             $_i++;
          }

?>
        <div>
            <div id="media-content"><?php echo $_news_pilihan01; ?></div>
            <div id="media-content"><?php echo $_news_pilihan02; ?></div>
            <div id="media-content"><?php echo $_news_pilihan03; ?></div>
            <div id="media-content"><?php echo $_news_pilihan04; ?></div>
        </div>
<?php
        }
?>
   </div>
</div>
