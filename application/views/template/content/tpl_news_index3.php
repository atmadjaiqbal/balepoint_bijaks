<?php
if(!empty($news_detail)){
    // echo '<pre>';print_r($news_detail);echo '</pre>';
    $contentId = !empty($news_detail['content_id']) ? $news_detail['content_id'] : 0;
    $id = $news_detail['id'];
    $news_url = base_url() . "news/article/" . $news_detail['topic_id'] . "-" . $news_detail['id'] . "/" . trim( str_replace('/', '', $news_detail['slug']));
    $short_title = (strlen ($news_detail['title']) > 50) ? substr($news_detail['title'], 0, 50). '...' : $news_detail['title'];
    $title_2_line = (strlen($news_detail['title']) < 36 ) ? 'news-index-title-2-line' : '';
    $short_title = $news_detail['title'];
    $newsDate = !empty($news_detail['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($news_detail['date'])) : '';
    $description = !empty($news_detail['content_text']) ? substr($news_detail['content_text'],0,250).'...' : '';

    $newsImage = $news_detail['image_thumbnail'];

    $newsTitle = $news_detail['title'];

?>
    <div class="spanbox">
<div class="news_image">
    <img class="news-image lazy" alt='<?php echo $newsTitle; ?>' data-original="<?php echo $newsImage; ?>" src="<?php echo $newsImage; ?>">
    <div class="score-place score-place-overlay score<?php echo $id;?>" data-id="<?php echo $news_detail['content_id']; ?>" ></div>
    <div class="news_politisi_terkait">
        <?php
        if (count($news_detail['news_profile']) > 0){?>

            <div><b>Politisi Terkait</b></div>
            <?php
            foreach ($news_detail['news_profile'] as $key => $pro) {
                if($key === 4) break;
                $img2Url = base_url('aktor/profile/'.$pro['page_id']);
                $img_pola = key_exists('score', $pro) ? 'img-btm-border-'.$pro['score'] : '';

                ?>
                <div class="polter">
                    <a href="<?php echo $img2Url;?>">
                        <img class="img-btm-border <?php echo $img_pola;?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                    </a>
                </div>
            <?php };?>


        <?php }else{
            echo '<b>Tidak ada politisi terkait</b>';
        };?>
    </div>
<!--    <div class="score" data-id="--><?php //echo $news_detail['content_id']; ?><!--" >-->
</div>
<div class="news_details">
    <div class="news_title3">
        <a href="<?php echo $news_url; ?>">
            <?php echo $short_title; ?>
        </a>
        <p class="news-index-date" style=""><?php echo (!empty($news_detail['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($news_detail['date'])) : ''); ?></p>
    </div>
    <div class="news_description">
        <?php echo $description;?>
    </div>

    <?php //if(!empty($category)){?>
    <div style="clear: both;"></div>
    <div class="news_last_activity">
        <div class="time-line-content" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo $news_detail['content_id']; ?>"></div>
    </div>
    <?php //};?>
</div>
</div>

<?php
}
?>
<?php /*
<!-- <div class="span4"> -->
	<?php
    if(!empty($news_detail))
    {
			$news_url = base_url() . "news/article/" . $news_detail['topic_id'] . "-" . $news_detail['id'] . "/" . trim( str_replace('/', '', $news_detail['slug']));
			$short_title = (strlen ($news_detail['title']) > 50) ? substr($news_detail['title'], 0, 50). '...' : $news_detail['title'];
            $title_2_line = (strlen($news_detail['title']) < 36 ) ? 'news-index-title-2-line' : '';

            $short_title = $news_detail['title'];
	 ?>

        <h4 class="news-index-title <?=$title_2_line;?>" title="<?php echo $news_detail['title']; ?>"><a href="<?php echo $news_url; ?>"><?php echo $short_title; ?></a></h4>

        <p class="news-index-date" style=""><?php echo (!empty($news_detail['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($news_detail['date'])) : ''); ?></p>
	<img class="news-image lazy" alt='<?php echo $news_detail['title']; ?>' data-original="<?php echo $news_detail['image_thumbnail']; ?>">
<div class="score-place score-place-overlay score" data-id="<?php echo $news_detail['content_id']; ?>" >
</div>
	<div class="row-fluid politisi-terkait-container">
		<div class="span2"><span>Politisi Terkait</span></div>
		<?php if (count($news_detail['news_profile']) > 0){ ?>
		<?php foreach ($news_detail['news_profile'] as $key => $pro) { ?>
            <?php
            $img_pola = key_exists('score', $pro) ? 'img-btm-border-'.$pro['score'] : '';
            ?>
			<?php if($key === 4) break; ?>
			<div class="span2">
                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
				<img class="img-btm-border <?php echo $img_pola;?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
			    </a>
            </div>
		<?php } ?>
			<?php if (count($news_detail['news_profile']) > 4){ ?>
			<div class="span2">
				<span>More</span>
			</div>
			<?php } ?>
		<?php }else{ ?>
		<div class="span6"><span>Tidak ada politisi terkait</span></div>
		<?php } ?>
		
	</div>
	<div class="time-line-content" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo $news_detail['content_id']; ?>">
		
	</div>

<?php
    }
?>
<!-- </div> -->
*/?>

<script type="text/javascript">
$(document).ready(function(){
    $('.score<?php echo $id;?>').each(function(a, b){
            var id = $(b).data('id');
            var tipe = '0';
            if($(b).data('tipe')){
                tipe = $(b).data('tipe');
            }
            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
        });



    $(".score<?php echo $id;?>").on('click', ".score-btn", function(e){
            e.preventDefault();
            var thi = $(this);
    //        var parent = thi.closest('.score');
            var tipe = $(this).data('tipe');
            if(tipe != 'comment'){
                var id = $(this).data('id');
                var loader = $(this).parent().parent().prev('.loader');
                $(loader).css("visibility", "visible");
                $.ajax({
                    url : Settings.base_url+'timeline/content_vote',
                    type : 'POST',
                    data: {'id': id, 'tipe': tipe},
                    dataType: "json",
                    success: function(data)
                    {

                        if(data.rcode == 'ok'){
    //                        var exist_score = thi.children('span').text();
    //                        console.debug();
                            if(tipe == 'like'){
                                thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                                thi.children('span').text(data.msg[1].total);
                                thi.parent().next().children('a').children('span').text(data.msg[2].total);
                            }else{
                                thi.children('span').text(data.msg[2].total);
                                thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                                thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                            }

                            $('.score').each(function(a, b){
                                var id = $(b).data('id');
                                var tipe = '0';
                                if($(b).data('tipe')){
                                    tipe = $(b).data('tipe');
                                }
                                $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                            });

                        }else{
                            alert(data.msg)
                        }

                        $(loader).css("visibility", "hidden");
                    }
                });
            }

        });

    $(".score<?php echo $id;?>").on('click', ".score-btn-simple", function(e){
            e.preventDefault();
            var thi = $(this);
    //        var parent = thi.closest('.score');
            var tipe = $(this).data('tipe');
            if(tipe != 'comment'){
                var id = $(this).data('id');
                var loader = $(this).parent().parent().prev('.loader');
                $(loader).css("visibility", "visible");
                $.ajax({
                    url : Settings.base_url+'timeline/content_vote',
                    type : 'POST',
                    data: {'id': id, 'tipe': tipe},
                    dataType: "json",
                    success: function(data)
                    {

                        if(data.rcode == 'ok'){
    //                        var exist_score = thi.children('span').text();
    //                        console.debug();
                            if(tipe == 'like'){
                                thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                                thi.children('span').text(data.msg[1].total);
                                thi.parent().next().children('a').children('span').text(data.msg[2].total);
                            }else{
                                thi.children('span').text(data.msg[2].total);
                                thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                                thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                            }

                            $('.score').each(function(a, b){
                                var id = $(b).data('id');
                                var tipe = '0';
                                if($(b).data('tipe')){
                                    tipe = $(b).data('tipe');
                                }
                                $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                            });

                        }else{
                            alert(data.msg)
                        }

                        $(loader).css("visibility", "hidden");
                    }
                });
            }

        });
    
})


</script>