<div style="margin-top:10px;margin-bottom: 5px;">
    <div id="reses-carousel" class="row-fluid slide" style="height: 324px;">
        <div class="carousel-inner">

<?php
     if($result)
     {
        $y = 1; $_reses_arr = array();
        $news_reses01 = ''; $news_reses02 = ''; $news_reses03 = ''; $news_reses04 = '';
        $news_reses05 = ''; $news_reses06 = ''; $news_reses07 = ''; $news_reses08 = '';
        $news_reses09 = ''; $news_reses10 = ''; $news_reses11 = ''; $news_reses12 = '';
        $news_reses13 = ''; $news_reses14 = ''; $news_reses15 = ''; $news_reses16 = '';

        foreach($result as $key => $rwPilih)
        {
           $_topic_id = !empty($rwPilih['topic_id']) ? $rwPilih['topic_id'] : '';
           $_id = !empty($rwPilih['id']) ? $rwPilih['id'] : '';
           $_slug = !empty($rwPilih['slug']) ? $rwPilih['slug'] : '';
           $_title = !empty($rwPilih['title']) ? $rwPilih['title'] : '';

           $_news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
           $_short_title = (strlen ($_title) > 38) ? substr($_title, 0, 38). '...' : $_title;
           if(!empty($rwPilih['image_thumbnail'])) { $_image_thumb = $rwPilih['image_thumbnail']; } else { $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg'; }

            $carousel_reses = '
                   <a title="'.(!empty($rwPilih['title']) ? $rwPilih['title'] : '').'" href="'.$_news_url.'">
                     <div class="row-fluid">
                       <img src="'.$_image_thumb.'" class="lazy">
                     </div>
                     <div class="row-fluid" style="margin-top: 40px;">
                        <div class="score-place score-place-overlay score" data-tipe="1" data-id="'.(!empty($rwPilih['content_id']) ? $rwPilih['content_id'] : 0).'"></div>
                        <p class="news-date" style="margin-left:5px;">'.(!empty($rwPilih['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($rwPilih['date'])) : '').'</p>
                        <h5 style="margin-left:5px;">'.$_title.'</h5>
                     </div>
                   </a>';

            if($y == 1) $news_reses01 = $carousel_reses; if($y == 2) $news_reses02 = $carousel_reses; if($y == 3) $news_reses03 = $carousel_reses; if($y == 4) $news_reses04 = $carousel_reses;
            if($y == 5) $news_reses05 = $carousel_reses; if($y == 6) $news_reses06 = $carousel_reses; if($y == 7) $news_reses07 = $carousel_reses; if($y == 8) $news_reses08 = $carousel_reses;
            if($y == 9) $news_reses09 = $carousel_reses; if($y == 10) $news_reses10 = $carousel_reses; if($y == 11) $news_reses11 = $carousel_reses; if($y == 12) $news_reses12 = $carousel_reses;
            if($y == 13) $news_reses13 = $carousel_reses; if($y == 14) $news_reses14 = $carousel_reses; if($y == 15) $news_reses15 = $carousel_reses; if($y == 16) $news_reses16 = $carousel_reses;
            $y++;
        }
?>
            <div class="item active" style="margin-left: 5px;width: 633px;">
                <div id="media-content" class="reses-slice"><?php echo $news_reses01; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses02; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses03; ?></div>
                <div id="media-content" class="reses-slice" style="border-right: none;"><?php echo $news_reses04; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses05; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses06; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses07; ?></div>
                <div id="media-content" class="reses-slice" style="border-right: none;"><?php echo $news_reses08; ?></div>
            </div>
            <div class="item" style="margin-left: 5px;width: 633px;">
                <div id="media-content" class="reses-slice"><?php echo $news_reses09; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses10; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses11; ?></div>
                <div id="media-content" class="reses-slice" style="border-right: none;"><?php echo $news_reses12; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses13; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses14; ?></div>
                <div id="media-content" class="reses-slice"><?php echo $news_reses15; ?></div>
                <div id="media-content" class="reses-slice" style="border-right: none;"><?php echo $news_reses16; ?></div>
            </div>
<?php
     }
?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var imgCount = 1;
        $('#reses-carousel').carousel({ interval:false });

        $('#reses-carousel').bind('slide',function(en){
            console.debug(en);
            if(en.direction == 'left'){
                $('#button_reses_left').data('slide', 'prev')
                imgCount--;
                if(imgCount == 0){
                    $('#button_reses_right').data('slide', '');
                    $('#button_reses_left').data('slide', 'prev');
                }
            }else{
                $('#button_reses_right').data('slide', 'next');
                imgCount++;
                if(imgCount == 1){
                    $('#button_reses_left').data('slide', '');
                    $('#button_reses_right').data('slide', 'next');
                }

            }

        });
    });
</script>

