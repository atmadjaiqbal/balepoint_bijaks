<!--- ###### MODAL #######-->
<div id="modal_report" class="modal hide fade">
    <div class="modal-body">
        <div class="row-fluid">
            <h4>Kirim pesan sebagai spam.</h4>
            <form id="submit_report">
                <input name="tipe" type="hidden" value="" id="tipe">
                <input name="content_id" type="hidden" value="" id="cid">
                <input id="report_message" type="text" name="report_message" class="media-input comment_type span12">

                <div class="pull-right">
                    <input type="reset" id="reste" value="Batal" class="btn-flat btn-flat-gray">
                    <input type="submit" id="report" value="Kirim" class="btn-flat btn-flat-gray">
                </div>

            </form>
        </div>
    </div>
</div>