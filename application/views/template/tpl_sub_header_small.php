<div class="sub-header-container sub-header-container-fix-height" style="margin-top: 31px;">
    <div class='sub-header-container-bg-image' style="background: url(<?php echo $skandal_photos; ?>) no-repeat; background-size: 960px auto; "></div>
    <div class="row-fluid">
        <div class="logo-small-container">
            <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
        </div>
        <div class="right-container">
            <div class="banner">

            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-left category-news-title-<?php echo category_color(strtolower($category)); ?>">
                    <h1><?php echo strtoupper($category); ?></h1>
                </div>

            </div>
        </div>
    </div>
    <div class='sub-header-container-bottom-status'>
        <p>STATUS : <?php echo strtoupper($status).' | KERUGIAN NEGARA : '.strtoupper($dampak); ?></p>
    </div>

</div>
