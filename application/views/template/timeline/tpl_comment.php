<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.timeline.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>

    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

<script type="text/javascript">
    $(function(){
        console.debug($(window).parent());
        var id = '<?=$id;?>';
        $('#comment').load('/timeline/content_comment/'+id);
    });
</script>
</head>
<body>
<div class="media media-comment">
    <div class="pull-left media-side-left">
        <div style="background:
                url('<?php //echo $comment['image_url']; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

        </div>
    </div>

    <div class="media-body">
        <input type="text" class="media-input" >
    </div>
</div>

<div id="comment">

</div>
</body>
</html>