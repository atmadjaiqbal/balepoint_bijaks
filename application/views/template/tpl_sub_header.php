<div class="sub-header-container" style="margin-top: 31px;">
	
	<div class="logo-small-container">
		<img src="<?php echo base_url('assets/images/bijaks_logo.png'); ?>" >
	</div>

	<div class="right-container" style="background-color: #ffffff;">
		<div class="banner">
			<img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
		</div>
		<div class="category-news">
            <!-- category-news-title-< ?php echo category_color(strtolower($category)); ?> -->
			<div class="category-news-title category-news-title-right"  style="background: none;">
				<h1><?php

                    switch(strtoupper($category))
                    {
                        case 'HOT-PROFILE' :
                            echo 'PROFILE'; break;
                        case 'SUKSESI' :
                            echo 'SUKSESI DAN SURVEY'; break;
                        case 'SURVEY' :
                            echo 'POLLING KOMUNITAS'; break;
                        case 'TOPTEN' :
                            echo 'BIJAKS TOP 10'; break;
                        case 'PARLEMEN' :
                            echo 'POLITIK PARLEMEN'; break;
                        case 'EKONOMI' :
                            echo 'POLITIK EKONOMI'; break;
                        case 'HUKUM' :
                            echo 'POLITIK HUKUM'; break;
                        case 'NASIONAL' :
                            echo 'POLITIK NASIONAL'; break;
                        case 'DAERAH' :
                            echo 'POLITIK DAERAH'; break;
                        case 'INTERNASIONAL' :
                            echo 'POLITIK INTERNASIONAL'; break;
                        case 'LINGKUNGAN' :
                            echo 'POLITIK LINGKUNGAN'; break;
                        case 'KESENJANGAN' :
                            echo 'POLITIK KESENJANGAN'; break;
                        case 'SARA' :
                            echo 'POLITIK SARA'; break;
                        case 'KOMUNITAS' :
                            echo 'POLITIK SOSIAL MEDIA'; break;
                        case 'GAYAHIDUP' :
                            echo 'POLITIK GAYA HIDUP'; break;
                        case 'RESES' :
                            echo 'POLITIK SENGGANG'; break;
                        case 'HEADLINE' :
                            echo 'TAJUK UTAMA'; break;
                        case 'TERKINI' :
                            echo 'TAJUK UTAMA'; break;
                        case 'INDOBARAT' :
                            echo 'POLITIK INDONESIA BARAT'; break;
                        case 'INDOTIM' :
                            echo 'POLITIK INDONESIA TIMUR'; break;
                        case 'INDOTENG' :
                            echo 'POLITIK INDONESIA TENGAH'; break;

                        default :
                            echo strtoupper($category); break;
                    }
                    ?></h1>
			</div>
			
		</div>
	</div>
	
</div>
<div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
<div id='disclaimer' class="sub-header-container"></div>
