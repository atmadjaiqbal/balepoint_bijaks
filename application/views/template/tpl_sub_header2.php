<?php
switch(strtoupper($category)){
    case 'HOT-PROFILE' :
        $topLogo = base_url('assets/images/headers/HEADER-18.jpg');
        break;

    // case 'SUKSESI' :
    //     $topTitling = 'SUKSESI DAN SURVEY';
    //     $topLogo = base_url('assets/images/profile_logo.png');
    //     break;
    // case 'SURVEY' :
    //     $topTitling = 'POLLING KOMUNITAS';
    //     $topLogo = base_url('assets/images/profile_logo.png');
    //     break;
    case 'TOPTEN' :
        $topLogo = base_url('assets/images/headers/HEADER-17.jpg');
        break;
    case 'PARLEMEN' :
        $topLogo = base_url('assets/images/headers/HEADER-11.jpg');

        break;
    case 'EKONOMI' :
        $topLogo = base_url('assets/images/headers/HEADER-12.jpg');

        break;
    case 'HUKUM' :
        $topLogo = base_url('assets/images/headers/HEADER-13.jpg');
        break;
    case 'NASIONAL' :
        $topLogo = base_url('assets/images/headers/HEADER-14.jpg');
        break;
    case 'DAERAH' :
        $topLogo = base_url('assets/images/headers/HEADER-16.jpg');
        break;
    case 'INTERNASIONAL' :
        $topLogo = base_url('assets/images/headers/HEADER-10.jpg');
        break;
    case 'LINGKUNGAN' :
        $topLogo = base_url('assets/images/headers/HEADER-6.jpg');
        break;
    case 'KESENJANGAN' :
        $topLogo = base_url('assets/images/headers/HEADER-5.jpg');
        break;
    case 'SARA' :
        $topLogo = base_url('assets/images/headers/HEADER-4.jpg');
        break;
    case 'KOMUNITAS' :
        $topLogo = base_url('assets/images/headers/HEADER-3.jpg');
        break;
    case 'GAYAHIDUP' :
        $topLogo = base_url('assets/images/headers/HEADER-2.jpg');
        break;
    case 'RESES' :
        $topLogo = base_url('assets/images/headers/HEADER-1.jpg');
        break;
    case 'HEADLINE' :
        $topLogo = base_url('assets/images/headers/HEADER-15.jpg');
        break;
    case 'TERKINI' :
        $topLogo = base_url('assets/images/headers/HEADER-19.jpg');
        break;
    case 'INDOBARAT' :
        $topLogo = base_url('assets/images/headers/HEADER-9.jpg');
        break;
    case 'INDOTIM' :
        $topLogo = base_url('assets/images/headers/HEADER-7.jpg');
        break;
    case 'INDOTENG' :
        $topLogo = base_url('assets/images/headers/HEADER-8.jpg');
        break;
    default :
        $topLogo = base_url('assets/images/headers/HEADER-default.jpg');
}

?>
<style type="text/css">
#topside_header{
    background-image: url('<?php echo $topLogo;?>');
    background-repeat: no-repeat;
    height: 150px;
}    
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.carousel.css')?>" />
<script type="text/javascript" language="javascript" src="<?php echo base_url('assets/js/owl.carousel.min.js')?>" ></script>
<div class="sub-header-container" style="margin-top: 28px;">

    <div id="topside_header">
        <a href="<?php echo base_url();?>">
            <img src="<?php echo base_url('assets/images/bijaks_logo.png'); ?>" style="z-index: 99;margin-left: 33px;margin-top:23px;width: 158px;opacity: 0.8;"/>        
        </a>
    </div>

    <div style="clear: both"></div>
    <?php /*
    <div id="heading_carousel">
        <div id="hot_profiles_carousel" class="owl-carousel">
            <?php
            foreach($hot_profile as $k=>$v){
                $thumbUrl = $v['profile_photo'][0]['thumb_url'];
                $pageName = $v['page_name'];
                $pageId = $v['page_id'];
            ?>
                <div class="hot_profile">
                    <div class="imageHolder">
                        <a href="<?php echo base_url('aktor/profile/'.$pageId); ?>">
                            <img src="<?php echo $thumbUrl;?>" class="lazyOwl">
                        </a>
                        <div class="caption"><br><?php echo $pageName;?></div>
                    </div>

                </div>

            <?php
            }
            ?>
        </div>
    </div>
    */?>
</div>
<!-- <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div> -->

<div class="sub-header-container">
    <?php $this->timeline->sub_header('bijak'); ?>
</div>

<!--<div id='disclaimer' class="sub-header-container"></div>-->
<script type="text/javascript">
    $("#hot_profiles_carousel").owlCarousel({

        autoPlay: 1000, //Set AutoPlay to 3 seconds
        navigation: true,
        items : 5,
        itemsDesktop : [1199,5],
        itemsDesktopSmall : [979,5],
        navigationText: [
            "<i >></i>",
            "<i ><</i>"
        ],

    });

    $(document).ready(function(){
        $("#hot_profiles_carousel .owl-controls .owl-nav .owl-prev").html('<img src="<?php echo base_url('assets/images/left_transparent_control.png');?>"/>')
        $("#hot_profiles_carousel .owl-controls .owl-nav .owl-next").html('<img src="<?php echo base_url('assets/images/right_transparent_control.png');?>"/>')


    })
</script>
