<div class="col-3 social last-<?php echo $line['type']; ?>">
    <div class="media">
        <div style="background:url('<?php echo $line['image_url']; ?>') no-repeat;" class="pull-left circular">
            <img class="media-object" src="<?php echo $line['image_url']; ?>">
        </div>
        <!-- <a href="#" class="pull-right"> -->
        <img class="pull-right" style="margin-right:5px;margin-top:2px;width: 25px;height: 25px;" src="<?php echo base_url() ?>assets/images/<?php echo $line['type']; ?>_icon_large_new.png">
        <div style="font-size: 10px;margin-bottom: 1px;margin-top: 26px;float: right;margin-right: -37px;">
            <?php
            if(!empty($line['entry_date'])){
                echo time_passed($line['entry_date']);
            };?>
        </div>
        <!-- </a> -->
        <div class="media-body">
            <strong class="media-heading"><a href="<?php echo base_url('komunitas/profile/'.$line['page_id']); ?>"><?php echo $line['page_name']; ?></a></strong><br>
            <a class="" title="<?php echo $line['title']; ?>" href="<?php echo $line['news_url'];?>"><?php echo (strlen($line['title']) > 26) ? substr($line['title'], 0, 24). '...' : $line['title']; ?></a>
        </div>
    </div>
</div>
