<?php foreach($suksesi as $val){ ?>
<?php
$suksesi_url = base_url() . 'suksesi/index/'.$val['id_race'].'-'.urltitle($val['race_name']);
$map_photo = '';
$logo_photo = '';
foreach($val['photos'] as $foto){
    if(intval($foto['type']) === 2){
        $map_photo = $foto['large_url'];
    }elseif(intval($foto['type']) === 1){
        $logo_photo = $foto['thumb_portrait_url'];
    }
}
$status_suksesi = 'Survey / Prediksi';
$index_trace_status = 0;
foreach($val['status'] as $idx => $status){
    if(intval($status['draft']) == 1){
        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
        $index_trace_status = $idx;
    }
}

$total_kandidat = count( $val['status'][$index_trace_status]['kandidat'] );
$tabSection = "section-tab-".$val['id_race'];
?>
<div class="row-fluid">
    <div class="span12">
        <div class="row-fluid">
            <div class="span3 suksesi-img-logo">
                <img src="<?php echo $logo_photo; ?>">
            </div>
            <div class="span9">
                <a href="<?php echo $suksesi_url; ?>"><h4 title="<?php echo $val['race_name']; ?>"><?php echo (strlen($val['race_name']) > 40) ? substr($val['race_name'], 0, 40).'...' : $val['race_name']; ?></h4></a>
                <br>
                <br>
                <div class="score-place score-place-overlay score" data-id="<?php echo $val['content_id']; ?>" >
                    <?=$val['last_score'];?>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <table class="table">
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Daftar pemilih tetap</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo $val['daftar_pemilih_tetap']; ?> Jiwa</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Jumlah kandidat</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo $total_kandidat; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Tanggal pelaksanaan</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo mdate('%d %M %Y', strtotime($val['tgl_pelaksanaan'])); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Status</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo $status_suksesi; ?></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>

</div>
<?php } ?>