<?php $this->load->view($template_path .'tpl_header');?>

<section class="page-container page-boxed">

	<!-- ROW 1 BERITA_TERAKHIR, HOT_ISSUE, ACTIVITY-->
	<div class="row-fluid">
		<article class="span4 data-block">
			<div class="data-container">
            <header>
					<h3>Berita Terakhir</h3>
				</header>
            <?php $this->load->view($template_path .'blocks/tpl_block_news_list');?>						
			</div>
		</article>

		<article class="span4 data-block">
			<div class="data-container">
				<header><h3>Hot Issue</h3></header>
            <?php $this->load->view($template_path .'blocks/tpl_block_news_list');?>						
			</div>
		</article>

		<article class="span4 data-block">
			<div class="data-container black">
				<header>
					<h3>Activity</h3>
				</header>
            <?php $this->load->view($template_path .'blocks/tpl_block_activity');?>						
			</div>
		</article>
	</div>


	<!-- ROW 2 HOT PROFILE-->
	<div class="row-fluid">
		<article class="span12 data-block ">
			<div class="data-container blue">
				<header><h2>Hot Profile</h2></header>
				<section>
					<p>
					   Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
					</p>
				</section>
			</div>
		</article>
	</div>


	<!-- ROW 3 SKANDAL, RESES-->
	<div class="row-fluid">
		<article class="span8 data-block">
			<div class="data-container purple">
				<header><h2>Skandal</h2></header>
				<section>
            	<div class="row-fluid">
            		<article class="span6 data-block">
            			<div class="data-container black" style="padding-left:0px">
                           <?php $this->load->view($template_path .'blocks/tpl_block_activity');?>						
            			</div>
            		</article>
            
            
            		<article class="span6 data-block">
            			<div class="data-container black" >
                           <?php $this->load->view($template_path .'blocks/tpl_block_activity');?>						
            			</div>
            		</article>
            	</div>
				</section>
			</div>
		</article>


		<article class="span4 data-block">
			<div class="data-container">
				<header>
					<h3>Reses</h3>
				</header>
				<section>
					<p>
					   Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
					</p>
				</section>
			</div>
		</article>
	</div>

	<!-- ROW 4 EKONOMI, HUKUM, PARLEMEN-->
	<div class="row-fluid">
		<article class="span4 data-block">
			<div class="data-container">
				<header><h2>Ekonomi</h2></header>
            <?php $this->load->view($template_path .'blocks/tpl_block_news_list');?>						
			</div>
		</article>
		
		<article class="span4 data-block">
			<div class="data-container">
				<header><h2>Hukum</h2></header>
            <?php $this->load->view($template_path .'blocks/tpl_block_news_list');?>						
			</div>
		</article>	
		
		<article class="span4 data-block">
			<div class="data-container">
				<header><h2>Parlemen</h2></header>
				<?php $this->load->view($template_path .'blocks/tpl_block_news_list');?>						
			</div>
		</article>							
   </div>

	<!-- ROW 5 SUKSESI-->
	<div class="row-fluid">
		<article class="span12 data-block">
			<div class="data-container">
				<header><h2>Suksesi</h2></header>
				<section>

            	<div class="span12">
            		<article class="span6 data-block">
            			<div class="data-container black" style="padding-left:0px;">
            			   <section>
            			      aaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaa
                           <?php $this->load->view($template_path .'blocks/tpl_block_activity');?>						
                        </section>
            			</div>
            		</article>
            
            
            		<article class="span6 data-block">
            			<div class="data-container black">
            			   <section>
                           <?php $this->load->view($template_path .'blocks/tpl_block_activity');?>						
                        </section>
            			</div>
            		</article>
            	</div>

				</section>
			</div>
		</article>
   </div>
   
	<!-- ROW 6 BLOG TERKINI, BLOG FAVORIT, LATEST KOMENT-->
	<div class="row-fluid">
		<article class="span4 data-block">
			<div class="data-container blue">
				<header><h2>Blog Terkini</h2></header>
				<?php $this->load->view($template_path .'blocks/tpl_block_news_list');?>						
			</div>
		</article>

		<article class="span4 data-block">
			<div class="data-container yellow">
				<header><h2>Blog Favorit</h2></header>
            <?php $this->load->view($template_path .'blocks/tpl_block_news_list');?>			
			</div>
		</article>


		<article class="span4 data-block">
			<div class="data-container green">
				<header>
					<h3>Last KOment</h3>
				</header>
            <?php $this->load->view($template_path .'blocks/tpl_block_activity');?>												
			</div>
		</article>

	</div>

</section>

<?php $this->load->view($template_path .'tpl_footer');?>