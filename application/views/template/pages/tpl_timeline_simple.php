<?php $this->load->view($template_path .'tpl_header');?>


<!-- Page container -->
<section class="page-container page-boxed">
	<div class="row-fluid">
		<article class="span12 data-block">
			<div class="data-container">
				<header>
					<h3>TIMELINE</h3>
				</header>
				<section>

<div class="timeline">
    <div class="timeline_floor timeline_begin">
        <span class="timeline_date timeline_left"> March 2014</span>

        <div class="timeline_point"></div>
        <span class="timeline_content timeline_right">Creation</span>
    </div>

    <div class="timeline_floor">
        <span class="timeline_date timeline_right"> DECEMBER 2015</span>

        <div class="timeline_point"></div>
        <span class="timeline_content timeline_left">Firest project</span>
    </div>

    <div class="timeline_floor">
        <span class="timeline_date timeline_left"> FEBRUARY 2017</span>

        <div class="timeline_point"></div>
        <span class="timeline_content timeline_right">Moving to Paris</span>
    </div>

    <div class="timeline_floor">
        <span class="timeline_date timeline_right "> jULy 2017</span>

        <div class="timeline_point"></div>
        <span class="timeline_content timeline_left">An important thing</span>
    </div>

    <div class="timeline_floor">
        <span class="timeline_date timeline_left"> JANUARY 2018</span>

        <div class="timeline_point"></div>
        <span class="timeline_content timeline_right">We were 30</span>
    </div>

    <div class="timeline_floor center">
        <span class="timeline_content timeline_center">Here We Are</span>
    </div>

    <div class="timeline_time"></div>
</div>


				</section>
			</div>
		</article>
	</div>
</section>


<?php $this->load->view($template_path .'tpl_footer');?>