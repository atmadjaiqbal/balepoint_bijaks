<?php $this->load->view($template_path .'tpl_header');?>

<section class="page-container page-boxed">

	<!-- ROW 1 BERITA_TERAKHIR, HOT_ISSUE, ACTIVITY-->
	<div class="row-fluid">
		<article class="span8 data-block">
			<div class="data-container">
            <header>
					<h3>
					   <span class="icon-list"></span>
					   Article List
					</h3>
				</header>
            <?php $this->load->view($template_path .'blocks/tpl_block_article_list');?>						
			</div>
		</article>

		<article class="span4 data-block">
			<div class="data-container green">
				<header>
					<h3>Terkait</h3>
				</header>
            <?php $this->load->view($template_path .'blocks/tpl_block_activity');?>						
			</div>
		</article>
	</div>
	
	<div class="row-fluid">
	   <div class="span12 load-more-wrapper">
		   <button type="button" class="btn btn-large btn-block btn-error">Load More</button>
		</div>
   </div>

</section>

<?php $this->load->view($template_path .'tpl_footer');?>