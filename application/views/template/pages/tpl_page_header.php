<?php $this->load->view($template_path .'tpl_header');?>


<!-- Page container -->
<section class="page-container page-boxed">
	<div class="row-fluid">
		<article class="span12 data-block">
			<div class="data-container">
				<header>
					<h3>TIMELINE</h3>
				</header>
				<section>

<div class="bottom_area" style="width:300px;">

   <div class="block bottom_box">
       <div class="bblock_title">
           <div class="title_bg"><h2><a href="http://theme.effectivelab.net/effnews/blog/category/tech/">Tech</a></h2>
           </div>
           <span></span>
       </div>
   
       <section class="section_box_2">
           <a href="http://theme.effectivelab.net/effnews/blog/how-to-search-like-a-spy-googles-secret-hacks-revealed/">
               <img title="How to search like a spy: Google�s secret hacks revealed"
                    alt="How to search like a spy: Google�s secret hacks revealed"
                    src="http://theme.effectivelab.net/effnews/wp-content/uploads/2013/05/7893581588_18dedcb11a_b-300x225-232x136.jpg">
           </a>
   
           <div class="stylebox2">
               <div class="first_news">
                   <h2><a rel="bookmark"
                          href="http://theme.effectivelab.net/effnews/blog/how-to-search-like-a-spy-googles-secret-hacks-revealed/">How
                       to search like a spy: Google�s secret ha...</a></h2>
               </div>
               <ul>
                   <li>
                       <h2>� <a rel="bookmark"
                                href="http://theme.effectivelab.net/effnews/blog/nokias-asha-501-phone-claims-48-day-battery-life/">Nokia�s
                           Asha 501 Phone Claim...</a></h2>
                   </li>
                   <li>
                       <h2>� <a rel="bookmark"
                                href="http://theme.effectivelab.net/effnews/blog/samsung-galaxy-s4-keeps-calm-carries-on-with-big-screen-8-core-chip-and-yes-eye-tracking/">Samsung
                           Galaxy S4 keeps calm, ...</a></h2>
                   </li>
                   <li>
                       <h2>� <a rel="bookmark"
                                href="http://theme.effectivelab.net/effnews/blog/blackberry-z10-video-review/">BlackBerry
                           Z10 Video Review</a></h2>
                   </li>
                   <li>
                       <h2>� <a rel="bookmark"
                                href="http://theme.effectivelab.net/effnews/blog/report-zuckerberg-facing-1billion-tax-bill/">Report:
                           Zuckerberg facing $1bi...</a></h2>
                   </li>
                   <a class="read_more" href="http://theme.effectivelab.net/effnews/blog/category/tech/">More �</a>
               </ul>
           </div>
       </section>
   </div>
   
   <div class="block bottom_box">
       <div class="bblock_title">
           <div class="title_bg"><h2><a href="http://theme.effectivelab.net/effnews/blog/category/world/">World</a></h2>
           </div>
           <span></span>
       </div>
   
       <section class="section_box_2">
           <a href="http://theme.effectivelab.net/effnews/blog/obama-praises-hero-nypd-cop-and-girlfriend-during-white-house-ceremony/">
               <img title="Obama praises hero NYPD cop and girlfriend during White House ceremony"
                    alt="Obama praises hero NYPD cop and girlfriend during White House ceremony"
                    src="http://theme.effectivelab.net/effnews/wp-content/uploads/2013/05/8210028879_3da4c1abfd_b-225x300-225x136.jpg">
           </a>
   
           <div class="stylebox2">
               <div class="first_news">
                   <h2><a rel="bookmark"
                          href="http://theme.effectivelab.net/effnews/blog/obama-praises-hero-nypd-cop-and-girlfriend-during-white-house-ceremony/">Obama
                       praises hero NYPD cop and girlfriend during ...</a></h2>
               </div>
               <ul>
                   <li>
                       <h2>� <a rel="bookmark"
                                href="http://theme.effectivelab.net/effnews/blog/president-obama-urges-congress-to-boost-homeowners-in-healing-housing-market/">President
                           Obama Urges Congress...</a></h2>
                   </li>
                   <li>
                       <h2>� <a rel="bookmark"
                                href="http://theme.effectivelab.net/effnews/blog/nato-alarm-over-afghan-army-crisis-loss-of-recruits-threatens-security-as-handover-looms/">Nato
                           alarm over Afghan army cr...</a></h2>
                   </li>
                   <li>
                       <h2>� <a rel="bookmark"
                                href="http://theme.effectivelab.net/effnews/blog/north-korea-says-enters-state-of-war-against-south/">North
                           Korea says enters �...</a></h2>
                   </li>
                   <li>
                       <h2>� <a rel="bookmark"
                                href="http://theme.effectivelab.net/effnews/blog/cubiclenama-the-chillax-manifesto/">Cubiclenama
                           | The chillax mani...</a></h2>
                   </li>
                   <a class="read_more" href="http://theme.effectivelab.net/effnews/blog/category/world/">More �</a>
               </ul>
           </div>
       </section>
   </div>

</div>


				</section>
			</div>
		</article>
	</div>
</section>


<?php $this->load->view($template_path .'tpl_footer');?>