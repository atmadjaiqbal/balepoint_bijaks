<?php $this->load->view($template_path .'tpl_header');?>


<!-- Page container -->
<section class="page-container page-boxed">
	<div class="row-fluid">
		<article class="span12 data-block">
			<div class="data-container">
				<header>
					<h3>TIMELINE</h3>
				</header>
				<section>

<div id="timeline" class="demo1">
    <div class="timeline dual" style="width: 900px;">
        <div class="spine animated"></div>
        <div class="column column_left year_2012">
            <div class="timeline_element iframe animated" style="width: 400px;">
                <div class="title"><span class="label">Map</span><span class="date">03 September 2012</span></div>
                <div class="content" style="height: 300px;">
                    <iframe frameborder="0" src="https://maps.google.com.au/?ie=UTF8&amp;ll=-27.40739,153.002859&amp;spn=1.509276,2.515869&amp;t=v&amp;z=9&amp;output=embed"></iframe>
                </div>
                <div class="del">Delete</div>
            </div>
        </div>
        <div class="column column_right year_2012">
            <div class="timeline_element iframe animated" style="width: 400px;">
                <div class="title"><span class="label">Video</span><span class="date">12 August 2012</span></div>
                <div class="content" style="height: 300px;">
                    <iframe frameborder="0" src="http://www.youtube.com/embed/0ZQBRsEyN1E?wmode=opaque"></iframe>
                </div>
                <div class="del">Delete</div>
            </div>
        </div>
        
        <div class="date_separator animated"><span>2011</span></div>
        
        <div class="column column_left year_2011">
            <div class="timeline_element slider notitle animated" style="width: 400px;">
                <div class="content" style="width: 400px; height: 150px;">
                    <div style="display: none;" class="img_container" data-order="0" data-total="3">
                        <img src="<?php echo $asset_url;?>img/timeline/group.jpg">

                        <div class="img_overlay">
                           <span data-img="<?php echo $asset_url;?>img/timeline/group.jpg" data-type="slider" data-order="0" data-total="3" class="magnifier"></span>
                        </div>
                    </div>
                    
                    <div style="display: block;" class="img_container active" data-order="1" data-total="3"><img
                            src="<?php echo $asset_url;?>img/timeline/old.jpg">

                        <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/old.jpg" data-type="slider" data-order="1"
                                                       data-total="3" class="magnifier"></span></div>
                    </div>
                    <div style="display: none;" class="img_container" data-order="2" data-total="3"><img
                            src="<?php echo $asset_url;?>img/timeline/win.jpg">

                        <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/win.jpg" data-type="slider" data-order="2"
                                                       data-total="3" class="magnifier"></span></div>
                    </div>
                    <span class="slider_prev"></span><span class="slider_next"></span></div>
                <div class="del">Delete</div>
            </div>
            <div class="timeline_element gallery animated" style="width: 400px;">
                <div class="title"><span class="label">Mini Gallery</span><span class="date">12 April 2011</span></div>
                <div class="scroll_container loaded">
                    <div class="scroller" style="width: 1015px;">
                        <div class="ruler">
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/rooney.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/rooney.jpg" data-type="gallery"
                                                               data-order="0" data-total="5" class="magnifier"></span>
                                </div>
                            </div>
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/tshirt.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/tshirt.jpg" data-type="gallery"
                                                               data-order="1" data-total="5" class="magnifier"></span>
                                </div>
                            </div>
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/giggs.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/giggs.jpg" data-type="gallery"
                                                               data-order="2" data-total="5" class="magnifier"></span>
                                </div>
                            </div>
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/rio.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/rio.jpg" data-type="gallery"
                                                               data-order="3" data-total="5" class="magnifier"></span>
                                </div>
                            </div>
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/paper.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/paper.jpg" data-type="gallery"
                                                               data-order="4" data-total="5" class="magnifier"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del">Delete</div>
            </div>
        </div>
        <div class="column column_right year_2011">
            <div class="timeline_element blog_post animated" style="width: 400px;">
                <div class="title"><span class="label">Blog Post</span><span class="date">03 August 2011</span></div>
                <div class="img_container"><img src="<?php echo $asset_url;?>img/timeline/rio.jpg">

                    <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/rio.jpg" data-type="blog_post"
                                                   class="magnifier"></span></div>
                </div>
                <div class="content"><b>Lorem Ipsum</b> is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a type specimen book. It has survived not
                    only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </div>
                <div class="readmore"><a href="http://www.manutd.com">Read More</a></div>
                <div class="del">Delete</div>
            </div>
        </div>
        <div class="date_separator animated"><span>2010</span></div>
        <div class="column column_left year_2010">
            <div class="timeline_element blog_post animated" style="width: 400px;">
                <div class="title"><span class="label">Blog Post</span><span class="date">03 August 2010</span></div>
                <div class="img_container"><img src="<?php echo $asset_url;?>img/timeline/evra.jpg">

                    <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/evra.jpg" data-type="blog_post"
                                                   class="magnifier"></span></div>
                </div>
                <div class="content"><b>Lorem Ipsum</b> is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a type specimen book. It has survived not
                    only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                </div>
                <div class="readmore"><a href="http://www.scriptgates.com">Read More</a></div>
                <div class="del">Delete</div>
            </div>
        </div>
        <div class="column column_right year_2010">
            <div class="timeline_element slider notitle animated" style="width: 400px;">
                <div class="content" style="width: 400px; height: 200px;">
                    <div style="display: none;" class="img_container" data-order="0" data-total="2"><img
                            src="<?php echo $asset_url;?>img/timeline/ferguson.jpg">

                        <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/ferguson.jpg" data-type="slider" data-order="0"
                                                       data-total="2" class="magnifier"></span></div>
                    </div>
                    <div style="display: block;" class="img_container active" data-order="1" data-total="2"><img
                            src="<?php echo $asset_url;?>img/timeline/paper.jpg">

                        <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/paper.jpg" data-type="slider" data-order="1"
                                                       data-total="2" class="magnifier"></span></div>
                    </div>
                    <span class="slider_prev"></span><span class="slider_next"></span></div>
                <div class="del">Delete</div>
            </div>
            <div class="timeline_element gallery animated" style="width: 400px;">
                <div class="title"><span class="label">Mini Gallery</span><span class="date">12 April 2010</span></div>
                <div class="scroll_container loaded">
                    <div class="scroller" style="width: 632px;">
                        <div class="ruler">
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/stadium.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/stadium.jpg" data-type="gallery"
                                                               data-order="0" data-total="4" class="magnifier"></span>
                                </div>
                            </div>
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/rafel.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/rafel.jpg" data-type="gallery"
                                                               data-order="1" data-total="4" class="magnifier"></span>
                                </div>
                            </div>
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/logo.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/logo.jpg" data-type="gallery"
                                                               data-order="2" data-total="4" class="magnifier"></span>
                                </div>
                            </div>
                            <div class="img_container"><img height="150" src="<?php echo $asset_url;?>img/timeline/rvp.jpg">

                                <div class="img_overlay"><span data-img="<?php echo $asset_url;?>img/timeline/rvp.jpg" data-type="gallery"
                                                               data-order="3" data-total="4" class="magnifier"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del">Delete</div>
            </div>
        </div>
    </div>
</div>


				</section>
			</div>
		</article>
	</div>
</section>


<?php $this->load->view($template_path .'tpl_footer');?>