<?php $this->load->view($template_path .'tpl_header');?>

<section class="page-container page-boxed">

	<div class="row-fluid">
		<article class="artickel span8 data-block">
			<div class="data-container">
            <header>
					<h3>
					   <span class="icon-list"></span>
					   Article List
					</h3>
				</header>
            <section class="articles">
               <article class="article-item ">
                  <div class="article-content row-fluid">
                     	<h3>
                     		<a href="detik.com">1 Nunc congue metus quis posuere tempor</a>
                     	</h3>
            
                     	<div class="social-info">
                     	   <div class="social-data">
                        	   <span>01 May 2013 - 01:22</span>
                        	   <span>81 <i class="icon-comments"></i></span>
                        	   <span>12 <i class="icon-thumbs-up"></i></span>
                        	   <span>0 <i class="icon-thumbs-down"></i></span>
                     	   </div>
                           <div class="social-data">
                              
                           </div>   
                     	</div>

                        <figure class="detail">
                           <img alt="" src="http://news.bijaks.net/uploads/2013/05/konflik-agraria.jpg" >
                           <p class="wp-caption-text">Konflik aksi damai menuntut penyelesaian konflik agraria. Foto : angkringanwarta.com</p>
                        </figure>

                  
                     	<p class="detail">
                           <strong>JAKARTA, BIJAK </strong><strong>&nbsp;-</strong> Sudah hampir setahun sejak dibentuk oleh DPR, Timwas Penyelesaian Sengketa Pertanahan dan Konflik Agraria belum pernah sekali pun mengadakan rapat.
                           Timwas Sengketa Tanah dan Konflik Agraria, begitu singkatnya, dibentuk pada 4 Juni 2012 berdasarkan keputusan Pimpinan DPR No. 24C/Pimp/IV/2011-2012 tentang Pembentukan Tim Pengawasan DPR Terhadap Penyelesaian Sengketa Pertanahan dan Konflik Agraria.
                           <br /><br />
                           Timwas ini terdiri dari 30 anggota DPR lintas komisi, dan dipimpin Wakil Ketua DPR RI Koordinator Bidang Politik dan Keamanan, Priyo Budi Santoso.
                           <br /><br />
                           Sementara itu Pembaruan Agraria yang selama ini dijanjikan presiden tidak pernah menyentuh akar persoalan. Buktinya Rancangan Peraturan Pemerintah (RPP) tentang Reforma Agraria atau Pembaruan Agraria (PA), yang selama ini dijanjikan presiden pun tidak pernah ada.
                           <br /><br />
                           Ketua Umum Partai Rakyat Demokratik (PRD) Agus Jabo Priyono pernah menegaskan, konflik agraria yang terus meningkat tidak terlepas dari kebijakan rezim SBY yang makin liberal dan pro-neoliberalisme.
                           <br /><br />
                           Aktivis penggiat pemberaharuan agraria berkali-kali menuntut langkah cepat untuk penyelesaian konflik yang sudah ada. SBY juga diminta segera mengembalikan politik agraria Indonesia sesuai garis konstitusi, yakni pasal 33 UUD 1945 dan UUPA 1960.(Jp/Aw)
                     	</p>
                  </div>         
                  
               	<div class="article-social row-fluid">
                  	<div class="span4"><b>Politisi:</b>
                        <div class="social-friends">
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user1_55.jpg"></a>
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user2_55.jpg"></a>
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user5_55.jpg"></a>
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user4_55.jpg"></a>
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user3_55.jpg"></a>
                        </div>      	   
                  	</div>
                  	
                  	<div class="span4"><b>Likes:</b>
                        <div class="social-friends">
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user1_55.jpg"></a>
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user2_55.jpg"></a>
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user5_55.jpg"></a>
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user4_55.jpg"></a>
                          <a href="#"><img width="32" src="<?php echo $asset_url;?>img/people-face/user3_55.jpg"></a>
                        </div>      	   
                  	</div>
            
                  	<div class="span4"><b>Comment:</b>
                        <div class="social-friends">
                          <a href="#"><img width="32" class="img-polaroid" src="<?php echo $asset_url;?>img/people-face/user1_55.jpg"></a>
                          <a href="#"><img width="32" class="img-polaroid" src="<?php echo $asset_url;?>img/people-face/user2_55.jpg"></a>
                          <a href="#"><img width="32" class="img-polaroid" src="<?php echo $asset_url;?>img/people-face/user5_55.jpg"></a>
                          <a href="#"><img width="32" class="img-polaroid" src="<?php echo $asset_url;?>img/people-face/user4_55.jpg"></a>
                          <a href="#"><img width="32" class="img-polaroid" src="<?php echo $asset_url;?>img/people-face/user3_55.jpg"></a>
                        </div>      	   
                  	</div>
            
               	</div>      
               </article>
            </section>                  
            
			</div>
		</article>

		<article class="span4 data-block">
			<div class="data-container">
				<header>
					<h3>Aktivitas terkait Artikel</h3>
				</header>
            <?php $this->load->view($template_path .'blocks/tpl_block_activity');?>						
			</div>

			<div class="data-container green">
				<header>
					<h3>Artikel Terkait</h3>
				</header>
            <?php $this->load->view($template_path .'blocks/tpl_block_news_list');?>						
			</div>
		</article>
	</div>
	
</section>

<?php $this->load->view($template_path .'tpl_footer');?>