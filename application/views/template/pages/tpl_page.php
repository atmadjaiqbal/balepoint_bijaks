<?php $this->load->view($template_path .'tpl_header');?>

<!-- Page container -->
<section class="page-container page-boxed">

	<!-- Grid row -->
	<div class="row-fluid">
		<article class="span8 data-block">
			<div class="data-container">
				<header>
					<h3>Alerts &amp; notifications</h3>
				</header>
				<section>
					<div>
                  <ul class="thumbnails-action">
							<li class="span2">
								<ul class="thumbnail-actions">
									<li><a title="" href="#" data-original-title="Edit photo"><span class="icon-edit"></span></a></li>
									<li><a title="" href="#" data-original-title="Delete photo"><span class="icon-trash"></span></a></li>
									<li><a title="" href="#" data-original-title="Download photo"><span class="icon-download-alt"></span></a></li>
								</ul>
								<a href="#" class="thumbnail"><img src="<?php echo $asset_url.'img/examples/sample-image.png';?>" alt=""></a>
							</li>
							<li class="span2">
								<ul class="thumbnail-actions">
									<li><a title="" href="#" data-original-title="Edit photo"><span class="icon-edit"></span></a></li>
									<li><a title="" href="#" data-original-title="Delete photo"><span class="icon-trash"></span></a></li>
									<li><a title="" href="#" data-original-title="Download photo"><span class="icon-download-alt"></span></a></li>
								</ul>
								<a href="#" class="thumbnail"><img src="<?php echo $asset_url.'img/examples/sample-image.png';?>" alt=""></a>
							</li>
							<li class="span2">
								<ul class="thumbnail-actions">
									<li><a title="" href="#" data-original-title="Edit photo"><span class="icon-edit"></span></a></li>
									<li><a title="" href="#" data-original-title="Delete photo"><span class="icon-trash"></span></a></li>
									<li><a title="" href="#" data-original-title="Download photo"><span class="icon-download-alt"></span></a></li>
								</ul>
								<a href="#" class="thumbnail"><img src="<?php echo $asset_url.'img/examples/sample-image.png';?>" alt=""></a>
							</li>
						</ul>
                  <div class="clearfix"></div>
						<p>
						   Best check yo self, you're not looking too good. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
						</p>
					</div>
				</section>
			</div>
		</article>
		
		<article class="span4 data-block">
			<div class="data-container">
				<header>
					<h2><span class="icon-tint"></span> Fusce nisl dolor</h2>
				</header>
				<section>
					<ul class="thumbnails-action">
						<li>
                     <ul class="thumbnail-actions">
								<li><a title="" href="#" data-original-title="Edit photo"><span class="icon-edit"></span></a></li>
								<li><a title="" href="#" data-original-title="Delete photo"><span class="icon-trash"></span></a></li>
								<li><a title="" href="#" data-original-title="Download photo"><span class="icon-download-alt"></span></a></li>
							</ul>												   
							<div class="thumbnail"><img src="<?php echo $asset_url.'img/examples/250.jpg';?>" alt="Sample Image" /></div>
						</li>
					</ul>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam justo velit, eleifend nec adipiscing id, consequat ut augue. Pellentesque nec neque et leo ullamcorper pellentesque. Pellentesque aliquet iaculis velit sit amet vestibulum.</p>
				</section>
			</div>
		</article>
	</div>
	<!-- /Grid row -->
	
	
	<!-- TAB -->
	<div class="row-fluid">
		<!-- Data (buttons) block -->
		<article class="span12 data-block">
			<div class="data-container">
				<header>
					<h2>Buttons</h2>
					<ul class="data-header-actions tabs">
						<li class="demoTabs active"><a href="#default">Default</a></li>
						<li class="demoTabs"><a href="#lindworm">Lindworm</a></li>
						<li class="demoTabs"><a href="#flat">Flat</a></li>
					</ul>
				</header>

				<!-- Tab content -->
				<section class="tab-content">
					
					<!-- Tab #default -->
					<div class="tab-pane fade in active" id="default">
						<div class="row-fluid">
							<article class="span12 data-block data-block-alt">
								<div class="data-container">
									<header>
										<h2>Default buttons</h2>
									</header>
									<section>
									   Default section content
									</section>
								</div>
							</article>

						</div>
					</div>
					<!-- Tab #default -->
					
					<!-- Tab #lindworm -->
					<div class="tab-pane fade" id="lindworm">
						<div class="row-fluid">
							<article class="span12 data-block data-block-alt">
								<div class="data-container">
									<header>
										<h2>Lindworm buttons</h2>
									</header>
									<section>
                                 Lindorm Section Content
									</section>

								</div>
							</article>

						</div>
					</div>
					<!-- Tab #lindworm -->
					
					<!-- Tab #flat -->
					<div class="tab-pane fade" id="flat">
						<div class="row-fluid">
							<article class="span12 data-block data-block-alt">
								<div class="data-container">
									<header>
										<h2>Flat buttons</h2>
									</header>
									<section>
									   Flat Section Content
									</section>

								</div>
							</article>

						</div>
					</div>
					<!-- Tab #flat -->
						
				</section>
				<!-- /Tab content -->

			</div>
		<!-- /Data (buttons) block -->

	   </article>
	</div>
	<!-- /TAB  -->
	
	
</section>

<?php $this->load->view($template_path .'tpl_footer');?>