<?php if (!empty($sidebar)):?>

	<?php foreach($sidebar as $section): ?>
	
	   <h3><?php echo $section['title'];?></h3>
	   <table>
	   <tbody>
		   <?php foreach($section['list'] as $row): ?>
		   <tr>
		      <td valign="top" style="width:40px; padding-top:7px;">
		         <a class="f" href="<?php echo base_url().'politisi/index/'.$row['page_id'];?>">
		            <img src="<?php echo icon_url($row['attachment_title'],'politisi/'.$row['page_id']);?>" alt="<?php echo $row['page_name'];?>" width="32">
		         </a>
		      </td>
		      <td valign="top">
		         <a href="<?php echo base_url().'politisi/index/'.$row['page_id'];?>">
		            <?php echo $row['page_name'];?>
		         </a>
		         <div class="content_<?php echo $row['page_id'];?>" ><?php echo $row['count_page']; ?></div>
		      </td> 
		   </tr>
		   <?php endforeach;?>
	   
	   </tbody>
	   </table>
	
	<?php endforeach;?>

<?php endif;?>
   