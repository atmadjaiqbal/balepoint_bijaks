<?php 
	include('include/header_view.php');
?>
<?php echo '<p>'.date( "d/m/Y", time() ).'</p>';?>
Hi, <?php echo $email; ?>
<p>
You registered to www.bijak.com with this email account. Before you can continue our services, you must first activate.
</p>
<p>
Click <a href="<?php echo base_url();?>home/activation?uid=<?php echo urldecode($account_id);?>">here</a> to activate your account. 
</p>

Regards,</br>
www.bijak.com
<?php 
	include('include/footer_view.php');
?>
