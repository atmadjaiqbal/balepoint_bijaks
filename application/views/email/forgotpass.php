<?php 
	include('include/header_view.php');
	$resetpass_url = base_url().'home/resetpassword?token='.$token;
	$cancelresetpass_url = base_url().'home/cancelresetpassword?token='.$token;
?>
<?php echo '<p>'.date( "d/m/Y", time() ).'</p>';?>
Hi, <?php echo $name; ?>
<p>
You (or someone else) requested a password reset to www.bijak.com with this email account. Your password reset service is valid within 24 hours since you requested.
</p>
<p>
Click <a href="<?php echo $resetpass_url;?>">here</a> if you would continue to reset your password or you can <a href="<?php echo $cancelresetpass_url;?>">cancel</a> your password reset request and continue using your old password.</br>
</p>


Salam,</br>
www.bijak.com
<?php 
	include('include/footer_view.php');
?>
