<div class="container" style="margin-top: 31px;">
  <div class="sub-header-container">
	<div class="logo-small-container">
		<img src="<?php echo base_url('assets/images/logo.png'); ?>" >
	</div>
	<div class="right-container">
		<div class="banner">
			<img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
		</div>
		<div class="category-news">
			<div class="category-news-title category-news-title-right category-news-title-merah">
				<h1><?php echo $message_title; ?></h1>
			</div>
			
		</div>
	</div>
  </div>	
  <div id="loginpage" class="sub-header-container">    
<div class='aboutbijaks'>

    <div class='hot-profile'>
      <ul>
<?php
       foreach ($hot_profile as $row)
       {
          $_profile_url = base_url().'aktor/profile/'.$row['page_id'];
?>
          <li>
             <span data-id="<?php echo $row['page_id']; ?>">
               <img src='<?php echo $row['badge_url']; ?>' data-src='<?php echo $row['badge_url']; ?>'
                    title="<?php echo $row['page_name']; ?>" alt=""/>
             </span>
          </li>
<?php
       }
?>
      </ul>
    </div>
	
<p><strong>Bijaks.net</strong> merupakan portal Social Network (SN) politik pertama dan utama di Indonesia, yang memadukan unsur jaringan sosial, berita dan arsip terkait dunia politik Indonesia, mulai dari poltisi, partai politik maupun dinamika politik Indonesia.</p>
<p>Setiap hari, <strong>Bijaks.net</strong> menghadirkan sesuatu yang baru dan fresh, baik dari sisi pemberitaan maupun kumpulan arsip politik Indonesia, mulai dari eksekutif, legislatif, maupun yudikatif.</p>
</div>

<div class="loginarea">
	<p class="info-changepass">System has automatically log you in. Please input a new password for your account.</p>
   <div class="error-message">
     <span class='message_login' style='display:none'><b></b></span>
   </div>	
	<?php if(isset($message)){ ?>
	<div class="message"><b><?php echo $message;?></b></div>
	<?php }?>
	<form id="changepassword" name="changepassword" method="POST" action="<?php echo base_url();?>home/resetpassnewpass">
     <div class="field-change"><input type="password" class="inputlogin" type="password" name="newpasswd1" id="newpasswd1" size="35" placeholder="type new password ..."/></div>
     <div class="field-change"><input type="password" class="inputlogin" type="password" name="newpasswd2" id="newpasswd2" size="35" placeholder="retype new password ..."/></div>
     <div class="field-change"><input type="submit" class="loginbutton" value="Save" style="margin-top:15px;margin-left:0px !important;"/>
	</form> 

</div>
  </div>
</div>
<br/>



<script>
$(document).ready(function(){

	$('#changepassword').validate({
		rules: {
			newpasswd1: { required: true},
			newpasswd2: { required: true},
			
			// , equalTo : "#newspasswd1" 
		},

		messages: {
			newpasswd1: { required: "Please enter your news password." },
			newpasswd2: { required: "Plese re-type your password." },
		},

		errorPlacement: function(error, element) {
 		   $('.message_login b').text('');
			$('.message_login b').append(error);
			$('.message_login').show();
		},
	});
	
});
</script>