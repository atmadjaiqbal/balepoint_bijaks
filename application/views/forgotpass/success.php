<?php
$in_where = 'login';
$isSentimen = FALSE;
$this->load->view('include_me/header');
?>
<div id='main'><div id='body' class='page'><div class='container'>

<div class='row row-col'>
	<div id='col-1-1' class='col col-1-1'>
		<div class='block'>
			<div class='message' style='display: none;'><b></b></div>
			<div class='divFormLogin span5'>

				<h4>Reset Password</h4>
				<p>Your password reset is successful. <a href="<?php echo base_url()."home/login";?>">continue to login</a>.</p>

			</div>
			<p>&nbsp;</p>

		</div>
	</div>
</div>

</div></div></div><!-- #main -->
<?php
$this->load->view('include_me/footer')
?>