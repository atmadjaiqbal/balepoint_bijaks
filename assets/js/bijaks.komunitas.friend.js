var host = '';
$(function(){
    var id = $('#friend_list').data('id');
    var user = $('#friend_list').data('user');
    friend_list('#friend_list', id, user, 1, 1);

    $('#friendTab a').click(function (e) {
        e.preventDefault();
        var target = e.target;
        var target_id = $(target).attr('href');
//        console.debug(target_id);
        var id = $(target).data('id');
        var user = $(target).data('user');
        var tipe = $(target).data('tipe');
        if($(target_id+ '_list').is(':empty')){
            friend_list(target_id + '_list', id, user, tipe, 1);
        }
        $(this).tab('show');
    });

    $('#friend_load_more').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var user = $(this).data('user');
        var tipe = $(this).data('tipe');
        var page = $(this).data('page');
        friend_list('#friend_list', id, user, tipe, page);
        $(this).data('page', page+1);
    });

    $('#request_load_more').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var user = $(this).data('user');
        var tipe = $(this).data('tipe');
        var page = $(this).data('page');
        friend_list('#request_list', id, user, tipe, page);
        $(this).data('page', page+1);
    });

    $('#pending_load_more').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var user = $(this).data('user');
        var tipe = $(this).data('tipe');
        var page = $(this).data('page');
        friend_list('#pending_list', id, user, tipe, page);
        $(this).data('page', page+1);
    });

    $('#friend_list').on("click", '.unfriend', function(e){
        e.preventDefault();
        if(confirm('Yakin unfriend user ini')){
            var tipe = $(this).data('tipe');
            var id = $(this).data('id');
            var user = $(this).data('user');
            var fid = $(this).data('fid');
            var uri = Settings.base_url+'/timeline/unfriend';
            var data = {'tipe':tipe, 'id':id, 'user':user, 'fid':fid};
            var parent = $(this).parent().parent();
            $.post(uri, data, function(res){
                if(res.rcode == 'ok'){
                    parent.next().remove();
                    parent.hide('blind').remove();
                }
            });
        }

    });

    $('#request_list').on("click", '.unfriend', function(e){
        e.preventDefault();
        if(confirm('Yakin menerima user ini')){
            var tipe = $(this).data('tipe');
            var id = $(this).data('id');
            var user = $(this).data('user');
            var fid = $(this).data('fid');
            var uri = host+'/timeline/unfriend';
            var data = {'tipe':tipe, 'id':id, 'user':user, 'fid':fid};
            var parent = $(this).parent().parent();
            $.post(uri, data, function(res){
                if(res.rcode == 'ok'){
                    parent.next().remove();
                    parent.hide('blind').remove();
                }
            });
        }

    });

    $('#request_list').on("click", '.tolak', function(e){
        e.preventDefault();
        if(confirm('Yakin menolak user ini')){
            var tipe = $(this).data('tipe');
            var id = $(this).data('id');
            var user = $(this).data('user');
            var fid = $(this).data('fid');
            var uri = Settings.base_url+'/timeline/unfriend';
            var data = {'tipe':tipe, 'id':id, 'user':user, 'fid':fid};
            var parent = $(this).parent().parent();
            $.post(uri, data, function(res){
                if(res.rcode == 'ok'){
                    parent.next().remove();
                    parent.hide('blind').remove();
                }
            });
        }

    });

    $('#pending_list').on("click", '.unfriend', function(e){
        e.preventDefault();
        if(confirm('Yakin unfriend user ini')){
            var tipe = $(this).data('tipe');
            var id = $(this).data('id');
            var user = $(this).data('user');
            var fid = $(this).data('fid');
            var uri = Settings.base_url+'/timeline/unfriend';
            var data = {'tipe':tipe, 'id':id, 'user':user, 'fid':fid};
            var parent = $(this).parent().parent();
            $.post(uri, data, function(res){
                if(res.rcode == 'ok'){
                    parent.next().remove();
                    parent.hide('blind').remove();
                }
            });
        }

    });

})

function friend_list(holder, id, user_id, tipe, page)
{
    var foll = $(holder);
    var please = '<div id="'+tipe+'_wall_loader" class="loader"></div>'; // '<p id="wall_loader">Please wait...</p>';
    foll.append(please);
    $('#'+tipe+'_wall_loader').css('visibility', 'visible');
    var to_id = user_id;

    $.ajax({
        url: Settings.base_url+'/komunitas/friend_list/'+to_id+'/'+tipe+'/'+page
    }).done(function(html){
            $('#'+tipe+'_wall_loader').hide('blind').remove();
            foll.append(html);
            page = foll.data('page') + 1;
            foll.data('page', page);

        });
}

