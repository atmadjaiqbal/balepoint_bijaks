$(document).ready(function(){

    jadwal_list('#jadwal_container');
    $('#jadwal_load_more').click(function(e){
        e.preventDefault();
        jadwal_list('#jadwal_container');
    });
});


function jadwal_list(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    console.log(Settings.base_url+'page/jadwal_list/'+page);

    $.get(Settings.base_url+'page/jadwal_list/'+page, function(data){

        //console.log(data);

        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();

        var jmlRow = $('#detail-jadwal').data('jumlah');
        var sisah = jmlRow - 20;
        $('#load_more_place').data('rows', sisah);

        if(sisah <= 0 ) { $('#load_more_place').hide(); }

    });

}