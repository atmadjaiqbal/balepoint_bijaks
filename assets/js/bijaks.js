//var Settings.base_url = window.location.Settings.base_urlname;
//console.debug(Settings.base_url);

$(function(){

    $('.selanjutnya-category-scandal').click(function(){
        var page = $(this).data('page');
        var categoryId = $(this).data('category-id');
        var holder = $(this).data('holder');
        $.ajax({
            async: false,
            cache: false,
            type: "post",
            url: Settings.base_url+'scandal/ajaxFooterCategory',
            data: {'page':page,'categoryId':categoryId},
            beforeSend: function(){
                
            },
            success: function(response){
                $('#'+holder).html(response);
                
            },
            complete: function(){
            },
            error: function(){

            }
        });
        $(this).data('page',(page+1));
    })

    $('.scandal-category-n-next').click(function(){
        var page = $(this).data('page');
        var categoryId = $(this).data('category-id');
        var holder = $(this).data('holder');
        $.ajax({
            async:false,
            cache: false,
            type: "post",
            url: Settings.base_url+'scandal/ajaxNextScandals',
            data: {'page':page,'categoryId':categoryId},
            beforeSend: function(){

            },
            success: function(response){
                $('#'+holder).append(response);
                
            },
            complete: function(){
            },
            error: function(){

            }            
        });
        $(this).data('page',(page+1));  
    })

    $('#head_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#dv_next").animate(
                {"top": "-=1715px"},
                "slow");
            $(this).text('20 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#dv_next").animate(
                {"top": "+=1715px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('20 Berikutnya');
        }

    })

    $('#terkini_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#terkini_next").animate(
                {"top": "-=1030px"},
                "slow");
            $(this).text('12 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#terkini_next").animate(
                {"top": "+=1030px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('12 Berikutnya');
        }

    })

    $('#head_terkini_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#head_terkini_next").animate(
                {"top": "-=430px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#head_terkini_next").animate(
                {"top": "+=430px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('5 Berikutnya');
        }

    })

    $('#head_reses_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#head_reses_next").animate(
                {"top": "-=375px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#head_reses_next").animate(
                {"top": "+=375px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('5 Berikutnya');
        }

    })

    $('#headline_headline_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#head-headline-next").animate(
                {"top": "-=975px"},
                "slow");
            $(this).text('13 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#head-headline-next").animate(
                {"top": "+=975px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('13 Berikutnya');
        }

    })

    $('#reses_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#reses_next").animate(
                {"top": "-=425px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#reses_next").animate(
                {"top": "+=425px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('5 Berikutnya');
        }

    })

    $('#opini_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        if(parseInt(tipe) == 1){
            $("#opini_next").animate(
                {"top": "-=500px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#opini_next").animate(
                {"top": "+=500px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('8 Berikutnya');
        }
    })

    $('#opini_news_prev').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        $(this).hide();
        $('#opini_news_next').show();
        $("#opini_next").animate({"top": "+=500px"},"slow");
        $(this).data('tipe', "1");
        $(this).text('12 Sebelumnya');
    })

    $('#komentar_button_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        if(parseInt(tipe) == 1){
            $("#komentar_next").animate(
                {"top": "-=500px"},
                "slow");
            $(this).text('16 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#komentar_next").animate(
                {"top": "+=500px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('16 Berikutnya');
        }
    })

    $('#komentar_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        $(this).hide();
        $('#komentar_news_prev').show();
        $("#komentar_next").animate({"top": "-=495px"},"slow");
        $(this).text('15 Berikutnya');
        $(this).data('tipe', "0");
    })

    $('.next-list').click(function(e){
        e.preventDefault();
        var holder = $(this).data('holder')
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("."+holder).animate(
                {"top": "-=375px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("."+holder).animate(
                {"top": "+=375px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('5 Berikutnya');
        }

    })

    $('.news-next-list').click(function(e){
        e.preventDefault();
        var holder = $(this).data('holder')
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("."+holder).animate(
                {"top": "-=285px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("."+holder).animate(
                {"top": "+=285px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('5 Berikutnya');
        }

    })


    $('.time-line-content').each( function(a, b){
        var uri = $(b).data('uri');
        var size = $(b).data('size');        
        var cat = $(b).data('cat');
        // console.debug(uri);
        $(b).load(Settings.base_url+'timeline/last/content/'+uri+'/false/'+cat+'/'+size);
    });


    $('.time-line-profile').each( function(a, b){
        var uri = $(b).data('uri');
        var cat = $(b).data('cat');
        // console.debug(uri);
        $(b).load(Settings.base_url+'timeline/last/profile/'+uri+'/false/'+cat);
    });

    $('.score').each(function(a, b){
        var id = $(b).data('id');
        var tipe = '0';
        if($(b).data('tipe')){
            tipe = $(b).data('tipe');
        }
        $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
    });

    var tipe_last = $('#sub_header_activity').data('tipe');

    $('#sub_header_activity').load(Settings.base_url+'timeline/sub_header/'+tipe_last);

    $('#disclaimer').load(Settings.base_url+'timeline/disclaimer');

    $(".score").on('click', ".score-btn", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });

    //CLICK LIKE UNLIKE  score-place
    $(".score").on('click', ".score-btn-simple", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });


    $(".komunitas-scoring").on('click', ".score-btn-simple", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '1';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });

    $(".komunitas-scoring").on('click', ".score-btn", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '1';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });

    comment_list('#comment');

    $('#comment_load_more').click(function(e){
        e.preventDefault();
        comment_list('#comment');
    });

    $('#send_coment').click(function(e){
        var id = $('#comment_type').data('id');
        var val = $('#comment_type').val();
       // var val = $('textarea#comment_type').val();

        var please = '<div id="comment_loader" class="loader"></div>';
        $('#comment').prepend(please);
        $('#comment_loader').css('visibility', 'visible');
        $.post(Settings.base_url+'timeline/post_comment/', {'id':id, 'val':val}, function(data){
            $('#comment_loader').remove();
            if(data.rcode == 'ok'){
                $('#comment').prepend(data.msg);
                $('.score').each(function(a, b){
                    var id = $(b).data('id');
                    var tipe = '0';
                    if($(b).data('tipe')){
                        tipe = $(b).data('tipe');
                    }
                    $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                });
            }else{
                alert(data.msg);
            }
            $('#comment_type').val('');
        })
    });

    news_ekonomi_list('#ekonomi_container');
    $('#ekonomi_loadmore').click(function(e){
        e.preventDefault();
        news_ekonomi_list('#ekonomi_container');
    });

    news_hukum_list('#hukum_container');
    $('#hukum_loadmore').click(function(e){
        e.preventDefault();
        news_hukum_list('#hukum_container');
    });

    news_tajuk_list('#tajuk_container');
    $('#tajuk_loadmore').click(function(e){
        e.preventDefault();
        news_tajuk_list('#tajuk_container');
    });

    news_parlemen_list('#parlemen_container');
    $('#parlemen_loadmore').click(function(e){
        e.preventDefault();
        news_parlemen_list('#parlemen_container');
    });

    news_nasional_list('#nasional_container');
    $('#nasional_loadmore').click(function(e){
        e.preventDefault();
        news_nasional_list('#nasional_container');
    });

    news_daerah_list('#daerah_container');
    $('#daerah_loadmore').click(function(e){
        e.preventDefault();
        news_daerah_list('#daerah_container');
    });

    news_internasional_list('#internasional_container');
    $('#inter_loadmore').click(function(e){
        e.preventDefault();
        news_internasional_list('#internasional_container');
    });

    news_lingkungan_list('#lingkungan_container');
    $('#lingkungan_loadmore').click(function(e){
        e.preventDefault();
        news_lingkungan_list('#lingkungan_container');
    });

    news_kesenjangan_list('#kesenjangan_container');
    $('#kesenjangan_loadmore').click(function(e){
        e.preventDefault();
        news_kesenjangan_list('#kesenjangan_container');
    });

    news_sara_list('#sara_container');
    $('#sara_loadmore').click(function(e){
        e.preventDefault();
        news_sara_list('#sara_container');
    });

    news_sosmed_list('#sosmed_container');
    $('#sosmed_loadmore').click(function(e){
        e.preventDefault();
        news_sosmed_list('#sosmed_container');
    });

    news_gayahidup_list('#gayahidup_container');
    $('#gayahidup_loadmore').click(function(e){
        e.preventDefault();
        news_gayahidup_list('#gayahidup_container');
    });

    news_reses_list('#reses_container');
    $('#reses_loadmore').click(function(e){
        e.preventDefault();
        news_reses_list('#reses_container');
    });

    news_indobar_list('#indobar_container');
    $('#indobar_loadmore').click(function(e){
        e.preventDefault();
        news_indobar_list('#indobar_container');
    });

    news_indotim_list('#indotim_container');
    $('#indotim_loadmore').click(function(e){
        e.preventDefault();
        news_indotim_list('#indotim_container');
    });

    news_indoteng_list('#indoteng_container');
    $('#indoteng_loadmore').click(function(e){
        e.preventDefault();
        news_indoteng_list('#indoteng_container');
    });

    $('#linkterkait').click(function(e) {
        var catid = $(this).data('catid');
        $.ajax({
            url : Settings.base_url+'topten/UpdateViewer',
            type : 'post',
            data: {'catid' : catid},
            dataType: "json",
            success: function(data)
            {
                console.log("testing".catid);
            }
        });
    });

});

function comment_list(holder)
{
    var id = $(holder).data('id');
    var page = $(holder).data('page');

    var please = '<div id="comment_loader" class="loader"></div>';
    $(holder).append(please);
    $('#comment_loader').css('visibility', 'visible');

    var uri = Settings.base_url+'timeline/content_comment/'+ id + '/'+page;
    $.get(uri, function(data){
        $(holder).append(data);
        $(holder).data('page', parseInt(page) + 1);
        $('#comment_loader').hide('blind').remove();
    })

}

function alert_msg(msg)
{
    var str = '<div class="alert alert-error">';
    str += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
    str += msg + '</div>';
    return str;
}

function news_ekonomi_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#ekonomi_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#ekonomi_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_tajuk_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#tajuk_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#tajuk_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_hukum_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#hukum_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#hukum_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_parlemen_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#parlemen_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#parlemen_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_nasional_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#nasional_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#nasional_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_daerah_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#daerah_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#daerah_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_internasional_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#inter_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#inter_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_lingkungan_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#lingkungan_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#lingkungan_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_kesenjangan_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#kesenjangan_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#kesenjangan_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_sara_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#sara_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#sara_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_sosmed_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#sosmed_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#sosmed_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_gayahidup_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#gayahidup_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#gayahidup_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_reses_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#reses_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#reses_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_indobar_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#indobar_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#indobar_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_indotim_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#indotim_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#indotim_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function news_indoteng_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var newscat = $(holder).data('category');

    $.get(Settings.base_url+'news/newsloader/'+newscat+'/'+page, function(data){
        if(page == 1) {$('#indoteng_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#indoteng_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}
