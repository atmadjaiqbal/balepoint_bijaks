$(function(){
    //COMMENT ON HOVER
    $('#comment').on('mouseenter', '.comment-row', function(e){
        e.preventDefault();
        $(this).find('.flag').show();

    }).on('mouseleave', '.comment-row' ,function(e){
            $(this).find('.flag').hide();
            $(this).find('.dropdown').removeClass('open');
        });

    $('#comment').on('click', '.report-spam', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var tipe = $(this).data('tipe');
        $('#submit_report>#tipe').val(tipe);
        $('#submit_report>#cid').val(id);
        $('#modal_report').modal('show');
    });

    $('#modal_report').on('hidden', function(){
        $('#submit_report>#tipe').val('');
        $('#submit_report>#report_message').val('');
        $('#submit_report>#cid').val('');
        $('.alert').remove();
    });

    // SUBMIT REPORT SPAM
    $('#submit_report').submit(function(){
        $('.alert').remove();
        var please = '<div id="comment_loader" class="loader"></div>';
        $('#submit_report').before(please);
        $('#comment_loader').css('visibility', 'visible');
        var dt = $(this).serialize();
        $.post(Settings.base_url+'timeline/report_spam', dt, function(data){
            if(data.rcode == 'ok'){
                $('#submit_report').before(alert_msg(data.msg));
            }else{
                var text_msg = 'Data yang di input tidak benar';

                if(typeof data.msg === "string"){
                    text_msg = data.msg;
                }

                if(data.msg.content_id){
                    text_msg += ', '+ data.msg.content_id;
                }
                if(data.msg.report_message){
                    text_msg += ', '+ data.msg.report_message;
                }
                var mksg = alert_msg(text_msg);
                $('#submit_report').before(mksg);
            }
            $('#comment_loader').remove();
        });
        return false;
    });

});