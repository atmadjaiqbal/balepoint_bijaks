var host = '';
$(document).ready(function(){
//    $('.skandal-slim').slimScroll({
//        height: '600px',
//        alwaysVisible: true
//    });
})

$(function(){

    kronologies_list('#kronologies_container');
    $('#kronologies_load_more').click(function(e){
        e.preventDefault();
        kronologies_list('#kronologies_container');
    });
});

function kronologies_list(holder)
{
    var the_loader = '<div id=""wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var scandal = $(holder).data('scandal');

    console.log(Settings.base_url+'scandal/kronologies_list/'+scandal+'/'+page);

    $.get(Settings.base_url+'scandal/kronologies_list/'+scandal+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });
}