var host = '';
$(function(){
    var id = $('#lembaga_list').data('id');
    var user = $('#lembaga_list').data('user');
    follow_list('#lembaga_list', id, user, 1, 1);

    $('#followTab a').click(function (e) {
        e.preventDefault();
        var target = e.target;
        var target_id = $(target).attr('href');
//        console.debug(target_id);
        var id = $(target).data('id');
        var user = $(target).data('user');
        var tipe = $(target).data('tipe');
        if($(target_id+ '_list').is(':empty')){
            follow_list(target_id + '_list', id, user, tipe, 1);
        }
        $(this).tab('show');
    });

    $('#lembaga_load_more').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var user = $(this).data('user');
        var tipe = $(this).data('tipe');
        var page = $(this).data('page');
        follow_list('#lembaga_list', id, user, tipe, page);
        $(this).data('page', page+1);
    });

    $('#follower_load_more').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var user = $(this).data('user');
        var tipe = $(this).data('tipe');
        var page = $(this).data('page');
        follow_list('#follower_list', id, user, tipe, page);
        $(this).data('page', page+1);
    });

    $('#follow_load_more').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var user = $(this).data('user');
        var tipe = $(this).data('tipe');
        var page = $(this).data('page');
        follow_list('#follow_list', id, user, tipe, page);
        $(this).data('page', page+1);
    });

    $('#lembaga_list').on("click", '.unfollow', function(e){
        e.preventDefault();
        if(confirm('Yakin unfollow user ini')){
            var tipe = $(this).data('tipe');
            var id = $(this).data('id');
            var user = $(this).data('user');
            var uri = Settings.base_url+'/timeline/unfollow';
            var data = {'tipe':tipe, 'id':id, 'user':user};
            var parent = $(this).parent().parent();
            $.post(uri, data, function(res){
               if(res.rcode == 'ok'){
                   parent.next().remove();
                   parent.hide('blind').remove();
               }
            });
        }

    });

    $('#follower_list').on("click", '.unfollow', function(e){
        e.preventDefault();
        if(confirm('Yakin unfollow user ini')){
            var tipe = $(this).data('tipe');
            var id = $(this).data('id');
            var user = $(this).data('user');
            var uri = Settings.base_url+'/timeline/unfollow';
            var data = {'tipe':tipe, 'id':id, 'user':user};
            var parent = $(this).parent().parent();
            $.post(uri, data, function(res){
                if(res.rcode == 'ok'){
                    parent.next().remove();
                    parent.hide('blind').remove();
                }
            });
        }

    });

    $('#follow_list').on("click", '.unfollow', function(e){
        e.preventDefault();
        if(confirm('Yakin unfollow user ini')){
            var tipe = $(this).data('tipe');
            var id = $(this).data('id');
            var user = $(this).data('user');
            var uri = Settings.base_url+'/timeline/unfollow';
            var data = {'tipe':tipe, 'id':id, 'user':user};
            var parent = $(this).parent().parent();
            $.post(uri, data, function(res){
                if(res.rcode == 'ok'){
                    parent.next().remove();
                    parent.hide('blind').remove();
                }
            });
        }

    });

})

function follow_list(holder, id, user_id, tipe, page)
{
    var foll = $(holder);
    var please = '<div id="'+tipe+'_wall_loader" class="loader"></div>'; // '<p id="wall_loader">Please wait...</p>';
    foll.append(please);
    $('#'+tipe+'_wall_loader').css('visibility', 'visible');
    var to_id = id;

    $.ajax({
        url:Settings.base_url+'/komunitas/follower_list/'+user_id+'/'+to_id+'/'+tipe+'/'+page
    }).done(function(html){
            $('#'+tipe+'_wall_loader').hide('blind').remove();
            foll.append(html);
            page = foll.data('page') + 1;
            foll.data('page', page);

        });
}

