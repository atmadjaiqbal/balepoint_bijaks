$(function(){
    $('#profileTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var komu = $('#wall_list');
    wall_list(komu);
    $('#wall_load_more').click(function(e){
        e.preventDefault();
        wall_list(komu);
    });

    $('#comment_load_more').click(function(e){
        e.preventDefault();
        comment_list('#comment');
    });

    comment_list('#comment');

    scandal_list('#profile_scandal_container');
    $('#profile_scandal_load_more').click(function(e){
        e.preventDefault();
        scandal_list('#profile_scandal_container');
    });

    news_list('#profile_news_container');
    $('#profile_news_load_more').click(function(e){
        e.preventDefault();
        news_list('#profile_news_container');
    });

    scandal_score_click('#profile_news_container');
    scandal_score_click('profile_scandal_container');

})

function scandal_list(holder)
{
    var please = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(please);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var user = $(holder).data('user');

    $.get(Settings.base_url+'/aktor/scandal_list/'+user+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
        $('.score').each(function(a, b){
            var id = $(b).data('id');
            var tipe = '0';
            if($(b).data('tipe')){
                tipe = $(b).data('tipe');
            }
            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
        });

//        scandal_score_click(holder);
    });


}

function news_list(holder)
{
    var please = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(please);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    var user = $(holder).data('user');

    $.get(Settings.base_url+'/aktor/news_list/'+user+'/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
        $('.score').each(function(a, b){
            var id = $(b).data('id');
            var tipe = '0';
            if($(b).data('tipe')){
                tipe = $(b).data('tipe');
            }
            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
        });

//        scandal_score_click(holder);
    });


}

function scandal_score_click(holder)
{
    $(holder).on('click', ".score-btn", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });
}

function load_coment()
{
    $.each('.comment-container-wall', function(a, b){
        console.debug(b);
        wall_list(b);
    })
}

function alert_msg(msg)
{
    var str = '<div class="alert alert-error">';
    str += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
    str += msg + '</div>';
    return str;
}

function wall_list(komu)
{
    var please = '<div id="wall_loader" class="loader"></div>';
    komu.append(please);
    $('#wall_loader').css('visibility', 'visible');
    $.ajax({
        url: Settings.base_url+'/profile/walllist/'+komu.data('user')+'/'+komu.data('page'),
        data: {'id':komu.data('id'), 'page':komu.data('page')},
        type: "POST"
    }).done(function(html){
            $('#wall_loader').hide('blind').remove();
            komu.append(html);
            page = komu.data('page') + 1;
            komu.data('page', page);
            var coment = $('.comment-container-wall'); //.children('.comment-container-wall');
            $.each(coment, function(a, b){
//                console.debug(b);
                comment_list(b);
            })

        });
};

function comment_list(holder)
{

    var id = $(holder).data('id');
    var limit = $(holder).data('limit');
    var offset = $(holder).data('offset');

    var please = '<div id="comment_loader" class="loader"></div>';
    $(holder).append(please);
    $('#comment_loader').css('visibility', 'visible');

    var uri = Settings.base_url+'timeline/profile_comment/'+ id + '/'+limit+'/'+offset;
    $.get(uri, function(data){
        if(data.rcode == 'ok'){
            someText = data.msg.replace(/(\r\n|\n|\r)/gm," ");
//            console.debug(someText.toString().trim().length);
            if(someText.toString().trim().length > 0){
                $(holder).show();
                $(holder).append(data.msg);
                $(holder).data('offset', parseInt(limit) + parseInt(offset));
            }

        }

        $('#comment_loader').hide('blind').remove();
    })

}