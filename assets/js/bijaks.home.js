$(function() {

    var powermap_ready = false;
    var base_uri = '';
    $('.home #hot-profile .col-1-1').height('auto');
    $('.home-hot-profile-detail').eq(0).addClass('active');
    $('.home-hot-lembaga-detail').eq(0).addClass('active');

    $('#home-hot-profile-menu ul li').eq(0).addClass(function(){
        var id = $(this).data('id');
        var place = 'pm_'+id;
        var emp = $('#'+place).html();
        // console.debug(id);
        if($.trim(emp).length == 0){
            // console.debug(place);
            var uri = $('#'+place).data('uri');
            var width = '575';
            var height = '205';

            var data = '<iframe marginheight="0px" marginwidth="0px" width="575" height="205" scrolling="no" frameborder="0" src="'+uri+'?w=575&h=205&t=2">';
            data += '</iframe>';


            $('#'+place).append(data);

            // $.post(uri, {'w': width , 'h': height, 't' : '2'}, function(data){
            // 	$('#'+place).append(data);
            // });

            /*$.ajax({
             type:'post',
             url:uri,
             data:{'w': width , 'h': height, 't' : '2'},
             async : false
             }).done(function(data){
             $('#'+place).html(data);
             powermap_ready = true;
             }); */

        }

        return 'active';
    });

    $('#home-hot-lembaga-menu ul li').eq(0).addClass(function(){
        var id = $(this).data('id');
        var place = 'pm_'+id;
        var emp = $('#'+place).html();
        if($.trim(emp).length == 0){
            var uri = $('#'+place).data('uri');
            var width = '575';
            var height = '205';
            var data = '<iframe marginheight="0px" marginwidth="0px" width="575" height="205" scrolling="no" frameborder="0" src="'+uri+'?w=575&h=205&t=2">';
            data += '</iframe>';
            $('#'+place).append(data);
        }

        return 'active';
    });


//    $('#home-hot-profile-marker').animate({top:65});
    $('#home-hot-profile-menu ul li').click(function() {
        // console.debug($(this).data('id'));
        idx = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $('.home-hot-profile-detail').eq(idx).fadeIn('slow').addClass('active').siblings().removeClass('active').hide();
        ttop = $(this).position().top;
        mpos = ttop + 65; // http://i.imgur.com/zZHCqKh.gif
        $('#home-hot-profile-marker').animate({top:mpos});

        var id = $(this).data('id');
        var place = 'pm_'+id;
        var emp = $('#'+place).html();
        console.debug(id);
        if($.trim(emp).length == 0){
            // console.debug(place);
            var uri = $('#'+place).data('uri');
            var width = '575';
            var height = '205';

            var data = '<iframe marginheight="0px" marginwidth="0px" width="575" height="205" scrolling="no" frameborder="0" src="'+uri+'?w=575&h=205&t=2">';
            data += '</iframe>';


            $('#'+place).append(data);

            /*$.post(uri, {'w': width , 'h': height, 't' : '2'}, function(data){
             $('#'+place).append(data);
             });*/
            // home_map(width, height, uri, place, '2');
        }

    });

    $('#home-hot-lembaga-menu ul li').click(function() {
        idx = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $('.home-hot-lembaga-detail').eq(idx).fadeIn('slow').addClass('active').siblings().removeClass('active').hide();
        ttop = $(this).position().top;
        mpos = ttop + 65; // http://i.imgur.com/zZHCqKh.gif
        $('#home-hot-lembaga-marker').animate({top:mpos});

        var id = $(this).data('id');
        var place = 'pm_'+id;
        var emp = $('#'+place).html();
        console.debug(id);
        if($.trim(emp).length == 0){
            var uri = $('#'+place).data('uri');
            var width = '575';
            var height = '205';

            var data = '<iframe marginheight="0px" marginwidth="0px" width="575" height="205" scrolling="no" frameborder="0" src="'+uri+'?w=575&h=205&t=2">';
            data += '</iframe>';

            $('#'+place).append(data);
        }

    });

    $('.tooltip-top').tooltip();
    $('.tooltip-left').tooltip({placement:'left'});
    $('.tooltip-right').tooltip({placement:'right'});
    $('.tooltip-bottom').css('color', '#cecece;');
    $('.tooltip-bottom').tooltip({placement:'bottom'})


    //Suksesi Tabs When page loads...
    $('.home-content-suksesi').each(function() {
        $(this).find(".nav-tabs > li:first").addClass("active"); //Activate first tab
        $(this).find(".tab-pane:first").addClass("active"); //Show first tab content

       /*-- $(this).find(".tab-content").slimScroll({height: '485px', railDraggable : true, railVisible: true, alwaysVisible: true}); --*/
        //$("#content-"+this.id).slimScroll({height: '200px'});
        //$(this).find(".tab-content").slimScroll({height: '300px'}); // make scroable

    });

    //Suksesi Tabs On Click Event
    $(".home-content-suksesi > ul.nav-tabs > li > a").click(function(e) {
        $(this).parents('.home-content-suksesi').find(".tab-pane").removeClass("active").hide(); //Remove any "active" & "hide" tab content
        $(this).parents('.home-content-suksesi').find(".nav-tabs > li").removeClass("active"); //Remove any "active" tab pange

        var tabID = this.id;
        $(".home-content-suksesi #conten-"+tabID).addClass("active").show();
        $(".home-content-suksesi #list-"+tabID).addClass("active");
        e.preventDefault();
    });
    
    	// generic load more
	$('.load-more').click(function() {
		self = $(this);
		var page = $(this).attr("data-page");
		var url  =  $(this).attr("data-url") + page;
        var sekarang = parseInt($(this).attr("data-sekarang"));
        if(sekarang == 0)
        {
            $('.load-more').hide();
        } else {
            self.parent().find('.load-more-loader').addClass('loading');
            $.ajax({url: url,cache: false}).done(function( response ) {
                self.parent().parent().append(response)
                self.parent().remove();
            });
        }
	});

    setInterval(function() {
        newshot_list('#newshot_container');
        parlehot_list('#parlehot_container');
    }, 900000);

    $(".open-politik-others").on("click", function() {
        var shownews = $(this).data('shownews');
        if(shownews == '1')
        {
            $("#politik-expand").hide();
            $("#politik-others").collapse('show');
            $('.status-collapse').text('menutup');
            $(this).data('shownews', 2);
        } else {
            $("#politik-expand").show();
            $("#politik-others").collapse('hide');
            $('.status-collapse').text('menampilkan');
            $(this).data('shownews', 1);
        }
    });

    terkini_list('#terkini_container');
    $('#terkini_loadmore').click(function(e){
        e.preventDefault();
        terkini_list('#terkini_container');
    });

    headline_list('#headline_container');
    $('#headline_loadmore').click(function(e){
        e.preventDefault();
        headline_list('#headline_container');
    });
        
    kampusabal2_list('#kampusabal2_container');
    $('#kampusabal2_loadmore').click(function(e){
        e.preventDefault();
        kampusabal2_list('#kampusabal2_container');
    });

    asap_list('#asap_container');
    $('#kampusabal2_loadmore').click(function(e){
        e.preventDefault();
        kampusabal2_list('#kampusabal2_container');
    });

    berita_list('#tambang_container','news_tambang');
    $('#tambang_loadmore').click(function(e){
        e.preventDefault();
        berita_list('#tambang_container','news_tambang');
    });

    berita_list('#restorasi_container','news_restorasi');
    $('#restorasi_loadmore').click(function(e){
        e.preventDefault();
        berita_list('#restorasi_container','news_restorasi');
    });

    newsgaza_list('#newsgaza_container');
    $('#newsgaza_loadmore').click(function(e){
        e.preventDefault();
        newsgaza_list('#newsgaza_container');
    });

    newsisis_list('#newsisis_container');
    $('#newsisis_loadmore').click(function(e){
        e.preventDefault();
        newsisis_list('#newsisis_container');
    });

    newslionair_list('#newslionair_container');
    $('#newslionair_loadmore').click(function(e){
        e.preventDefault();
        newslionair_list('#newslionair_container');
    });

    newsahok_list('#newsahok_container');
    $('#newsahok_loadmore').click(function(e){
        e.preventDefault();
        newsahok_list('#newsahok_container');
    });

    newskaa_list('#newskaa_container');
    $('#newskaa_loadmore').click(function(e){
        e.preventDefault();
        newskaa_list('#newskaa_container');
    });

    newsgolkar_list('#newsgolkar_container');
    $('#newsgolkar_loadmore').click(function(e){
        e.preventDefault();
        newsgolkar_list('#newsgolkar_container');
    });

    newssitusradikal_list('#newssitusradikal_container');
    $('#newssitusradikal_loadmore').click(function(e){
        e.preventDefault();
        newssitusradikal_list('#newssitusradikal_container');
    });

    newspssi_list('#newspssi_container');
    $('#newspssi_loadmore').click(function(e){
        e.preventDefault();
        newspssi_list('#newspssi_container');
    });

    newsprostitusi_list('#newsprostitusi_container');
    $('#newsprostitusi_loadmore').click(function(e){
        e.preventDefault();
        newsprostitusi_list('#newsprostitusi_container');
    });

    newsdanaaspirasi_list('#newsdanaaspirasi_container');
    $('#newsdanaaspirasi_loadmore').click(function(e){
        e.preventDefault();
        newsdanaaspirasi_list('#newsdanaaspirasi_container');
    });

    newskampungpulo_list('#newskampungpulo_container');
    $('#newspasalhantu_loadmore').click(function(e){
        e.preventDefault();
        newskampungpulo_list('#newskampungpulo_container');
    });

    newspasalhantu_list('#newspasalhantu_container');
    $('#newskampungpulo_loadmore').click(function(e){
        e.preventDefault();
        newspasalhantu_list('#newspasalhantu_container');
    });

    newstolikara_list('#newstolikara_container');
    $('#newstolikara_loadmore').click(function(e){
        e.preventDefault();
        newstolikara_list('#newstolikara_container');
    });

    newspetral_list('#newspetral_container');
    $('#newspetral_loadmore').click(function(e){
        e.preventDefault();
        newspetral_list('#newspetral_container');
    });

    newssabdaraja_list('#newssabdaraja_container');
    $('#newssabdaraja_loadmore').click(function(e){
        e.preventDefault();
        newssabdaraja_list('#newssabdaraja_container');
    });

    newshukuman_list('#newshukuman_container');
    $('#newshukuman_loadmore').click(function(e){
        e.preventDefault();
        newshukuman_list('#newshukuman_container');
    });

    newsuu_list('#newsuu_container');
    $('#newsuu_loadmore').click(function(e){
        e.preventDefault();
        newsuu_list('#newsuu_container');
    });

    cakapolri_list('#cakapolri_container');
    $('#cakapolri_loadmore').click(function(e){
        e.preventDefault();
        cakapolri_list('#cakapolri_container');
    });

    newshot_list('#newshot_container');
    $('#newshot_loadmore').click(function(e){
        e.preventDefault();
        newshot_list('#newshot_container');
    });

    parlehot_list('#parlehot_container');
    $('#parlehot_loadmore').click(function(e){
        e.preventDefault();
        parlehot_list('#parlehot_container');
    });

    komentarlist('#komentar_container');
    $('#komentar_loadmore').click(function(e){
        e.preventDefault();
        komentarlist('#komentar_container');
    });

    bloglist('#blog_container');
    $('#blog_loadmore').click(function(e){
        e.preventDefault();
        bloglist('#blog_container');
    });

    //bloglatest('#bloglatest_container');
    //$('#bloglatest_loadmore').click(function(e){
    //    e.preventDefault();
    //    bloglatest('#bloglatest_container');
    //});

    bloglatest_onscroll('#bloglatest_container');    
    $('#bloglatest_loadmore').click(function(e){
        e.preventDefault();
        bloglatest_onscroll('#bloglatest_container');
    });

    bloggaza_onscroll('#bloggaza_container');
    blogisis_onscroll('#blogisis_container');
    bloglatest_devel('#bloglatestdevel_container');
/*
    $('#bloglatest_container').scroll(function(e){
        e.preventDefault();
        bloglatest_onscroll('#bloglatest_container');
    });
*/

});



function terkini_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'home/terkini_list/'+page, function(data){
        if(page == 1) {$('#terkini_loadmore').text('12 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#terkini_loadmore').text('12 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function headline_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'home/headline_list/'+page, function(data){
        if(page == 1) {$('#headline_loadmore').text('10 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#headline_loadmore').text('10 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newshot_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var newstype = $(holder).data('newstype');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'news/home_hotlist/'+newstype+'/'+page, function(data){
        if(page == 1) {$('#newshot_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newshot_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function parlehot_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var newstype = $(holder).data('newstype');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'news/home_hotlist/'+newstype+'/'+page, function(data){
        if(page == 1) {$('#parlehot_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#parlehot_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function komentarlist(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'home/KomentarKomu/'+page, function(data){
        if(page == 1) {$('#komentar_loadmore').text('20 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#komentar_loadmore').text('20 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function bloglist(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'home/BlogKomu/'+page, function(data){
        if(page == 1) {$('#blog_loadmore').text('5 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#blog_loadmore').text('5 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function bloglatest(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'home/BlogLatest/'+page, function(data){
        if(page == 1) {$('#bloglatest_loadmore').text('15 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#bloglatest_loadmore').text('15 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function bloglatest_onscroll(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    $.get(Settings.base_url+'home/BlogLatest/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });
}

function bloggaza_onscroll(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    $.get(Settings.base_url+'hotpages/BlogGaza/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });
}

function blogisis_onscroll(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    $.get(Settings.base_url+'hotpages/BlogIsis/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });
}

function bloglatest_devel(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    $.get(Settings.base_url+'devel/BlogLatest/'+page+'?pwd=bijaks321', function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });
}

function newsgaza_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newsgaza_list/'+page, function(data){
        if(page == 1) {$('#newsgaza_loadmore').text('15 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newsgaza_loadmore').text('15 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newsisis_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newsisis_list/'+page, function(data){
        if(page == 1) {$('#newsisis_loadmore').text('15 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newsisis_loadmore').text('15 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newslionair_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newslionair_list/'+page, function(data){
        if(page == 1) {$('#newslionair_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newslionair_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newsahok_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newsahok_list/'+page, function(data){
        if(page == 1) {$('#newsahok_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newsahok_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newsgolkar_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newsgolkar_list/'+page, function(data){
        if(page == 1) {$('#newsgolkar_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newsgolkar_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newskaa_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newskaa_list/'+page, function(data){
        if(page == 1) {$('#newskaa_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newskaa_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newssitusradikal_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newssitusradikal_list/'+page, function(data){
        if(page == 1) {$('#newssitusradikal_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newssitusradikal_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newspssi_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newspssi_list/'+page, function(data){
        if(page == 1) {$('#newspssi_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newspssi_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newsprostitusi_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newsprostitusi_list/'+page, function(data){
        if(page == 1) {$('#newsprostitusi_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newsprostitusi_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newsdanaaspirasi_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newsdanaaspirasi_list/'+page, function(data){
        if(page == 1) {$('#newsdanaaspirasi_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newsdanaaspirasi_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newskampungpulo_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newskampungpulo_list/'+page, function(data){
        if(page == 1) {$('#newskampungpulo_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newskampungpulo_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newspasalhantu_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newspasalhantu_list/'+page, function(data){
        if(page == 1) {$('#newspasalhantu_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newspasalhantu_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newstolikara_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newstolikara_list/'+page, function(data){
        if(page == 1) {$('#newstolikara_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newstolikara_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newspetral_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newspetral_list/'+page, function(data){
        if(page == 1) {$('#newspetral_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newspetral_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newssabdaraja_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newssabdaraja_list/'+page, function(data){
        if(page == 1) {$('#newssabdaraja_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newssabdaraja_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newshukuman_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newshukuman_list/'+page, function(data){
        if(page == 1) {$('#newshukuman_loadmore').text('Selanjutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newshukuman_loadmore').text('Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function newsuu_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newspilkada_list/'+page, function(data){
        if(page == 1) {$('#newsuu_loadmore').text('10 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newsuu_loadmore').text('10 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function cakapolri_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/cakapolri_list/'+page, function(data){
        if(page == 1) {$('#cakapolri_loadmore').text('10 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#cakapolri_loadmore').text('10 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function kampusabal2_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newskampus_abal2/'+page, function(data){
        if(page == 1) {$('#newsuu_loadmore').text('7 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#newsuu_loadmore').text('7 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function asap_list(holder)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/newsasap/'+page, function(data){
        if(page == 1) {$('#asap_loadmore').text('7 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#asap_loadmore').text('7 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}

function berita_list(holder,controller)
{
    $(holder).html('');
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');
    $.get(Settings.base_url+'hotpages/'+controller+'/'+page, function(data){
        if(page == 1) {$('#asap_loadmore').text('7 Berikutnya'); $(holder).data('page', 2);}
        if(page == 2) {$('#asap_loadmore').text('7 Sebelumnya'); $(holder).data('page', 1);}
        $(holder).append(data);
        $('#wall_loader').remove();
    });
}