$(function(){
    $('#edit_dasar').click(function(){
        $('#info_dasar').hide();
        $('#edit_info_dasar').show();
    });

    $('#edit_kontak').click(function(){
        $('#info_kontak').hide();
        $('#edit_info_kontak').show();

    });

    $('#batal_dasar').click(function(){
        $('#edit_info_dasar').hide();
        $('#info_dasar').show();
    });

    $('#batal_kontak').click(function(){
        $('#edit_info_kontak').hide();
        $('#info_kontak').show();
    });

    var checkin = $('#dob').datepicker({
        format : 'yyyy-mm-dd'
    });

    $('#edit_form_dasar').submit(function(e){
        e.preventDefault();
        $('#dasar_loading').show();
        var data = $(this).serialize();
        $.post(Settings.base_url+'timeline/post_dasar', data, function(res){
            if(res.rcode == 'ok'){
                $('#dasar_loading').hide();
                location.reload();
            }else{
                var msg = '';
                if(res.msg.fname){
                    msg += res.msg.fname;
                }
                if(res.msg.lname){
                    msg += res.msg.lname;
                }
                if(res.msg.birthday){
                    msg += res.msg.birthday;
                }
                if(res.msg.current_city){
                    msg += res.msg.current_city;
                }
                message_profile(msg);
            }
        });
    });

    $('#edit_form_kontak').submit(function(e){
        e.preventDefault();
        $('#kontak_loading').show();
        var data = $(this).serialize();
        $.post(Settings.base_url+'timeline/post_kontak', data, function(res){
            if(res.rcode == 'ok'){
                $('#kontak_loading').hide();
                location.reload();
            }else{
                var msg = '';
                if(res.msg.address){
                    msg += res.msg.address;
                }
                if(res.msg.city_state){
                    msg += res.msg.city_state;
                }
                if(res.msg.country){
                    msg += res.msg.country;
                }
                if(res.msg.province_state){
                    msg += res.msg.province_state;
                }
                message_profile(msg);
            }
        });
    })

    //add as friend
    $('#teman').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        if(tipe == '1'){
            if(confirm('Yakin ingin menghapus teman ini ?')){
                var id = $(this).data('id');
                var user = $(this).data('user');
                var fid = $(this).data('fid');
                var uri = Settings.base_url+'/timeline/unfriend';
                var data = {'tipe':tipe, 'id':id, 'user':user, 'fid':fid};
                var parent = $(this).parent().parent();
                $.post(uri, data, function(res){
                    if(res.rcode == 'ok'){
                       location.reload();
                    }
                });
            }
        }else{
            var user = $(this).data('user');
            $('#submit_friend>#fid').val(user);
            $('#modal_teman').modal('show');
        }
    });

    $('#reste').click(function(){
        $('#modal_teman').modal('hide');
    })

    $('#modal_teman').on('hidden', function(){
        $('#submit_friend>#report_message').val('');
        $('#submit_friend>#fid').val('');
        $('.alert').remove();
    });

    $('#submit_friend').submit(function(){
        $('.alert').remove();
        var please = '<div id="comment_loader" class="loader"></div>';
        $('#submit_friend').before(please);
        $('#comment_loader').css('visibility', 'visible');
        var dt = $(this).serialize();
        $.post(Settings.base_url+'timeline/add_friend', dt, function(data){
            if(data.rcode == 'ok'){
                $('#submit_friend').before(alert_msg(data.msg));
            }else{
                var text_msg = 'Data yang di input tidak benar';

                if(typeof data.msg === "string"){
                    text_msg = data.msg;
                }

                var mksg = alert_msg(text_msg);
                $('#submit_friend').before(mksg);
            }
            $('#comment_loader').remove();
        });
        return false;
    });


    //*** test **//

    politisi_comment_list('#politisi_comment');

    $('#politisi_comment_load_more').click(function(e){
        e.preventDefault();
        politisi_comment_list('#politisi_comment');
    });


    function politisi_comment_list(holder)
    {
        var id = $(holder).data('id');
        var page = $(holder).data('page');

        var please = '<div id="comment_loader" class="loader"></div>';
        $(holder).append(please);
        $('#comment_loader').css('visibility', 'visible');

        var uri = Settings.base_url+'politik/user_politisi_comment/'+ id + '/'+page;
        $.get(uri, function(data){
            $(holder).append(data);
            $(holder).data('page', parseInt(page) + 1);
            $('#comment_loader').hide('blind').remove();
        })

    }

    $('#send_comment').click(function(e){
        var id = $('#comment_type').data('id');
        var val = $('#comment_type').val();
        var please = '<div id="comment_loader" class="loader"></div>';
        $('#politisi_comment').prepend(please);
        $('#comment_loader').css('visibility', 'visible');
        $.post(Settings.base_url+'/politik/user_post_comment/', {'id':id, 'val':val}, function(data){
            $('#comment_loader').remove();
            if(data.rcode == 'ok'){
                $('#politisi_comment').prepend(data.msg);
                $('.score').each(function(a, b){
                    var id = $(b).data('id');
                    $(b).load(Settings.base_url+'/timeline/last_score/'+id);
                });
            }else{
                alert(data.msg);
            }
            $('#comment_type').val('');
        })
    });

    $('#follow').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var is_follow = $(this).data('follow');
        $.post(Settings.base_url+'/politik/followaction/', {'pid':id, 'follow':is_follow}, function(data){
            if(data.message != 'OK'){
                alert('Maaf, tidak bisa melakukan follow untuk politisi ini.');
            } else {
                if(is_follow != '0'){
                    $('#follow').text('FOLLOW');
                    $('#follow').data('follow','0');
                    $('#follow').append(' <i class="icon-ok icon-white">');
                } else {
                    $('#follow').text('UNFOLLOW');
                    $('#follow').data('follow','1');
                    $('#follow').append(' <i class="icon-remove icon-white">');
                }
            }

        });

    });


})

function message_profile(data)
{
    var msg = '<div id="alert_s" class="alert">';
        msg += '<button type="button" class="close" data-dismiss="alert">&times;</button>'
        msg += '<strong>Error!</strong>'+data+'</div>';
    $('#info').before(msg);
//    console.debug($('#info'));

    setInterval(function(){$('#alert_s').hide('bind').remove()},8000);

}
